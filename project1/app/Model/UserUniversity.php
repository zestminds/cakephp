<?php
App::uses('AppModel', 'Model');
/**
 * UserUniversity Model
 *
 * @property User $User
 * @property University $University
 */
class UserUniversity extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'user_id';


	// The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'University' => array(
			'className' => 'University',
			'foreignKey' => 'university_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
