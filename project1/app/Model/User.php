<?php
App::uses('AppModel', 'Model');
/**
 * User Model
 *
 * @property UserType $UserType
 * @property Biiling $Biiling
 * @property Tag $Tag
 * @property UserCategory $UserCategory
 * @property UserDetail $UserDetail
 * @property UserUniversity $UserUniversity
 * @property VoucherSale $VoucherSale
 * @property Voucher $Voucher
 */
class User extends AppModel {

	var $tmpData = array();

	// The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'UserType' => array(
			'className' => 'UserType',
			'foreignKey' => 'user_type_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
	
	
	public $hasOne = array(
		"UserDetail" => array(
			"className" => "UserDetail",
			"foreignKey" => "user_id",
			"type" => "Inner",
			"dependent"=>true
			
		)
	);

//~ Public $hasMany = array(
//~ 'UserCategory' => array(
			//~ 'className' => 'UserCategory',
			//~ 'foreignKey' => 'user_id',
			//~ 'dependent' => false,
			//~ 'conditions' => '',
			//~ 'fields' => '',
			//~ 'order' => '',
			//~ 'limit' => '',
			//~ 'offset' => '',
			//~ 'exclusive' => '',
			//~ 'finderQuery' => '',
			//~ 'counterQuery' => ''
		//~ )
//~ );
/**
 * hasMany associations
 *
 * @var array
 */
 /*
	public $hasMany = array(
		'Biiling' => array(
			'className' => 'Biiling',
			'foreignKey' => 'user_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Tag' => array(
			'className' => 'Tag',
			'foreignKey' => 'user_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'UserCategory' => array(
			'className' => 'UserCategory',
			'foreignKey' => 'user_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'VoucherSale' => array(
			'className' => 'VoucherSale',
			'foreignKey' => 'user_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Voucher' => array(
			'className' => 'Voucher',
			'foreignKey' => 'user_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);
	
	*/
	public $validate = array (
		"username" => array (
			"notempty" => array (
				"rule" => "notBlank",
				"message" => "Please enter email."
			),
			
			"email" => array (
				"rule" => "email",
				"message" => "Please enter valid email."
			),
			
			"isunique" => array (
				"rule" => "isunique",
				"message" => "email is already in use.",
				//"on"=>"create"
			),
			
		),
		"password" => array (
		    "notempty" => array (
				"rule" => "notBlank",
				"message" => "Please enter Password."
				
			),
				
			"length" => array (
				"rule" => array ('between', 6, 20),
				"message" => "Your password must be between 6 and 20 characters."
			)
		),
		"confirm_password" => array(
            "notempty" => array (
				"rule" => "notBlank",
				"message" => "Please enter confirm Password."
				
			),
              "match"=>array(
              "rule" => "validate_password",
              "message" => "Passwords do not match"
      )
    )
		
		
	);
	
	function validate_password() {
		return (isset($this->data['User']['confirm_password']) && ($this->data['User']['password'] == $this->data['User']['confirm_password']))?true:false;
	}
	
	function afterDelete() {
		
		if(!empty($this->tmpData)){
			$conditions="";
			foreach($this->tmpData as $id)
			{										
					$location_model = ClassRegistry::init('Location');	
					$voucher_model = ClassRegistry::init('Voucher');
					$userdetail_model = ClassRegistry::init('UserDetail');
					
				    $userdetail=$userdetail_model->find("first",array("conditions"=>array("UserDetail.user_id"=>$id)));
				     
					$userdetail_model->id = $userdetail['UserDetail']["id"];
					$userdetail_model->delete();
					
					$voucher_model->belongsTo = array(
							"VoucherCategory" => array(
							"className" => "VoucherCategory",
							"foreignKey" => false,
							"type" => "left",
							"conditions"=>"VoucherCategory.voucher_id=Voucher.id"
						)
					);	
					
					$voucher=$voucher_model->find("all",array("conditions"=>array("Voucher.user_id"=>$id),'fields' => array('Voucher.id')));
					//pr($voucher);				die;
					if(!empty($voucher))
					{	
						foreach($voucher as $val)
						{				
							if($voucher_model->VoucherCategory->hasAny(array('voucher_id' => $val["Voucher"]["id"])))
							{
								$voucher_model->VoucherCategory->deleteAll(array('voucher_id' => $val["Voucher"]["id"]));
							}
							$voucher_model->id = $val["Voucher"]["id"];
							$voucher_model->delete();
						}
						 
					}
					$location=$location_model->find("all",array("conditions"=>array("Location.user_id"=>$id),'fields' => array('Location.id','Location.payment_profile_id')));
					if(!empty($location))
					{	
						foreach($location as $val)
						{	
							$location_model->id=$val["Location"]["id"];
							$location_model->delete();
							if($val["Location"]["payment_profile_id"]!=""){							
								$result = $this->Paypal->change_subscription_status($val["Location"]["payment_profile_id"],"Cancel");
							}						
						}
					}	
				 }
						
			}
		}
	

}
