<?php
App::uses('AppModel', 'Model');
/**
 * Billing Model
 *
 * @property User $User
 * @property Plan $Plan
 */
class Billing extends AppModel {


	// The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Plan' => array(
			'className' => 'Plan',
			'foreignKey' => 'plan_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'UserDetail' => array(
			'className' => 'UserDetail',
			'foreignKey' => '',
			'conditions' => 'UserDetail.user_id=Billing.user_id',
			'fields' => '',
			'order' => ''
		),
		'Location' => array(
			'className' => 'Location',
			'foreignKey' => '',
			'conditions' => 'Location.user_id=Billing.user_id',
			'fields' => '',
			'order' => ''
		)
		
	);
}
