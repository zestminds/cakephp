<?php
App::uses('AppModel', 'Model');
/**
 * Location Model
 *
 */
class Location extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'title';
	
	public $hasMany = array(
			"LocationImage" => array(
			"className" => "LocationImage",
			"foreignKey" => "location_id",
			"type" => "Inner",
			"dependent"=>true
			)
		);
}
