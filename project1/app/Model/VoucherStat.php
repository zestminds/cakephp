<?php
App::uses('AppModel', 'Model');
/**
 * VoucherSale Model
 *
 * @property User $User
 * @property Voucher $Voucher
 */
class VoucherStat extends AppModel {


	// The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),/*
		'Voucher' => array(
			'className' => 'Voucher',
			'foreignKey' => 'voucher_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),*/
		'UserDetail' => array(
			'className' => 'UserDetail',
			'foreignKey' => 'user_id',
		    'conditions' => 'User.id = UserDetail.user_id',
			'fields' => '',
			'order' => ''
		)
	);
/*	
public $hasMany= array(
		"VoucherSale" => array(
			"className" => "VoucherSale",
			"foreignKey" => "user_id",
			"type" => "Inner",
			'dependent' => true,
		)
	);
	*/
}
