<?php
App::uses('AppModel', 'Model');
/**
 * Voucher Model
 *
 * @property User $User
 * @property VoucherSale $VoucherSale
 */
class Voucher extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'title';


	// The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
	public $hasMany = array(		
		'VoucherCategory' => array(
			'className' => 'VoucherCategory',
			'foreignKey' => 'voucher_id',
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'dependent'=>true
		)
	);
/**
 * hasMany associations
 *
 * @var array
	public $hasMany = array(
		'VoucherSale' => array(
			'className' => 'VoucherSale',
			'foreignKey' => 'voucher_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	); */
	

}
