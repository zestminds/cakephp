<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
 
/**
 * Here, we are connecting '/' (base path) to controller called 'Pages',
 * its action called 'display', and we pass a param to select the view file
 * to use (in this case, /app/View/Pages/home.ctp)...
 */
	Router::connect('/', array('controller' => 'pages', 'action' => 'index'));
	Router::connect('/testpayment', array('controller' => 'pages', 'action' => 'testpayment'));
	Router::connect('/advertise-with-moola', array('controller' => 'pages', 'action' => 'merchant'));
	Router::connect('/pricing', array('controller' => 'pages', 'action' => 'pricing'));
	//Router::connect('/about', array('controller' => 'pages', 'action' => 'about'));
	Router::connect('/advertiser_faqs', array('controller' => 'pages', 'action' => 'advertiser_faqs'));
	Router::connect('/coming_soon/*', array('controller' => 'pages', 'action' => 'coming_soon'));
	Router::connect('/share_url', array('controller' => 'pages', 'action' => 'share_url'));
	Router::connect('/terms_and_conditions', array('controller' => 'pages', 'action' => 'pages_database'));
	Router::connect('/about', array('controller' => 'pages', 'action' => 'pages_database'));
	Router::connect('/privacy_policy', array('controller' => 'pages', 'action' => 'pages_database'));
	Router::connect('/cookie_policy', array('controller' => 'pages', 'action' => 'pages_database'));
	Router::connect('/advertise', array('controller' => 'pages', 'action' => 'pages_database'));
	
	
	Router::connect('/account_confirmation', array('controller' => 'pages', 'action' => 'account_confirmation'));
	Router::connect('/message_confirm', array('controller' => 'pages', 'action' => 'message_confirm'));
	Router::connect('/message_confirm_link', array('controller' => 'pages', 'action' => 'message_confirm_link'));
	Router::connect('/message_forget_password', array('controller' => 'pages', 'action' => 'message_forget_password'));
	Router::connect('/send_notifications/*', array('controller' => 'EmailTemplates', 'action' => 'send_notifications',"admin"=>true));
	Router::connect('/merchants/*', array('controller' => 'Users', 'action' => 'moola_merchant_listing'));
	Router::connect('/m/*', array('controller' => 'Vouchers', 'action' => 'merchant_detail'));
	Router::connect('/moola-for-students/*', array('controller' => 'pages', 'action' => 'index'));
	Router::connect('/student_support_faqs/*', array('controller' => 'pages', 'action' => 'student_support_faqs'));
	Router::connect('/help/*', array('controller' => 'pages', 'action' => 'help'));
	Router::connect('/contact_us/*', array('controller' => 'pages', 'action' => 'contact_us'));
	Router::connect('/sitemap.xml', array('controller' => 'pages', 'action' => 'sitemap'));
	
	App::import('Model', 'Location');
	$location = new Location();
	$locs = $location->find("all",array("fields"=>array("id","title","address"),"recursive"=>-1));
	$tmpUrl = array();
	$tmp = array();
	foreach($locs as $key=>$val) {
		$tval = preg_replace('!\s+!', ' ', $val['Location']['title']);
		$tval .= " ".preg_replace('!\s+!', ' ', $val['Location']['address']);
		$tval = str_replace(array(","),"",$tval);
		//echo $tmpVal = $val['Location']['id']."-".str_replace(array(" "),"-",$tval);		
		$tmpVal = str_replace(array(" "),"-",trim($tval));
		if (in_array($tmpVal,$tmpUrl)) {
			$tmpVal = $tmpVal.count($tmpUrl);
		}
		$tmpUrl[] = $tmpVal; 
		$tmp['Location'][$val['Location']['id']] = array("id"=>$val['Location']['id'],"seourl"=>$tmpVal);
		Router::connect('/'.$tmpVal, array('controller' => 'Vouchers', 'action' => 'merchant_detail'));
	}
	$location->create();
	$location->saveAll($tmp['Location']);
	
	Router::connect('/Manchester-Arena-Apartment-Salford-Manchester-UK', array('controller' => 'pages', 'action' => 'students'));
	
	Router::connect('/login', array('controller' => 'users', 'action' => 'login'));
	Router::connect('/signup', array('controller' => 'users', 'action' => 'signup'));
	Router::connect('/logout', array('controller' => 'users', 'action' => 'logout'));
	Router::connect('/subscribelist', array('controller' => 'users', 'action' => 'subscribelist'));
	Router::connect('/deleteuseraccount', array('controller' => 'users', 'action' => 'deleteuseraccount'));
	Router::connect('/pausepaypalpayment', array('controller' => 'users', 'action' => 'pausepaypalpayment'));
	Router::connect('/confirmlink/*', array('controller' => 'users', 'action' => 'confirmlink'));
	Router::connect('/forgot_password/*', array('controller' => 'users', 'action' => 'forgot_password'));
	Router::connect('/forgot-password/*', array('controller' => 'users', 'action' => 'forgot_password_admin'));
	Router::connect('/reset_password/*', array('controller' => 'pages', 'action' => 'index'));
	Router::connect('/process_reset_password/*', array('controller' => 'users', 'action' => 'reset_password'));
	Router::connect('/sociallogin/*', array('controller' => 'users', 'action' => 'sociallogin'));
	Router::connect('/adminlogin/*', array('controller' => 'users', 'action' => 'adminlogin'));
	Router::connect('/share_voucher/*', array('controller' => 'pages', 'action' => 'share_voucher'));
	
	Router::connect('/addbusiness/*', array('controller' => 'Locations', 'action' => 'addbusiness'));
	Router::connect('/business/*', array('controller' => 'users', 'action' => 'business'));
	Router::connect('/b/*', array('controller' => 'Locations', 'action' => 'business'));
	Router::connect('/deleteuserbusiness/*', array('controller' => 'Locations', 'action'=> 'deleteuserbusiness'));
	Router::connect('/processcart/*', array('controller' => 'Locations', 'action' => 'processcart'));
	Router::connect('/locations/*', array('controller' => 'Locations', 'action' => 'index'));
	Router::connect('/my_cart/*', array('controller' => 'Locations', 'action' => 'my_cart'));
	//Router::connect('/locationtocart/*', array('controller' => 'Locations', 'action' => 'locationtocart'));
	//Router::connect('/deletelocationtocart/*', array('controller' => 'Locations', 'action' => 'deletelocationtocart'));
	Router::connect('/checklocation/*', array('controller' => 'Vouchers', 'action' => 'checklocation'));
	Router::connect('/publish_all_vouchers/*', array('controller' => 'Vouchers', 'action' => 'publish_all_vouchers'));
	Router::connect('/add_location/*', array('controller' => 'Locations', 'action' => 'add'));
	
	
	
	Router::connect('/view_business/*', array('controller' => 'Locations', 'action' => 'view_business',"admin"=>true));
	Router::connect('/edit_business/*', array('controller' => 'Locations', 'action' => 'edit_business',"admin"=>true));
	Router::connect('/voucher_location/*', array('controller' => 'users', 'action' => 'voucher_location'));	
	Router::connect('/delete-location-images/*', array('controller' => 'Locations', 'action' => 'delete_location_images'));
	
	
	Router::connect('/total_app_downloads/*', array('controller' => 'VoucherStats', 'action' => 'total_app_downloads',"admin"=>true));
	Router::connect('/total_logins/*', array('controller' => 'VoucherStats', 'action' => 'total_logins',"admin"=>true));
	Router::connect('/total_registrations/*', array('controller' => 'VoucherStats', 'action' => 'total_registrations',"admin"=>true));
	Router::connect('/total_verifications/*', array('controller' => 'VoucherStats', 'action' => 'total_verifications',"admin"=>true));
	Router::connect('/total_reachconversions/*', array('controller' => 'VoucherStats', 'action' => 'total_reachconversions',"admin"=>true));
	
	
	
    Router::connect('/total_renewals/*', array('controller' => 'VoucherStats', 'action' => 'total_renewals',"admin"=>true));
	Router::connect('/total_merchant_logins/*', array('controller' => 'VoucherStats', 'action' => 'total_merchant_logins',"admin"=>true));
	Router::connect('/total_merchant_registrations/*', array('controller' => 'VoucherStats', 'action' => 'total_merchant_registrations',"admin"=>true));
	Router::connect('/total_merchant_paidsus/*', array('controller' => 'VoucherStats', 'action' => 'total_merchant_paidsus',"admin"=>true));
	Router::connect('/total_merchant_reachconversion/*', array('controller' => 'VoucherStats', 'action' => 'total_merchant_reachconversion',"admin"=>true));
	Router::connect('/total_merchant_drafted/*', array('controller' => 'VoucherStats', 'action' => 'total_merchant_drafted',"admin"=>true));
	//login with facebook
	Router::connect('/login_with_facebook/*', array('controller' => 'users', 'action' => 'loginwith',"Facebook"));
	// Admin Profile
	Router::connect('/dashboard/*', array('controller' => 'users', 'action' => 'dashboard',"admin"=>true));
	Router::connect('/view-insights/*', array('controller' => 'VoucherStats', 'action' => 'insights',"admin"=>true));
	Router::connect('/view-merchant-insights/*', array('controller' => 'VoucherStats', 'action' => 'merchant_insights',"admin"=>true));
	Router::connect('/manage-profile/*', array('controller' => 'users', 'action' => 'profile',"admin"=>true));
	Router::connect('/manage-password/*', array('controller' => 'users', 'action' => 'changepassword',"admin"=>true));
	Router::connect('/setting/*', array('controller' => 'plans', 'action' => 'index',"admin"=>true));
	Router::connect('/edit-plan/*', array('controller' => 'plans', 'action' => 'edit',"admin"=>true));
	//Users Students
	Router::connect('/manage-students/*', array('controller' => 'users', 'action' => 'index',3,"admin"=>true));
	Router::connect('/edit-students/*', array('controller' => 'users', 'action' => 'edit',"admin"=>true));
	Router::connect('/view-students/*', array('controller' => 'users', 'action' => 'view',"admin"=>true));
	//Users Merchants
	Router::connect('/manage-merchants/*', array('controller' => 'users', 'action' => 'index',2,"admin"=>true));
	Router::connect('/edit-merchants/*', array('controller' => 'users', 'action' => 'edit',"admin"=>true));
	Router::connect('/view-merchants/*', array('controller' => 'users', 'action' => 'view',"admin"=>true));
	Router::connect('/delete-user/*', array('controller' => 'users', 'action' => 'delete',"admin"=>true));
	//Users Manage Vouchers
	Router::connect('/edit-voucher-detail/*', array('controller' => 'Vouchers', 'action' => 'edit',"admin"=>true));
	Router::connect('/view-vouchers/*', array('controller' => 'Vouchers', 'action' => 'index',"admin"=>true));
	Router::connect('/view-sales/*', array('controller' => 'Billings', 'action' => 'index',"admin"=>true));
	// Category
	Router::connect('/add-category/*', array('controller' => 'Categories', 'action' => 'add',"admin"=>true));
	Router::connect('/edit-category/*', array('controller' => 'Categories', 'action' => 'edit',"admin"=>true));
	Router::connect('/manage-categories/*', array('controller' => 'Categories', 'action' => 'index',"admin"=>true));
	// University
	Router::connect('/add-university/*', array('controller' => 'Universities', 'action' => 'add',"admin"=>true));
	Router::connect('/manage-universities/*', array('controller' => 'Universities', 'action' => 'index',"admin"=>true));
	Router::connect('/edit-university/*', array('controller' => 'Universities', 'action' => 'edit',"admin"=>true));
	Router::connect('/delete-university/*', array('controller' => 'Universities', 'action' => 'delete',"admin"=>true));	
	Router::connect('/manage-domain-extensions/*', array('controller' => 'UniversityDomains', 'action' => 'index',"admin"=>true));
	Router::connect('/add-domain-extension/*', array('controller' => 'UniversityDomains', 'action' => 'add',"admin"=>true));	
	Router::connect('/edit-domain-extension/*', array('controller' => 'UniversityDomains', 'action' => 'edit',"admin"=>true));
	//Router::connect('/delete-university/*', array('controller' => 'UniversityDomains', 'action' => 'delete',"admin"=>true));	

	
	// Location
	Router::connect('/add-location/*', array('controller' => 'Locations', 'action' => 'add',"admin"=>true));
	Router::connect('/manage-locations/*', array('controller' => 'Locations', 'action' => 'index',"admin"=>true));
	Router::connect('/edit-location/*', array('controller' => 'Locations', 'action' => 'edit',"admin"=>true));
	Router::connect('/delete-location/*', array('controller' => 'Locations', 'action' => 'delete',"admin"=>true));


	//Default Values 
	Router::connect('/manage-defaultvalues/*', array('controller' => 'DefaultValues', 'action' => 'edit',"admin"=>true));
	Router::connect('/CmsPages/*', array('controller' => 'CmsPages', 'action' => 'index',"admin"=>true));
	Router::connect('/ad-new-cmspage/*', array('controller' => 'CmsPages', 'action' => 'add',"admin"=>true));
	Router::connect('/edit-cmspage/*', array('controller' => 'CmsPages', 'action' => 'edit',"admin"=>true));
	Router::connect('/email-templates/*', array('controller' => 'EmailTemplates', 'action' => 'index',"admin"=>true));
	Router::connect('/edit-email-template/*', array('controller' => 'EmailTemplates', 'action' => 'edit',"admin"=>true));
	
	/* Merchant    */
	Router::connect('/merchant/*', array('controller' => 'users', 'action' => 'merchant'));
	Router::connect('/change-password/*', array('controller' => 'users', 'action' => 'changepassword'));
	Router::connect('/manage-billing/*', array('controller' => 'Plans', 'action' => 'index'));
	Router::connect('/buy-plan/*', array('controller' => 'Plans', 'action' => 'buyplan'));
	Router::connect('/create-profile/*', array('controller' => 'Plans', 'action' => 'createprofile'));
	
	
	Router::connect('/create-voucher/*', array('controller' => 'vouchers', 'action' => 'add'));
	Router::connect('/list-vouchers/*', array('controller' => 'vouchers', 'action' => 'index'));
	Router::connect('/edit-voucher/*', array('controller' => 'vouchers', 'action' => 'edit'));
	Router::connect('/delete-voucher/*', array('controller' => 'vouchers', 'action' => 'delete'));
	//Single Voucher save to publish
	Router::connect('/publish-voucher/*', array('controller' => 'vouchers', 'action' => 'publish-voucher'));	
	Router::connect('/copy-voucher/*', array('controller' => 'vouchers', 'action' => 'copy_voucher'));	
	Router::connect('/published-vouchers/*', array('controller' => 'vouchers', 'action' => 'index',1));
	Router::connect('/drafted-vouchers/*', array('controller' => 'vouchers', 'action' => 'index',0));
	Router::connect('/view-voucher/*', array('controller' => 'vouchers', 'action' => 'view'));
	Router::connect('/perview-voucher/*', array('controller' => 'vouchers', 'action' => 'perview'));	
	
	Router::connect('/my-account/*', array('controller' => 'users', 'action' => 'edit'));
	Router::connect('/insights/*', array('controller' => 'VoucherStats', 'action' => 'index'));
	Router::connect('/insights/reach', array('controller' => 'VoucherStats', 'action' => 'index'));
	Router::connect('/insights/impressions', array('controller' => 'VoucherStats', 'action' => 'index'));
	Router::connect('/insights/conversions', array('controller' => 'VoucherStats', 'action' => 'index'));
	
	
	

	//Router::connect('/add-profile/*', array('controller' => 'users', 'action' => 'profile'));
	
/**
 * ...and connect the rest of 'Pages' controller's URLs.
 */
	Router::connect('/pages/*', array('controller' => 'pages', 'action' => 'display'));

/**
 * Load all plugin routes. See the CakePlugin documentation on
 * how to customize the loading of plugin routes.
 */
	CakePlugin::routes();

/**
 * Load the CakePHP default routes. Only remove this if you do not want to use
 * the built-in default routes.
 */
	require CAKE . 'Config' . DS . 'routes.php';
