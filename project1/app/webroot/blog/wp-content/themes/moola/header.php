<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php endif; ?>
	
	<?php wp_head(); ?>
	 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>

<body <?php body_class(); ?>>
<?php 

//~ $_SESSION['testsessioncake'] = "Hellow World!";

//~ 
    //~ echo $this->Session->read('testsessioncake'); 
//~ 
//~ 
if(isset($_SESSION["User"]))
{
	$user=$_SESSION["User"];	 
	 if(isset($user['User'])){//pr($user);
			if(isset($user['User']['UserDetail']['business']))
			 $name= $user['User']['UserDetail']['business']	;
			 else
			  $name= $user['UserDetail']['business']	;	 
			 if($name==""){$name="Your Merchant Name";}
			 
			 $email= $user['User']['username'];
			 
			 if($email==""){$email="";}
			 $fname= explode(" ",$name);
			  //if(empty($user['User']['UserDetail']['image']) || !file_exists(WWW_ROOT."/img/profile/".$user['User']['UserDetail']['image'])){				 							 
			 if(empty($user['User']['UserDetail']['image'])){
				 $imageprofile="default_new.png";}
			 else{$imageprofile= "profile/".$user['User']['UserDetail']['image'];}
			 if(isset($user['User']['identifier']) &&  !empty($user['User']['identifier']!="")){ 
				$logout='<a href="javascript:void(0);" onclick="fblogout()" class="round-btn btn-blue">Logout</a>';
			  }
			 else{
				 $logout='<a href="'.SITE_LINK.'logout" class="round-btn btn-blue">Logout</a>';
			  }
		}		
}

?>
<div class="main"> 
  <!-- Preloader -->
  <div class="preLoader"></div>
  <?php if(isset($_SESSION["User"])){?>
    <header class="header inner-page-header">
	<nav class="navbar navbar-theme navbar-fixed-top">
	  <div class="container">
		<div class="navbar-header">
		  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		  </button>
		 <a class="navbar-brand brand-logo" href="<?php echo SITE_LINK; ?>"><img src="<?php echo SITE_LINK ?>/img/logo.png" alt="Moola Rewards" ></a>
		</div>
		<div id="navbar" class="navbar-collapse collapse header-menu"> 
		  <ul class="nav navbar-nav main-menu">
			<li><a href="<?php echo SITE_LINK; ?>">Home</a></li>
			<li><a href="<?php echo SITE_LINK; ?>about">About</a></li>
			<li><a href="<?php echo SITE_LINK; ?>pricing">Pricing</a></li>
			<li><a href="<?php echo SITE_LINK; ?>advertiser_faqs">faq</a></li>
			<li class="active"><a href="<?php echo SITE_LINK; ?>blog/">Blog</a></li>
		  </ul>
		  <ul class="nav-top-menu list-inline header-top">
				<li><a class="round-btn" href="<?php echo SITE_LINK.'insights'?>">INSIGHTS</a></li>				
				<li class="dropdown user_prof">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
						<span class="user_img"><img src="<?php echo SITE_LINK  ?>img/<?php echo $imageprofile; ?>"></span>
						<span><?php echo $fname[0];?></span>
						<b class="fa fa-angle-down"></b>
					</a>
					<ul class="dropdown-menu">
						<li>
							<div class="navbar-content">
								<div class="prof-inr">									
								   <span class="user_img"><img src="<?php echo SITE_LINK ?>img/<?php echo $imageprofile; ?>"></span> 
								   <div class="usrprof-desc">
									  <h4><?php echo $name;?></h4>
									  <p><?php echo $email;?></p>										
								</div>
								<ul class="profile-menu">																		
									<li><a href="<?php echo SITE_LINK; ?>my-account" class="view">My Account</a></li>
									<li><a href="javascript:void(0)" class="view">Billing</a></li>
									<li><a href="javascript:void(0)" class="view">Recommend to a Friend</a></li>                                        
									<li class="logout"><?php echo $logout ?></li>
								</ul>
								</div>
							</div>
						</li>
					</ul>
				</li>
				
				<?php $count_cart=count($_SESSION["cart_items"]);?>
				<li class="dropdown cart-section">
					<a href="javascript:void(0)" class="dropdown-toggle show_loc_list" data-toggle="dropdown" aria-expanded="true">
						<span><img src="<?php echo SITE_LINK ?>/img/cart.png"><?php (!empty($_Session["cart_total_items"])? $class="display:inline" : $class="display:none" )?> <div class="cart_total_items" style="<?php echo $class;?>"><?php echo $_Session["cart_total_items"]; ?></div> <div class="cart_total_items"><?php echo $count_cart ?></div></span>
					</a>				   
					
				
				<ul class="dropdown-menu">
						 <?php $count_cart=count($_SESSION["cart_items"]);if($count_cart==0){ ?>
								<div class="alert alert-warning">No Item in cart</div>	
						<?php } else {?>	 
						<li>
							<div class="cart-top">
								<div class="price-is">Each location price is</div>
								<div class="cart-price">£ 9.99</div>
							</div>
							<h4>Location List :</h4>
							
							<ul class="cart-items">
								<?php $totalamount = $total_L =0; 
								foreach($_SESSION["cart_items"] as $key=>$loclist) {
									if($loclist['is_view'] == 1) {
										if(empty($loclist['image']) || !file_exists(WWW_ROOT."/img/profile/".$loclist['image']))										{
											$locimg="default_new.png";										
										}else{$locimg="profile/".$loclist['image'];}
										$totalamount += 9.99;
										$total_L += 1;										
									?>									
									<li id="cart<?php echo $key; ?>"><a href="javascript:void(0)" title="<?php echo $loclist["title"]?>"><img src="<?php echo SITE_LINK."img/".$locimg ;?>"><?php echo $loclist["address"]?></a></li>
									<?php } else { 
										if(empty($loclist['image']) || !file_exists(WWW_ROOT."/img/profile/".$loclist['image']))
										{$locimg="default_new.png";}else{$locimg="profile/".$loclist['image'];}
										//$totalamount += 9.99;
										//$total_L += 1;
									?>
									<li id="cart<?php echo $key; ?>" style="display:none;"><a href="javascript:void(0)" title="<?php echo $loclist["title"]?>"><img src="<?php echo SITE_LINK."img/".$locimg?>"><?php echo $loclist["address"]?></a></li>
									<?php } ?>
								<?php } ?>
							</ul>							
							<div class="cart-bottom">
								<h2 class="cart_tval"><?php echo $total_L ?>  items in Basket   £ <?php echo number_format(($totalamount+(($totalamount)*20/100)),2); ?></h2>
								<a href="<?php echo SITE_LINK."my_cart" ?>" class="btn" title="Checkout">Checkout</a>
								
							</div>
						</li>
						 <?php }?>
					</ul>  
				
				
		</ul>			
		</div><!--/.nav-collapse -->
	  </div>
	</nav>
</header>
    
    <?php } else {?>
		<div class="mob-menu">
       <ul class="nav-top-menu list-inline header-top">
          <li><a class="round-btn" href="<?php echo SITE_LINK."advertise-with-moola"; ?>">ADVERTISE</a></li>
       </ul>
	   </div>

		<header class="header">
			<nav class="navbar navbar-theme navbar-theme">
			  <div class="container">
				<div class="navbar-header">
				  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				  </button>
				  <a class="navbar-brand brand-logo" href="<?php echo SITE_LINK; ?>"><img src="<?php echo SITE_LINK ?>/img/logo.png" alt="Moola Rewards"></a>
				</div>
				<div id="navbar" class="navbar-collapse collapse header-menu"> 
				  <ul class="nav navbar-nav main-menu">
					<li ><a href="<?php echo SITE_LINK; ?>">Home</a></li>
					<li ><a href="<?php echo SITE_LINK; ?>about">About</a></li>
					<li class="active"><a href="<?php echo SITE_LINK; ?>blog/">Blog</a></li>
					<!-- <li><a href="#about">Pricing</a></li>
					<li><a href="#">faq</a></li> -->
				  </ul>
				  <ul class="nav-top-menu list-inline">
					  <li><a class="round-btn" href="<?php echo SITE_LINK."advertise-with-moola"; ?>">ADVERTISE</a></li>
				  </ul>
				</div><!--/.nav-collapse -->
			  </div>
			</nav>
		</header>
		
		<?php } ?>
	<section class="inner-banner topheader">
       <div class="container">
	        <div class="banner-cont"> 
            	<h2>Blog</h2>
                <h5></h5>
	        </div>
       </div>
    </section>
		<div id="content" class="site-content">
		
