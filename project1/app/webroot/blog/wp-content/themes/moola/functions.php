<?php
add_action('init', 'boaCakePHPSession', 1);
define("SITE_LINK","https://www.moolarewards.co.uk/");
define("SITE_CHILD_LINK",get_stylesheet_directory_uri());
define('ROOTPATH', dirname(__FILE__));
function boaCakePHPSession() {
    if(!session_id()) {
        session_name("CAKEPHP");
        session_start();
    }
}

function my_theme_enqueue_styles() {

    $parent_style = 'twentysixteen-style'; // This is 'twentyfifteen-style' for the Twenty Fifteen theme.

    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array( $parent_style ),
        wp_get_theme()->get('Version')
    );
    // used for popup it is alredy inculded in footer   //  wp_enqueue_script( 'script',  get_stylesheet_directory_uri()  . '/js/bootstrap.min.js', array ( 'jquery' ), 1.1, true);
   wp_enqueue_style( 'bootstrap',  get_stylesheet_directory_uri()  . '/css/bootstrap.min.css',false,'1.1','all');
   //wp_enqueue_style( 'font',  get_stylesheet_directory_uri() . '/css/fontawesome.min.css',false,'1.1','all');
   wp_enqueue_style( 'style',  get_stylesheet_directory_uri() . '/css/style.css',false,'1.1','all');
   wp_enqueue_style( 'fontstyle',  get_stylesheet_directory_uri() . '/css/responsive.css',false,'1.1','all');
   wp_enqueue_style( 'font',  get_stylesheet_directory_uri()  . '/css/font-awesome.min.css',false,'1.1','all');
   wp_enqueue_style( 'style1',  get_stylesheet_directory_uri() . '/css/internal.css',false,'1.1','all'); 
   wp_enqueue_style( 'style1',  get_stylesheet_directory_uri() . '/css/owl.carousel.css',false,'1.1','all'); 
}
add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );
function my_theme_scripts_function() {

	wp_enqueue_script( 'myscript', get_stylesheet_directory_uri() . '/js/jquery-2-2-4.min.js');
	wp_enqueue_script( 'myscript2', get_stylesheet_directory_uri() . '/js/bootstrap.min.js');
	
	wp_enqueue_script( 'myscript1', get_stylesheet_directory_uri() . '/js/custom.js');
	//wp_enqueue_script( 'myscript3', get_stylesheet_directory_uri() . 'js/owl.carousel.js');
	wp_enqueue_script( 'myscript4', get_stylesheet_directory_uri() . '/js/moment.js');
	wp_enqueue_script( 'myscript5', get_stylesheet_directory_uri() . '/js/popper.min.js');
	
}
add_action('wp_enqueue_scripts','my_theme_scripts_function');
function new_excerpt_length($length) {
return 100;
}
add_filter('excerpt_length', 'new_excerpt_length');



//End Home footer Content 


//Site Map file sitemap.xml at root


add_action("publish_post", "eg_create_sitemap");
add_action("publish_page", "eg_create_sitemap");
function eg_create_sitemap() {
$postsForSitemap = get_posts(array(
'numberposts' => -1,
'orderby' => 'modified',
'post_type' => array('post','page'),
'order' => 'DESC'
));
$sitemap = '<?xml version="1.0" encoding="UTF-8"?>';
$sitemap .= '<?xml-stylesheet type="text/xsl" href="sitemap-style.xsl"?>';
$sitemap .= '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
foreach($postsForSitemap as $post) {
setup_postdata($post);
$postdate = explode(" ", $post->post_modified);
$sitemap .= '<url>'.
'<loc>'. get_permalink($post->ID) .'</loc>'.
'<priority>1</priority>'.
'<lastmod>'. $postdate[0] .'</lastmod>'.
'<changefreq>daily</changefreq>'.
'</url>';
}
$sitemap .= '</urlset>';
$fp = fopen(ABSPATH . "sitemap.xml", 'w');
fwrite($fp, $sitemap);
fclose($fp);
}
//End Site Map file sitemap.xml at root
?>
