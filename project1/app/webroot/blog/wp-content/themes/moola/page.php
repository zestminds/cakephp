<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>
<?php  $page_title = $wp_query->queried_object->post_name;?>
	 
<div id="content" class="site-content">
	<div class="container-fluid">
	<div class="row">
	<div class="col-md-12">
		<div class="col-md-8">
<div id="primary" class="content-area">
	
	<?php 
	   $page_title = $wp_query->queried_object->post_name;
	  if($page_title=="products" || $page_title=="about" || $page_title=="services" ||$page_title=="testimonial" || $page_title=="contact")
	  { 
		  echo '<main id="main" class="site-main" role="main">';}
		  else{?>
			<main id="main" class="site-main-post" role="main">
		<?php 
	}
		// Start the loop.
		while ( have_posts() ) : the_post();

			// Include the page content template.
			get_template_part( 'template-parts/content', 'page' );

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) {
				comments_template();
			}

			// End of the loop.
		endwhile;
		?>

	</main><!-- .site-main -->

	<?php get_sidebar( 'content-bottom' ); ?>

</div><!-- .content-area -->

<div class="col-md-4">
<?php get_sidebar(); ?>
</div>
</div>
</div>
</div>
	</div>
<?php get_footer(); ?>
