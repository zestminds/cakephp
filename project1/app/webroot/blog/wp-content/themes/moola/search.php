<?php
/**
 * The template for displaying search results pages
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>
<div id="content" class="site-content">
	<div class="container-fluid">
	<div class="row">
	<div class="col-md-12">
		<div class="col-md-8">
	<section id="primary" class="content-area search_blog" >
		<main id="main" class="site-main-search" role="main">

		<?php if ( have_posts() ) : ?>

			<header class="page-header">
				<h2 class="entry-title "><span><?php printf( __( 'Search Results for: %s', 'twentysixteen' ),  esc_html( get_search_query() )); ?></span></h2>
			</header><!-- .page-header -->

			<?php
			// Start the loop.
			while ( have_posts() ) : the_post();

				/**
				 * Run the loop for the search to output the results.
				 * If you want to overload this in a child theme then include a file
				 * called content-search.php and that will be used instead.
				 */
				get_template_part( 'template-parts/content', 'search' );

			// End the loop.
			endwhile;
            ?>
            <div style="clear:both"></div>
            <?php
			// Previous/next page navigation.
			the_posts_pagination( array(
				'prev_text'          => __( 'Previous page', 'twentysixteen' ),
				'next_text'          => __( 'Next page', 'twentysixteen' ),
				'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'twentysixteen' ) . ' </span>',
			) );

		// If no content, include the "No posts found" template.
		else :
			get_template_part( 'template-parts/content', 'none' );

		endif;
		?>

		</main><!-- .site-main -->
	</section><!-- .content-area -->

</div><!-- .content-area -->
<div class="col-md-4">
<?php get_sidebar(); ?>
</div>
</div>
</div>
</div>
	</div>
<?php get_footer(); ?>
