<?php
/**
 * The template part for displaying single posts
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>
<div class="post-contents">
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<header class="entry-header">
		<?php twentysixteen_post_thumbnail(); ?>
		
	</header><!-- .entry-header -->
<div class="post_contents_single">
	<h2 class="entry-title posttitle">
		<span><?php the_title( ); ?></span>
		</h2>
		<div class="time">
		<span><i class="fa fa-calendar" aria-hidden="true"></i> </span>
		&nbsp;<?php echo $post_date = get_the_date( 'F j,  Y' );?>
		</div>
	<?php //twentysixteen_excerpt(); ?>

	

	<div style="clear:both;margin-bottom:5px;"></div>
	<div class="post-content">
		<?php
			the_content();

			wp_link_pages( array(
				'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'twentysixteen' ) . '</span>',
				'after'       => '</div>',
				'link_before' => '<span>',
				'link_after'  => '</span>',
				'pagelink'    => '<span class="screen-reader-text">' . __( 'Page', 'twentysixteen' ) . ' </span>%',
				'separator'   => '<span class="screen-reader-text">, </span>',
			) );

			if ( '' !== get_the_author_meta( 'description' ) ) {
				get_template_part( 'template-parts/biography' );
			}
		?>
	</div><!-- .entry-content -->
	<div style="clear:both;margin:9px;"></div>
	<?php 
	$current_url =  get_site_url();  
	$post_cats = count(get_the_category());?>
	<div class="fa fa-folder-open iconorg"></div><div class="meta_cats"><ul>
	<?php $i=0; foreach((get_the_category()) as $category) { 
		if ( $i < $post_cats-1 ) {
			$quote = ", ";					
		}  
		else{	$quote = "";	} 
		 $i++;
		 
    echo "<li><a href='".$current_url."/category/".$category->cat_name."'>".$category->cat_name;echo $quote.'</a></li>'; 
	}
	?>	
	</ul></div>
<?php $YPE_slugs_names = array();  $YPE_tags = get_the_tags();/* $post_tags = count(get_the_tags()); */ if($YPE_tags){?>
<div style="clear:both;margin:9px;"></div>
   <div class="fa fa-tag iconorg"></div> <div class="meta_tags"><ul>
	<?php $i=0; foreach((get_the_tags()) as $tags) { 
		//if ( $i < $post_tags-1 ) {
		//	$quote = ", ";					
		//}  
		//else{	$quote = "";	} 
		// $i++;
    echo "<li><a href=''>".$tags->name."</a></li>  "; 
	}
	?>  
	</ul></div><?php }?>
	<div><?php echo do_shortcode(' [addthis tool="addthis_inline_share_toolbox_jc45"] ');?></div>
	<footer class="entry-footer">
	
		<?php
			edit_post_link(
				sprintf(
					/* translators: %s: Name of current post */
					__( 'Edit<span class="screen-reader-text"> "%s"</span>', 'twentysixteen' ),
					get_the_title()
				),
				'<span class="edit-link">',
				'</span>'
			);
		?>
	</footer><!-- .entry-footer -->
	</div>
</article><!-- #post-## -->
</div>
