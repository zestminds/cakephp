
<?php /* Template Name: Page Template */ ?>

<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>
<div id="primary" class="content-area">
		<main id="main" class="site-main-home" role="main">
			<section class="inner-banner">
				<div class="container-fluid">
				<div class="row">					
					<div class="inner-banner-content">
						<h2><?php  echo $page_title =get_the_title() ?></h2>
					</div>
					
				</div>
				</div>
			</section>
  
			<section class="product-page">
						<div class="container">
							<div class="row">	
								<div class="col-md-12">
									<div class="about-content">
									<?php 
										while ( have_posts() ) : the_post(); ?> <!--Because the_content() works only inside a WP Loop -->
											<div class="entry-content-page">
												<?php the_content(); ?> <!-- Page Content -->
											</div><!-- .entry-content-page -->

										<?php
										endwhile; //resetting the page loop
										wp_reset_query(); //resetting the page query
									
										?>
										</div>
									</div>
								</div>
						</div>
			   </section>  
		</main><!-- .site-main -->
</div><!-- .primary-area -->




<?php get_footer(); ?>
