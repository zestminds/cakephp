<?php
/**
 * The template part for displaying content
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>
	<div class="post-contents">
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php if ( is_sticky() && is_home() && ! is_paged() ) : ?>
			<span class="sticky-post"><?php _e( 'Featured', 'twentysixteen' ); ?></span>
		<?php endif; ?>

		
	</header><!-- .entry-header -->

	
	<div><?php twentysixteen_post_thumbnail('medium'); ?></div>
	<?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
	<div style="margin-top:12px;">	
	
	<div class="post-content">
		<div class="post-date"><p><span><i class="fa fa-calendar" aria-hidden="true"></i></span><?php echo $post_date = get_the_date( ' M j, Y' );?></p></div>
		<div style="clear:both;margin:9px;"></div>
		
		<?php twentysixteen_excerpt(); 
			/* translators: %s: Name of current post */
			//~ the_content( sprintf(
				//~ __( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'twentysixteen' ),
				//~ get_the_title()
			//~ ) );

			wp_link_pages( array(
				'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'twentysixteen' ) . '</span>',
				'after'       => '</div>',
				'link_before' => '<span>',
				'link_after'  => '</span>',
				'pagelink'    => '<span class="screen-reader-text">' . __( 'Page', 'twentysixteen' ) . ' </span>%',
				'separator'   => '<span class="screen-reader-text">, </span>',
			) );
		?>
		<div class="more-link"><?php echo  '<a href="'.esc_url( get_permalink() ).'" rel="bookmark">Read More</a>' ;	?></div>
		
	</div>
	
	<div style="clear:both;margin:9px;"></div>

<!--
<div class="post-date">By&nbsp;</div><div class="post-author"><?php //$post_author = get_the_author(); echo $post_author;?></div>
-->


	
	<!-- .entry-content -->
</div></div>
	<footer class="entry-footer">
		
		<?php
			edit_post_link(
				sprintf(
					/* translators: %s: Name of current post */
					__( 'Edit<span class="screen-reader-text"> "%s"</span>', 'twentysixteen' ),
					get_the_title()
				),
				'<span class="edit-link">',
				'</span>'
			);
		?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
