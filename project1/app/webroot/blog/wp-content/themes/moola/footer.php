<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>

		</div><!-- .site-content -->
</div>
		<footer class="footer">
			<div class="footer-top">
				 <div class="container">
					 <div class="footer-widget">
						 <div class="col-9 ftr-left">
							 <div class="ftr-social">
								<a href="<?php echo SITE_LINK ?>" class="ftr-logo">
									<img src="<?php echo SITE_LINK ?>img/logo.png">
								</a>
								<div class="social-links">
									<h5>Follow us on</h5>
									<ul class="social">
									   <li><a href="https://www.facebook.com/moola.rewards/" target="_blank"><i class="fa fa-facebook-f"></i></a></li>
									   <li><a href="https://twitter.com/MoolaRewards" target="_blank"><i class="fa fa-twitter"></i></a></li>
									   <li><a href="https://www.youtube.com/channel/UCABfi1EObH7mdgT6Z3Dynug" target="_blank"><i class="fa fa-youtube"></i></a></li>
									   <li><a href="https://www.instagram.com/moola.rewards/" target="_blank"><i class="fa fa-instagram"></i></a></li>

									</ul>
								</div>
							 </div>
							 <div class="basic-links">
								 <h4>Important Links</h4>
								 <div class="infoLink">
									 <ul>
										 <li><a href="<?php echo SITE_LINK?>advertise-with-moola">Advertise</a></li>
										 <li><a href="<?php echo SITE_LINK?>about">About </a></li>
										 <li><a href="<?php echo SITE_LINK?>contact_us">Contact Us</a></li>
										
										 
									 </ul>
									 <ul>															 
										<li><a href="<?php echo SITE_LINK?>help">Help</a></li>
										<li><a href="<?php echo SITE_LINK?>student_support_faqs">Support FAQs</a></li>
										<li><a href="<?php echo SITE_LINK?>terms_and_conditions">Term & Conditions</a></li>
										
									 </ul>
									 <ul>								 
										 <li><a href="<?php echo SITE_LINK?>privacy_policy">Privacy Policy</a></li>
										 <li><a href="<?php echo SITE_LINK?>cookie_policy">Cookie Policy</a></li>	
									 </ul>
								 </div>
								 <div class="copyright">
									 <p>Copyright © 2018 Moola ® All Rights Reserved.</p>
								 </div>
							 </div>
						 </div>
						 <div class="col-3 ftr-right">
							 <h4>DOWNLOAD our APP</h4>
							 <div class="dwnload-apps application">
								<ul class="list-inline">
									<li><a href="https://play.google.com/store/apps/details?id=com.androidzestminds.moola" target="_blank" > <span class="fa fa-android st-appiocn"></span> <p> available on <span> Google Store </span></p></a></li>
									<li><a href="https://itunes.apple.com/in/app/moolarewards/id1434308130?mt=8" target="_blank" > <span class="fa fa-apple st-appiocn"></span>  <p>available on <span> Apple Store </span></p></a></li>
								</ul>
							 </div>
						 </div>
					 </div>
				 </div>
			</div>
		</footer><!-- .site-footer -->
	</div><!-- .site-inner -->
</div><!-- .site -->
   
    
      
    </div>
  </div>
  <!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-131170044-1"></script>
	<script>
	window.dataLayer = window.dataLayer || [];
	function gtag(){dataLayer.push(arguments);}
	gtag('js', new Date());

	gtag('config', 'UA-131170044-1');
	</script>
   
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo get_stylesheet_directory_uri(); ?>/js/bootstrap.min.js"></script>
	
<?php wp_footer(); ?>
</body>
</html>
