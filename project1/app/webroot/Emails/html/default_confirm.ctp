<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>::index::</title>
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<!-- css -->
<!--[if IE]>
        <script src="js/html5shiv.js"></script>
    <![endif]-->
<style type="text/css">
@import url('https://fonts.googleapis.com/css?family=Open+Sans:400,600,700');
body {
	font-family: 'Open Sans', sans-serif;
	margin: 0;
	padding: 0;
	background-color: #f6f6f6;
	-ms-text-size-adjust: 100%;
	-webkit-text-size-adjust: 100%;
}
* {
	-moz-box-sizing: border-box;
	-webkit-box-sizing: border-box;
	box-sizing: border-box;
}
img {
	max-width: 100%;
}
table {
	border-spacing: 0;
}
table td {
	border-collapse: collapse;
}
.table-main {
	max-width: 1080px;
	margin: 0 auto;
}
 @media screen and (max-width: 1024px) {
.site-links {
	font-size: 30px !important;
}
.headig-one, .payment-text {
	font-size: 30px !important;
}
.btn-click {
	font-size: 30px !important;
	max-width: 584px !important;
}
}
 @media screen and (max-width: 767px) {
.headig-one, .payment-text, .paddign-50 {
	font-size: 15px !important;
	padding: 5px 15px !important;
}
.space {
	height: 20px !important;
}
.btn-click {
	font-size: 14px !important;
	padding: 10px !important;
	max-width: 284px !important;
	top: -19px !important;
}
.site-links {
	font-size: 14px !important;
}
.heigh-50 {
	height: 20px !important;
}
.space.large-height {
	height: 34px !important;
}
.logo img {
	max-width: 110px;
}
}
</style>
</head>
<body>
<div class="table-main">
  <table cellpadding="0" cellspacing="0" border="0" width="100%" style="width:100%; color:#fff; font-family: 'Open Sans', sans-serif;">
    <tr>
      <td class="space" style="height:85px; background-color:#000;"></td>
    </tr>
    <tr>
      <td class="logo" style="text-align:center; padding:0px 15px; background-color:#000;"><img src="<?php echo "https://www.moolarewards.co.uk/"; ?>img/logo.png" alt="<?php echo "Moolavoucher"; ?>" title="<?php echo "VoolaVoucher"; ?>" border="0" style="margin:10px;"></td>
    </tr>
    <tr>
      <td class="space " style="height:85px; background-color:#000;"></td>
    </tr>
    <tr>
      <td class="headig-one paddign-50 name" style="background:#000; font-size:48px; font-weight:600; padding:5px 50px;">Dear Tom Atkinson,</td>
    </tr>
    <tr>
      <td class="headig-one paddign-50" style="background:#000; font-size:40px; font-weight:600; padding:5px 50px;">We have received a request from you to signup.</td>
    </tr>
    <tr>
      <td style="height:20px; background-color:#000;"></td>
    </tr>
    <tr>
      <td class="payment-text paddign-50" style="font-size:38px; padding:0px 50px; background:#000;"> Your Payment has been confirmed. Please find your attached time-stamped copy of  the social contract.</td>
    </tr>
    <tr>
      <td class="space large-height " style="height:170px; background-color:#000;"></td>
    </tr>
    <tr>
      <td style="text-align:center; position:relative;" ><a href="javascript:void(0)" class="btn-click" style=" background-color:#f89529; display:inline-block; font-size:40px; font-weight:bold; color:#fff; text-decoration:none; position:relative; padding:38px 31px; outline:none !important; -moz-border-radius:80px; -webkit-border-radius:80px; border-radius:80px; position:absolute; left:0px; right:0px; margin:0 auto; text-align:center; max-width:760px; top:-72px;">Click here to confirm your account </a></td>
    </tr>
    <tr>
      <td class="space large-height " style="height:140px;"></td>
    </tr>
    <tr>
      <td class="headig-one paddign-50" style="color:#7c7c7c; font-size:40px;  padding:5px 50px;">If you face any problem in clicking link, please
        copy and paste below</td>
    </tr>
    <tr>
      <td class="heigh-50" style="height:50px;"></td>
    </tr>
    <tr>
      <td  class="paddign-50" style="padding:0px 50px; "><a href="javascript:void(0)" class="site-links" style="font-size:44px; color:#1998c7; text-decoration:underline;">htpps://www.moolarewards.co.uk/
        confirmlink/1c468da4ad47chjsvchjcbgh
        87ghyu</a></td>
    </tr>
    <tr>
      <td class="space" style="height:85px;"></td>
    </tr>
    <tr>
      <td class="headig-one paddign-50" style="color:#7c7c7c; font-size:40px;  padding:5px 50px;">Best Regards,</td>
    </tr>
    <tr>
      <td class="headig-one paddign-50" style="color:#f89529; font-size:46px;  padding:5px 50px;">Best Regards,</td>
    </tr>
    <tr>
      <td class="space" style="height:85px;"></td>
    </tr>
  </table>
</div>
</body>
</html>
