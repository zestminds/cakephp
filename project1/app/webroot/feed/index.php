<!DOCTYPE html>
<html lang="en">
<head>
  <title>Wowcher Feed</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
</head>
<body>
<?php
ini_set("max_execution_time",0);

$filename = 'test.xml';
$time_diff = 4000;
if (file_exists($filename)) {
	$s = filemtime($filename);
	$e = strtotime(date("M-d-y H:i:s"));
	$time_diff = $e-$s;
}
//$time_diff = 4000;
if ($time_diff >= 3600) {
	unlink("test.xml");
	$feed = file_get_contents('https://public-api.wowcher.co.uk/v1/feed?brand=wowcher&countryCode=gb');
	file_put_contents("test.xml", $feed, FILE_APPEND|LOCK_EX);
}

$doc = new DOMDocument();
      $doc->preserveWhiteSpace = false;
      $doc->load('test.xml');
      $i=0;
	  $tmpCount = 0;
	  $cnt = 0;

$cat = "";
$url = "";
if(isset($_REQUEST['category']) && !empty($_REQUEST['category'])) {
	$cat = $_REQUEST['category'];
	$url = "?category=".$cat."&page=";
} else {
	$cat = "";
	$url = "?page=";
}
$page = $prevPage = 1;
if(isset($_REQUEST['page']) && !empty($_REQUEST['page'])) {
	$page = $_REQUEST['page'];
	$prevPage = $page-1;
	
} else {
	$page = 1;
}
//echo $page;
//echo $i;
?>

<div class="container" style="margin-top:50px;">

<form id="frm" name="search" type="get">
 <div class="form-group">
    <label for="cat">Search By Category:</label>
    
  
<input type="text" name="category" id="cat" class="form-control" value="<?php echo $cat; ?>" placeholder="enter category to search" />
</div>
<button type="submit" class="btn btn-default">Submit</button>
</form>
<br/>
<a href="<?php echo $url.$prevPage; ?>" class="btn btn-info">previous</a>

<br/>
<br/>
<?php
if ( empty($cat) ) {
	//die;
}
$allowedTags = array("name","desc","promotext","imgurl","spec","category");
	$flag = false;
	while(is_object($finance = $doc->getElementsByTagName("product")->item($i)))
	{
		// echo $i;
		// echo $page;
		// echo $tmpCount;
		// echo "<br/>";
		//if ($tmpCount == $page) {
		$str = "<ul class='list-group'>";
		$str1 = "";
	
		foreach($finance->childNodes as $nodename)
		{
			//echo $nodename->nodeName;
			//echo "<br/>";
			if (in_array(trim($nodename->nodeName),$allowedTags)) {
				//echo $nodename->nodeName; 
				//echo "<br/>";
				$str1 .="<li class='list-group-item'>";
				
				if($nodename->nodeName=='price')
				{
					$str1 .="<b>Prices</b>";
					$str1.= "<ul class='list-group'>";
					
					 foreach($nodename->childNodes as $subNodes)
					 {
						$str1 .= "<li class='list-group-item'>";
						$str1 .= "<b>".$subNodes->nodeName."</b> - ".$subNodes->nodeValue."<br>";
						$str1 .="</li>";
						
					 }
					$str1 .="</ul>";
					
				} elseif($nodename->nodeName=='imgurl')
				{
					//$str.= "<b>Image</b>";
					$tmpStr = "<li class='list-group-item'><img src='".$nodename->nodeValue."'><br/>";
					$tmpStr.= "<a href='".$nodename->nodeValue."' target='_blank'>View Image</a></li>";
					
				}elseif($nodename->nodeName=='spec')
				{
					$str1.= "<b>Address</b> - ";
					$str1.= $nodename->nodeValue;
					
				}elseif($nodename->nodeName=='category')
				{
					$str1.= "";
					
				}
				else
				{
					$str1 .= "<b>".$nodename->nodeName."</b> - ".$nodename->nodeValue."<br>";
				}
				$str1 .= "</li>";
				if ( $nodename->nodeName == "category" && (trim(strtolower($nodename->nodeValue)) == trim(strtolower($cat))) ) {
					//die("here");
					$flag = true;
					$tmpCount++;
				} 
			}
		}
		//echo $page;
		//echo $tmpCount;
		//die;
		$str .= $tmpStr.$str1."</ul>";
		
		if ( $flag && $tmpCount == $page ) {
			echo $str;
			//$flag = false;
			break;
			//echo "<hr/>";
		} elseif( empty($cat) ){
			if( ($i >= ($page-1)*3) && $cnt <= 3) {
			
			echo $str;
			$cnt++;
			//$flag = false;
			//break;
		} elseif($cnt >= 3) {
			break;
		}
		}
		//}
      $i++;
	}
?>
<br/>
<a href="<?php echo $url.($page+1); ?>" class="btn btn-info">next</a>
<br/>
<br/>
</div>