<?php
$img = $_REQUEST['img']; 
// Load the stamp and the photo to apply the watermark to
$info = getimagesize($img );
$extension = image_type_to_extension($info[2]);
$stamp =  imagecreatefrompng('./watermark_logo.png') or die("1");
if($extension==".jpeg")
$im = imagecreatefromjpeg($img) or die("2");
else
$im = imagecreatefrompng($img) or die("2");

// Set the margins for the stamp and get the height/width of the stamp image
$marge_right = 5;
$marge_bottom = 5;
$sx = imagesx($stamp);
$sy = imagesy($stamp);

// Copy the stamp image onto our photo using the margin offsets and the photo 
// width to calculate positioning of the stamp. 
imagecopy($im, $stamp, imagesx($im) - $sx - $marge_right, imagesy($im) - $sy - $marge_bottom, 0, 0, imagesx($stamp), imagesy($stamp));

// Output and free memory
header('Content-type: image/png');

imagepng($im);
imagedestroy($im);
?>
