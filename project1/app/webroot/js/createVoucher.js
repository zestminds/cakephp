function voucherStatus(status)
{
	if(status==="save")
	$("#VoucherVoucherStatus").val(0);
	else
	{
		$("#VoucherVoucherStatus").val(1);			
	}
}
$(".confirmlocationadd").on("click",function(){
	$("#VoucherVoucherStatus").val(3);
	confirmSubmit = true;
	$("#confirmaddvoucher").modal("hide");
	$('#VoucherAddForm').submit();
});
$(".confirmvouchernot").on("click",function(){
	$("#VoucherVoucherStatus").val(0);
	confirmSubmit = true;
	$("#confirmaddvoucher").modal("hide");
	$('#VoucherAddForm').submit();
});	  

function enable_text(status,value,textfield)
{	
	if(status=="D")
	{		
		$( "#"+value ).prop( "readonly", true ); 
		var input = $( "#"+textfield).val();
		$( "#"+value).val(input);
			
		if(value=="discountvalue")
		{	$('#description_statusC').prop('checked',false);	
			$("#descriptionvalue").prop( "readonly", true );			
			$('#description_statusD').prop('checked',true);
			$('#descriptionvalue').val($('#default_description').val());
			$('#olddiscount').val(input);
		}
		
	}
	else
	{		
		$( "#"+value).prop( "readonly", false ); 
		if(value=="discountvalue")
		{	$('#description_statusC').prop('checked',true);	
			$("#descriptionvalue").prop( "readonly", false );	
		}
	}	
}
	
$(document).ready(function () {	

	$('.allDay').click(function (event) {	
		var checkValue = $("input[name='data[Voucher][day_status]']:checked").val();	
		
		if(checkValue=='0'){	
			 $(".grp_date").hide();	
		} else {	
			 $(".grp_date").show();	
			 //$('#statusToggle').prop('checked', false).change();
		}
		
		if ($("#VouchDayStatus").val() == '' || $("#VouchDayStatus").val() == 0 ) {
				$("#VouchDayStatus").val("1");
			} else {
				$("#VouchDayStatus").val("0");
			}
	});
	
	
	$('#statusToggle').change(function() {
		
	  //$('#console-event').html('Toggle: ' + $(this).prop('checked'))
	  console.log( $(this).prop('checked'));
	  var checkValue = $("input[name='data[Voucher][day_status]']:checked").val();
	  if ($(this).prop('checked')) {
		$("#togglerepeat").show();
		$("#rd1,#rd2").hide();
		var tmp = $("#VoucherRurl").val();
		if (tmp != 'URL' ) {
		} else {
			$("#VoucherRurl").val("URL");
		}
	  } else {
		  

		$("#togglerepeat").hide();
		$("#rd1,#rd2").show();
		var tmp = $("#VoucherRurl").val().trim();
		
		if (tmp == 'URL' ) {
			
			$("#VoucherRurl").val('');
		} else {
			$("#VoucherRurl").val(tmp);
		}
	  }
	  if (checkValue == 0) {
	  } else {
		$('.allDay').trigger("click");
	  }
	});
	
function publishpayment()
{
	var rslt= false;
	var href =SITE_LINK+"checklocation";
		  $.ajax({
			type: "GET",   
			cache: false,   
			url: href,   
			success: function(data){
				data = JSON.parse(data);
				if(data.status){ 				
					confirmSubmit = true;
					$("#confirmaddvoucher").modal("hide");
					$('#VoucherAddForm').submit();
					rslt= true;			
				}
				else 
				{
					$("#confirmaddvoucher").modal("show");
					$("#VoucherVoucherStatus").val(0);
					rslt= false;
				}
			}  
		});	
		return rslt;
}	
var confirmSubmit = false;
var retflag = true;
$('#VoucherAddForm').validate({ // initialize the plugin
	 ignore: "input[type='text']:hidden",
	 rules: {			
		 
		  "data[Voucher][title_discount]": {
			required: true		
			//number: true,
			//maxlength: 2		
		  }/*,
		  "data[Voucher][sub_title]": {
			required: true				
		  }*/,
		  "data[Voucher][description]": {
			required: true				
		  },
		  "data[Voucher][terms]": {
			required: true				
		  },
		  "data[Voucher][start_date]": {
			required: true				
		  },
		  "data[Voucher][end_date]": {
			required: true,
			greaterThan: "data[Voucher][start_date]"				
		  },		  
		  "data[Voucher][start_hour]": {
			required: true				
		  },
		  "data[Voucher][end_hour]": {
			required: true				
		  },
		  "data[Voucher][tmpImage]": {
			required: true				
		  },
		  "data[Voucher][tags]": {
			required: true				
		  },
		  "data[Voucher][rurl]" :{
			  required: true
		  },
			  // "data[Voucher][business][]": {
				// req_business:true
		 // },
		  "data[Voucher][category_id]": {
			required: true	
		 }/*,
		   "data[Voucher][VouchDayStatus]": {				  
			  vouch_status: true
		  }*/
		},
		// Specify validation error messages
		messages: {		
					 
		   "data[Voucher][title_discount]": {
				required: "Please enter Discount value"		
				//mber: "Only numeric values allowed",
				//maxlength: "Only two numeric values allowed"		
			}/*,							
			"data[Voucher][sub_title]": {
				required: "Please enter Sub Title"			
			}*/,			 
		   "data[Voucher][description]": {
				required: "Please enter Description"			
			},							
			"data[Voucher][terms]": {
				required: "Please enter Terms"			
			},			 
		   "data[Voucher][start_date]": {
				required: "Please select Start date"			
			},							
			"data[Voucher][end_date]": {
				required: "Please select End date"			
			},			 
		   "data[Voucher][start_hour]": {
				required: "Please select Start hour"			
			},
			"data[Voucher][end_hour]": {
				required: "Please select End Hour"			
			},	 
		   "data[Voucher][tmpImage]": {
				required: "Please select Image"			
			},							
			"data[Voucher][tags]": {
				required: "Please enter Tags"			
			},
		    "data[Voucher][rurl]" :{
			  required: "Please enter redemption url."
			},
			 // "data[Voucher][business][]": {
				// req_business:"Please select at least one business"
			  // },
			 "data[Voucher][category_id]": {
				req_business:"Please select at least one category"
			  }
			
		},
		// Make sure the form is submitted to the destination defined
		// in the "action" attribute of the form when valid
		submitHandler: function(form) {		
				$('#error_msg').hide();	
				 var radioValue = $("input[name='data[Voucher][repeat_on]']:checked").val();			 
				 if(radioValue=='W'){
					   if ($(".weekdays").children("input:checked").length <= 0) {
						$('#error_msg').show();						
							return false;
						}				
					 }
					
				if($("#VoucherVoucherStatus").val()==1 && !confirmSubmit)
				{	    
					var result=publishpayment();
					return result;					
				}
			    form.submit();
		}
	  });
		 $.validator.addMethod('req_business', function(value, elem) {
		 if ($("#VoucherBusiness").children("option:checked").length <= 0) {	
					
				return false;
			}
			 else { 
				return true;
			}					
		
	   }, 'Please select at least one business');
		//~ $(".confirmvoucheradd").on("click",function(){
			//~ //alert("here");
			//~ confirmSubmit = true;
			//~ $('#VoucherAddForm').submit();
		//~ });
				  
	  $.validator.addMethod('req_question', function(value, elem) {
				$('#error_msg').hide();	
			  var radioValue = $("input[name='data[Voucher][repeat_on]']:checked").val();
			 if(radioValue=='W'){
				   if ($(".weekdays").children("input:checked").length <= 0) {		
				  	$('#error_msg').show();						
						return false;
					}
					 else { 
						return true;
					}					
				 }
		   else {
				return true;
			}
	  }, '');
	 /* $.validator.addMethod('vouch_status', function(value, elem) {			  				  
				if($("#VouchDayStatus").val()=='0'){
					if($("#VoucherStartHour").val()=='' || $("#VoucherEndHour").val()=='')	
					{							
						return false;
					} else {
						return true;
					}
					
				 } else {
					return true;
				}
	  }, 'Please enter Start Time and End Time');*/

	$.validator.addMethod("greaterThan", 
		function(value, element, params) {

			var StartDate= $("#VoucherStartDate").val();
			var EndDate= $("#VoucherEndDate").val();
			var eDate = new Date(EndDate);
			var sDate = new Date(StartDate);
		   if(StartDate!= '' && StartDate!= '' && sDate> eDate)
			{			
			 return false;
			}
			else{
				return true;
			}
		},'End date must be greater than or equal to start date.');


	 $("#preview").click(function() {		
		
		 var StartDate= $("#VoucherStartDate").val();
		 var StartTime=$("#VoucherStartHour").val();		
		 var EndDate= $("#VoucherEndDate").val();
		 var EndTime=$("#VoucherEndHour").val();		
		 var s_datetime= getdatetime(StartDate,StartTime,EndDate,EndTime);
		 var is_toggel = $('#statusToggle').prop('checked');
		if ( $("#tmpImage").val() != "" ) { 		 
			$("#VoucherImg").attr("src",$("#tmpImage").val());
		} else {
			$("#VoucherImg").attr("src",SITE_LINK+"img/default_voucher.jpeg");
		}
		if ( is_toggel ) {
			$("#mapview").hide();
			$("#dirid").hide();
			$("#linkid").show();
			$("#linkid1").attr("href",$("#VoucherRurl").val());
			$("#linkid1").attr("target","_blank");
		} else {
			$("#mapview").show();
			$("#dirid").show();
			$("#linkid").hide();
		}
		var checkValue = $("input[name='data[Voucher][title_status]']:checked").val();
		
		 if ($("#VoucherBusiness").children("option:checked").length <= 0)
		 {
			  $("#voucher-offer-model #locationname").text("Location Name");
			  $("#voucher-offer-model #voucherlocation").text("Voucher Location");
		 }
		 else{
				var j=0;
				if(j==0)
				{
					$.each($("#VoucherBusiness option:selected"), function(){
					   $("#voucher-offer-model #locationname").text($(this).text());
						 
						$.get(SITE_LINK+"users/voucher_location/"+$(this).val(),function(data, status){
						
						var obj = jQuery.parseJSON( data);
						//console.log(obj.message);locationmap
						$("#voucher-offer-model #voucherlocation").text(obj.message);
						//var address="<iframe  class='map-img' src='https://www.google.com/maps?q="+obj.message+"&output=embed></iframe>";
					    $("#voucher-offer-model #locationmap").attr('src',"https://www.google.com/maps?q="+obj.message+"&output=embed");	
											
						}); 
						j++;
					});
				} 
		 }
		var str = $("#discountvalue").val();
		if(checkValue=="D")
		{			
			if (str.indexOf('%') == -1  && str!="") {
			 str=str+"%";
			}
		}
		if(str=="")
		{
			str="10%";
		}
		$("#voucher-offer-model #Voucherdiscount").text(str);
		len=$("#sub_title").val().length;
		 if(len>28)				
		  $("#voucher-offer-model #Vouchersubtitle").text($("#sub_title").val().substr(0,28)+'...');
		else
		 $("#voucher-offer-model #Vouchersubtitle").text($("#sub_title").val());
		
		if($("#sub_title").val()=="")
		{
			$("#voucher-offer-model #Vouchersubtitle").text("e.g. on all cocktails");
		}
		
		$("#voucher-offer-model #Voucheraddress").text($("#VoucherAddress").val());
		len=$("#descriptionvalue").val().length;
		if(len>131)
		  $("#voucher-offer-model #Voucherdescription").text($("#descriptionvalue").val().substr(0,131)+'...');		
		else
		$("#voucher-offer-model #Voucherdescription").text($("#descriptionvalue").val());
		len=$("#termsvalue").val().length;
		if(len>131)
		 $("#voucher-offer-model #Voucherterms").text($("#termsvalue").val().substr(0,131)+'...');		
		else
		$("#voucher-offer-model #Voucherterms").text($("#termsvalue").val());
		//$("#voucher-offer-model #voucherlink").attr("href", $("#terms_url").val())
		$("#voucher-offer-model").modal("show");
	});
	function getdatetime(s_date,s_time,e_date,e_time)
	{	
		//var data = {"s_date":s_date,"s_time":s_time};
		var weekdays = new Array();
		$(".weekdays").children("input:checked").each(function(){
			weekdays.push($(this).val());
		});
		weekdays = weekdays.join(",");
		
		$.get(SITE_LINK+"vouchers/getdate?s_date="+s_date+"&s_time="+s_time+"&e_date="+e_date+"&e_time="+e_time+"&days="+weekdays, function(data, status){

			var obj = jQuery.parseJSON( data);
			$("#voucher-offer-model #Voucherlftmsg").text(obj.message);			
		});
  }
  
	// Applied globally on all textareas with the "autoExpand" class
	$("#descriptionvalue").on('input', function() {
	var scroll_height1 = $("#descriptionvalue").get(0).scrollHeight;

	$("#descriptionvalue").css('height', scroll_height1 + 'px');
	});
     
	$("#termsvalue").on('input', function() {
	var scroll_height1 = $("#termsvalue").get(0).scrollHeight;

	$("#termsvalue").css('height', scroll_height1 + 'px');
	
	});
     /* $('#sample_input').awesomeCropper(
		{ width: 382, height: 382, debug: true }
     );
*/
	
	$( "#VoucherHashtags").keyup(function( event ) {
		var keycode = (event.keyCode ? event.keyCode : event.which);
		
	  if ( keycode== 13 ) {
		 event.preventDefault();		
		 var vals="<li><a href='javascript:void(0)'>"+$(this).val()+" </a><button type='button' class='delete_button' value="+$(this).val()+"><img src='"+SITE_LINK+"img/cros-icon.png' alt='' onclick='delete_tag_data(this.value)'/></button></li>";		
		 if($("#VoucherTags").val()=="")
		 var valtag=$(this).val()+ $("#VoucherTags").val();
		 else
		 var valtag=$(this).val()+","+ $("#VoucherTags").val();
		  $(".tags").append(vals);
		  $("#VoucherHashtags").val('');
		  $("#VoucherTags").val(valtag);
	  }
	
	});
	
	$(document).on("click",".delete_button",function(){
		
		var itemtoRemove=($(this).attr("value"));		
		var result = $("#VoucherTags").val().split(',');
		for(var i = result.length-1; i >= 0; i--){  
			if(result[i] == itemtoRemove){         
				result.splice(i,1);           
			}
		}	
		var result=result.join(",");
		 $("#VoucherTags").val(result);
			$(this).parent().remove();	
	});
	
	$( ".deletetag").click(function( event ) {
		 $("#VoucherHashtags").val('');
	});
	$("#discountvalue").on("keyup",function(){	
		var checkValue = $("input[name='data[Voucher][title_status]']:checked").val();
		
		if(checkValue=="C") {
			$("#descriptionvalue").prop( "readonly", false );	
			$('#description_statusC').prop('checked',true);
		}
		var oldVal = $("#olddiscount").val();
		var val = $("#descriptionvalue").val();
		var strArr = val.split(" ");
		var finalStr = "";
		for ( var i=0; i < strArr.length; i++ ) {
			if ( strArr[i] == oldVal+"%" ) {
				strArr[i] = $(this).val()+"%" ;
			}
			finalStr += strArr[i]+" ";
		}
		$("#descriptionvalue").val(finalStr.trim());
		if ( finalStr != "" ) {
			$("#olddiscount").val($(this).val());
		}
	});	

	$("#discountvalue").on("blur",function(){	
		var checkValue = $("input[name='data[Voucher][title_status]']:checked").val();			
		if(checkValue=="C") {
			$("#descriptionvalue").prop( "readonly", false );	
			$('#description_statusC').prop('checked',true);
		}
		var oldVal = $("#olddiscount").val();
		var val = $("#descriptionvalue").val();
		var strArr = val.split(" ");
		var finalStr = "";
		for ( var i=0; i < strArr.length; i++ ) {
			if ( strArr[i] == oldVal ) {
				strArr[i] = $(this).val();
			}
			finalStr += strArr[i]+" ";
		}
		$("#descriptionvalue").val(finalStr.trim());
		if ( finalStr != "" ) {
			$("#olddiscount").val($(this).val());
		}
	});	
	var emptyStr = "";
	
	$("#title_statusD,#title_statusC").on("click",function(){
		var dValue = $("#default_title").val();
		var dDesc = $("#default_description").val();
		emptyStr = "";
		if ( $(this).val() == 'D' ) {
			$("#discountvalue").val(dValue);
			//$("#discountvalue").attr("readonly","readonly");
			$("#description_statusD").prop("checked", true);
			$("#description_statusC").prop("checked", false);
			$("#olddiscount").val(dValue);
			$("#descriptionvalue").val(dDesc);
			//$("#descriptionvalue").attr("readonly","readonly");
			$('.percertange_sign').show();
		}
		else
		{
			$('.percertange_sign').hide();
			$("#discountvalue").removeAttr("readonly");
			$("#discountvalue").val('');
			$("#discountvalue").focus('');
			$("#description_statusD").prop("checked", false);
			$("#description_statusC").prop("checked", true);
			$("#descriptionvalue").val('');
			$("#descriptionvalue").removeAttr("readonly");
		}
		
	});
	
	
	$("#description_statusD,#description_statusC").on("click",function() {
		
		if ( $(this).val() == 'D' ) {
			//$("#descriptionvalue").attr("readonly","readonly");
			emptyStr = $("#descriptionvalue").val();
			var oldVal = $("#default_title").val();
			var val = $("#default_description").val();			
			var strArr = val.split(" ");
			var finalStr = "";
			for ( var i=0; i < strArr.length; i++ ) {
				if ( strArr[i] == oldVal ) {
					strArr[i] = $("#olddiscount").val();
				}
				finalStr += strArr[i]+" ";
			}
			$("#descriptionvalue").val(finalStr.trim());
		} else {
			$("#descriptionvalue").removeAttr("readonly");
			if ( emptyStr != '' ) {
				$("#descriptionvalue").val(emptyStr);
			}
		}
	});
	var nTerms = "";
	$("#terms_statusD,#terms_statusC").on("click",function(){
		var dTerms = $("#default_terms").val();
		if ( $(this).val() == "D" ) {
			nTerms = $("#termsvalue").val();
			$("#termsvalue").val(dTerms);
			//$("#termsvalue").attr("readonly","readonly");
			$(".website-field").show();
		} else {
			//if ( nTerms != "" ) {
				$("#termsvalue").val(nTerms);
			//}
			$(".website-field").hide();
			$("#termsvalue").removeAttr("readonly");
		}
	});
	
	var nTitle = "";
	$("#sub_title_statusC,#sub_title_statusD").on("click",function(){
		var dTitle = $("#default_sub_title").val();
		if ( $(this).val() == "D" ) {
			nTitle = $("#sub_title").val();
			$("#sub_title").val(dTitle);
			$("#sub_title").attr("readonly","readonly");
		} else {
			//if ( nTitle != "" ) {
				$("#sub_title").val(nTitle);
			//}
			$("#sub_title").removeAttr("readonly");
		}
	});
	
	
	$("#img-upload,#drop-zone").on("click",function(){
		$("#imageModal").modal("show");
	});
	
	$("#VoucherCategory").on("focus",function(){
		//if ( $(this).val().trim() != "" ) {
			getresults($(this).val().trim());
		//}
	});
	
	$("#VoucherCategory").on("keyup",function(){
		//if ( $(this).val().trim() != "" && ($(this).val().trim().length >= 3) ) {
			getresults($(this).val().trim());
		//}
	});
	$("#VoucherCategory").on("blur",function(){
		setTimeout(function(){ $("#inresult").html(""); $(".result").hide(); },1000);
		//$("#inresult").html("");
		//$(".result").hide();
	});
	
	var listCategories = "<li id='{ID}' class='selcat'>{VALUE}</li>";
	var getresults = function(keyword){
		$("#inresult").html("");
		$(".result").hide()
		$.get(SITE_LINK+"users/getcategories?key="+keyword, function(data, status){
			var obj = JSON.parse(data);
			//console.log(obj);
			tmpNew = listCategories;
			$.each(obj,function(i,val){
				tmpNew = listCategories.replace(/{ID}/g,i);
				tmpNew = tmpNew.replace(/{VALUE}/g,val);
				$("#inresult").append(tmpNew);
			});
		});
		$(".result").show();
	}
	
	$(document).on("click",".selcat",function(){
		 var valtag;
		 var vals;
		 var split_str;
		 if($("#VoucherCategoryId").val()=="")
		 {
			 valtag=$(this).attr("id")+ $("#VoucherCategoryId").val();
			 vals="<li><a href='javascript:void(0)'>"+$(this).html()+" </a><button type='button' class='delete_cate' value="+$(this).attr("id")+"><img src='"+SITE_LINK+"img/cros-icon.png' alt='' onclick='delete_tag_data(this.value)'/></button></li>";		
		 }
		 else
		 {
			var split_str = $("#VoucherCategoryId").val().split(",");			
			if (split_str.indexOf($(this).attr("id")) !== -1) {				
				valtag= $("#VoucherCategoryId").val();
				vals="";
			} 
			else
			{
				valtag=$(this).attr("id")+","+ $("#VoucherCategoryId").val();
				vals="<li><a href='javascript:void(0)'>"+$(this).html()+" </a><button type='button' class='delete_cate' value="+$(this).attr("id")+"><img src='"+SITE_LINK+"img/cros-icon.png' alt='' onclick='delete_tag_data(this.value)'/></button></li>";		

			}
		  }
		  $(".catenames").append(vals);
		  $("#VoucherCategory").val('');
		  $("#VoucherCategoryId").val(valtag);
		
		//$("#VoucherCategoryId").val($(this).attr("id"));
		//$("#VoucherCategory").val($(this).html());
	});
     $(document).on("click",".delete_cate",function(){
		
		var itemtoRemove=($(this).attr("value"));		
		var result = $("#VoucherCategoryId").val().split(',');
		for(var i = result.length-1; i >= 0; i--){  
			if(result[i] == itemtoRemove){         
				result.splice(i,1);           
			}
		}	
		var result=result.join(",");
		 $("#VoucherCategoryId").val(result);
			$(this).parent().remove();	
	});
});
