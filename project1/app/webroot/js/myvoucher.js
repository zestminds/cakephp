 $(document).ready(function(){
	var page =2;
	var loadFlag = true;
	if(window.location.href.indexOf("drafted-vouchers") > -1) {
		var tmp = '<div class="box-card"><div class="box-info"><a href="javascript:void(0);"><div class="card-img"><img src="{SITE_LINK}img/voucher/{IMG}" alt=""></div></a><div class="card-overlay"><ul class="view-btns"><li><a href="javascript:void(0);" class="view_voucher" data-value="{VOUCHERID}"><img src="{SITE_LINK}img/eye-icon2.png" alt=""></a></li><li><a href="javascript:void(0);" data-value="{VOUCHERID}" class="pview_voucher"><img src="{SITE_LINK}img/srch-icon1.png" alt=""> </a></li></ul><ul class="action-btns"><li><a href="javscript:void(0);" id="{VOUCHERID}" value="{VOUCHERID}"  class="copy-icon"></a> </li><li><a href="{SITE_LINK}edit-voucher/{VOUCHERID}"><img src="{SITE_LINK}img/edit-icon1.png" alt=""></a></li><li> <a href="javascript:void(0);" class="confirmdelete" action="{SITE_LINK}delete-voucher/{VOUCHERID}" ><img src="{SITE_LINK}img/del-icon1.png" alt="Delete" title="Delete"></a>  </li></ul></div></div></div>';
	}else{
		var tmp = '<div class="box-card"><div class="box-info"><a href="javascript:void(0);"><div class="card-img"><img src="{SITE_LINK}img/voucher/{IMG}" alt=""></div></a><div class="card-overlay"><ul class="view-btns"><li><a href="javascript:void(0);" data-value="{VOUCHERID}" class="view_voucher"><img src="{SITE_LINK}img/eye-icon2.png" alt=""></a></li><li><a href="javascript:void(0);" data-value="{VOUCHERID}" class="pview_voucher"><img src="{SITE_LINK}img/srch-icon1.png" alt=""> </a></li><li><a href="javascript:void(0);"><img src="{SITE_LINK}img/link-icon1.png" alt=""></a></li></ul><ul class="action-btns"><li><a href="javascript:void(0);" id="{VOUCHERID}" value="{VOUCHERID}"  class="copy-icon"></a> </li><li><a href="{SITE_LINK}edit-voucher/{VOUCHERID}"><img src="{SITE_LINK}img/edit-icon1.png" alt=""></a></li><li> <a href="javascript:void(0);" action="{SITE_LINK}delete-voucher/{VOUCHERID}" class="confirmdelete"><img src="{SITE_LINK}img/del-icon1.png" alt="Delete" title="Delete"></a>  </li></ul></div></div></div>';
	}
	var tmplink = "";
	$(document).on("click",".confirmdelete",function(){
		tmplink = $(this).attr("action");
		$("#cnfrmdelid").modal("show");
	});
	$(document).on("click",".delvoucher",function(){
		//console.log(tmplink);
		location.href= tmplink;
	});
	/*
	$(document).on("click",".delvoucher",fucntion(){
		console.log(tmplink);
	});
	*/
	 $(document).on("click",".pview_voucher",function(e){
		 e.preventDefault();
		 var href =SITE_LINK+"perview-voucher/"+$(this).attr('data-value');		
		 $.ajax({   
			type: "GET",   
			cache: false,   
			url: href,   
			success: function(data){    
				//console.log(data);
				bootbox.dialog(
					{ 	
						message: data,
						backdrop: true,
						title: "",    
					}
				).find("div.modal-dialog").addClass("lg-model"); 
			}  
		});
		
	});
	
	$(document).on("click",".view_voucher",function(e){
		 e.preventDefault();
		 $("#voucher-offer-model").modal("show");
		 var href =SITE_LINK+"view-voucher/"+$(this).attr('data-value');		
		 $.ajax({   
			type: "GET",   
			cache: false,   
			url: href,   
			success: function(data){    
				//console.log(data);
				data = JSON.parse(data);
				console.log(data);
				var details = data[0];
				var business = details.Location.business;
				var avatar = SITE_LINK+"img/profile/"+details.UserDetail.image;
				var voucherImage = SITE_LINK+"img/voucher/"+details.Voucher.image;
				$("#bname").text(business);
				
				var sub_title=details.Voucher.sub_title;
				len = sub_title.length;
				 if(len>28)	
				  $("#bname1").text(sub_title.substr(0,28)+'...');				
				else
				 $("#bname1").text(sub_title);
				
				if ( sub_title.length == 0 ) {
					$("#bname1").text("NA");
				}
				 
				$("#Voucherdiscount").text(details.Voucher.title_discount);
				$("#Voucherlftmsg").text(data['message'].message);
				$("#av-img").attr("src",avatar);
				$("#VoucherImg").attr("src",voucherImage);
				var str = '<iframe  class="map-img" src="https://www.google.com/maps?q='+details.UserDetail.address+'&output=embed"></iframe>';
				$("#map").html(str);
				
				var description=details.Voucher.description;
				len = description.length;
				 if(len>131)	
				  $("#Voucherdescription").text(description.substr(0,131)+'...');				
				else
				 $("#Voucherdescription").text(description);
				 
				 var terms=details.Voucher.terms;
				len = terms.length;
				 if(len>131)	
				  $("#Voucherterms").text(terms.substr(0,131)+'...');				
				else
				 $("#Voucherterms").text(terms);
				$("#uadd").html(details.UserDetail.address);
				
			}  
		});
		
	});
	var flag = true;
	$(window).scroll(function (e) {
		if (flag) {
			if ($(window).scrollTop() + $(window).height() >= $(document).height() - 50) {
				if (loadFlag) {
					var href;
					if(window.location.href.indexOf("drafted-vouchers") > -1) {
						href =SITE_LINK+"drafted-vouchers/page:"+page;
					}else{
						href =SITE_LINK+"published-vouchers/page:"+page;
					}				
					$.ajax({
						type : "GET",
						url : href,
						dataType: 'json',
						success: function(data) {
							if ( data.length > 0 ) {
								var tmpNew = tmp;
								$.each(data,function(key,val){
									tmpNew = tmp.replace(/{IMG}/g,val['image']);
									tmpNew = tmpNew.replace(/{VOUCHERID}/g,val['id']);
									tmpNew = tmpNew.replace(/{SITE_LINK}/g,SITE_LINK);
									$(".voucher-boxCont").append(tmpNew);
								});
							} else {
								flag = false;
								loadFlag = false;
							}
						},
						error: function(){
							flag = false;	
						}
					});
					page +=1;
				}
			}
		}
	});	 
	$(document).on("click",".copy-icon",function(e){
		$("#cnfrmcopyid").modal("show");
		$("#VoucherVoucherid").val($(this).attr("value"));	
	});
	$(document).on("click",".copyvoucher",function(e){
		var vouchid=$("#VoucherVoucherid").val();
		var url = SITE_LINK+"copy-voucher/"+vouchid;
		
		$.get(url, function(data, status){
			var obj = JSON.parse(data);
			var href = SITE_LINK+"drafted-vouchers/"+obj.id;
			if ( obj.status == 1 && !obj.vstatus) {
				$.ajax({
					type : "GET",
					url : href,
					dataType: 'json',
					success: function(data) {
						if ( data.length > 0 ) {
							var tmpNew = tmp;
							$.each(data,function(key,val){
								tmpNew = tmp.replace(/{IMG}/g,val['image']);
								tmpNew = tmpNew.replace(/{VOUCHERID}/g,val['id']);
								tmpNew = tmpNew.replace(/{SITE_LINK}/g,SITE_LINK);
								$(".voucher-boxCont").prepend(tmpNew);
							});
							
						} else {
							flag = false;
							loadFlag = false;
						}
					},
					error: function(){
						flag = false;	
					}
					
				});
			} else {
				//$("#cnfrmcopyid").modal("show");
			}
		});
		$("#cnfrmcopyid").modal("hide");
		e.preventDefault();
		return false;
	});
	
});
