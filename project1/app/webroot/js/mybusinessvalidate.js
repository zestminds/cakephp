	$(document).ready(function () {
	 $('#flashMessage').delay(5000).fadeOut();
	 
	 $("#makepay").on("click",function(){
		$("#makepayment").modal("show");
	 });
	 
	 $(".confirmpayment").on("click",function(){
		var src = $("#paymentbtn").attr("href");
		location.href = src;
	 });
	 
     $('#BusinessBusinessForm').validate({ // initialize the plugin
        rules: {			
			 
			  "data[Business][title]": {
				required: true,				
			  },
			  "data[Business][address]": {
				required: true				
			  }		
			},
			// Specify validation error messages
			messages: {			
				"data[Business][title]": {
					required: "Please enter business name"			
				},		 
				"data[Business][address]": {
					required: "Please enter business address"			
				}
			},			
			submitHandler: function(form) {
			  form.submit();
			}
		  });
	
	
    var flag = true;
	//~ $(".img-upload").on("click",function(){
				//~ $("#imageModalProfile").modal("show");
	//~ });
				
	$(document).on("click","#deleteuserbusiness",function(e){
		$("#deletebusienssbox").modal("show");
	});
	
	$(document).on("click",".confirmdeletebusiness",function(e){
		 var id=$("#BusinessBusinessid").val();		
		 var href =SITE_LINK+"deleteuserbusiness?id="+id;		
		 $.ajax({   
			type: "GET",   
			cache: false,   
			url: href,   
			success: function(data){
				data = JSON.parse(data);
				if(data.message=="Deleted")				
				{
					$("#deletebusienssbox").modal("hide");
					window.location.href = SITE_LINK+"my-account";
				}
			}  
		});		
	});
	$(document).on("click","#pausepaypalpayment",function(e){
		 var busid=$("#LocationBusinessid").val();		
		 var href =SITE_LINK+"pausepaypalpayment?busid="+busid;			
		 $.ajax({   
			type: "GET",   
			cache: false,   
			url: href,   
			success: function(data){
				data = JSON.parse(data);				
				if(data.message=="0"){
					$("#pausepaypalpayment").html(("Re-activate Payment"));
				}
				else if(data.message=="1")
				{
					$("#pausepaypalpayment").html(("Pause Payment"));
				}
				else{}
					
			}  
		});		
	});	
	var switchStatus = false;
		$("#statusToggle").on('change', function() {
			if ($(this).is(':checked')) {
				switchStatus = $(this).is(':checked');
				$("#statusToggle").val(1);
			}
			else {
			   switchStatus = $(this).is(':checked');
			   $("#statusToggle").val(0);
			}
		});
		
	
		
		// Open popup model
		//~ $("#drop-zone1").on("click",function(){
		 $("#drop-zone1").on("click",function(){
			$("#imageModal").modal("show");
		});
		
		 $("#drop-zone2").on("click",function(){
			$("#imageModal2").modal("show");
		});		
		
		 $("#drop-zone3").on("click",function(){
		$("#imageModal3").modal("show");
		});	
		$("#drop-zone4").on("click",function(){
			$("#imageModal4").modal("show");
		});
		
		 $("#drop-zone5").on("click",function(){
			$("#imageModal5").modal("show");
		});		
		
		 $("#drop-zone6").on("click",function(){
		$("#imageModal6").modal("show");
		});	
		
		
		
		
		
	var action=preview_empty="";
	$(document).on("click",".delete_location",function(e){
		$("#delete_locationbox").modal("show");
		  action= $(this).attr("action");
		  empty_value= $(this).attr("value");
	});
	$(document).on("click",".confirmdelete_location",function(e){
		
			if(action=="")
			{
				$("#delete_locationbox").modal("hide");
				$('#img_upload'+empty_value).attr("src",SITE_LINK+"img/location-here.png");	
				$('#img_upload'+empty_value+'_value').val('');
				$('.img_upload'+empty_value+'_image').hide();
				$('#image_'+empty_value).attr("action", "")	;	
			}
			else
			{			
				var href =SITE_LINK+action;			 
				 $.ajax({   
					type: "GET",   
					cache: false,   
					url: href,   
					success: function(data){				
						data = JSON.parse(data);
						if(data.message=="Deleted")
						{
							 $("#delete_locationbox").modal("hide");
							$('#img_upload'+empty_value).attr("src",SITE_LINK+"img/location-here.png");	
							$('#img_upload'+empty_value+'_value').val('');
							$('#img_upload'+empty_value+'_old').val('');
							$('.img_upload'+empty_value+'_image').hide();
							$('#image_'+empty_value).attr("action", "")	;						
							//~ location=$("#LocationUrlPage").val();						
							//~ location.href=location;
						}
					}			
				});	
			}			
	});
	
	
});
