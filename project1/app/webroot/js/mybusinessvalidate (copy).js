	$(document).ready(function () {
	 $('#flashMessage').delay(5000).fadeOut();
	 
	 $("#makepay").on("click",function(){
		$("#makepayment").modal("show");
	 });
	 
	 $(".confirmpayment").on("click",function(){
		var src = $("#paymentbtn").attr("href");
		location.href = src;
	 });
	 
     $('#BusinessBusinessForm').validate({ // initialize the plugin
        rules: {			
			 
			  "data[Business][title]": {
				required: true,				
			  },
			  "data[Business][address]": {
				required: true				
			  }		
			},
			// Specify validation error messages
			messages: {			
				"data[Business][title]": {
					required: "Please enter business name"			
				},		 
				"data[Business][address]": {
					required: "Please enter business address"			
				}
			},			
			submitHandler: function(form) {
			  form.submit();
			}
		  });
	 
    var flag = true;
	$(".img-upload").on("click",function(){
		//var reader = new FileReader();
		//reader.onload = function(e) {
			/*var CanvasCrop = $.CanvasCrop({
				cropBox : ".imageBox",
				imgSrc : avatar,
				limitOver : 2
			});
			rot =0 ;
			ratio = 1;*/
		//}
		//reader.readAsDataURL(avatar);
		$("#imageModal").modal("show");
		
	});		
	$(document).on("click","#deleteuserbusiness",function(e){
		$("#deletebusienssbox").modal("show");
	});
	
	$(document).on("click",".confirmdeletebusiness",function(e){
		 var id=$("#BusinessBusinessid").val();		
		 var href =SITE_LINK+"deleteuserbusiness?id="+id;		
		 $.ajax({   
			type: "GET",   
			cache: false,   
			url: href,   
			success: function(data){
				data = JSON.parse(data);
				if(data.message=="Deleted")				
				{
					$("#deletebusienssbox").modal("hide");
					window.location.href = SITE_LINK+"my-account";
				}
			}  
		});		
	});
	$(document).on("click","#pausepaypalpayment",function(e){
		 var busid=$("#LocationBusinessid").val();		
		 var href =SITE_LINK+"pausepaypalpayment?busid="+busid;			
		 $.ajax({   
			type: "GET",   
			cache: false,   
			url: href,   
			success: function(data){
				data = JSON.parse(data);				
				if(data.message=="0"){
					$("#pausepaypalpayment").html(("Re-activate Payment"));
				}
				else if(data.message=="1")
				{
					$("#pausepaypalpayment").html(("Pause Payment"));
				}
				else{}
					
			}  
		});		
	});	
	
	
});
