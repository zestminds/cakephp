$(document).ready(function () {	
	$("a[data]").click(function(e) { 		
		   location.href = SITE_LINK+"insights?type="+ $("#voucher_status").val()+"&interval="+ $(this).attr("data")+"&#top";		  
		});
		
	$(".showHour").click(function() {			
			$("#chart_week").hide();
			$("#chart_hour").show();
			$("showHour").addClass("intro");
	});
	
	$(".showWeek").click(function() {
			$("#chart_week").show();
			$("#chart_hour").hide();
	});	
	
	var page =2;
	var loadFlag = true;
	var tmp = '<div class="box-card"><div class="box-info"><a href="javascript:void(0);"><div class="card-img"><img src="{SITE_LINK}img/voucher/{IMG}" alt=""></div></a><span class="pst-cont">{COUNT}</span></div></div>';
	
	
	var flag = true;
	$(window).scroll(function (e) {
		if (flag) {
			if ($(window).scrollTop() + $(window).height() >= $(document).height() - 50) {
				if (loadFlag) {
					var href =SITE_LINK+"insights/page:"+page+"?type="+ $("#voucher_status").val()+"&interval="+$("#interval").val();		
								
					$.ajax({
						type : "GET",
						url : href,
						dataType: 'json',
						success: function(data) {
							console.log(data);
							if ( data.length > 0 ) {
								var tmpNew = tmp;
								$.each(data,function(key,val){
									tmpNew = tmp.replace(/{IMG}/g,val['image']);
									tmpNew = tmpNew.replace(/{COUNT}/g,val['count']);
									tmpNew = tmpNew.replace(/{SITE_LINK}/g,SITE_LINK);									
									$(".voucher-boxCont").append(tmpNew);
								});
							} else {
								flag = false;
								loadFlag = false;
							}
						},
						error: function(){
							flag = false;	
						}
					});
					page +=1;
				}
			}
		}
	});	 
	
	
	
});

