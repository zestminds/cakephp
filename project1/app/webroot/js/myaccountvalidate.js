	$(document).ready(function () {
	 $('#flashMessage').delay(5000).fadeOut();
     $('#UserEditForm').validate({ // initialize the plugin
        rules: {			
			 
			  "data[UserDetail][name]": {
				required: true,				
			  },
			  "data[UserDetail][business]": {
				required: true				
			  }		
			},
			// Specify validation error messages
			messages: {			
				"data[UserDetail][name]": {
					required: "Please enter Name"			
				},			 
				"data[UserDetail][business]": {
					required: "Please add business name"			
				}
			},			
			submitHandler: function(form) {
			  form.submit();
			}
		  });
	 
    var flag = true;
	$(".img-upload").on("click",function(){
		//var reader = new FileReader();
		//reader.onload = function(e) {
			/*var CanvasCrop = $.CanvasCrop({
				cropBox : ".imageBox",
				imgSrc : avatar,
				limitOver : 2
			});
			rot =0 ;
			ratio = 1;*/
		//}
		//reader.readAsDataURL(avatar);
		$("#imageModal").modal("show");
		
	});	
	
	$("#UserCategoryCategoryId").on("focus",function(){
		//if ( $(this).val().trim() != "" ) {
			getresults($(this).val().trim());
		//}
	});
	
	$("#UserCategoryCategoryId").on("keyup",function(){
		//if ( $(this).val().trim() != "" && ($(this).val().trim().length >= 3) ) {
			getresults($(this).val().trim());
		//}
	});
	$("#UserCategoryCategoryId").on("blur",function(){
		setTimeout(function(){ $("#inresult").html(""); $(".result").hide(); },1000);
		//$("#inresult").html("");
		//$(".result").hide();
	});
	
	var listCategories = "<li id='{ID}' class='selcat'>{VALUE}</li>";
	var getresults = function(keyword){
		$("#inresult").html("");
		$(".result").hide()
		$.get(SITE_LINK+"users/getcategories?key="+keyword, function(data, status){
			var obj = JSON.parse(data);
			//console.log(obj);
			tmpNew = listCategories;
			$.each(obj,function(i,val){
				tmpNew = listCategories.replace(/{ID}/g,i);
				tmpNew = tmpNew.replace(/{VALUE}/g,val);
				$("#inresult").append(tmpNew);
			});
		});
		$(".result").show();
	}
	$(document).on("click",".selcat",function(){
		$("#UserCategoryCategoryIdTmp").val($(this).attr("id"));
		$("#UserCategoryCategoryId").val($(this).html());
	});
	
	
	 $(document).on("click","#subscribelist",function(e){
		 var href =SITE_LINK+"subscribelist";		
		 $.ajax({   
			type: "GET",   
			cache: false,   
			url: href,   
			success: function(data){
				data = JSON.parse(data);
				$("#subscribelist").html((data.message));
			}  
		});		
	});
	
	$(document).on("click","#deleteuseraccount",function(e){
		 var href =SITE_LINK+"deleteuseraccount";		
		 $.ajax({   
			type: "GET",   
			cache: false,   
			url: href,   
			success: function(data){
				data = JSON.parse(data);
				if(data.message=="Deleted")
				window.location.replace(SITE_LINK+"logout");

			}  
		});		
	});
	$(document).on("click","#pausepaypalpayment",function(e){
		 var href =SITE_LINK+"pausepaypalpayment";		
		 $.ajax({   
			type: "GET",   
			cache: false,   
			url: href,   
			success: function(data){
				data = JSON.parse(data);				
				if(data.message=="0"){
					$("#pausepaypalpayment").html(("Re-activate Payment"));
				}
				else if(data.message=="1")
				{
					$("#pausepaypalpayment").html(("Pause Payment"));
				}
				else{}
					
			}  
		});		
	});
	
	$(document).on("click",".addbusiness",function(e){
		 var href =SITE_LINK+"addbusiness";		
		 $.ajax({   
			type: "GET",   
			cache: false,   
			url: href,   
			success: function(data){
				data = JSON.parse(data);
				if(data.message=="added")
				{
					$("#BusinessBusinesslastid").val(data.busid);					
				}
			}  
		});		
	});
	
});
