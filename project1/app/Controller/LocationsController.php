<?php
App::uses('AppController', 'Controller');
/**
 * Locations Controller
 *
 * @property Location $Location
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 * @property FlashComponent $Flash
 */
class LocationsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session', 'Flash',"Paypal");

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->layout = "mooladesignmyvoucher";
		$this->loadModel("LocationImages");
		$this->conditions=array("Location.user_id"=> $this->Session->read('Auth.User.id'));	
		$paymentconfirm="";
		if ( $this->request->is("post") ) {	
			$searchval = strtolower(trim($this->data['search']));
			$this->conditions = array_merge($this->conditions,array("OR"=>array("LOWER(Location.title) like"=> "%".$searchval."%","LOWER(Location.address) like"=> "%".$searchval."%")));
			//pr($this->conditions);die;
			 $business=$this->Paginator->paginate($this->conditions);
		}
		else
		{	
			if(isset($this->request->pass[0]))
			{
				if($this->request->pass[0]=="cancel")
				{
					unset($_SESSION['vouc_id']);
					$paymentconfirm="cancel";
				}
				else if($this->request->pass[0]=="b")
				{					
					$paymentconfirm="newadded";
				}
				else{
					if(!empty($this->Session->read("vouc_id")))
					{$paymentconfirm="voucherupdate";}
					else 
					{
						$this->loadModel('Voucher');									
						$voucher = $this->Voucher->find('first',array('conditions'=>array('Voucher.user_id'=>$this->Session->read('Auth.User.id'))));					
						if(!empty($voucher))
						{$paymentconfirm="paymentconfirm";}
						else{$paymentconfirm="novoucher";}
					}
					unset($_SESSION['cart_items']);				
			     	$this->location_list();
				}	
			}			
		}		
		$business=$this->Location->find("all",array('conditions'=>$this->conditions));
		//pr($business);	
		$this->set('paymentconfirm', $paymentconfirm);
		$this->set('business', $business);
	}
	
	//moolarewards.com/my_cart
	public function my_cart() {	
		$this->layout = "mooladesignmycart";	
		//pr($this->Session->read('cart_items'));
		if ($this->request->is('post')) {
			$business=$this->data["Locations"]["businessid"];
			$planid=$this->data["Locations"]["planid"];
			
			$this->redirect(SITE_LINK."buy-plan/".$planid."/".$business );
		}
		$this->loadModel("Plan");
		$this->conditions=array("Plan.is_active"=>1);	
		$plan=$this->Plan->find("first",$this->conditions);	
		$this->conditions=array("Location.user_id"=> $this->Session->read('Auth.User.id'),"Location.is_paid"=>0);	
		$business=$this->Paginator->paginate($this->conditions);	
		if(empty($business))
		{
			$this->redirect(SITE_LINK."locations");
		}
		else
		{			
			 $this->set('planid', $plan["Plan"]["id"]);
		}
	}
	
/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Location->exists($id)) {
			throw new NotFoundException(__('Invalid Location'));
		}
		$options = array('conditions' => array('Location.' . $this->Location->primaryKey => $id));
		$this->set('location', $this->Location->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
 //add business using ajax
 
 public function add()
	{
		$this->layout = "mooladesignmycart";
		$this->loadModel("User");
		$message = "";
		$this->set('optionsWeekday', array( '1' => 'M', '2' => 'T','3' => 'W', '4' => 'T','5' => 'F', '6' => 'S', '7' => 'S'));

		$this->Location->create();		
		$this->jsArray[] = array("main_location","cropper.min","mybusinessvalidate");	
		$this->cssArray[] = array("cropper.min","main");
		$this->loadModel("OpeningTiming");
		$this->loadModel("LocationImages");
		$options = array('user_id' => $this->Session->read('Auth.User.id'));

		 $data= $this->OpeningTiming->find('all', $options);
		 $this->set("data",$data);		  
		
		if ($this->request->is(array('post', 'put'))) {	
			
		
			
			$this->request->data['Location']['user_id'] =$this->Session->read("Auth.User.id");
			if ($this->Location->save($this->request->data)) {
				$busid=$this->Location->getLastInsertID();	
				$location_arr=array("id"=>$busid,"address"=>$this->request->data['Location']['address'],"title"=>$this->request->data['Location']['title'] ,"image"=>$this->request->data['Location']['image'],"is_view"=>1 ,"created"=>date("Y/m/d H:i:s"));
				$_SESSION['cart_items'][$busid]= $location_arr;				
				$_SESSION["cart_total_items"]+= 1;
				
			}
			
			$totalimage= count($this->request->data['LocationImages']);
			
				for($i=1;$i<=$totalimage;$i++)
				{
					if (isset($this->request->data['LocationImages']['img_upload'.$i.'_value']) && !empty($this->request->data['LocationImages']['img_upload'.$i.'_value'])) {				
						$this->genImage($this->request->data['LocationImages']['img_upload'.$i.'_value'],"location");
						$this->request->data['LocationImages'][$i]['image'] = $this->imagename;
						$this->request->data['LocationImages'][$i]['location_id'] = $busid;					
					} 
					unset($this->request->data["LocationImages"]['img_upload'.$i.'_value']);
				}
				
				if(!empty($this->request->data["LocationImages"]))
				$this->LocationImages->saveAll($this->request->data["LocationImages"]);		
			
			
			
			if(isset($this->request->data['OpeningTiming']))
			{	
				
				
				for($i=1;$i<=7;$i++) {
					
					$this->request->data["OpeningTiming"][$i]["user_id"]=$this->Session->read('Auth.User.id');
				    $this->request->data["OpeningTiming"][$i]["location_id"]=$busid;
					$this->request->data["OpeningTiming"][$i]["days"]=$i;
					if(empty($this->request->data['OpeningTiming']["starttime_".$i]))
					{
						$this->request->data['OpeningTiming'][$i]["start_time"]='';
					}
					else{
						$this->request->data["OpeningTiming"][$i]["start_time"]=date("H:i:s",strtotime($this->request->data["OpeningTiming"]["starttime_".$i]));					
					}
					if(empty($this->request->data['OpeningTiming']["endtime_".$i]))
					{
						$this->request->data['OpeningTiming'][$i]["end_time"]='';
					}		
					else{				

					$this->request->data["OpeningTiming"][$i]["end_time"]=date("H:i:s",strtotime($this->request->data["OpeningTiming"]["endtime_".$i]));
					}
					unset($this->request->data["OpeningTiming"]["endtime_".$i]);
					unset($this->request->data["OpeningTiming"]["starttime_".$i]);
					//~ $days[$i]=$weekday;				
				}				
				$this->OpeningTiming->saveAll($this->request->data["OpeningTiming"]);				
			 }	
						
			$url ="locations/b";
		   $this->redirect(SITE_LINK.$url);		
			
		}
		else
		{
			$user = $this->User->find('first',array("conditions"=>array("User.id"=>$this->Session->read("Auth.User.id")),"fields"=>"UserDetail.business,UserDetail.image,UserDetail.facebookid,UserDetail.twitterid,UserDetail.instagramid,UserDetail.aboutme"));		
			//pr($user);	
			$address="Manchester, United Kingdom";
			$latitude="53.4807593";		
			$longitude="-2.2426305000000184";	
			$this->set(compact('latitude',$latitude));
			$this->set(compact('longitude',$longitude));
			$this->set(compact('address',$address));
			$this->set(compact('user'));
		}
	
	}
 public function addbusiness()
	{
		if ( $this->request->is("ajax") ) {		
			
			$this->loadModel("User");
			$message = "";
			$this->Location->create();
			//pr($this->Session->read("Auth"));
			//die;
			
			$user = $this->User->find('first',array("conditions"=>array("User.id"=>$this->Session->read("Auth.User.id"))));				
			$this->request->data["Location"]["user_id"]= $this->Session->read("Auth.User.id");
			$this->request->data["Location"]["title"]= $user["UserDetail"]["business"];
			$this->request->data["Location"]["facebookid"]= $user["UserDetail"]["facebookid"];
			$this->request->data["Location"]["twitterid"]= $user["UserDetail"]["twitterid"];
			$this->request->data["Location"]["instagramid"]= $user["UserDetail"]["instagramid"];
			$this->request->data["Location"]["about"]= $user["UserDetail"]["aboutme"];			
			
			if( isset($user['UserDetail']['image']) && !empty($user['UserDetail']['image']) && file_exists(WWW_ROOT."/img/profile/".$user['UserDetail']['image']) )
			 {
				$tmpname = strtotime(date("Y-m-d h:i:s")).$user['UserDetail']['image'];
				//echo WWW_ROOT."img/voucher/".$voucher['Voucher']['image'];
				//die;
				if ( copy(WWW_ROOT."img/profile/".$user['UserDetail']['image'],WWW_ROOT."img/profile/".$tmpname) ) {
					$this->request->data['Location']['image'] = $tmpname;					
				}
			}
						

			
			if ($this->Location->save($this->request->data)) {
				$busid=$this->Location->getLastInsertID();	
				$location_arr=array("id"=>$busid,"address"=>"Manchester, United Kingdom","title"=>$this->request->data['Location']['title'] ,"image"=>$this->request->data['Location']['image'],"is_view"=>1 ,"created"=>date("Y/m/d H:i:s"));					
				//$loc_arr[$busid]=$location_arr;				
				$_SESSION['cart_items'][$busid]= $location_arr;				
				$_SESSION["cart_total_items"]+= 1;
				$result = array("message"=>"added","busid"=>$busid."-".str_replace(" ","-",strtolower($this->request->data["Location"]["title"])));
			} else {
				$result = array("message"=>$message);
			}	
			echo json_encode($result);
			die;			
		}		
	}
/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
 /// Update business
 public function business($slug = NULL)
	{
		$this->layout = "mooladesignmyvoucher";
		//$this->loadModel("Business");
		$this->loadModel("Plan");
		$this->set('optionsWeekday', array( '1' => 'M', '2' => 'T','3' => 'W', '4' => 'T','5' => 'F', '6' => 'S', '7' => 'S'));		
		$this->jsArray[] = array("main_location","cropper.min","mybusinessvalidate");	
		$this->cssArray[] = array("cropper.min","main");
		$this->loadModel("OpeningTiming");
		$this->loadModel("LocationImage");
		
		
		//$this->Session->read("cart_items");die;
		if ($this->request->is(array('post', 'put'))) {		
			//pr($this->request->data);die;
			$businessid=$this->request->data['Location']['businessid'];
			if (isset($this->request->data['Location']['tmpImage']) && !empty($this->request->data['Location']['tmpImage'])) {				
				$this->genImage($this->request->data['Location']['tmpImage'],"profile");
				$this->request->data['Location']['image'] = $this->imagename;
				
			} else {
				unset($this->request->data['Location']['image']);
			}
			if(isset( $_SESSION["cart_items"][$businessid])){			 
				$_SESSION["cart_items"][$businessid]['title'] = $this->request->data['Location']['title'];
				$_SESSION["cart_items"][$businessid]['address'] = $this->request->data['Location']['address'];
				if(isset($this->request->data['Location']['image']))
				$_SESSION["cart_items"][$businessid]['image'] = $this->request->data['Location']['image'];
			}
			$this->request->data["Location"]["id"]= $this->request->data["Location"]["businessid"];
			if ($this->Location->save($this->request->data)) {
				$this->Flash->success(__('The Location has been saved.'));	
			} else {
				$this->Flash->error(__('The Location could not be saved. Please, try again.'));
			}
			
			
			$totalimage= count($this->request->data['LocationImages']);
			//pr($this->request->data['LocationImages']);
			//echo $totalimage;die;
				for($i=1;$i<=6;$i++)
				{
					if (isset($this->request->data['LocationImages']['img_upload'.$i.'_value']) && !empty($this->request->data['LocationImages']['img_upload'.$i.'_value'])) {				
						$this->genImage($this->request->data['LocationImages']['img_upload'.$i.'_value'],"location");
						$this->request->data['LocationImages'][$i]['image'] = $this->imagename;
						$this->request->data['LocationImages'][$i]['location_id'] = $businessid;	
				
					}
					else
					{
						if (isset($this->request->data['LocationImages']['img_upload'.$i.'_old']) && !empty($this->request->data['LocationImages']['img_upload'.$i.'_old'])) {				
						$this->request->data['LocationImages'][$i]['image'] = $this->request->data['LocationImages']['img_upload'.$i.'_old'];
						$this->request->data['LocationImages'][$i]['location_id'] = $businessid;	

						}
					} 
					unset($this->request->data["LocationImages"]['img_upload'.$i.'_value']);
					unset($this->request->data["LocationImages"]['img_upload'.$i.'_old']);
				}
				$this->conditions=array("location_id"=>$businessid);				
				$this->LocationImage->deleteAll($this->conditions);
				
				if(!empty($this->request->data["LocationImages"]))
				$this->LocationImage->saveAll($this->request->data["LocationImages"]);		
			
			
			
			
			if(isset($this->request->data['OpeningTiming']))
			{				
				
				for($i=1;$i<=7;$i++) {
					
					$this->request->data["OpeningTiming"][$i]["user_id"]=$this->Session->read('Auth.User.id');
				    $this->request->data["OpeningTiming"][$i]["location_id"]=$this->request->data["Location"]["businessid"];					
					$this->request->data["OpeningTiming"][$i]["days"]=$i;
					if(empty($this->request->data['OpeningTiming']["starttime_".$i]))
					{
						$this->request->data['OpeningTiming'][$i]["start_time"]='';
					}
					else{
						$this->request->data["OpeningTiming"][$i]["start_time"]=date("H:i:s",strtotime($this->request->data["OpeningTiming"]["starttime_".$i]));					
					}
					if(empty($this->request->data['OpeningTiming']["endtime_".$i]))
					{
						$this->request->data['OpeningTiming'][$i]["end_time"]='';
					}		
					else{				

					$this->request->data["OpeningTiming"][$i]["end_time"]=date("H:i:s",strtotime($this->request->data["OpeningTiming"]["endtime_".$i]));
					}
					unset($this->request->data["OpeningTiming"]["endtime_".$i]);
					unset($this->request->data["OpeningTiming"]["starttime_".$i]);
					
					$days[$i]=$i;				
				}
				//pr($this->request->data["OpeningTiming"]);die;
				$this->conditions_days = array_merge($this->conditions,array("days IN"=>$days));				
				
				$data= $this->OpeningTiming->find('all', array("conditions"=>$this->conditions_days));				  
				if(count($data)>0)
				{				
					$this->conditions=array_merge($this->conditions,array("days IN"=>$days));			
					$this->OpeningTiming->deleteAll($this->conditions);	
				}			
				
				$this->OpeningTiming->saveAll($this->request->data["OpeningTiming"]);				
			 }
			
			
			
			
			 //$this->redirect(SITE_LINK."b/".$this->request->data["Location"]["id"]."-".str_replace(" ","-",strtolower($this->request->data["Location"]['title'])));			
			 $this->redirect(SITE_LINK."locations");			

		}	
		$slug = explode("-",$slug);
		$slug = $slug[0];
		$this->request->data["Location"]["id"]= $slug;
		
		$options = array('conditions' => array('Location.id' => $this->request->data["Location"]["id"]));
		$business = $this->Location->find('first',$options);	
		//pr($business);		
		$this->set('business', $business);
		$this->set("title_for_layout",$business['Location']['title']." | Moola Rewards");
		$plan = $this->Plan->find('first',array("conditions"=>array("is_active"=>1)));	
		$this->set('plan', $plan);		
		
		$options = array('conditions' => array('location_id' => $this->request->data["Location"]["id"]),'order'=>'days');

		$data= $this->OpeningTiming->find('all', $options);		
		$this->set("data",$data);
		
		$location_images= $this->LocationImage->find('all', array('conditions' => array('location_id' => $this->request->data["Location"]["id"]),'fields'=>'id,image'));	
		$this->set("location_images",$location_images);
		
	}
	//Delete Location Images
	function delete_location_images() {
		if ( $this->request->is("ajax") ) {	
					
			if(!empty($this->request->query("id")))
			{	
				
				$this->loadModel("LocationImage");
				$message = "";				
				$busid="";	
				$options = array("conditions" => array("LocationImage." . $this->LocationImage->primaryKey => $this->request->query("id")));				
				if ($tmp = $this->LocationImage->find("first",$options) ) {			
					
					$this->LocationImage->id = $tmp["LocationImage"]["id"];	
					if(!empty($tmp["LocationImage"]["image"]) && file_exists(WWW_ROOT."img/location/".$tmp["LocationImage"]["image"]))
					{
						unlink(WWW_ROOT."img/location/".$tmp["LocationImage"]["image"]);
					}
					if ($this->LocationImage->delete()) {
						$message="Deleted";
					} else {
						$message="error";
					}
				}
				else
				{
					$message = "error";
				}
			}
			else
			{
				$message = "Invalid Request";
			}
			$result = array("status"=>true,"message"=>$message);
			echo json_encode($result);
			die;
		}
	}
	
	
	
 	function deleteuserbusiness() {
		if ( $this->request->is("ajax") ) {	
					
			if(!empty($this->request->query("id")))
			{		
				$message = "";				
				$busid="";	
				$options = array('conditions' => array('Location.' . $this->Location->primaryKey => $this->request->query("id")));				
				if ($tmp = $this->Location->find("first",$options) ) {				
					 $paypal_profile=$tmp["Location"]["payment_profile_id"];
					 $this->Location->id = $tmp['Location']['id'];	
					 $this->Location->delete();	
					 if(!empty($_SESSION["cart_items"][$tmp['Location']['id']])){
						$busid=$tmp['Location']['id'];
						unset($_SESSION["cart_items"][$busid]);	
						$_SESSION["cart_total_items"]-= 1;
					 }
					$this->loadModel("OpeningTiming");
					$this->loadModel("LocationImage");
					
					$openingtiming=$this->OpeningTiming->find("all",array("conditions"=>array("location_id"=>$this->request->query("id")),'fields' => array('id','location_id')));
					if(!empty($openingtiming))
					{		
							$this->conditions=array("location_id"=>$this->request->query("id"));		
							$this->OpeningTiming->deleteAll($this->conditions);
					}
					$locationimages=$this->LocationImage->find("all",array("conditions"=>array("location_id"=>$this->request->query("id")),'fields' => array('id','location_id','image')));
					if(!empty($locationimages))
					{		
						foreach($locationimages as $images)
						{
							if(!empty($images["LocationImage"]["image"]) && file_exists(WWW_ROOT."img/location/".$images["LocationImage"]["image"]))
							{
								unlink(WWW_ROOT."img/location/".$images["LocationImage"]["image"]);
							}							
						}						
						$this->conditions=array("location_id"=>$this->request->query("id"));		
						$this->LocationImage->deleteAll($this->conditions);
					}
					  //~ if(!empty($paypal_profile)){
						 //~ $options = array('conditions' => array('Location.user_id=' .$this->Session->read("Auth.User.id"),"is_paid"=>1));
						 //~ $location=$this->Location->find("all",$options);
						  //~ if(empty($location))
						  //~ {								  
								 //~ $result = $this->Paypal->change_subscription_status($paypal_profile,"Cancel");	
						  //~ }
					   //~ }
					 $message = "Deleted";
				}
				else
				{
					$message = "Invalid Location";
				}
			}
			else
				{
					$message = "Invalid Request";
				}
			$result = array("status"=>true,"message"=>$message);
			echo json_encode($result);
			die;
		}
	}
	
	function processcart() {
		if ( $this->request->is("ajax") ) {
			$data = $this->request->query;
			
			if ( $this->Session->read("cart_items") ) {
				if ( $_SESSION["cart_items"][$data['locid']]['is_view'] == 1 ) {
					$_SESSION["cart_items"][$data['locid']]['is_view'] = 0;
					$_SESSION["cart_total_items"] -= 1;
				} else {
					$_SESSION["cart_items"][$data['locid']]['is_view'] = 1;
					$_SESSION["cart_total_items"]+= 1;
				}
			}
		}
	}
	
/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index($limit=20) {
		$this->layout = "mooladesignadmin";
		$this->Location->recursive = 0;
		$url = "manage-locations";	
		$this->bulkactions();
		$this->set('records', array( '20' => '20', '30' => '30','40' => '40', '60' => '60','80' => '80','100' => '100'));
		if ( $this->request->is("get") ) {		
			isset($this->request->query["records"])?$limit=$this->request->query["records"]:'';
			
			/*if ( !empty($this->data['User']['searchval']) ) {
				$this->redirect(SITE_LINK.$url."?searchval=".$this->data['User']['searchval']);
			} else {
				$this->redirect(SITE_LINK."".$url);
			}*/
		}	
		$this->set('limit',$limit);	
		(isset($this->params["named"]["page"]))?$sno=(($this->params["named"]["page"]*$limit)-($limit-1)):$sno=1;
		$this->set('sno',$sno);
		if ( $this->request->query("searchval") && !empty($this->request->query("searchval")) ) {
			
			$this->set("searchval",$this->request->query("searchval"));
			$searchval = strtolower(trim($this->request->query("searchval")));
			$this->conditions = array("OR"=>array("LOWER(Location.title) like"=> "%".$searchval."%"));
		}
		if ( $this->request->is("post") ) {
			if ( !empty($this->data['Location']['searchval']) ) {
				$this->redirect(SITE_LINK.$url."?searchval=".$this->data['Location']['searchval']);
			} else {
				$this->redirect(SITE_LINK."".$url);
			}
		}
		//$this->paginate = array("limit"=>1);		
		$this->paginate = array("order"=>"Location.created desc","limit"=>$limit);
		$this->set('locations', $this->Paginator->paginate($this->conditions));
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		$this->layout = "mooladesignadmin";
		if (!$this->Location->exists($id)) {
			throw new NotFoundException(__('Invalid Location'));
		}
		$options = array('conditions' => array('Location.' . $this->Location->primaryKey => $id));
		$this->set('location', $this->Location->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		$this->layout = "mooladesignadmin";
		if ($this->request->is('post')) {
			$this->Location->create();
			if ($this->Location->save($this->request->data)) {
				$this->Flash->success(__('The Location has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The Location could not be saved. Please, try again.'));
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		$this->layout = "mooladesignadmin";
		if (!$this->Location->exists($id)) {
			throw new NotFoundException(__('Invalid Location'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Location->save($this->request->data)) {
				$this->Flash->success(__('The Location has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The Location could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Location.' . $this->Location->primaryKey => $id));
			$this->request->data = $this->Location->find('first', $options);
		}
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Location->id = $id;
		if (!$this->Location->exists()) {
			throw new NotFoundException(__('Invalid Location'));
		}
		
		if ($this->Location->delete()) {
			$this->Flash->success(__('The location has been deleted.'));
		} else {
			$this->Flash->error(__('The location could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
	
	
	
	public function admin_view_business($id=null,$limit=20)
	{
		$this->layout = "mooladesignadmin";
		$this->loadModel("User");
		$uid="";
		$this->Location->hasMany = $this->Location->hasOne = $this->Location->belongsTo = array();
		$this->Location->belongsTo = array(			
				"User"=>array(
				"className"=>"User",
				"foreignKey"=>false,
				"conditions"=> "User.id = Location.user_id"
			),		
				"UserDetail"=>array(
				"className" => "UserDetail",
				"foriegnKey" => false,
				"conditions" => "UserDetail.user_id = Location.user_id"
			)
		);
		$this->set('records', array( '20' => '20', '30' => '30','40' => '40', '80' => '80','100' => '100'));
		$this->bulkactions(true);
		$this->conditions ="";
		isset($this->request->query["records"])?$limit=$this->request->query["records"]:'';		
		$this->set('limit',$limit);	
		(isset($this->params["named"]["page"]))?$sno=(($this->params["named"]["page"]*$limit)-($limit-1)):$sno=1;
		
		if(isset($this->request->query["uid"]) && !empty($this->request->query["uid"]))
		{
			$this->conditions = array("Location.user_id"=>$this->request->query["uid"]);
			$options = array('conditions' => array('User.' .$this->User->primaryKey => $this->request->query["uid"]));
				$uid=$this->request->query["uid"];
		}
		
		$this->set('uid',$uid);	
		if ( $this->request->is("post") ) {
			
			
			if(isset($this->request->data["Location"]["userid"]))
			{
				if (!empty($this->data['Location']['searchval']) ) {
					$this->redirect(SITE_LINK."view_business?uid=".$this->request->data["Location"]["userid"]."&searchval=".$this->data['Location']['searchval']);
				} else {
					$this->redirect(SITE_LINK."view_business?uid=".$this->request->query["uid"]);
				}
			}
			else
			{
				if (!empty($this->data['Location']['searchval']) ) {
					$this->redirect(SITE_LINK."view_business?searchval=".$this->data['Location']['searchval']);
				} else {
					$this->redirect(SITE_LINK."view_business");
				}
			}
		}
			
		if ( $this->request->query("searchval") && !empty($this->request->query("searchval")) ) {
			$this->set("searchval",$this->request->query("searchval"));
			$searchval = strtolower(trim($this->request->query("searchval")));
			
			$this->conditions = array_merge($this->conditions,array("OR"=>array("LOWER(Location.address) like"=> "%".$searchval."%")));
		}
		
		$this->set('sno',$sno);			
		$this->paginate = array("order"=>"Location.created desc","limit"=>$limit);
		$tmp =$this->Paginator->paginate("Location",$this->conditions);	
		//pr($tmp);die;
		$this->set('businesses', $tmp);
		//$this->set('user', $this->User->find('first', $options));
	}
	public function admin_edit_business()
	{
		$this->layout = "mooladesignadmin";
		$this->loadModel("Location");		
				
		$this->jsArray[] = array("main","cropper.min","mybusinessvalidate");	
		$this->cssArray[] = array("cropper.min","main");

		if ($this->request->is(array('post', 'put'))) {		
			//pr($this->request->data);die;
			
			if (isset($this->request->data['Location']['tmpImage']) && !empty($this->request->data['Location']['tmpImage'])) {				
				$this->genImage($this->request->data['Location']['tmpImage'],"business");
				$this->request->data['Location']['image'] = $this->imagename;
			} else {
				unset($this->request->data['Location']['image']);
			}
			//pr($this->request->data);die;
			$this->request->data["Location"]["id"]= $this->request->data["Location"]["businessid"];
			if ($this->Location->save($this->request->data)) {
				$this->Flash->success(__('The Location has been saved.'));	
			    $this->redirect(SITE_LINK."view_business?uid=".$this->request->data["Location"]["userid"]);			
			} else {
				$this->Flash->error(__('The Location could not be saved. Please, try again.'));
			}
		}
		else
		{			
			if(isset($this->request->query["bid"]))
			{
				$options = array('conditions' => array('Location.' . $this->Location->primaryKey => $this->request->query["bid"]));
				$business = $this->Location->find('first',$options);				
				if(!empty($business))
				{	
					$this->set('business', $business);				
				} 
				else {
					$this->Flash->error(__('Invalid Location. Please, try again.'));
					$this->redirect(SITE_LINK."manage-merchants");
				}
				
			}
			else 
			{
				$this->Flash->error(__('Invalid  Request. Please, try again.'));
			 	$this->redirect(SITE_LINK."manage-merchants");
			}
		}
	}

}

