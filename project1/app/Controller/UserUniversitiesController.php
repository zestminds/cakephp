<?php
App::uses('AppController', 'Controller');
/**
 * UserUniversities Controller
 *
 * @property UserUniversity $UserUniversity
 * @property PaginatorComponent $Paginator
 */
class UserUniversitiesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->UserUniversity->recursive = 0;
		$this->set('userUniversities', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->UserUniversity->exists($id)) {
			throw new NotFoundException(__('Invalid user university'));
		}
		$options = array('conditions' => array('UserUniversity.' . $this->UserUniversity->primaryKey => $id));
		$this->set('userUniversity', $this->UserUniversity->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->UserUniversity->create();
			if ($this->UserUniversity->save($this->request->data)) {
				$this->Flash->success(__('The user university has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The user university could not be saved. Please, try again.'));
			}
		}
		$users = $this->UserUniversity->User->find('list');
		$universities = $this->UserUniversity->University->find('list');
		$this->set(compact('users', 'universities'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->UserUniversity->exists($id)) {
			throw new NotFoundException(__('Invalid user university'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->UserUniversity->save($this->request->data)) {
				$this->Flash->success(__('The user university has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The user university could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('UserUniversity.' . $this->UserUniversity->primaryKey => $id));
			$this->request->data = $this->UserUniversity->find('first', $options);
		}
		$users = $this->UserUniversity->User->find('list');
		$universities = $this->UserUniversity->University->find('list');
		$this->set(compact('users', 'universities'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->UserUniversity->id = $id;
		if (!$this->UserUniversity->exists()) {
			throw new NotFoundException(__('Invalid user university'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->UserUniversity->delete()) {
			$this->Flash->success(__('The user university has been deleted.'));
		} else {
			$this->Flash->error(__('The user university could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->layout = "mooladesignadmin";
		$this->UserUniversity->recursive = 0;
		$this->set('userUniversities', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		$this->layout = "mooladesignadmin";
		if (!$this->UserUniversity->exists($id)) {
			throw new NotFoundException(__('Invalid user university'));
		}
		$options = array('conditions' => array('UserUniversity.' . $this->UserUniversity->primaryKey => $id));
		$this->set('userUniversity', $this->UserUniversity->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		$this->layout = "mooladesignadmin";
		if ($this->request->is('post')) {
			$this->UserUniversity->create();
			if ($this->UserUniversity->save($this->request->data)) {
				$this->Flash->success(__('The user university has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The user university could not be saved. Please, try again.'));
			}
		}
		$users = $this->UserUniversity->User->find('list');
		$universities = $this->UserUniversity->University->find('list');
		$this->set(compact('users', 'universities'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		$this->layout = "mooladesignadmin";
		if (!$this->UserUniversity->exists($id)) {
			throw new NotFoundException(__('Invalid user university'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->UserUniversity->save($this->request->data)) {
				$this->Flash->success(__('The user university has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The user university could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('UserUniversity.' . $this->UserUniversity->primaryKey => $id));
			$this->request->data = $this->UserUniversity->find('first', $options);
		}
		$users = $this->UserUniversity->User->find('list');
		$universities = $this->UserUniversity->University->find('list');
		$this->set(compact('users', 'universities'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
	
		$this->UserUniversity->id = $id;
		if (!$this->UserUniversity->exists()) {
			throw new NotFoundException(__('Invalid user university'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->UserUniversity->delete()) {
			$this->Flash->success(__('The user university has been deleted.'));
		} else {
			$this->Flash->error(__('The user university could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
