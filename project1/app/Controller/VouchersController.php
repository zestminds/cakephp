<?php
header('X-Frame-Options: GOFORIT'); 
App::uses('AppController', 'Controller');
/**
 * Vouchers Controller
 *
 * @property Voucher $Voucher
 * @property PaginatorComponent $Paginator
 */
class VouchersController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');
	
	function beforefilter(){
		parent::beforefilter();
		$this->Auth->allow("getdate","merchant_detail","getbizzdetail");
	}

	function getbizzdetail() {
		$this->autoRender = false;
		$val = $this->request->query["bizz_ids"];
		if ( !empty($val) ) {
			$vals = explode(",",$val);
			$this->loadModel("Location");
			$business = $this->Location->find("list",array("conditions"=>array("id"=>$vals,"is_paid is null or is_paid = ''"),"fields"=>array("id","title")));
			$result = array("flag"=>0);
			if(!empty($business)) {
				$businessNames = implode(", ",$business);
				$message = "Payment method for business <b>".$businessNames."</b> have not been verified yet, You still can publish your voucher but will not be displayed for students till you will make payment.";
				$result = array("flag"=>1,"message"=>$message);
			} 
		}
		echo json_encode($result);
		die;
	}
	
	
/**
 * index method
 *
 * @return void
 */
	public function index($publish_status = 1,$id = NULL) { 
		$this->layout = "mooladesignmyvoucher";
		$this->jsArray[] = array("myvoucher","bootbox");	
		$this->Voucher->recursive = -1;
		$this->loadModel('Location');
		$location = $this->Location->find("first",array("conditions"=>array("user_id"=>$this->Session->read('Auth.User.id'))));			
		$this->set(compact('location'));		
		$this->loadModel('UserDetail');
		$this->loadModel('User');
		$userdetails = $this->UserDetail->find("first",array("conditions"=>array("user_id"=>$this->Session->read('Auth.User.id'))));
		$flag = false;
		if ( $tmpUser = $this->User->find("first",array("conditions"=>array("id"=>$this->Session->read('Auth.User.id'),"is_login"=>0),"recursive"=>-1)) ) {
			$this->set("is_login",1);
			$tmp['User']['is_login'] = 1;
			$this->User->create();
			$this->User->id = $this->Session->read('Auth.User.id');
			$this->User->save($tmp,array("validate"=>false));
			$flag = true;
		}
		//$flag = true;
		$this->set(compact('userdetails'));		
		
		if (empty($id)) {
			$this->conditions[] = array("voucher_status"=>$publish_status,"user_id"=>$this->Session->read("Auth.User.id"));
		} else {
			$this->conditions[] = array("voucher_status"=>$publish_status,"user_id"=>$this->Session->read("Auth.User.id"),"id"=>$id);
		}
		
		if( $this->request->is("ajax")) {
			$this->layout = false;
			$this->render = false;
			$this->paginate = array("order"=>"Voucher.modified desc","limit"=>"16","fields"=>array("id","image"));
			$data = $this->Paginator->paginate($this->conditions);
			$tmpData = array();
			foreach( $data as $key=>$val  ) {
				$tmpData[] = $val['Voucher'];
			}
			echo json_encode($tmpData);	
			die;		
		} else {
			$this->paginate = array("order"=>"Voucher.modified desc","limit"=>"16");
			$this->set('vouchers_draft', $this->Paginator->paginate($this->conditions));
			$this->set("status",$publish_status);
		}
		if( $flag ) {
			$this->layout = false;
			$this->render("video");
		} 
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->layout = false;
		
		$this->Voucher->hasMany = $this->Voucher->belongsTo = $this->Voucher->hasOne = array();
		$this->Voucher->belongsTo = array(
				"UserDetail" => array(
					"className" => "UserDetail",
					"foreignKey" => false,
					"type" => "Inner",
					"conditions"=>"UserDetail.user_id=Voucher.user_id"
				),
				"Location" => array(
					"className" => "Location",
					"foreignKey" => false,
					"type" => "Inner",
					"conditions" => "UserDetail.user_id = Location.user_id"
				)
			); 
			
			
		if (!$this->Voucher->exists($id)) {
			throw new NotFoundException(__('Invalid voucher'));
		}
		
		$options = array('conditions' => array('Voucher.' . $this->Voucher->primaryKey => $id));
		$tmpData =$this->Voucher->find('first', $options);
		
		$voucher = $tmpData;
		
		$this->set(compact("voucher"));
		$s_date=$voucher["Voucher"]["start_date"];
		$e_date=$voucher["Voucher"]["end_date"];
		$s_time=$voucher["Voucher"]["start_hour"];
		$e_time=$voucher["Voucher"]["end_hour"];
		$w_days=$voucher["Voucher"]["repeat_days"];		
		$hours_left=$this->getdatevoucher($s_date,$e_date,$s_time,$e_time,$w_days);			
		$this->set("hours_left",$hours_left["message"]);
		$data = array($voucher,"message"=>$hours_left);
		echo json_encode($data);
		die;
	}

/**
 * Perview method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function perview($id = null) {
		$this->layout = false;
		if (!$this->Voucher->exists($id)) {
			throw new NotFoundException(__('Invalid voucher'));
		}
		$options = array('conditions' => array('Voucher.' . $this->Voucher->primaryKey => $id));
		$tmpData =$this->Voucher->find('first', $options);
		$dayIntArr = explode(",",$tmpData['Voucher']['repeat_days']);
		$dayArr = array(1=>"Monday",2=>"Tuesday",3=>"Wednesday",4=>"Thursday",5=>"Friday",6=>"Saturday",7=>"Sunday");
		$voucher = $tmpData;
		$this->set(compact("voucher","dayArr","dayIntArr"));
	}

/**
 * add method
 *$this->jsArray[] = array("myvoucher","bootbox","jquery.canvasCrop");	
		$this->cssArray[] = array("croper/style","croper/canvasCrop");	
 * @return void
 */
	public function add() {
				
		$this->layout = "mooladesigncreatevoucher";			
		$this->jsArray[] = array("createVoucher","cropper.min","main");	
		$this->cssArray[] = array("cropper.min","main");		
		
		//$this->loadModel('UserCategory');				
		//$user_category_exits = $this->UserCategory->find("first",array("conditions"=>array("user_id"=>$this->Session->read('Auth.User.id'))));
		
		 if ($this->request->is('post')) {
			
			
			//$business_list=$this->request->data["Voucher"]["business"];
			$category_list=explode(",",$this->request->data["Voucher"]["category_id"]);			
			
			$this->Voucher->hasMany = $this->Voucher->belongsTo = $this->Voucher->hasOne = array();
			$this->Voucher->hasMany = array(
				
				"VoucherCategory" => array(
					"className" => "VoucherCategory",
					"foreignKey" => "voucher_id",
					"type" => "Inner"
				)
			); 
				
			if (!empty($this->request->data['Voucher']['start_hour'])) {
				 $tmpStart = date('Y-m-d H:i:s',strtotime(($this->request->data['Voucher']['start_date'].' '.$this->request->data['Voucher']['start_hour'])));
			} else {
				 $tmpStart = date('Y-m-d 00:00:00',strtotime(($this->request->data['Voucher']['start_date'].' '.$this->request->data['Voucher']['start_hour'])));
			}
			if (!empty($this->request->data['Voucher']['end_hour'])) {
				 $tmpEnd = date('Y-m-d H:i:s',strtotime(($this->request->data['Voucher']['end_date'].' '.$this->request->data['Voucher']['end_hour'])));
			} else {
				 $tmpEnd = date('Y-m-d 23:59:59',strtotime(($this->request->data['Voucher']['end_date'].' '.$this->request->data['Voucher']['end_hour'])));
			}		
			
			 $this->request->data['Voucher']['voucher_start_date']= $tmpStart;
			 $this->request->data['Voucher']['voucher_end_date']= $tmpEnd;
			
			 $this->request->data['Voucher']['day_status']=$this->request->data['Voucher']['VouchDayStatus'];

			if($this->request->data['Voucher']['repeat_on']=='W' && !empty($this->request->data['Voucher']['repeat_days']))
			$this->request->data['Voucher']['repeat_days']=implode(",",$this->request->data['Voucher']['repeat_days']);		
			else	
			$this->request->data['Voucher']['repeat_days']="";
			
			if($this->request->data['Voucher']["title_status"]=="D"){
				if (strpos( $this->request->data['Voucher']["title_discount"],"%") != true) {
					$this->request->data['Voucher']["title_discount"]=$this->request->data['Voucher']["title_discount"]."%";
				}
			}
			if (isset($this->request->data['Voucher']['tmpImage']) && !empty($this->request->data['Voucher']['tmpImage'])) {
				
				$this->genImage($this->request->data['Voucher']['tmpImage'],"voucher");
				$this->request->data['Voucher']['image'] = $this->imagename;
			} else {
				$this->request->data['Voucher']['image'] = "";
			}
			$this->request->data['Voucher']['is_active']=1;
			if(!isset($user_category_exits["Category"]["id"]))
			{
				//$this->request->data['Voucher']['voucher_status']=0;
			}			
			
			$url = ($this->request->data['Voucher']['voucher_status']==0)?"drafted-vouchers":"published-vouchers";	
			
			if($url=="published-vouchers" && $this->request->data['Voucher']['voucher_status']==3)
			{
				$url="locations";
				$this->request->data['Voucher']['voucher_status']=0;
			}
			$this->request->data['Voucher']['online_status'] = empty($this->request->data['Voucher']['online_status'])?"on":"off";
			//pr($this->request->data); die;
			$this->Voucher->create();
			if ($this->Voucher->save($this->request->data)) {
				//$vouid=$this->Voucher->getLastInsertID();
				$this->request->data['VoucherCategory']['voucher_id']=$this->Voucher->getLastInsertID();	
				if($url=="locations")
				{$this->Session->write("vouc_id",$this->Voucher->getLastInsertID());}
						
				foreach($category_list as $cate_id)	
				{	
					$this->request->data['VoucherCategory']['category_id']=$cate_id;					
					$this->Voucher->VoucherCategory->saveAll($this->request->data);					
					
				}
				$this->Flash->success(__('The voucher has been saved.'));
				 //~ if ($this->request->data['Voucher']['voucher_status'] ==1)  {
					 //~ return $this->redirect(SITE_LINK."my_cart");
				//~ } else {
			 
					return $this->redirect(SITE_LINK."".$url);
				 //~ }
			} else {
				$this->Flash->error(__('The voucher could not be saved. Please, try again.'));
			}
		}
		else{			
			$this->loadModel('Location');
			$location = $this->Location->find("first",array("conditions"=>array("user_id"=>$this->Session->read('Auth.User.id'))));			
			$this->set(compact('location'));
			$users = $this->Voucher->User->find('list');
			$this->set('optionsTitle', array( 'D' => 'Default', 'C' => 'Custom'));
			$this->set('optionsSubtitle', array( 'D' => 'Default', 'C' => 'Custom'));
			$this->set('optionsDesc', array( 'D' => 'Default', 'C' => 'Custom'));
			$this->set('optionsTerms', array( 'D' => 'Default', 'C' => 'Custom'));
			$this->set('optionsAllday', array( '1' => 'Yes', '0' => 'No'));
			$this->set('optionsRepeat', array( 'D' => 'Daily', 'W' => 'Weekly'));
			$this->set('optionsWeekday', array( '1' => 'M', '2' => 'T','3' => 'W', '4' => 'T','5' => 'F', '6' => 'S', '7' => 'S'));				
			$this->loadModel("DefaultValue");
			$defaultvalue=$this->DefaultValue->find("all",array("conditions"=>array("is_active"=>1)));		
			$this->set(compact('defaultvalue'));
			$this->set(compact('arrMin'));
			$this->set(compact('arrHour'));
			$user_id=$this->Session->read('Auth.User.id');
			$this->set(compact('user_id'));
			$this->loadModel("UserDetail");			
			$userdetails= $this->UserDetail->find("first",array("conditions"=>array("user_id"=>$this->Session->read('Auth.User.id'))));
		
			$this->set('userdetails',$userdetails);	
			$defaultdescription=str_replace("<Merchant Name>",$userdetails["UserDetail"]["business"],$defaultvalue[2]["DefaultValue"]["value"]);
			$this->set(compact('defaultdescription'));
			$this->set(compact('defaultvalue'));
			
			//User Business	

			$business_list_active = $this->Location->find("first",array("conditions"=>array("user_id"=>$this->Session->read('Auth.User.id'),"active"=>1)));
			//pr($business_list_active);
			$this->set(compact('business_list_active'));		
	}
}
/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {		
		
		$this->layout = "mooladesigncreatevoucher";
		$this->loadModel('Location');
		$location = $this->Location->find("first",array("conditions"=>array("user_id"=>$this->Session->read('Auth.User.id'))));			
		$this->set(compact('location'));	
		$this->jsArray[] = array("editvouchervalidate","cropper.min","main");	
		$this->cssArray[] = array("cropper.min","main");
		$this->Voucher->hasMany = $this->Voucher->belongsTo = $this->Voucher->hasOne = array();
		$this->Voucher->hasMany	 = array(			
			"VoucherCategory" => array(
				"className" => "VoucherCategory",
				"foreignKey" => "voucher_id",
				"fields" => array("VoucherCategory.category_id","(select c.title from categories c where c.id = VoucherCategory.category_id) as cname")					
				
			)
		); 
		 
		
		
		if (!$this->Voucher->exists($id)) {
			throw new NotFoundException(__('Invalid voucher'));	
		}
		
		if ($this->request->is(array('post', 'put'))) {
			
			
			$category_list=explode(",",$this->request->data["Voucher"]["category_id"]);
			//pr($this->request->data);die;
			if (!empty($this->request->data['Voucher']['start_hour'])) {
				 $tmpStart = date('Y-m-d H:i:s',strtotime(($this->request->data['Voucher']['start_date'].' '.$this->request->data['Voucher']['start_hour'])));
			} else {
				 $tmpStart = date('Y-m-d 00:00:00',strtotime(($this->request->data['Voucher']['start_date'].' '.$this->request->data['Voucher']['start_hour'])));
			}
			if (!empty($this->request->data['Voucher']['end_hour'])) {
				 $tmpEnd = date('Y-m-d H:i:s',strtotime(($this->request->data['Voucher']['end_date'].' '.$this->request->data['Voucher']['end_hour'])));
			} else {
				 $tmpEnd = date('Y-m-d 23:59:59',strtotime(($this->request->data['Voucher']['end_date'].' '.$this->request->data['Voucher']['end_hour'])));
			}		
			
			 $this->request->data['Voucher']['voucher_start_date']= $tmpStart;
			 $this->request->data['Voucher']['voucher_end_date']= $tmpEnd;
			 
			$this->request->data['Voucher']['day_status']=$this->request->data['Voucher']['VouchDayStatus'];
			if($this->request->data['Voucher']['repeat_on']=='W')
			$this->request->data['Voucher']['repeat_days']=implode(",",$this->request->data['Voucher']['repeat_days']);		
			else	
			$this->request->data['Voucher']['repeat_days']="";	
						
			if (isset($this->request->data['Voucher']['tmpImage']) && !empty($this->request->data['Voucher']['tmpImage'])) {
				$this->genImage($this->request->data['Voucher']['tmpImage'],"voucher");	
				$this->request->data['Voucher']['image'] = $this->imagename;
			} else {
				$this->request->data['Voucher']['image'] = $this->request->data['Voucher']['image_old'];
			}		
			if(!isset($user_category_exits["Category"]["id"]))
			{
				//$this->request->data['Voucher']['voucher_status']=0;
			}			
			
			$url = ($this->request->data['Voucher']['voucher_status']==0)?"drafted-vouchers":"published-vouchers";			
			
			
			if($url=="published-vouchers" && $this->request->data['Voucher']['voucher_status']==3)
			{
				$url="locations";
				$this->request->data['Voucher']['voucher_status']=0;
				$this->Session->write("vouc_id",$this->request->data['Voucher']['id']);	
			}
			if($this->request->data['Voucher']["title_status"]=="D"){
				if (strpos( $this->request->data['Voucher']["title_discount"],"%") != true) {
					$this->request->data['Voucher']["title_discount"]=$this->request->data['Voucher']["title_discount"]."%";
				}
			}
			$this->request->data['Voucher']['online_status'] = empty($this->request->data['Voucher']['online_status'])?"on":"off";
			if ($this->Voucher->save($this->request->data)) {
				
				if($this->Voucher->VoucherCategory->hasAny(array('voucher_id' => $this->request->data["Voucher"]["id"])))
				{
					$this->Voucher->VoucherCategory->deleteAll(array('voucher_id' => $this->request->data["Voucher"]["id"]));
				}
				
				$this->request->data['VoucherCategory']['voucher_id']=$this->request->data["Voucher"]["id"];	
				foreach($category_list as $cate_id)	
				{	
					$this->request->data['VoucherCategory']['category_id']=$cate_id;					
					$this->Voucher->VoucherCategory->saveAll($this->request->data);					
					
				}
				$this->Flash->success(__('The voucher has been saved.'));
				
				// if ($this->request->data['Voucher']['voucher_status'] && !$this->Session->read("Auth.User.is_paid") ) {
					// return $this->redirect(SITE_LINK."manage-billing");
				// } else {
					return $this->redirect(SITE_LINK."".$url);
				//}
			} else {
				$this->Flash->error(__('The voucher could not be saved. Please, try again.'));
			}
		
		}
		else {		
			$this->set('optionsTitle', array( 'D' => 'Default', 'C' => 'Custom'));
			$this->set('optionsSubtitle', array( 'D' => 'Default', 'C' => 'Custom'));
			$this->set('optionsDesc', array( 'D' => 'Default', 'C' => 'Custom'));
			$this->set('optionsTerms', array( 'D' => 'Default', 'C' => 'Custom'));
			$this->set('optionsAllday', array( '1' => 'Yes', '0' => 'No'));		
			$this->set('optionsRepeat', array( 'D' => 'Daily', 'W' => 'Weekly'));
			$this->set('optionsWeekday', array( '1' => 'M', '2' => 'T','3' => 'W', '4' => 'T','5' => 'F', '6' => 'S', '7' => 'S'));
						
			$this->loadModel("DefaultValue");		
			$defaultvalue = $this->DefaultValue->find("all");					
					
			$user_id=$this->Session->read('Auth.User.id');
			$this->set(compact('user_id'));
			$options = array('conditions' => array('Voucher.' . $this->Voucher->primaryKey => $id));
			$tmpData =$this->Voucher->find('first', $options);
			$this->request->data = $tmpData;
			$tmpData['Voucher']['repeat_days'] = explode(",",$tmpData['Voucher']['repeat_days']);
		    $this->request->data = $tmpData;
			if(!empty($tmpData['Voucher']['tags']))		{	
				$tmpData['Voucher']['tags_list'] = explode(",",$tmpData['Voucher']['tags']);	
			}
			
			if($tmpData['Voucher']["title_status"]=="D"){				
					$tmpData['Voucher']["title_discount"]=str_replace("%","",$this->request->data['Voucher']["title_discount"]);
					$this->set('voucher_discount',$tmpData['Voucher']["title_discount"]);			
			}
			else
			$this->set('voucher_discount',$tmpData['Voucher']["title_discount"]);
										
			$vouchercat=array();			
			foreach($tmpData["VoucherCategory"] as $key=>$val)
			{
				$vouchercat[]=$val["category_id"];
			}
			$tmpData['Voucher']["voucher_cate_id"]=implode(",",$vouchercat);
			
						
			$voucher = $tmpData;
			
			$this->set(compact('voucher'));
			
			//pr($voucher);
			$this->loadModel("UserDetail");	
			$userdetails= $this->UserDetail->find("first",array("conditions"=>array("user_id"=>$this->Session->read('Auth.User.id'))));
			$this->set('userdetails',$userdetails);	
			
			
			$defaultvalue=$this->DefaultValue->find("all",array("conditions"=>array("is_active"=>1)));
			//pr($defaultvalue[2]);				
			$this->set(compact('defaultvalue'));				
		    $defaultdescription=str_replace("<Merchant Name>",$userdetails["UserDetail"]["business"],$defaultvalue[2]["DefaultValue"]["value"]);
			$this->set(compact('defaultdescription'));
			//User Business	

			$business_list_active = $this->Location->find("list",array("conditions"=>array("user_id"=>$this->Session->read('Auth.User.id'),"active"=>1)));
			$this->set(compact('business_list_active'));						
			
		}
		
		//pr($voucher);
}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null,$publish_status=null) {
		$voucher = $this->Voucher->find("first",array("conditions"=>array("id"=>$id,"user_id"=>$this->Auth->user("id")),"recursive"=>-1));
		if (empty($voucher)) {
			throw new NotFoundException(__('Invalid voucher'));
		}
		$publish_status = $voucher['Voucher']['voucher_status'];
		$this->Voucher->id = $id;
		if ($this->Voucher->delete()) {
			$this->Flash->success(__('The voucher has been deleted.'));
		} else {
			$this->Flash->error(__('The voucher could not be deleted. Please, try again.'));
		}
		$url = ($publish_status=="0")?"drafted-vouchers":"published-vouchers";
		return $this->redirect(SITE_LINK.$url);
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index($limit=20) {		
		$this->set('records', array( '20' => '20', '30' => '30','40' => '40', '80' => '80','100' => '100'));
		if ( $this->request->is("get") ) {		
			isset($this->request->query["records"])?$limit=$this->request->query["records"]:20;
			
		}	
		$this->bulkactions(true);
		if ( $this->request->is("post") ) {
			
			if ( !empty($this->data['Voucher']['searchval']) ) {
				$url = SITE_LINK."view-vouchers";
				if ( $this->request->query["merchant"] && !empty($this->request->query["merchant"]) ) {
					$url .= "?merchant=".$this->request->query["merchant"]."&";
				} else {
					$url .= "?";
				}
				$this->redirect($url."searchval=".$this->data['Voucher']['searchval']);
			} else {
				$this->redirect(SITE_LINK."view-vouchers");
			}
		}
		$this->set('limit',$limit);	
		
		(isset($this->params["named"]["page"]))?$sno=(($this->params["named"]["page"]*$limit)-($limit-1)):$sno=1;
		$this->layout = "mooladesignadmin";
		$this->Voucher->hasMany = $this->Voucher->hasOne = $this->Voucher->belongsTo = array();
		$this->Voucher->belongsTo = array(
			"UserDetail"=>array(
				"className" => "UserDetail",
				"foriegnKey" => false,
				"conditions" => "UserDetail.user_id = Voucher.user_id"
			)
			);
			$url="view-vouchers";		
				
		if ( $this->request->query("searchval") && !empty($this->request->query("searchval")) ) {	
		
			$this->set("searchval",$this->request->query("searchval"));
			$searchval = strtolower(trim($this->request->query("searchval")));
			$this->conditions = array("OR"=>array("LOWER(Location.address) like"=> "%".$searchval."%","LOWER(Voucher.title_discount) like"=> "%".$searchval."%","LOWER(Location.title) like"=> "%".$searchval."%"));
		}
	
		if ( $this->request->query("merchant") && !empty($this->request->query("merchant"))) {
			$this->conditions = array_merge($this->conditions,array("Voucher.user_id"=>$this->request->query("merchant")));
		}		
		
		$this->set('sno',$sno);		
	    $this->paginate = array("order"=>"Voucher.created desc","limit"=>$limit);
	   	$vouchers=$this->Paginator->paginate($this->conditions);
	   //pr($vouchers);die;
	   $this->set('vouchers', $vouchers);

	}

	
/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		$this->layout = "mooladesignadmin";
		if (!$this->Voucher->exists($id)) {
			throw new NotFoundException(__('Invalid voucher'));
		}
		
		$options = array('conditions' => array('Voucher.' . $this->Voucher->primaryKey => $id));
		$tmpData =$this->Voucher->find('first', $options);
		$dayIntArr = explode(",",$tmpData['Voucher']['repeat_days']);
		$dayArr = array(1=>"Monday",2=>"Tuesday",3=>"Wednesday",4=>"Thursday",5=>"Friday",6=>"Saturday",7=>"Sunday");
		$voucher = $tmpData;
		$this->set(compact("voucher","dayArr","dayIntArr"));		 
		//$this->set('voucher', $this->Voucher->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Voucher->create();
			if ($this->Voucher->save($this->request->data)) {
				$this->Flash->success(__('The voucher has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The voucher could not be saved. Please, try again.'));
			}
		}
		$users = $this->Voucher->User->find('list');
		$this->set(compact('users'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		$this->layout = "mooladesignadmin";
		//$this->jsArray[] = array("createVoucher","jquery.awesome-cropper","jquery.imgareaselect");			
		$this->jsArray[] = array("jquery-ui.min","editvoucheradmin","cropper.min","main");			
		//$this->cssArray[] = array("imgareaselect-default","jquery.awesome-cropper");	
		$this->cssArray[] = array("cropper.min","main");
		if (!$this->Voucher->exists($id)) {
			throw new NotFoundException(__('Invalid voucher'));
		}
		
		$this->Voucher->hasMany = $this->Voucher->belongsTo = $this->Voucher->hasOne = array();
		$this->Voucher->hasMany	 = array(			
			"VoucherCategory" => array(
				"className" => "VoucherCategory",
				"foreignKey" => "voucher_id",
				"fields" => array("VoucherCategory.category_id","(select c.title from categories c where c.id = VoucherCategory.category_id) as cname")					
				
			)
		); 	
		
		if ($this->request->is(array('post', 'put'))) {	
			$category_list=explode(",",$this->request->data["Voucher"]["category_id"]);
				
			if (!empty($this->request->data['Voucher']['start_hour']) && empty($this->request->data['Voucher']['VouchDayStatus'])) {
				 $tmpStart = date('Y-m-d H:i:s',strtotime(($this->request->data['Voucher']['start_date'].' '.$this->request->data['Voucher']['start_hour'])));
			} else {
				 $tmpStart = date('Y-m-d 00:00:00',strtotime(($this->request->data['Voucher']['start_date'].' '.$this->request->data['Voucher']['start_hour'])));
			}
			if (!empty($this->request->data['Voucher']['end_hour']) && empty($this->request->data['Voucher']['VouchDayStatus'])) {
				 $tmpEnd = date('Y-m-d H:i:s',strtotime(($this->request->data['Voucher']['end_date'].' '.$this->request->data['Voucher']['end_hour'])));
			} else {
				 $tmpEnd = date('Y-m-d 23:59:59',strtotime(($this->request->data['Voucher']['end_date'].' '.$this->request->data['Voucher']['end_hour'])));
			}		
			
			 $this->request->data['Voucher']['voucher_start_date']= $tmpStart;
			 $this->request->data['Voucher']['voucher_end_date']= $tmpEnd;
			$this->request->data['Voucher']['day_status']=$this->request->data['Voucher']['VouchDayStatus'];

		
			 $startdate=date('Y-m-d',strtotime($this->request->data['Voucher']['start_date'])); 			
			 $starthour=date("H:i:s", strtotime($this->request->data['Voucher']['start_hour']));
			 $enddate=date('Y-m-d',strtotime($this->request->data['Voucher']['end_date'])); 			
			 $endhour=date("H:i:s", strtotime($this->request->data['Voucher']['end_hour']));	
			if($this->request->data['Voucher']['repeat_on']=='W')
			$this->request->data['Voucher']['repeat_days']=implode(",",$this->request->data['Voucher']['repeat_days']);		
			else	
			$this->request->data['Voucher']['repeat_days']="";
					
			//Image upload check if new then update otherwise update image name from hidden field wchich is aleardy added
			if (isset($this->request->data['Voucher']['tmpImage']) && !empty($this->request->data['Voucher']['tmpImage'])) {
				$this->genImage($this->request->data['Voucher']['tmpImage'],"voucher");	
				$this->request->data['Voucher']['image'] = $this->imagename;
			} else {
				$this->request->data['Voucher']['image'] = $this->request->data['Voucher']['image_old'];
			}		
						
			//$this->request->data['Voucher']['voucher_status']=isset($this->request->data['save'])?0:1;
			
			if($this->request->data['Voucher']["title_status"]=="D"){
				if (strpos( $this->request->data['Voucher']["title_discount"],"%") != true) {
					$this->request->data['Voucher']["title_discount"]=$this->request->data['Voucher']["title_discount"]."%";
				}
			}
			$this->request->data['Voucher']['start_date']=date('Y-m-d',strtotime($this->request->data['Voucher']['start_date']));  	
			$this->request->data['Voucher']['end_date']=date('Y-m-d',strtotime($this->request->data['Voucher']['end_date']));		
			
			//pr($this->request->data);die;
			if ($this->Voucher->save($this->request->data)) {				
				if($this->Voucher->VoucherCategory->hasAny(array('voucher_id' => $this->request->data["Voucher"]["id"])))
				{
					$this->Voucher->VoucherCategory->deleteAll(array('voucher_id' => $this->request->data["Voucher"]["id"]));
				}
				
				$this->request->data['VoucherCategory']['voucher_id']=$this->request->data["Voucher"]["id"];	
				foreach($category_list as $cate_id)	
				{	
					$this->request->data['VoucherCategory']['category_id']=$cate_id;					
					$this->Voucher->VoucherCategory->saveAll($this->request->data);					
					
				}
				$this->Flash->success(__('The voucher has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The voucher could not be saved. Please, try again.'));
			}		
		} else {
			
			$this->set('optionsSubtitle', array( 'D' => 'Default', 'C' => 'Custom'));
			$this->set('optionsTitle', array( 'D' => 'Default', 'C' => 'Custom'));
			$this->set('optionsDesc', array( 'D' => 'Default', 'C' => 'Custom'));
			$this->set('optionsTerms', array( 'D' => 'Default', 'C' => 'Custom'));
			$this->set('optionsAllday', array( '1' => 'Yes', '0' => 'No'));		
			$this->set('optionsRepeat', array( 'D' => 'Daily', 'W' => 'Weekly'));
			$this->set('optionsWeekday', array( '1' => 'Mon', '2' => 'Tue','3' => 'Wed', '4' => 'Thur','5' => 'Fri', '6' => 'Sat', '7' => 'Sun'));			
			
			
			
			$this->set(compact('arrMin'));
			$this->set(compact('arrHour'));			
			$options = array('conditions' => array('Voucher.' . $this->Voucher->primaryKey => $id));
			$tmpData =$this->Voucher->find('first', $options);
			$tmpData['Voucher']['repeat_days'] = explode(",",$tmpData['Voucher']['repeat_days']);
			
			if(!empty($tmpData['Voucher']['tags']))		{	
				$tmpData['Voucher']['tags_list'] = explode(",",$tmpData['Voucher']['tags']);
			}
						
			
			if($tmpData['Voucher']["title_status"]=="D"){				
					$tmpData['Voucher']["title_discount"]=str_replace("%","",$tmpData['Voucher']["title_discount"]);
					$this->set('voucher_discount',$tmpData['Voucher']["title_discount"]);			
			}
			else
			$this->set('voucher_discount',$tmpData['Voucher']["title_discount"]);
			
			
			$vouchercat=array();			
			foreach($tmpData["VoucherCategory"] as $key=>$val)
			{
				$vouchercat[]=$val["category_id"];
			}
			$tmpData['Voucher']["voucher_cate_id"]=implode(",",$vouchercat);
					
			$this->request->data = $tmpData;
			$voucher= $tmpData;
			$this->set(compact('voucher'));
			
			$this->loadModel("Location");					
			$location = $this->Location->find("first",array("conditions"=>array("Location.user_id"=>$voucher["Voucher"]["user_id"])));				
			$this->set('location',$location);	
			
			$this->loadModel("UserDetail");					
			$userdetails = $this->UserDetail->find("first",array("conditions"=>array("UserDetail.user_id"=>$voucher["Voucher"]["user_id"])));
			$this->set('userdetails',$userdetails);	
			
			
			$this->loadModel("DefaultValue");		
			$defaultvalue= $this->DefaultValue->find("all",array("conditions"=>array("is_active"=>1)));	
			//$defaultvalue=$this->request->data;
			$this->set(compact('defaultvalue'));							
		    $defaultdescription=str_replace("<Merchant Name>",$userdetails["UserDetail"]["business"],$defaultvalue[2]["DefaultValue"]["value"]);
			$this->set(compact('defaultdescription'));
			//pr($tmpData);die;
		}
		//$users = $this->Voucher->User->find('list');
		//$this->set(compact('users'));
		//$this->render("edit");
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Voucher->id = $id;
		if (!$this->Voucher->exists()) {
			throw new NotFoundException(__('Invalid voucher'));
		}	
		
		if ($this->Voucher->delete()) {
			$this->Flash->success(__('The voucher has been deleted.'));
		} else {
			$this->Flash->error(__('The voucher could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
	public function publish_voucher($id = null) {
		$this->Voucher->id = $id;
		if (!$this->Voucher->exists()) {
			throw new NotFoundException(__('Invalid voucher'));
		}
		$this->request->data['Voucher']['voucher_status']=1;		
		if ($this->Voucher->save($this->request->data)) {
			$this->Flash->success(__('The voucher has been Published.'));
			return $this->redirect(SITE_LINK.'list-vouchers');
		} else {
			$this->Flash->error(__('The voucher could not be published. Please, try again.'));
		}
	}
	public function copy_voucher($id = null) {
		$this->Voucher->id = $id;
		
		if (!$this->Voucher->exists()) {
			throw new NotFoundException(__('Invalid voucher'));
		}	
		$options = array('conditions' => array('Voucher.' . $this->Voucher->primaryKey => $id,"user_id"=>$this->Session->read("Auth.User.id")));		
		$voucher= $this->Voucher->find('first',$options);
		//$temp_data=$voucher['Voucher'];		
		unset($voucher['Voucher']['id']);
		unset($voucher['Voucher']['modified']);
		unset($voucher['Voucher']['created']);
		
		//pr($voucher);
		if ( isset($voucher['Voucher']['image']) && !empty($voucher['Voucher']['image']) && file_exists(WWW_ROOT."/img/voucher/".$voucher['Voucher']['image']) ) {
			$tmpname = strtotime(date("Y-m-d h:i:s")).$voucher['Voucher']['image'];
			//echo WWW_ROOT."img/voucher/".$voucher['Voucher']['image'];
			//die;
			if ( copy(WWW_ROOT."img/voucher/".$voucher['Voucher']['image'],WWW_ROOT."img/voucher/".$tmpname) ) {
				$voucher['Voucher']['image'] = $tmpname;
				
			}
		}
		$tmpStatus = $voucher['Voucher']['voucher_status'];
		$voucher['Voucher']['voucher_status'] = 0;
		$voucher['Voucher']['is_active'] = 0;
		$this->request->data=$voucher['Voucher'];
		$url = ($voucher['Voucher']['voucher_status']==0)?"drafted-vouchers/#top":"published-vouchers/#top";			
		$this->Voucher->create();
		$result = array("status"=>false);
		if ($this->Voucher->save($this->request->data)) {
			$result["status"] = 1;
			$result["id"] = $this->Voucher->getLastInsertID();
			$result["vstatus"] = $tmpStatus;
			//$this->Flash->success(__('The voucher has been copied.'));
			//return $this->redirect(SITE_LINK."".$url);
		} else {
			//$this->Flash->error(__('The voucher could not be copied. Please, try again.'));
		}
		echo json_encode($result);
		die;
	}
	public function getdatevoucher($s_date,$e_date,$s_time,$e_time,$w_days) {
		$date = $this->Voucher->query("select WEEKDAY(now()) as curDay");
		$currentDay = ($date[0][0]['curDay'])+1;
		$addDay = 0;
		if (!empty($w_days)) {
			$tmp = explode(",",$w_days);
			$prevDay = $nextDay = 0;
			foreach( $tmp as $dayskey=>$daysval ) {
				if ( empty($prevDay) && $currentDay > $daysval ) {
					$prevDay = $daysval;
				}
				if (empty($nextDay) && $currentDay < $daysval) {
					$nextDay = $daysval;
				}
			}
			if ( !empty($nextDay) ) {
				$addDay = $nextDay-$currentDay;
				//$str = "Starting in ".$tmpdays." ".(($tmpdays > 1)?"Days":"Day" );
			} else {
				$addDay = (7-$currentDay)+$tmp[0];
				//$str = "Starting in ".$tmpdays." ".(($tmpdays > 1)?"Days":"Day" );
			}
			//echo $str;
			//die;
		}
		if (!empty($s_time)) {
			 $tmpStart = (date('Y-m-d H:i:s',strtotime(($s_date.' '.$s_time)." + ".$addDay." Day")));
		} else {
			 $tmpStart = (date('Y-m-d 00:00:00',strtotime(($s_date)." + ".$addDay." Day")));
		}
		if (!empty($e_time)) {
			 $tmpEnd = (date('Y-m-d H:i:s',strtotime(($e_date.' '.$e_time))));
		} else {
			 $tmpEnd = (date('Y-m-d 00:00:00',strtotime(($e_date))));
		}
		$this->Voucher->virtualFields["is_expired"] = "if(now() > '".$tmpEnd."',1,0)";
		$this->Voucher->virtualFields["start_in_day"] = "if((date(now()) < date('".$tmpStart."')),TIMESTAMPDIFF(DAY,now(), '".$tmpStart."'),0)";
		$this->Voucher->virtualFields["start_in_hour"] = "if(TIMEDIFF(time('".$tmpStart."'),time(now())) > 0 , hour(TIMEDIFF(time('".$tmpStart."'),time(now()))),0)";
		$this->Voucher->virtualFields["start_in_minute"] = "if(TIMEDIFF(time('".$tmpStart."'),time(now())) > 0 ,minute(TIMEDIFF(time('".$tmpStart."'),time(now()))),0)";
		$this->Voucher->virtualFields["end_in_day"] = "if((date('".$tmpEnd."') > date(now())),TIMESTAMPDIFF(DAY, now(),'".$tmpEnd."'),0)";
		$this->Voucher->virtualFields["end_in_hour"] = "if(TIMEDIFF(time('".$tmpEnd."'),time(now())) > 0 , hour(TIMEDIFF(time('".$tmpEnd."'),time(now()))),0)";
		$this->Voucher->virtualFields["end_in_minute"] = "if(TIMEDIFF(time('".$tmpEnd."'),time(now())) > 0 ,minute(TIMEDIFF(time('".$tmpEnd."'),time(now()))),0)";
		$tmp = $val = $this->Voucher->find("first",array("fields"=>array("Voucher.is_expired","Voucher.start_in_day","Voucher.end_in_day","Voucher.start_in_hour","Voucher.end_in_hour","Voucher.start_in_minute","Voucher.end_in_minute")));
		$message = "";
		$startFlag = true;
		$str = "";
		if ( !empty($tmp["Voucher"]["is_expired"]) ) {
			$message = "Voucher Expired";
		} else {
			$str = "Starting in"; 
			if ( !empty($val['Voucher']['start_in_day']) ) {
				$startFlag = false;
				$str .= " ".$val['Voucher']['start_in_day']." ".(($val['Voucher']['start_in_day'] > 1)?"Days":"Day" );
			} 
			if ( !empty($val['Voucher']['start_in_hour']) ) {
				$startFlag = false;
				$str .= " ".$val['Voucher']['start_in_hour']." ".(($val['Voucher']['start_in_hour'] > 1)?"Hours":"Hour" );
			} 
			if ( !empty($val['Voucher']['start_in_minute']) ) {
				$startFlag = false;
				$str .= " ".$val['Voucher']['start_in_minute']." ".(($val['Voucher']['start_in_minute'] > 1)?"Minutes":"Minute" );
			} 
			if ($startFlag) {
				$str = "";
				if ( !empty($val['Voucher']['end_in_day']) ) {
					$str .= "".$val['Voucher']['end_in_day']." ".(($val['Voucher']['end_in_day'] > 1)?"Days":"Day" )." ";
				} 
				if ( !empty($val['Voucher']['end_in_hour']) ) {
					$str .= "".$val['Voucher']['end_in_hour']." ".(($val['Voucher']['end_in_hour'] > 1)?"Hours":"Hour" )." ";
				} 
				if ( !empty($val['Voucher']['end_in_minute']) ) {
					$str .= $val['Voucher']['end_in_minute']." ".(($val['Voucher']['end_in_minute'] > 1)?"Minutes":"Minute" )." ";
				}
				$str .=" Left";
			}
		}
		if ( empty($str) ) {
			$str = "Voucher Expired";
		}
		$result["message"] = $str;
		return $result;
			
	}
	
	
	
	function getdate() {
		
		$date = $this->Voucher->query("select WEEKDAY(now()) as curDay");
		$currentDay = ($date[0][0]['curDay'])+1;
		$addDay = 0;
		if (!empty($this->request->query("days"))) {
			$tmp = explode(",",$this->request->query("days"));
			$prevDay = $nextDay = 0;
			foreach( $tmp as $dayskey=>$daysval ) {
				if ( empty($prevDay) && $currentDay > $daysval ) {
					$prevDay = $daysval;
				}
				if (empty($nextDay) && $currentDay < $daysval) {
					$nextDay = $daysval;
				}
			}
			if ( !empty($nextDay) ) {
				$addDay = $nextDay-$currentDay;
				//$str = "Starting in ".$tmpdays." ".(($tmpdays > 1)?"Days":"Day" );
			} else {
				$addDay = (7-$currentDay)+$tmp[0];
				//$str = "Starting in ".$tmpdays." ".(($tmpdays > 1)?"Days":"Day" );
			}			
		}
		if (!empty($this->request->query['s_time'])) {
			 $tmpStart = (date('Y-m-d H:i:s',strtotime(($this->request->query("s_date").' '.$this->request->query("s_time"))." + ".$addDay." Day")));
		} else {
			 $tmpStart = (date('Y-m-d 00:00:00',strtotime(($this->request->query['s_date'])." + ".$addDay." Day")));
		}
		if (!empty($this->request->query['e_time'])) {
			 $tmpEnd = (date('Y-m-d H:i:s',strtotime(($this->request->query("e_date").' '.$this->request->query("e_time")))));
		} else {
			 $tmpEnd = (date('Y-m-d 00:00:00',strtotime(($this->request->query['e_date']))));
		}
		$this->Voucher->virtualFields["is_expired"] = "if(now() > '".$tmpEnd."',1,0)";
		$this->Voucher->virtualFields["start_in_day"] = "if((date(now()) < date('".$tmpStart."')),TIMESTAMPDIFF(DAY,now(), '".$tmpStart."'),0)";
		$this->Voucher->virtualFields["start_in_hour"] = "if(TIMEDIFF(time('".$tmpStart."'),time(now())) > 0 , hour(TIMEDIFF(time('".$tmpStart."'),time(now()))),0)";
		$this->Voucher->virtualFields["start_in_minute"] = "if(TIMEDIFF(time('".$tmpStart."'),time(now())) > 0 ,minute(TIMEDIFF(time('".$tmpStart."'),time(now()))),0)";
		$this->Voucher->virtualFields["end_in_day"] = "if((date('".$tmpEnd."') > date(now())),TIMESTAMPDIFF(DAY, now(),'".$tmpEnd."'),0)";
		$this->Voucher->virtualFields["end_in_hour"] = "if(TIMEDIFF(time('".$tmpEnd."'),time(now())) > 0 , hour(TIMEDIFF(time('".$tmpEnd."'),time(now()))),0)";
		$this->Voucher->virtualFields["end_in_minute"] = "if(TIMEDIFF(time('".$tmpEnd."'),time(now())) > 0 ,minute(TIMEDIFF(time('".$tmpEnd."'),time(now()))),0)";
		$tmp = $val = $this->Voucher->find("first",array("fields"=>array("Voucher.is_expired","Voucher.start_in_day","Voucher.end_in_day","Voucher.start_in_hour","Voucher.end_in_hour","Voucher.start_in_minute","Voucher.end_in_minute")));
		$message = "";
		$startFlag = true;
		$str = "";
		if ( !empty($tmp["Voucher"]["is_expired"]) ) {
			$message = "Voucher Expired";
		} else {
			$str = "Starting in"; 
			if ( !empty($val['Voucher']['start_in_day']) ) {
				$startFlag = false;
				$str .= " ".$val['Voucher']['start_in_day']." ".(($val['Voucher']['start_in_day'] > 1)?"Days":"Day" );
			} 
			if ( !empty($val['Voucher']['start_in_hour']) ) {
				$startFlag = false;
				$str .= " ".$val['Voucher']['start_in_hour']." ".(($val['Voucher']['start_in_hour'] > 1)?"Hours":"Hour" );
			} 
			if ( !empty($val['Voucher']['start_in_minute']) ) {
				$startFlag = false;
				$str .= " ".$val['Voucher']['start_in_minute']." ".(($val['Voucher']['start_in_minute'] > 1)?"Minutes":"Minute" );
			} 
			if ($startFlag) {
				$str = "";
				if ( !empty($val['Voucher']['end_in_day']) ) {
					$str .= "".$val['Voucher']['end_in_day']." ".(($val['Voucher']['end_in_day'] > 1)?"Days":"Day" )." ";
				} 
				if ( !empty($val['Voucher']['end_in_hour']) ) {
					$str .= "".$val['Voucher']['end_in_hour']." ".(($val['Voucher']['end_in_hour'] > 1)?"Hours":"Hour" )." ";
				} 
				if ( !empty($val['Voucher']['end_in_minute']) ) {
					$str .= $val['Voucher']['end_in_minute']." ".(($val['Voucher']['end_in_minute'] > 1)?"Minutes":"Minute" )." ";
				}
				$str .=" Left";
			}
		}
		if ( empty($str) ) {
			$str = "Voucher Expired";
		}
		$result["message"] = $str;
		echo json_encode($result);
		die;
		
	}	
	function checklocation()
	{
		$this->layout = false;
		$this->render(false);
		if ( $this->request->is("ajax") ) {
		$this->loadModel("Location");		
		
		//$location = $this->Location->find('all',array('conditions'=>array('Location.user_id'=>$this->Session->read('Auth.User.id'),'Location.is_paid'=>1)));
		//pr($location);
		if($location = $this->Location->find('all',array('conditions'=>array('Location.user_id'=>$this->Session->read('Auth.User.id'),'Location.is_paid'=>1))))
		{ 
			 $result = array("status"=>true);
		}
		else
		{
			 $result = array("status"=>false);
		}
			echo json_encode($result);
			die;
		}
	}
	public function publish_all_vouchers()
	{
		$this->layout=false;
		$this->render(false);
		if ( $this->request->is("ajax") ) {
			//echo $this->Session->read("vouc_id");
			//$this->conditions =(array("Voucher.id"=>$this->Session->read("vouc_id")));	
			//$this->request->data["Vocuher"]["voucher_status"]=1;
			$this->Voucher->id = $this->Session->read("vouc_id");
			if($voucher = $this->Voucher->save(array('voucher_status'=>1)))		
			{
				unset($_SESSION['vouc_id']);
				 $result = array("status"=>true);
			}			
			echo json_encode($result);
			die;
		}
	}
	public function merchant_detail()
	{			
		
		$this->layout = false;
		if(isset($this->request->url))
		{
			if (!isset($this->request->url) || empty($this->request->url)) {
				$this->redirect(SITE_LINK);
			}else{		
				$this->loadModel("Locations");				
				$conditions = array("seourl"=>$this->request->url);		
				$location = $this->Locations->find("first",array("conditions"=>$conditions));
			}
			$userD=$location["Locations"]["user_id"];
		    $fields = array("Voucher.id","Voucher.user_id","Voucher.title_discount", "Voucher.description", "Voucher.tags", "Voucher.businessName","Voucher.voucherImageUrl","Voucher.businessImageUrl","Voucher.repeat_on","Voucher.repeat_days","Voucher.start_in_day","Voucher.start_in_hour","Voucher.start_in_minute","Voucher.end_in_day","Voucher.voucher_start_date","Voucher.voucher_end_date","Voucher.end_in_hour","Voucher.end_in_minute","Voucher.is_expired","Voucher.latitude","Voucher.longitude","Voucher.tags","Voucher.day_status","Voucher.sub_title","Voucher.terms_url");
			$date = $this->Voucher->query("select WEEKDAY(now()) as curDay");
			$currentDay = ($date[0][0]['curDay'])+1;
			$order = array("Voucher.start_date desc");
			$tmp = $this->request->query;
			
			$this->makeAssociation();				
			$this->Voucher->virtualFields["businessName"] = "UserDetail.business";
			$this->Voucher->virtualFields["latitude"] = "UserDetail.latitude";
			$this->Voucher->virtualFields["longitude"] = "UserDetail.longitude";
			$this->Voucher->virtualFields["voucherImageUrl"] = "concat('".SITE_LINK."','img/voucher/',Voucher.image)";
			$this->Voucher->virtualFields["businessImageUrl"] = "concat('".SITE_LINK."','img/profile/',UserDetail.image)";
			$this->Voucher->virtualFields["is_expired"] = "if(now() > voucher_end_date,1,0)";
			
			$this->Voucher->virtualFields["start_in_day"] = "if((date(now()) < date(Voucher.voucher_start_date)),TIMESTAMPDIFF(DAY, voucher_start_date, now()),0)"; 
			$this->Voucher->virtualFields["start_in_hour"] = "if(TIMEDIFF(time(voucher_start_date),time(now())) > 0 , hour(TIMEDIFF(time(voucher_start_date),time(now()))),0)";
			$this->Voucher->virtualFields["start_in_minute"] = "if(TIMEDIFF(time(voucher_start_date),time(now())) > 0 ,minute(TIMEDIFF(time(voucher_start_date),time(now()))),0)";
			
			$this->Voucher->virtualFields["end_in_day"] = "if((date(Voucher.voucher_end_date) > date(now())),TIMESTAMPDIFF(DAY, now(),voucher_end_date),0)"; 
			$this->Voucher->virtualFields["end_in_hour"] = "if(TIMEDIFF(time(voucher_end_date),time(now())) > 0 , hour(TIMEDIFF(time(voucher_end_date),time(now()))),0)";
			$this->Voucher->virtualFields["end_in_minute"] = "if(TIMEDIFF(time(voucher_end_date),time(now())) > 0 ,minute(TIMEDIFF(time(voucher_end_date),time(now()))),0)";
			
			
			$tmpCond = array();
			$conditions = array("Voucher.voucher_status"=>1 /*,"repeat_on like 'D' or repeat_days like '%".$currentDay."%'" /*,"OR"=>array("repeat_on like 'D'","repeat_days like '%".$currentDay."%'")*/);
			//$tmpCond['OR'] = array("repeat_on like 'D'","repeat_days like '%".$currentDay."%'");	
			
			$conditions = array_merge($conditions,array("Voucher.is_expired"=>0));			
			$conditions = array_merge($conditions,$tmpCond);
			
			//$this->log($tmp);
			
			$conditions = array_merge($conditions,array("Voucher.user_id "=> $userD));
		    $vouchers = $this->Voucher->find("all",array("conditions"=>$conditions,"fields"=>$fields,"order"=>$order,"group"=>"Voucher.id"));
		   
		    $vouchers = $this->processVoucher($currentDay,$vouchers);
			
			$this->set('vouchers', $vouchers);
		    
			$this->set('location', $location);
			$this->set("title_for_layout",$location["Locations"]["title"].' '.$location["Locations"]["address"]);
			$this->set("keywords","Merchant & Vouchers Detail");	
		} else {
			$this->redirect(SITE_LINK);
		}
		
	}
	function processVoucher($currentDay,$vouchers) {
		foreach( $vouchers as $key=>$val  ) {
							
				if (!empty($val['Voucher']['day_status'])) {
					$vouchers[$key]['Voucher']["start_val"] = $vouchers[$key]['Voucher']["end_val"] = "All Day";
				} else /*if ( $val['Voucher']['repeat_on'] == 'D' )*/ {
					$str = "";
					if ( !empty($val['Voucher']['start_in_day']) ) {						
						$str = "Starts at ".date("h:i A",strtotime($val['Voucher']['voucher_start_date']));
					} elseif ( !empty($val['Voucher']['start_in_hour']) ) {					
						$str = "Starts at ".date("h:i A",strtotime($val['Voucher']['voucher_start_date']));
					} elseif ( !empty($val['Voucher']['start_in_minute']) ) {						
						$str = "Starts at ".date("h:i A",strtotime($val['Voucher']['voucher_start_date']));
					}
					$vouchers[$key]['Voucher']["start_val"] = $str;
					$str = "";
					if ( !empty($val['Voucher']['end_in_day']) ) {						
						$str = "Ends at ".date("h:i A",strtotime($val['Voucher']['voucher_end_date']));
					} elseif ( !empty($val['Voucher']['end_in_hour']) ) {
					
						$str = "Ends at ".date("h:i A",strtotime($val['Voucher']['voucher_end_date']));
					} elseif ( !empty($val['Voucher']['end_in_minute']) ) {						
						$str = "Ends at ".date("h:i A",strtotime($val['Voucher']['voucher_end_date']));
					}
					$vouchers[$key]['Voucher']["end_val"] = $str;
				}				
				
			} return $vouchers;
	}
	
	function makeAssociation($flag = false) {
		$this->Voucher->belongsTo = array(
				"User" => array(
					"className" => "User",
					"foreignKey"=> "user_id",
					"type" => "Inner"
				),
				"UserDetail" => array(
					"className" => "UserDetail",
					"foreignKey"=> false,
					"type" => "Inner",
					"conditions" => "User.id = UserDetail.user_id"
				)
			);		
	}

}
