<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {
	
	
	public $components = array(
	"Session",
	"Flash","Image",
	"Cookie",
	'Auth' => array(
			'loginAction' => array(
				'controller' => 'users',
				'action' => 'login',
			),
			'authError' => 'Did you really think you are allowed to see that?',
			'authenticate' => array(
				'Form' => array(
					'fields' => array(
					  'username' => 'username', //Default is 'username' in the userModel
					  'password' => 'password'  //Default is 'password' in the userModel
					),
					'scope' => array('User.is_active' => '1'),
					//'passwordHasher' => 'Blowfish'
				)
			)
		)
	);
	
	var $conditions = array();
	var $delarr = array();
	var $updatearr = array();
	public $mailBody = 'MoolaVoucher';
    public $from = 'ranjuzestmind@gmail.com';
    public $subject = 'MoolaVoucher';
    public $imagename = '';
    public $uploaddir =  '';
    public $jsArray = array();
    public $cssArray = array();
    
    function beforefilter() {	
        //phpinfo(); die;
		$this->Auth->scope = array('User.is_active' =>1);
		
		if ($this->params['prefix']) {
			//die("here");
			if ( $this->Session->read("Auth.User.user_type_id") != 1 ) {				
				 $this->Flash->error(__("You are not authorized to view this page."), 'default', array("class"=>"error_message"));
				 $this->redirect("/");				
			}
			$this->layout = "admin";
		} else {
			if ( $this->request->is("ajax") ) {
				
			} else {
				if ( $this->Session->read("Auth.User.user_type_id") == 1 && ($this->params['action'] != "logout") ) {				
					 //$this->Flash->error(__("You are not authorized to view this page."), 'default', array("class"=>"error_message"));
					 $this->redirect("/dashboard");				
				}
				$this->location_list();
				if ( !$this->Session->read("Auth.User.id") ) {
					$this->jsArray[] = array("jquery.validate.min","loginvalidate","fblogin","jquery.form.min","signupvalidate");
				} 
				else if ($this->Session->read("Auth.User.identifier") != '' ) {
					$this->jsArray[] = array("fblogin","jquery.validate.min");			
				}
				else
				{
					$this->jsArray[] = array("fblogin","jquery.validate.min");		
				}
				
				
				
				$this->layout = "admin";
			}
		}
		
		
	}
	
	function beforeRender() {
		$this->set("jsArray",$this->jsArray);
		$this->set("cssArray",$this->cssArray);
	}
	
	function checkLogin() {
		if ( $this->Session->read("Auth.User.id") ) {
			$user = $this->Session->read("Auth");
			if ( $user['User']['user_type_id'] == 1 ) {
				$this->redirect("/dashboard");
			} else {
				$this->redirect("/merchant");
			}
		} 
	}
	
	public function getUserDetail() {
		$id = $this->Session->read("Auth.User.id");
		$this->loadModel("User");
		$temp = $this->User->find("first",array("conditions"=>array("User.id"=>$id)));
		$this->Session->write("User",$temp);
	}
	
	function bulkactions($flag = false) {
		
		$controller = is_array($this->data)?array_keys($this->data):'';
		$statuskey  = '';
		$controller = isset($controller[0])?$controller[0]:'';
		$allowedarr = array("Account","User");
		if (isset($this->data[$controller]) && !empty($this->data[$controller]['options']) && !empty($controller) && !empty($this->data['id'])) {
			
			$str = "";
			if ( !empty($this->request->query) ) {
				foreach ( $this->request->query as $key=>$val ) {
					if (empty($str)) {
						$str = $key."=".$val;
					} else {
						$str .= "&".$key."=".$val;
					}
				}
			}
			$url = $this->params->url;
			if ( !empty($str) ) {
				$url .= "?".$str;
			}
			
			
			foreach ($this->data['id'] as $key=>$val) {
				if ($val > 0) {
					$this->delarr[]	= $key;
					if ($flag) {
						$statuskey = ($this->data[$controller]['options']);
						$this->updatearr[$controller][$key] = array("id"=>$key,"voucher_status"=>($this->data[$controller]['options'] == 'Active'?1:0));
					} else {
						$statuskey = ($this->data[$controller]['options'] == 'Active'?1:0);
						$this->updatearr[$controller][$key]	= array("id"=>$key,"is_active"=>($this->data[$controller]['options'] == 'Active'?1:0));
					}
				}
			}
			
			if (isset($this->data[$controller]['options']) && $this->data[$controller]['options'] == 'Delete') {
				//pr($this->delarr); die;
				if ( isset($this->$controller->tmpData) ) {
					$this->$controller->tmpData = $this->delarr;
				}
				if($this->$controller->delete($this->delarr)) {
					$this->Session->setFlash(__('Record has been deleted successfully.'));
				}
				$this->redirect($this->referer());
				
				$statuskey = -1;
			} else {
				//pr($this->updatearr[$controller]);
				//die;
				//die;
				$this->$controller->create();
				if($this->$controller->saveAll($this->updatearr[$controller],array("validate"=>false))) {
					 $this->Session->setFlash(__('Record has been updated successfully.'));
				}
			
				$this->redirect($this->referer());
			}
			if (empty($this->data['Admin']['searchval'])) {
				$this->data = array();
			}
			$this->redirect($url);
		}
		if (in_array($controller,$allowedarr) && $statuskey > -1) {
			$arr['keys'] 	= $this->delarr;
			$arr['status']  = $statuskey;
			return $arr; 
		}
		if ($flag) {
			$arr['keys'] = $this->delarr;
			$arr['status']  = $statuskey;
			return $arr; 
		}
		
		/* end of code to change status and delete by checking data from page */
	}
	
	
	/*
     * @function name	: getmaildata
     * @purpose			: getting email data for various purposes
     * @arguments		: Following are the arguments to be passed:
     * id				: id of email templates from cmsemail table
     * @return			: NONE
     * @created on		: 
     * @description		: function will assign value to global variables like mailbody,from, subject which will be used while sending email
     */

    function getMailData($mail_slug = null, $to = null) {
        $this->loadModel('EmailTemplate');
        $cmsemail = $this->EmailTemplate->find('first', array('conditions' => array('EmailTemplate.slug' => $mail_slug)));       
        if (!empty($cmsemail)) {
            $this->mailBody = $cmsemail['EmailTemplate']['content'];
            $this->from = $cmsemail['EmailTemplate']['email_from'];
            $this->subject = str_replace("{TO}", $to, $cmsemail['EmailTemplate']['subject']);
        }        
    }
    
    
    /* end of function */
    /*
     * @function name	: sendmail
     * @purpose			: sending email for various actions
     * @arguments		: Following are the arguments to be passed:
     * from		: contain email address from which email is sending
     * Subject	: Subject of Email
     * to		: Email address to whom the email is sending
     * body		: content of email
     * template : if defining a html template for sending email else false.
     * values	: to be given in email template like username etc.
     * @return			: true if email sending successfull else return false.
     * @created on		: 11th March 2014
     * @description		: NA
     */

    function sendMail($to, $template = 'email', $fromname = 'Moola Voucher') {
		//pr($this->mailBody);die;
        App::uses('CakeEmail', 'Network/Email');
       // if (isset($this->params->base) && !empty($this->params->base)) {
            //$email = new CakeEmail("gmail");
        //} else {
           //$email = new CakeEmail("zestminds");
           $email = new CakeEmail("ses");
        //}
        //Use filter_var_array for multiple emails
        $this->from = "no-reply@moolarewards.co.uk";
        $is_valid = is_array($to) ? filter_var_array($to, FILTER_VALIDATE_EMAIL) : filter_var($to, FILTER_VALIDATE_EMAIL);
        if ($is_valid) {
            $email->from(array($this->from => $fromname));
            $email->to($to);
            $email->subject($this->subject);
            $headers[] = 'MIME-Version: 1.0';
            $headers[] = 'Content-type: text/html; charset=iso-8859-1';
            $email->addHeaders($headers);
            $email->emailFormat('both');
            if (empty($template)) {
				try {
					if ( !$email->send($this->mailBody)) {
						throw new Exception;
                    } else {
						return true;
                    }
                } catch (Exception $e) {
                    return false;
                }
            } else {
                if (!empty($this->mailBody)) {
                    $email->viewVars(array("mail" => $this->mailBody));
                }
                $email->template($template, '');
                try {
                    if (!$email->send()) {
						throw new Exception;
                    } else {
						return true;
                    }
                    
                } catch (Exception $e) {
					echo $e->getMessage();
					die;
                    return false;
                } 
            }
        } else {
            return false;
        }
    }

    /* end of function */
    
    function encryptpass($password, $salt = '', $method = 'md5', $crop = true, $start = 4, $end = 10) {
		$salt = strtotime(date("Y-m-d h:i:s"));
		if ($crop) {
			$password = $method(substr($method($password.$salt), $start, $end));
		} else {
			$password = $method($password.$salt);
		}
		return $password;
    }
    
    function checkRemember() {
		$cookie = $this->Cookie->read("rememberme");	
		
		if ( !empty($cookie) ) {
			$this->loadModel("User");
			if ( $tmpUser = $this->User->find("first",array("conditions"=>array("remember_token"=>$cookie),"recursive"=>0)) ) {
				//pr($tmpUser); die;
				$this->request->data['User']['remember_token'] = $cookie;
				$this->components['Auth']['authenticate']['Form']['fields'] = array("remember_token"=>"remember_token");
				if ( $this->Auth->login($this->request->data['User']) ) {
					$this->Session->write("Auth",$tmpUser);
				} 
			}
		}
	}
	
	
	function uploadImage($file , $destination = NULL, $old_img = false,$first = NULL,$second=NULL, $filetypes = array('jpg', 'jpeg', 'png')) {
		$flag = false;
		$file_ext = explode(".",$file['name']);
		$file_ext = strtolower(end($file_ext));
		$this->imagename = $this->uploaddir =  '';
		if ( in_array($file_ext,$filetypes) ) {
			
			$this->uploaddir = WWW_ROOT."img/".$destination."/";
			
			if ( !empty($destination) && !is_dir($this->uploaddir) ) {
				mkdir($this->uploaddir,0777,true);
			}
			$this->imagename = mt_rand().strtotime(date("y-m-d h:i:s")).".".$file_ext;
			if ( move_uploaded_file($file['tmp_name'],$this->uploaddir.$this->imagename) ) {
				 if($old_img) {
					if ( !empty($first) && file_exists($this->uploaddir.$first) ) {					
						@unlink($this->uploaddir.$first);
					}
					if ( !empty($second) && file_exists($this->uploaddir.$second) ) {				
						@unlink($this->uploaddir.$second);					
					}
				}
				$flag = true;
			} 
		} else {			
			$flag = false;
		}
		return $flag; 
	}
	
	
	function genImage($data, $destination = NULL) {
		if (preg_match('/^data:image\/(\w+);base64,/', $data, $type)) {
			$data = substr($data, strpos($data, ',') + 1);
			$type = strtolower($type[1]); // jpg, png, gif

			if (!in_array($type, [ 'jpg', 'jpeg', 'gif', 'png' ])) {
				throw new \Exception('invalid image type');
			}

			$data = base64_decode($data);

			if ($data === false) {
				throw new \Exception('base64_decode failed');
			}
			
			$this->imagename = $this->uploaddir =  '';
			$this->uploaddir = WWW_ROOT."img/".$destination."/";
			if ( !empty($destination) && !is_dir($this->uploaddir) ) {
				mkdir($this->uploaddir,0777,true);
			}
			$this->imagename = mt_rand().strtotime(date("y-m-d h:i:s")).".".$type;
			$this->uploaddir.$this->imagename;
			
			if ( file_put_contents($this->uploaddir.$this->imagename, $data)) {
				//die("1");
				return true;
			} else {
				//die("10");
				return false;
			}
			
		} else {
			throw new \Exception('did not match data URI with image data');
		}
		
		
	}
	
public function verifyRecatpcha($aData)
 {
    $recaptcha_secret = SECRET_KEY;
    $url = "https://www.google.com/recaptcha/api/siteverify?secret=".$recaptcha_secret."&response=".$aData['g-recaptcha-response']; 
    $response = json_decode(@file_get_contents($url));          
                
    if($response->success)
    {
        return true;
    }
    else
    {
        return false;
         
    }   
 } 
   public function location_list() {
		$this->render = false;
		$location_list=array();		
		$this->loadModel("Plan");
		$planoptions = array("Plan.is_active"=>1);
		$plan=$this->Plan->find("first",$planoptions);	
		$this->loadModel("Location");
		
		$options = array('Location.user_id'=> $this->Session->read('Auth.User.id'),'Location.is_paid'=>0,'Location.payment_status is null');
		
		if (empty($this->Session->read('cart_items'))) {
			$locations = $this->Location->find('all',array("conditions"=>$options,"fields"=>array("id","address","title","image","created"),"recursive"=>-1));
			///pr($locations);
			$cart_total_items=0;
			foreach ($locations as $key=>$val) {
				$tmpVal = $val['Location'];
				$tmpVal["is_view"] = 1;
				$location_list[$val['Location']['id']] = $tmpVal;
				$cart_total_items+=1;
			}
			//echo $cart_total_items;
			//pr($location_list);die;
			$this->Session->write('cart_total_items',$cart_total_items);
			$this->Session->write('cart_items',$location_list);
		} 
		
		//unset($_SESSION['cart_items']);
		
	}
	
}
