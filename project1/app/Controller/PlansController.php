<?php
App::uses('AppController', 'Controller');
/**
 * Plans Controller
 *
 * @property Plan $Plan
 * @property PaginatorComponent $Paginator
 */
class PlansController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator',"Paypal");

/**
 * index method
 *
 * @return void
 */
	public function index() {
		
		$this->layout = "mooladesignmyvoucher";		
		$plan = $this->Plan->find("all",array("conditions"=>array("is_active"=>1)));
		$this->set("plans",$plan);
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Plan->exists($id)) {
			throw new NotFoundException(__('Invalid plan'));
		}
		$options = array('conditions' => array('Plan.' . $this->Plan->primaryKey => $id));
		$this->set('plan', $this->Plan->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Plan->create();
			if ($this->Plan->save($this->request->data)) {
				$this->Flash->success(__('The plan has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The plan could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->layout = "mooladesignadmin";
		if (!$this->Plan->exists($id)) {
			throw new NotFoundException(__('Invalid plan'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Plan->save($this->request->data)) {
				$this->Flash->success(__('The plan has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The plan could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Plan.' . $this->Plan->primaryKey => $id));
			$this->request->data = $this->Plan->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Plan->id = $id;
		if (!$this->Plan->exists()) {
			throw new NotFoundException(__('Invalid plan'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Plan->delete()) {
			$this->Flash->success(__('The plan has been deleted.'));
		} else {
			$this->Flash->error(__('The plan could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->layout = "mooladesignadmin";
		$this->Plan->recursive = 0;
		$this->set('plans', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		
		if (!$this->Plan->exists($id)) {
			throw new NotFoundException(__('Invalid plan'));
		}
		$options = array('conditions' => array('Plan.' . $this->Plan->primaryKey => $id));
		$this->set('plan', $this->Plan->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Plan->create();
			if ($this->Plan->save($this->request->data)) {
				$this->Flash->success(__('The plan has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The plan could not be saved. Please, try again.'));
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		$this->layout = "mooladesignadmin";
		if (!$this->Plan->exists($id)) {
			throw new NotFoundException(__('Invalid plan'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Plan->save($this->request->data)) {
				$this->Flash->success(__('The plan has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The plan could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Plan.' . $this->Plan->primaryKey => $id));
			$this->request->data = $this->Plan->find('first', $options);
		}
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Plan->id = $id;
		if (!$this->Plan->exists()) {
			throw new NotFoundException(__('Invalid plan'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Plan->delete()) {
			$this->Flash->success(__('The plan has been deleted.'));
		} else {
			$this->Flash->error(__('The plan could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
	
	
	function buyplan($id = NULL,$busid=null) {
		
		$this->layout=false;
		$this->render(false);
		
		if ( !empty($id) ) {
			$this->Plan->id = $id;
			if (!$this->Plan->exists()) {
				throw new NotFoundException(__('Invalid plan'));
			}
			$plan = $this->Plan->find("first",array("conditions"=>array("id"=>$id,"is_active"=>1),"recursive"=>-1));
			$price = $plan['Plan']['price'];
			$businessid=explode(",",$busid);	
			$price=count($businessid) * $price;
			$vat = ceil(($price * 20)/100);
			$price = $price+$vat;			
			$this->Paypal->cancelurl = SITE_LINK."locations/cancel";
			$this->Paypal->returnurl = SITE_LINK."create-profile/".$id."/".$busid."/";
			$this->Paypal->initializePayment($price);			
			if ( $this->Paypal->status ) {
				$this->redirect($this->Paypal->redirectURL);
			}
		}
	}
	
	function createprofile($id = NULL,$busid=null) {
		//echo $busid;echo $id;
		$plan = $this->Plan->find("first",array("conditions"=>array("id"=>$id,"is_active"=>1),"recursive"=>-1));
		$price = $plan['Plan']['price'];
		$result = $this->Paypal->createrecurringProfile($price,$this->request->query['token'],$this->request->query['PayerID']);
		
		if ( $result['PROFILESTATUS'] == 'ActiveProfile' && $result['ACK'] == 'Success' ) {
			
			//$tmpUser['User']['is_paid'] = 1;
			//$tmpUser['User']['payment_profile_id'] = $result['PROFILEID'];
			$this->loadModel("User");
			//~ $this->User->id = $this->Auth->user("id");
			//~ $this->User->save($tmpUser);
			$this->loadModel("Location");
			//$tmpUser['Location']['is_paid'] = 1;
			//$tmpUser['Location']['payment_profile_id'] =  $result['PROFILEID'];
			
			$businessid=explode(",",$busid);
			
			$tmpBusiness = $tmpBilling =  array();
			foreach ( $businessid as $bid ) {
				
				$tmpBusiness[$bid]['Location'] = array("id"=>$bid,"is_paid"=>1,'payment_profile_id'=>$result['PROFILEID']);
				$tmpBilling[$bid]['Billing']['user_id'] = $this->Auth->user("id");
				$tmpBilling[$bid]['Billing']['plan_id'] = $id;
				$tmpBilling[$bid]['Billing']['business_id'] = $bid;				
				$tmpBilling[$bid]['Billing']['bill_start_date'] = date("Y-m-d",strtotime($result['TIMESTAMP']));
				$tmpBilling[$bid]['Billing']['bill_end_date'] = date("Y-m-d",strtotime($result['TIMESTAMP']." +1 MONTH"));
				$tmpBilling[$bid]['Billing']['renew_date'] = date("Y-m-d",strtotime($result['TIMESTAMP']));
				$tmpBilling[$bid]['Billing']['due_date'] = date("Y-m-d",strtotime($result['TIMESTAMP']." +1 MONTH"));
				$tmpBilling[$bid]['Billing']['payment_start_on'] = ($result['TIMESTAMP']);
				$tmpBilling[$bid]['Billing']['bill_status'] = ($result['PROFILESTATUS']);
				$tmpBilling[$bid]['Billing']['bill_status'] = ($result['PROFILESTATUS']);
				$tmpBilling[$bid]['Billing']['paypal_token'] = ($result['CORRELATIONID']);
				$tmpBilling[$bid]['Billing']['paypal_profile'] = ($result['PROFILEID']);
			}			
			$this->loadModel("Billing");			
			$this->Location->saveAll($tmpBusiness);
			$this->Billing->saveAll($tmpBilling);	
		
			$price=count($businessid) * $price;
			$vat = ceil(($price * 20)/100);
			$price = $price+$vat;	
			//~ //send email						
			$users = $this->User->find("first",array("conditions"=>array("User.id"=>$this->Auth->user("id")),'fields' => array("UserDetail.business  AS accountname","User.username  AS accountemail")));
			$cmsemail=$this->getMailData("BILLING_RECEIPT");			
			$this->mailBody = str_replace("{username}",$users["UserDetail"]["accountname"] ,$this->mailBody);			
			$this->mailBody = str_replace("{amount}",$price ,$this->mailBody);
			$billdate=date("M d, Y",strtotime($result['TIMESTAMP']))." at <br/>".date("h:i A",strtotime($result['TIMESTAMP']));
			$nextbiilingdate=date("M d, Y",strtotime($result['TIMESTAMP']." +1 MONTH"))." at <br/>".date("h:i A",strtotime($result['TIMESTAMP']." +1 MONTH"));
			$servicedate=date("M d",strtotime($result['TIMESTAMP']))." to ".date("M, Y",strtotime($result['TIMESTAMP']." +1 MONTH"));
			$this->mailBody = str_replace("{billingdate}",$billdate,$this->mailBody);
			$this->mailBody = str_replace("{nextbiilingdate}",$nextbiilingdate,$this->mailBody);
			$this->mailBody = str_replace("{servicedate}",$servicedate,$this->mailBody);			
			$this->mailBody = str_replace("{ownername}",$users["UserDetail"]["accountname"]  ,$this->mailBody);
			$this->mailBody = str_replace("{accountemail}",$users["User"]["accountemail"] ,$this->mailBody);
			$this->sendMail($users['User']['accountemail']);
			
			//$this->Session->write("Auth.User.is_paid",1);
		}
		return $this->redirect(SITE_LINK."locations/paymentconfirm");
	}
}
