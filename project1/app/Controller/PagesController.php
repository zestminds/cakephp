<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class PagesController extends AppController {

/**
 * This controller does not use a model
 *
 * @var array
 */
	public $uses = array();
	public $components = array('Paginator',"Paypal",'RequestHandler');

	
	function beforefilter() {
		parent::beforefilter();
		$this->Auth->allow();
	}
	
	function index($token = NULL) {
		
		$this->set("title_for_layout","Best Deals - Discounts - Pub, Restaurant, Salon - Moola Rewards");
		//$this->set("description","Best Deals For Students In Uk - The best mobile app moola rewards will provide discount vouchers for students nearby areas, Students can find nearby local retailers.");
		$this->set("description","Moola Rewards is free to download the mobile app that shows you the best deals in town for pubs, clubs, bars, restaurant, shopping and entertainment.");
		$this->set("keywords","Moola, Cheap Hotel Deals In Matlock, Best Gym Membership Deals In Matlock, Club Membership Deals In Matlock, Best Pub Deals In Matlock, Beauty Salon Special Offers In Matlock, Best Restaurant Deals In Matlock, Best Shopping Offers In Matlock");
		if ($this->Session->read("Auth.User.id")) 
		{
			$this->layout = "mooladesignmyvoucher";		
		}
		else
		{
			$this->layout = "moolahomedesign";	
		}
			
		if (!is_array($this->params['url'])) {
			$url = explode("/",$this->params['url']);
			$url = $url[0];
		} else {
			$url = "";
		}	
		if ( !empty($token) ) { 			
			$this->loadModel("User");
			$users = $this->User->find("first",array("conditions"=>array("User.password_token"=>$token,"User.is_active"=>1),"recursive"=>-1));
			if ( !empty($users) ) {				
				$this->set("token",$token);
				$this->jsArray[] = "resetPassword";
			} else {				
				$this->Flash->error(__("Your password can not be reset, please try again later."), 'default', array("class"=>"error_message"));
				$this->redirect(SITE_LINK);
			}
		} elseif ( $url == "reset_password" ) {						
		
			$this->redirect(SITE_LINK);
		} 
	}
	
	public function merchant() {
		$this->set("title_for_layout","Great Offers For Merchant | Advertise with Moola | Moola Rewards");
		$this->set("keywords","Offers For Merchant, Cheap Deals For Merchant");
		$this->layout = "mooladesign";		
		if (!is_array($this->params['url'])) {
			$url = explode("/",$this->params['url']);
			$url = $url[0];
		} else {
			$url = "";
		}	
		if ( !empty($token) ) { 			
			$this->loadModel("User");
			$users = $this->User->find("first",array("conditions"=>array("User.password_token"=>$token,"User.is_active"=>1),"recursive"=>-1));
			if ( !empty($users) ) {				
				$this->set("token",$token);
				$this->jsArray[] = "resetPassword";
			} else {				
				$this->Flash->error(__("Your password can not be reset, please try again later."), 'default', array("class"=>"error_message"));
				$this->redirect(SITE_LINK);
			}
		} elseif ( $url == "reset_password" ) {						
		
			$this->redirect(SITE_LINK);
		}
	}
	
	public function sitemap() {
		$this->RequestHandler->respondAs('xml');
		Configure::write('debug', 0);
		$this->layout = 'ajax';
		$this->loadModel("Location");
		$urls = $this->Location->find("list",array("conditions"=>array("is_paid"=>1),"fields"=>array("id","seourl")));
		$this->set("urls",$urls);
		
	}
	
	public function pricing() {
		$this->set("title_for_layout","Great Offers For Merchant | Moola Rewards");
		$this->set("keywords","Offers For Merchant, Cheap Deals For Merchant");
		$this->layout = "mooladesignmyvoucher";
		$this->loadModel("Plan");
		$plan = $this->Plan->find("first",array("conditions"=>array("is_active"=>1)));
		$this->set("plan",$plan);
	}
	
	public function about() {
		$this->set("title_for_layout","You Need To Know About Us | Moola Rewards");
		$this->set("keywords","About Moola Rewards, Moola Rewards");
		$this->layout = "mooladesignmyvoucher";
	}	
	public function help() {
		$this->set("title_for_layout","How Can I Help You | Moola Rewards");
		$this->set("keywords","Help , Moola Rewards");
		$this->layout = "mooladesignmyvoucher";
	}	
	public function account_confirmation() {
		$this->set("title_for_layout","Moola : Congrats!");
		$this->set("keywords","Account Confirmation , Moola Rewards");
		$this->layout = "previewmoolavoucher";
	}	
	public function contact_us() {
		$this->set("title_for_layout","Contact Us | Moola Rewards");
		$this->set("keywords","Contact Us , Moola Rewards");
		$this->layout = "mooladesignmyvoucher";	
		
		if ( $this->request->is("post") ) {
			$this->autoRender = false;
			$response = array("status"=>false);
			if ($this->verifyRecatpcha($this->request->data)) {
				//pr($this->request->data);die;
				$this->getMailData("CONTACT_US");				
				$this->mailBody = str_replace("{name}",$this->request->data['Contact']['name'],$this->mailBody);
				$this->mailBody = str_replace("{phone}",$this->request->data['Contact']['phone'],$this->mailBody);
				$this->mailBody = str_replace("{email}",$this->request->data['Contact']['email'],$this->mailBody);
				$this->mailBody = str_replace("{message}",$this->request->data['Contact']['message'],$this->mailBody);
				//$this->mailBody = str_replace("{support_mail}","support@moolarewards.co.uk",$this->mailBody);				
				//$this->sendMail("mandeepteja@zestminds.com");
				 $this->sendMail("stuart@chillilogic.com");			
				$response = array("status"=>true,"url"=>"contact_us");
			}
			else {
				$response = array("status"=>false,"message"=>"Invalid captcha.");					
			}
			echo json_encode($response);
			die;
		}
	}
	public function advertiser_faqs() {
		$this->layout = "mooladesignmyvoucher";
		$this->set("title_for_layout","Merchant Support FAQ's | Moola Rewards");
		$this->set("keywords","Merchant Support FAQ's, Moola Rewards");
	}
	public function student_support_faqs() {
		$this->layout = "mooladesignmyvoucher";
		$this->set("title_for_layout","Student Support FAQ's | Moola Rewards");
		$this->set("keywords","Student Support FAQ's, Moola Rewards");
	}
	 public function message_confirm() {
		$this->layout = "mooladesignmessagescreen";
	}
	public function message_confirm_link() {
		$this->layout = "mooladesignmessagescreen";
	}
	public function message_forget_password() {
		$this->layout = "mooladesignmessagescreen";
	}
	public function coming_soon() {
		
		$this->layout = "mooladesigncomingsoon";
		$this->loadModel("Newsletter");
		$this->set("title_for_layout","Coming Soon | Moolarewards ");
		if ($this->request->is('post')) {	
			$this->Newsletter->create();
			$this->request->data["Newsletter"]["username"]=$this->request->data["notify"]["useremailnotify"];
			//pr($this->request->data);	die;
			if ($this->Newsletter->save($this->request->data)) {
				$this->Flash->success(__('We will notify you when our website is ready.'));				
				 $this->redirect( SITE_LINK);
			} else {
				$this->Flash->error(__('Error found.Please, try again.'));
			}
		}	
	}
	//~ public function share_url() {
		//~ $url = SITE_LINK;
		//~ 
		//~ if(isset($this->request->query["os"]))
		//~ {
			//~ $data = $this->request->query;
			//~ if ( $data['os'] == "android" ){		
				//~ $url = 'http://play.google.com/store/apps/details?id=com.truecaller&hl=en';
			//~ } elseif ( $data['os'] == "ios" ){
				//~ $url = 'http://itunes.apple.com/lb/app/truecaller-caller-id-number/id448142450?mt=8';
			//~ } else {
				//~ $url = SITE_LINK;
			//~ }
		//~ }
		//~ $this->redirect($url);
	//~ }
	
	public function share_url() {	
		$this->layout = false;	
		if(isset($this->request->query["id"]))
		{	
			$isfb = stripos(strtolower($_SERVER['HTTP_REFERER']),'facebook');
			if ($isfb) {
				$iPod    = stripos($_SERVER['HTTP_USER_AGENT'],"iPod");
				$iPhone  = stripos($_SERVER['HTTP_USER_AGENT'],"iPhone");
				$iPad    = stripos($_SERVER['HTTP_USER_AGENT'],"iPad");
				$Android = stripos($_SERVER['HTTP_USER_AGENT'],"Android");

				//do something with this information
				if( $iPod || $iPhone ){
					die("1");
					//browser reported as an iPhone/iPod touch -- do something here
				}else if($iPad){
					die("2");
					//browser reported as an iPad -- do something here
				}else if($Android){
					die("3");
					//browser reported as an Android device -- do something here
				}else {
					$this->redirect(SITE_LINK);
				}
			}
				$id=$this->request->query["id"];
				$this->loadModel("Voucher");
				$this->Voucher->hasMany = $this->Voucher->belongsTo = $this->Voucher->hasOne = array();
				$this->Voucher->belongsTo = array(
					"UserDetail" => array(
						"className" => "UserDetail",
						"foreignKey" => "",
						"type" => "Inner",
						"conditions"=>"UserDetail.user_id=Voucher.user_id"
					)
				); 
				
				
			if (!$this->Voucher->exists($id)) {
				throw new NotFoundException(__('Invalid voucher'));
			}
			
			$options = array('conditions' => array('Voucher.id'=> $id));
			$voucher =$this->Voucher->find('first', $options);		
			//pr($voucher);
			$this->set(compact("voucher"));		
		} else {
			$this->redirect(SITE_LINK);
		}		
	}
	
	public function pages_database()
	{	//pr($this->params->url);die;
		$this->loadModel("CmsPage");
	
		if ($this->Session->read("Auth.User.id")) 
		{
			$this->layout = "mooladesignmyvoucher";		
		}
		else
		{
			$this->layout = "moolahomedesign";	
		}
			
		if(isset($this->params->url) || !empty($this->params->url))
		{
			$slug=$this->params->url;
			$conditions=array('conditions' =>array("seo_url"=>$slug,"is_active"=>1));	
			$tmp = $this->CmsPage->find('first', $conditions);	
			$this->set("title_for_layout",$tmp['CmsPage']['header']." | Moolarewards ");
			$this->set("keywords",$tmp['CmsPage']['meta_keyword']);			
			$this->set('CmsPage', $tmp);
		}
		else
		{
			$this->redirect(SITE_LINK);
		}
	}
	
	//~ public function students(){
	//~ 
		//~ 
	//~ }
	//~ 
/**
 * Displays a view
 *
 * @return CakeResponse|null
 * @throws ForbiddenException When a directory traversal attempt.
 * @throws NotFoundException When the view file could not be found
 *   or MissingViewException in debug mode.
 */
	public function display() {
		$path = func_get_args();

		$count = count($path);
		if (!$count) {
			return $this->redirect('/');
		}
		if (in_array('..', $path, true) || in_array('.', $path, true)) {
			throw new ForbiddenException();
		}
		$page = $subpage = $title_for_layout = null;

		if (!empty($path[0])) {
			$page = $path[0];
		}
		if (!empty($path[1])) {
			$subpage = $path[1];
		}
		if (!empty($path[$count - 1])) {
			$title_for_layout = Inflector::humanize($path[$count - 1]);
		}
		$this->set(compact('page', 'subpage', 'title_for_layout'));

		try {
			$this->render(implode('/', $path));
		} catch (MissingViewException $e) {
			if (Configure::read('debug')) {
				throw $e;
			}
			throw new NotFoundException();
		}
	}
	
	function testpayment() {
		
		$paypal_profile="I-Y5LA02LV39V5";
			 if(!empty($paypal_profile)){					    
				 $result = $this->Paypal->get_subscription_status($paypal_profile);
				 //$paypal_amount=9.99;						 
				  $price=4.99;
				  $paypal_amount=9.99 ;
				  $vat = ceil(($price * 20)/100);
				  $price = $price+$vat;							 
				  $paypal_amount=number_format($paypal_amount-$price,2,".",",");						 
				   $result = $this->Paypal->update_profile($paypal_profile,$paypal_amount);
				  // pr($result);die;						
			  }
					  die;
	}
}
