<?php
App::uses('AppController', 'Controller');
/**
 * EmailTemplates Controller
 *
 * @property EmailTemplate $EmailTemplate
 * @property PaginatorComponent $Paginator
 */
class EmailTemplatesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');
	
/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->EmailTemplate->recursive = 0;
		$this->set('emailTemplates', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->EmailTemplate->exists($id)) {
			throw new NotFoundException(__('Invalid email template'));
		}
		$options = array('conditions' => array('EmailTemplate.' . $this->EmailTemplate->primaryKey => $id));
		$this->set('emailTemplate', $this->EmailTemplate->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->EmailTemplate->create();
			if ($this->EmailTemplate->save($this->request->data)) {
				$this->Flash->success(__('The email template has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The email template could not be saved. Please, try again.'));
			}
		}
		$languages = $this->EmailTemplate->Language->find('list');
		$this->set(compact('languages'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->EmailTemplate->exists($id)) {
			throw new NotFoundException(__('Invalid email template'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->EmailTemplate->save($this->request->data)) {
				$this->Flash->success(__('The email template has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The email template could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('EmailTemplate.' . $this->EmailTemplate->primaryKey => $id));
			$this->request->data = $this->EmailTemplate->find('first', $options);
		}
		$languages = $this->EmailTemplate->Language->find('list');
		$this->set(compact('languages'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->EmailTemplate->id = $id;
		if (!$this->EmailTemplate->exists()) {
			throw new NotFoundException(__('Invalid email template'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->EmailTemplate->delete()) {
			$this->Flash->success(__('The email template has been deleted.'));
		} else {
			$this->Flash->error(__('The email template could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index($searchval = null,$limit=20) {
		$this->layout = "mooladesignadmin";
		$this->bulkactions();
		$this->set('records', array( '20' => '20', '30' => '30','40' => '40', '60' => '60','80' => '80','100' => '100'));
		
		if ( $this->request->is("get") ) {		
			isset($this->request->query["records"])?$limit=$this->request->query["records"]:'';
			
			/*if ( !empty($this->data['User']['searchval']) ) {
				$this->redirect(SITE_LINK.$url."?searchval=".$this->data['User']['searchval']);
			} else {
				$this->redirect(SITE_LINK."".$url);
			}*/
		}	
		$this->set('limit',$limit);	
		(isset($this->params["named"]["page"]))?$sno=(($this->params["named"]["page"]*$limit)-($limit-1)):$sno=1;
		
		if ( !empty($searchval) ) {
			$this->set("searchval",$searchval);
			$this->conditions = array("OR"=>array("EmailTemplate.header like"=> "%".$searchval."%","EmailTemplate.subject like"=> "%".$searchval."%"));
		}
		if ( $this->request->is("post") ) {			
		
			if ( !empty($this->data['EmailTemplate']['searchval']) )
				 {
			
				$this->redirect(SITE_LINK."email-templates/".$this->data['EmailTemplate']['searchval']);
				
			} 
			else { 
				$this->redirect(SITE_LINK."email-templates/");
			}
		}
		$this->set('sno',$sno);
		$this->paginate = array("limit"=>$limit);
		$this->EmailTemplate->recursive = 0;		
		$this->set('emailTemplates' , $this->Paginator->paginate($this->conditions));
	}



/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		$this->layout = "mooladesignadmin";
		if (!$this->EmailTemplate->exists($id)) {
			throw new NotFoundException(__('Invalid email template'));
		}
		$options = array('conditions' => array('EmailTemplate.' . $this->EmailTemplate->primaryKey => $id));
		$this->set('emailTemplate', $this->EmailTemplate->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		$this->layout = "mooladesignadmin";
		if ($this->request->is('post')) {
			//pr($this->request->data);
			//die;
			$this->EmailTemplate->create();
			if ($this->EmailTemplate->save($this->request->data)) {
				$this->Flash->success(__('The email template has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The email template could not be saved. Please, try again.'));
			}
		}
		$languages = $this->EmailTemplate->Language->find('list');
		$this->set(compact('languages'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		$this->layout = "mooladesignadmin";
		if (!$this->EmailTemplate->exists($id)) {
			throw new NotFoundException(__('Invalid email template'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->EmailTemplate->save($this->request->data)) {
				$this->Flash->success(__('The email template has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The email template could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('EmailTemplate.' . $this->EmailTemplate->primaryKey => $id));
			$this->request->data = $this->EmailTemplate->find('first', $options);
		}
		
		$this->set(compact('languages'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->EmailTemplate->id = $id;
		if (!$this->EmailTemplate->exists()) {
			throw new NotFoundException(__('Invalid email template'));
		}
		//$this->request->allowMethod('post', 'delete');
		if ($this->EmailTemplate->delete()) {
			$this->Flash->success(__('The email template has been deleted.'));
		} else {
			$this->Flash->error(__('The email template could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
	
// Send push messages by admin to students device based on university

	public function admin_send_notifications()
	{
		$this->layout = "mooladesignadmin";
		$this->loadModel("University");	
		$this->loadModel("UserUniversity");	
		$this->loadModel("UserDevice");	
		if ($this->request->is(array('post', 'put'))) {	
			$this->UserUniversity->hasMany = $this->UserUniversity->belongsTo = $this->UserUniversity->hasOne = array();
			$this->UserUniversity->belongsTo = array(
				'UserDevice' => array(
					'className' => 'UserDevice',
					'conditions' => 'UserDevice.user_id=UserUniversity.user_id',
					"type"=> "Inner",
					'fields' => '',
					'order' => ''
				)
			);
		
			$universities=$this->request->data['SendNotifications']["university"];			
			if(empty($universities))
			{
				$this->Flash->success(__('University not selected'));
			}
			else{
				$user_device_key=array();
				foreach ($universities as $key=>$val) {
					$conditions=array("UserUniversity.university_id"=>$val);
					$user_devices = $this->UserUniversity->find("all",array("conditions"=>$conditions,"fields"=>array("UserDevice.token as user_device_key","UserDevice.device_id as device_type"),"order"=>"UserUniversity.university_id asc"));
					if(count($user_devices)>0) 				
					$user_device_key[]=$user_devices[0]['UserDevice']['user_device_key'];
								
				}	
				//pr($user_device_key);die;
				$message=$this->request->data['SendNotifications']["message"];
				$notificationType=$this->request->data['SendNotifications']["type"];
				$msg=$this->push_message($user_device_key,$message,$notificationType);
					
				if($msg['success']>0 && $msg['failure']==0)
				{
					$this->Flash->success(__('Notifications successfully send'));
				}
				else
				{
					$this->Flash->error(__('Error occured while sending some notification. Please, try again.'));
				}
			}
		}
	   else {				
			
		} 
		    $university = $this->University->find("list",array("conditions"=>array("is_active"=>1)));
			$this->set(compact('university'));
			$this->set('notificationType', array( '1' => 'Promotion', '2' => 'Informative', '3' => 'Vouchers'));
	}	
	
	
	public function push_message($user_device_key,$message,$notificationType)
	{
		#API access key from Google API's Console
		//define( 'API_ACCESS_KEY', 'AAAAcrHv1cs:APA91bF-P2AvXNIvVCT6lip7Voc6xRpIeQ8dsUCBhBsf5q2pPuUBLADcPtxk_vVGqACA75dsrZeHCUDD3JAnGJm2syk9v6F9A_2s4LHlVnXYJB3ft-6fSPzS2uNGJdCWrMxU3i_Z31KZkRPsDJoWwtBXTgBWKmmXww' );
		//$registrationIds = $_GET['id']; // these are device token we will fetch from the database.

		#prep the bundle
			 $msg = array
				  (
				'body' 	=> $message,
				'type'	=> $notificationType,						
				'icon'	=> 'myicon',/*Default Icon*/
				'sound' => 'mySound'/*Default sound*/
				  );

			$fields = array
					(
						//'to' =>$registrationIds,
						'registration_ids' =>$user_device_key,
						//'to'=>'c2YKnIl1gaI:APA91bHmZlT19NojIVExY_ZCeGhRsn3C3sQB4j1_ycMqWwy989XWJvXCe2vxUBJ5aJoGh8rGL9GZoxmGis4lSeNJesKGn-tvWlCFqGw_SG3nOGzfLx3WTrJvLE0-kuGq30pvTIv95MDl99ylF0GE1c5WhRL6F7k-YA',
						'notification' =>  $msg 
						//'notificationtype'	=> $notificationType
					);
			

			$headers = array
					(
						'Authorization: key=' . API_ACCESS_KEY,
						'Content-Type: application/json'
					);

				#Send Reponse To FireBase Server	
						$ch = curl_init();
						curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
						curl_setopt( $ch,CURLOPT_POST, true );
						curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
						curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
						curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
						curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
						$cexecute = curl_exec($ch );
						curl_close( $ch );

				#Echo Result Of FireBase Server
				//echo $result;die;
				 $result=json_decode($cexecute,true);
				return $result;
	}
}
