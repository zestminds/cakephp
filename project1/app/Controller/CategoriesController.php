<?php
App::uses('AppController', 'Controller');
/**
 * Categories Controller
 *
 * @property Category $Category
 * @property PaginatorComponent $Paginator
 */
class CategoriesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Category->recursive = 0;
		$this->set('categories', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Category->exists($id)) {
			throw new NotFoundException(__('Invalid category'));
		}
		$options = array('conditions' => array('Category.' . $this->Category->primaryKey => $id));
		$this->set('category', $this->Category->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Category->create();
			if ($this->Category->save($this->request->data)) {
				$this->Flash->success(__('The category has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The category could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Category->exists($id)) {
			throw new NotFoundException(__('Invalid category'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Category->save($this->request->data)) {
				$this->Flash->success(__('The category has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The category could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Category.' . $this->Category->primaryKey => $id));
			$this->request->data = $this->Category->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Category->id = $id;
		if (!$this->Category->exists()) {
			throw new NotFoundException(__('Invalid category'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Category->delete()) {
			$this->Flash->success(__('The category has been deleted.'));
		} else {
			$this->Flash->error(__('The category could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index($limit=20) {
		
		$this->Category->recursive = 0;	
		$this->layout = "mooladesignadmin";
		$url = "manage-categories";	
		$this->bulkactions();
		$this->set('records', array( '20' => '20', '30' => '30','40' => '40', '60' => '60','80' => '80','100' => '100'));
		
		if ( $this->request->is("get") ) {		
			isset($this->request->query["records"])?$limit=$this->request->query["records"]:'';
			
			/*if ( !empty($this->data['User']['searchval']) ) {
				$this->redirect(SITE_LINK.$url."?searchval=".$this->data['User']['searchval']);
			} else {
				$this->redirect(SITE_LINK."".$url);
			}*/
		}	
		$this->set('limit',$limit);	
		(isset($this->params["named"]["page"]))?$sno=(($this->params["named"]["page"]*$limit)-($limit-1)):$sno=1;
		
		if ( $this->request->query("searchval") && !empty($this->request->query("searchval")) ) {			
			$this->set("searchval",$this->request->query("searchval"));
			$searchval = strtolower(trim($this->request->query("searchval")));
			$this->conditions = array("OR"=>array("LOWER(Category.title) like"=> "%".$searchval."%",));
		}
		if ( $this->request->is("post") ) {
			if ( !empty($this->data['Category']['searchval']) ) {
				$this->redirect(SITE_LINK.$url."?searchval=".$this->data['Category']['searchval']);
			} else {
				$this->redirect(SITE_LINK."".$url);
			}
		}
		$this->set('sno',$sno);
		$this->paginate = array("order"=>"Category.created desc","limit"=>$limit);
		//$this->paginate = array("order"=>"Voucher.created desc","limit"=>$limit);				
		$this->set('categories', $this->Paginator->paginate($this->conditions));
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		$this->layout = "mooladesignadmin";
		if (!$this->Category->exists($id)) {
			throw new NotFoundException(__('Invalid category'));
		}
		$options = array('conditions' => array('Category.' . $this->Category->primaryKey => $id));
		$this->set('category', $this->Category->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		$this->layout = "mooladesignadmin";
		//$this->jsArray[] = array("jquery.awesome-cropper","jquery.imgareaselect");			
		//$this->cssArray[] = array("imgareaselect-default","jquery.awesome-cropper");	
		$this->jsArray[] = array("cropper.min","main");
		$this->cssArray[] = array("cropper.min","main");	

		if ($this->request->is('post')) {
			$this->Category->create();
			if (isset($this->request->data['Category']['tmpImage']) && !empty($this->request->data['Category']['tmpImage'])) {				
				$this->genImage($this->request->data['Category']['tmpImage'],"category");
				$this->request->data['Category']['image'] = $this->imagename;
			} else {
				$this->request->data['Category']['image'] = "";
			}
			if ($this->Category->save($this->request->data)) {
				$this->Flash->success(__('The category has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The category could not be saved. Please, try again.'));
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		$this->layout = "mooladesignadmin";
		$this->jsArray[] = array("cropper.min","main");
		$this->cssArray[] = array("cropper.min","main");		

		if (!$this->Category->exists($id)) {
			throw new NotFoundException(__('Invalid category'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if($this->request->data['Category']['tmpImage']=="haveimage")
			{
				unset($this->request->data['Category']['image']);
			}
			else {				
				$this->genImage($this->request->data['Category']['tmpImage'],"category");
				$this->request->data['Category']['image'] = $this->imagename;
			} 
			if ($this->Category->save($this->request->data)) {
				$this->Flash->success(__('The category has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The category could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Category.' . $this->Category->primaryKey => $id));
			$this->request->data = $this->Category->find('first', $options);
			$this->set("category",$this->request->data);
		}
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Category->id = $id;
		if (!$this->Category->exists()) {
			throw new NotFoundException(__('Invalid category'));
		}
		
		if ($this->Category->delete()) {
			$this->Flash->success(__('The category has been deleted.'));
		} else {
			$this->Flash->error(__('The category could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
