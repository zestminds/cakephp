<?php
App::uses('AppController', 'Controller');
/**
 * Billings Controller
 *
 * @property Billing $Billing
 * @property PaginatorComponent $Paginator
 */
class BillingsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		
		$this->Billing->recursive = 0;
		$this->set('billings', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Billing->exists($id)) {
			throw new NotFoundException(__('Invalid billing'));
		}
		$options = array('conditions' => array('Billing.' . $this->Billing->primaryKey => $id));
		$this->set('billing', $this->Billing->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Billing->create();
			if ($this->Billing->save($this->request->data)) {
				$this->Flash->success(__('The billing has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The billing could not be saved. Please, try again.'));
			}
		}
		$users = $this->Billing->User->find('list');
		$plans = $this->Billing->Plan->find('list');
		$this->set(compact('users', 'plans'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Billing->exists($id)) {
			throw new NotFoundException(__('Invalid billing'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Billing->save($this->request->data)) {
				$this->Flash->success(__('The billing has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The billing could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Billing.' . $this->Billing->primaryKey => $id));
			$this->request->data = $this->Billing->find('first', $options);
		}
		$users = $this->Billing->User->find('list');
		$plans = $this->Billing->Plan->find('list');
		$this->set(compact('users', 'plans'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Billing->id = $id;
		if (!$this->Billing->exists()) {
			throw new NotFoundException(__('Invalid billing'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Billing->delete()) {
			$this->Flash->success(__('The billing has been deleted.'));
		} else {
			$this->Flash->error(__('The billing could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index($limit=20) {
		$this->layout = "mooladesignadmin";
		$this->Billing->recursive = 0;		
		$this->bulkactions();
		$url='view-sales';
		$this->set('records', array( '20' => '20','30' => '30', '40' => '40','60' => '60', '80' => '80','100' => '100'));
		if ( $this->request->is("get") ) {		
			isset($this->request->query["records"])?$limit=$this->request->query["records"]:'';	
		}	
		$this->set('limit',$limit);	
		(isset($this->params["named"]["page"]))?$sno=(($this->params["named"]["page"]*$limit)-($limit-1)):$sno=1;
		if ( $this->request->query("searchval") && !empty($this->request->query("searchval")) ) {
		
			$this->set("searchval",$this->request->query("searchval"));
			$searchval = strtolower(trim($this->request->query("searchval")));
			$this->conditions = array("OR"=>array("LOWER(UserDetail.business) like"=> "%".$searchval."%"));
		}
		if ( $this->request->is("post") ) {
			if ( !empty($this->data['Billing']['searchval']) ) {
				$this->redirect(SITE_LINK.$url."?searchval=".$this->data['Billing']['searchval']);				
			} else {
				$this->redirect(SITE_LINK."".$url);
			}
		}
		
		$this->set('sno',$sno);
		
		$this->paginate = array("order"=>"Billing.renew_date desc","limit"=>$limit);
		$this->set('billings', $this->Paginator->paginate($this->conditions));
		
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->Billing->exists($id)) {
			throw new NotFoundException(__('Invalid billing'));
		}
		$options = array('conditions' => array('Billing.' . $this->Billing->primaryKey => $id));
		$this->set('billing', $this->Billing->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Billing->create();
			if ($this->Billing->save($this->request->data)) {
				$this->Flash->success(__('The billing has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The billing could not be saved. Please, try again.'));
			}
		}
		$users = $this->Billing->User->find('list');
		$plans = $this->Billing->Plan->find('list');
		$this->set(compact('users', 'plans'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->Billing->exists($id)) {
			throw new NotFoundException(__('Invalid billing'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Billing->save($this->request->data)) {
				$this->Flash->success(__('The billing has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The billing could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Billing.' . $this->Billing->primaryKey => $id));
			$this->request->data = $this->Billing->find('first', $options);
		}
		$users = $this->Billing->User->find('list');
		$plans = $this->Billing->Plan->find('list');
		$this->set(compact('users', 'plans'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Billing->id = $id;
		if (!$this->Billing->exists()) {
			throw new NotFoundException(__('Invalid billing'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Billing->delete()) {
			$this->Flash->success(__('The billing has been deleted.'));
		} else {
			$this->Flash->error(__('The billing could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
