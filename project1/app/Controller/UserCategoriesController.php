<?php
App::uses('AppController', 'Controller');
/**
 * UserCategories Controller
 *
 * @property UserCategory $UserCategory
 * @property PaginatorComponent $Paginator
 */
class UserCategoriesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->UserCategory->recursive = 0;
		$this->set('userCategories', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->UserCategory->exists($id)) {
			throw new NotFoundException(__('Invalid user category'));
		}
		$options = array('conditions' => array('UserCategory.' . $this->UserCategory->primaryKey => $id));
		$this->set('userCategory', $this->UserCategory->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->UserCategory->create();
			if ($this->UserCategory->save($this->request->data)) {
				$this->Flash->success(__('The user category has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The user category could not be saved. Please, try again.'));
			}
		}
		$categories = $this->UserCategory->Category->find('list');
		$users = $this->UserCategory->User->find('list');
		$this->set(compact('categories', 'users'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->UserCategory->exists($id)) {
			throw new NotFoundException(__('Invalid user category'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->UserCategory->save($this->request->data)) {
				$this->Flash->success(__('The user category has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The user category could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('UserCategory.' . $this->UserCategory->primaryKey => $id));
			$this->request->data = $this->UserCategory->find('first', $options);
		}
		$categories = $this->UserCategory->Category->find('list');
		$users = $this->UserCategory->User->find('list');
		$this->set(compact('categories', 'users'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->UserCategory->id = $id;
		if (!$this->UserCategory->exists()) {
			throw new NotFoundException(__('Invalid user category'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->UserCategory->delete()) {
			$this->Flash->success(__('The user category has been deleted.'));
		} else {
			$this->Flash->error(__('The user category could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->UserCategory->recursive = 0;
		$this->set('userCategories', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->UserCategory->exists($id)) {
			throw new NotFoundException(__('Invalid user category'));
		}
		$options = array('conditions' => array('UserCategory.' . $this->UserCategory->primaryKey => $id));
		$this->set('userCategory', $this->UserCategory->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->UserCategory->create();
			if ($this->UserCategory->save($this->request->data)) {
				$this->Flash->success(__('The user category has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The user category could not be saved. Please, try again.'));
			}
		}
		$categories = $this->UserCategory->Category->find('list');
		$users = $this->UserCategory->User->find('list');
		$this->set(compact('categories', 'users'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->UserCategory->exists($id)) {
			throw new NotFoundException(__('Invalid user category'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->UserCategory->save($this->request->data)) {
				$this->Flash->success(__('The user category has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The user category could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('UserCategory.' . $this->UserCategory->primaryKey => $id));
			$this->request->data = $this->UserCategory->find('first', $options);
		}
		$categories = $this->UserCategory->Category->find('list');
		$users = $this->UserCategory->User->find('list');
		$this->set(compact('categories', 'users'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->UserCategory->id = $id;
		if (!$this->UserCategory->exists()) {
			throw new NotFoundException(__('Invalid user category'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->UserCategory->delete()) {
			$this->Flash->success(__('The user category has been deleted.'));
		} else {
			$this->Flash->error(__('The user category could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
