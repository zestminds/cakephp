<?php
App::uses('AppController', 'Controller');
/**
 * VoucherSales Controller
 *
 * @property VoucherSale $VoucherSale
 * @property PaginatorComponent $Paginator
 */
class VoucherStatsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->layout = "mooladesignmyvoucher";	
		$this->jsArray[] = array("merchant_insights");
		$query = $this->request->query;
		
		$this->VoucherStat->belongsTo = array(
			"Voucher"=> array(
				"className" => "Voucher",
				"foreignKey"=>"voucher_id",
				"type"=> "Inner"
			),
			"UserDetail"=> array(
				"className" => "UserDetail",
				"foreignKey"=>false,			
				"type"=> "Inner",
				"conditions"=> "VoucherStat.user_id = UserDetail.user_id"
			)
		);
		if( !isset($query['interval']) || empty($query['interval']) ) {
			$interval ="7 Day	";			
		} else {	
			$interval=$query['interval'];
		}
		
		$conditions = array("Voucher.user_id"=> $this->Session->read('Auth.User.id'),"VoucherStat.created >=(CURDATE() - INTERVAL ".$interval.")");
		
		if ( !isset($query['type']) || empty($query['type']) ) {
			$conditions = array_merge($conditions,array("VoucherStat.stat_type" => 'Reach'));
			$voucher_status='Reach';
		}	 else {
			$conditions = array_merge($conditions,array("VoucherStat.stat_type" => $query['type']));
			$voucher_status=$query['type'];
		}
		
		
		$this->VoucherStat->virtualFields = array("count"=>"select count(*) from voucher_stats where voucher_id = Voucher.id and stat_type = '".$voucher_status."' and voucher_stats.created >=(CURDATE() - INTERVAL ".$interval.")");
		
		if( $this->request->is("ajax")) {
			$this->layout = false;
			$this->render = false;
			$this->paginate = array("order"=>"VoucherStat.created desc","limit"=>"16","fields"=>array("Voucher.id","Voucher.image","VoucherStat.count"),"group"=>"Voucher.id");
			$data = $this->Paginator->paginate($conditions);
			$tmpData = array();			
			foreach( $data as $key=>$val  ) {
				$tmpData[]= array("image"=>$val['Voucher']['image'],"count"=>$val['VoucherStat']['count']);	
			}				
			echo json_encode($tmpData);	
			die;		
		} else {
			$this->paginate = array("order"=>"VoucherStat.created desc","limit"=>"16","fields"=>array("Voucher.id","Voucher.image","VoucherStat.count"),"group"=>"Voucher.id");			
			$this->set('voucher_live_status', $this->Paginator->paginate($conditions));			
		}
		
		$this->set("voucher_status",$voucher_status);
		$this->set(compact("interval"));
				
		
		//Audience	By Days
		$condition=array("Voucher.user_id"=> $this->Session->read('Auth.User.id'));

		$conditions=array_merge(array($condition,"VoucherStat.created >=(CURDATE() - INTERVAL 6 Day)"));
		$this->VoucherStat->virtualFields = array("startDate"=>"CURDATE() - INTERVAL 6 Day");
		
		$vouchers_weekly = $this->VoucherStat->find("all",array("conditions"=>$conditions,"fields"=>array("count(VoucherStat.id) as cnt",'DAYNAME(VoucherStat.created) as "dayname"',"date(VoucherStat.created) as date","VoucherStat.startDate"),"group"=>"date(VoucherStat.created)","order"=>"VoucherStat.created asc"));
		
		$startDate = (isset($vouchers_weekly[0]['VoucherStat']['startDate']))?$vouchers_weekly[0]['VoucherStat']['startDate']:'';
		foreach($vouchers_weekly as $key=>$val){			
			$dayArr[$val[0]['date']] = $val[0]['cnt'];		
		}
		
		$finalArr = array();
		for ( $i = 0; $i<=6; $i++ ) {
			$tmpDate = date("Y-m-d",strtotime($startDate." +".$i." days"));
			$tmpDay = date("D",strtotime($startDate." +".$i." days"));
			$finalArr[$tmpDay] = isset($dayArr[$tmpDate])?$dayArr[$tmpDate]:0;
		}
		
		//Audience	By Hours
		$conditions=array_merge(array($condition,"VoucherStat.created >=(CURDATE() - INTERVAL 24 Hour)"));
		$this->VoucherStat->virtualFields = array("startTime"=>" (CURTIME())");
		$vouchers_hourly = $this->VoucherStat->find("all",array("conditions"=>$conditions,"fields"=>array("count(VoucherStat.id) as cnt",'DAYNAME(VoucherStat.created) as "dayname"',"time(VoucherStat.created) as time",'startTime'),"group"=>"time(VoucherStat.created)","order"=>"VoucherStat.created asc"));
		
		$startTime = (isset($vouchers_hourly[0]['VoucherStat']['startTime']))?$vouchers_hourly[0]['VoucherStat']['startTime']:'';
		foreach($vouchers_hourly as $key=>$val){	
			 $tempTime= date("H",strtotime($val[0]['time'])).":00:00";
			 $HourArr[$tempTime] = $val[0]['cnt'];		
		}
		$finalHourArr = array();						
		for ( $i = 0; $i<=24; $i++ ) {
			 $tmpTime = date("H",strtotime("+".$i." hours")).":00:00";	
			$finalHourArr[$tmpTime] = isset($HourArr[$tmpTime])?$HourArr[$tmpTime]:0;
		}
		
		$this->set(compact("finalArr"));
		$this->set(compact("finalHourArr"));
		
		//Gender
		
		$this->VoucherStat->virtualFields = array("totalCount"=>"SELECT count( voucher_stats.id)FROM voucher_stats, vouchers WHERE voucher_stats.voucher_id = vouchers.id AND vouchers.user_id=".$this->Session->read('Auth.User.id'));
		
		$vouchers_gender = $this->VoucherStat->find("all",array("conditions"=>$condition,"fields"=>array("count(VoucherStat.id) as cnt","UserDetail.gender","totalCount"),"group"=>"UserDetail.gender"));
		
		$genderArr = array();
		if ( !empty($vouchers_gender) ) {
			foreach($vouchers_gender as $key=>$val) {
				if ( isset($val['UserDetail']['gender']) ) {
					$genderArr[] = array("gender"=>$val['UserDetail']['gender'],"val"=>round(((($val[0]['cnt'])/$val['VoucherStat']['totalCount'])*100),2),"image"=>"gender".$val['UserDetail']['gender'].".png");
				}
			}
		}
		$this->set(compact("genderArr"));
		
	}
 
 
	public function index_bak() {
		
		$malecount=$femalecount=0;
		$this->layout = "mooladesignmyvoucher";	
		$this->jsArray[] = array("merchant_insights");
		$query = $this->request->query;
		$conditions = array(1=>1);
		
		if( !isset($query['interval']) || empty($query['interval']) ) {
			$interval ="7 Day	";			
		} else {	
			$interval=$query['interval'];
		}		
		
		if ( !isset($query['type']) || empty($query['type']) ) {
			$conditions = array("stat_type" => 'Reach',"Voucher.user_id"=> $this->Session->read('Auth.User.id'),"VoucherStat.created >=(CURDATE() - INTERVAL ".$interval.")");
			$voucher_status='Reach';
		} else {
			$conditions = array("stat_type" => $query['type'],"Voucher.user_id"=> $this->Session->read('Auth.User.id'),"VoucherStat.created >=(CURDATE() - INTERVAL ".$interval.")");
			$voucher_status=$query['type'];
		}
		$this->set("voucher_status",$voucher_status);
		
		$this->loadModel("VoucherStat");
		$this->VoucherStat->belongsTo = array(
			"Voucher"=> array(
				"className" => "Voucher",
				"foreignKey"=>"voucher_id",
				"type"=> "Inner"
			),
			"UserDetail"=> array(
				"className" => "UserDetail",
				"foreignKey"=>false,
				"conditions"=>"VoucherStat.user_id = UserDetail.user_id",
				"type"=> "Inner"
			)
		);
		
		$voucher_live_status = $this->VoucherStat->find("all",array("conditions"=>$conditions,"fields"=>array("count(VoucherStat.id) as count",'Voucher.image'),'group' => 'VoucherStat.voucher_id'));		
		$this->set(compact("voucher_live_status"));
		
		//Gender
		$conditions=array("Voucher.user_id"=> $this->Session->read('Auth.User.id'));
		$this->VoucherStat->virtualFields = array("totalCount"=>"SELECT count( voucher_stats.id)FROM voucher_stats, vouchers WHERE voucher_stats.voucher_id = vouchers.id AND vouchers.user_id=".$this->Session->read('Auth.User.id'));
		
		$vouchers_gender = $this->VoucherStat->find("all",array("conditions"=>$conditions,"fields"=>array("count(VoucherStat.id) as cnt","UserDetail.gender","totalCount"),"group"=>"UserDetail.gender"));
		
		$genderArr = array();
		if ( !empty($vouchers_gender) ) {
			foreach($vouchers_gender as $key=>$val) {
				if ( isset($val['UserDetail']['gender']) ) {
					$genderArr[] = array("gender"=>$val['UserDetail']['gender'],"val"=>round(((($val[0]['cnt'])/$val['VoucherStat']['totalCount'])*100),2),"image"=>"gender".$val['UserDetail']['gender'].".png");
				}
			}
		}
		
		//Audience	By Days
		$conditions_audi=array("Voucher.user_id"=> $this->Session->read('Auth.User.id'),"VoucherStat.created >=(CURDATE() - INTERVAL 7 Day)");
		$this->VoucherStat->virtualFields = array("startDate"=>"CURDATE() - INTERVAL 7 Day");
		$vouchers_weekly = $this->VoucherStat->find("all",array("conditions"=>$conditions_audi,"fields"=>array("count(VoucherStat.id) as cnt",'DAYNAME(VoucherStat.created) as "dayname"',"date(VoucherStat.created) as date","VoucherStat.startDate"),"group"=>"date(VoucherStat.created)","order"=>"VoucherStat.created asc"));
		
		$startDate = (isset($vouchers_weekly[0]['VoucherStat']['startDate']))?$vouchers_weekly[0]['VoucherStat']['startDate']:'';
		foreach($vouchers_weekly as $key=>$val){			
			$dayArr[$val[0]['date']] = $val[0]['cnt'];		
		}
		$finalArr = array();
		for ( $i = 0; $i<=6; $i++ ) {
			$tmpDate = date("Y-m-d",strtotime($startDate." +".$i." days"));
			$tmpDay = date("D",strtotime($startDate." +".$i." days"));
			$finalArr[$tmpDay] = isset($dayArr[$tmpDate])?$dayArr[$tmpDate]:0;
		}
		
		//Audience	By Hours
		$conditions_hour=array("Voucher.user_id"=> $this->Session->read('Auth.User.id'),"VoucherStat.created >=(CURDATE() - INTERVAL 24 Hour)");
		$this->VoucherStat->virtualFields = array("startTime"=>" (CURTIME())");
		$vouchers_hourly = $this->VoucherStat->find("all",array("conditions"=>$conditions_hour,"fields"=>array("count(VoucherStat.id) as cnt",'DAYNAME(VoucherStat.created) as "dayname"',"time(VoucherStat.created) as time",'startTime'),"group"=>"time(VoucherStat.created)","order"=>"VoucherStat.created asc"));
		
		$startTime = (isset($vouchers_hourly[0]['VoucherStat']['startTime']))?$vouchers_hourly[0]['VoucherStat']['startTime']:'';
		foreach($vouchers_hourly as $key=>$val){	
			 $tempTime= date("H",strtotime($val[0]['time'])).":00:00";
			 $HourArr[$tempTime] = $val[0]['cnt'];		
		}
		$finalHourArr = array();						
		for ( $i = 0; $i<=24; $i++ ) {
			 $tmpTime = date("H",strtotime("+".$i." hours")).":00:00";	
			$finalHourArr[$tmpTime] = isset($HourArr[$tmpTime])?$HourArr[$tmpTime]:0;
		}
		$this->set(compact("genderArr"));
		$this->set(compact("finalArr"));
		$this->set(compact("finalHourArr"));
		
		
	}


/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->VoucherSale->exists($id)) {
			throw new NotFoundException(__('Invalid voucher sale'));
		}
		$options = array('conditions' => array('VoucherSale.' . $this->VoucherSale->primaryKey => $id));
		$this->set('voucherSale', $this->VoucherSale->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->VoucherSale->create();
			if ($this->VoucherSale->save($this->request->data)) {
				$this->Flash->success(__('The voucher sale has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The voucher sale could not be saved. Please, try again.'));
			}
		}
		$users = $this->VoucherSale->User->find('list');
		$vouchers = $this->VoucherSale->Voucher->find('list');
		$this->set(compact('users', 'vouchers'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->VoucherSale->exists($id)) {
			throw new NotFoundException(__('Invalid voucher sale'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->VoucherSale->save($this->request->data)) {
				$this->Flash->success(__('The voucher sale has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The voucher sale could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('VoucherSale.' . $this->VoucherSale->primaryKey => $id));
			$this->request->data = $this->VoucherSale->find('first', $options);
		}
		$users = $this->VoucherSale->User->find('list');
		$vouchers = $this->VoucherSale->Voucher->find('list');
		$this->set(compact('users', 'vouchers'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->VoucherSale->id = $id;
		if (!$this->VoucherSale->exists()) {
			throw new NotFoundException(__('Invalid voucher sale'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->VoucherSale->delete()) {
			$this->Flash->success(__('The voucher sale has been deleted.'));
		} else {
			$this->Flash->error(__('The voucher sale could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->VoucherSale->recursive = 0;
		$this->set('voucherSales', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->VoucherSale->exists($id)) {
			throw new NotFoundException(__('Invalid voucher sale'));
		}
		$options = array('conditions' => array('VoucherSale.' . $this->VoucherSale->primaryKey => $id));
		$this->set('voucherSale', $this->VoucherSale->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->VoucherSale->create();
			if ($this->VoucherSale->save($this->request->data)) {
				$this->Flash->success(__('The voucher sale has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The voucher sale could not be saved. Please, try again.'));
			}
		}
		$users = $this->VoucherSale->User->find('list');
		$vouchers = $this->VoucherSale->Voucher->find('list');
		$this->set(compact('users', 'vouchers'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->VoucherSale->exists($id)) {
			throw new NotFoundException(__('Invalid voucher sale'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->VoucherSale->save($this->request->data)) {
				$this->Flash->success(__('The voucher sale has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The voucher sale could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('VoucherSale.' . $this->VoucherSale->primaryKey => $id));
			$this->request->data = $this->VoucherSale->find('first', $options);
		}
		$users = $this->VoucherSale->User->find('list');
		$vouchers = $this->VoucherSale->Voucher->find('list');
		$this->set(compact('users', 'vouchers'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->VoucherSale->id = $id;
		if (!$this->VoucherSale->exists()) {
			throw new NotFoundException(__('Invalid voucher sale'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->VoucherSale->delete()) {
			$this->Flash->success(__('The voucher sale has been deleted.'));
		} else {
			$this->Flash->error(__('The voucher sale could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
	
	//Admin Insights Students
	function admin_insights ($days=7) {	
		
			$this->layout = "mooladesignadmin";	
			$this->jsArray = "admin_insights";				
			$this->loadModel('User');
			$this->loadModel('University');
			$this->loadModel('UserUniversity');								
			$startDate=date("Y")."-01-01";			
			$todayDate=date("Y-m-d");			
			$select_univserity=$days_univserity=$conditions="";			
			$this->University->hasMany = $this->University->belongsTo = $this->University->hasOne = array();
			$this->University->belongsTo = array(
				"UserUniversity" => array(
					"className" => "UserUniversity",
					"foreignKey" => "",
					"type" => "Inner",
					"conditions"=>"UserUniversity.university_id=University.id"
				)
			); 
			$uni_regver=$uni_appdown=$uni_reachcon="All";
			$days_regver=$days_appdown=$days_reachcon="7";
			
			
			$this->set('uni_regver',$uni_regver);
			$this->set('days_regver',$days_regver);
			$this->set('uni_appdown',$uni_appdown);
			$this->set('days_appdown',$days_appdown);
			$this->set('uni_reachcon',$uni_reachcon);
			$this->set('days_reachcon',$days_reachcon);
			
			$this->set('show_regver',"weeks");
			$this->set('show_reachconv',"weeks");
			
			$this->set('records', array('7' => '7 Days','30' => '30 Days', '90' => '3 Months', '6 Months' => '6 Months','1 Year' => '1 Year'));
			$this->set('university',$this->University->find("list",array("conditions"=>array("is_active"=>1))));
		
	}	
	// Start  Total App Downloads Students
	public function admin_total_app_downloads()
	{	
			$this->layout = false;				
			$this->loadModel('User');
			$this->loadModel('University');
			$this->loadModel('UserUniversity');								
			$startDate=date("Y")."-01-01";			
			$todayDate=date("Y-m-d");
			$this->University->hasMany = $this->University->belongsTo = $this->University->hasOne = array();
			$this->University->belongsTo = array(
				"UserUniversity" => array(
					"className" => "UserUniversity",
					"foreignKey" => "",
					"type" => "Inner",
					"conditions"=>"UserUniversity.university_id=University.id"
				)
			); 				
				
			if($this->request->is("get") ) {	
								
				if(isset($this->request->query["days"]) && !empty($this->request->query["days"])){
					if($this->request->query["university"]=="")
					$uni_appdown="All";
					else
					$uni_appdown=$this->request->query["university"];						
					$days_appdown=$this->request->query["days"];					
					if($days_appdown=="7" || $days_appdown=="30" || $days_appdown=="90")
					{			
						$total_app_downloads=$this->total_app_downloads_days($days_appdown,$uni_appdown);	
					}
					else
					{						
						$total_app_downloads=$this->total_app_downloads_months($days_appdown,$uni_appdown);	
					}								
				}
				
			}
		
			//total app downloads and login				
			$this->set('total_app_downloads',$total_app_downloads);
			$this->set('uni_appdown',$uni_appdown);
			$this->set('days_appdown',$days_appdown);
	
	}
	public function total_app_downloads_days($days,$university)
	{
		if($university=="All")
		$conditions=array("User.is_active"=>1,"User.user_type_id"=>"3","User.created >=(CURDATE() - INTERVAL ".$days." Day)");
		else
		$conditions=array("UserUniversity.university_id"=>$university,"User.is_active"=>1,"User.user_type_id"=>"3","User.created >=(CURDATE() - INTERVAL ".$days." Day)");
		
		$this->UserUniversity->virtualFields =array("startDate"=>" (CURDATE() - INTERVAL ".$days." Day)");
		$appdownloads = $this->UserUniversity->find("all",array("conditions"=>$conditions,"fields"=>array("count(User.id) as cnt",'DAYNAME(User.created) as "dayname"',"date(User.created) as date","startDate"),"group"=>"date(User.created)","order"=>"User.created asc"));
		
		$startDate = (isset($appdownloads[0]['UserUniversity']['startDate']))?$appdownloads[0]['UserUniversity']['startDate']:'';
		$dayArr = array();
		foreach($appdownloads as $key=>$val){			
			$dayArr[$val[0]['date']] = $val[0]['cnt'];		
		}
		$finalArr = array();
		for ( $i = 0; $i<=$days; $i++ ) {
			
			$tmpDate = date("Y-m-d",strtotime($startDate." +".$i." days"));
			if($days==7){
				$tmpDay = date("D",strtotime($startDate." +".$i." days"));
				$finalArr[$tmpDay] = isset($dayArr[$tmpDate])?$dayArr[$tmpDate]:0;
			}
			else
			{	
				$tempshortdate=strtotime($tmpDate);
				$month=date("M j",$tempshortdate);			
				$finalArr[$month] = isset($dayArr[$tmpDate])?$dayArr[$tmpDate]:0;
			}
		}	
			return 	$finalArr;
			
	}	
	public function total_app_downloads_months($days,$university)
	{
		if($days=="6 Months")
		{
			$startmonth = date('Y-m-01 00:00:00', strtotime("-5 month"));
			$group=array("monthname");
		}
		else{
			$startmonth = date('Y-m-01 00:00:00', strtotime("-11 month"));
			$group=array("year","monthname");			
		}
		
		$endmonth = date('Y-m-d 00:00:00');
		$endmonthDate = strtotime(date('Y-m-01 00:00:00'));
		$tmpDate = $startmonth;
		
		$conditions=array("User.user_type_id"=>3,"date(User.created) BETWEEN date('".$startmonth."') AND date('".$endmonth."')");	
				
		if($university!="All")
		$conditions=array_merge(array("UserUniversity.university_id"=>$university));
		
		$this->UserUniversity->virtualFields =array("firstDate"=>"DATE_FORMAT(User.created,'%Y-%m-01 00:00:00')");		
		$appdownloads = $this->UserUniversity->find("all",array("conditions"=>$conditions,"fields"=>array("count(User.id) as cnt",'MONTHNAME(User.created) as "monthname"','Year(User.created) as year',"date(User.created) as date","firstDate"),"group"=>$group,"order"=>"User.created asc"));
		
		$tmpData = array();
		foreach($appdownloads as $key=>$val) {
			$tmpData[$val['UserUniversity']['firstDate']] = $val[0];
		}
		
		$data = array();
		$i = 0;
		$finalData = array();
		while ( $endmonthDate >= $tmpDate ) {
			if ( empty($i) ) {
				$tmpDate;
			} else {
				$tmpDate = date("Y-m-d 00:00:00",$tmpDate);
			}
			$month = date("F",strtotime($tmpDate));
			if ( isset($tmpData[$tmpDate]) ) {
				$finalData[$tmpData[$tmpDate]['monthname']] = $tmpData[$tmpDate]['cnt'];
			} else {
				$finalData[$month] = 0;
			}
			$tmpDate = strtotime($tmpDate." +1 Month");
			$i++;
		}
		return $finalData;
			
	}	
	// End  Total App Downloads Students
	
	
	// Total Logins  Students
	public function admin_total_logins()
	{		
			$this->layout = false;				
			$this->loadModel('User');
			$this->loadModel('University');
			$this->loadModel('UserUniversity');								
			$startDate=date("Y")."-01-01";			
			$todayDate=date("Y-m-d");
			$this->University->hasMany = $this->University->belongsTo = $this->University->hasOne = array();
			$this->University->belongsTo = array(
				"UserUniversity" => array(
					"className" => "UserUniversity",
					"foreignKey" => "",
					"type" => "Inner",
					"conditions"=>"UserUniversity.university_id=University.id"
				)
			); 					
			if($this->request->is("get") ) {
				if($this->request->query["days"])
				{				
					if(isset($this->request->query["days"]) && !empty($this->request->query["days"])){
						
						$uni=$this->request->query["university"];						
						$days=$this->request->query["days"];					
						if($days=="7" || $days=="30" || $days=="90")
						{			
							$total_logins=$this->total_logins_days($days,$uni);		
						}	
						else
						{						
							//$total_logins=$this->total_logins_months($days_appdown,$uni_appdown);		

						}						
					}
				}
			}
			//total app downloads and login
			
			$this->set('total_logins',$total_logins);
			$this->set('uni_appdown',$uni);
			$this->set('days_appdown',$days);
		
	}
	
	public function total_logins_days($days,$university)
	{		
		$conditions=array("User.is_active"=>1,"User.user_type_id"=>3,"User.created >=(CURDATE() - INTERVAL ".$days." Day)");
		
		if($university!="All")		
		$conditions=array_merge(array("UserUniversity.university_id"=>$university));
		
		$this->UserUniversity->virtualFields =array("startDate"=>" (CURDATE() - INTERVAL ".$days." Day)");
		$appdownloads = $this->UserUniversity->find("all",array("conditions"=>$conditions,"fields"=>array("count(User.id) as cnt",'DAYNAME(User.created) as "dayname"',"date(User.created) as date","startDate"),"group"=>"date(User.created)","order"=>"User.created asc"));		
		$startDate = (isset($appdownloads[0]['UserUniversity']['startDate']))?$appdownloads[0]['UserUniversity']['startDate']:'';
		$dayArr = array();
		foreach($appdownloads as $key=>$val){			
			$dayArr[$val[0]['date']] = $val[0]['cnt'];		
		}
		$finalArr = array();
		for ( $i = 0; $i<=$days; $i++ ) {
			$tmpDate = date("Y-m-d",strtotime($startDate." +".$i." days"));
			if($days==7){
				$tmpDay = date("D",strtotime($startDate." +".$i." days"));
				$finalArr[$tmpDay] = isset($dayArr[$tmpDate])?$dayArr[$tmpDate]:0;
			}
			else
			{	
				$tempshortdate=strtotime($tmpDate);
				$month=date("M j",$tempshortdate);		
				$finalArr[$month] = isset($dayArr[$tmpDate])?$dayArr[$tmpDate]:0;		
			}
		}
		return $finalArr;
	}
	
	public function total_logins_months($days,$university)
	{	
		
		if($days=="6 Months")
		{
			$startmonth = date('Y-m-01 00:00:00', strtotime("-5 month"));
			$group=array("monthname");
		}
		else{
			$startmonth = date('Y-m-01 00:00:00', strtotime("-11 month"));
			$group=array("year","monthname");			
		}
		
		$endmonth = date('Y-m-d 00:00:00');
		$endmonthDate = strtotime(date('Y-m-01 00:00:00'));
		$tmpDate = $startmonth;
		
		$conditions=array("User.user_type_id"=>3,"date(User.created) BETWEEN date('".$startmonth."') AND date('".$endmonth."')");
		
		if($university!="All")		
		$conditions=array_merge(array("UserUniversity.university_id"=>$university));
		
		$this->UserUniversity->virtualFields =array("firstDate"=>"DATE_FORMAT(User.created,'%Y-%m-01 00:00:00')");
		$appdownloads = $this->UserUniversity->find("all",array("conditions"=>$conditions,"fields"=>array("count(User.id) as cnt",'MONTHNAME(User.created) as "monthname"','Year(User.created) as year',"date(User.created) as date","firstDate"),"group"=>$group,"order"=>"User.created asc"));		
		
		$tmpData = array();
		foreach($appdownloads as $key=>$val) {
			$tmpData[$val['UserUniversity']['firstDate']] = $val[0];
		}
		
		$data = array();
		$i = 0;
		$finalData = array();
		while ( $endmonthDate >= $tmpDate ) {
			if ( empty($i) ) {
				$tmpDate;
			} else {
				$tmpDate = date("Y-m-d 00:00:00",$tmpDate);
			}
			$month = date("F",strtotime($tmpDate));
			if ( isset($tmpData[$tmpDate]) ) {
				$finalData[$tmpData[$tmpDate]['monthname']] = $tmpData[$tmpDate]['cnt'];
			} else {
				$finalData[$month] = 0;
			}
			$tmpDate = strtotime($tmpDate." +1 Month");
			$i++;
		}
		return $finalData;
	}
	
	//End  Total Logins  Students
	
	
	//Strat Facebook V/s Manual
	
	public function admin_total_registrations()
	{
		$this->layout = false;				
		$this->loadModel('User');
		$this->loadModel('University');
		$this->loadModel('UserUniversity');								
		$startDate=date("Y")."-01-01";			
		$todayDate=date("Y-m-d");
		$this->University->hasMany = $this->University->belongsTo = $this->University->hasOne = array();
		$this->University->belongsTo = array(
			"UserUniversity" => array(
				"className" => "UserUniversity",
				"foreignKey" => "",
				"type" => "Inner",
				"conditions"=>"UserUniversity.university_id=University.id"
			)
		); 	
		$todayDate=date("Y-m-d");
		if($this->request->is("get") ) {
			if($this->request->query["days"])
			{				
				if(isset($this->request->query["days"]) && !empty($this->request->query["days"])){
					if($this->request->query["university"]=="")
					$university="All";
					else
					$university=$this->request->query["university"];						
					$days=$this->request->query["days"];		
					if($days=="7" || $days=="30" || $days=="90")
					{					
						$total_app_facebook=$this->total_app_facebook_days($days,$university,"Facebook");	
						$total_app_manual=$this->total_app_facebook_days($days,$university,"Manual");	
					}
					else
					{							
						$total_app_facebook=$this->total_app_facebook_months($days,$university,"Facebook");
						$total_app_manual=$this->total_app_facebook_months($days,$university,"Manual");
					}
				}
			}
		}
		
		$this->set('total_app_facebook',$total_app_facebook);
		$this->set('total_app_manual',$total_app_manual);	
		$this->set('uni_regver',$university);
		$this->set('days_regver',$days);
		
	}	
	
	public function total_app_facebook_days($days,$university,$identifier)
	{	
		$conditions=array("User.user_type_id"=>3,"User.created >=(CURDATE() - INTERVAL ".$days." Day)");
		if($identifier=="Facebook"){
			if($university=="All")
			{
				 $conditions=array_merge(array('not' => array('User.identifier' => null)));
			}
			else{
				$conditions=array_merge(array('not' => array('User.identifier' => null),"UserUniversity.university_id"=>$university));
			}
		}
		else{
			if($university=="All")
				$conditions=array_merge(array('User.identifier' => null));
			else
				$conditions=array_merge(array('User.identifier' => null,"UserUniversity.university_id"=>$university));
		}
				
		$this->UserUniversity->virtualFields =array("startDate"=>" (CURDATE() - INTERVAL ".$days." Day)");
		$appdownloads = $this->UserUniversity->find("all",array("conditions"=>$conditions,"fields"=>array("count(User.id) as cnt",'DAYNAME(User.created) as "dayname"',"date(User.created) as date","startDate",'User.identifier'),"group"=>"date(User.created)","order"=>"User.created asc"));		
		$startDate = (isset($appdownloads[0]['UserUniversity']['startDate']))?$appdownloads[0]['UserUniversity']['startDate']:''; 
		$dayArr = array();
		foreach($appdownloads as $key=>$val){			
			$dayArr[$val[0]['date']] = $val[0]['cnt'];		
		}
		$finalArr = array();
		for ( $i = 0; $i<=$days; $i++ ) {
			$tmpDate = date("Y-m-d",strtotime($startDate." +".$i." days"));
			if($days==7){
				$tmpDay = date("D",strtotime($startDate." +".$i." days"));
			$finalArr[$tmpDay] = isset($dayArr[$tmpDate])?$dayArr[$tmpDate]:0;
			}else{	
				$tempshortdate=strtotime($tmpDate);	
				$month=date("M j",$tempshortdate);	
				$finalArr[$month] = isset($dayArr[$tmpDate])?$dayArr[$tmpDate]:0;
			}			
		}
		return $finalArr;
	}	
	
	
	public function total_app_facebook_months($days,$university,$identifier)
	{	
		if($days=="6 Months")
		{
			$startmonth = date('Y-m-01 00:00:00', strtotime("-5 month"));
			$group=array("monthname");
		}
		else{
			$startmonth = date('Y-m-01 00:00:00', strtotime("-11 month"));
			$group=array("year","monthname");			
		}
		
		$endmonth = date('Y-m-d 00:00:00');
		$endmonthDate = strtotime(date('Y-m-01 00:00:00'));
		$tmpDate = $startmonth;
		
		$conditions=array("User.user_type_id"=>3,"date(User.created) BETWEEN date('".$startmonth."') AND date('".$endmonth."')");
		
		if($identifier=="Facebook"){
			if($university=="All")
			{
				 $conditions=array_merge(array('not' => array('User.identifier' => null)));
			}
			else{
				$conditions=array_merge(array('not' => array('User.identifier' => null),"UserUniversity.university_id"=>$university));
			}
		}
		else{
			if($university=="All")
				$conditions=array_merge(array('User.identifier' => null));
			else
				$conditions=array_merge(array('User.identifier' => null,"UserUniversity.university_id"=>$university));
		}
		
		$this->UserUniversity->virtualFields =array("firstDate"=>"DATE_FORMAT(User.created,'%Y-%m-01 00:00:00')");
		$appdownloads = $this->UserUniversity->find("all",array("conditions"=>$conditions,"fields"=>array("count(User.id) as cnt",'MONTHNAME(User.created) as "monthname"','Year(User.created) as year',"date(User.created) as date","firstDate"),"group"=>$group,"order"=>"User.created asc"));		
		
		$tmpData = array();
		foreach($appdownloads as $key=>$val) {
			$tmpData[$val['UserUniversity']['firstDate']] = $val[0];
		}
		
		$data = array();
		$i = 0;
		$finalData = array();
		while ( $endmonthDate >= $tmpDate ) {
			if ( empty($i) ) {
				$tmpDate;
			} else {
				$tmpDate = date("Y-m-d 00:00:00",$tmpDate);
			}
			$month = date("F",strtotime($tmpDate));
			if ( isset($tmpData[$tmpDate]) ) {
				$finalData[$tmpData[$tmpDate]['monthname']] = $tmpData[$tmpDate]['cnt'];
			} else {
				$finalData[$month] = 0;
			}
			$tmpDate = strtotime($tmpDate." +1 Month");
			$i++;
		}
		return $finalData;
	}	
	
	
	//End Facebook V/s Manual
	
	//Start Total Verifications: Verified v/s Non-Verified
	public function admin_total_verifications()
	{		
		$this->layout = false;				
		$this->loadModel('User');
		$this->loadModel('University');
		$this->loadModel('UserUniversity');								
		$startDate=date("Y")."-01-01";			
		$todayDate=date("Y-m-d");
		$this->University->hasMany = $this->University->belongsTo = $this->University->hasOne = array();
		$this->University->belongsTo = array(
			"UserUniversity" => array(
				"className" => "UserUniversity",
				"foreignKey" => "",
				"type" => "Inner",
				"conditions"=>"UserUniversity.university_id=University.id"
			)
		); 	
		$todayDate=date("Y-m-d");
		if($this->request->is("get") ) {
			if($this->request->query["days"])
			{				
				if(isset($this->request->query["days"]) && !empty($this->request->query["days"])){
					$university=$this->request->query["university"];						
					$days=$this->request->query["days"];		
					if($days=="7" || $days=="30" || $days=="90")
					{					
						$total_verified=$this->total_verified_days($days,$university,"verified");	
						$total_nonverified=$this->total_verified_days($days,$university,"nonverified");	
					}
					else{							
						$total_verified=$this->total_verified_months($days,$university,"verified");	
						$total_nonverified=$this->total_verified_months($days,$university,"nonverified");	
					}
				}
			}
		}
		
		$this->set('total_verified',$total_verified);
		$this->set('total_nonverified',$total_nonverified);
		$this->set('uni_regver',$university);
		$this->set('days_regver',$days);
	}
	public function total_verified_days($days,$university,$identifier)
	{	
		$conditions=array("User.user_type_id"=>3,"User.created >=(CURDATE() - INTERVAL ".$days." Day)");
		
		if($university!="All")
		{
			$conditions=array_merge(array("UserUniversity.university_id"=>$university));
		}
		if($identifier=="verified"){
			
				 $conditions=array_merge(array('User.is_active' =>1));			
		}
		else{			
				$conditions=array_merge(array('User.is_active' =>0));			
		}
		
		$this->UserUniversity->virtualFields =array("startDate"=>" (CURDATE() - INTERVAL ".$days." Day)");
		$appdownloads = $this->UserUniversity->find("all",array("conditions"=>$conditions,"fields"=>array("count(User.id) as cnt",'DAYNAME(User.created) as "dayname"',"date(User.created) as date","startDate",'User.identifier'),"group"=>"date(User.created)","order"=>"User.created asc"));		
		$startDate = (isset($appdownloads[0]['UserUniversity']['startDate']))?$appdownloads[0]['UserUniversity']['startDate']:''; 
	
		$dayArr = array();
		foreach($appdownloads as $key=>$val){			
			$dayArr[$val[0]['date']] = $val[0]['cnt'];		
		}
		$finalArr = array();
		for ( $i = 0; $i<=$days; $i++ ) {
			$tmpDate = date("Y-m-d",strtotime($startDate." +".$i." days"));
			if($days==7){
			$tmpDay = date("D",strtotime($startDate." +".$i." days"));
			$finalArr[$tmpDay] = isset($dayArr[$tmpDate])?$dayArr[$tmpDate]:0;
			}else{
				$tempshortdate=strtotime($tmpDate);
				$month=date("M j",$tempshortdate);
				$finalArr[$month] = isset($dayArr[$tmpDate])?$dayArr[$tmpDate]:0;
			}
		}		
		return $finalArr;
	}	
	
	public function total_verified_months($days,$university,$identifier)
	{	
		
		if($days=="6 Months")
		{
			$startmonth = date('Y-m-01 00:00:00', strtotime("-5 month"));
			$group=array("monthname");
		}
		else{
			$startmonth = date('Y-m-01 00:00:00', strtotime("-11 month"));
			$group=array("year","monthname");			
		}
		
		$endmonth = date('Y-m-d 00:00:00');
		$endmonthDate = strtotime(date('Y-m-01 00:00:00'));
		$tmpDate = $startmonth;
			
		$conditions=array("User.user_type_id"=>3,"date(User.created) BETWEEN date('".$startmonth."') AND date('".$endmonth."')");
		
		if($university!="All")
		{
			$conditions=array_merge(array("UserUniversity.university_id"=>$university));
		}
		if($identifier=="verified"){
			
				 $conditions=array_merge(array('User.is_active' =>1));			
		}
		else{			
				$conditions=array_merge(array('User.is_active' =>0));			
		}
		
		$this->UserUniversity->virtualFields =array("firstDate"=>"DATE_FORMAT(User.created,'%Y-%m-01 00:00:00')");
		$appdownloads = $this->UserUniversity->find("all",array("conditions"=>$conditions,"fields"=>array("count(User.id) as cnt",'MONTHNAME(User.created) as "monthname"','Year(User.created) as year',"date(User.created) as date","firstDate"),"group"=>$group,"order"=>"User.created asc"));		
		
		$tmpData = array();
		foreach($appdownloads as $key=>$val) {
			$tmpData[$val['UserUniversity']['firstDate']] = $val[0];
		}
		
		$data = array();
		$i = 0;
		$finalData = array();
		while ( $endmonthDate >= $tmpDate ) {
			if ( empty($i) ) {
				$tmpDate;
			} else {
				$tmpDate = date("Y-m-d 00:00:00",$tmpDate);
			}
			$month = date("F",strtotime($tmpDate));
			if ( isset($tmpData[$tmpDate]) ) {
				$finalData[$tmpData[$tmpDate]['monthname']] = $tmpData[$tmpDate]['cnt'];
			} else {
				$finalData[$month] = 0;
			}
			$tmpDate = strtotime($tmpDate." +1 Month");
			$i++;
		}
		return $finalData;
	}	
	//End Student Total Verifications: Verified v/s Non-Verified
	
	// Start Student Reach Impresions Conversions
	
	public function admin_total_reachconversions()
	{
		$this->layout = false;				
		$this->loadModel('User');
		$this->loadModel('University');
		$this->loadModel('UserUniversity');								
		$startDate=date("Y")."-01-01";			
		$todayDate=date("Y-m-d");
		$todayDate=date("Y-m-d");
		
		if($this->request->is("get") ) {
			if($this->request->query["days"])
			{				
				if(isset($this->request->query["days"]) && !empty($this->request->query["days"])){
					if($this->request->query["university"]=="")
					$university="All";
					else
					$university=$this->request->query["university"];						
					$days=$this->request->query["days"];	
						if($days=="7" || $days=="30" || $days=="90")
						{	
							$total_reach=$this->total_reach_conversions_days($days,$university,"Reach");	
							$total_impresions=$this->total_reach_conversions_days($days,$university,"Convert");
							$total_conversions=$this->total_reach_conversions_days($days,$university,"Impression");						
						}
						else
						{						
							$total_reach=$this->total_reach_conversions_months($days,$university,"Reach");	
							$total_impresions=$this->total_reach_conversions_months($days,$university,"Convert");
							$total_conversions=$this->total_reach_conversions_months($days,$university,"Impression");
						}
				}
			}
		}	
		$this->set('total_reach',$total_reach);
		$this->set('total_impresions',$total_impresions);
		$this->set('total_conversions',$total_conversions);	
	
	}

	
	public function total_reach_conversions_days($days,$university,$status)
	{
		//echo $days;echo $university;die;
		$conditions=array("VoucherStat.stat_type"=>$status,"VoucherStat.created >=(CURDATE() - INTERVAL ".$days." Day)");
		$this->VoucherStat->belongsTo = $this->VoucherStat->hasMany = $this->VoucherStat->hasOne = array();
		$this->UserUniversity->belongsTo = array(
			 "VoucherStat"=> array(
				"className" => "VoucherStat",
				"foreignKey" => false,
				"type" => "Inner",
				"conditions" => "VoucherStat.user_id = UserUniversity.user_id"
			)
		);
		if($university!="" && $university!="All")
		{
			$conditions=array_merge(array("UserUniversity.university_id"=>$university),$conditions);
		}				
		$this->UserUniversity->virtualFields =array("startDate"=>" (CURDATE() - INTERVAL ".$days." Day)");
		$appdownloads = $this->UserUniversity->find("all",array("conditions"=>$conditions,"fields"=>array("count(VoucherStat.id) as cnt",'DAYNAME(VoucherStat.created) as "dayname"',"date(VoucherStat.created) as date","startDate"),"group"=>"date(VoucherStat.created)","order"=>"VoucherStat.created asc"));	
				
		$startDate = (isset($appdownloads[0]['UserUniversity']['startDate']))?$appdownloads[0]['UserUniversity']['startDate']:''; 
		$dayArr = array();
		foreach($appdownloads as $key=>$val){			
			$dayArr[$val[0]['date']] = $val[0]['cnt'];		
		}
		$finalArr = array();
		for ( $i = 0; $i<=$days; $i++ ) {
			$tmpDate = date("Y-m-d",strtotime($startDate." +".$i." days"));
			if($days==7){
				$tmpDay = date("D",strtotime($startDate." +".$i." days"));
				$finalArr[$tmpDay] = isset($dayArr[$tmpDate])?$dayArr[$tmpDate]:0;
			}
			else{
				$tempshortdate=strtotime($tmpDate);
				$month=date("M j",$tempshortdate);
				$finalArr[$month] = isset($dayArr[$tmpDate])?$dayArr[$tmpDate]:0;
			}
		}
		
		return $finalArr;
	}
	
	public function total_reach_conversions_months($days,$university,$status)
	{
		$this->VoucherStat->belongsTo = $this->VoucherStat->hasMany = $this->VoucherStat->hasOne = array();
		$this->UserUniversity->belongsTo = array(
			 "VoucherStat"=> array(
				"className" => "VoucherStat",
				"foreignKey" => false,
				"type" => "Inner",
				"conditions" => "VoucherStat.user_id = UserUniversity.user_id"
			)
		);
		
		if($days=="6 Months")
		{
			$startmonth = date('Y-m-01 00:00:00', strtotime("-5 month"));
			$group=array("monthname");
		}
		else{
			$startmonth = date('Y-m-01 00:00:00', strtotime("-11 month"));
			$group=array("year","monthname");			
		}
		
		$endmonth = date('Y-m-d 00:00:00');
		$endmonthDate = strtotime(date('Y-m-01 00:00:00'));
		$tmpDate = $startmonth;
		
		$conditions=array("VoucherStat.stat_type"=>$status,"date(VoucherStat.created) BETWEEN date('".$startmonth."') AND date('".$endmonth."')");
		
		if($university!="" && $university!="All")
		{
			$conditions=array_merge(array("UserUniversity.university_id"=>$university),$conditions);
		}				
		$this->UserUniversity->virtualFields =array("firstDate"=>"DATE_FORMAT(VoucherStat.created,'%Y-%m-01 00:00:00')");
		
		$appdownloads = $this->UserUniversity->find("all",array("conditions"=>$conditions,"fields"=>array("count(VoucherStat.id) as cnt",'MONTHNAME(VoucherStat.created) as "monthname"','Year(VoucherStat.created) as year',"date(VoucherStat.created) as date","firstDate"),"group"=>$group,"order"=>"VoucherStat.created asc"));	

		$tmpData = array();
		foreach($appdownloads as $key=>$val) {
			$tmpData[$val['UserUniversity']['firstDate']] = $val[0];
		}
		
		$data = array();
		$i = 0;
		$finalData = array();
		while ( $endmonthDate >= $tmpDate ) {
			if ( empty($i) ) {
				$tmpDate;
			} else {
				$tmpDate = date("Y-m-d 00:00:00",$tmpDate);
			}
			$month = date("F",strtotime($tmpDate));
			if ( isset($tmpData[$tmpDate]) ) {
				$finalData[$tmpData[$tmpDate]['monthname']] = $tmpData[$tmpDate]['cnt'];
			} else {
				$finalData[$month] = 0;
			}
			$tmpDate = strtotime($tmpDate." +1 Month");
			$i++;
		}
		return $finalData;
	}
	
	// End Student Reach Impresions Conversions
	
	
// Merchant Insights Char start
	
	function admin_merchant_insights ($days=7) {	
		
			$this->layout = "mooladesignadmin";	
			$this->jsArray = "admin_merchant_insights";				
			$this->loadModel('User');
			$this->loadModel('UserDetail');
			$this->loadModel('Location');										
			$startDate=date("Y")."-01-01";			
			$todayDate=date("Y-m-d");		
			
			$this->User->hasMany = $this->User->belongsTo = $this->User->hasOne = array();
			$this->User->hasOne['UserDetail'] = array(
			"className"=>"UserDetail",
			"foreignKey"=>false,
			"type" => "Inner",
			"conditions"=> "User.id = UserDetail.user_id",
			);
				
			$this->set('records', array('7' => '7 Days','30' => '30 Days', '90' => '3 Months', '6 Months' => '6 Months','1 Year' => '1 Year'));
			$location= $this->Location->find("list",array("conditions"=>array("active"=>1)));
			$merhchant= $this->User->find("all",array("conditions"=>array("User.user_type_id"=>2)));
			//	pr($merhchant);
			$locationname=array();
			foreach($location as $key=>$val)
			{
					$locationname[$val]=$val;
			}			
			$this->set('location',$locationname);
			
			$merhchantname=array();
			foreach($merhchant as $key=>$val)
			{
					$merhchantname[$val["UserDetail"]["business"]]=$val["UserDetail"]["business"];
			}	
			
			$this->set('merhchantname',$merhchantname);
			$this->set('loc_renewals',"All");
			$this->set('days_renewals',"7 Days");
			$this->set('loc_logins',"All");
			$this->set('days_logins',"7 Days");
			$this->set('days_draft',"7 Days");	
			$this->set('merchant_draft',"All");
			$this->set('loc_draft',"All");
	}
	
	public function admin_total_renewals()
	{	
			$this->layout = false;				
			$this->loadModel('User');
			$this->loadModel('Location');
			$this->loadModel('Billing');										
			$startDate=date("Y")."-01-01";			
			$todayDate=date("Y-m-d");
			$loc=$days="";
			
								
			if($this->request->is("get") ) {	
								
				if(isset($this->request->query["days"]) && !empty($this->request->query["days"])){
					if($this->request->query["location"]=="")
					$loc="All";
					else
					$loc=$this->request->query["location"];						
					$days=$this->request->query["days"];					
					if($days=="7" || $days=="30" || $days=="90")
					{		
						$total_renewals=$this->total_renewals_days($days,$loc);
					}	
					else
					{						
						$total_renewals=$this->total_renewals_months($days,$loc);
					}					
				}
				
			}		
			//total app downloads and login						
			$this->set('total_renewals',$total_renewals);
			$this->set('loc_renewals',$loc);
			$this->set('days_renewals',$days);
	
	}
	
	public function total_renewals_days($days,$location)
	{
		$conditions=array("User.user_type_id"=>2,"User.created >=(CURDATE() - INTERVAL ".$days." DAY)");
		
		if($location!="All")
		$conditions=array_merge(array("UserDetail.address like"=> "%".$location."%"),$conditions);
		
		$this->User->virtualFields =array("startDate"=>" (CURDATE() - INTERVAL ".$days." Day)");
			
		$appdownloads = $this->User->find("all",array("conditions"=>$conditions,"fields"=>array("count(User.id) as cnt",'DAYNAME(User.created) as "dayname"',"date(User.created) as date","startDate"),"group"=>"date(User.created)","order"=>"User.created asc"));		
		//pr($appdownloads);
		$startDate = (isset($appdownloads[0]['User']['startDate']))?$appdownloads[0]['User']['startDate']:'';
		$dayArr = array();
		foreach($appdownloads as $key=>$val){			
			$dayArr[$val[0]['date']] = $val[0]['cnt'];		
		}
		$finalArr = array();
		for ( $i = 0; $i<=$days; $i++ ) {
			$tmpDate = date("Y-m-d",strtotime($startDate." +".$i." days"));
			if($days==7){
				$tmpDay = date("D",strtotime($startDate." +".$i." days"));
				$finalArr[$tmpDay] = isset($dayArr[$tmpDate])?$dayArr[$tmpDate]:0;
			}
			else
			{	
				$tempshortdate=strtotime($tmpDate);
				$month=date("M j",$tempshortdate);			
				$finalArr[$month] = isset($dayArr[$tmpDate])?$dayArr[$tmpDate]:0;		
			}
		}
		
		return $finalArr;
			
	}
	
	
	public function total_renewals_months($days,$location)
	{
		if($days=="6 Months")
		{
			$startmonth = date('Y-m-01 00:00:00', strtotime("-5 month"));
			$group=array("monthname");
		}
		else{
			$startmonth = date('Y-m-01 00:00:00', strtotime("-11 month"));
			$group=array("year","monthname");			
		}
		
		$endmonth = date('Y-m-d 00:00:00');
		$endmonthDate = strtotime(date('Y-m-01 00:00:00'));
		$tmpDate = $startmonth;
		
		$conditions=array("User.user_type_id"=>2,"date(User.created) BETWEEN date('".$startmonth."') AND date('".$endmonth."')");	
		if($location!="All")
		$conditions=array_merge(array("UserDetail.address like"=> "%".$location."%"),$conditions);
		
		$this->User->virtualFields =array("firstDate"=>"DATE_FORMAT(User.created,'%Y-%m-01 00:00:00')");
		$appdownloads = $this->User->find("all",array("conditions"=>$conditions,"fields"=>array("count(User.id) as cnt",'MONTHNAME(User.created) as "monthname"','Year(User.created) as year',"date(User.created) as date","firstDate"),"group"=>$group,"order"=>"User.created asc"));		
			
		$tmpData = array();
		foreach($appdownloads as $key=>$val) {
			$tmpData[$val['User']['firstDate']] = $val[0];
		}
		
		$data = array();
		$i = 0;
		$finalData = array();
		while ( $endmonthDate >= $tmpDate ) {
			if ( empty($i) ) {
				$tmpDate;
			} else {
				$tmpDate = date("Y-m-d 00:00:00",$tmpDate);
			}
			$month = date("F",strtotime($tmpDate));
			if ( isset($tmpData[$tmpDate]) ) {
				$finalData[$tmpData[$tmpDate]['monthname']] = $tmpData[$tmpDate]['cnt'];
			} else {
				$finalData[$month] = 0;
			}
			$tmpDate = strtotime($tmpDate." +1 Month");
			$i++;
		}
		return $finalData;
			
	}	
	
	
	public function admin_total_merchant_logins()
	{	
			$this->layout = false;				
			$this->loadModel('User');
			$this->loadModel('Location');							
			$startDate=date("Y")."-01-01";			
			$todayDate=date("Y-m-d");
										
			if($this->request->is("get") ) {
				if($this->request->query["days"])
				{				
					if(isset($this->request->query["days"]) && !empty($this->request->query["days"]))
					{
						if($this->request->query["location"]=="")
						$loc_logins="All";
						else
						$loc_logins=$this->request->query["location"];						
						$days_logins=$this->request->query["days"];		
									
						if($days_logins=="7" || $days_logins=="30" || $days_logins=="90")
						{			
							$total_logins=$this->total_merchant_logins_days($days_logins,$loc_logins);	
						}	
						else
						{						
							$total_logins=$this->total_merchant_logins_months($days_logins,$loc_logins);	
						}				
					}
				}
				
				$this->set('total_logins',$total_logins);
				$this->set('loc_logins',$loc_logins);
				$this->set('days_logins',$days_logins);
				
			}
	}
	public function total_merchant_logins_days($days,$location)
	{		
		$conditions=array("User.user_type_id"=>2,"User.created >=(CURDATE() - INTERVAL ".$days." DAY)");
		
		if($location!="All")
		$conditions=array_merge(array("UserDetail.address like"=> "%".$location."%"),$conditions);
		
		$this->User->virtualFields =array("startDate"=>" (CURDATE() - INTERVAL ".$days." Day)");
			
		$appdownloads = $this->User->find("all",array("conditions"=>$conditions,"fields"=>array("count(User.id) as cnt",'DAYNAME(User.created) as "dayname"',"date(User.created) as date","startDate"),"group"=>"date(User.created)","order"=>"User.created asc"));		
		//pr($appdownloads);
		$startDate = (isset($appdownloads[0]['User']['startDate']))?$appdownloads[0]['User']['startDate']:'';
		$dayArr = array();
		foreach($appdownloads as $key=>$val){			
			$dayArr[$val[0]['date']] = $val[0]['cnt'];		
		}
		$finalArr = array();
		for ( $i = 0; $i<=$days; $i++ ) {
			$tmpDate = date("Y-m-d",strtotime($startDate." +".$i." days"));
			if($days==7){
				$tmpDay = date("D",strtotime($startDate." +".$i." days"));
				$finalArr[$tmpDay] = isset($dayArr[$tmpDate])?$dayArr[$tmpDate]:0;
			}
			else
			{	
				$tempshortdate=strtotime($tmpDate);
				$month=date("M j",$tempshortdate);			
				$finalArr[$month] = isset($dayArr[$tmpDate])?$dayArr[$tmpDate]:0;		
			}
		}
		
		return $finalArr;
	}
	public function total_merchant_logins_months($days,$location)
	{		
		
		if($days=="6 Months")
		{
			$startmonth = date('Y-m-01 00:00:00', strtotime("-5 month"));
			$group=array("monthname");
		}
		else{
			$startmonth = date('Y-m-01 00:00:00', strtotime("-11 month"));
			$group=array("year","monthname");			
		}
		
		$endmonth = date('Y-m-d 00:00:00');
		$endmonthDate = strtotime(date('Y-m-01 00:00:00'));
		$tmpDate = $startmonth;
		
		$conditions=array("User.user_type_id"=>2,"date(User.created) BETWEEN date('".$startmonth."') AND date('".$endmonth."')");	
		if($location!="All")
		$conditions=array_merge(array("UserDetail.address like"=> "%".$location."%"),$conditions);
		
		$this->User->virtualFields =array("firstDate"=>"DATE_FORMAT(User.created,'%Y-%m-01 00:00:00')");
		$appdownloads = $this->User->find("all",array("conditions"=>$conditions,"fields"=>array("count(User.id) as cnt",'MONTHNAME(User.created) as "monthname"','Year(User.created) as year',"date(User.created) as date","firstDate"),"group"=>$group,"order"=>"User.created asc"));		
			
		$tmpData = array();
		foreach($appdownloads as $key=>$val) {
			$tmpData[$val['User']['firstDate']] = $val[0];
		}
		
		$data = array();
		$i = 0;
		$finalData = array();
		while ( $endmonthDate >= $tmpDate ) {
			if ( empty($i) ) {
				$tmpDate;
			} else {
				$tmpDate = date("Y-m-d 00:00:00",$tmpDate);
			}
			$month = date("F",strtotime($tmpDate));
			if ( isset($tmpData[$tmpDate]) ) {
				$finalData[$tmpData[$tmpDate]['monthname']] = $tmpData[$tmpDate]['cnt'];
			} else {
				$finalData[$month] = 0;
			}
			$tmpDate = strtotime($tmpDate." +1 Month");
			$i++;
		}
		return $finalData;
	}
	//Facebook Vs Mannual
	public function admin_total_merchant_registrations()
	{	
			$this->layout = false;				
			$this->loadModel('User');
			$this->loadModel('Location');							
			$startDate=date("Y")."-01-01";			
			$todayDate=date("Y-m-d");
										
			if($this->request->is("get") ) {
				if($this->request->query["days"])
				{				
					if(isset($this->request->query["days"]) && !empty($this->request->query["days"]))
					{
						if($this->request->query["location"]=="")
						$loc_reg="All";
						else
						$loc_reg=$this->request->query["location"];						
						$days_reg=$this->request->query["days"];		
									
						if($days_reg=="7" || $days_reg=="30" || $days_reg=="90")
						{			
							$total_app_facebook=$this->total_merchant_reg_days($days_reg,$loc_reg,"Facebook");
							$total_app_manual=$this->total_merchant_reg_days($days_reg,$loc_reg,"Mnnual");	
						}	
						else
						{						
							$total_app_facebook=$this->total_merchant_reg_months($days_reg,$loc_reg,"Facebook");
							$total_app_manual=$this->total_merchant_reg_months($days_reg,$loc_reg,"Mnnual");	
						}						
					}
				}	
				$this->set('total_app_facebook',$total_app_facebook);
				$this->set('total_app_manual',$total_app_manual);					
			}
	}
	public function total_merchant_reg_days($days,$location,$identifier)
	{	
				
		if($identifier=="Facebook"){
			
			 $conditions=array('not' => array('User.identifier' => null),"User.user_type_id"=>2,"User.created >=(CURDATE() - INTERVAL ".$days." Day)");	
		}
		else{
			  $conditions=array('User.identifier' => null,"User.is_active"=>1,"User.user_type_id"=>2,"User.created >=(CURDATE() - INTERVAL  ".$days." Day)");			
		}		
		if($location!="All"){
				$conditions=array_merge(array("UserDetail.address like"=> "%".$location."%"),$conditions);
		}
		
		$this->User->virtualFields =array("startDate"=>" (CURDATE() - INTERVAL ".$days." Day)");
		
		$appdownloads = $this->User->find("all",array("conditions"=>$conditions,"fields"=>array("count(User.id) as cnt",'DAYNAME(User.created) as "dayname"',"date(User.created) as date","startDate",'User.identifier'),"group"=>"date(User.created)","order"=>"User.created asc"));		
		$startDate = (isset($appdownloads[0]['User']['startDate']))?$appdownloads[0]['User']['startDate']:''; 
		
		$dayArr = array();
		foreach($appdownloads as $key=>$val){			
			$dayArr[$val[0]['date']] = $val[0]['cnt'];		
		}
		$finalArr = array();
		for ( $i = 0; $i<=$days; $i++ ) {
			$tmpDate = date("Y-m-d",strtotime($startDate." +".$i." days"));
			if($days==7){
				$tmpDay = date("D",strtotime($startDate." +".$i." days"));
			$finalArr[$tmpDay] = isset($dayArr[$tmpDate])?$dayArr[$tmpDate]:0;
			}else{	
				$tempshortdate=strtotime($tmpDate);	
				$month=date("M j",$tempshortdate);	
				$finalArr[$month] = isset($dayArr[$tmpDate])?$dayArr[$tmpDate]:0;
			}			
		}
		return $finalArr;
	}	
	
	public function total_merchant_reg_months($days,$location,$identifier)
	{	
		
		if($days=="6 Months")
		{
			$startmonth = date('Y-m-01 00:00:00', strtotime("-5 month"));
			$group=array("monthname");
		}
		else{
			$startmonth = date('Y-m-01 00:00:00', strtotime("-11 month"));
			$group=array("year","monthname");			
		}
		
		$endmonth = date('Y-m-d 00:00:00');
		$endmonthDate = strtotime(date('Y-m-01 00:00:00'));
		$tmpDate = $startmonth;
				
		if($identifier=="Facebook"){
			
			 $conditions=array('not' => array('User.identifier' => null),"User.user_type_id"=>2,"date(User.created) BETWEEN date('".$startmonth."') AND date('".$endmonth."')");	
		}
		else{
			  $conditions=array('User.identifier' => null,"User.is_active"=>1,"User.user_type_id"=>2,"date(User.created) BETWEEN date('".$startmonth."') AND date('".$endmonth."')");			
		}		
		if($location!="All"){
				$conditions=array_merge(array("UserDetail.address like"=> "%".$location."%"),$conditions);
		}
		$this->User->virtualFields =array("firstDate"=>"DATE_FORMAT(User.created,'%Y-%m-01 00:00:00')");

		
		$appdownloads = $this->User->find("all",array("conditions"=>$conditions,"fields"=>array("count(User.id) as cnt",'MONTHNAME(User.created) as "monthname"',"date(User.created) as date",'Year(User.created) as year',"firstDate",'User.identifier'),"group"=>$group,"order"=>"User.created asc"));			
		$startDate = (isset($appdownloads[0]['User']['startDate']))?$appdownloads[0]['User']['startDate']:''; 
		
		$tmpData = array();
		foreach($appdownloads as $key=>$val) {
			$tmpData[$val['User']['firstDate']] = $val[0];
		}
		
		$data = array();
		$i = 0;
		$finalData = array();
		while ( $endmonthDate >= $tmpDate ) {
			if ( empty($i) ) {
				$tmpDate;
			} else {
				$tmpDate = date("Y-m-d 00:00:00",$tmpDate);
			}
			$month = date("F",strtotime($tmpDate));
			if ( isset($tmpData[$tmpDate]) ) {
				$finalData[$tmpData[$tmpDate]['monthname']] = $tmpData[$tmpDate]['cnt'];
			} else {
				$finalData[$month] = 0;
			}
			$tmpDate = strtotime($tmpDate." +1 Month");
			$i++;
		}
		return $finalData;
	}	
	
	// Reach Convert Impression
	public function admin_total_merchant_reachconversion()
	{
		$this->layout =false;			
		$this->loadModel('User');
		$this->loadModel('Location');
		if($this->request->is("get") ) {			
			$loc=$this->request->query["location"];						
			$days=$this->request->query["days"];		
			$merchant=$this->request->query["merchant"];	
				
			if($days=="7" || $days=="30" || $days=="90")
			{			
				$total_reach=$this->total_merchant_reach_conversions($days,$loc,$merchant,"Reach");	
				$total_impresions=$this->total_merchant_reach_conversions($days,$loc,$merchant,"Convert");
				$total_conversions=$this->total_merchant_reach_conversions($days,$loc,$merchant,"Impression");
			}	
			else
			{						
				$total_reach=$this->total_merchant_reach_conversions_months($days,$loc,$merchant,"Reach");	
				$total_impresions=$this->total_merchant_reach_conversions_months($days,$loc,$merchant,"Convert");
				$total_conversions=$this->total_merchant_reach_conversions_months($days,$loc,$merchant,"Impression");
			}				
			
			$this->set('total_reach',$total_reach);
			$this->set('total_impresions',$total_impresions);
			$this->set('total_conversions',$total_conversions);			
		}
	}
	
	//Reach Impresions Conversions
	public function total_merchant_reach_conversions($days,$location,$merhcant,$status)
	{
		$this->VoucherStat->hasMany = $this->VoucherStat->belongsTo = $this->VoucherStat->hasOne = array();
		$this->VoucherStat->belongsTo = array(
			"Voucher" => array(
				"className" => "Voucher",
				"foreignKey" => false,
				"type" => "Inner",	
				"conditions"=>"VoucherStat.voucher_id=Voucher.id"
			),
			"UserDetail" => array(
				"className" => "UserDetail",
				"foreignKey" => false,
				"type" => "Inner",
				"conditions"=>"Voucher.user_id=UserDetail.user_id"
			)
		); 	
		$conditions=array("VoucherStat.stat_type"=>$status,"VoucherStat.created >=(CURDATE() - INTERVAL ".$days." Day)");		
		if($location != "All"){
				$conditions=array_merge(array("UserDetail.address like"=> "%".$location."%"),$conditions);
		}			
		if($merhcant != "All"){
				$conditions=array_merge(array("UserDetail.business like"=> "%".$merhcant."%"),$conditions);
		}
		
		$this->VoucherStat->virtualFields =array("startDate"=>" (CURDATE() - INTERVAL ".$days." Day)");
		$appdownloads = $this->VoucherStat->find("all",array("conditions"=>$conditions,"fields"=>array("count(VoucherStat.id) as cnt",'DAYNAME(VoucherStat.created) as "dayname"',"date(VoucherStat.created) as date","startDate"),"group"=>"date(VoucherStat.created)","order"=>"VoucherStat.created asc"));
				
		$startDate = (isset($appdownloads[0]['VoucherStat']['startDate']))?$appdownloads[0]['VoucherStat']['startDate']:''; 
		//pr($appdownloads);
		$dayArr = array();
		foreach($appdownloads as $key=>$val){			
			$dayArr[$val[0]['date']] = $val[0]['cnt'];		
		}
		$finalArr = array();
		for ( $i = 0; $i<=$days; $i++ ) {
			$tmpDate = date("Y-m-d",strtotime($startDate." +".$i." days"));
			if($days==7){
				$tmpDay = date("D",strtotime($startDate." +".$i." days"));
				$finalArr[$tmpDay] = isset($dayArr[$tmpDate])?$dayArr[$tmpDate]:0;
			}
			else{
				$tempshortdate=strtotime($tmpDate);
				$month=date("M j",$tempshortdate);	
				$finalArr[$month] = isset($dayArr[$tmpDate])?$dayArr[$tmpDate]:0;
			}
		}
		//pr($finalArr);
		return $finalArr;
	}
	
	
	
	//Reach Impresions Conversions
	public function total_merchant_reach_conversions_months($days,$location,$merhcant,$status)
	{
		$this->VoucherStat->hasMany = $this->VoucherStat->belongsTo = $this->VoucherStat->hasOne = array();
		$this->VoucherStat->belongsTo = array(
			"Voucher" => array(
				"className" => "Voucher",
				"foreignKey" => false,
				"type" => "Inner",
				"conditions"=>"VoucherStat.voucher_id=Voucher.id"
			),
			"UserDetail" => array(
				"className" => "UserDetail",
				"foreignKey" => false,
				"type" => "Inner",
				"conditions"=>"Voucher.user_id=UserDetail.user_id"
			)
		); 	
		if($days=="6 Months")
		{
			$startmonth = date('Y-m-01 00:00:00', strtotime("-5 month"));
			$group=array("monthname");
		}
		else{
			$startmonth = date('Y-m-01 00:00:00', strtotime("-11 month"));
			$group=array("year","monthname");			
		}
		
		$endmonth = date('Y-m-d 00:00:00');
		$endmonthDate = strtotime(date('Y-m-01 00:00:00'));
		$tmpDate = $startmonth;
		$conditions=array("VoucherStat.stat_type"=>$status,"date(VoucherStat.created) BETWEEN date('".$startmonth."') AND date('".$endmonth."')");		
		if($location != "All"){
				$conditions=array_merge(array("UserDetail.address like"=> "%".$location."%"),$conditions);
		}			
		if($merhcant != "All"){
				$conditions=array_merge(array("UserDetail.business like"=> "%".$merhcant."%"),$conditions);
		}
		
		$this->VoucherStat->virtualFields =array("firstDate"=>"DATE_FORMAT(VoucherStat.created,'%Y-%m-01 00:00:00')");
		$appdownloads = $this->VoucherStat->find("all",array("conditions"=>$conditions,"fields"=>array("count(VoucherStat.id) as cnt",'MONTHNAME(VoucherStat.created) as "monthname"','Year(Voucher.created) as year',"date(VoucherStat.created) as date","firstDate"),"group"=>$group,"order"=>"VoucherStat.created asc"));
				
		$tmpData = array();
		foreach($appdownloads as $key=>$val) {
			$tmpData[$val['VoucherStat']['firstDate']] = $val[0];
		}
		
		$data = array();
		$i = 0;
		$finalData = array();
		while ( $endmonthDate >= $tmpDate ) {
			if ( empty($i) ) {
				$tmpDate;
			} else {
				$tmpDate = date("Y-m-d 00:00:00",$tmpDate);
			}
			$month = date("F",strtotime($tmpDate));
			if ( isset($tmpData[$tmpDate]) ) {
				$finalData[$tmpData[$tmpDate]['monthname']] = $tmpData[$tmpDate]['cnt'];
			} else {
				$finalData[$month] = 0;
			}
			$tmpDate = strtotime($tmpDate." +1 Month");
			$i++;
		}
		return $finalData;
	}
	
	
	
	
	// Drafted Published Created
	public function admin_total_merchant_drafted()
	{
		$this->layout =false;			
		$this->loadModel('User');
		$this->loadModel('Location');
		$this->loadModel('Voucher');
		$this->loadModel('UserDetail');
		if($this->request->is("get") ) {			
			$loc=$this->request->query["location"];						
			$days=$this->request->query["days"];		
			$merchant=$this->request->query["merchant"];	
				
			if($days=="7" || $days=="30" || $days=="90")
			{			
				$total_drafted=$this->total_merchant_drafted($days,$loc,$merchant,0);	
				$total_published=$this->total_merchant_drafted($days,$loc,$merchant,1);
				$total_created=$this->total_merchant_drafted($days,$loc,$merchant,2);
				$show_reach="weeks";
			}	
			elseif($days=="6 Months" || $days=="1 Year")
			{						
				$total_drafted=$this->total_merchant_drafted_month($days,$loc,$merchant,0);	
				$total_published=$this->total_merchant_drafted_month($days,$loc,$merchant,1);
				$total_created=$this->total_merchant_drafted_month($days,$loc,$merchant,2);
				$show_reach="weeks";
			}	
			$this->set('show_reach',$show_reach);
			$this->set('total_drafted',$total_drafted);
			$this->set('total_published',$total_published);
			$this->set('total_created',$total_created);	
			$this->set('days_draft',$days);	
			$this->set('merchant_draft',$merchant);
			$this->set('loc_draft',$loc);		
		}
	}
	
	// Drafted Publishe Created 7 day 30 days and 3 months
	public function total_merchant_drafted($days,$location,$merhcant,$status)
	{
		$this->Voucher->hasMany = $this->Voucher->belongsTo = $this->Voucher->hasOne = array();
		$this->Voucher->belongsTo = array(			
			"UserDetail" => array(
				"className" => "UserDetail",
				"foreignKey" => false,
				"type" => "Inner",
				"conditions"=>"Voucher.user_id=UserDetail.user_id"
			)
		); 
			
		$conditions=array("Voucher.created >=(CURDATE() - INTERVAL ".$days." Day)");	
		if($status!=2)
		$conditions=array_merge(array("Voucher.voucher_status"=>$status));
		
		if($location != "All"){
				$conditions=array_merge(array("UserDetail.address like"=> "%".$location."%"),$conditions);
		}			
		if($merhcant != "All"){
				$conditions=array_merge(array("UserDetail.business like"=> "%".$merhcant."%"),$conditions);
		}
		
		$this->Voucher->virtualFields =array("startDate"=>" (CURDATE() - INTERVAL ".$days." Day)");
		$appdownloads = $this->Voucher->find("all",array("conditions"=>$conditions,"fields"=>array("count(Voucher.id) as cnt",'DAYNAME(Voucher.created) as "dayname"',"date(Voucher.created) as date","startDate"),"group"=>"date(Voucher.created)","order"=>"Voucher.created asc"));
				
		$startDate = (isset($appdownloads[0]['Voucher']['startDate']))?$appdownloads[0]['Voucher']['startDate']:''; 
		
		$dayArr = array();
		foreach($appdownloads as $key=>$val){			
			$dayArr[$val[0]['date']] = $val[0]['cnt'];		
		}
		$finalArr = array();
		for ( $i = 0; $i<=$days; $i++ ) {
			$tmpDate = date("Y-m-d",strtotime($startDate." +".$i." days"));
			if($days==7){
				$tmpDay = date("D",strtotime($startDate." +".$i." days"));
				$finalArr[$tmpDay] = isset($dayArr[$tmpDate])?$dayArr[$tmpDate]:0;
			}
			else{
				$tempshortdate=strtotime($tmpDate);
				$month=date("M j",$tempshortdate);				
				$finalArr[$month] = isset($dayArr[$tmpDate])?$dayArr[$tmpDate]:0;
			}
		}		
		return $finalArr;
	}
	
	
	// Drafted Publishe Created 6 month and 1 year
	public function total_merchant_drafted_month($days,$location,$merhcant,$status)
	{
		$this->Voucher->hasMany = $this->Voucher->belongsTo = $this->Voucher->hasOne = array();
		$this->Voucher->belongsTo = array(			
			"UserDetail" => array(
				"className" => "UserDetail",
				"foreignKey" => false,
				"type" => "Inner",
				"conditions"=>"Voucher.user_id=UserDetail.user_id"
			)
		); 
		
		if($days=="6 Months")
		{
			$startmonth = date('Y-m-01 00:00:00', strtotime("-5 month"));
			$group=array("monthname");
		}
		else{
			$startmonth = date('Y-m-01 00:00:00', strtotime("-11 month"));
			$group=array("year","monthname");			
		}
		
		$endmonth = date('Y-m-d 00:00:00');
		$endmonthDate = strtotime(date('Y-m-01 00:00:00'));
		$tmpDate = $startmonth;
		$conditions=array("date(Voucher.created) BETWEEN date('".$startmonth."') AND date('".$endmonth."')");	
		if($status!=2)
		$conditions=array_merge(array("Voucher.voucher_status"=>$status));
		
		if($location != "All"){
				$conditions=array_merge(array("UserDetail.address like"=> "%".$location."%"),$conditions);
		}			
		if($merhcant != "All"){
				$conditions=array_merge(array("UserDetail.business like"=> "%".$merhcant."%"),$conditions);
		}
		
		$this->Voucher->virtualFields =array("firstDate"=>"DATE_FORMAT(Voucher.created,'%Y-%m-01 00:00:00')");
		
		
		$appdownloads = $this->Voucher->find("all",array("conditions"=>$conditions,"fields"=>array("count(Voucher.id) as cnt",'MONTHNAME(Voucher.created) as monthname','Year(Voucher.created) as year',"date(Voucher.created) as date","firstDate"),"group"=>$group,"order"=>"Voucher.created asc"));
		
		$tmpData = array();
		foreach($appdownloads as $key=>$val) {
			$tmpData[$val['Voucher']['firstDate']] = $val[0];
		}
		
		$data = array();
		$i = 0;
		$finalData = array();
		while ( $endmonthDate >= $tmpDate ) {
			if ( empty($i) ) {
				$tmpDate;
			} else {
				$tmpDate = date("Y-m-d 00:00:00",$tmpDate);
			}
			$month = date("F",strtotime($tmpDate));
			if ( isset($tmpData[$tmpDate]) ) {
				$finalData[$tmpData[$tmpDate]['monthname']] = $tmpData[$tmpDate]['cnt'];
			} else {
				$finalData[$month] = 0;
			}
			$tmpDate = strtotime($tmpDate." +1 Month");
			$i++;
		}
		return $finalData;
	}
	
	public function admin_total_merchant_paidsus()
	{	
			$this->layout = false;				
			$this->loadModel('User');
			$this->loadModel('Location');
			$this->loadModel('Billing');										
			$startDate=date("Y")."-01-01";			
			$todayDate=date("Y-m-d");
			$loc=$days="";
			
								
			if($this->request->is("get") ) {	
								
				if(isset($this->request->query["days"]) && !empty($this->request->query["days"])){
					if($this->request->query["location"]=="")
					$loc="All";
					else
					$loc=$this->request->query["location"];						
					$days=$this->request->query["days"];					
					if($days=="7" || $days=="30" || $days=="90")
					{		
						$total_paidsus_paid=$this->total_merchant_paidsus_days($days,$loc,"PAID");
						$total_paidsus_suspend=$this->total_merchant_paidsus_days($days,$loc,"SUSPENDED");
					}	
					else
					{						
						$total_paidsus_paid=$this->total_merchant_paidsus_months($days,$loc,"PAID");
						$total_paidsus_suspend=$this->total_merchant_paidsus_months($days,$loc,"SUSPENDED");
					}					
				}
				
			}		
			//total app downloads and login						
			$this->set('total_paidsus_paid',$total_paidsus_paid);
			$this->set('total_paidsus_suspend',$total_paidsus_suspend);
	}
	
	public function total_merchant_paidsus_days($days,$location,$status)
	{
		$this->Billing->hasMany = $this->Billing->belongsTo = $this->Billing->hasOne = array();
		$this->Billing->belongsTo = array(			
			"UserDetail" => array(
				"className" => "UserDetail",
				"foreignKey" => false,
				"type" => "Inner",
				"conditions"=>"Billing.user_id=UserDetail.user_id"
			)
		); 	
		$conditions=array("Billing.renew_date >=(CURDATE() - INTERVAL ".$days." Day)");
		
		if($status=="SUSPENDED")
		$conditions=array_merge(array("Billing.bill_status ="=>"SUSPENDED"));
		else
		$conditions=array_merge(array("Billing.bill_status !="=>"SUSPENDED"));
		
		if($location != "All"){
				$conditions=array_merge(array("UserDetail.address like"=> "%".$location."%"),$conditions);
		}
		
		$this->Billing->virtualFields =array("startDate"=>" (CURDATE() - INTERVAL ".$days." Day)");
		$appdownloads = $this->Billing->find("all",array("conditions"=>$conditions,"fields"=>array("count(Billing.id) as cnt",'DAYNAME(Billing.renew_date) as "dayname"',"date(Billing.renew_date) as date","startDate"),"group"=>"date(Billing.renew_date)","order"=>"Billing.renew_date asc"));
		
		$startDate = (isset($appdownloads[0]['Billing']['startDate']))?$appdownloads[0]['Billing']['startDate']:'';
		$dayArr = array();
		foreach($appdownloads as $key=>$val){			
			$dayArr[$val[0]['date']] = $val[0]['cnt'];		
		}
		$finalArr = array();
		for ( $i = 0; $i<=$days; $i++ ) {
			
			$tmpDate = date("Y-m-d",strtotime($startDate." +".$i." days"));
			if($days==7){
				$tmpDay = date("D",strtotime($startDate." +".$i." days"));
				$finalArr[$tmpDay] = isset($dayArr[$tmpDate])?$dayArr[$tmpDate]:0;
			}
			else
			{	
				$tempshortdate=strtotime($tmpDate);
				$month=date("M j",$tempshortdate);			
				$finalArr[$month] = isset($dayArr[$tmpDate])?$dayArr[$tmpDate]:0;
			}
		}	
			return 	$finalArr;
			
	}
	public function total_merchant_paidsus_months($days,$location,$status)
	{
		$this->Billing->hasMany = $this->Billing->belongsTo = $this->Billing->hasOne = array();
		$this->Billing->belongsTo = array(			
			"UserDetail" => array(
				"className" => "UserDetail",
				"foreignKey" => false,
				"type" => "Inner",
				"conditions"=>"Billing.user_id=UserDetail.user_id"
			)
		); 	
		if($days=="6 Months")
		{
			$startmonth = date('Y-m-01 00:00:00', strtotime("-5 month"));
			$group=array("monthname");
		}
		else{
			$startmonth = date('Y-m-01 00:00:00', strtotime("-11 month"));
			$group=array("year","monthname");			
		}
		
		$endmonth = date('Y-m-d 00:00:00');
		$endmonthDate = strtotime(date('Y-m-01 00:00:00'));
		$tmpDate = $startmonth;
		
		$conditions=array("date(Billing.renew_date) BETWEEN date('".$startmonth."') AND date('".$endmonth."')");
				
		if($status=="SUSPENDED")
		$conditions=array_merge(array("Billing.bill_status ="=>"SUSPENDED"));
		else
		$conditions=array_merge(array("Billing.bill_status !="=>"SUSPENDED"));
		
		if($location != "All"){
				$conditions=array_merge(array("UserDetail.address like"=> "%".$location."%"),$conditions);
		}
	     $this->Billing->virtualFields =array("firstDate"=>"DATE_FORMAT(Billing.renew_date,'%Y-%m-01 00:00:00')");

		$appdownloads = $this->Billing->find("all",array("conditions"=>$conditions,"fields"=>array("count(Billing.id) as cnt",'MONTHNAME(Billing.renew_date) as "monthname"','Year(Billing.renew_date) as year',"date(Billing.renew_date) as date","firstDate"),"group"=>$group,"order"=>"Billing.renew_date asc"));
		
		$tmpData = array();
		foreach($appdownloads as $key=>$val) {
			$tmpData[$val['Billing']['firstDate']] = $val[0];
		}
		
		$data = array();
		$i = 0;
		$finalData = array();
		while ( $endmonthDate >= $tmpDate ) {
			if ( empty($i) ) {
				$tmpDate;
			} else {
				$tmpDate = date("Y-m-d 00:00:00",$tmpDate);
			}
			$month = date("F",strtotime($tmpDate));
			if ( isset($tmpData[$tmpDate]) ) {
				$finalData[$tmpData[$tmpDate]['monthname']] = $tmpData[$tmpDate]['cnt'];
			} else {
				$finalData[$month] = 0;
			}
			$tmpDate = strtotime($tmpDate." +1 Month");
			$i++;
		}
		return $finalData;
			
	}
	
	
}
