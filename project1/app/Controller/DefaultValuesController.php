<?php
App::uses('AppController', 'Controller');
/**
 * Categories DefaultValue
 *
 * @property DefaultValue $DefaultValue
 * @property PaginatorComponent $Paginator
 */
class DefaultValuesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit() {		
		if ($this->request->is(array('post', 'put'))) {
			
			$tempdata=$this->request->data;			
			$ids = array(1,2,3,4,5);
			$data[] = array('id' => 1, 'value' => $tempdata['DefaultValue'][1]);
			$data[] =array('id' => 2, 'value' => $tempdata['DefaultValue'][2]);
			$data[] =array('id' => 3, 'value' => $tempdata['DefaultValue'][3]);
			$data[] =array('id' => 4, 'value' => $tempdata['DefaultValue'][4]);
			$data[] =array('id' => 5, 'value' => $tempdata['DefaultValue'][5]);
			
			if ($this->DefaultValue->saveAll($data)) {
				$this->Flash->success(__('The default value has been saved.'));
				return $this->redirect(array('action' => 'edit'));
			} else {
				$this->Flash->error(__('The default value could not be saved. Please, try again.'));
			}
		} else {			
			$this->request->data = $this->DefaultValue->find('all');
			
			$this->set('defaultvalues', $this->request->data);
		}
	}


}
