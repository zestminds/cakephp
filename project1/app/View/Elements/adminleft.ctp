<?php
if ($this->Session->read("Auth.User.id") && $this->Session->read("Auth.User.user_type_id") == 1) {
    $typeId = $this->Session->read("AuthUser.User.usertype_id");
    $userTypes = array(1, 2, 3);
    //pr($this->params['action']);
    //die;
?>
    <div id="adminMenu" class="ddsmoothmenu actions">
        <a class="side-bar" onclick="hidepanel();" id="btn" href="javascript:void(0);" title="Click to hide panel" >Click to hide panel</a>
        <?php if ($this->Session->read("Auth.User.id") && $this->Session->read("Auth.User.user_type_id") == 1 ) { ?>  
            <ul>
                <ul class="admintoggel">				
				<li>
					<a href="<?php echo $this->Html->url(SITE_LINK.'dashboard'); ?>" <?php if ($this->params['controller'] == 'users' && $this->params['action'] == 'admin_dashboard') { ?> class="active" <?php } ?> >Dashboard </a>
				</li>	
				<li>
					<a href="<?php echo $this->Html->url(SITE_LINK.'manage-merchants/'); ?>" <?php if ($this->params['controller'] == 'user_details') { ?> class="active" <?php } ?>>Manage Merchants</a>
				</li> 
				<li>
					<a href="<?php echo $this->Html->url(SITE_LINK.'manage-students/'); ?>" <?php if ($this->params['controller'] == 'user_details') { ?> class="active" <?php } ?>>Manage Students</a>
				</li> 
				<li>
					<a href="<?php echo $this->Html->url(SITE_LINK.'view-vouchers/'); ?>" <?php if ($this->params['controller'] == 'admin_index') { ?> class="active" <?php } ?>>Manage Vouchers</a>
				</li>
				<?php /* 
				<li>
					<a href="<?php echo $this->Html->url(SITE_LINK.'manage_insights'); ?>" <?php if ($this->params['controller'] == '') { ?> class="active" <?php } ?> >Insights </a>
				</li>*/ ?>
				<li>
					<a href="<?php echo $this->Html->url(SITE_LINK.'view-sales'); ?>" <?php if ($this->params['controller'] == 'users'  && $this->params['action'] == 'admin_index' ) { ?> class="active" <?php } ?> >Sales </a>
				</li>
				
				<li>
					<a href="<?php echo $this->Html->url(SITE_LINK.'manage-categories'); ?>" <?php if ($this->params['controller'] == 'Categories') { ?> class="active" <?php } ?>>Manage Category</a>
				</li>
				
				<li>
					<a href="<?php echo $this->Html->url(SITE_LINK.'manage-locations'); ?>" <?php if ($this->params['controller'] == 'Locations') { ?> class="active" <?php } ?>>Manage Location</a>
				</li>
				
				<li>
					<a href="<?php echo $this->Html->url(SITE_LINK.'manage-domain-extensions'); ?>" <?php if ($this->params['controller'] == 'UniversitiesDomains') { ?> class="active" <?php } ?>>Manage University</a>
				</li>
				<li>
					<a href="<?php echo $this->Html->url(SITE_LINK.'setting'); ?>" <?php if ($this->params['controller'] == 'plans') { ?> class="active" <?php } ?>>Setting</a>
				</li>				
				
				<li>
					<a href="<?php echo $this->Html->url(SITE_LINK.'CmsPages'); ?>" <?php if ($this->params['controller'] == 'cmspages') { ?> class="active" <?php } ?>>Pages</a>
				</li>
				<li>
					<a href="<?php echo $this->Html->url(SITE_LINK.'email-templates'); ?>" <?php if ($this->params['controller'] == 'emailtemplates') { ?> class="active" <?php } ?> >Email </a>
				</li>
				
				
				 <li>
					<a href="<?php echo $this->Html->url(SITE_LINK.'manage-profile'); ?>" <?php if ($this->params['action'] == 'admin_profile') { ?> class="active" <?php } ?>>Manage Profile</a>
				</li> 				
			</ul>
		<?php } ?>
    </div>
<?php } ?>

