<?php /*
<header class="header">
	<nav class="navbar navbar-theme navbar-fixed-top">
	  <div class="container">
		<div class="top-manuBar">
			<a class="round-btn" href="#loginModel" data-toggle="modal">ADVERTISE</a>
		</div>
		<div class="navbar-header">
		  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		  </button>
		  <a class="navbar-brand brand-logo" href="#"><?php echo $this->Html->image("logo.png",array("alt"=>"Moola")); ?></a>
		</div>
		<div id="navbar" class="navbar-collapse collapse" class="header-menu"> 
		  <ul class="nav navbar-nav main-menu">
			<li class="active"><a href="#">Home</a></li>
			<li><a href="#about">About</a></li>
			<!-- <li><a href="#about">Pricing</a></li>
			<li><a href="#">faq</a></li> -->
		  </ul>
		</div><!--/.nav-collapse -->
	  </div>
	</nav>
</header>
*/ ?>
<?php if ($this->params['controller'] == 'pages' && $this->params['action'] == 'index' ) { ?>
<div class="mob-menu">
       <ul class="nav-top-menu list-inline header-top">
          <li><a class="round-btn" href="<?php echo SITE_LINK."advertise-with-moola"; ?>">ADVERTISE</a></li>
       </ul>
   </div>

    <header class="header">
        <nav class="navbar navbar-theme navbar-theme">
          <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand brand-logo" href="<?php echo SITE_LINK; ?>"><?php echo $this->Html->image("logo.png",array("alt"=>"Moola")); ?></a>
            </div>
            <div id="navbar" class="navbar-collapse collapse header-menu"> 
              <ul class="nav navbar-nav main-menu">
                <li <?php echo ($this->params['controller'] == 'pages' && $this->params['action'] == 'index' )? 'class="active"':''?>><a href="<?php echo SITE_LINK; ?>">Home</a></li>
                <li <?php echo ($this->params['controller'] == 'pages' && $this->params['action'] == 'about' )? 'class="active"':''?>><a href="<?php echo SITE_LINK; ?>about">About</a></li>
                <li><a href="<?php echo SITE_LINK; ?>blog/">Blog</a></li>
                <!-- <li><a href="#about">Pricing</a></li>
                <li><a href="#">faq</a></li> -->
              </ul>
              <ul class="nav-top-menu list-inline">
                  <li><a class="round-btn" href="<?php echo SITE_LINK."advertise-with-moola"; ?>">ADVERTISE</a></li>
              </ul>
            </div><!--/.nav-collapse -->
          </div>
        </nav>
    </header>
    <!-- End header sec -->
<?php } else { ?>
	<div class="mob-menu">
       <ul class="nav-top-menu list-inline header-top">
          <li><a class="round-btn btn-blue" href="#regModel" data-toggle="modal" >Sign up</a></li>
          <li><a class="round-btn" href="#loginModel" data-toggle="modal">Sign in</a></li>
			<?php if($this->request->action=="merchant_detail") {?>	  
		 <li><a  class="shareSelector" href="javascript:void()" ><?php echo $this->Html->image("share-icon.png",array("alt"=>"Moola","class"=>"share-icon")); ?></a></li>                
	  </ul>
	  <?php }?>
   </div>

    <header class="header inner-page-header">
        <nav class="navbar navbar-theme navbar-fixed-top">
          <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand brand-logo" href="<?php echo SITE_LINK; ?>"><?php echo $this->Html->image("logo.png",array("alt"=>"Moola")); ?></a>
            </div>
            <div id="navbar" class="navbar-collapse collapse header-menu"> 
              <ul class="nav navbar-nav main-menu">
                <li><a href="<?php echo SITE_LINK; ?>">Home</a></li>
                <li <?php echo ($this->params['controller'] == 'pages' && $this->params['action'] == 'about' )? 'class="active"':''?>><a href="<?php echo SITE_LINK; ?>about">About</a></li>
                <li <?php echo ($this->params['controller'] == 'pages' && $this->params['action'] == 'pricing' )? 'class="active"':''?> ><a href="<?php echo SITE_LINK; ?>pricing">Pricing</a></li>
                <li <?php echo ($this->params['controller'] == 'pages' && $this->params['action'] == 'advertiser_faqs' )? 'class="active"':''?>><a href="<?php echo SITE_LINK; ?>advertiser_faqs">faq</a></li>
                <li><a href="<?php echo SITE_LINK; ?>blog/">Blog</a></li>
              </ul>
              <ul class="nav-top-menu list-inline header-top">
                 <li><a class="round-btn btn-blue" href="#regModel" data-toggle="modal" >Sign up</a></li>
                 <li><a class="round-btn" href="#loginModel" data-toggle="modal">Sign in</a></li>
              </ul>
              <?php if($this->request->action=="merchant_detail") {?>
               <ul class="nav-top-menu list-inline header-top">
                 <li><a  class="shareSelector" href="javascript:void()" ><?php echo $this->Html->image("share-icon.png",array("alt"=>"Moola","class"=>"share-icon")); ?></a></li>                
              </ul>
              <?php }?>
            </div><!--/.nav-collapse -->
          </div>
        </nav>
    </header>
    <!-- End header sec -->
<?php } ?>
