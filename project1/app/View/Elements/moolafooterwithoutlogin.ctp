<footer class="footer">
	<div class="footer-top">
		 <div class="container">
			 <div class="footer-widget">
				 <div class="col-9 ftr-left">
					 <div class="ftr-social">
						<a href="<?php echo SITE_LINK ?>" class="ftr-logo">
							<?php echo $this->Html->image("logo.png"); ?>
						</a>
						<div class="social-links">
							<h5>Follow us on</h5>
							<ul class="social">
							   <li><a href="https://www.facebook.com/moola.rewards/" target="_blank"><i class="fa fa-facebook-f"></i></a></li>
							   <li><a href="https://twitter.com/MoolaRewards" target="_blank"><i class="fa fa-twitter"></i></a></li>
							   <li><a href="https://www.youtube.com/channel/UCABfi1EObH7mdgT6Z3Dynug" target="_blank"><i class="fa fa-youtube"></i></a></li>
							   <li><a href="https://www.instagram.com/moola.rewards/" target="_blank"><i class="fa fa-instagram"></i></a></li>

							</ul>
						</div>
					 </div>
					 <div class="basic-links">
						 <h4>Important Links</h4>
						 <div class="infoLink">
							 <ul>
								 <li><a href="<?php echo SITE_LINK?>advertise-with-moola">Advertise</a></li>
								 <li><a href="<?php echo SITE_LINK?>about">About </a></li>
								 <li><a href="<?php echo SITE_LINK?>contact_us">Contact Us</a></li>
								
								 
							 </ul>
							 <ul>															 
								<li><a href="<?php echo SITE_LINK?>help">Help</a></li>
								<li><a href="<?php echo SITE_LINK?>student_support_faqs">Support FAQs</a></li>
								<li><a href="<?php echo SITE_LINK?>terms_and_conditions">Term & Conditions</a></li>
								
							 </ul>
							 <ul>								 
								 <li><a href="<?php echo SITE_LINK?>privacy_policy">Privacy Policy</a></li>
								 <li><a href="<?php echo SITE_LINK?>cookie_policy">Cookie Policy</a></li>	
							 </ul>
						 </div>
						 <div class="copyright">
							 <p>Copyright © 2018 Moola ® All Rights Reserved.</p>
						 </div>
					 </div>
				 </div>
				 <div class="col-3 ftr-right">
					 <h4>DOWNLOAD our APP</h4>
					 <div class="dwnload-apps application">
						<ul class="list-inline">
							<li><a href="https://play.google.com/store/apps/details?id=com.androidzestminds.moola" target="_blank" > <span class="fa fa-android st-appiocn"></span> <p> available on <span> Google Store </span></p></a></li>
							<li><a href="https://itunes.apple.com/in/app/moolarewards/id1434308130?mt=8" target="_blank" > <span class="fa fa-apple st-appiocn"></span>  <p>available on <span> Apple Store </span></p></a></li>
						</ul>
					 </div>
				 </div>
			 </div>
		 </div>
	</div>
</footer>
 <!--/. footer sec -->

 <!--   Modelox= Section -->
<?php if (!$this->Session->read("Auth.User.id")) { ?>
 <!--Login Modal HTML -->
	<div id="loginModel" class="modal fade loginModel themeModel">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Login to create your voucher!</h4>
				</div>
				
				<div class="modal-body">
					<div class="fb-login" onclick="fblogin()">
						<a href="javascript:void(0);"><i class="fa fa-facebook-f"></i> Connect With Facebook</a>
					</div>
					<span class="or-divider">OR</span>
					<div class="alert alert-warning" id="error_msg" style="display:none;"></div>
					<div class="alert alert-danger" id="error_msg_captcha" style="display:none;"><strong>Attention!</strong> Please check captcha to proceed.</div>
					
					<?php echo $this->Form->create("User",array("url"=>"/login","class"=>"themeForm","id"=>"UserLoginForm","novalidate"=>"true")); ?>
					
						<div class="form-group">
							<div class="inputIcon-grp">
								<span class="inpIcon"><?php echo $this->Html->image("env-icon.png"); ?></span>
								<?php echo $this->Form->input("username",array("type"=>"text","maxlength"=>100,"class"=>" inputText form-control","div"=>false,"label"=>false,"placeholder"=>"Email","id"=>"loginusername")); ?>
							</div>
						</div>
						<div class="form-group">
						   <div class="inputIcon-grp">
								<span class="inpIcon"><?php echo $this->Html->image("pwd-icon.png"); ?></span>
								<?php echo $this->Form->input("password",array("type"=>"password","maxlength"=>100,"class"=>"inputText form-control","div"=>false,"label"=>false,"placeholder"=>"Password","id"=>"loginpassword")); ?>
							</div>
						</div>
						<div class="g-recaptcha-login" data-sitekey="<?php echo SITE_KEY; ?>" id="RecaptchaField1"></div>
						<input type="hidden" class="hiddenRecaptcha" name="login_hiddenRecaptcha" value="" id="login_hiddenRecaptcha">						
						<div class="form-submit text-right">
						<div class="forg-pwd"><a href="javascript:void(0);" id="forgetpopup">Forgot Password?</a></div>   
						   <span class="logSwitch">Don’t have an account? <a href="javascript:void(0);" id="signupPopup">SIGN UP</a></span>
						   <button type="submit" class="btn btn-theme">SIGN IN</button>
						</div>
					<?php echo $this->Form->end(); ?>
				</div>                   
			</div>
		</div>
	</div>

<!--Reg Modal HTML -->
	<div id="regModel" class="modal fade loginModel themeModel">
		
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Get Started for free</h4>
				</div>
				<div class="modal-body">
					<div class="fb-login" onclick="fblogin()">
						<a href="javascript:void(0);"><i class="fa fa-facebook-f"></i> Connect With Facebook</a>
					</div>
					<span class="or-divider">OR</span>
					<div id="error_msg_signup" class="alert alert-warning" style="display:none"></div><div class="alert alert-success" style="display:none" id="success_msg_signup"></div>
					<div class="alert alert-danger" id="error_msg_captcha_signup" style="display:none;"><strong>Attention!</strong> Please check captcha to proceed.</div>
					<?php echo $this->Form->create("User",array("url"=>"/signup","class"=>"themeForm","id"=>"UserSignupForm","novalidate"=>"true")); ?>
						<div class="form-group">
						   <div class="inputIcon-grp">
							 <span class="inpIcon"><?php echo $this->Html->image("user-icon.png"); ?></span>
							 <?php echo $this->Form->input("UserDetail.name",array("type"=>"text","maxlength"=>100,"class"=>"inputText form-control","div"=>false,"label"=>false,"placeholder"=>"Business Name")); ?>							 
							</div>
						</div>
						<div class="form-group">
						   <div class="inputIcon-grp">
							 <span class="inpIcon"><?php echo $this->Html->image("env-icon.png"); ?></span>
							 <?php echo $this->Form->input("User.username",array("type"=>"text","maxlength"=>100,"class"=>"inputText form-control","div"=>false,"label"=>false,"placeholder"=>"Email")); ?>
							</div>
						</div>
					   <div class="form-group">
						   <div class="inputIcon-grp">
							 <span class="inpIcon"><?php echo $this->Html->image("pwd-icon.png"); ?></span>
							 <?php echo $this->Form->input("User.password",array("type"=>"password","maxlength"=>100,"class"=>"inputText form-control","div"=>false,"label"=>false,"placeholder"=>"Password")); ?>
							</div>
						</div> 
						<div class="g-recaptcha_signup" data-sitekey=<?php echo SITE_KEY; ?> id="RecaptchaField2"></div>
						<input type="hidden" class="hiddenRecaptcha" name="signup_hiddenRecaptcha" id="signup_hiddenRecaptcha">						
						<div class="form-submit text-right">
						   <span class="logSwitch">Already have an account? <a href="javascript:void(0);" id="loginPopup" >SIGN IN</a></span>
						   <button type="submit" class="btn btn-theme">Sign Up</button>
						</div>
					<?php echo $this->Form->end(); ?>
				</div>                   
			</div>
		</div>
	</div>
	
<!-- Forget Passowrd HTML -->
	<div id="forgetModel" class="modal fade loginModel themeModel">
		
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Forgot Password</h4>
					<p class="modal-title">Enter your email below to receive your password reset instructions</p>
				</div>
				<div class="modal-body">					
					<div class="alert alert-warning" style="display:none" id="error_msg_forget"></div><div style="display:none" class="alert alert-success" id="success_msg_forget"></div>
					<div class="alert alert-danger" id="error_msg_captcha_forget" style="display:none;"><strong>Attention!</strong> Please check captcha to proceed.</div>
					<?php echo $this->Form->create("User",array("url"=>"/forgot_password","class"=>"themeForm","id"=>"UserForgotPasswordForm","novalidate"=>"true")); ?>
						<div class="form-group">
						   <div class="inputIcon-grp">
							 <span class="inpIcon"><?php echo $this->Html->image("env-icon.png"); ?></span>
							<?php echo $this->Form->input("email",array("type"=>"text","maxlength"=>200,"class"=>"inputText form-control","div"=>false,"label"=>false,'placeholder'=>"Enter Email")); ?>					 
							</div>
						</div>		
						<div class="g-recaptcha_forgetpass" data-sitekey=<?php echo SITE_KEY; ?> id="RecaptchaField3"></div>
						<input type="hidden" class="hiddenRecaptcha" name="forgetpass_hiddenRecaptcha" id="forgetpass_hiddenRecaptcha">			
						<div class="form-submit text-right">
						   <button type="submit" class="btn btn-theme">Send Password</button>
						</div>
					<?php echo $this->Form->end(); ?>
				</div>                   
			</div>
		</div>
</div>
<?php if(isset($token)) { ?>
<!--Reset Passowrd HTML -->
	<div id="resetModel" class="modal fade loginModel themeModel">
		
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title">Reset Password</h4>				
				</div>
				<div class="modal-body">					
					<div class="alert alert-warning" style="display:none" id="error_msg_reset"></div><div class="alert alert-success" style="display:none" id="success_msg_reset"></div>
					<div class="alert alert-danger" id="error_msg_captcha_reset" style="display:none;"><strong>Attention!</strong> Please check captcha to proceed.</div>
					<?php echo $this->Form->create("User",array("url"=>"/process_reset_password","class"=>"themeForm","id"=>"UserResetPasswordForm","novalidate"=>"true")); ?>
						<div class="form-group">
						   <div class="inputIcon-grp">
							   <?php echo $this->Form->hidden("User.token",array("value"=>$token)); ?>	 
							 <span class="inpIcon"><?php echo $this->Html->image("pwd-icon.png"); ?></span>
							 <?php echo $this->Form->input("User.newpassword",array("type"=>"password","maxlength"=>50,"class"=>"inputText form-control","div"=>false,"label"=>false,'placeholder'=>"New Password")); ?> 
							</div>
						</div>	
						  <div class="form-group">
						   <div class="inputIcon-grp">
							 <span class="inpIcon"><?php echo $this->Html->image("pwd-icon.png"); ?></span>
							 <?php echo $this->Form->input("User.confirm_password",array("type"=>"password","maxlength"=>50,"class"=>"inputText form-control","div"=>false,"label"=>false,"placeholder"=>"Confirm Password")); ?>
							</div>
						</div> 	
										
						<div class="form-submit text-right">
						   <button type="submit" class="btn btn-theme">Set Password</button>
						</div>
					<?php echo $this->Form->end(); ?>
				</div>                   
			</div>
		</div>
</div>
<?php } ?>
</div>
<?php }?>
<script>
var widgetId1;
var widgetId2;
var widgetId3;
var widgetId4;
var widgetId5;

var CaptchaCallback = function() {
    widgetId1 = grecaptcha.render('RecaptchaField1', {'sitekey' : '<?php echo SITE_KEY; ?>', 'callback' : correctCaptcha_login});
    widgetId2 = grecaptcha.render('RecaptchaField2', {'sitekey' : '<?php echo SITE_KEY; ?>', 'callback' : correctCaptcha_signup});
    widgetId3 = grecaptcha.render('RecaptchaField3', {'sitekey' : '<?php echo SITE_KEY; ?>', 'callback' : correctCaptcha_forgetpass});
    //widgetId4 = grecaptcha.render('RecaptchaField4', {'sitekey' : '<?php echo SITE_KEY; ?>', 'callback' : correctCaptcha_resetpass});
  	widgetId5 = grecaptcha.render('RecaptchaField5', {'sitekey' : '<?php echo SITE_KEY; ?>', 'callback' : correctCaptcha_contactus});

};
var correctCaptcha_login = function(response) {
    $("#login_hiddenRecaptcha").val(response);
};
var correctCaptcha_signup = function(response) {
    $("#signup_hiddenRecaptcha").val(response);
};
var correctCaptcha_forgetpass = function(response) {
    $("#forgetpass_hiddenRecaptcha").val(response);
};
var correctCaptcha_contactus = function(response) {
    $("#contactus_hiddenRecaptcha").val(response);
};
</script>

<script> $('#flashMessage').delay(5000).fadeOut();</script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-131170044-1"></script>
<script>
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());

gtag('config', 'UA-131170044-1');
</script>

