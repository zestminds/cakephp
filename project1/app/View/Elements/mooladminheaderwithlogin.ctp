<?php 
$user = $this->Session->read("Auth");
	if(isset($user['User'])){ 
			 $name= $user['User']['UserDetail']['name']	;
			 if($name==""){$name="Admin";}
			 $email= $user['User']['username']; 
			 $fname= explode(" ",$name);								 
			 if(empty($user['User']['UserDetail']['image'])  || !file_exists(WWW_ROOT."img/profile/".$user['User']['UserDetail']['image'])){$imageprofile=SITE_LINK."img/default.jpeg";}
			 else{$imageprofile= "profile/".$user['User']['UserDetail']['image'];}
			 $logout=$this->Html->link('Logout', SITE_LINK.'/logout',array('class'=>'round-btn btn-blue')); 
	}
?>	
  
<header class="tp-header">
    <div class="tp-hd-mid">
      <div class="admin-logo"> <a href=""><?php echo $this->Html->image("admin-logo.png",array("alt"=>"Moola Voucher"));?></a> <span class="btn-toggle"><?php echo $this->Html->image("toggle-button.png",array("alt"=>"Moola Voucher"));?></span> </div>
      <div class="hdr-right">
        <div class="hd-md-right">
          <ul class="list-left list-inline">
           
          </ul>
          
			  <div class="srh-bar">
				<?php /* echo $this->Form->create(array("url"=>"/view-vouchers","id"=>"allsearch","type"=>"get")); ?>
					<button class="btn-searh"><i class="fa fa-search"></i></button>
					<input type="text" class="form-control" name="srch" id="srch" placeholder="search..." value="<?php echo (isset($srch)?$srch:""); ?>">
				<?php echo $this->Form->end(); */ ?>
			  </div>
          
          <div class="usrarea dropdown"> <a class="dropdown-toggle" data-toggle="dropdown"> <span class="user_img"><?php echo $this->Html->image($imageprofile,array("alt"=>"user"));?></span> <span class="name-usr"><?php echo $fname[0]; ?></span> <span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li><a href="<?php echo $this->Html->url(SITE_LINK.'logout'); ?>">Log out</a></li>              
            </ul>
          </div>
        </div>
      </div>
    </div>
</header>
