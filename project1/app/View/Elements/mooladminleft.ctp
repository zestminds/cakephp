<div class="cnt_mid">
	<aside class="das-sidebar">
	  <div class="lcmain">
		<ul class="main-nav">
		  <li class="parent "><a href="<?php echo $this->Html->url(SITE_LINK.'dashboard'); ?>"><?php echo $this->Html->image("dasbord-img-1.png",array("alt"=>"Moola Voucher Dashboard"));?> Dashboard </a> <span class="arrow-toggle"> <i class="fa fa-angle-right"></i> </span>
		   </li>
		  <li class="parent active"><a href="javascript:void(0)"> <?php echo $this->Html->image("dasbord-img-2.png",array("alt"=>"Manage Users"));?>Manage Users</a>
			<ul class="subchilds" style="display:block;">
			  <li class="student"><a href="<?php echo $this->Html->url(SITE_LINK.'manage-students'); ?>"> <i class="fa fa-circle-o"></i> Students </a></li>
			  <li class="marchant"><a href="<?php echo $this->Html->url(SITE_LINK.'manage-merchants'); ?>"> <i class="fa fa-circle-o"></i> Merchants </a></li>			  
			</ul>
			 <span class="arrow-toggle"> <i class="fa fa-angle-right"></i> </span>
		  </li>
		  <li class="parent"><a href="<?php echo $this->Html->url(SITE_LINK.'view_business'); ?>"><?php echo $this->Html->image("location.png",array("alt"=>"Insights"));?> Manage Location </a>
		  <span class="arrow-toggle"> <i class="fa fa-angle-right"></i> </span>
		  </li>
		  <li class="parent"><a href="<?php echo $this->Html->url(SITE_LINK.'view-vouchers'); ?>"><?php echo $this->Html->image("voucher.png",array("alt"=>"Insights"));?> Manage Vouchers </a>
		  <span class="arrow-toggle"> <i class="fa fa-angle-right"></i> </span>
		  </li>
		  <li class="parent"><a href="<?php echo $this->Html->url(SITE_LINK.'manage-categories'); ?>"><?php echo $this->Html->image("category.png",array("alt"=>"Insights"));?>Manage Category </a>
		   <span class="arrow-toggle"> <i class="fa fa-angle-right"></i> </span>
		  </li>
		 <!-- <li class="parent"><a href="<?php echo $this->Html->url(SITE_LINK.'manage-locations'); ?>"><?php echo $this->Html->image("view_business.png",array("alt"=>"Insights"));?>Manage Location </a>
		  <span class="arrow-toggle"> <i class="fa fa-angle-right"></i> </span>
		  </li>-->
		  <!--<li class="parent"><a href="<?php echo $this->Html->url(SITE_LINK.'manage-universities'); ?>"><?php echo $this->Html->image("university.png",array("alt"=>"Insights"));?>Manage University &nbsp;</a>
		  <span class="arrow-toggle"> <i class="fa fa-angle-right"></i> </span>
		  </li>	-->	
		   <li class="parent"><a href="<?php echo $this->Html->url(SITE_LINK.'manage-domain-extensions'); ?>"><?php echo $this->Html->image("domain.png",array("alt"=>"Insights"));?>Manage University &nbsp;</a>
		  <span class="arrow-toggle"> <i class="fa fa-angle-right"></i> </span>
		  </li>	 
		  <li class="parent"><a href="<?php echo $this->Html->url(SITE_LINK.'manage-profile'); ?>"><?php echo $this->Html->image("user.png",array("alt"=>"Manage Profile"));?>Manage Profile </a>
		  <span class="arrow-toggle"> <i class="fa fa-angle-right"></i> </span>
		  </li>
		  <li class="parent"><a href="<?php echo $this->Html->url(SITE_LINK.'view-insights'); ?>"><?php echo $this->Html->image("dasbord-img-3.png",array("alt"=>"Insights"));?>Insights </a>
		  <span class="arrow-toggle"> <i class="fa fa-angle-right"></i> </span>
		  </li>
		  <li class="parent"><a href="<?php echo $this->Html->url(SITE_LINK.'view-sales'); ?>"><?php echo $this->Html->image("dasbord-img-4.png",array("alt"=>"Sales"));?>Sales</a>
		  <span class="arrow-toggle"> <i class="fa fa-angle-right"></i> </span>
		  </li>	
		  <li class="parent"><a href="<?php echo $this->Html->url(SITE_LINK.'send_notifications'); ?>"><?php echo $this->Html->image("alarm.png",array("alt"=>"Send Notifications"));?>Send Notifications</a>
		  <span class="arrow-toggle"> <i class="fa fa-angle-right"></i> </span>
		  </li>			  
		   <li class="parent active"><a href="javascript:void(0)"> <?php echo $this->Html->image("dasbord-img-5.png",array("alt"=>"Setting"));?>Settings </a>		 
			<ul class="subchilds" style="display:block;">
			  <li class="student"><a href="<?php echo $this->Html->url(SITE_LINK.'setting'); ?>"> <i class="fa fa-circle-o"></i> Plans </a></li>
			  <li class="marchant"><a href="<?php echo $this->Html->url(SITE_LINK.'CmsPages'); ?>"> <i class="fa fa-circle-o"></i> CmsPages </a></li>
			  <li class="student"><a href="<?php echo $this->Html->url(SITE_LINK.'email-templates'); ?>"> <i class="fa fa-circle-o"></i> Email Templates </a>
			  </li>			  
			</ul>
			 <span class="arrow-toggle"> <i class="fa fa-angle-right"></i> </span>
		  </li>
		</ul>
	  </div>
	</aside>

