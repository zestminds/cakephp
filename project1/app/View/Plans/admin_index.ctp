<div class="cont-right">
   <div class="cont-right-innr">	
   <div class="main-hd-in">
          <div class="row">
            <div class="col-sm-12">
              <h2 class="title-das"> Manage Voucher Plan</h2>   
            </div>
          </div>
        </div>
		<div class="categories index border-box">
			<?php echo $this->Form->create("voucherplan",array("div"=>false,)); ?>
			<div class="table-responsive">
				<table class="Marchant-table table table-bordered" width="100%">					
					<tr>			
							<th><?php echo $this->Paginator->sort('title'); ?></th>
							<th><?php echo $this->Paginator->sort('description'); ?></th>			
							<th><?php echo $this->Paginator->sort('price'); ?></th>			
							<th class="actions"><?php echo __('Actions'); ?></th>
					</tr>	<?php foreach ($plans as $plan): ?>
					<tr>		
						<td><?php echo h($plan['Plan']['title']); ?>&nbsp;</td>
						<td><?php echo substr(h($plan['Plan']['description']),0,120); ?>...&nbsp;</td>
						<td><?php echo h($plan['Plan']['price']); ?>&nbsp;</td>
						<td>
							<a href="<?php echo SITE_LINK.'edit-plan/'.$plan['Plan']['id']?>"><i class="fa fa-pencil"></i></a>		
									
							<?php //echo $this->Html->link(__('Edit'), array('action' => 'edit', $plan['Plan']['id'])); ?>
							<?php //echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $plan['Plan']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $plan['Plan']['id']))); ?>
						</td>
					</tr>
				  <?php endforeach; ?>				  
			  </table>	
			</div>
		</div>	
    </div>		
</div>
