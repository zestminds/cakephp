<div class="plans view">
<h2><?php echo __('Plan'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($plan['Plan']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Title'); ?></dt>
		<dd>
			<?php echo h($plan['Plan']['title']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Description'); ?></dt>
		<dd>
			<?php echo h($plan['Plan']['description']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Features'); ?></dt>
		<dd>
			<?php echo h($plan['Plan']['features']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Price'); ?></dt>
		<dd>
			<?php echo h($plan['Plan']['price']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($plan['Plan']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($plan['Plan']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Plan'), array('action' => 'edit', $plan['Plan']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Plan'), array('action' => 'delete', $plan['Plan']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $plan['Plan']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Plans'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Plan'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Biilings'), array('controller' => 'biilings', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Biiling'), array('controller' => 'biilings', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Biilings'); ?></h3>
	<?php if (!empty($plan['Biiling'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th><?php echo __('Plan Id'); ?></th>
		<th><?php echo __('Bill Dates'); ?></th>
		<th><?php echo __('Bill Status'); ?></th>
		<th><?php echo __('Is Active'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($plan['Biiling'] as $biiling): ?>
		<tr>
			<td><?php echo $biiling['id']; ?></td>
			<td><?php echo $biiling['user_id']; ?></td>
			<td><?php echo $biiling['plan_id']; ?></td>
			<td><?php echo $biiling['bill_dates']; ?></td>
			<td><?php echo $biiling['bill_status']; ?></td>
			<td><?php echo $biiling['is_active']; ?></td>
			<td><?php echo $biiling['created']; ?></td>
			<td><?php echo $biiling['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'biilings', 'action' => 'view', $biiling['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'biilings', 'action' => 'edit', $biiling['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'biilings', 'action' => 'delete', $biiling['id']), array('confirm' => __('Are you sure you want to delete # %s?', $biiling['id']))); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Biiling'), array('controller' => 'biilings', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
