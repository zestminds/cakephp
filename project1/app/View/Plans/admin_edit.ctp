<script>
	$(document).ready(function () {
    $('#PlanAdminEditForm').validate({ // initialize the plugin
        rules: {			
			 
			  "data[Plan][title]": {
				required: true				
			  },
			  "data[Plan][description]": {
				required: true				
			  }	
			  ,
			  "data[Plan][price]": {
				required: true				
			  }		
			},
			// Specify validation error messages
			messages: {			
				"data[Plan][title]": {
					required: "Please enter Title"			
				},			 
			  "data[Plan][description]": {
					required: "Please enter Description	"			
				}
				,			 
			  "data[Plan][price]": {
					required: "Please enter Price"			
				}
			},
			// Make sure the form is submitted to the destination defined
			// in the "action" attribute of the form when valid
			submitHandler: function(form) {
			  form.submit();
			}
		  });		
});
</script>
<div class="cont-right">
   <div class="cont-right-innr">
   <legend><?php echo __('Edit Plan'); ?></legend>
		<div class="plans form border-box2">
		<?php echo $this->Form->create('Plan'); ?>
			<fieldset>
				
			<?php
				echo $this->Form->input('id');
				echo $this->Form->input('title');
				echo $this->Form->input('description',array('type'=>'textarea'));
				echo $this->Form->input('price',array('label'=>'Price (£)'));
			?>
			</fieldset>
		<div style="padding-left:0px"><?php echo $this->Form->end(__('Submit')); ?></div>
		</div>
	</div>		
</div>
