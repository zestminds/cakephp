<div class="plans index">
	<h2><?php echo __('Membership'); ?></h2>
	<?php echo $this->Form->create('Plan',array("type"=>"file"));  ?>
	<table cellpadding="0" cellspacing="0">		
	<tbody>
	
	<tr>	
		<td  style="width:40%">
		<table cellpadding="0" cellspacing="0">	
			<?php $i=1;foreach ($plans as $plan): ?>
			<tr>
				<td><?php echo $this->Form->radio('type',array($plan['Plan']['id']=>$plan['Plan']['title']), array('legend' => false,'checked'=>(($i==1) ? "checked" : ""),'value' => $plan['Plan']['id'])); echo "<br>"; echo  h($plan['Plan']['description']); ?>
				</td>				
			</tr>
			<?php $i++;endforeach; ?>			
		</table>
		</td>	
		<td>&nbsp;</td>
		<td class="actions">	
			<table cellpadding="0" cellspacing="0" style="border:none">		
				<tr>
					<td>Billing Information</td>
				</tr>				
				<tr>
					<td><?php echo $this->Form->input('name_on_card',array('label' => 'Name On Card','placeholder'=>'Business Name'));?></td>
				</tr>
				<tr>
					<td><?php echo $this->Form->input('card_number',array('label' => 'Card Number','placeholder'=>'Card Number'));?></td>
				</tr>
				<tr>
					<td><?php echo $this->Form->input('cvv',array('label' => 'CVV','placeholder'=>'123'));?></td>
				</tr>
				<tr>
					<td><?php echo $this->Form->input('expiry_year',array('label' => 'Expiry Month/Year','placeholder'=>'MM/YY	'));?></td>
				</tr>
				<tr>
					<td><?php echo $this->Form->submit('Pay Now')?></td>
				</tr>
			</table>	
		</td>
	</tr>

	</tbody>
	</table>	
	<?php echo $this->Form->end(); ?>	
</div>
