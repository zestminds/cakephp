<link rel="stylesheet" type="text/css" href="<?php echo SITE_LINK ?>css/moola/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="<?php echo SITE_LINK ?>css/moola/font-awesome.min.css" />
<link rel="stylesheet" type="text/css" href="<?php echo SITE_LINK ?>css/moola/style.css" />
<link rel="stylesheet" type="text/css" href="<?php echo SITE_LINK ?>css/moola/responsive.css" />


 <!--Voucher Offer Modal HTML 	 <img src='<?php echo $src ;?>' style="width:100%;height:275px">    -->
<?php (h($voucher["Voucher"]["image"])!="" || !file_exists(WWW_ROOT."/img/voucher/".$voucher["Voucher"]["image"]))?$src=SITE_LINK."img/voucher/".h($voucher["Voucher"]["image"]):$src=SITE_LINK."img/default_voucher.jpeg"?>
		  
<div class="vouch-offer" onload="myFunction()" style="margin:0;">
	<div class="auto-bar">
	  <div class="mobile-main">
		<div class="ng-header">
		<div class="container mobile-container">
		  <div class="row">
			<div class="col-sm-12"> 
				<span class="back_arrow"><a href="javascript:void(0);"><?php echo $this->Html->image("back-arrow.png",array("alt"=>"Voucher"));?></a></span>Offer Details <span></span>
			  <div class="hd-right">
				<ul class="list-inline">
				  <li><a href="javascript:void(0);"><?php echo $this->Html->image("fav-iocn.png",array("alt"=>"favorite"));?></a></li>
				<li><a href="javascript:void(0);"><?php echo $this->Html->image("share-iocn.png",array("alt"=>"Share"));?></a></li>
				</ul>
			  </div>
			</div>
		  </div>
		</div>
	  </div>	  
	  <div class="mobile_offer"><img class="VoucherImg" src="<?php echo $src; ?>"/></div>
	  <section class="jusic-banner"> 	  
		<div class="cnt_middle">
		  <div class="cnt-contant">		 
			<div class="snu-profile">
				 
			  <div class="avtar-img">
				   <?php (h($voucher["UserDetail"]["image"])!="" || !file_exists(WWW_ROOT."/img/profile/".$voucher["UserDetail"]["image"]))?$src=SITE_LINK."img/profile/".h($voucher["UserDetail"]["image"]):
					  $src=SITE_LINK."img/default.jpeg"?>
					  <img src='<?php echo $src ;?>'>;
			  </div>
			  <h1 class="text-center"><?php echo h($voucher['UserDetail']['business']); ?></h1>
			  <div class="row">
				<div class="snu-student">
				  <ul class="list-inline">
					<li>
					  <div class="banr-text"> <span class="disct"><?php echo h($voucher['Voucher']['title_discount']); ?></span>
						<p> Student <span>Discount</span></p>
					  </div>
					</li>
					<li>
					  <div class="banr-text">
						<p> <span class="break-content"><?php echo h($voucher['Voucher']['sub_title']); ?> </span></p>
					  </div>
					</li>
				  </ul>
				</div>
			  </div>
			  <span class="show-status"><?php echo $hours_left;?></span> </div>
			<div class="student-conditions">
			  <div class="blog-artiale">
				<div class="map-left"><iframe  class="map-img" src="https://www.google.com/maps?q=<?php echo $voucher['UserDetail']['address'];?>&output=embed"></iframe></div>
				<div class="articale-right">
				  <p>Less than 0.5 miles away</p>
				  <h2><?php echo h($voucher['UserDetail']['address']); ?></h2>
				</div>
			  </div>
			  <div class="blog-artiale">
				<h2><?php echo $this->Html->image("about-help.png",array("alt"=>"About this discount"));?>About this discount</h2>
				<p><?php echo substr(h($voucher['Voucher']['description']),0,75); ?>... </p>
				<a href="javascript:void(0);">Read More</a> </div>
			  <div class="blog-artiale">
				<h2><?php echo $this->Html->image("terms-condition.png",array("alt"=>"Terms & Conditions"));?>Terms & Conditions</h2>
				<p><?php echo  substr(h($voucher['Voucher']['terms']),0,75); ; ?>...</p>
				<a href="<?php echo h($voucher['Voucher']['terms_url']); ?>" target="_blank">Read More</a> </div>
			</div>
		  </div>
		</div>
	  </section>
		<div class="mobile-footr">
			<div class="container mobile-container">
			  <div class="container mobile-container">
				<h2> <?php echo $this->Html->image("get-direcation.png",array("alt"=>"Get Directions"));?>Get Directions</h2>
			  </div>
			</div>
		</div>
	</div>
	</div>
</div>  
                   
      <!--Voucher Offer Modal HTML -->

<script type="text/javascript" src="<?php echo SITE_LINK ?>js/moola/jquery-2-2-4.min.js"></script> 
<script type="text/javascript" src="<?php echo SITE_LINK ?>js/moola/bootstrap.min.js"></script> 
<script type="text/javascript" src="<?php echo SITE_LINK ?>js/moola/custom.js"></script>
