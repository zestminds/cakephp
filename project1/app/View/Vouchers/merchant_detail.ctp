<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<title><?php echo $location['Locations']['title'].' '.$location['Locations']['address']." | Moola Rewards";?></title>
<meta name="description" content="<?php echo "<a href='https://www.moolarewards.co.uk/".$this->request->url."'>click for discounts</a>" ?>" />
<meta property="og:title" content="<?php echo h($location['Locations']['title'])." ".$location['Locations']['address']; ?>"/>
<?php $img = (($location['Locations']['image']=="" || !file_exists(WWW_ROOT."img/profile/".$location['Locations']['image']))?(SITE_LINK."img/default.jpeg"):(SITE_LINK."img/profile/".h($location['Locations']['image']))); ?>
<meta property="og:image" content='<?php echo SITE_LINK."watermark.php?img=".$img; ?>' />
<meta property="og:site_name" content="https://www.moolarewards.co.uk/<?php echo $this->request->url; ?>"/>
<meta property="og:description" content="<?php echo "click for discounts"; ?>"/>

<meta name="twitter:card" content="summary_midium_image" />
<meta name="twitter:description" content="<?php echo "click for discounts"; ?>" />
<meta name="twitter:title" content="<?php echo h($location['Locations']['title'])." ".$location['Locations']['address'];?>" />
<meta name="twitter:image" content="<?php echo SITE_LINK."watermark.php?img=".$img;  ?>" />
 <meta name="twitter:site" content="https://www.moolarewards.co.uk/<?php echo $this->request->url; ?>">
	
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<!-- css -->
<?php
echo $this->Html->meta('icon', $this->Html->url(SITE_LINK.'img/fav_icon.png'));
echo $this->Html->css(array("moola/bootstrap.min","moola/font-awesome.min","moola/owl.carousel","moola/style","moola/responsive","internal","arthref.min"));
echo $this->Html->script(array('moola/jquery-2-2-4.min'));
 ?>
<!--[if IE]>
	<script src="js/html5shiv.js"></script>
<![endif]-->

</head>
<body>
	<div class="main mola-mrchent"> 
  <!-- Preloader -->
  <div class="preLoader"></div>
	<?php echo $this->Session->flash(); ?>	
	<?php if ( !$this->Session->read("Auth.User.id") ) {
		echo $this->element("moolaheaderwithoutlogin"); 
    }
    else{
		echo $this->element("moolaheaderwithlogin"); 
	}
    ?>
    <!-- End header sec -->
		<section class="banner">
		<div class="container">
		<div class="banner-cont merchant-page">
			<div class="bnr-left">
				<div class="download-app">
					<!--client data -->
					<div class="alchemist-discounts">
					  <div class="alchemist-img location-pic-land"><img src="<?php echo SITE_LINK ?>img/profile/<?php echo $location["Locations"]["image"]?>" alt="<?php echo $location["Locations"]["title"]?>"></div>
					  <div class="alchemist-cont">
						<h2><?php echo $location["Locations"]["title"]?></h2>
						<p><?php echo $location["Locations"]["address"]?></p>
					  </div>
					</div>	
					<!--client data -->	
					<!--voucher details  data -->		
					<div class="discounts-slide">
						<div class="owl-carousel owl-theme owl-loaded owl-drag">
							<!--voucher loop   data -->
							<?php if(empty($vouchers)){echo "No Voucher to dispaly";}else{  foreach ($vouchers as  $key=>$voucher){ ?>
							<div class="item">
								<div class="discount-item">
									<div class="item-left">
										<?php echo $this->Html->Image("discount-slide.jpg"); ?>		
										<div class="venu-text">
										<h2><?php echo $voucher["Voucher"]["title_discount"]?> Student Discount</h2>
										<h4><?php echo $voucher["Voucher"]["sub_title"]?></h4>
										<p>Get Complimentary </p>
										</div>
										<span class="show-status"><?php echo (!empty($voucher["Voucher"]["start_val"]))? $voucher["Voucher"]["start_val"]:$voucher["Voucher"]["end_val"] ?></span>
									</div>
									<div class="item-right"> <img src="<?php echo $voucher["Voucher"]["voucherImageUrl"]?>" alt="<?php echo $location["Locations"]["title"]?>" >	</div>
								</div>
							</div>
							<?php }} ?>

							<!--voucher loop   data -->	
						</div>
					</div>  
					<!--voucher details  data  -->	 
					 <div class="application">
					  <ul class="list-inline">
						<li><a href="https://play.google.com/store/apps/details?id=com.androidzestminds.moola" target="_blank"> <span class="fa fa-android st-appiocn"></span>
						  <p> available on <span> Google Store </span></p>
						  </a></li>
						<li><a href="https://itunes.apple.com/in/app/moolarewards/id1434308130?mt=8" target="_blank"> <span class="fa fa-apple st-appiocn"></span>
						  <p>available on <span> Apple Store </span></p>
						  </a></li>
					  </ul>
					</div>
				</div>
			</div>            
			<div class="bnr-right">
				<div class="phone-rt">
					<?php echo $this->Html->Image("phone.png",array("alt"=>"Moola App")); ?>
				</div>
			</div> 
		</div>
		</div>
		</section>
		<!--/. banner sec -->

		<section class="who-they-are">
			<div class="container">
			  <div class="row">
				<div class="col-sm-12">
				  <div class="they-are-main">
					<div class="main-inr-ttl">
					  <h1>Who they are</h1>
					</div>
					<p><?php echo (!empty($location["Locations"]["about"]))?$location["Locations"]["about"]:"No information provided."?></p>
					<div class="social-menus">
					  <ul>
						  <?php if(!empty($location["Locations"]["facebookid"])) {?>
						<li><a href="<?php echo $location["Locations"]["facebookid"]?>" target="_blank"><i class="fa fa-facebook-f"></i></a></li>
						<?php } if(!empty($location["Locations"]["twitterid"])){?>
						<li><a href="<?php echo $location["Locations"]["twitterid"]?>" target="_blank"><i class="fa fa-twitter"></i></a></li>
						
						<?php } if(!empty($location["Locations"]["instagramid"])){?>
						<li><a href="<?php echo $location["Locations"]["instagramid"]?>" target="_blank"><i class="fa fa-instagram"></i></a></li>
						<?php }?>
					  </ul>
					</div>
				  </div>
				</div>
			  </div>
			</div>
		 </section>
		<section class="map-main">
					  <iframe  src="https://www.google.com/maps?q=<?php echo $location['Locations']['address'];?>&output=embed&z=15" style="width:100%; height:444px;border:1px solid #f1f1f1"></iframe>
		</section>
 <?php echo $this->element("moolafooterwithoutlogin"); ?>
    
<?php echo $this->Html->script(array("moola/bootstrap.min","moola/owl.carousel","moola/custom","socialShare.min")); ?>
<script src="https://www.google.com/recaptcha/api.js?onload=CaptchaCallback&render=explicit" async defer></script>

<?php echo $this->element("jscssloader",array("js"=>$jsArray,"css"=>$cssArray)); ?>
<script>
$(document).ready(function () {
		$('.discounts-slide .owl-carousel').owlCarousel({
			loop:true,
			margin:0,
			nav:false,
			dots:true,
			autoplay:true,
			autoplayTimeout:5000,
			responsive:{
				0:{
					items:1
				},
				600:{
					items:1
				},
				1000:{
					items:1
				}
			}
		});

		$('.shareSelector').socialShare({
			social: 'facebook,google,linkedin,pinterest,twitter',
			whenSelect: true,
			selectContainer: '.shareSelector'
		});
});

function socailmodel(){	
	$(".arthref").hide();
}
</script>

</body>
</html>
