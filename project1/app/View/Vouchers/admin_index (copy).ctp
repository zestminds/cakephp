<div class="vouchers index">
	<h2><?php echo __('Vouchers'); //pr($vouchers); ?></h2>
	<?php echo $this->Form->create("Voucher",array("div"=>false,)); ?>
	<div class="srch">
		<?php echo $this->element("admins/common",array("place"=>'Search by Merchant Name, Location, 	Discount ',"flag"=>false,"pageheader"=>'',"buttontitle"=>'no',"listflag"=>"no","action"=>'no')); ?>
		
	</div>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Form->input("check",array("label"=>false,"div"=>false,"id"=>'checkall',"type"=>'checkbox')); ?></th>
			<th><?php echo 'S.No.'?></th>
			<th><?php echo $this->Paginator->sort('image'); ?></th>
			<th><?php echo $this->Paginator->sort('UserDetail.name',"Merchant Name"); ?></th>
			<th><?php echo $this->Paginator->sort('title_discount','Discount'); ?></th>
			<th><?php echo $this->Paginator->sort('UserDetail.address','Location'); ?></th>	
			<th><?php echo $this->Paginator->sort('Category.title','Category'); ?></th>	
			<th><?php echo $this->Paginator->sort('start_date'); ?></th>
			<th><?php echo $this->Paginator->sort('end_date'); ?></th>			
			<th><?php echo $this->Paginator->sort('is_active','Status'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php $i=1; foreach ($vouchers as $voucher): ?>
	<tr>
				<td><?php echo $this->Form->input("id.".$voucher['Voucher']['id'],array("class"=>'chk',"value"=>$voucher['Voucher']['id'],"type"=>'checkbox',"div"=>false,"label"=>false)); ?><?php echo $this->Form->input("status.".$voucher['Voucher']['id'],array("type"=>'hidden',"value"=>($voucher['Voucher']['is_active'] == 1?0:1))); ?></td>

		<td><?php echo $i; ?>&nbsp;</td>
		<td><?php echo ($voucher['Voucher']['image'] == "") ? '<img src="'.SITE_LINK.'img/profile/default.png">':' <img src="'.SITE_LINK.'img/voucher/'.h($voucher['Voucher']['image']).'" width="75" height="75">';?></td>
		<td>
			<?php echo h($voucher['UserDetail']['name']); ?>
		</td>
		<td><?php echo h($voucher['Voucher']['title_discount']); ?>&nbsp;</td>
		<td><?php echo h($voucher['UserDetail']['address']); ?>&nbsp;</td>	
		<td><?php echo h($voucher['Category']['title']); ?>&nbsp;</td>	
		<td><?php $startdate =  date_create(h($voucher['Voucher']['start_date'])); echo date_format($startdate,"j M, Y"); ?>&nbsp;</td>
		<td><?php $enddate =  date_create(h($voucher['Voucher']['end_date'])); echo date_format($enddate,"j M, Y"); ?>&nbsp;</td>		
		<td><?php echo h(($voucher['Voucher']['is_active'] == 1)?'Active':'Inactive');  ?>&nbsp;</td>		
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $voucher['Voucher']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $voucher['Voucher']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $voucher['Voucher']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $voucher['Voucher']['title']))); ?>
		</td>
	</tr>
<?php $i++; endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
	<?php echo $this->Form->end(); ?>
</div>
