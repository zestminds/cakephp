<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		
		<?php echo $this->Html->css("video/style"); ?>
		<!--link href="css/video/style.css" rel="stylesheet"-->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
		<?php if (isset($title_for_layout)) {  ?>
	<title><?php echo $title_for_layout; ?></title>
	
<?php } else { ?>
<title><?php echo $title_for_layout; ?></title>
<?php } ?>
<meta name="google-site-verification" content="BkSs3My_0HPG5U-j4GailFxRkPGyxYsLm0uWz4Y8zNQ" />
<?php if (isset($keywords)) {  ?>
	<meta name="keywords" content="<?php echo $keywords; ?>" />
<?php } else { ?>
	<meta name="keywords" content="moola rewards" />
<?php } ?>
<?php if (isset($description)) {  ?>
	<meta name="Description" content="<?php echo $description; ?>">
<?php } else { ?>
	<meta name="Description" content="Best Deals For Students In Uk - The best mobile app moola rewards will provide discount vouchers for students nearby areas, Students can find nearby local retailers.">
<?php } ?>
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<!-- css -->
<?php 
echo $this->Html->meta('icon', $this->Html->url(SITE_LINK.'img/fav_icon.png'));
		 echo $this->Html->script("jquery-1.9.1"); ?>
	</head>
<body class="video-bg-body">
	<div class="video-section">
		<div class="video-section-main-inner">
			<a href="javascript:void(0)" class="close-video-pop" title="Close"><img src="img/video-close.png" alt="Close" title="Close"></a>
			<div class="video-section-main">
				<iframe width="" height="" src="https://www.youtube.com/embed/IvgGu1Tywks?rel=0&amp;controls=0&amp;showinfo=0&amp;modestbranding=1" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen="allowfullscreen"></iframe>
			</div>
		</div>
	</div>
	
	<script type="text/javascript">
		$( document ).ready(function() {
			$(".close-video-pop").click(function(){
				$(".video-section-main-inner").addClass("hide");
				window.location.reload();
			});
			 $('.close-video-pop').click(function(){      
		        $('.video-section-main iframe').attr('src', $('iframe').attr('src'));
				window.location.reload();
				
		    });
		});
	</script>
</body>
</html>