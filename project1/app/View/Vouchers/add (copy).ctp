<script language="JavaScript">
	function enable_text(status,value,textfield)
	{	
		if(status=="D")
		{		
			$( "#"+value ).prop( "readonly", true ); 
			var input = $( "#"+textfield).val();	
			$( "#"+value).val(input);
		}
		else
		{		
			$( "#"+value).prop( "readonly", false ); 
			
		}	
	}
	$(document).ready(function () {	
		$('#VoucherAddForm').validate({ // initialize the plugin
			rules: {			
				 
				  "data[Voucher][title]": {
					required: true,				
				  },
				  "data[Voucher][title_discount]": {
					required: true				
				  },
				  "data[Voucher][sub_title]": {
					required: true				
				  },
				  "data[Voucher][description]": {
					required: true				
				  },
				  "data[Voucher][terms]": {
					required: true				
				  },
				  "data[Voucher][start_date]": {
					required: true				
				  },
				  "data[Voucher][end_date]": {
					required: true				
				  },
				  "data[Voucher][image]": {
					required: true				
				  },
				  "data[Voucher][tags]": {
					required: true				
				  }	 ,
				  "data[Voucher][repeat_on]": {				  
					  req_question: true
				  }
				},
				// Specify validation error messages
				messages: {			
					"data[Voucher][title]": {
						required: "Please enter Title"			
					},			 
				   "data[Voucher][title_discount]": {
						required: "Please enter Discount"			
					},							
					"data[Voucher][sub_title]": {
						required: "Please enter Sub Title"			
					},			 
				   "data[Voucher][description]": {
						required: "Please enter Description"			
					},							
					"data[Voucher][terms]": {
						required: "Please enter Terms"			
					},			 
				   "data[Voucher][start_date]": {
						required: "Please select Start date"			
					},							
					"data[Voucher][end_date]": {
						required: "Please select End date"			
					},			 
				   "data[Voucher][image]": {
						required: "Please select Image"			
					},							
					"data[Voucher][tags]": {
						required: "Please enter Tags"			
					},
				  "data[Voucher][repeat_on]": {				  
					  req_question: "Please select atleast one day of week."
				  }
					
				},
				// Make sure the form is submitted to the destination defined
				// in the "action" attribute of the form when valid
				submitHandler: function(form) {
					
				  form.submit();
				}
			  });		
			  
			  
			  
			  $.validator.addMethod('req_question', function(value, elem) {
					  var radioValue = $("input[name='data[Voucher][repeat_on]']:checked").val();	
					 
						if(radioValue=='W'){
							if ($(".weekdays").children("input:checked").length <= 0) {								
								return false;
							} else { 
								return true;
							}
							
						 } else {
							return true;
						}
			  }, 'Please check at least one box in this group.');

	});
</script>
<style>
.select_hour{width:183px;}
.weekdays{float:left;}
</style>

<div class="vouchers form">
<?php echo $this->Form->create('Voucher',array("type"=>"file")); ?>
	<fieldset>
		<div>
		<legend><?php echo __('Add Voucher'); ?></legend>
		</div>
		<div style="float:left;width:45%">
			<div>
				<?php
					  echo $this->Form->hidden('user_id',array('value'=>$user_id));	
					  echo $this->Form->input('title');
				  ?>
			</div>
			<div>
				<?php 
				echo $this->Form->radio('title_status',$optionsTitle,array('label' => 'false','legend'=>'Discount Value','id'=>'title_status','value'=>'D','class'=>"titlebox",'onClick'=>'enable_text(this.value,"discountvalue","default_title")'));	
				echo $this->Form->input('title_discount',array('id'=>'discountvalue','label' => '','value'=>$defaultvalue[0]['DefaultValue']['value'],'readonly'=>'readonly'));
				 echo $this->Form->hidden("default_title",array('id'=>'default_title','value'=>$defaultvalue[0]['DefaultValue']['value']));
				?>
			</div>
			<div>
				<?php 
				echo $this->Form->input('sub_title',array('id'=>'sub_title','label' => 'Sub Title','placeholder'=>$defaultvalue[1]['DefaultValue']['value']));
				?>
			</div>
			<div>
				<?php 
				echo $this->Form->radio('description_status', $optionsDesc,array('legend'=>'Description','id'=>'description_status',"value"=>"D",'onClick'=>'enable_text(this.value,"descriptionvalue","default_description")'));	
				echo $this->Form->input('description',array('id'=>'descriptionvalue','label' => '','value'=>$defaultvalue[2]['DefaultValue']['value'],'readonly'=>'readonly'));
				echo $this->Form->hidden("default_description",array('id'=>'default_description','value'=>$defaultvalue[2]['DefaultValue']['value']));
				?>
			</div>
			<div>
				<?php 
				echo $this->Form->radio('terms_status', $optionsTerms,array('id'=>'terms_status','legend'=>'Terms and Conditions',"value"=>"D",'onClick'=>'enable_text(this.value,"termsvalue","default_terms")'));
				echo $this->Form->input('terms',array('id'=>'termsvalue','value','label' => '','value'=>$defaultvalue[3]['DefaultValue']['value'],'readonly'=>'readonly'));
				echo $this->Form->hidden("default_terms",array('id'=>'default_terms','value'=>$defaultvalue[3]['DefaultValue']['value']));
				echo $this->Form->input('terms_url',array('id'=>'terms_url','label' => 'Terms Url','placeholder'=>$defaultvalue[4]['DefaultValue']['value']));
				?>
			</div>			
		</div>
		<div style="float:right;width:45%">
			<div>
				<?php 
				echo "Time";
					echo "<br>";
					echo $this->Form->input('day_status', array('label'=>'All Day','type'=>'select', 'options'=>$optionsAllday)) ;	
				?>
			</div>	
			<div>
				<?php 
					echo $this->Form->input('start_date',array("type"=>"text",'readonly'=>'readonly','autofill'=>'false'));	
				?>
			</div>
			<div>
				<div style ="float:left;">
				<?php 
					echo $this->Form->input('start_hour',array('label'=>'Hour',"options"=>$arrHour,'class'=>'select_hour'));	
				?>
				</div>
				<div style ="float:right;">
				<?php 
					echo $this->Form->input('start_min',array('label'=>'Min',"options"=>$arrMin,'class'=>'select_hour'));
				?>
				</div>
			</div>
			<div>
				<?php 
					echo $this->Form->input('end_date',array("type"=>"text",'readonly'=>'readonly','autofill'=>'false'));	
				?>
			</div>
			<div>
				<div style ="float:left;">
				<?php 
						echo $this->Form->input('end_hour',array('label'=>'Hour',"options"=>$arrHour,'class'=>'select_hour'));
				?>
				</div>
				<div style ="float:right;">
				<?php 
					echo $this->Form->input('end_min',array('label'=>'Min',"options"=>$arrMin,'class'=>'select_hour'));
				?>
				</div>
			</div>
			
			
			
			<div style="clear:both">
				<?php 				
					echo $this->Form->radio('repeat_on', $optionsRepeat,array('legend'=>'Repeat','id'=>'repeat_on','value'=>'D',"class"=>"weekoption"));						
					echo $this->Form->input('repeat_days', array('id'=>'repeat_day','type'=>'select','multiple'=>'checkbox','options'=>$optionsWeekday,'disabled' => "disabled","class"=>"weekdays")) ;
					
				?>	
			</div>	
			<div style="clear:both">
			<?php
					echo "Cover Image";
					echo $this->Form->file('image');
					echo $this->Form->input('tags');
					echo $this->Form->input('is_active');
			?>
			</div>
		</div>																
			<?php
		
	?>
	<div style="float:left"><?php echo $this->Form->submit('Save', array('name'=>'save')); ?></div>
	<div style="float:left;"><?php echo $this->Form->submit('Publish', array('name'=>'publish')); ?>	</div>
	</fieldset>
	
	<?php //echo $this->Form->submit('Preview', array('name'=>'preview')); ?>
<?php echo $this->Form->end(); ?>	
</div>

<script>
  $( function() {
    $( "#VoucherStartDate,#VoucherEndDate" ).datepicker({ altFormat: "yyyy-mm-dd"});
  } );
  
	$(document).ready(function(){
		
		$(document).on("click",".weekoption",function(){
			//console.log($(this).val());
			
			if ($(this).val() == "W") {
				$(".weekdays").each(function(){
					$(this).children("input").removeAttr("disabled");
				});
			} else {
				$(".weekdays").each(function(){
					$(this).children("input").attr("disabled","DISABLED");
				});
			}
		});
		
		
	});
  
  </script>
