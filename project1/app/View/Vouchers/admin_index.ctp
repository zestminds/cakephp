<?php // pr($vouchers); die; ?>
<div class="cont-right">
  <div class="cont-right-innr">
	  <div class="main-hd-in">
          <div class="row">
            <div class="col-sm-12">
              <h2 class="title-das"> Manage Vouchers</h2>
              
              	<div class="show-record">                	
                   <span> Show Records </span> 
                   <?php echo $this->Form->create("voucher1",array("div"=>false,"type"=>"get")); 			
					 echo $this->Form->input('records',array("id"=>"records",'label'=>'',"options"=>$records,'class'=>'form-control',"selected"=>$limit));
					 echo $this->Form->end(); ?>	
                </div>
            </div>
          </div>
        </div>
		<?php echo $this->Form->create("Voucher",array("div"=>false,)); ?>
			<div class="srch" style="margin-left:30px;">
				<?php echo $this->element("admins/common",array("place"=>'Search Voucher',"flag"=>false,"pageheader"=>'',"buttontitle"=>'no',"listflag"=>"no","action"=>'no')); ?>
				
			</div>
        <div class="manage-marchant_vouch">
			  <div class="table-responsive">
				<table class="Marchant-table_vouch table table-bordered merchant_headfont" id="voc_index" width="100%">
				  <tr>
					<th><?php echo $this->Form->input("check",array("label"=>false,"div"=>false,"id"=>'checkall',"type"=>'checkbox')); ?></th>
					<th>Image</th>
<!--
					<th><?php //echo $this->Paginator->sort('Location.title',"Location Name"); ?></th>
-->
					<th><?php echo $this->Paginator->sort('UserDetail.business',"Business Name"); ?></th>	
					<th><?php echo $this->Paginator->sort('title_discount','Discount'); ?></th>
<!--
					<th><?php //echo $this->Paginator->sort('Location.address','Address'); ?></th>					
-->
					<th><?php echo $this->Paginator->sort('start_date'); ?></th>
					<th><?php echo $this->Paginator->sort('end_date'); ?></th>
					
					<th><?php echo $this->Paginator->sort('voucher_status','Voucher Status'); ?></th>
					<th> action </th>
				  </tr>
				  <?php $i=$sno; foreach ($vouchers as $key=>$voucher): 
				 // pr($voucher);
				  ?>
				  <tr>
					<td><?php echo $this->Form->input("id.".$voucher['Voucher']['id'],array("class"=>'chk',"value"=>$voucher['Voucher']['id'],"type"=>'checkbox',"div"=>false,"label"=>false)); ?>
				<?php echo $this->Form->input("voucher_status.".$voucher['Voucher']['id'],array("type"=>'hidden',"value"=>($voucher['Voucher']['voucher_status'] == 1?0:1))); ?></td>
					<td><?php echo ($voucher['Voucher']['image'] == "" || !file_exists(WWW_ROOT."img/voucher/".$voucher['Voucher']['image'])) ? '<img src="'.SITE_LINK.'img/default_voucher.jpeg" width="75" height="75">':' <img src="'.SITE_LINK.'img/voucher/'.h($voucher['Voucher']['image']).'" width="75" height="75">';?></td>
<!--
					<td><a href="<?php //echo SITE_LINK."edit_business?bid=".$voucher['Location']['id']; ?>"><?php //echo  h($voucher['Location']['title']); ?></a></td>
-->
					<td><a href="<?php echo SITE_LINK."view_business/".$voucher['UserDetail']['user_id']; ?>"><?php echo h($voucher['UserDetail']['business']); ?></a></td>

					<td><?php echo h($voucher['Voucher']['title_discount']); ?></td>
<!--
					<td><?php //echo h($voucher['Location']['address']); ?></td>
-->
					<td><?php $startdate =  date_create(h($voucher['Voucher']['start_date'])); echo date_format($startdate,"j M, Y"); ?></td>
					<td><?php $enddate =  date_create(h($voucher['Voucher']['end_date'])); echo date_format($enddate,"j M, Y"); ?></td>
					<td><?php echo h(($voucher['Voucher']['voucher_status'] == 1)?'Published':'Drafted');?></td>
					<td><a href="<?php echo SITE_LINK.'edit-voucher-detail/'.$voucher['Voucher']['id']?>"><i class="fa fa-pencil"></i></a>	
						<?php echo $this->html->link($this->Html->tag('i', '', array('class' => 'glyphicon glyphicon-trash')). " ",   array('action' => 'delete', $voucher['Voucher']['id']),array('escape'=>false),__('Are you sure you want to delete voucher?', ''));?>
					</td>
				  </tr>
				  <?php $i++; endforeach; ?>
				</table>
			</div>
			  
			<div class="pagination-main">
				<div class="paging pagination">
				<?php echo $this->Paginator->prev('<i class="fa fa-caret-left"></i>', array('escape' => false), null, array('class' => 'fa prev disabled'));
				
				echo $this->Paginator->numbers(array('separator' => ''));
				echo $this->Paginator->next('<i class="fa fa-caret-right "></i>', array('escape' => false), null, array('class'	=> 'fa next disabled'));
				?>
			   </div>  
			</div>         
        </div>
		<?php echo $this->Form->end(); ?>
	</div>
</div>
<script>
$(document).ready(function () {
	$("#records").change(function(e) { 
			var val=$('#records').val();
			window.location.replace(SITE_LINK+"view-vouchers/?records="+val);	
			//$("#voucherAdminIndexForm").submit();
				  	  
	});
});	
</script>
