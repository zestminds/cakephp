<link rel="stylesheet" type="text/css" href="<?php SITE_LINK?>moola/css/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="<?php SITE_LINK?>moola/css/font-awesome.min.css" />
<link rel="stylesheet" type="text/css" href="<?php SITE_LINK?>moola/css/style.css" />
<link rel="stylesheet" type="text/css" href="<?php SITE_LINK?>moola/css/responsive.css" />
<div class="vouch-details">	
	<div class="vouch-img">
		<?php echo ($voucher['Voucher']['image']=="" || !file_exists(WWW_ROOT."img/voucher/".$voucher['Voucher']['image']))?($this->Html->image("default_voucher.jpeg")):($this->Html->image("voucher/".h($voucher['Voucher']['image']),array('width'=>'550','height'=>'300'))); ?>

	
	</div>
	<div class="vouch-desc">
	  <h3><?php echo h($voucher['Voucher']['title_discount']); ?> Student Discount</h3>
	  <ul>
		  <li>
			  <h5><span><?php echo $this->Html->image("icon-1.png"); ?></span> About this discount</h5>
			  <p><?php echo h($voucher['Voucher']['description']); ?></p>
		  </li>
		  <li>
			  <h5><span><?php echo $this->Html->image("icon-2.png"); ?></span> Terms & Conditions</h5>
			  <p><?php echo h($voucher['Voucher']['terms']); ?></p>
		  </li>
		  <li>
			  <h5><span><?php echo $this->Html->image("icon-3.png"); ?></span> Time</h5>
			  <ul>
				  <li>Start : <?php echo h($voucher['Voucher']['start_date']); ?><?php echo ($voucher['Voucher']['day_status']==0 && $voucher['Voucher']['start_hour']!="")? " at ".h($voucher['Voucher']['start_hour']):""; ?></li>
				  <li>End :  <?php echo h($voucher['Voucher']['end_date']); ?><?php echo (h($voucher['Voucher']['day_status']==0 && $voucher['Voucher']['end_hour']!=""))? " at ".h($voucher['Voucher']['end_hour']):""; ?></li>
				  <li>Repeat:  <?php echo ($voucher['Voucher']['repeat_on']=='W')? 'Weekly' :'Daily'; ?>
					  <ul>
						<?php if($voucher['Voucher']['repeat_on']=='W') { 		
						 foreach($dayIntArr as $key=>$val) { ?>
							<li class="fa-li fa fa-square"><?php echo $dayArr[$val]; ?></li>
						  <?php 
								 } 
							}
						 ?>
					  </ul>
				  </li>
			  </ul>
		  </li>
		  
		  <li>
			  <h5><span><?php echo $this->Html->image("icon-4.png"); ?></span> Hashtags</h5>
			  <p><?php $temp=array();(h($voucher['Voucher']['tags'])=="")? '': $temp =explode(",",h($voucher['Voucher']['tags'])); foreach($temp as $key=>$val){echo "#".$val." ";}?></p>
			  		  </li>
	  </ul>
	</div>
</div>

