<script language="JavaScript">
<!--

function enable_text(status,value,textfield)
{	
	if(status=="D")
	{		
		$( "#"+value ).prop( "readonly", true ); 
		var input = $( "#"+textfield).val();	
        $( "#"+value).val(input);
	}
	else
	{		
		$( "#"+value).prop( "readonly", false ); 
		
	}	
}
$(document).ready(function () {	
		$('#VoucherEditForm').validate({ // initialize the plugin
			rules: {			
				 
				  "data[Voucher][title]": {
					required: true,				
				  },
				  "data[Voucher][title_discount]": {
					required: true				
				  },
				  "data[Voucher][sub_title]": {
					required: true				
				  },
				  "data[Voucher][description]": {
					required: true				
				  },
				  "data[Voucher][terms]": {
					required: true				
				  },
				  "data[Voucher][start_date]": {
					required: true				
				  },
				  "data[Voucher][end_date]": {
					required: true				
				  },
				  "data[Voucher][tags]": {
					required: true				
				  }	 ,
				  "data[Voucher][repeat_on]": {				  
					  req_question: true
				  }
				},
				// Specify validation error messages
				messages: {			
					"data[Voucher][title]": {
						required: "Please enter Title"			
					},			 
				   "data[Voucher][title_discount]": {
						required: "Please enter Discount"			
					},							
					"data[Voucher][sub_title]": {
						required: "Please enter Sub Title"			
					},			 
				   "data[Voucher][description]": {
						required: "Please enter Description"			
					},							
					"data[Voucher][terms]": {
						required: "Please enter Terms"			
					},			 
				   "data[Voucher][start_date]": {
						required: "Please select Start date"			
					},							
					"data[Voucher][end_date]": {
						required: "Please select End date"			
					},						
					"data[Voucher][tags]": {
						required: "Please enter Tags"			
					},
				  "data[Voucher][repeat_on]": {				  
					  req_question: "Please select atleast one day of week."
				  }
					
				},
				// Make sure the form is submitted to the destination defined
				// in the "action" attribute of the form when valid
				submitHandler: function(form) {
					
				  form.submit();
				}
			  });		
			  
			  
			  
			  $.validator.addMethod('req_question', function(value, elem) {
					  var radioValue = $("input[name='data[Voucher][repeat_on]']:checked").val();						  
						if(radioValue=='W'){
							if ($(".weekdays").children("input:checked").length <= 0) {								
								return false;
							} else {
								return true;
							}
							
						 } else {
							return true;
						}
			  }, 'Please check at least one box in this group.');

	});
</script>
<style>
.upload-btn-wrapper {
  position: relative;
  overflow: hidden;
  display: inline-block;
}

.btnfile {
  border: 2px solid gray;
  color: gray;
  background-color: white;
  padding: 8px 20px;
  border-radius: 8px;
  font-size: 20px;
  font-weight: bold;
}

.upload-btn-wrapper input[type=file] {
  font-size: 100px;
  position: absolute;
  left: 0;
  top: 0;
  opacity: 0;
}
.select_hour{width:183px;}
.weekdays{float:left;}
</style>
<?php //pr($this->request->data); ?>
<div class="cont-right">
   <div class="cont-right-innr">
		<div class="vouchers form">
		<?php echo $this->Form->create('Voucher',array("type"=>"file")); ?>

			<fieldset>
				<div>
				<legend><?php echo __('Edit Voucher'); ?></legend>
				</div>
				<div style="float:left;width:45%">
					
					<div>
						<?php
							 echo $this->Form->hidden('User.id');		
							 echo $this->Form->input('id');
							 echo $this->Form->input('title');
						?>
					</div>
					<div>
						<?php
							 echo $this->Form->radio('title_status',$optionsTitle,array('legend'=>'Discount Value','id'=>'title_status','value'=>($voucher['Voucher']['title_status']== "D")?'D':'C','class'=>"titlebox",'onClick'=>'enable_text(this.value,"discountvalue","default_title")'));	
							echo $this->Form->input('title_discount',array('id'=>'discountvalue','label' => '','value'=>($voucher['Voucher']['title_status']== "D")?$defaultvalue[0]['DefaultValue']['value']:$voucher['Voucher']['title_discount'],'readonly'=>($voucher['Voucher']['title_status']== "D")?'readonly':''));
							echo $this->Form->hidden("default_title",array('id'=>'default_title','value'=>$defaultvalue[0]['DefaultValue']['value']));

						?>
					</div>
					
					<div>
						<?php
							 echo $this->Form->input('sub_title',array('id'=>'sub_title','label' => 'Sub Title','value'=>($voucher['Voucher']['title_status']== "D")?$defaultvalue[1]['DefaultValue']['value']:$voucher['Voucher']['sub_title']));
						?>
					</div>
					<div>
						<?php
							 echo $this->Form->radio('description_status', $optionsDesc,array('legend'=>'Description','id'=>'description_status',"value"=>($voucher['Voucher']['description_status']== "D")?'D':'C','onClick'=>'enable_text(this.value,"descriptionvalue","default_description")'));	
							echo $this->Form->input('description',array('id'=>'descriptionvalue','label' => '','value'=>($voucher['Voucher']['title_status']== "D")?$defaultvalue[2]['DefaultValue']['value']:$voucher['Voucher']['description'],'readonly'=>($voucher['Voucher']['description_status']== "D")?'readonly':''));
							echo $this->Form->hidden("default_description",array('id'=>'default_description','value'=>$defaultvalue[2]['DefaultValue']['value']));	
						?>
					</div>
					<div>
						<?php
							echo $this->Form->radio('terms_status', $optionsTerms,array('legend' => 'Terms and Conditions','id'=>'terms_status',"value"=>($voucher['Voucher']['terms_status']== "D")?'D':'C','onClick'=>'enable_text(this.value,"termsvalue","default_terms")'));
							echo $this->Form->input('terms',array('id'=>'termsvalue','value','label' => '','value'=>($voucher['Voucher']['title_status']== "D")?$defaultvalue[3]['DefaultValue']['value']:$voucher['Voucher']['terms'],'readonly'=>($voucher['Voucher']['terms_status']== "D")?'readonly':''));
							echo $this->Form->hidden("default_terms",array('id'=>'default_terms','value'=>$defaultvalue[3]['DefaultValue']['value']));
							echo $this->Form->input('terms_url',array('id'=>'terms_url','label' => 'Terms Url','value'=>($voucher['Voucher']['title_status']== "D")?$defaultvalue[4]['DefaultValue']['value']:$voucher['Voucher']['terms_url']));
						?>
					</div>
				</div>
				<div style="float:right;width:45%">	
					<div>
						<?php					
							echo "Time";
							echo "<br>";		
							echo $this->Form->input('day_status', array('label'=>'All Day','type'=>'select', 'options'=>$optionsAllday)) ;
						?>
					</div>
					<div>
						<?php					
							echo $this->Form->input('start_date',array('type'=>'text','readonly'=>'readonly','autofill'=>'false','value'=>$voucher['Voucher']['start_date']));
						?>
					</div>
					<div>
						<div style ="float:left;">
						<?php					
							echo $this->Form->input('start_hour',array('label'=>'Hour',$voucher['Voucher']['start_hour'],"options"=>$arrHour,'class'=>'select_hour'));
						?>
						</div>
						<div style ="float:left;">	
						<?php
							echo $this->Form->input('start_min',array('label'=>'Min',"options"=>$arrMin,'class'=>'select_hour'));	
						?>
						</div>
					</div>
					<div>
						<?php					
							echo $this->Form->input('end_date',array("type"=>"text",'value'=>$voucher['Voucher']['end_date'],'readonly'=>'readonly','autofill'=>'false'));
						?>
					</div>
					<div>
						<div style ="float:left;">				
						<?php					
							echo $this->Form->input('end_hour',array('label'=>'Hour',"options"=>$arrHour,'class'=>'select_hour'));
						?>
						</div>
						<div style ="float:left;">	
						<?php
							echo $this->Form->input('end_min',array('label'=>'Min',"options"=>$arrMin,'class'=>'select_hour'));
						?>
						</div>
					</div>
					<div>
						<?php	
							($voucher['Voucher']['repeat_on']=="D")? $repeat_dis="'disabled'=>&quotdisabled&quot" : $repeat_dis='';
							//echo $repeat_dis;
							echo $this->Form->radio('repeat_on', $optionsRepeat,array('legend'=>'Repeat',"class"=>"weekoption"));			
							echo $this->Form->input('repeat_days', array('label'=>'Week Days','type'=>'select', 'multiple'=>'checkbox',"value"=>$this->request->data['Voucher']['repeat_days'],'options'=>$optionsWeekday,'selected' => $voucher['Voucher']['repeat_days'], $repeat_dis,"class"=>"weekdays")) ;
						?>
					</div>
					<div  style="clear:both">
						<div class="upload-btn-wrapper">
							<button class="btnfile">Update profile Pic</button>
							<?php echo $this->Form->file('image');?>		
							</div>
							<?php
							echo "<br><br>";
							echo '<img src="'.SITE_LINK.'img/voucher/'.trim($voucher['Voucher']['image']).'" height=75px width=75px>';
							echo $this->Form->hidden('image_old',array('value'=>$voucher['Voucher']['image']));					
							?>
						</div>	
					
				<?php
				
				
				echo $this->Form->input('tags');
				echo $this->Form->input('is_active');
				?>
			</div>
			</fieldset>
			<div style="float:left">
			<?php echo $this->Form->submit('Save', array('name'=>'save')); ?>
			</div>
			<div style="float:left">
			<?php echo $this->Form->submit('Publish', array('name'=>'publish')); ?>
			</div>
			
		<?php echo $this->Form->end(); ?>
		</div>
 </div>		
</div>
<script>
  $( function() {
    $( "#VoucherStartDate,#VoucherEndDate" ).datepicker({ altFormat: "yyyy-mm-dd"});
  } );
  
  $(document).ready(function(){
		
		$(document).on("click",".weekoption",function(){
			
			//console.log($(this).val());
			if ($(this).val() == "W") {
				$(".weekdays").each(function(){
					$(this).children("input").removeAttr("disabled");
				});
			} else {
				$(".weekdays").each(function(){
					$(this).children("input").attr("disabled","DISABLED");
				});
			}
		});
		
		
	});
  </script>
