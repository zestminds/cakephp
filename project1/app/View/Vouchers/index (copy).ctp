<style>
ul.list{float:left;width:100%;list-style:none;}
ul.list li{float:left;width:100%;}
ul.list li  img{width:250px;height:200px}
.links{margin-top:15px;}
.links a{margin-top:7px;padding:5px;color:#444444;border:1px solid #000;text-decoration:none;}
.links a:hover{background:#444;color:#fff;text-decoration:none;}
</style>

<div class="vouchers index">
	<div class="rhs_actions right">
			<?php echo ( $status==0	)?'<a href="'.SITE_LINK.'publish-vouchers/1">Publish</a>':'<a href="'.SITE_LINK.'draft-vouchers/0">Draft</a>'?>		
		</div>
	<fieldset>
		<legend><?php ;echo ($status==1)?'Published Vouchers List':'Draft Vouchers List'; ?></legend>
	
	<ul class="list">	
	<?php foreach ($vouchers as $voucher){ ?>
		<li>
			<div style="float:left;width:50%">
				<?php echo "<img src='".SITE_LINK."img/voucher/".h($voucher['Voucher']['image'])."'>"; ?>
			</div>
			<div style="float:left">
				<div style="font-size:20px;"><?php echo h($voucher['Voucher']['title']); ?></div>
				<div class="links">
					<a href="<?php echo SITE_LINK?>edit-voucher/<?php  echo $voucher['Voucher']['id'] ?>">Edit</a>
					<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $voucher['Voucher']['id'],$status), array('confirm' => __('Are you sure you want to delete # %s?', $voucher['Voucher']['title']))); ?>
					<?php echo $this->Form->postLink(__('Copy'), array('action' => 'copy_voucher', $voucher['Voucher']['id'],$status)); ?>
					<br>	<br><br>				
					<a href="<?php echo SITE_LINK?>view-voucher/<?php  echo $voucher['Voucher']['id'] ?>">View</a>
					<a href="<?php echo SITE_LINK?>perview-voucher/<?php  echo $voucher['Voucher']['id'] ?>">Perview</a>
					<?php echo  ( $status==0)?$this->Form->postLink(__('Publish'), array('action' => 'publish_voucher', $voucher['Voucher']['id'],$status), array('confirm' => __('Are you sure you want to Publish # %s?', $voucher['Voucher']['title']))):'' ?>
				</div>
			</div>	
		</li>
	<?php } ?>
	</ul>
	</fieldset>
	<?php	
	
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div> 
</div>
