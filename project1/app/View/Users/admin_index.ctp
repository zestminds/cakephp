 <div class="cont-right">
      <div class="cont-right-innr">
		 <div class="main-hd-in">
			  <div class="row">
				<div class="col-sm-12">
				  <h2 class="title-das"><?php echo (($user_type==2)?'Manage Merchants':'Manage Students') ?></h2>
				  
					<div class="show-record">						
					   <span> Show Records </span> 
					   <?php echo $this->Form->create("UserDetail",array("div"=>false,"type"=>"post"));				
						echo $this->Form->input('records',array("id"=>"records",'label'=>'',"options"=>$records,'class'=>'form-control',"selected"=>$limit));
						echo $this->Form->end(); ?>
					</div>
				</div>
			  </div>
			</div>
			<?php echo $this->Form->create("User",array("div"=>false,)); ?>
			<div class="srch" style="margin-left:30px;">
				<?php echo $this->element("admins/common",array("place"=>'Search by merchant name or email ',"flag"=>false,"pageheader"=>'',"buttontitle"=>'no',"listflag"=>"no","action"=>'no')); ?>
				
			</div>
			<div class="manage-marchant">
			  <div class="table-responsive">
				<table class="Marchant-table table table-bordered " width="100%">
				  <tr>		
					<th><?php echo $this->Form->input("check",array("label"=>false,"div"=>false,"id"=>'checkall',"type"=>'checkbox')); ?></th>
					<!--th> Sr no </th-->
					<th> <?php echo $this->Paginator->sort('id','ID'); ?> </th>
					<?php if ($user_type==2) {?>
					<th><?php echo "Logo"; ?></th>
					<?php } ?>
					<?php if ($user_type==2) {?>
					<th><?php echo $this->Paginator->sort('UserDetail.business',"Merchant Name"); ?></th>
					<?php }else {?>
					<th><?php echo $this->Paginator->sort('UserDetail.name','Student Name'); ?></th>
					<?php }?>					
					<th> <?php echo $this->Paginator->sort('created','registration date'); ?> </th>
					<?php if ($user_type == 3) {?>
					<th><?php echo $this->Paginator->sort('Verified'); ?></th>
					<th><?php echo $this->Paginator->sort('University.title','University'); ?></th>
					<?php }?>
					<?php if ($user_type == 2) {?>					
					<th>View Locations</th>
					<?php }?>
					<th> <?php echo $this->Paginator->sort('is_active',"STATUS"); ?> </th>
					<th> ACTION </th>
				  </tr>
				  <?php foreach ($users as $user): ?>
				  <tr>
					<td><?php echo $this->Form->input("id.".$user['User']['id'],array("class"=>'chk',"value"=>$user['User']['id'],"type"=>'checkbox',"div"=>false,"label"=>false)); ?>
				<?php echo $this->Form->input("status.".$user['User']['id'],array("type"=>'hidden',"value"=>($user['User']['is_active'] == 1?0:1))); ?></td>
					<!--td><?php //echo $sno;?></td-->
					<td><?php echo h($user['User']['id']); ?></td>
					<?php if ($user_type == 2) {?>
					<td><?php echo $this->Html->image("profile/".$user['UserDetail']['image'],array("style"=>"height:70px;width:100%;")); ?>&nbsp;</td>
					<?php } ?>
					<?php if ($user_type == 2) {?>
					<td><a href="<?php echo SITE_LINK."view-vouchers?merchant=".$user['UserDetail']["user_id"]; ?>" title="Click to view vouchers of <?php echo h($user['UserDetail']['business']); ?>" ><?php echo h($user['UserDetail']['business']); ?>&nbsp;</a></td>
					<?php } else { ?>					
						<td><?php echo h($user['UserDetail']['name']); ?>&nbsp;</td>
					<?php } ?>									
					<td><?php $joined =  date_create(h($user['User']['created'])); echo date_format($joined,"j M, Y");  ?>&nbsp;</td>
					<?php if ($user_type == 3) {?>
					<td><?php echo h(($user['User']['is_active'] == 1)?'Yes':'No')?>&nbsp;</td>
					<td><?php echo h($user['University']['title']); ?>&nbsp;</td>
					<?php }?>
				
					<?php if ($user_type == 2) {?>					
					<td><a href="<?php echo SITE_LINK."view_business?uid=".$user['User']['id']?>">View Locations</a></td>
					<?php }?>
					<td><?php echo h(($user['User']['is_active'] == 1)?'Active':'Inactive'); ?>&nbsp;</td>	
					<?php if ($user_type == 2) $action='merchants/'; else $action='students/';?>
					<td><a href="<?php echo SITE_LINK.'edit-'.$action.$user['User']['id']?>"><i class="fa fa-pencil"></i></a>	
					<?php echo $this->html->link($this->Html->tag('i', '', array('class' => 'glyphicon glyphicon-trash')). " ",   array('action' => 'delete', $user['User']['id'],$user['User']['user_type_id']),array('escape'=>false),__('Are you sure you want to delete # %s?', $user['UserDetail']['name']));?>
					</td>
				  </tr>
				  <?php $sno++;endforeach; ?>
				</table>
			  </div>		 
			     
			 <div class="pagination-main">
				<div class="paging pagination">
					<?php echo $this->Paginator->prev('<i class="fa fa-caret-left"></i>', array('escape' => false), null, array('class' => 'fa prev disabled'));
					
					echo $this->Paginator->numbers(array('separator' => ''));
					echo $this->Paginator->next('<i class="fa fa-caret-right "></i>', array('escape' => false), null, array('class'	=> 'fa next disabled'));
					?>
				</div>  
			</div> 
        </div>
		<?php echo $this->Form->end(); ?>
	</div>
</div>
<script>
$(document).ready(function () {
	$("#records").change(function(e) { 
	var val=$('#records').val();
	if(window.location.href.indexOf("manage-students") > -1) {
		window.location.replace(SITE_LINK+"manage-students?records="+val);	
	}else{
		window.location.replace(SITE_LINK+"manage-merchants?records="+val);	
	}				
			
	});
});	

</script>

