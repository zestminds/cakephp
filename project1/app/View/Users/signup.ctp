<script>
	$(document).ready(function () {

    $('#UserSignupForm').validate({ // initialize the plugin
        rules: {
			
			  "data[UserDetail][name]": "required",			  
			  "data[User][username]": {
				required: true,
				email: true
			  },
			 "data[User][password]": {
				required: true,
				minlength: 6,
				maxlength: 20
			  },
			
			"data[User][confirm_password]": {
				required: true,				
				equalTo: '#UserPassword'
			  }
			},
			// Specify validation error messages
			messages: {
			  "data[UserDetail][name]": "Please enter your name",			 
			  "data[User][password]": {
				required: "Please provide a password",
				minlength: "Your password must be at least 6 characters long"
			  },
			  "data[User][confirm_password]": {
				required: "Please provide confirm password"				
			  },
			  "data[User][username]": {
				required: "Please enter email address",	
				email: "Please enter a valid email address",			
			  }
			},
			// Make sure the form is submitted to the destination defined
			// in the "action" attribute of the form when valid
			submitHandler: function(form) {
			  form.submit();
			}
		  });
		  
		  
		  
		  
		  jQuery.validator.addMethod( 'passwordMatch', function(value, element) {
    
			// The two password inputs
			var password = $("data[User][password]").val();
			var confirmPassword = $("data[User][confirm_password]").val();

			// Check for equality with the password inputs
			if (password != confirmPassword ) {
				return false;
			} else {
				return true;
			}

		}, "Your Passwords Must Match");

		  
		  
	
});
</script>

<section class="main-page inner-pageWhite-bg">
	<div class="container">
		<div class="container" style="position:relative;left:30%;width:700px">
		<section class="form-outer">
			 <div class="facebook_login">
				<a href="javascript:void(0);" onclick="fblogin()" ></a>                    
			</div>
			<div style="position:relative;left:33%;margin-top:20px">OR</div>
			<?php echo $this->Form->create("User", array("novalidate"=>true,"type"=>"file")); ?>
			<div class="form-section">
				<?php //echo $step; ?>
				<h1 class="heading"><?php echo __("Sign Up");?></h1>

				<div class="form-group">
				<label><?php echo __("Name");?></label>
				<?php echo $this->Form->input("UserDetail.name",array("type"=>"text","maxlength"=>100,"class"=>"form-control","div"=>false,"label"=>false)); ?>
				</div>				
				<div class="form-group">
				<label><?php echo __("Email");?></label>
				<?php echo $this->Form->input("User.username",array("type"=>"text","maxlength"=>100,"class"=>"form-control","div"=>false,"label"=>false)); ?>
				</div>
				<div class="form-group">
				<label><?php echo __("Password");?></label>
				<?php echo $this->Form->input("User.password",array("type"=>"password","maxlength"=>100,"class"=>"form-control","div"=>false,"label"=>false)); ?>
				</div>
				<div class="form-group">
				<label><?php echo __("Confirm Password");?></label>
				<?php echo $this->Form->input("User.confirm_password",array("type"=>"password","maxlength"=>100,"class"=>"form-control","div"=>false,"label"=>false)); ?>
				</div>
				<button type="submit" class="btn btn-large" id="sub1"><?php echo __("continue");?>  &nbsp; <i class="fa fa-arrow-right" aria-hidden="true"></i></button>
				<a href="login" class="btn cancel-btn">Cancel</a>
				<div class="text-center border-top">
                     <div class="form-group clearfix"></div>

				</div>
			</div>
		</section>
		</div>
	</div>
</section>






