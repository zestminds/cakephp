<script>
	$(document).ready(function () {
    $('#UserEditForm').validate({ // initialize the plugin
        rules: {			
			 
			  "data[UserDetail][name]": {
				required: true,				
			  },
			  "data[UserDetail][address]": {
				required: true				
			  }	
			  ,
			  "data[UserCategory][category_id]": {
				required: true				
			  }		
			},
			// Specify validation error messages
			messages: {			
				"data[UserDetail][name]": {
					required: "Please enter Name"			
				},			 
			  "data[UserDetail][address]": {
					required: "Please enter Address"			
				}
				,			 
			  "data[UserCategory][category_id]": {
					required: "Please select Category"			
				}
			},
			// Make sure the form is submitted to the destination defined
			// in the "action" attribute of the form when valid
			submitHandler: function(form) {
			  form.submit();
			}
		  });		
});
</script>
<style>
.upload-btn-wrapper {
  position: relative;
  overflow: hidden;
  display: inline-block;
}

.btnfile {
  border: 2px solid gray;
  color: gray;
  background-color: white;
  padding: 8px 20px;
  border-radius: 8px;
  font-size: 20px;
  font-weight: bold;
}

.upload-btn-wrapper input[type=file] {
  font-size: 100px;
  position: absolute;
  left: 0;
  top: 0;
  opacity: 0;
}
</style>
<div class="users form">
<?php echo $this->Form->create('User',array("type"=>"file")); ?>
	<fieldset>
		<legend><?php echo __('General Information');?></legend>
		<?php
		echo $this->Form->input('id');			
		?>
		<div style="float:left">
		<?php echo ($user['UserDetail']['image'] == "") ? '<img src="'.SITE_LINK.'img/profile/default.png">':' <img src="'.SITE_LINK.'img/profile/'.$user['UserDetail']['image'].'">';?></div>
			<div class="upload-btn-wrapper">
			<button class="btnfile">Update profile Pic</button>
				<?php echo $this->Form->file('UserDetail.image',array('width'=>10));?>		
			</div>
		<?php
		
		echo $this->Form->hidden('image_old',array('value'=>$user['UserDetail']['image']));
		echo "<br><br><b>";
		echo $user['UserDetail']['name'];
		echo "</b><br><br><b>";
		echo $user['User']['username'];
		echo "</b><br><br><b>";
		echo "Joined: "; $joined =  date_create(h($user['User']['created'])); echo date_format($joined,"jS M, Y"); echo "<br>";
		echo "</b><br><br>";
		?>
		<div style="clear:both">
		<?php
		echo $this->Form->input('UserDetail.name');
		echo $this->Form->hidden('UserDetail.id');
		echo $this->Form->input('username',array('label'=>'Email Address',array('disabled' => 'disabled')));
		
		echo $this->Form->input('UserCategory.category_id',array("options"=>$categories,"empty"=>"select a category"));	
		echo $this->Form->hidden('UserCategory.id');
		
		echo $this->Form->input('UserDetail.address',array('label'=>'Primary Address'));
		if(empty($user['User']['identifier'])){?>
		<a href="<?php echo SITE_LINK?>change-password">Change Password</a>
		<?php }?>
	</fieldset>
<?php echo $this->Form->end(__('Save Changes','Save Changes')); ?>
<?php echo $this->Html->link('cancel', array('controller' => 'users', 'action' => 'merchant'),array('class'=>'btn cancel-btn',"title"=>"Cancel"));  ?>

</div>
