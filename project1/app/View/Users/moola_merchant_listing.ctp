<style>
span.result{width: 48%!important}
.more_link {color:#444 !important;text-decoration: underline;}
.more_link a{color:#444}
.more_link a:hover{color:#444 !important}
</style>
<section class="inner-banner topheader">
   <div class="container">
		<div class="banner-cont">
		   <h2>Merchants</h2>  
		</div>
   </div>
</section>
<!--/. banner sec -->

<section class="create-vouch-desc">
	<div class="container">
		<div class="row"> 	
			<div class="col-sm-9">
				<?php echo $this->Form->create("merchant",array("div"=>false,"type"=>"get")); ?>
				<div >
					<div class="srch" style="width:46%;float:left">			
					<?php //echo $this->element("admins/common",array("place"=>'Search by name ',"flag"=>false,"pageheader"=>'',"buttontitle"=>'no',"listflag"=>"no","action"=>'no')); ?>
					<?php echo $this->Form->input('search',array("class"=>"form-control","type"=>"text","placeholder"=>"Search by business name","maxlength"=>200,"label"=>false,"autocomplete"=>"off","value"=>''));?>
					</div>
					<div style="width:50%;float:right">
					<?php echo $this->Form->input('category',array("class"=>"form-control","type"=>"text","placeholder"=>"select a category","maxlength"=>200,"label"=>false,"autocomplete"=>"off","value"=>''));?>
					<span class="result">
					<ul id="inresult">

					<ul>
					</span>
					</div>
				</div>	
			 </div>	
			 <div class="col-sm-9">	
					<div class="srch" style="width:47%;margin-top:20px;float:left">
						<div id="pac-container"><?php echo $this->Form->input('address',array("id"=>"pac-input","class"=>"form-control",'label'=>false,"placeholder"=>"123, Manchester, United Kingdom"));?></div>			
						<?php echo $this->Form->hidden('lat');?>
						<?php echo $this->Form->hidden('lng');?>
						<div class="map-location" style="display:none">
							 <div class="map-frame">																			
								 <div id="map" style="height:490px;width:1170px;"></div>
								 <div id="infowindow-content">
								  <img src="" width="16" height="16" id="place-icon">
								  <span id="place-name"  class="title"></span><br>
								  <span id="place-address"></span>
								 </div>								
								 
							 </div>
						</div>
					</div>	
			 </div>
			 <div class="col-sm-9">
				<div style="clear:both;margin-top:20px;margin-bottom:20px;"><div style="float:left;margin-right:20px;"><?php echo $this->Form->submit('Search',array("class"=>"btn btn-theme")); ?></div><div><?php echo $this->Form->submit('Clear Search',array("name"=>"Clear","class"=>"btn btn-theme")); ?></div></div>
			</div>
			<div class="col-sm-12">	
				
				 <div class="tab-content tabs-body">
					<div class="tab-pane active" id="draft">
						<div class="voucher-boxCont draft">
							<?php foreach ($users as $user) {?>						
							<div class="box-card">
								<div class="box-info">
									<a href="javascript:void(0);">
										<div class="card-img">
											<?php echo $this->Html->image("profile/".$user['UserDetail']['image']); ?>	
										</div>
									</a>
									<div class="cdard-overlay">
										<?php echo h($user['UserDetail']['business']); ?><br/>
										<?php echo h($user['UserDetail']['address']); ?><br/>		
										Category: <?php echo h($user['Category']['title']); ?><br/>
										<?php $business=strtolower(str_replace(' ','-',$user['UserDetail']['business']));?>
										<a href="<?php SITE_LINK?>m/<?php echo $user['User']['id'];?>-<?php echo $business; ?>" class="more_link">View More</a>
									</div>
								</div>
							</div> 
							<?php } ?>
							<!--/. End box-card -->
						</div> <!--/.voucher-boxCont -->                    
					</div>
					<!--/. End tab-panel -->  
							
				<?php echo $this->Form->end(); ?>
			</div>	
		</div>
	</div>

</section>	
<script>
$(document).ready(function () {
	$("#merchantCategory").on("focus",function(){		
			getresults($(this).val().trim());
			
	});
	
	$("#merchantCategory").on("keyup",function(){		
			getresults($(this).val().trim());		
	});
	$("#merchantCategory").on("blur",function(){
		setTimeout(function(){ $("#inresult").html(""); $(".result").hide(); },1000);		
	});
	
	var listCategories = "<li id='{ID}' class='selcat'>{VALUE}</li>";
	var getresults = function(keyword){
		$("#inresult").html("");
		$(".result").hide()
		$.get(SITE_LINK+"users/getcategories?key="+keyword, function(data, status){
			var obj = JSON.parse(data);
			//console.log(obj);
			tmpNew = listCategories;
			$.each(obj,function(i,val){
				tmpNew = listCategories.replace(/{ID}/g,i);
				tmpNew = tmpNew.replace(/{VALUE}/g,val);
				$("#inresult").append(tmpNew);
			});
		});
		$(".result").show();
	}
	$(document).on("click",".selcat",function(){
	//	$("#UserCategoryCategoryIdTmp").val($(this).attr("id"));
		$("#merchantCategory").val($(this).html());
	});
	var page =2;
	var loadFlag = true;
	var tmp = '<div class="box-card"><div class="box-info"><div class="card-img"><img src="{SITE_LINK}/img/profile/{IMG}" alt=""/></div><div class="cdard-overlay">{BUSINESS}<br/>{ADDRESS}<br/>Category: {CATEGORY}<br/><a href="{SITE_LINK}merchant_detail?id={ID}" class="more_link">View More</a></div></div></div> ';

	var flag = true;
	$(window).scroll(function (e) {
		if (flag) {
			if ($(window).scrollTop() + $(window).height() >= $(document).height() - 50) {
				if (loadFlag) {
					var href;					
					href =SITE_LINK+"merchants/page:"+page;							
					$.ajax({
						type : "GET",
						url : href,
						dataType: 'json',
						success: function(data) {	
							
							if ( data.length > 0 ) {
								var tmpNew = tmp;
								$.each(data,function(key,val){									
									tmpNew = tmp.replace(/{ID}/g,val["id"]);
									tmpNew = tmp.replace(/{IMG}/g,val["image"]);	
									tmpNew = tmpNew.replace(/{BUSINESS}/g,val["business"]);
									tmpNew = tmpNew.replace(/{ADDRESS}/g,val["address"]);
									tmpNew = tmpNew.replace(/{CATEGORY}/g,val["category"]);
									tmpNew = tmpNew.replace(/{SITE_LINK}/g,SITE_LINK);
									$(".voucher-boxCont").append(tmpNew);
								});
							} else {
								flag = false;
								loadFlag = false;
							}
						},
						error: function(){
							flag = false;	
						}
					});
					page +=1;
				}
			}
		}
	});	 
	
});	
</script>	
<script>
// This example requires the Places library. Include the libraries=places
      // parameter when you first load the API. For example:
      // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

      function initMap() {
		  		  
        var map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: 32.7498382, lng: 129.86759830000005},
          zoom: 13
        });
        var card = document.getElementById('pac-card');
        var input = document.getElementById('pac-input');
        var types = document.getElementById('type-selector');
        var strictBounds = document.getElementById('strict-bounds-selector');

        map.controls[google.maps.ControlPosition.TOP_RIGHT].push(card);

        var autocomplete = new google.maps.places.Autocomplete(input);

        // Bind the map's bounds (viewport) property to the autocomplete object,
        // so that the autocomplete requests use the current map bounds for the
        // bounds option in the request.
        autocomplete.bindTo('bounds', map);
		
        var infowindow = new google.maps.InfoWindow();
        var infowindowContent = document.getElementById('infowindow-content');
        infowindow.setContent(infowindowContent);
        var marker = new google.maps.Marker({
		 position: new google.maps.LatLng(32.7498382,129.86759830000005), 
          map: map   
        });

          autocomplete.addListener('place_changed', function() {
          infowindow.close();
          marker.setVisible(false);
          var place = autocomplete.getPlace();
          if (!place.geometry) {
            // User entered the name of a Place that was not suggested and
            // pressed the Enter key, or the Place Details request failed.
            //window.alert("No details available for input: '" + place.name + "'");
            return;
          }

          // If the place has a geometry, then present it on a map.
          if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
            var address =place.name;
            var geocoder = new google.maps.Geocoder();
            geocoder.geocode({ 'address': address}, function (results, status) {
                        if (status == google.maps.GeocoderStatus.OK) {
							var latitude = place.geometry.location.lat();
                            var longitude = place.geometry.location.lng();                            
                            $('#merchantLat').val(latitude);
                            $('#merchantLng').val(longitude);
                        } else {
                           
                        }
                    });
            
          } else {
			 
            map.setCenter(place.geometry.location);
            map.setZoom(17);  // Why 17? Because it looks good.
          }
          marker.setPosition(place.geometry.location);
          marker.setVisible(true);

          var address = '';
          if (place.address_components) {
            address = [
              (place.address_components[0] && place.address_components[0].short_name || ''),
              (place.address_components[1] && place.address_components[1].short_name || ''),
              (place.address_components[2] && place.address_components[2].short_name || '')
            ].join(' ');
          }       
          infowindowContent.children['place-icon'].src = place.icon;
          infowindowContent.children['place-name'].textContent = place.name;
          infowindowContent.children['place-address'].textContent = address;
         
          	
			infowindow.open(map, marker);
        }    
        );	        
		       

	}
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDcBcY5ObGmiZ6TETlFS1cEOi5ttvBRnuw&libraries=places&callback=initMap" async defer></script>

 
