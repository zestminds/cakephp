<section class="inner-banner topheader">
   <div class="container">
		<div class="banner-cont">
		   <h2>Change Password</h2>  
		</div>
   </div>
</section>
    <!--/. banner sec -->
<?php echo $this->Form->create('User',array("type"=>"file","class"=>"themeForm")); ?> 				
<?php echo $this->Form->input("id"); ?>
<section class="my-account">
	 <div class="container">
		<div class="account-profile box-shadow">
			<div class="account-detals">	
				<?php //echo $this->Session->flash(); ?>	
				<?php 
					echo $this->Form->create('User',array("id"=>'changepassword',"novalidate"=>true));
				?>
				<div class="row">
					<div class="form-group inputField col-sm-12">			
						<span class="inpIcon"><?php echo $this->Html->image("pwd-icon.png",array("alt"=>"Current Password")); ?></span>
				 		<?php echo $this->Form->input("currentpassword",array("type"=>'password',"id"=>'CurrentPassword','placeholder'=>"Current Password",'value'=>'','class'=>'form-control','div'=>false,'label'=>false)); ?>
						<?php echo $this->Form->hidden("id",array("value"=>$this->Session->read("AuthUser.User.id")));?>
					</div>	
					<div class="form-group inputField col-sm-12">
						<span class="inpIcon"><?php echo $this->Html->image("pwd-icon.png",array("alt"=>"Current Password")); ?></span>
						<?php echo $this->Form->input("newpassword",array("type"=>'password',"id"=>'UserPassword','placeholder'=>"New Password",'value'=>'','class'=>'form-control','div'=>false ,'label'=>false)); ?>
					</div>	

					<div class="form-group inputField col-sm-12">
						<span class="inpIcon"><?php echo $this->Html->image("pwd-icon.png",array("alt"=>"Current Password")); ?></span>
						<?php echo $this->Form->input("confirmpassword",array("type"=>'password',"id"=>'UserRetypePassword','placeholder'=>"Confirm Password",'value'=>'','class'=>'form-control','div'=>false,'label'=>false)); ?>
					</div>						
					<div class="form-submit">
						<?php echo $this->Form->Submit('SUBMIT',array("class"=>"btn btn-theme","title"=>"Submit",'div'=>false)); ?>
						<?php echo $this->Html->link('cancel', array('controller' => 'users', 'action' => 'edit'),array('class'=>'btn btn-theme',"title"=>"Cancel"));  ?>
						<?php echo $this->Form->end();?>	
					</div>
				</div>				
			</div>
	   </div>
	</div>		
</section>


