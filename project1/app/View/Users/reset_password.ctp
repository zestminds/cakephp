<script>
	$(document).ready(function () {

    $('#UserResetPasswordForm').validate({ // initialize the plugin
        rules: {
					  
			 "data[User][password]": {
				required: true,
				minlength: 6,
				maxlength: 20
			  },			
			"data[User][confirm_password]": {
				required: true,				
				passwordMatch:true
			  }
			},
			// Specify validation error messages
			messages: {			 
			  "data[User][password]": {
				required: "Please provide a password",
				minlength: "Your password must be at least 6 characters long"
			  },
			  "data[User][confirm_password]": {
				required: "Please provide confirm password",
				passwordMatch: "Confrim password does not match with password"
			  }			 
			},
			// Make sure the form is submitted to the destination defined
			// in the "action" attribute of the form when valid
			submitHandler: function(form) {
			  form.submit();
			}
		  });
		  
});
</script>
 <section class="main-page inner-pageWhite-bg">
		<div class="container">
			<section class="form-outer">
				<?php echo $this->Form->create("User",array("novalidate"=>true)); ?>
					<div class="form-section">
						<h1 class="heading"><?php echo __("Reset Password?");?></h1>					
						<p class="top-txt"></p>
						<div class="form-group">
							<label><?php echo __("Password");?></label>
							      <?php echo $this->Form->input("password",array("type"=>"password","maxlength"=>200,"class"=>"form-control","div"=>false,"label"=>false)); ?>
							<label><?php echo __("Confirm Password");?></label>    
							       <?php echo $this->Form->input("confirm_password",array("type"=>"password","maxlength"=>200,"class"=>"form-control","div"=>false,"label"=>false)); ?>  
						</div>	
					</div>
						<button type="submit" class="btn btn-large"><?php echo __("Reset");?>  &nbsp; <i class="fa fa-arrow-right" aria-hidden="true"></i></button>
				<?php echo $this->Form->end(); ?>
			</section>
		</div>
  </section>
