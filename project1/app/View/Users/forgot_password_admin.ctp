 <script>
	$(document).ready(function () {
	$('#resetMessage').hide();
    $('#UserForgotPasswordForm').validate({ // initialize the plugin
        rules: {			
			 
			  "data[User][email]": {
				required: true,
				email : true
			  },
			  "forgetpass_hiddenRecaptcha": {
				required: true
			  }					 		
			},
			// Specify validation error messages
			messages: {			
						 
			  "data[User][email]": {
				required: "Please enter a email",
				email : "Please enter valid email"			
			  },
			  "forgetpass_hiddenRecaptcha": {
				required: "Invalid captcha"					
			  }
			},
			// Make sure the form is submitted to the destination defined
			// in the "action" attribute of the form when valid
			submitHandler: function(form) {				
					$('#error_msg_forget').hide();
					$('#success_msg_forget').hide();
					$("#error_msg_captcha_forget").hide();
					
					if ($("#forgetpass_hiddenRecaptcha").val().trim() == "") {
						$('#error_msg_forget').hide();
						$('#success_msg_forget').hide();
						$("#error_msg_captcha_forget").show();
						return false;
					}
			 $('#UserForgotPasswordForm').ajaxSubmit(function(response) { 		
					response = JSON.parse(response);
					if (response.status) {	
						$("#error_msg_forget").hide();	
						$('#success_msg_forget').show();				
						$('#success_msg_forget').text(response.message);
						  window.setTimeout(function(){
							// Move to a new location or you can do something else
							window.location.href = SITE_LINK+"/adminlogin";
						}, 7000);
											 				
					} else {							
						 $("#error_msg_forget").show();				
						 $('#error_msg_forget').text(response.message);	
						 grecaptcha.reset(widgetId1);
						 $('#forgetpass_hiddenRecaptcha').val('');					 						
						return false;
					}
				});
			}
		  });	
});
</script>
 <div class="login-content">
         <div id="resizeMe" class="login-page">
             <div class="container">
                 <div class="login-box">
                     <div class="log-logo">
                            <a href="#"><?php echo $this->Html->image("login-logo.png"); ?></a>
                     </div>				
                     <div class="alert alert-warning" style="display:none" id="error_msg_forget"></div><div style="display:none" class="alert alert-success" id="success_msg_forget"></div>
					<div class="alert alert-danger" id="error_msg_captcha_forget" style="display:none;"><strong>Attention!</strong> Please check captcha to proceed.</div>
					<?php echo $this->Form->create("User",array("url"=>"/forgot_password","id"=>"UserForgotPasswordForm","novalidate"=>"true")); ?>
							<div class="login_fiels">
                          <div class="form_fiels">
                             <label>Email :</label>
                             <?php echo $this->Form->input("email",array("type"=>"text","maxlength"=>100,"class"=>"form-control","div"=>false,"label"=>false)); ?>                             
                          </div>
							<div class="g-recaptcha_forgetpass" data-sitekey=<?php echo SITE_KEY; ?> id="RecaptchaField3"></div>
							<input type="hidden" class="hiddenRecaptcha" name="forgetpass_hiddenRecaptcha" id="forgetpass_hiddenRecaptcha">	
							 <div class="form_sub_field">                          
                             <button class="btn btn-theme btn-block" type="submit">Send Password</button>                             
                          </div>
                        </div>
						<?php echo $this->Form->end(); ?>
                 </div>
             </div>
        </div>
    </div>
<script>
var widgetId1;
var CaptchaCallback = function() {    
    widgetId1 = grecaptcha.render('RecaptchaField3', {'sitekey' : '<?php echo SITE_KEY; ?>', 'callback' : correctCaptcha_forgetpass});   
};
var correctCaptcha_forgetpass = function(response) {
    $("#forgetpass_hiddenRecaptcha").val(response);
};
</script>
