<div class="cont-right">
   <div class="cont-right-innr">
	<div class="users view">
	<h2><?php echo __('Profile: '); echo h($user['UserDetail']['name'])?> </h2>
		<dl>
			<dt><?php echo __('Id'); ?></dt>
			<dd>
				<?php echo h($user['User']['id']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Username'); ?></dt>
			<dd>
				<?php echo h($user['User']['username']); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Name'); ?></dt>
			<dd>
				<?php echo h($user['UserDetail']['name']) ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Address'); ?></dt>
			<dd>
				<?php echo h($user['UserDetail']['address']) ?>
				&nbsp;
			</dd>	
			<?php if($user['User']['user_type_id']==3) {?>
			<dt><?php echo __('University'); ?></dt>
			<dd>
				<?php echo h($user['University']['title'])?>
				&nbsp;
			</dd>	
			<?php }?>		
			<?php if($user['User']['user_type_id']==2) {?>
			<dt><?php echo __('Category'); ?></dt>
			<dd>
				<?php echo h($user['Category']['title'])?>
				&nbsp;
			</dd>	
			<?php }?>	
			<dt><?php echo __('Is Active'); ?></dt>
			<dd>
				<?php echo h(($user['User']['is_active'] == 1)?'Active':'Inactive'); ?>
				&nbsp;
			</dd>
			<dt><?php echo __('Joined'); ?></dt>
			<dd>
				<?php  $joined =  date_create(h($user['User']['created'])); echo date_format($joined,"jS M, Y h:i:s"); ?>
				&nbsp;
			</dd>
			
		</dl>
	</div>
	</div>	
</div>
