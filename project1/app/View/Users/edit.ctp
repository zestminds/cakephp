<script type="text/javascript">
	var avatar = "<?php echo (empty($user['UserDetail']['image']) || !file_exists(WWW_ROOT."/img/profile/".$user['UserDetail']['image'])) ?  SITE_LINK.'/img/default_new.png' : SITE_LINK.'img/profile/'.$user['UserDetail']['image']; ?>";
</script>
<?php
$tmpImage = (empty($user['UserDetail']['image']) || !file_exists(WWW_ROOT."/img/profile/".$user['UserDetail']['image'])) ?  SITE_LINK.'/img/default_new.png' : SITE_LINK.'img/profile/'.$user['UserDetail']['image'];

//echo $tmpImage = file_get_contents($tmpImage); die;
 ?>
<section class="inner-banner topheader">
   <div class="container">
		<div class="banner-cont">
		   <ul class="menu-banner">
				<li><a href="<?php echo SITE_LINK;?>drafted-vouchers" class="<?php echo ($this->params['controller'] == 'vouchers' && $this->params['action'] == 'index' )? 'active':''?>" title="My Vouchers">My Vouchers</a></li>   
				<li><a href="<?php echo SITE_LINK;?>locations" class="<?php echo ($this->params['controller'] == 'Locations' && $this->params['action'] == 'index' )? 'active':''?>" title="My Locations">My Locations</a></li>   
		   </ul>
		</div>
   </div>
</section>		

<section class="my-account-desc">
	 <div class="container">		
		<!-- My account -->	
		<div class="page_heading">	
		 <h1 class="clearfix">My Account </h1>
		 </div>
		<div id="myaccount">
			<div class="account-profile box-shadow">
			<?php echo $this->Form->create('User',array("type"=>"file","class"=>"themeForm")); ?> 
			<?php echo $this->Form->input("id");  echo $this->Form->hidden('addaccount');?>	
			  <div class="my-account-pro clearfix">	
				 <div class="my-account-pro-left">	
					<div class="profile-cont">		 
						<div class="prof-pic user_pic img-upload" style="background-image:url('<?php echo (empty($user['UserDetail']['image']) || !file_exists(WWW_ROOT."/img/profile/".$user['UserDetail']['image'])) ?  SITE_LINK.'/img/default_new.png' : SITE_LINK.'img/profile/'.$user['UserDetail']['image']; ?>');"> 
						<?php echo $this->Form->input("tmpImage",array("label"=>false,"type"=>"textarea","style"=>"display:none;","id"=>"tmpImage")); ?>	
						</div>
						<div class="prof-details">
						   <h4><?php echo $user['UserDetail']['business'];?></h4>
						   <p><i class="icons"><?php echo $this->Html->image("env-icon.png",array("alt"=>"Merchant Email")); ?></i> <span><?php echo $user['User']['username'];?></span></p>
						   <p><i class="icons"><?php echo $this->Html->image("cal-icon.png",array("alt"=>"Joining Date")); ?></i> <span><?php $joined =  date_create(h($user['User']['created'])); echo date_format($joined,"jS M, Y");?></span></p>
						</div>
					</div>
				</div>					
				 <div class="my-account-pro-right">				
					 <?php /*<button type="button" class="btn btn-white" title="Subscribe" id="subscribelist"><?php echo (isset($this->request->data['Newsletter']['id'])?"Unsubscribe":"Subscribe"); ?></button>*/ ?>
					 <?php /*<button type="button" class="btn btn-white confirmaddbusienssaccount" title="Add Location" >Add Location</button>*/ ?>
				</div>
			 </div>

			<div class="account-detals">
					<?php //echo $this->Form->hidden('image_old',array('value'=>$user['UserDetail']['image']));?>
					<div class="row">
					   <div class="form-group inputField col-sm-6">
						  <label class="lable-control"><i class="sm-icons"><?php echo $this->Html->image("sm-icon1.png",array("alt"=>"Full Name","placeholder"=>"Full Name")); ?></i> Merchant Name</label>
						  <?php echo $this->Form->input('UserDetail.business',array('label'=>false,"class"=>"form-control","placeholder"=>"Merchant Name"));?>						
					   </div>
					   <div class="form-group inputField col-sm-6">
						  <label class="lable-control"><i class="sm-icons"><?php echo $this->Html->image("sm-icon2.png",array("alt"=>"Email Address","placeholder"=>"Email Address")); ?></i> Email Address</label>
						  <?php echo $this->Form->hidden('UserDetail.id');echo $this->Form->hidden('UserDetail.latitude'); echo $this->Form->hidden('UserDetail.longitude');
						echo $this->Form->input('username',array('label'=>false,"class"=>"form-control","placeholder"=>"businessname@gmail.com"));?> 						
					   </div>
					</div>
					<div class="row">
					   <div class="form-group inputField col-sm-6">
						 <label class="lable-control"><i class="sm-icons"><?php echo $this->Html->image("facebook-logo.png",array("alt"=>"Facebook")); ?></i> Facebook </label>
						  <?php
						  echo $this->Form->input('UserDetail.facebookid',array("class"=>"form-control","placeholder"=>"https://www.facebook.com/example/","maxlength"=>200,"label"=>false,"autocomplete"=>"off","value"=>$user['UserDetail']['facebookid']));	?>	
					   </div>					  
					   <div class="form-group inputField col-sm-6">
						 		 <label class="lable-control"><i class="sm-icons"><?php echo $this->Html->image("twitter.png",array("alt"=>"Twitter")); ?></i> Twitter </label>
						    <?php echo $this->Form->input('UserDetail.twitterid',array("class"=>"form-control","placeholder"=>"https://twitter.com/example/","maxlength"=>200,"label"=>false,"autocomplete"=>"off","value"=>$user['UserDetail']['twitterid']));	?>		  
					   </div>
					</div>
					<div class="row">					  
					   <div class="form-group inputField col-sm-6">
						  <label class="lable-control"><i class="sm-icons"><?php echo $this->Html->image("instagram.png",array("alt"=>"Instagram")); ?></i> Instagram </label>
						  <?php
						  echo $this->Form->input('UserDetail.instagramid',array("class"=>"form-control","placeholder"=>"https://www.instagram.com/example/","maxlength"=>200,"label"=>false,"autocomplete"=>"off","value"=>$user['UserDetail']['instagramid']));	?>	
					   </div>
					    <div class="form-group inputField col-sm-6">
						  
						</div>	
					</div>	
					<div class="row">
						 <div class="form-group inputField col-sm-12">
						  <label class="lable-control"><i class="sm-icons"><?php echo $this->Html->image("about-us.png",array("alt"=>"About Me")); ?></i> About </label>
						  <?php
						  echo $this->Form->input('UserDetail.aboutme',array("class"=>"form-control","type"=>"text","placeholder"=>"","rows"=>3,"label"=>false,"autocomplete"=>"off","value"=>$user['UserDetail']['aboutme']));	?>					  
					   </div>	
					</div>
					<div class="form-submit form-submit-bottom clearfix">					 
						<div class="pull-left">
							  <button type="button" class="btn btn-white" title="Delete Account" id="deleteuseraccount">Delete Account</button>
						 </div>		
						  <?php if(empty($user['User']['identifier'])){?>
							<a href="<?php echo SITE_LINK?>change-password" class="btn change-pwd">Change Password</a><?php }?>							
							<button type="submit" class="btn btn-theme">SAVE CHANGES</button>
					</div>				
				</div>
				<?php echo $this->Form->end(); ?>
				</div>	
			</div>			
			<!-- End My account -->
	 </div>
</section>
<!--/. My Account -->

<style>
.imageBox{width:520px !important;height:350px !important;}
</style>
<div class="modal fade loginModel themeModel" id="imageModal" tabindex="-1" role="dialog" aria-labelledby="imageModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
       <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title">Select Image</h4>
      </div>
      <div class="modal-body" id="actions">
        <div class="selct-img-voc">
			<div class="img-container" style="width:100%;">
				<img src="<?php echo $tmpImage; ?>" alt="Picture">
			</div>
		  <div class="iviewer" >
			 <ul class="custom-file-upload" class="docs-buttons">
				 <li><label for="upload-file" class="custom-file-upload"><span ><?php echo $this->Html->image("gallery-img.png",array("alt"=>"Voucher Pic")); ?></span></label> <input id="upload-file" type="file" name="file" accept="image/*"/></li>
				<li data-method="rotate" data-option="-90"><a href="javascript:void(0)" title="Rotate Left"><span id="rotateLeft"><?php echo $this->Html->image("rotate-left.png",array("alt"=>"Voucher Pic")); ?></span></a></li>
				 <li data-method="rotate" data-option="90"><a href="javascript:void(0)" title="Rotate Right"><span id="rotateRight"><?php echo $this->Html->image("rotate-right.png",array("alt"=>"Voucher Pic")); ?></span></a></li>
				 <li data-method="zoom" data-option="-0.1"><a href="javascript:void(0)" title="Zoom Out"><span id="zoomIn"><?php echo $this->Html->image("zoom-out.png",array("alt"=>"Voucher Pic")); ?></span></a></li>
				  <li data-method="zoom" data-option="0.1"><a href="javascript:void(0)" title="Zoom In"><span id="zoomOut"><?php echo $this->Html->image("zoom-in.png",array("alt"=>"Voucher Pic")); ?></span></a></li>
			 </ul>             
		  </div>		 
		</div>
        
		<div class="iviewer-btn docs-buttons">
			<ul class="list-inline">
			   <li><button class="btn btn-custom" data-dismiss="modal">CLOSE</button></li> 
			   <li><button class="btn btn-custom btn-primary" data-method="getCroppedCanvas" data-option="{ &quot;maxWidth&quot;: 4096, &quot;maxHeight&quot;: 4096 }" id="saveimage">SAVE CHANGES</button></li>
			</ul>
		</div>
	 </div>  	
    </div>
  </div>
</div>
<style>
.img-container {
  /* Never limit the container height here */
  max-width: 100%;
}

.img-container img {
  /* This is important */
  width: 100%;
}
.cropper-bg {background-image:none !important;}
.cropper-modal {background-color:#EEEEEE !important;opacity:0 !important;}

</style>
<div class="modal in fade themeModel" tabindex="-1" role="dialog" id="addbusienssaccountbox">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">New Location Creation Confirmation</h5>        
      </div>
      <div class="modal-body">
        <p>Are you sure you want to update my account information and want to create a new location with same data.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary btn-theme" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-secondary btn-theme confirmupdatedata">Ok</button>
      </div>
    </div>
  </div>
</div>  
<script>
$(document).ready(function () {	
	 $(".confirmaddbusienssaccount").on("click",function(){
			$("#addbusienssaccountbox").modal("show");
	});
	
	 $(".confirmupdatedata").on("click",function(){
		 $("#addbusienssaccountbox").modal("hide");
		 $("#UserAddaccount").val("addaccount");
		$('#UserEditForm').submit();
	 });
});
</script>
