 <section class="inner-banner topheader">
       <div class="container">
	        <div class="banner-cont">
	           <h2>INSIGHTS</h2> 
	        </div>
       </div>
    </section>
    <!--/. banner sec -->

    <section class="new-vouchers insights">
       <div class="container">
             <div class="audience">
                 <div class="inside-ttl">
                     <h4>Audience </h4>
                     <p>What does this mean?</p>
                 </div>
                 <div class="theme-chart row">
                     <div class="col-sm-6 chartBox">
                        <div class="chart-card" style="height: 373px;">
							<div class="chart_title">Gender</div>
                           <div id="chart_gender"></div>
                           <div class="chart_desc">								

                           </div>
                        </div>
                     </div>
                     <div class="col-sm-6 chartBox">
                        <div class="chart-card">
							<div class="chart_title">Hours  Days</div>
                            <div id="audience_weekly"></div>
                        </div>
                     </div>
                 </div>
             </div> <!--/.Audience -->
             <div class="activity">
                    <div class="inside-ttl">
                         <h4>Activity </h4>
                         <p>What does this mean?</p>
                     </div>
                     <div class="tabbable tabs-header">
                      <div class="btn-group" role="group">
                          <ul class="nav nav-tabs">
							  <?php echo $this->Form->hidden('voucher_status',array('value'=>$voucher_status));?>
                            <li <?php echo ($voucher_status=="Reach")?'class="active"':''?> ><a href="<?php echo SITE_LINK?>insights?type=Reach" >Reach</a></li>
                            <li <?php echo ($voucher_status=="Impression" || $voucher_status=="")?'class="active"':''?>><a href="<?php echo SITE_LINK?>insights?type=Impression">Impressions</a></li>
                            <li <?php echo ($voucher_status=="Convert")?'class="active"':''?>><a href="<?php echo SITE_LINK?>insights?type=Convert">Conversions</a></li>
                          </ul>
                        </div>

                         <div class="filter-activity dropdown">
                            <button type="button" class="btn dropdown-toggle" data-toggle="dropdown"><?php echo $this->Html->image("filter-icon.png",array("alt"=>"user"));?></button>
                            <ul class="dropdown-menu act-filter" role="menu">
                                <li><a href="javascript:void(0);">7 Days</a></li>
                                <li><a href="javascript:void(0);">30 Days</a></li>
                                <li><a href="javascript:void(0);">3 Months</a></li>
                                <li><a href="javascript:void(0);">6 Months</a></li>
                                <li><a href="javascript:void(0);">1Year</a></li>
                            </ul>
                         </div>               
                    </div>
              
                   <div class="tab-content tabs-body">
                        <div class="tab-pane active" id="tab1">
                            <div class="voucher-boxCont draft">
								
								<?php foreach ($voucher_live_status as $voucherdata){ ?>
                                <div class="box-card">
                                    <div class="box-info">
                                        <a href="javascript:void(0);">
                                            <div class="card-img">
												<?php echo ($voucherdata['Voucher']['image']=="")?($this->Html->image("default_voucher.jpeg")):($this->Html->image("voucher/".h($voucherdata['Voucher']['image']))); ?>
                                            </div>
                                            <span class="pst-cont"><?php echo $voucherdata['0']['count']?></span>
                                        </a>
                                    </div>
                                </div> <!--/. End box-card -->        
                                 <?php } ?>                               
                                                      
                            </div> <!--/.voucher-boxCont -->
                        </div>
                        <!--/. End tab-panel -->                                             
                   </div>
             </div>  <!--/.Activity -->
       </div>      
    </section>
    <!--/. My Account -->
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/highcharts-3d.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/series-label.js"></script>



<script>
Highcharts.setOptions({
 colors: ['#f18058', '#5dc3f3','#0e7228','#0c4a87','#785a04','#717171']
});
Highcharts.setOptions({
    colors: Highcharts.map(Highcharts.getOptions().colors, function (color) {
        return {
            radialGradient: {
				r: 0.6,
                cx: 0.1,
                cy: 0.2                
            },
            stops: [
                [0, color],
                [1, Highcharts.Color(color).brighten(-0.1).get('rgb')] // darken
            ]
        };
    })
});
Highcharts.chart('chart_gender', {
    chart: {
        type: 'pie',
        height: 300,
        width: 350,
        options3d: {
            enabled: true,
            alpha: 10
        }
    },
    title: {
        text: ''
    },
    plotOptions: {
        pie: {
            innerSize: 100,
            depth: 80,
           
            dataLabels: {
                enabled: true,
            }
        }
    },
        tooltip: {
            formatter: function(){
                var point = this.point;
                console.log(point);
                return '<b>' + point.name + ': ' + point.y + '%';
            }
        },
    series: [{
        name: '%',
        data: [
			<?php
				foreach( $genderArr as $key=>$val ) {
				if ( $key > 0 ) {
			?>
				,['<?php echo $val['gender']; ?>',<?php echo $val['val']; ?>]
			<?php		
				} else  {
			?>
				['<?php echo $val['gender']; ?>',<?php echo $val['val']; ?>]
			<?php		
				}
				}
			?>     
        ]
    }]
});


Highcharts.chart('audience_weekly', {
	 chart: {     
        height: 325,
        width: 520
    },
    title: {
        text: ''
    },
    xAxis: {
       categories: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
    },
    legend: {
        enabled: false
    },
    tooltip: {
        formatter: function () {
            return '<b>Audience:</b> '+this.y;
        }
    },
    labels: {
        items: [{
            html: '',
            style: {
                left: '50px',
                top: '18px',
                color: (Highcharts.theme && Highcharts.theme.textColor) || 'black'
            }
        }]
    },
     plotOptions: {
        series: {
            dataLabels: {
                enabled: false
            }
        }
    },
    series: [{
        type: 'column',
        name: '',
        data: [3, 2, 1, 3, 4,7,9]
    } ,{
        type: 'line',
        name: 'Audience',
        visible: true,        
        data: [3, 2, 1, 3, 4,7,9],
        marker: {
            lineWidth: 2,
            lineColor: Highcharts.getOptions().colors[3],
            fillColor: 'white'
        }
    }]
});
</script>

