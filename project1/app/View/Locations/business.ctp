<script type="text/javascript">
	var avatar = "<?php echo (empty($business['Location']['image']) || !file_exists(WWW_ROOT."/img/profile/".$business['Location']['image'])) ?  SITE_LINK.'/img/location-here.png' : SITE_LINK.'img/profile/'.$business['Location']['image']; ?>";
</script>
<?php

$count=count($location_images);
$j=1;
$i=0;
if($count>0)
{
	for($i=0;$i<$count;$i++)
	{		
		${"tmpImage".$j}= (empty($location_images[$i]['LocationImage']['image']) || !file_exists(WWW_ROOT."/img/location/".$location_images[$i]['LocationImage']['image'])) ?  SITE_LINK.'img/location-here.png	' : SITE_LINK.'img/location/'.$location_images[$i]['LocationImage']['image'];
		$j++;
	}
}
$i++;
for($i;$i<=6;$i++)
	{
		${"tmpImage".$j}=SITE_LINK.'img/location-here.png';
		$j++;
	}
//echo $tmpImage = file_get_contents($tmpImage); die;
 ?>
 <section class="inner-banner topheader">
   <div class="container">
		<div class="banner-cont">
		   <ul class="menu-banner">
				<li><a href="<?php echo SITE_LINK;?>drafted-vouchers" class="<?php echo ($this->params['controller'] == 'vouchers' && $this->params['action'] == 'index' )? 'active':''?>" title="My Vouchers">My Vouchers</a></li>   
				<li><a href="<?php echo SITE_LINK;?>locations" class="<?php echo ($this->params['controller'] == 'Locations' && $this->params['action'] == 'index' )? 'active':''?>" title="My Locations">My Locations</a></li>   
		   </ul>
		</div>
   </div>
</section>
    <!--/. banner sec -->				

<section class="my-account-desc">
	 <div class="container">
		 
		 <?php echo $this->Form->create('Location',array("type"=>"file","class"=>"themeForm")); ?> 
		
		 <?php echo $this->Form->input("url_page",array("type"=>"hidden","value"=>$this->params->url)); ?>
		<div class="location_image_preview">
			
			<div class="form-group inputField col-sm-12">
				<?php $count=count($location_images);$j=1;	
					if($count>0)
					{				
						for($i=0;$i<$count;$i++)
						{ ?>
						<div class="col-sm-2 location preview<?php echo $j ;?>">
							<div class="image-preview">	
								<div class="location_image img_upload<?php echo $j ?>_image">
									<a href="javascript:void(0);" class="delete_location" value="<?php echo $j ;?>" action="delete-location-images?id=<?php echo $location_images[$i]['LocationImage']['id'] ?>" id="image_<?php echo $j ;?>"> <?php echo $this->Html->image('delete_comment.png', array("alt"=>"Delete Images"));?></a>
								</div>
							  <div class="upload-drop-zone" id="drop-zone<?php echo $j ;?>"> 
								   <div class="upload-file"> 
									   <?php echo $this->Html->image(${"tmpImage".$j}, array("alt"=>"Voucher Pic","id"=>"img_upload".$j,"class"=>"img-upload")); ?>
									   <?php echo $this->Form->input("LocationImages.img_upload".$j."_value",array("id"=>"img_upload".$j."_value","label"=>false,"type"=>"hidden")); ?>
									   <?php echo $this->Form->input("LocationImages.img_upload".$j."_old",array("id"=>"img_upload".$j."_old","label"=>false,"type"=>"hidden","value"=>$location_images[$i]['LocationImage']['image'])); ?>
									   
								   </div>	
							   </div> 
							 </div>  
						</div>	
					<?php	
						$j++;
						}
					 }					
					 for($j;$j<=6;$j++)
						{ ($j==1)? $count=1: $count=$count;?>
						<div class="col-sm-2 location preview<?php echo $j ;?>" <?php echo ($j==1)? '': "style='display:none'" ?>>
							<div class="image-preview">	
								<div class="location_image img_upload<?php echo $j ?>_image"  style="display:none"><a href="javascript:void(0);" class="delete_location" value="<?php echo $j ;?>" action="" id="image_<?php echo $j ;?>"> <?php echo $this->Html->image('delete_comment.png', array("alt"=>"Delete Images"));?></a>
								</div>													
							  <div class="upload-drop-zone" id="drop-zone<?php echo $j ;?>"> 
								   <div class="upload-file"> 
									   <?php echo $this->Html->image(${"tmpImage".$j}, array("alt"=>"Voucher Pic","id"=>"img_upload".$j,"class"=>"img-upload")); ?>
									   <?php echo $this->Form->input("LocationImages.img_upload".$j."_value",array("id"=>"img_upload".$j."_value","label"=>false,"type"=>"hidden")); ?>
								   </div>	
							   </div> 
							 </div>  
						</div>	
						<?php
									
						}
					 
					 
					 ?>
					
			</div>	
			</div>	
			<div class="form-group inputField col-sm-12">
					<div class="my-account-pro-right">				
							<input id="addmore" class="add_location_image" type="button" value="Add Location Images"/>
					</div>
			</div>
		<!-- Add account -->		 
			<div class="account-detals">
					<?php //echo $this->Form->hidden('image_old',array('value'=>$user['UserDetail']['image']));?>
					 <?php echo $this->Form->hidden('businessid',array("value"=>$business["Location"]["id"]));echo $this->Form->hidden('latitude',array("value"=>$business["Location"]["latitude"])); echo $this->Form->hidden('longitude',array("value"=>$business["Location"]["longitude"]));?>
					<div class="row">
						<div class="col-sm-12 vochr-right">
						<div class="timeboxsetting" style="float:right;">						
							<!--input type="checkbox" name="data[Voucher][online_status]" id="statusToggle" data-toggle="toggle" data-on="Online" data-off="In Store" data-onstyle="success" data-offstyle="danger"-->
							<?php $checked = ($business['Location']['show_on_rave'] == '1')? "checked":'';?>
							<div class="instore">Show on Rave</div>
							<label class="switch switch_btn">		
								
								 <?php $optionsOnOff = array("on"=>"on","off"=>"off"); echo $this->Form->checkbox('show_on_rave', array('id'=>'statusToggle','value'=>$business['Location']['show_on_rave'],'label'=>'','options'=>$optionsOnOff,$checked)) ;
								 ?>

								  <span class="slider round"></span>
								  <!-- <span class="absolute-no">NO</span> -->
							</label>
						</div>	
						</div>	
					</div>
					<div class="row">
					   <div class="form-group inputField col-sm-6">
						  <label class="lable-control"><i class="sm-icons"><?php echo $this->Html->image("sm-icon1.png",array("alt"=>"Full Name","placeholder"=>"Location Name")); ?></i> Location Name</label>
						  <?php echo $this->Form->input('title',array("value"=>$business["Location"]["title"],'label'=>false,"class"=>"form-control","placeholder"=>"Location Name"));?>						
					   </div>					  
					   <div class="form-group inputField col-sm-6">
						<label class="lable-control"><i class="sm-icons"><?php echo $this->Html->image("facebook-logo.png",array("alt"=>"Facebook")); ?></i> Facebook </label>
						  <?php
						  echo $this->Form->input('facebookid',array("value"=>$business["Location"]["facebookid"],"class"=>"form-control","placeholder"=>"https://www.facebook.com/example/","maxlength"=>200,"label"=>false,"autocomplete"=>"off"));	?>		
					   </div>	
					</div>
					<div class="row">					  				  
					   <div class="form-group inputField col-sm-6">
						  	<label class="lable-control"><i class="sm-icons"><?php echo $this->Html->image("twitter.png",array("alt"=>"Twitter")); ?></i> Twitter </label>
						    <?php echo $this->Form->input('twitterid',array("value"=>$business["Location"]["twitterid"],"class"=>"form-control","placeholder"=>"https://twitter.com/example/","maxlength"=>200,"label"=>false,"autocomplete"=>"off"));	?>	  
					   </div>
					   <div class="form-group inputField col-sm-6">
						  <label class="lable-control"><i class="sm-icons"><?php echo $this->Html->image("instagram.png",array("alt"=>"Instagram")); ?></i> Instagram </label>
						  <?php
						  echo $this->Form->input('instagramid',array("value"=>$business["Location"]["twitterid"],"class"=>"form-control","placeholder"=>"https://www.instagram.com/example/","maxlength"=>200,"label"=>false,"autocomplete"=>"off")); ?>						  						
					   </div>
					</div>
					
					<div class="row">						
					    <div class="form-group inputField col-sm-12">
						   <label class="lable-control"><i class="sm-icons"><?php echo $this->Html->image("sm-icon4.png",array("alt"=>"Discount Venue")); ?></i> Discount Venue</label>
						    <div id="pac-container"><?php echo $this->Form->input('address',array("value"=>$business["Location"]["address"],"id"=>"pac-input","class"=>"form-control",'label'=>false,"placeholder"=>"Your Company Name, Your Street, Your Town"));?>
							</div>
						</div>						
					</div>
					<div class="row">
							<div class="col-sm-12">
								<i class="sm-icons"><?php echo $this->Html->image("cal-icon.png",array("alt"=>"Time")); ?></i>Set Opening and closing Time
							</div>
					</div>		
					<div class="row">
						<div class="form-group inputField_time col-sm-6">
							<div class="col-sm-4">
							  <div class="weekday_text">Monday</div>	
							</div>  
							<div class="col-sm-4">								
								  <div class='input_date_grp input-group date datetimepicker2'>
								  <?php echo $this->Form->input('OpeningTiming.starttime_1',array("label"=>false,"type"=>"text",'autofill'=>'false',"class"=>"form-control","value"=> ($data[0]['OpeningTiming']['start_time']=="")? '' : date('h:i A',strtotime($data[0]['OpeningTiming']['start_time']))));?>                
								  <span class="input-group-addon">
									<span class="fa fa-clock-o"></span>
								  </span>
								</div>
							</div>
							<div class="col-sm-4">
							  <div class='input_date_grp input-group date datetimepicker2'>
								  <?php echo $this->Form->input('OpeningTiming.endtime_1',array("label"=>false,"type"=>"text",'autofill'=>'false',"class"=>"form-control","value"=> ($data[0]['OpeningTiming']['end_time']=="")? '' : date('h:i A',strtotime($data[0]['OpeningTiming']['end_time']))));?>                
								  <span class="input-group-addon">
									<span class="fa fa-clock-o"></span>
								  </span>
								</div>
							</div>
						</div>
						<div class="form-group inputField_time col-sm-6">
							<div class="col-sm-4">
							  <div class="weekday_text">Friday</div>	
							</div>  
							<div class="col-sm-4">								
								  <div class='input_date_grp input-group date datetimepicker2'>
								  <?php echo $this->Form->input('OpeningTiming.starttime_5',array("label"=>false,"type"=>"text",'autofill'=>'false',"class"=>"form-control","value"=> ($data[4]['OpeningTiming']['start_time']=="")? '' : date('h:i A',strtotime($data[4]['OpeningTiming']['start_time']))));?>                
								  <span class="input-group-addon">
									<span class="fa fa-clock-o"></span>
								  </span>
								</div>
							</div>
							<div class="col-sm-4">
							  <div class='input_date_grp input-group date datetimepicker2'>
								  <?php echo $this->Form->input('OpeningTiming.endtime_5',array("label"=>false,"type"=>"text",'autofill'=>'false',"class"=>"form-control","value"=>($data[4]['OpeningTiming']['end_time']=="")? '' :  date('h:i A',strtotime($data[4]['OpeningTiming']['end_time']))));?>                
								  <span class="input-group-addon">
									<span class="fa fa-clock-o"></span>
								  </span>
								</div>
							</div>
						</div>		
					</div>
					<div class="row">
						<div class="form-group inputField_time col-sm-6">
							<div class="col-sm-4">
							  <div class="weekday_text">Tuesday</div>	
							</div>  
							<div class="col-sm-4">								
								  <div class='input_date_grp input-group date datetimepicker2'>
								  <?php echo $this->Form->input('OpeningTiming.starttime_2',array("label"=>false,"type"=>"text",'autofill'=>'false',"class"=>"form-control","value"=> ($data[1]['OpeningTiming']['start_time']=="")? '' : date('h:i A',strtotime($data[1]['OpeningTiming']['start_time']))));?>                
								  <span class="input-group-addon">
									<span class="fa fa-clock-o"></span>
								  </span>
								</div>
							</div>
							<div class="col-sm-4">
							  <div class='input_date_grp input-group date datetimepicker2'>
								  <?php echo $this->Form->input('OpeningTiming.endtime_2',array("label"=>false,"type"=>"text",'autofill'=>'false',"class"=>"form-control","value"=> ($data[1]['OpeningTiming']['end_time']=="")? '' : date('h:i A',strtotime($data[1]['OpeningTiming']['end_time']))));?>                
								  <span class="input-group-addon">
									<span class="fa fa-clock-o"></span>
								  </span>
								</div>
							</div>
						</div>
						<div class="form-group inputField_time col-sm-6">
							<div class="col-sm-4">
							  <div class="weekday_text">Saturday</div>	
							</div>  
							<div class="col-sm-4">								
								  <div class='input_date_grp input-group date datetimepicker2'>
								  <?php echo $this->Form->input('OpeningTiming.starttime_6',array("label"=>false,"type"=>"text",'autofill'=>'false',"class"=>"form-control","value"=>($data[5]['OpeningTiming']['start_time']=="")? '' :  date('h:i A',strtotime($data[5]['OpeningTiming']['start_time']))));?>                
								  <span class="input-group-addon">
									<span class="fa fa-clock-o"></span>
								  </span>
								</div>
							</div>
							<div class="col-sm-4">
							  <div class='input_date_grp input-group date datetimepicker2'>
								  <?php echo $this->Form->input('OpeningTiming.endtime_6',array("label"=>false,"type"=>"text",'autofill'=>'false',"class"=>"form-control","value"=> ($data[5]['OpeningTiming']['end_time']=="")? '' : date('h:i A',strtotime($data[5]['OpeningTiming']['end_time']))));?>                
								  <span class="input-group-addon">
									<span class="fa fa-clock-o"></span>
								  </span>
								</div>
							</div>
						</div>		
					</div>
					<div class="row">
						<div class="form-group inputField_time col-sm-6">
							<div class="col-sm-4">
							  <div class="weekday_text">Wednesday</div>	
							</div>  
							<div class="col-sm-4">								
								  <div class='input_date_grp input-group date datetimepicker2'>
								  <?php echo $this->Form->input('OpeningTiming.starttime_3',array("label"=>false,"type"=>"text",'autofill'=>'false',"class"=>"form-control","value"=>($data[2]['OpeningTiming']['start_time']=="")? '' :  date('h:i A',strtotime($data[2]['OpeningTiming']['start_time']))));?>                
								  <span class="input-group-addon">
									<span class="fa fa-clock-o"></span>
								  </span>
								</div>
							</div>
							<div class="col-sm-4">
							  <div class='input_date_grp input-group date datetimepicker2'>
								  <?php echo $this->Form->input('OpeningTiming.endtime_3',array("label"=>false,"type"=>"text",'autofill'=>'false',"class"=>"form-control","value"=> ($data[2]['OpeningTiming']['end_time']=="")? '' : date('h:i A',strtotime($data[2]['OpeningTiming']['end_time']))));?>                
								  <span class="input-group-addon">
									<span class="fa fa-clock-o"></span>
								  </span>
								</div>
							</div>
						</div>
						<div class="form-group inputField_time col-sm-6">
							<div class="col-sm-4">
							  <div class="weekday_text">Sunday</div>	
							</div>  
							<div class="col-sm-4">								
								  <div class='input_date_grp input-group date datetimepicker2'>
								  <?php echo $this->Form->input('OpeningTiming.starttime_7',array("label"=>false,"type"=>"text",'autofill'=>'false',"class"=>"form-control","value"=>($data[6]['OpeningTiming']['start_time']=="")? '' : date('h:i A',strtotime($data[6]['OpeningTiming']['start_time']))));?>                
								  <span class="input-group-addon">
									<span class="fa fa-clock-o"></span>
								  </span>
								</div>
							</div>
							<div class="col-sm-4">
							  <div class='input_date_grp input-group date datetimepicker2'>
								  <?php echo $this->Form->input('OpeningTiming.endtime_7',array("label"=>false,"type"=>"text",'autofill'=>'false',"class"=>"form-control","value"=>($data[6]['OpeningTiming']['end_time']=="")? '' : date('h:i A',strtotime($data[6]['OpeningTiming']['end_time']))));?>                
								  <span class="input-group-addon">
									<span class="fa fa-clock-o"></span>
								  </span>
								</div>
							</div>
						</div>		
					</div>
					<div class="row">
						<div class="form-group inputField_time col-sm-6">
							<div class="col-sm-4">
							  <div class="weekday_text">Thursday</div>	
							</div>  
							<div class="col-sm-4">								
								  <div class='input_date_grp input-group date datetimepicker2'>
								  <?php echo $this->Form->input('OpeningTiming.starttime_4',array("label"=>false,"type"=>"text",'autofill'=>'false',"class"=>"form-control","value"=> ($data[3]['OpeningTiming']['start_time']=="")? '' :date('h:i A',strtotime($data[3]['OpeningTiming']['start_time']))));?>                
								  <span class="input-group-addon">
									<span class="fa fa-clock-o"></span>
								  </span>
								</div>
							</div>
							<div class="col-sm-4">
							  <div class='input_date_grp input-group date datetimepicker2'>
								  <?php echo $this->Form->input('OpeningTiming.endtime_4',array("label"=>false,"type"=>"text",'autofill'=>'false',"class"=>"form-control","value"=> ($data[3]['OpeningTiming']['end_time']=="")? '' :date('h:i A',strtotime($data[3]['OpeningTiming']['end_time']))));?>                
								  <span class="input-group-addon">
									<span class="fa fa-clock-o"></span>
								  </span>
								</div>
							</div>
						</div>							
					</div>
					
					
					<div class="row">
						 <div class="form-group inputField col-sm-12">
						  <label class="lable-control"><i class="sm-icons"><?php echo $this->Html->image("about-us.png",array("alt"=>"About Me")); ?></i> About Location</label>
						  <?php
						  echo $this->Form->input('about',array("value"=>$business["Location"]["about"],"class"=>"form-control","type"=>"text","placeholder"=>"","rows"=>4,"label"=>false));	?>					  
					   </div>	
					</div>
					<div class="map-location">
						 <div class="map-frame">																			
							 <div id="map" style="height:490px;width:1170px;"></div>
							 <div id="infowindow-content">
							  <img src="" width="16" height="16" id="place-icon">
							  <span id="place-name"  class="title"></span><br>
							  <span id="place-address"></span>
							 </div>		
						 </div>
					</div>
					<div class="form-submit form-submit-bottom clearfix">										 
<!--
									<?php //if($business['Location']['is_paid']==1 ) {?>
									 <button type="button" class="btn btn-white" title="Pause Payment" id="pausepaypalpayment">Pause Payment</button>
									  <?php //} else if ($business['Location']['is_paid']==0 &&   $business['Location']['payment_status']=="Suspend" ){ ?>
										<button type="button" class="btn btn-white" title="Reactivate Payment" id="pausepaypalpayment">Re-activate Payment</button>
									<?php //} else {}?>								
-->
							<button type="submit" class="btn btn-theme">Update</button>
					</div>				
				</div>
				<?php echo $this->Form->end(); ?>
			</div>			
			<!-- End add account -->
	 </div>
</section>
<!--/. My Account -->

<style>
.imageBox{width:520px !important;height:350px !important;}
</style>
 <!-- Business Deletion Confirmation-->   
<div class="modal in fade themeModel" tabindex="-1" role="dialog" id="delete_locationbox">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Delete</h5>       
      </div>
      <div class="modal-body">
        <p>Are you sure you want to delete this location image?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary btn-theme" data-dismiss="modal">No</button>
        <button type="button" class="btn btn-secondary btn-theme confirmdelete_location">Yes</button>
      </div>
    </div>
  </div>
</div> 
 <!-- End Business Deletion Confirmation-->   

<div class="modal fade loginModel themeModel" id="imageModal" tabindex="-1" role="dialog" aria-labelledby="imageModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
       <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title">Select Image </h4>
      </div>
      <div class="modal-body actions">
        <div class="selct-img-voc">
		  <div class="img-container" style="width:100%;">
				<img src="<?php echo $tmpImage1; ?>" alt="Picture">
				<input hidden class="replace-image" value="img_upload1" />
			</div>
		  <div class="iviewer" >
			 <ul class="custom-file-upload" class="docs-buttons">
				 <li><label for="upload-file" class="custom-file-upload"><span ><?php echo $this->Html->image("gallery-img.png",array("alt"=>"Voucher Pic")); ?></span></label> <input id="upload-file" class="upload-file" type="file" name="file" accept="image/*"/></li>
				<li data-method="rotate" data-option="-90"><a href="javascript:void(0)" title="Rotate Left"><span id="rotateLeft"><?php echo $this->Html->image("rotate-left.png",array("alt"=>"Voucher Pic")); ?></span></a></li>
				 <li data-method="rotate" data-option="90"><a href="javascript:void(0)" title="Rotate Right"><span id="rotateRight"><?php echo $this->Html->image("rotate-right.png",array("alt"=>"Voucher Pic")); ?></span></a></li>
				 <li data-method="zoom" data-option="-0.1"><a href="javascript:void(0)" title="Zoom Out"><span id="zoomIn"><?php echo $this->Html->image("zoom-out.png",array("alt"=>"Voucher Pic")); ?></span></a></li>
				  <li data-method="zoom" data-option="0.1"><a href="javascript:void(0)" title="Zoom In"><span id="zoomOut"><?php echo $this->Html->image("zoom-in.png",array("alt"=>"Voucher Pic")); ?></span></a></li>
			 </ul>             
		  </div>
		</div>
        
		<div class="iviewer-btn">
			<ul class="list-inline">
			   <li><button class="btn btn-custom" data-dismiss="modal">CLOSE</button></li> 
			   <li><button class="btn btn-custom btn-primary" data-method="getCroppedCanvas" data-option="{ &quot;maxWidth&quot;: 4096, &quot;maxHeight&quot;: 4096 }" id="saveimage">SAVE CHANGES</button></li>
			</ul>
		</div>
	 </div>  	
    </div>
  </div>
</div>



<div class="modal fade loginModel themeModel" id="imageModal2" tabindex="-1" role="dialog" aria-labelledby="imageModal2Label" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
       <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title">Select Image </h4>
      </div>
      <div class="modal-body actions">
        <div class="selct-img-voc">
		  <div class="img-container" style="width:100%;">
				<img src="<?php echo $tmpImage2; ?>" alt="Picture">
				<!-- Hidden field contains the id of the field it's going to replace -->
				<input hidden class="replace-image" value="img_upload2" />
			</div>
		  <div class="iviewer" >
			 <ul class="custom-file-upload" class="docs-buttons">
				 <li><label for="upload-file" class="custom-file-upload"><span ><?php echo $this->Html->image("gallery-img.png",array("alt"=>"Voucher Pic")); ?></span></label> <input id="upload-file-banner"  class="upload-file" type="file" name="file" accept="image/*"/></li>
				<li data-method="rotate" data-option="-90"><a href="javascript:void(0)" title="Rotate Left"><span id="rotateLeft"><?php echo $this->Html->image("rotate-left.png",array("alt"=>"Voucher Pic")); ?></span></a></li>
				 <li data-method="rotate" data-option="90"><a href="javascript:void(0)" title="Rotate Right"><span id="rotateRight"><?php echo $this->Html->image("rotate-right.png",array("alt"=>"Voucher Pic")); ?></span></a></li>
				 <li data-method="zoom" data-option="-0.1"><a href="javascript:void(0)" title="Zoom Out"><span id="zoomIn"><?php echo $this->Html->image("zoom-out.png",array("alt"=>"Voucher Pic")); ?></span></a></li>
				  <li data-method="zoom" data-option="0.1"><a href="javascript:void(0)" title="Zoom In"><span id="zoomOut"><?php echo $this->Html->image("zoom-in.png",array("alt"=>"Voucher Pic")); ?></span></a></li>
			 </ul>             
		  </div>
		</div>
        
		<div class="iviewer-btn">
			<ul class="list-inline">
			   <li><button class="btn btn-custom" data-dismiss="modal">CLOSE</button></li> 
			   <li><button class="btn btn-custom btn-primary" data-method="getCroppedCanvas" data-option="{ &quot;maxWidth&quot;: 4096, &quot;maxHeight&quot;: 4096 }" id="saveimage">SAVE CHANGES</button></li>
			</ul>
		</div>
	 </div>  	
    </div>
  </div>
</div>
<div class="modal fade loginModel themeModel" id="imageModal3" tabindex="-1" role="dialog" aria-labelledby="imageModal3Label" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
       <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title">Select Image </h4>
      </div>
      <div class="modal-body actions">
        <div class="selct-img-voc">
		  <div class="img-container" style="width:100%;">
				<img src="<?php echo $tmpImage3; ?>" alt="Picture">
				<!-- Hidden field contains the id of the field it's going to replace -->
				<input hidden class="replace-image" value="img_upload3" />
			</div>
		  <div class="iviewer" >
			 <ul class="custom-file-upload" class="docs-buttons">
				 <li><label for="upload-file" class="custom-file-upload"><span ><?php echo $this->Html->image("gallery-img.png",array("alt"=>"Voucher Pic")); ?></span></label> <input id="upload-file-banner3"  class="upload-file" type="file" name="file" accept="image/*"/></li> 
				<li data-method="rotate" data-option="-90"><a href="javascript:void(0)" title="Rotate Left"><span id="rotateLeft"><?php echo $this->Html->image("rotate-left.png",array("alt"=>"Voucher Pic")); ?></span></a></li>
				 <li data-method="rotate" data-option="90"><a href="javascript:void(0)" title="Rotate Right"><span id="rotateRight"><?php echo $this->Html->image("rotate-right.png",array("alt"=>"Voucher Pic")); ?></span></a></li>
				 <li data-method="zoom" data-option="-0.1"><a href="javascript:void(0)" title="Zoom Out"><span id="zoomIn"><?php echo $this->Html->image("zoom-out.png",array("alt"=>"Voucher Pic")); ?></span></a></li>
				  <li data-method="zoom" data-option="0.1"><a href="javascript:void(0)" title="Zoom In"><span id="zoomOut"><?php echo $this->Html->image("zoom-in.png",array("alt"=>"Voucher Pic")); ?></span></a></li>
			 </ul>             
		  </div>
		</div>
        
		<div class="iviewer-btn">
			<ul class="list-inline">
			   <li><button class="btn btn-custom" data-dismiss="modal">CLOSE</button></li> 
			   <li><button class="btn btn-custom btn-primary" data-method="getCroppedCanvas" data-option="{ &quot;maxWidth&quot;: 4096, &quot;maxHeight&quot;: 4096 }" id="saveimage">SAVE CHANGES</button></li>
			</ul>
		</div>
	 </div>  	
    </div>
  </div>
</div>
<div class="modal fade loginModel themeModel" id="imageModal4" tabindex="-1" role="dialog" aria-labelledby="imageModal4Label" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
       <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title">Select Image </h4>
      </div>
      <div class="modal-body actions">
        <div class="selct-img-voc">
		  <div class="img-container" style="width:100%;">
				<img src="<?php echo $tmpImage4; ?>" alt="Picture">
				<!-- Hidden field contains the id of the field it's going to replace -->
				<input hidden class="replace-image" value="img_upload4" />
			</div>
		  <div class="iviewer" >
			 <ul class="custom-file-upload" class="docs-buttons">
				 <li><label for="upload-file" class="custom-file-upload"><span ><?php echo $this->Html->image("gallery-img.png",array("alt"=>"Voucher Pic")); ?></span></label> <input id="upload-file-banner4"  class="upload-file" type="file" name="file" accept="image/*"/></li> 
				<li data-method="rotate" data-option="-90"><a href="javascript:void(0)" title="Rotate Left"><span id="rotateLeft"><?php echo $this->Html->image("rotate-left.png",array("alt"=>"Voucher Pic")); ?></span></a></li>
				 <li data-method="rotate" data-option="90"><a href="javascript:void(0)" title="Rotate Right"><span id="rotateRight"><?php echo $this->Html->image("rotate-right.png",array("alt"=>"Voucher Pic")); ?></span></a></li>
				 <li data-method="zoom" data-option="-0.1"><a href="javascript:void(0)" title="Zoom Out"><span id="zoomIn"><?php echo $this->Html->image("zoom-out.png",array("alt"=>"Voucher Pic")); ?></span></a></li>
				  <li data-method="zoom" data-option="0.1"><a href="javascript:void(0)" title="Zoom In"><span id="zoomOut"><?php echo $this->Html->image("zoom-in.png",array("alt"=>"Voucher Pic")); ?></span></a></li>
			 </ul>             
		  </div>
		</div>
        
		<div class="iviewer-btn">
			<ul class="list-inline">
			   <li><button class="btn btn-custom" data-dismiss="modal">CLOSE</button></li> 
			   <li><button class="btn btn-custom btn-primary" data-method="getCroppedCanvas" data-option="{ &quot;maxWidth&quot;: 4096, &quot;maxHeight&quot;: 4096 }" id="saveimage">SAVE CHANGES</button></li>
			</ul>
		</div>
	 </div>  	
    </div>
  </div>
</div>

<div class="modal fade loginModel themeModel" id="imageModal5" tabindex="-1" role="dialog" aria-labelledby="imageModal5Label" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
       <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title">Select Image </h4>
      </div>
      <div class="modal-body actions">
        <div class="selct-img-voc">
		  <div class="img-container" style="width:100%;">
				<img src="<?php echo $tmpImage5; ?>" alt="Picture">
				<!-- Hidden field contains the id of the field it's going to replace -->
				<input hidden class="replace-image" value="img_upload5" />
			</div>
		  <div class="iviewer" >
			 <ul class="custom-file-upload" class="docs-buttons">
				 <li><label for="upload-file" class="custom-file-upload"><span ><?php echo $this->Html->image("gallery-img.png",array("alt"=>"Voucher Pic")); ?></span></label> <input id="upload-file-banner5"  class="upload-file" type="file" name="file" accept="image/*"/></li> 
				<li data-method="rotate" data-option="-90"><a href="javascript:void(0)" title="Rotate Left"><span id="rotateLeft"><?php echo $this->Html->image("rotate-left.png",array("alt"=>"Voucher Pic")); ?></span></a></li>
				 <li data-method="rotate" data-option="90"><a href="javascript:void(0)" title="Rotate Right"><span id="rotateRight"><?php echo $this->Html->image("rotate-right.png",array("alt"=>"Voucher Pic")); ?></span></a></li>
				 <li data-method="zoom" data-option="-0.1"><a href="javascript:void(0)" title="Zoom Out"><span id="zoomIn"><?php echo $this->Html->image("zoom-out.png",array("alt"=>"Voucher Pic")); ?></span></a></li>
				  <li data-method="zoom" data-option="0.1"><a href="javascript:void(0)" title="Zoom In"><span id="zoomOut"><?php echo $this->Html->image("zoom-in.png",array("alt"=>"Voucher Pic")); ?></span></a></li>
			 </ul>             
		  </div>
		</div>
        
		<div class="iviewer-btn">
			<ul class="list-inline">
			   <li><button class="btn btn-custom" data-dismiss="modal">CLOSE</button></li> 
			   <li><button class="btn btn-custom btn-primary" data-method="getCroppedCanvas" data-option="{ &quot;maxWidth&quot;: 4096, &quot;maxHeight&quot;: 4096 }" id="saveimage">SAVE CHANGES</button></li>
			</ul>
		</div>
	 </div>  	
    </div>
  </div>
</div>


<div class="modal fade loginModel themeModel" id="imageModal6" tabindex="-1" role="dialog" aria-labelledby="imageModal6Label" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
       <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title">Select Image </h4>
      </div>
      <div class="modal-body actions">
        <div class="selct-img-voc">
		  <div class="img-container" style="width:100%;">
				<img src="<?php echo $tmpImage6; ?>" alt="Picture">
				<!-- Hidden field contains the id of the field it's going to replace -->
				<input hidden class="replace-image" value="img_upload6" />
			</div>
		  <div class="iviewer" >
			 <ul class="custom-file-upload" class="docs-buttons">
				 <li><label for="upload-file" class="custom-file-upload"><span ><?php echo $this->Html->image("gallery-img.png",array("alt"=>"Voucher Pic")); ?></span></label> <input id="upload-file-banner6"  class="upload-file" type="file" name="file" accept="image/*"/></li> 
				<li data-method="rotate" data-option="-90"><a href="javascript:void(0)" title="Rotate Left"><span id="rotateLeft"><?php echo $this->Html->image("rotate-left.png",array("alt"=>"Voucher Pic")); ?></span></a></li>
				 <li data-method="rotate" data-option="90"><a href="javascript:void(0)" title="Rotate Right"><span id="rotateRight"><?php echo $this->Html->image("rotate-right.png",array("alt"=>"Voucher Pic")); ?></span></a></li>
				 <li data-method="zoom" data-option="-0.1"><a href="javascript:void(0)" title="Zoom Out"><span id="zoomIn"><?php echo $this->Html->image("zoom-out.png",array("alt"=>"Voucher Pic")); ?></span></a></li>
				  <li data-method="zoom" data-option="0.1"><a href="javascript:void(0)" title="Zoom In"><span id="zoomOut"><?php echo $this->Html->image("zoom-in.png",array("alt"=>"Voucher Pic")); ?></span></a></li>
			 </ul>             
		  </div>
		</div>
        
		<div class="iviewer-btn">
			<ul class="list-inline">
			   <li><button class="btn btn-custom" data-dismiss="modal">CLOSE</button></li> 
			   <li><button class="btn btn-custom btn-primary" data-method="getCroppedCanvas" data-option="{ &quot;maxWidth&quot;: 4096, &quot;maxHeight&quot;: 4096 }" id="saveimage">SAVE CHANGES</button></li>
			</ul>
		</div>
	 </div>  	
    </div>
  </div>
</div>


<style>
.img-container {
  /* Never limit the container height here */
  max-width: 100%;
}

.img-container img {
  /* This is important */
  width: 100%;
}
.cropper-bg {background-image:none !important;}
.cropper-modal {background-color:#EEEEEE !important;opacity:0 !important;}

.weekday_text{margin-top:14px;}
.account-detals .inputField_time{margin-bottom:14px;}
 .account-detals .inputField_time input{font-size:14px;}
</style>

 <script>
      // This example requires the Places library. Include the libraries=places
      // parameter when you first load the API. For example:
      // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">
     	//Adding Place for images preview
     		
		if(<?php echo $count?>==6)
			{
				$("#addmore").attr("disabled", true);			
				$('#addmore').css('cursor','not-allowed');
			}
		<?php  $add_more= ++$count;  ?>;
		var add_more=<?php echo $add_more?>;	
		$("#addmore").on('click', function() {
			//$("#imageModalHeader1"+read_more).modal("show");
			$(".txt_demo_location_image").hide();			
			$(".preview"+add_more).show();	
			
			if(add_more>=6)
			{
				$("#addmore").attr("disabled", true);			
				$('#addmore').css('cursor','not-allowed');
			}				
			add_more++;
			
		});
      
     
		var switchStatus = false;
		$("#statusToggle").on('change', function() {
			if ($(this).is(':checked')) {
				switchStatus = $(this).is(':checked');
				$("#statusToggle").val(1);
			}
			else {
			   switchStatus = $(this).is(':checked');
			   $("#statusToggle").val(0);
			}
		});				
		
      function initMap() {
		  		  
        var map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: <?php echo $business["Location"]["latitude"]?>, lng: <?php echo $business["Location"]["longitude"]?>},
          zoom: 13
        });
        var card = document.getElementById('pac-card');
        var input = document.getElementById('pac-input');
        var types = document.getElementById('type-selector');
        var strictBounds = document.getElementById('strict-bounds-selector');

        map.controls[google.maps.ControlPosition.TOP_RIGHT].push(card);

        var autocomplete = new google.maps.places.Autocomplete(input);

        // Bind the map's bounds (viewport) property to the autocomplete object,
        // so that the autocomplete requests use the current map bounds for the
        // bounds option in the request.
        autocomplete.bindTo('bounds', map);
		
        var infowindow = new google.maps.InfoWindow();
        var infowindowContent = document.getElementById('infowindow-content');
        infowindow.setContent(infowindowContent);
        var marker = new google.maps.Marker({
		 position: new google.maps.LatLng( <?php echo $business["Location"]["latitude"]?>,<?php echo $business["Location"]["longitude"]?>), 
          map: map   
        });

          autocomplete.addListener('place_changed', function() {
          infowindow.close();
          marker.setVisible(false);
          var place = autocomplete.getPlace();
          if (!place.geometry) {
            // User entered the name of a Place that was not suggested and
            // pressed the Enter key, or the Place Details request failed.
            //window.alert("No details available for input: '" + place.name + "'");
            return;
          }

          // If the place has a geometry, then present it on a map.
          if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
            var address =place.name;
            var geocoder = new google.maps.Geocoder();
            geocoder.geocode({ 'address': address}, function (results, status) {
                        if (status == google.maps.GeocoderStatus.OK) {
							var latitude = place.geometry.location.lat();
                            var longitude = place.geometry.location.lng();
                            $('#LocationLatitude').val(latitude);
                            $('#LocationLongitude').val(longitude);
                        } else {
                           
                        }
                    });
            
          } else {
			 
            map.setCenter(place.geometry.location);
            map.setZoom(17);  // Why 17? Because it looks good.
          }
          marker.setPosition(place.geometry.location);
          marker.setVisible(true);

          var address = '';
          if (place.address_components) {
            address = [
              (place.address_components[0] && place.address_components[0].short_name || ''),
              (place.address_components[1] && place.address_components[1].short_name || ''),
              (place.address_components[2] && place.address_components[2].short_name || '')
            ].join(' ');
          }       
          infowindowContent.children['place-icon'].src = place.icon;
          infowindowContent.children['place-name'].textContent = place.name;
          infowindowContent.children['place-address'].textContent = address;         
          	
			infowindow.open(map, marker);
        }    
        );	
        
		map.setCenter(new google.maps.LatLng( <?php echo $business["Location"]["latitude"]?>,<?php echo $business["Location"]["longitude"]?>));		
        map.setZoom(17);  
        
         if(<?php echo $business['Location']['latitude']?>!=""){
          marker.addListener('click', function() {
			infowindow = new google.maps.InfoWindow({
			content: "<?php echo $business['Location']['address'];?>"
			});
			infowindow.open(map, marker);
			});
		}
	}
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDcBcY5ObGmiZ6TETlFS1cEOi5ttvBRnuw&libraries=places&callback=initMap" async defer></script>
<div class="modal in fade themeModel" tabindex="-1" role="dialog" id="deletebusienssbox">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Location Deletion Confirmation</h5>       
      </div>
      <div class="modal-body">
        <p>Are you sure you want to delete location permanently.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary btn-theme" data-dismiss="modal">No</button>
        <button type="button" class="btn btn-secondary btn-theme confirmdeletebusiness">Yes</button>
      </div>
    </div>
  </div>
</div>  
<div class="modal in fade themeModel" tabindex="-1" role="dialog" id="makepayment">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Make Payment</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="pricing-card">
			<div class="pricing-plan">
				<div class="price-crcl">
				   <p>PRICING PLAN</p>
					<h2><span>£</span> 9.99  <small>+VAT /mo</small></h2>
				</div>
				<div class="plan-desc">
					<p>We have got you covered.</p>
					<p>Create any number of vouchers at just £9.99 / mo</p>                        
				</div>
			</div>
			<div class="price-desc">
				<p>We will only offer one payment plan which is £9.99 / mo. In that plan a merchant can publish as many vouchers as they like.</p>
			</div>                 
		</div>  
		<a href="<?php echo SITE_LINK."buy-plan/".$plan['Plan']['id']."/".$business['Location']['id']; ?>" style="display:none;" id="paymentbtn" ></a>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary btn-theme" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-secondary btn-theme confirmpayment">Make Payment</button>
      </div>
    </div>
  </div>
</div>  
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
