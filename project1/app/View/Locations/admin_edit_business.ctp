<script type="text/javascript">
	var avatar = "<?php echo (empty($business['Location']['image']) || !file_exists(WWW_ROOT."/img/profile/".$business['Location']['image'])) ?  SITE_LINK.'/img/default_new.png' : SITE_LINK.'img/profile/'.$business['Location']['image']; ?>";
</script>
<?php
$tmpImage = (empty($business['Location']['image']) || !file_exists(WWW_ROOT."/img/profile/".$business['Location']['image'])) ?  SITE_LINK.'/img/default_new.png' : SITE_LINK.'img/profile/'.$business['Location']['image'];

//echo $tmpImage = file_get_contents($tmpImage); die;
 ?>
<div class="cont-right">
   <div class="cont-right-innr">
		<div class="main-hd-in">
			  <div class="row">
				<div class="col-sm-12">
				  <h2>Edit Location</h2>              
				</div>
			  </div>
        </div>
		<div class="manage-marchant">			
				<?php echo $this->Form->create('Location',array("type"=>"file","class"=>"themeForm")); ?> 
				 <div class="my-account-pro clearfix">	
				 <div class="my-account-pro-left">	
					<div class="profile-cont">		 
						<div class="prof-pic user_pic img-upload" style="background-image:url('<?php echo (empty($business['Location']['image']) || !file_exists(WWW_ROOT."/img/profile/".$business['Location']['image'])) ?  SITE_LINK.'/img/default_new.png' : SITE_LINK.'img/profile/'.$business['Location']['image']; ?>');"> 
						<?php echo $this->Form->input("tmpImage",array("label"=>false,"type"=>"textarea","style"=>"display:none;","id"=>"tmpImage")); ?>	
						</div>
						<div class="prof-details">						  
						</div>
					</div>
				</div>					
				 <div class="my-account-pro-right">				
					
				</div>
			 </div>
			<div class="account-detals">
					<?php //echo $this->Form->hidden('image_old',array('value'=>$user['UserDetail']['image']));?>
					 <?php echo $this->Form->hidden('businessid',array("value"=>$business["Location"]["id"]));echo $this->Form->hidden('userid',array("value"=>$business["Location"]["user_id"]));echo $this->Form->hidden('latitude',array("value"=>$business["Location"]["latitude"])); echo $this->Form->hidden('longitude',array("value"=>$business["Location"]["longitude"]));?>

					<div class="row">
					   <div class="form-group inputField col-sm-6">
						  <label class="lable-control"><i class="sm-icons"><?php echo $this->Html->image("sm-icon1.png",array("alt"=>"Full Name","placeholder"=>"Lcation Name")); ?></i> Location Name</label>
						  <?php echo $this->Form->input('title',array("value"=>$business["Location"]["title"],'label'=>false,"class"=>"form-control","placeholder"=>"Location Name"));?>						
					   </div>
					   <!-- <div class="form-group inputField col-sm-6">
						  <label class="lable-control"><i class="sm-icons"><?php //echo $this->Html->image("sm-icon2.png",array("alt"=>"Email Address","placeholder"=>"Email Address")); ?></i> Email Address</label>
						  <?php //echo $this->Form->input('email',array("value"=>$business["Location"]["email"],'label'=>false,"class"=>"form-control","placeholder"=>"businessname@gmail.com"));?> 						
					   </div>-->
					   <div class="form-group inputField col-sm-6">
						<label class="lable-control"><i class="sm-icons"><?php echo $this->Html->image("facebook-logo.png",array("alt"=>"Facebook")); ?></i> Facebook </label>
						  <?php
						  echo $this->Form->input('facebookid',array("value"=>$business["Location"]["facebookid"],"class"=>"form-control","placeholder"=>"https://www.facebook.com/example/","maxlength"=>200,"label"=>false,"autocomplete"=>"off"));	?>		
					   </div>	
					</div>
					<div class="row">					  				  
					   <div class="form-group inputField col-sm-6">
						  	<label class="lable-control"><i class="sm-icons"><?php echo $this->Html->image("twitter.png",array("alt"=>"Twitter")); ?></i> Twitter </label>
						    <?php echo $this->Form->input('twitterid',array("value"=>$business["Location"]["twitterid"],"class"=>"form-control","placeholder"=>"https://twitter.com/example/","maxlength"=>200,"label"=>false,"autocomplete"=>"off"));	?>	  
					   </div>
					   <div class="form-group inputField col-sm-6">
						  <label class="lable-control"><i class="sm-icons"><?php echo $this->Html->image("instagram.png",array("alt"=>"Instagram")); ?></i> Instagram </label>
						  <?php
						  echo $this->Form->input('instagramid',array("value"=>$business["Location"]["twitterid"],"class"=>"form-control","placeholder"=>"https://www.instagram.com/example/","maxlength"=>200,"label"=>false,"autocomplete"=>"off")); ?>						  						
					   </div>
					</div>
					<div class="row">
					    <div class="form-group inputField col-sm-12">
						   <label class="lable-control"><i class="sm-icons"><?php echo $this->Html->image("sm-icon4.png",array("alt"=>"Discount Venue")); ?></i> Discount Venue</label>
						    <div id="pac-container"><?php echo $this->Form->input('address',array("value"=>$business["Location"]["address"],"id"=>"pac-input","class"=>"form-control",'label'=>false,"placeholder"=>"123, Manchester, United Kingdom"));?>
							</div>
						</div>	
					</div>
					<div class="row">
						 <div class="form-group inputField col-sm-12">
						  <label class="lable-control"><i class="sm-icons"><?php echo $this->Html->image("about-us.png",array("alt"=>"About Me")); ?></i> About Location</label>
						  <?php
						  echo $this->Form->input('about',array("value"=>$business["Location"]["about"],"class"=>"form-control","type"=>"text","placeholder"=>"","rows"=>3,"label"=>false));	?>					  
					   </div>	
					</div>
					<div class="map-location">
						 <div class="map-frame">																			
							 <div id="map" style="height:490px;width:1170px;"></div>
							 <div id="infowindow-content">
							  
							  <span id="place-name"  class="title"></span>
							  <span id="place-address"></span>
							 </div>		
						 </div>
					</div>
					<div class="form-submit form-submit-bottom clearfix">										 
							<button type="submit" class="btn btn-theme">Update Location</button>
							<a href="<?php echo SITE_LINK."view_business?uid=".$business["Location"]["user_id"]?>" class="btn btn-theme">cancel</a>				  
					</div>				
				</div>
				<?php echo $this->Form->end(); ?>
			</div>			
			<!-- End add account -->
	 </div>
</section>
<!--/. My Account -->

<style>
.imageBox{width:520px !important;height:350px !important;}
</style>
<div class="modal fade loginModel themeModel" id="imageModal" tabindex="-1" role="dialog" aria-labelledby="imageModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
       <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title">Select Image</h4>
      </div>
      <div class="modal-body" id="actions">
        <div class="selct-img-voc">
			<div class="img-container" style="width:100%;">
				<img src="<?php echo $tmpImage; ?>" alt="Picture">
			</div>
		  <div class="iviewer" >
			 <ul class="custom-file-upload" class="docs-buttons">
				 <li><label for="upload-file" class="custom-file-upload"><span ><?php echo $this->Html->image("gallery-img.png",array("alt"=>"Location Pic")); ?></span></label> <input id="upload-file" type="file" name="file" accept="image/*"/></li>
				<li data-method="rotate" data-option="-90"><a href="javascript:void(0)" title="Rotate Left"><span id="rotateLeft"><?php echo $this->Html->image("rotate-left.png",array("alt"=>"Location Pic")); ?></span></a></li>
				 <li data-method="rotate" data-option="90"><a href="javascript:void(0)" title="Rotate Right"><span id="rotateRight"><?php echo $this->Html->image("rotate-right.png",array("alt"=>"Location Pic")); ?></span></a></li>
				 <li data-method="zoom" data-option="-0.1"><a href="javascript:void(0)" title="Zoom Out"><span id="zoomIn"><?php echo $this->Html->image("zoom-out.png",array("alt"=>"Location Pic")); ?></span></a></li>
				  <li data-method="zoom" data-option="0.1"><a href="javascript:void(0)" title="Zoom In"><span id="zoomOut"><?php echo $this->Html->image("zoom-in.png",array("alt"=>"Location Pic")); ?></span></a></li>
			 </ul>             
		  </div>		 
		</div>
        
		<div class="iviewer-btn docs-buttons">
			<ul class="list-inline">
			   <li><button class="btn btn-custom" data-dismiss="modal">CLOSE</button></li> 
			   <li><button class="btn btn-custom btn-primary" data-method="getCroppedCanvas" data-option="{ &quot;maxWidth&quot;: 4096, &quot;maxHeight&quot;: 4096 }" id="saveimage">SAVE CHANGES</button></li>
			</ul>
		</div>
	 </div>  	
    </div>
  </div>
</div>
<style>
.img-container {
  /* Never limit the container height here */
  max-width: 100%;
}

.img-container img {
  /* This is important */
  width: 100%;
}
.cropper-bg {background-image:none !important;}
.cropper-modal {background-color:#EEEEEE !important;opacity:0 !important;}

</style>

 <script>
      // This example requires the Places library. Include the libraries=places
      // parameter when you first load the API. For example:
      // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

      function initMap() {
		  		  
        var map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: <?php echo $business["Location"]["latitude"]?>, lng: <?php echo $business["Location"]["longitude"]?>},
          zoom: 13
        });
        var card = document.getElementById('pac-card');
        var input = document.getElementById('pac-input');
        var types = document.getElementById('type-selector');
        var strictBounds = document.getElementById('strict-bounds-selector');

        map.controls[google.maps.ControlPosition.TOP_RIGHT].push(card);

        var autocomplete = new google.maps.places.Autocomplete(input);

        // Bind the map's bounds (viewport) property to the autocomplete object,
        // so that the autocomplete requests use the current map bounds for the
        // bounds option in the request.
        autocomplete.bindTo('bounds', map);
		
        var infowindow = new google.maps.InfoWindow();
        var infowindowContent = document.getElementById('infowindow-content');
        infowindow.setContent(infowindowContent);
        var marker = new google.maps.Marker({
		 position: new google.maps.LatLng( <?php echo $business["Location"]["latitude"]?>,<?php echo $business["Location"]["longitude"]?>), 
          map: map   
        });

          autocomplete.addListener('place_changed', function() {
          infowindow.close();
          marker.setVisible(false);
          var place = autocomplete.getPlace();
          if (!place.geometry) {
            // User entered the name of a Place that was not suggested and
            // pressed the Enter key, or the Place Details request failed.
            //window.alert("No details available for input: '" + place.name + "'");
            return;
          }

          // If the place has a geometry, then present it on a map.
          if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
            var address =place.name;
            var geocoder = new google.maps.Geocoder();
            geocoder.geocode({ 'address': address}, function (results, status) {
                        if (status == google.maps.GeocoderStatus.OK) {
							var latitude = place.geometry.location.lat();
                            var longitude = place.geometry.location.lng();
                            $('#LocationLatitude').val(latitude);
                            $('#LocationLongitude').val(longitude);
                        } else {
                           
                        }
                    });
            
          } else {
			 
            map.setCenter(place.geometry.location);
            map.setZoom(17);  // Why 17? Because it looks good.
          }
          marker.setPosition(place.geometry.location);
          marker.setVisible(true);

          var address = '';
          if (place.address_components) {
            address = [
              (place.address_components[0] && place.address_components[0].short_name || ''),
              (place.address_components[1] && place.address_components[1].short_name || ''),
              (place.address_components[2] && place.address_components[2].short_name || '')
            ].join(' ');
          }       
          infowindowContent.children['place-icon'].src = place.icon;
          infowindowContent.children['place-name'].textContent = place.name;
          infowindowContent.children['place-address'].textContent = address;         
          	
			infowindow.open(map, marker);
        }    
        );	
        
		map.setCenter(new google.maps.LatLng( <?php echo $business["Location"]["latitude"]?>,<?php echo $business["Location"]["longitude"]?>));		
        map.setZoom(17);  
        
         if(<?php echo $business['Location']['latitude']?>!=""){
          marker.addListener('click', function() {
			infowindow = new google.maps.InfoWindow({
			content: "<?php echo $business['Location']['address'];?>"
			});
			infowindow.open(map, marker);
			});
		}
	}
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDcBcY5ObGmiZ6TETlFS1cEOi5ttvBRnuw&libraries=places&callback=initMap" async defer></script>
<div class="modal in fade themeModel" tabindex="-1" role="dialog" id="deletebusienssbox">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Location Deletion Confirmation</h5>       
      </div>
      <div class="modal-body">
        <p>Are you sure you want to delete business permanently.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary btn-theme" data-dismiss="modal">No</button>
        <button type="button" class="btn btn-secondary btn-theme confirmdeletebusiness">Yes</button>
      </div>
    </div>
  </div>
</div>  
