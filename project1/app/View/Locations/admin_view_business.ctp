 <div class="cont-right">
      <div class="cont-right-innr">
		 <div class="main-hd-in">
			  <div class="row">
				<div class="col-sm-12">
				  <h2 class="title-das">Manage Locations</h2>				  
					<div class="show-record">						
					   <span> Show Records </span> 
					   <?php echo $this->Form->create("BusinessSearch",array("div"=>false,"type"=>"post"));				
						echo $this->Form->input('records',array("id"=>"records",'label'=>'',"options"=>$records,'class'=>'form-control',"selected"=>$limit));
						echo $this->Form->end(); ?>
					</div>
				</div>
			  </div>
			</div>
			<?php echo $this->Form->create("Location",array("div"=>false,)); ?>
			<div class="srch" style="margin-left:30px;">
				<?php echo $this->element("admins/common",array("place"=>'Search ',"flag"=>false,"pageheader"=>'',"buttontitle"=>'no',"listflag"=>"no","action"=>'no',"selflag"=>true,"database"=>true)); ?>
				
			</div>
			<div class="manage-marchant">
			  <div class="table-responsive">
				<table class="Marchant-table_vouch table table-bordered " width="100%">
				  <tr>		
					<th><?php echo $this->Form->input("check",array("label"=>false,"div"=>false,"id"=>'checkall',"type"=>'checkbox')); ?></th>
					<th> Sr no </th>
					<!--<th> <?php //echo $this->Paginator->sort('id','ID'); ?> </th>-->
					<th><?php echo "Logo"; ?></th>
					<th><?php echo $this->Paginator->sort('Business.tile',"Location Address"); ?></th>					
					<th><?php echo $this->Paginator->sort('UserDetail.name','Merchant Name'); ?></th>					
					<th> <?php echo $this->Paginator->sort('created','Created'); ?> </th>	
					<th><?php echo $this->Paginator->sort('is_paid',"STATUS"); ?> </th>
					<th> ACTION </th>
				  </tr>
				  <?php echo $this->Form->input("userid",array("type"=>'hidden',"value"=>$uid)); ?>
				  <?php foreach ($businesses as $business): ?>
				  <tr>
					<td><?php echo $this->Form->input("id.".$business['Location']['id'],array("class"=>'chk',"value"=>$business['Location']['id'],"type"=>'checkbox',"div"=>false,"label"=>false)); ?>
					<?php echo $this->Form->input("status.".$business['Location']['id'],array("type"=>'hidden',"value"=>($business['Location']['is_paid'] == 1?0:1))); ?></td>
					<!--td><?php //echo $sno;?></td-->
					
					<td><?php echo $sno; ?></td>
					<?php if(empty($business['Location']['image']) || !file_exists(WWW_ROOT."/img/profile/".$business['Location']['image'])){$imagebuiness=SITE_LINK."img/default_new.png";}else{$imagebuiness="profile/".$business['Location']['image'];}?>
					<td><?php echo $this->Html->image($imagebuiness,array("class"=>"marchant-imgs")); ?>&nbsp;</td>				
					<td><a href="<?php echo SITE_LINK."view-vouchers?merchant=".$business['UserDetail']['user_id']."&busid=".$business['Location']['id']; ?>"><?php echo $business['Location']['address'];?></a></td>
					<td><a href="<?php echo SITE_LINK."edit-merchants/".$business['UserDetail']['user_id']; ?>"><?php echo h($business['UserDetail']['business']); ?></a></td>
					<td><?php $joined =  date_create(h($business['Location']['created'])); echo date_format($joined,"j M, Y");  ?>&nbsp;</td>
					<td><?php echo h(($business['Location']['is_paid'] == 1)?'Paid':'Not Paid'); ?>&nbsp;</td>						
					<td><a href="<?php echo SITE_LINK.'edit_business?bid='.$business['Location']['id']?>"><i class="fa fa-pencil"></i></a>
					<?php echo $this->html->link($this->Html->tag('i', '', array('class' => 'glyphicon glyphicon-trash')). " ",   array('action' => 'delete', $business['Location']['id']),array('escape'=>false),__('Are you sure you want to delete Location?', ''));?>
					</td>
				  </tr>
				  <?php $sno++;endforeach; ?>
				</table>
			  </div>		 
			     
			<div class="pagination-main">
				<div class="paging pagination">
				<?php echo $this->Paginator->prev('<i class="fa fa-caret-left"></i>', array('escape' => false), null, array('class' => 'fa prev disabled'));
				
				echo $this->Paginator->numbers(array('separator' => ''));
				echo $this->Paginator->next('<i class="fa fa-caret-right "></i>', array('escape' => false), null, array('class'	=> 'fa next disabled'));
				?>
			   </div>  
			</div>  
        </div>
		<?php echo $this->Form->end(); ?>
	</div>
</div>
<script>
$(document).ready(function () {
	$("#records").change(function(e) { 
	var val=$('#records').val();	
	var uid=$('#LocationUserid').val();	
	if(uid!="")
	{
		var uid=$('#LocationUserid').val();	
		window.location.replace(SITE_LINK+"view_business?uid="+uid+"&records="+val);	
	}
	else
	{
				window.location.replace(SITE_LINK+"view_business?records="+val);
	}
	
	});
});	

</script>

