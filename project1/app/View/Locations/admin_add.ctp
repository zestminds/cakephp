<script>
	$(document).ready(function () {
    $('#LocationAdminAddForm').validate({ // initialize the plugin
        rules: {			
			 
			  "data[Location][title]": {
				required: true				
			  }		
			},
			// Specify validation error messages
			messages: {			
				"data[Location][title]": {
					required: "Please enter a location"			
				}
			},
			// Make sure the form is submitted to the destination defined
			// in the "action" attribute of the form when valid
			submitHandler: function(form) {
			  form.submit();
			}
		  });		
});
</script>
<div class="cont-right">
   <div class="cont-right-innr">
	    <div class="main-hd-in">
          <div class="row">
            <div class="col-sm-12">
              <h2 class="title-das"> Add Location</h2>              
            </div>
          </div>
        </div>
		<div class="manage-marchant">
				<?php echo $this->Form->create('Location'); ?>
				<fieldset>
					
				<?php
					echo $this->Form->input('title',  array('label' => 'Location'));		
					echo $this->Form->input('is_active');
				?>
				</fieldset>
				<div style="margin-left:18px"><button type="submit" class="btn btn-theme" id="save" name="save">SAVE</button>
				<a href="<?php echo SITE_LINK ?>manage-locations" class="btn btn-theme">cancel</a>
				</div>
		</div>
	 </div>		
</div>		

