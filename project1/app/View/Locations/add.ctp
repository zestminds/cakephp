<script type="text/javascript">
	var avatar = "<?php echo (empty($user['UserDetail']['image']) || !file_exists(WWW_ROOT."/img/profile/".$user['UserDetail']['image'])) ?  SITE_LINK.'/img/default_new.png' : SITE_LINK.'img/profile/'.$user['UserDetail']['image']; ?>";
</script>

<?php
$tmpImageprofile=(empty($user['UserDetail']['image']) || !file_exists(WWW_ROOT."/img/profile/".$user['UserDetail']['image'])) ?  SITE_LINK.'/img/default_new.png' : SITE_LINK.'img/profile/'.$user['UserDetail']['image'];

//~ $tmpImage1 = $tmpImage2 =$tmpImage3 =$tmpImage4 =$tmpImage5 = $tmpImage6 =(empty($user['UserDetail']['image']) || !file_exists(WWW_ROOT."/img/profile/".$user['UserDetail']['image'])) ?  SITE_LINK.'/img/location-here.png.png' : SITE_LINK.'img/profile/'.$user['UserDetail']['image'];
$tmpImage1 = $tmpImage2 =$tmpImage3 =$tmpImage4 =$tmpImage5 = $tmpImage6 =  SITE_LINK.'/img/location-here.png';

//echo $tmpImage = file_get_contents($tmpImage); die;
 ?>
 <section class="inner-banner topheader">
   <div class="container">
		<div class="banner-cont">
		   <ul class="menu-banner">
				<li><a href="<?php echo SITE_LINK;?>drafted-vouchers" class="<?php echo ($this->params['controller'] == 'vouchers' && $this->params['action'] == 'index' )? 'active':''?>" title="My Vouchers">My Vouchers</a></li>   
				<li><a href="<?php echo SITE_LINK;?>locations" class="<?php echo ($this->params['controller'] == 'Locations' && $this->params['action'] == 'index' )? 'active':''?>" title="My Locations">My Locations</a></li>   
		   </ul>
		</div>
   </div>
</section>
    <!--/. banner sec -->				

<section class="my-account-desc">
	 <div class="container">		
		<!-- Add account -->
		<?php echo $this->Form->create('Location',array("type"=>"file","class"=>"themeForm")); ?> 
		
		 <div class="my-account-pro clearfix">	
				
		
		<div class="location_image_preview">
			<div class="form-group inputField col-sm-12">
					<div  class="txt_demo_location_image" style="display:none">You Location Images will be here. Click on "Add Location Images" to add images.</div>
					<div class="col-sm-2 location preview1">
						<div class="image-preview">
							<div class="location_image img_upload1_image"  style="display:none">
									<a href="javascript:void(0);" class="delete_location" value="1" action="" id="image_1"> <?php echo $this->Html->image('delete_comment.png', array("alt"=>"Delete Images"));?></a>
							</div>									
							<div class="upload-drop-zone" id="drop-zone1"> 
							   <div class="upload-file"> 
								   <?php echo $this->Html->image($tmpImage1, array("alt"=>"Voucher Pic","id"=>"img_upload1","class"=>"img-upload")); ?>
								   <?php echo $this->Form->input("LocationImages.img_upload1_value",array("id"=>'img_upload1_value',"label"=>false,"type"=>"hidden")); ?>
							   </div>	
							</div> 
						</div>  
					</div>					
					<div class="col-sm-2 location preview2" style="display:none">	
						<div class="image-preview">
							<div class="location_image img_upload2_image"  style="display:none">
									<a href="javascript:void(0);" class="delete_location" value="2" action="" id="image_2"> <?php echo $this->Html->image('delete_comment.png', array("alt"=>"Delete Images"));?></a>
							</div>								
							<div class="upload-drop-zone" id="drop-zone2">						  
							   <div class="upload-file"> 
								   <?php echo $this->Html->image($tmpImage2, array("alt"=>"Voucher Pic","id"=>"img_upload2","class"=>"img-upload")); ?>
								   <?php echo $this->Form->input("LocationImages.img_upload2_value",array("id"=>'img_upload2_value',"label"=>false,"type"=>"hidden")); ?>
							   </div>	
						    </div>
					   </div> 							
					</div>

					<div class="col-sm-2 location preview3" style="display:none">		
						<div class="image-preview">
							<div class="location_image img_upload3_image"  style="display:none">
									<a href="javascript:void(0);" class="delete_location" value="3" action="" id="image_3"> <?php echo $this->Html->image('delete_comment.png', array("alt"=>"Delete Images"));?></a>
							</div>			
						  <div class="upload-drop-zone" id="drop-zone3"> 							  
							   <div class="upload-file"> 
								   <?php echo $this->Html->image($tmpImage3, array("alt"=>"Voucher Pic","id"=>"img_upload3","class"=>"img-upload")); ?>
								   <?php echo $this->Form->input("LocationImages.img_upload3_value",array("id"=>'img_upload3_value',"label"=>false,"type"=>"hidden")); ?>
							   </div>	
						   </div>
						 </div> 	 							
					</div>	
					<div class="col-sm-2 location preview4" style="display:none">		
						<div class="image-preview">
						<div class="location_image img_upload4_image"  style="display:none">
									<a href="javascript:void(0);" class="delete_location" value="4" action="" id="image_5"> <?php echo $this->Html->image('delete_comment.png', array("alt"=>"Delete Images"));?></a>
						</div>	
						  <div class="upload-drop-zone" id="drop-zone4"> 							  
							   <div class="upload-file"> 
								   <?php echo $this->Html->image($tmpImage4, array("alt"=>"Voucher Pic","id"=>"img_upload4","class"=>"img-upload")); ?>
								   <?php echo $this->Form->input("LocationImages.img_upload4_value",array("id"=>'img_upload4_value',"label"=>false,"type"=>"hidden")); ?>
							   </div>	
						   </div>
						 </div> 	 							
					</div>	
					<div class="col-sm-2 location preview5" style="display:none">		
						<div class="image-preview">
							<div class="location_image img_upload5_image"  style="display:none">
									<a href="javascript:void(0);" class="delete_location" value="5" action="" id="image_5"> <?php echo $this->Html->image('delete_comment.png', array("alt"=>"Delete Images"));?></a>
							</div>			
						  <div class="upload-drop-zone" id="drop-zone5"> 							  
							   <div class="upload-file"> 
								   <?php echo $this->Html->image($tmpImage5, array("alt"=>"Voucher Pic","id"=>"img_upload5","class"=>"img-upload")); ?>
								   <?php echo $this->Form->input("LocationImages.img_upload5_value",array("id"=>'img_upload5_value',"label"=>false,"type"=>"hidden")); ?>
							   </div>	
						   </div>
						 </div> 	 							
					</div>	
					<div class="col-sm-2 location preview6" style="display:none">		
						<div class="image-preview">
							<div class="location_image img_upload6_image"  style="display:none">
									<a href="javascript:void(0);" class="delete_location" value="6" action="" id="image_6"> <?php echo $this->Html->image('delete_comment.png', array("alt"=>"Delete Images"));?></a>
							</div>			
							<div class="upload-drop-zone" id="drop-zone6"> 							  
							   <div class="upload-file"> 
								   <?php echo $this->Html->image($tmpImage6, array("alt"=>"Voucher Pic","id"=>"img_upload6","class"=>"img-upload")); ?>
								   <?php echo $this->Form->input("LocationImages.img_upload6_value",array("id"=>'img_upload6_value',"label"=>false,"type"=>"hidden")); ?>
							   </div>	
						   </div>
						</div> 	 							
					</div>	
			</div>	
			</div>	
			<div class="form-group inputField col-sm-12">
					<div class="my-account-pro-right">				
							<input id="addmore" class="add_location_image" type="button" value="Add Location Images"/>
					</div>
			</div>
					 
			<div id="addbusiness">				
				<div class="account-profile box-shadow">	
				
				 
					<div class="account-detals">
							 <?php echo $this->Form->hidden('image',array('value'=>$user['UserDetail']['image']));?>
							 <?php echo $this->Form->hidden('latitude',array("value"=>$latitude)); echo $this->Form->hidden('longitude',array("value"=>$longitude));?>
							<div class="row">
								<div class="col-sm-12 vochr-right">
								<div class="timeboxsetting" style="float:right;">						
									<!--input type="checkbox" name="data[Voucher][online_status]" id="statusToggle" data-toggle="toggle" data-on="Online" data-off="In Store" data-onstyle="success" data-offstyle="danger"-->
									<div class="instore">Show on Rave</div>
									<label class="switch switch_btn">		
																				  
										 <?php $optionsOnOff = array("on"=>"on","off"=>"off"); echo $this->Form->checkbox('show_on_rave', array('id'=>'statusToggle','value'=>0,"checked"=>false,'label'=>'','options'=>$optionsOnOff)) ;
										 ?>

										  <span class="slider round"></span>
										  <!-- <span class="absolute-no">NO</span> -->
									</label>
								</div>	
								</div>	
							</div>
							<div class="row">
							   <div class="form-group inputField col-sm-6">
								  <label class="lable-control"><i class="sm-icons"><?php echo $this->Html->image("sm-icon1.png",array("alt"=>"Full Name","placeholder"=>"Location Name")); ?></i> Location Name</label>
								  <?php echo $this->Form->input('title',array("value"=>$user["UserDetail"]["business"],'label'=>false,"class"=>"form-control","placeholder"=>"Location Name"));?>						
							   </div>					
							   <div class="form-group inputField col-sm-6">
								<label class="lable-control"><i class="sm-icons"><?php echo $this->Html->image("facebook-logo.png",array("alt"=>"Facebook")); ?></i> Facebook </label>
								  <?php
								  echo $this->Form->input('facebookid',array("value"=>$user["UserDetail"]["facebookid"],"class"=>"form-control","placeholder"=>"https://www.facebook.com/example/","maxlength"=>200,"label"=>false,"autocomplete"=>"off"));	?>		
							   </div>	
							</div>
							<div class="row">					  				  
							   <div class="form-group inputField col-sm-6">
									<label class="lable-control"><i class="sm-icons"><?php echo $this->Html->image("twitter.png",array("alt"=>"Twitter")); ?></i> Twitter </label>
									<?php echo $this->Form->input('twitterid',array("value"=>$user["UserDetail"]["twitterid"],"class"=>"form-control","placeholder"=>"https://twitter.com/example/","maxlength"=>200,"label"=>false,"autocomplete"=>"off"));	?>	  
							   </div>
							   <div class="form-group inputField col-sm-6">
								  <label class="lable-control"><i class="sm-icons"><?php echo $this->Html->image("instagram.png",array("alt"=>"Instagram")); ?></i> Instagram </label>
								  <?php
								  echo $this->Form->input('instagramid',array("value"=>$user["UserDetail"]["twitterid"],"class"=>"form-control","placeholder"=>"https://www.instagram.com/example/","maxlength"=>200,"label"=>false,"autocomplete"=>"off")); ?>						  						
							   </div>
							</div>
							<div class="row">
								<div class="form-group inputField col-sm-12">
								   <label class="lable-control"><i class="sm-icons"><?php echo $this->Html->image("sm-icon4.png",array("alt"=>"Discount Venue")); ?></i> Discount Venue</label>
									<div id="pac-container"><?php echo $this->Form->input('address',array("id"=>"pac-input","class"=>"form-control",'label'=>false,"placeholder"=>"Your Company Name, Your Street, Your Town"));?>
									</div>
								</div>						
							</div>
							<div class="row">
									<div class="col-sm-12">
										<i class="sm-icons"><?php echo $this->Html->image("cal-icon.png",array("alt"=>"Time")); ?></i>Set Opening and closing Time
									</div>
							</div>		
							<div class="row">
								<div class="form-group inputField_time col-sm-6">
									<div class="col-sm-4">
									  <div class="weekday_text">Monday</div>	
									</div>  
									<div class="col-sm-4">								
										  <div class='input_date_grp input-group date datetimepicker2'>
										  <?php echo $this->Form->input('OpeningTiming.starttime_1',array("label"=>false,"type"=>"text",'autofill'=>'false',"class"=>"form-control"));?>                
										  <span class="input-group-addon">
											<span class="fa fa-clock-o"></span>
										  </span>
										</div>
									</div>
									<div class="col-sm-4">
									  <div class='input_date_grp input-group date datetimepicker2'>
										  <?php echo $this->Form->input('OpeningTiming.endtime_1',array("label"=>false,"type"=>"text",'autofill'=>'false',"class"=>"form-control"));?>                
										  <span class="input-group-addon">
											<span class="fa fa-clock-o"></span>
										  </span>
										</div>
									</div>
								</div>
								<div class="form-group inputField_time col-sm-6">
									<div class="col-sm-4">
									  <div class="weekday_text">Friday</div>	
									</div>  
									<div class="col-sm-4">								
										  <div class='input_date_grp input-group date datetimepicker2'>
										  <?php echo $this->Form->input('OpeningTiming.starttime_5',array("label"=>false,"type"=>"text",'autofill'=>'false',"class"=>"form-control"));?>                
										  <span class="input-group-addon">
											<span class="fa fa-clock-o"></span>
										  </span>
										</div>
									</div>
									<div class="col-sm-4">
									  <div class='input_date_grp input-group date datetimepicker2'>
										  <?php echo $this->Form->input('OpeningTiming.endtime_5',array("label"=>false,"type"=>"text",'autofill'=>'false',"class"=>"form-control"));?>                
										  <span class="input-group-addon">
											<span class="fa fa-clock-o"></span>
										  </span>
										</div>
									</div>
								</div>		
							</div>
							<div class="row">
								<div class="form-group inputField_time col-sm-6">
									<div class="col-sm-4">
									  <div class="weekday_text">Tuesday</div>	
									</div>  
									<div class="col-sm-4">								
										  <div class='input_date_grp input-group date datetimepicker2'>
										  <?php echo $this->Form->input('OpeningTiming.starttime_2',array("label"=>false,"type"=>"text",'autofill'=>'false',"class"=>"form-control"));?>                
										  <span class="input-group-addon">
											<span class="fa fa-clock-o"></span>
										  </span>
										</div>
									</div>
									<div class="col-sm-4">
									  <div class='input_date_grp input-group date datetimepicker2'>
										  <?php echo $this->Form->input('OpeningTiming.endtime_2',array("label"=>false,"type"=>"text",'autofill'=>'false',"class"=>"form-control"));?>                
										  <span class="input-group-addon">
											<span class="fa fa-clock-o"></span>
										  </span>
										</div>
									</div>
								</div>
								<div class="form-group inputField_time col-sm-6">
									<div class="col-sm-4">
									  <div class="weekday_text">Saturday</div>	
									</div>  
									<div class="col-sm-4">								
										  <div class='input_date_grp input-group date datetimepicker2'>
										  <?php echo $this->Form->input('OpeningTiming.starttime_6',array("label"=>false,"type"=>"text",'autofill'=>'false',"class"=>"form-control"));?>                
										  <span class="input-group-addon">
											<span class="fa fa-clock-o"></span>
										  </span>
										</div>
									</div>
									<div class="col-sm-4">
									  <div class='input_date_grp input-group date datetimepicker2'>
										  <?php echo $this->Form->input('OpeningTiming.endtime_6',array("label"=>false,"type"=>"text",'autofill'=>'false',"class"=>"form-control"));?>                
										  <span class="input-group-addon">
											<span class="fa fa-clock-o"></span>
										  </span>
										</div>
									</div>
								</div>		
							</div>
							<div class="row">
								<div class="form-group inputField_time col-sm-6">
									<div class="col-sm-4">
									  <div class="weekday_text">Wednesday</div>	
									</div>  
									<div class="col-sm-4">								
										  <div class='input_date_grp input-group date datetimepicker2'>
										  <?php echo $this->Form->input('OpeningTiming.starttime_3',array("label"=>false,"type"=>"text",'autofill'=>'false',"class"=>"form-control"));?>                
										  <span class="input-group-addon">
											<span class="fa fa-clock-o"></span>
										  </span>
										</div>
									</div>
									<div class="col-sm-4">
									  <div class='input_date_grp input-group date datetimepicker2'>
										  <?php echo $this->Form->input('OpeningTiming.endtime_3',array("label"=>false,"type"=>"text",'autofill'=>'false',"class"=>"form-control"));?>                
										  <span class="input-group-addon">
											<span class="fa fa-clock-o"></span>
										  </span>
										</div>
									</div>
								</div>
								<div class="form-group inputField_time col-sm-6">
									<div class="col-sm-4">
									  <div class="weekday_text">Sunday</div>	
									</div>  
									<div class="col-sm-4">								
										  <div class='input_date_grp input-group date datetimepicker2'>
										  <?php echo $this->Form->input('OpeningTiming.starttime_7',array("label"=>false,"type"=>"text",'autofill'=>'false',"class"=>"form-control"));?>                
										  <span class="input-group-addon">
											<span class="fa fa-clock-o"></span>
										  </span>
										</div>
									</div>
									<div class="col-sm-4">
									  <div class='input_date_grp input-group date datetimepicker2'>
										  <?php echo $this->Form->input('OpeningTiming.endtime_7',array("label"=>false,"type"=>"text",'autofill'=>'false',"class"=>"form-control"));?>                
										  <span class="input-group-addon">
											<span class="fa fa-clock-o"></span>
										  </span>
										</div>
									</div>
								</div>		
							</div>
							<div class="row">
								<div class="form-group inputField_time col-sm-6">
									<div class="col-sm-4">
									  <div class="weekday_text">Thursday</div>	
									</div>  
									<div class="col-sm-4">								
										  <div class='input_date_grp input-group date datetimepicker2'>
										  <?php echo $this->Form->input('OpeningTiming.starttime_4',array("label"=>false,"type"=>"text",'autofill'=>'false',"class"=>"form-control"));?>                
										  <span class="input-group-addon">
											<span class="fa fa-clock-o"></span>
										  </span>
										</div>
									</div>
									<div class="col-sm-4">
									  <div class='input_date_grp input-group date datetimepicker2'>
										  <?php echo $this->Form->input('OpeningTiming.endtime_4',array("label"=>false,"type"=>"text",'autofill'=>'false',"class"=>"form-control"));?>                
										  <span class="input-group-addon">
											<span class="fa fa-clock-o"></span>
										  </span>
										</div>
									</div>
								</div>							
							</div>
							<div class="row">
								 <div class="form-group inputField col-sm-12">
								  <label class="lable-control"><i class="sm-icons"><?php echo $this->Html->image("about-us.png",array("alt"=>"About Me")); ?></i> About Location</label>
								  <?php
								  echo $this->Form->input('about',array("value"=>$user["UserDetail"]["aboutme"],"class"=>"form-control","type"=>"text","placeholder"=>"","rows"=>4,"label"=>false));	?>					  
							   </div>	
							</div>
							<div class="map-location">
								 <div class="map-frame">																			
									 <div id="map" style="height:490px;width:1170px;"></div>
									 <div id="infowindow-content">							 
									  <span id="place-name"  class="title"></span>
									  <span id="place-address"></span>
									 </div>		
								 </div>
							</div>
							<div class="form-submit form-submit-bottom clearfix">		
									<button type="submit" class="btn btn-theme">Save</button>
							</div>				
						</div>				
				</div>
						
			</div>
		<?php echo $this->Form->end(); ?>		
	 </div>
</section>
<!--/. My Account -->

<style>
.imageBox{width:520px !important;height:350px !important;}
</style>

<div class="modal fade loginModel themeModel" id="imageModal" tabindex="-1" role="dialog" aria-labelledby="imageModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
       <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title">Select Image </h4>
      </div>
      <div class="modal-body actions">
        <div class="selct-img-voc">
		  <div class="img-container" style="width:100%;">
				<img src="<?php echo $tmpImage1; ?>" alt="Picture">
				<input hidden class="replace-image" value="img_upload1" />
			</div>
		  <div class="iviewer" >
			 <ul class="custom-file-upload" class="docs-buttons">
				 <li><label for="upload-file" class="custom-file-upload"><span ><?php echo $this->Html->image("gallery-img.png",array("alt"=>"Voucher Pic")); ?></span></label> <input id="upload-file" class="upload-file" type="file" name="file" accept="image/*"/></li>
				<li data-method="rotate" data-option="-90"><a href="javascript:void(0)" title="Rotate Left"><span id="rotateLeft"><?php echo $this->Html->image("rotate-left.png",array("alt"=>"Voucher Pic")); ?></span></a></li>
				 <li data-method="rotate" data-option="90"><a href="javascript:void(0)" title="Rotate Right"><span id="rotateRight"><?php echo $this->Html->image("rotate-right.png",array("alt"=>"Voucher Pic")); ?></span></a></li>
				 <li data-method="zoom" data-option="-0.1"><a href="javascript:void(0)" title="Zoom Out"><span id="zoomIn"><?php echo $this->Html->image("zoom-out.png",array("alt"=>"Voucher Pic")); ?></span></a></li>
				  <li data-method="zoom" data-option="0.1"><a href="javascript:void(0)" title="Zoom In"><span id="zoomOut"><?php echo $this->Html->image("zoom-in.png",array("alt"=>"Voucher Pic")); ?></span></a></li>
			 </ul>             
		  </div>
		</div>
        
		<div class="iviewer-btn">
			<ul class="list-inline">
			   <li><button class="btn btn-custom" data-dismiss="modal">CLOSE</button></li> 
			   <li><button class="btn btn-custom btn-primary" data-method="getCroppedCanvas" data-option="{ &quot;maxWidth&quot;: 4096, &quot;maxHeight&quot;: 4096 }" id="saveimage">SAVE CHANGES</button></li>
			</ul>
		</div>
	 </div>  	
    </div>
  </div>
</div>



<div class="modal fade loginModel themeModel" id="imageModal2" tabindex="-1" role="dialog" aria-labelledby="imageModal2Label" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
       <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title">Select Image </h4>
      </div>
      <div class="modal-body actions">
        <div class="selct-img-voc">
		  <div class="img-container" style="width:100%;">
				<img src="<?php echo $tmpImage2; ?>" alt="Picture">
				<!-- Hidden field contains the id of the field it's going to replace -->
				<input hidden class="replace-image" value="img_upload2" />
			</div>
		  <div class="iviewer" >
			 <ul class="custom-file-upload" class="docs-buttons">
				 <li><label for="upload-file" class="custom-file-upload"><span ><?php echo $this->Html->image("gallery-img.png",array("alt"=>"Voucher Pic")); ?></span></label> <input id="upload-file-banner"  class="upload-file" type="file" name="file" accept="image/*"/></li>
				<li data-method="rotate" data-option="-90"><a href="javascript:void(0)" title="Rotate Left"><span id="rotateLeft"><?php echo $this->Html->image("rotate-left.png",array("alt"=>"Voucher Pic")); ?></span></a></li>
				 <li data-method="rotate" data-option="90"><a href="javascript:void(0)" title="Rotate Right"><span id="rotateRight"><?php echo $this->Html->image("rotate-right.png",array("alt"=>"Voucher Pic")); ?></span></a></li>
				 <li data-method="zoom" data-option="-0.1"><a href="javascript:void(0)" title="Zoom Out"><span id="zoomIn"><?php echo $this->Html->image("zoom-out.png",array("alt"=>"Voucher Pic")); ?></span></a></li>
				  <li data-method="zoom" data-option="0.1"><a href="javascript:void(0)" title="Zoom In"><span id="zoomOut"><?php echo $this->Html->image("zoom-in.png",array("alt"=>"Voucher Pic")); ?></span></a></li>
			 </ul>             
		  </div>
		</div>
        
		<div class="iviewer-btn">
			<ul class="list-inline">
			   <li><button class="btn btn-custom" data-dismiss="modal">CLOSE</button></li> 
			   <li><button class="btn btn-custom btn-primary" data-method="getCroppedCanvas" data-option="{ &quot;maxWidth&quot;: 4096, &quot;maxHeight&quot;: 4096 }" id="saveimage">SAVE CHANGES</button></li>
			</ul>
		</div>
	 </div>  	
    </div>
  </div>
</div>
<div class="modal fade loginModel themeModel" id="imageModal3" tabindex="-1" role="dialog" aria-labelledby="imageModal3Label" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
       <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title">Select Image </h4>
      </div>
      <div class="modal-body actions">
        <div class="selct-img-voc">
		  <div class="img-container" style="width:100%;">
				<img src="<?php echo $tmpImage3; ?>" alt="Picture">
				<!-- Hidden field contains the id of the field it's going to replace -->
				<input hidden class="replace-image" value="img_upload3" />
			</div>
		  <div class="iviewer" >
			 <ul class="custom-file-upload" class="docs-buttons">
				 <li><label for="upload-file" class="custom-file-upload"><span ><?php echo $this->Html->image("gallery-img.png",array("alt"=>"Voucher Pic")); ?></span></label> <input id="upload-file-banner3"  class="upload-file" type="file" name="file" accept="image/*"/></li> 
				<li data-method="rotate" data-option="-90"><a href="javascript:void(0)" title="Rotate Left"><span id="rotateLeft"><?php echo $this->Html->image("rotate-left.png",array("alt"=>"Voucher Pic")); ?></span></a></li>
				 <li data-method="rotate" data-option="90"><a href="javascript:void(0)" title="Rotate Right"><span id="rotateRight"><?php echo $this->Html->image("rotate-right.png",array("alt"=>"Voucher Pic")); ?></span></a></li>
				 <li data-method="zoom" data-option="-0.1"><a href="javascript:void(0)" title="Zoom Out"><span id="zoomIn"><?php echo $this->Html->image("zoom-out.png",array("alt"=>"Voucher Pic")); ?></span></a></li>
				  <li data-method="zoom" data-option="0.1"><a href="javascript:void(0)" title="Zoom In"><span id="zoomOut"><?php echo $this->Html->image("zoom-in.png",array("alt"=>"Voucher Pic")); ?></span></a></li>
			 </ul>             
		  </div>
		</div>
        
		<div class="iviewer-btn">
			<ul class="list-inline">
			   <li><button class="btn btn-custom" data-dismiss="modal">CLOSE</button></li> 
			   <li><button class="btn btn-custom btn-primary" data-method="getCroppedCanvas" data-option="{ &quot;maxWidth&quot;: 4096, &quot;maxHeight&quot;: 4096 }" id="saveimage">SAVE CHANGES</button></li>
			</ul>
		</div>
	 </div>  	
    </div>
  </div>
</div>
<div class="modal fade loginModel themeModel" id="imageModal4" tabindex="-1" role="dialog" aria-labelledby="imageModal4Label" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
       <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title">Select Image </h4>
      </div>
      <div class="modal-body actions">
        <div class="selct-img-voc">
		  <div class="img-container" style="width:100%;">
				<img src="<?php echo $tmpImage4; ?>" alt="Picture">
				<!-- Hidden field contains the id of the field it's going to replace -->
				<input hidden class="replace-image" value="img_upload4" />
			</div>
		  <div class="iviewer" >
			 <ul class="custom-file-upload" class="docs-buttons">
				 <li><label for="upload-file" class="custom-file-upload"><span ><?php echo $this->Html->image("gallery-img.png",array("alt"=>"Voucher Pic")); ?></span></label> <input id="upload-file-banner4"  class="upload-file" type="file" name="file" accept="image/*"/></li> 
				<li data-method="rotate" data-option="-90"><a href="javascript:void(0)" title="Rotate Left"><span id="rotateLeft"><?php echo $this->Html->image("rotate-left.png",array("alt"=>"Voucher Pic")); ?></span></a></li>
				 <li data-method="rotate" data-option="90"><a href="javascript:void(0)" title="Rotate Right"><span id="rotateRight"><?php echo $this->Html->image("rotate-right.png",array("alt"=>"Voucher Pic")); ?></span></a></li>
				 <li data-method="zoom" data-option="-0.1"><a href="javascript:void(0)" title="Zoom Out"><span id="zoomIn"><?php echo $this->Html->image("zoom-out.png",array("alt"=>"Voucher Pic")); ?></span></a></li>
				  <li data-method="zoom" data-option="0.1"><a href="javascript:void(0)" title="Zoom In"><span id="zoomOut"><?php echo $this->Html->image("zoom-in.png",array("alt"=>"Voucher Pic")); ?></span></a></li>
			 </ul>             
		  </div>
		</div>
        
		<div class="iviewer-btn">
			<ul class="list-inline">
			   <li><button class="btn btn-custom" data-dismiss="modal">CLOSE</button></li> 
			   <li><button class="btn btn-custom btn-primary" data-method="getCroppedCanvas" data-option="{ &quot;maxWidth&quot;: 4096, &quot;maxHeight&quot;: 4096 }" id="saveimage">SAVE CHANGES</button></li>
			</ul>
		</div>
	 </div>  	
    </div>
  </div>
</div>

<div class="modal fade loginModel themeModel" id="imageModal5" tabindex="-1" role="dialog" aria-labelledby="imageModal5Label" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
       <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title">Select Image </h4>
      </div>
      <div class="modal-body actions">
        <div class="selct-img-voc">
		  <div class="img-container" style="width:100%;">
				<img src="<?php echo $tmpImage5; ?>" alt="Picture">
				<!-- Hidden field contains the id of the field it's going to replace -->
				<input hidden class="replace-image" value="img_upload5" />
			</div>
		  <div class="iviewer" >
			 <ul class="custom-file-upload" class="docs-buttons">
				 <li><label for="upload-file" class="custom-file-upload"><span ><?php echo $this->Html->image("gallery-img.png",array("alt"=>"Voucher Pic")); ?></span></label> <input id="upload-file-banner5"  class="upload-file" type="file" name="file" accept="image/*"/></li> 
				<li data-method="rotate" data-option="-90"><a href="javascript:void(0)" title="Rotate Left"><span id="rotateLeft"><?php echo $this->Html->image("rotate-left.png",array("alt"=>"Voucher Pic")); ?></span></a></li>
				 <li data-method="rotate" data-option="90"><a href="javascript:void(0)" title="Rotate Right"><span id="rotateRight"><?php echo $this->Html->image("rotate-right.png",array("alt"=>"Voucher Pic")); ?></span></a></li>
				 <li data-method="zoom" data-option="-0.1"><a href="javascript:void(0)" title="Zoom Out"><span id="zoomIn"><?php echo $this->Html->image("zoom-out.png",array("alt"=>"Voucher Pic")); ?></span></a></li>
				  <li data-method="zoom" data-option="0.1"><a href="javascript:void(0)" title="Zoom In"><span id="zoomOut"><?php echo $this->Html->image("zoom-in.png",array("alt"=>"Voucher Pic")); ?></span></a></li>
			 </ul>             
		  </div>
		</div>
        
		<div class="iviewer-btn">
			<ul class="list-inline">
			   <li><button class="btn btn-custom" data-dismiss="modal">CLOSE</button></li> 
			   <li><button class="btn btn-custom btn-primary" data-method="getCroppedCanvas" data-option="{ &quot;maxWidth&quot;: 4096, &quot;maxHeight&quot;: 4096 }" id="saveimage">SAVE CHANGES</button></li>
			</ul>
		</div>
	 </div>  	
    </div>
  </div>
</div>


<div class="modal fade loginModel themeModel" id="imageModal6" tabindex="-1" role="dialog" aria-labelledby="imageModal6Label" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
       <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title">Select Image </h4>
      </div>
      <div class="modal-body actions">
        <div class="selct-img-voc">
		  <div class="img-container" style="width:100%;">
				<img src="<?php echo $tmpImage6; ?>" alt="Picture">
				<!-- Hidden field contains the id of the field it's going to replace -->
				<input hidden class="replace-image" value="img_upload6" />
			</div>
		  <div class="iviewer" >
			 <ul class="custom-file-upload" class="docs-buttons">
				 <li><label for="upload-file" class="custom-file-upload"><span ><?php echo $this->Html->image("gallery-img.png",array("alt"=>"Voucher Pic")); ?></span></label> <input id="upload-file-banner6"  class="upload-file" type="file" name="file" accept="image/*"/></li> 
				<li data-method="rotate" data-option="-90"><a href="javascript:void(0)" title="Rotate Left"><span id="rotateLeft"><?php echo $this->Html->image("rotate-left.png",array("alt"=>"Voucher Pic")); ?></span></a></li>
				 <li data-method="rotate" data-option="90"><a href="javascript:void(0)" title="Rotate Right"><span id="rotateRight"><?php echo $this->Html->image("rotate-right.png",array("alt"=>"Voucher Pic")); ?></span></a></li>
				 <li data-method="zoom" data-option="-0.1"><a href="javascript:void(0)" title="Zoom Out"><span id="zoomIn"><?php echo $this->Html->image("zoom-out.png",array("alt"=>"Voucher Pic")); ?></span></a></li>
				  <li data-method="zoom" data-option="0.1"><a href="javascript:void(0)" title="Zoom In"><span id="zoomOut"><?php echo $this->Html->image("zoom-in.png",array("alt"=>"Voucher Pic")); ?></span></a></li>
			 </ul>             
		  </div>
		</div>
        
		<div class="iviewer-btn">
			<ul class="list-inline">
			   <li><button class="btn btn-custom" data-dismiss="modal">CLOSE</button></li> 
			   <li><button class="btn btn-custom btn-primary" data-method="getCroppedCanvas" data-option="{ &quot;maxWidth&quot;: 4096, &quot;maxHeight&quot;: 4096 }" id="saveimage">SAVE CHANGES</button></li>
			</ul>
		</div>
	 </div>  	
    </div>
  </div>
</div>





<style>
.img-container {
  /* Never limit the container height here */
  max-width: 100%;
}

.img-container img {
  /* This is important */
  width: 100%;
}
.cropper-bg {background-image:none !important;}
.cropper-modal {background-color:#EEEEEE !important;opacity:0 !important;}
.weekday_text{margin-top:14px;}

</style>

 <script>
      // This example requires the Places library. Include the libraries=places
      // parameter when you first load the API. For example:
      // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">
			//Adding Place for images preview
		var i=2;
		$("#addmore").on('click', function() {
			//$("#imageModalHeader1"+read_more).modal("show");
			$(".txt_demo_location_image").hide();
			$(".preview"+i).show();		
			if(i==6)
			{
				$("#addmore").attr("disabled", true);			
				$('#addmore').css('cursor','not-allowed');
			}
				
			i++;	
			  		
		});
        function initMap() {
		  		  
        var map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: <?php echo $latitude ?>, lng: <?php echo $longitude ?>},
          zoom: 13
        });
        var card = document.getElementById('pac-card');
        var input = document.getElementById('pac-input');
        var types = document.getElementById('type-selector');
        var strictBounds = document.getElementById('strict-bounds-selector');

        map.controls[google.maps.ControlPosition.TOP_RIGHT].push(card);

        var autocomplete = new google.maps.places.Autocomplete(input);

        // Bind the map's bounds (viewport) property to the autocomplete object,
        // so that the autocomplete requests use the current map bounds for the
        // bounds option in the request.
        autocomplete.bindTo('bounds', map);
		
        var infowindow = new google.maps.InfoWindow();
        var infowindowContent = document.getElementById('infowindow-content');
        infowindow.setContent(infowindowContent);
        var marker = new google.maps.Marker({
		 position: new google.maps.LatLng( <?php echo $latitude ?>,<?php echo $longitude ?>), 
          map: map   
        });

          autocomplete.addListener('place_changed', function() {
          infowindow.close();
          marker.setVisible(false);
          var place = autocomplete.getPlace();
          if (!place.geometry) {
            // User entered the name of a Place that was not suggested and
            // pressed the Enter key, or the Place Details request failed.
            //window.alert("No details available for input: '" + place.name + "'");
            return;
          }

          // If the place has a geometry, then present it on a map.
          if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
            var address =place.name;
            var geocoder = new google.maps.Geocoder();
            geocoder.geocode({ 'address': address}, function (results, status) {
                        if (status == google.maps.GeocoderStatus.OK) {
							var latitude = place.geometry.location.lat();
                            var longitude = place.geometry.location.lng();
                            $('#LocationLatitude').val(latitude);
                            $('#LocationLongitude').val(longitude);
                        } else {
                           
                        }
                    });
            
          } else {
			 
            map.setCenter(place.geometry.location);
            map.setZoom(17);  // Why 17? Because it looks good.
          }
          marker.setPosition(place.geometry.location);
          marker.setVisible(true);

          var address = '';
          if (place.address_components) {
            address = [
              (place.address_components[0] && place.address_components[0].short_name || ''),
              (place.address_components[1] && place.address_components[1].short_name || ''),
              (place.address_components[2] && place.address_components[2].short_name || '')
            ].join(' ');
          }       
          infowindowContent.children['place-icon'].src = place.icon;
          infowindowContent.children['place-name'].textContent = place.name;
          infowindowContent.children['place-address'].textContent = address;         
          	
			infowindow.open(map, marker);
        }    
        );	
        
		map.setCenter(new google.maps.LatLng( <?php echo $latitude; ?>,<?php echo $longitude; ?>));		
        map.setZoom(17);  
        
         if(<?php echo $latitude ?>!=""){
          marker.addListener('click', function() {
			infowindow = new google.maps.InfoWindow({
			content: "<?php echo $address;?>"
			});
			infowindow.open(map, marker);
			});
		}
	}
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDcBcY5ObGmiZ6TETlFS1cEOi5ttvBRnuw&libraries=places&callback=initMap" async defer></script>
<script>	
$(document).ready(function () {
	
	  $('#LocationAddForm').validate({ // initialize the plugin
	rules: {			
		 
		  "data[Location][title]": {
			required: true,				
		  },
		  "data[Location][address]": {
			required: true				
		  }		
		},
		// Specify validation error messages
		messages: {			
			"data[Location][title]": {
				required: "Please enter Location  Name"			
			},		 
			"data[Location][address]": {
				required: "Please enter Location Address"			
			}
		},			
		submitHandler: function(form) {
		  form.submit();
		}
	  }); 
	

	$("div.weekdays").children("label").on("click",function(){	
			$(this).toggleClass("selected");		
	});	

	$(".btn btn-theme").on("click", function() {
		alert("It is working well");
		});	
});	  
		

</script>
