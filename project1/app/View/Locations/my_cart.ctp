<section class="inner-banner topheader">
   <div class="container">
		<div class="banner-cont">
		   <ul class="menu-banner">
				<li><a href="<?php echo SITE_LINK;?>drafted-vouchers" class="<?php echo ($this->params['controller'] == 'vouchers' && $this->params['action'] == 'index' )? 'active':''?>" title="My Vouchers">My Vouchers</a></li>   
				<li><a href="<?php echo SITE_LINK;?>locations" class="active" title="My Locations">My Locations</a></li>   
		   </ul>
		</div>
   </div>
</section>		
 <section class="section-grey">
        <div class="container">
            <h1 class="clearfix">My Cart </h1>
            <?php //pr($business);?>
            <?php echo $this->Form->create("Locations",array("div"=>false,)); ?>
            <div class="vouchers-location">
				<?php echo $this->Form->hidden('businessid'); echo $this->Form->hidden('planid',array("value"=>$planid));?>
				<?php foreach($this->Session->read("cart_items") as $val) {$vat = $total =0.00; $subtotal=0.00; $location['Location'] = $val; ?>
					
					<div class="vouchers-location-row clearfix">
					 
                    <div class="vouchers-location-pic">
                        <?php echo ($location['Location']['image']=="" || !file_exists(WWW_ROOT."img/profile/".$location['Location']['image']))?($this->Html->image("default_new.png",array("width"=>102))):($this->Html->image("profile/".$location['Location']['image'],array("width"=>102))); ?>
                    </div>
                    <div class="vouchers-location-dec">
                        <h2><?php echo $location["Location"]["title"]?></h2>
                        <p><?php echo $this->Html->image("map-icon.png",array("alt"=>"Location"));?><?php echo $location["Location"]["address"]?></p>
                        <p><?php echo $this->Html->image("date-icon.png",array("alt"=>"Date"));?><?php $created =  date_create(h($location['Location']['created'])); echo date_format($created,"j M, Y"); ?> | <?php  echo date_format($created,"h:i A"); ?>
                       
                        <span class="voucher-status">						 							 
							<?php echo $this->Html->image("inactive.png",array("alt"=>"Inactive"));?>Inactive
						</span>
					</p>
                    </div>                   
                     <div class="price-check">
                        <div class="price-check-left">£ 9.99</div> 
                        <div class="price-check-right">
							<?php if ($location["Location"]['is_view']) {  ?>
                              <input class="checkmark cart_item" value="<?php echo $location["Location"]["id"]?>" checked  type="checkbox" id="checkmark<?php echo $location["Location"]["id"]?>" val="<?php echo $location["Location"]["id"]?>" />
                            <?php } else { ?>
								<input class="checkmark cart_item" value="<?php echo $location["Location"]["id"]?>"  type="checkbox" id="checkmark<?php echo $location["Location"]["id"]?>" val="<?php echo $location["Location"]["id"]?>" />
							<?php } ?>
                               <label for="checkmark<?php echo $location["Location"]["id"]?>"></label>                 
                        </div>
                    </div>
                </div>
				<?php $subtotal += 9.99;} $vat = number_format($subtotal*20/100,2);$total = number_format($subtotal+$vat); ?>
            </div>
           
             <div class="cart-tbl clearfix">
                <div class="cart-tbl-main">
                    <table>
                        <tr>
                            <td width="420">Subtotal</td>
                            <td><b><div class="subtotal"></div></b></td>
                        </tr>
                        <tr>
                            <td>VAT @20%</td>
                            <td><b><div class="vat" ></div></b></td>
                        </tr>
                        <tr>
                            <td>Order total</td>
                            <td><b><div class="total"></div></b></td>
                        </tr>
                        <tr class="cart-tbl-footer">
                            <td>TOTAL</td>
                            <td><b><div class="total"></div> </b></td>
                        </tr>
                    </table>
                     <div class="proceed-btn clearfix" >                
                 <button type="submit" class="btn" title="PROCEED TO PAY" >PROCEED TO PAY</button>
               <div class="alert alert-danger error errormessage" style="display:none">Please select atleast one location</div>
            </div>
                </div>
            </div>
           
             </form>
        </div>
    </section>
    
 
<script>
$(document).ready(function () {	
	
	var valtag="";
	//var subtotal=<?php echo $subtotal ?>;	
	var vat=0.00;
	var total=0.00;
	var subtotal=0.00;
	$('input[type=checkbox]').each(function () {
		
		if ($(this).is(":checked")) {
            subtotal=parseFloat(subtotal)+ parseFloat(9.99);	
			subtotal=subtotal.toFixed(2);	
			vat=( subtotal * 20 / 100 ).toFixed(2);	
			total=parseFloat(subtotal)+parseFloat(vat);				
        }
	});
	
	if(subtotal=="0")
	{
		$(".subtotal").text("£ 0.00");
		$(".vat").text("£ 0.00");
		$(".total").text("£ 0.00");
		$(".cart_total_items").html(0);
		
	}
	else{
		$(".subtotal").text("£ "+subtotal);
		$(".vat").text("£ "+vat);
	    $(".total").text("£ "+ total.toFixed(2));	    
	   // $(".cart_total_items").html($('.cart_item:checked').length);
	}
	
	$('.cart_item').change(function (event) {
		//console.log($('.cart_item:checked').length);
		var id=$(this).attr("value");
		 if(this.checked) {
			subtotal=parseFloat(subtotal)+ parseFloat(9.99);	
			subtotal=subtotal.toFixed(2);
			$("#cart"+id).show();	
			$("#cart1"+id).show();		
		  }
		  else
		  {
			  subtotal=parseFloat(subtotal)- parseFloat(9.99);
			  subtotal=Math.abs(subtotal.toFixed(2));	
			  subtotal=subtotal.toFixed(2);	
			  $("#cart"+id).hide();
			  $("#cart1"+id).hide();
		  }
		  
		  if(subtotal==0)
			{
				$(".cart_total_items").hide();									
			}
			else
			{
				$(".cart_total_items").show();
			}
		  
		 var url = "processcart?locid="+$(this).attr("value");
		 var href =SITE_LINK+url;
		  $.ajax({
			type: "GET",   
			cache: false,   
			url: href,   
			success: function(data){
				data = JSON.parse(data);
				if(data.status){ 				
					
				}
			}  
			});			
		
		vat=( subtotal * 20 / 100 ).toFixed(2);		
		total=parseFloat(subtotal)+parseFloat(vat);				
		$(".subtotal").text("£ "+subtotal);
		$(".vat").text("£ "+vat);
		$(".total").text("£ "+ total.toFixed(2));
		$(".cart_tval").html($('.cart_item:checked').length+" items in Basket   £ "+ total);	
		$(".cart_total_items").html($('.cart_item:checked').length);
		
	});
	
	$( "#LocationsMyCartForm" ).submit(function( event ) {
		var is_chk=false;
		var ids="";
		
		$('input[type=checkbox]').each(function () {			
			if ($(this).is(":checked")) {
				is_chk=true;
				if($("#LocationsBusinessid").val()=="")
				{
				     valtag=$(this).val()+ $("#LocationsBusinessid").val();
				 }
				 else{
					  valtag=$(this).val()+","+ $("#LocationsBusinessid").val();	
				  }
				  $("#LocationsBusinessid").val(valtag);	
			}			
		});		
		if(!is_chk)
		{
			$(".errormessage").show();
			return false;
		}
		else
		{
			$(".errormessage").hide();	
			
			return true;
		}
		
	});
	
	$(document).on("click","#deleteuserbusiness",function(e){
		$("#deletebusienssbox").modal("show");
	});
});

</script>
