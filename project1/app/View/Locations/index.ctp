<section class="inner-banner topheader">
   <div class="container">
		<div class="banner-cont">
		   <ul class="menu-banner">
				<li><a href="<?php echo SITE_LINK;?>drafted-vouchers" class="<?php echo ($this->params['controller'] == 'vouchers' && $this->params['action'] == 'index' )? 'active':''?>" title="My Vouchers">My Vouchers</a></li>   
				<li><a href="<?php echo SITE_LINK;?>locations" class="<?php echo ($this->params['controller'] == 'Locations' && $this->params['action'] == 'index' )? 'active':''?>" title="My Locations">My Locations</a></li>   
		   </ul>
		</div>
   </div>
</section>		
 <section class="section-grey">
        <div class="container">
            <h1 class="clearfix">My Locations
                <div class="location-right">
                    <div class="location-right-seacrh">
						<?php echo $this->Form->create("Locations",array("div"=>false,)); ?>
							<input type="text" placeholder="Search"  id="search" name="search">
                        </form>
                    </div>
                    <a href="<?php echo SITE_LINK?>add_location" title="" class="plus-add view"><?php echo $this->Html->image("plus.png",array("alt"=>"Add Location"));?></a>
                </div>
            </h1>
            <?php //pr($business);?>
            <div class="vouchers-location">
				<?php echo $this->Form->hidden('businessid');?><?php echo $this->Form->hidden('paymentconfirm',array("value"=>$paymentconfirm));?>
				<?php if(!empty($business)) {?>
						<?php foreach($business as $location){?>
							<?php if($location["Location"]["is_paid"] == 0){ ?>	
								<div class="vouchers-location-row inactive clearfix">
							 <?php } else { ?>
								 <div class="vouchers-location-row active clearfix">
							 <?php } ?>
							<div class="vouchers-location-pic">
								
								<?php echo (isset($location['LocationImage'][0])?($location['LocationImage'][0]['image']=="" || !file_exists(WWW_ROOT."img/location/".$location['LocationImage'][0]['image']))?($this->Html->image("location-here.png",array("width"=>102))):($this->Html->image("location/".$location['LocationImage'][0]['image'],array("width"=>102))):($this->Html->image("location-here.png",array("width"=>102)))); ?>
							</div>
							<div class="vouchers-location-dec">
								<h2><?php echo $location["Location"]["title"]?></h2>
								<p><?php echo $this->Html->image("map-icon.png",array("alt"=>"Location"));?><?php echo $location["Location"]["address"]?></p>
								<p><?php echo $this->Html->image("date-icon.png",array("alt"=>"Date"));?><?php $created =  date_create(h($location['Location']['created'])); echo date_format($created,"j M, Y"); ?> | <?php  echo date_format($created,"h:i A"); ?>
							   
								<span class="voucher-status">
								 <?php if($location["Location"]["is_paid"] == 0){ ?>							 
									<?php echo $this->Html->image("inactive.png",array("alt"=>"Inactive"));?>Inactive
									 <?php } else { ?>
									<?php echo $this->Html->image("active.png",array("alt"=>"Active"));?>Active
									<?php } ?>
									</span></p>
							</div>                   
							<ul class="vocher-right">
								<?php $loc=str_replace(" ","-",$location["Location"]["title"])?>
								<li><a href="<?php echo SITE_LINK; ?>b/<?php echo $location["Location"]["id"]."-".$loc;?>" title="Edit"><?php echo $this->Html->image("v1.png",array("alt"=>"Edit"));?>Edit</a></li>
								<?php 
									//$url=str_replace(" ","-",$location["Location"]["address"]); $url=str_replace(",","",$url); 
									//$tval = preg_replace('!\s+!', ' ', $location['Location']['title']);
									$tval = preg_replace('!\s+!', ' ', $location['Location']['address']);
									$tval =str_replace(array(","),"",$tval);
									$url = $location['Location']['id']."-".str_replace(array(" ",","),"-",$tval);
								?>
								<li><a href="<?php echo SITE_LINK."".$url ?>" title="View" target="_blank"><?php echo $this->Html->image("v2.png",array("alt"=>"View"));?>View</a></li>
								<li><a href="javascript:void(0)" class="deleteuserbusiness" value="<?php echo $location["Location"]["id"]?>" title="Delete"><?php echo $this->Html->image("v3.png",array("alt"=>"Delete"));?>Delete</a></li>
							</ul>
						</div>
						<?php } }else{?>
							<div class="alert alert-info">No location Added.</div>
						<?php } ?>	
            </div>
            <?php ;if(count($_SESSION["cart_items"])>0) {?>
            <div class="proceed-btn">
                <a href="<?php echo SITE_LINK?>my_cart" title="PROCEED TO PAY" class="btn">PROCEED TO PAY</a>
            </div>            
			<?php } ?>	
        </div>
    </section>
 <!-- Business Deletion Confirmation-->   
<div class="modal in fade themeModel" tabindex="-1" role="dialog" id="deletebusienssbox">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Delete</h5>       
      </div>
      <div class="modal-body">
        <p>Are you sure you want to delete this location?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary btn-theme" data-dismiss="modal">No</button>
        <button type="button" class="btn btn-secondary btn-theme confirmdeletebusiness">Yes</button>
      </div>
    </div>
  </div>
</div> 
 <!-- End Business Deletion Confirmation-->   
  <!-- Payment Successfull no condition-->   
<div class="modal in fade themeModel" tabindex="-1" role="dialog" id="paymentconfirmbox">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Payment successful.</h5>        
      </div>      
      <div class="modal-footer">       
        <button type="button" class="btn btn-secondary btn-theme" data-dismiss="modal">Ok</button>
      </div>
    </div>
  </div>
</div> 
<!-- End Payment Successfull no condition-->   
<!-- Payment Successfull But no voucher added till by user-->   
<div class="modal in fade themeModel" tabindex="-1" role="dialog" id="novoucher">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Payment successful but unfortunately there is no voucher !!!</h5>    
      </div>
      <div class="modal-body">
        <p>Do you wish to create voucher.</p>
      </div>
      <div class="modal-footer">       
         <button type="button" class="btn btn-secondary btn-theme confirmpublish" value="create">Yes</button>
        <a href="<?php echo SITE_LINK?>locations"><button type="button" class="btn btn-primary btn-theme" >No</button></a>
      </div>
    </div>
  </div>
</div> 
<!--End Payment Successfull But no voucher added till by user--> 
<!-- Confirm Publish or Draft after payment when there is voucher id in session --> 
<div class="modal in fade themeModel" tabindex="-1" role="dialog" id="confirmboxpublishdraft">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Payment Successful !!!</h5>       
      </div>
      <div class="modal-body">       
      </div>
      <div class="modal-footer">
		<button type="button" class="btn btn-secondary btn-theme confirmpublish" value="publish">Ok</button>
      </div>     
    </div>
  </div>
</div>
<!-- End Confirm Publish or Dreaft after payment when there is voucher id in session -->  

<!-- after voucher Publish box --> 
<div class="modal in fade themeModel" tabindex="-1" role="dialog" id="updatepublishconfrim">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Voucher is Published Successfully !!!</h5>        
      </div> 
      <div class="modal-footer">
		 <a href="<?php  echo SITE_LINK."published-vouchers"?>" class="btn btn-secondary btn-theme" >OK</a>   
      </div>
    </div>
  </div>
</div> 
<!-- End after voucher Publish box --> 
<!--  after voucher save as draft box --> 
<div class="modal in fade themeModel" tabindex="-1" role="dialog" id="updatedraftconfrim">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Voucher is Saved as Draft !!!</h5>        
      </div>     
      <div class="modal-footer">
		 <a href="<?php  echo SITE_LINK."drafted-vouchers"?>" class="btn btn-secondary btn-theme" >OK</a>   
      </div>
    </div>
  </div>
</div> 
<!--  End after voucher save as draft box --> 

<!--  after voucher save as draft box --> 
<div class="modal in fade themeModel" tabindex="-1" role="dialog" id="cancelpayment">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Payment for Location(s) is cancelled by you.</h5>     
      </div>     
      <div class="modal-footer">
		 <a href="<?php  echo SITE_LINK."locations"?>" class="btn btn-secondary btn-theme" >OK</a>   
      </div>
    </div>
  </div>
</div> 
<!--  End after voucher save as draft box --> 
<!-- Confrim user that new location is added and also added to cart --> 
<div class="modal in fade themeModel" tabindex="-1" role="dialog" id="addbusienssbox">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Location Successfully Saved and Added to Cart !!!</h5>        
      </div>     
      <div class="modal-footer">
		 <a href="<?php  echo SITE_LINK."locations"?>" class="btn btn-secondary btn-theme" >OK</a>   
      </div>
    </div>
  </div>
</div> 
<!-- End Confrim user that new location is added and also added to cart  --> 
<script>
$(document).ready(function () {	
			
			
	$(document).on("click",".confirmpublish",function(e){		
		var pubdrft=($(this).attr("value"));		
		if(pubdrft=="draft")
		{
			$("#updatepublishconfrim").modal("hide");
			$("#confirmboxpublishdraft").modal("hide");
			$("#updatedraftconfrim").modal("show");
			
		}
		else if(pubdrft=="publish")
		{
			 var href =SITE_LINK+"publish_all_vouchers";		
			 $.ajax({   
				type: "GET",   
				cache: false,   
				url: href,   
				success: function(data){
					data = JSON.parse(data);
					if(data.status)				
					{
						$("#confirmboxpublishdraft").modal("hide");
						$("#updatedraftconfrim").modal("hide");
						$("#updatepublishconfrim").modal("show");
						window.location.href = SITE_LINK+"published-vouchers";
					}		
				}  
			});	
		}else
		{
		  window.location.href = SITE_LINK+"create-voucher";
		}	
	});	
			
	$(document).on("click",".deleteuserbusiness",function(e){	
	$("#LocationsBusinessid").val($(this).attr("value"));	
		$("#deletebusienssbox").modal("show");		
	});
	
	$(document).on("click",".confirmdeletebusiness",function(e){
		 var id=$("#LocationsBusinessid").val();			
		 var href =SITE_LINK+"deleteuserbusiness?id="+id;		
		 $.ajax({   
			type: "GET",   
			cache: false,   
			url: href,   
			success: function(data){
				data = JSON.parse(data);
				if(data.message=="Deleted")				
				{
					$("#deletebusienssbox").modal("hide");
					window.location.href = SITE_LINK+"locations";
				}
			}  
		});		
	});
	
});	

$(window).on('load',function(){
		
	if($("#LocationsPaymentconfirm").val()=="voucherupdate")
	{		
		$("#confirmboxpublishdraft").modal("show");	
	}
	else if($("#LocationsPaymentconfirm").val()=="paymentconfirm")
	{		
		$("#paymentconfirmbox").modal("show");	
	}
	else if($("#LocationsPaymentconfirm").val()=="novoucher")
	{		
		$("#novoucher").modal("show");	
	}
	else if($("#LocationsPaymentconfirm").val()=="cancel")
	{		
		$("#cancelpayment").modal("show");	
	}
	else if($("#LocationsPaymentconfirm").val()=="newadded")
	{		
		$("#addbusienssbox").modal("show");	
	}
	else{
			$("#paymentconfirmbox").modal("hide");	
			$("#confirmboxpublishdraft").modal("hide");	
	}
});	
</script>
