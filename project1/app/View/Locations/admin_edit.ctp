<script>
	$(document).ready(function () {
    $('#LocationAdminEditForm').validate({ // initialize the plugin
        rules: {			
			 
			  "data[Location][title]": {
				required: true				
			  }		
			},
			// Specify validation error messages
			messages: {			
				"data[Location][title]": {
					required: "Please enter a location"			
				}
			},
			// Make sure the form is submitted to the destination defined
			// in the "action" attribute of the form when valid
			submitHandler: function(form) {
			  form.submit();
			}
		  });		
});
</script>
<div class="cont-right">
   <div class="cont-right-innr">
	    <div class="main-hd-in">
          <div class="row">
            <div class="col-sm-12">
              <h2 class="title-das"> Edit Location</h2>              
            </div>
          </div>
        </div>

		<div class="manage-marchant">
				<?php echo $this->Form->create('Location'); ?>
			<fieldset>				
			<?php echo $this->Form->input('id');

				echo $this->Form->input('title', array('label' => 'Location'));		
				echo $this->Form->input('is_active');
			?>
			</fieldset>
		 <div style="margin-top:20px;margin-left:20px">
				  <button type="submit" class="btn btn-theme" id="save" name="save">SAVE</button>
							 
					<?php //echo $this->Form->Submit('SUBMIT',array("class"=>"btn btn-theme","title"=>"save",'div'=>false)); ?>
					<a href="<?php echo SITE_LINK ?>manage-locations" class="btn btn-theme">cancel</a>				  
		 </div>
		</div>
	</div>		
</div>
