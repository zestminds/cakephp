<?php echo $this->element("topmenu"); ?>
<div class="userCategories view">
<h2><?php echo __('User Category'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($userCategory['UserCategory']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Category'); ?></dt>
		<dd>
			<?php echo $this->Html->link($userCategory['Category']['title'], array('controller' => 'categories', 'action' => 'view', $userCategory['Category']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('User'); ?></dt>
		<dd>
			<?php echo $this->Html->link($userCategory['User']['id'], array('controller' => 'users', 'action' => 'view', $userCategory['User']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Is Active'); ?></dt>
		<dd>
			<?php echo h($userCategory['UserCategory']['is_active']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($userCategory['UserCategory']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($userCategory['UserCategory']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<?php  echo $this->element("leftmenu_admin");?>
