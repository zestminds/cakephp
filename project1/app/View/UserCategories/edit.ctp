<?php echo $this->element("topmenu"); ?>
<div class="userCategories form">
<?php echo $this->Form->create('UserCategory'); ?>
	<fieldset>
		<legend><?php echo __('Edit User Category'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('category_id');
		echo $this->Form->input('user_id');
		echo $this->Form->input('is_active');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<?php  echo $this->element("leftmenu_admin");?>
