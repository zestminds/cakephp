<script>
	$(document).ready(function () {
    $('#SendNotificationsAdminSendNotificationsForm').validate({ // initialize the plugin
        rules: {			
			 
			  "data[SendNotifications][university][]": {
				req_question:true
			  },
			 "data[SendNotifications][message]": {
				required: true		
			  }	
			},
			// Specify validation error messages
			messages: {		
								
				"data[SendNotifications][message]": {
					required: "Please enter Message"			
				}
			},
			// Make sure the form is submitted to the destination defined
			// in the "action" attribute of the form when valid
			submitHandler: function(form) {
			  form.submit();
			}
		  });		


	$.validator.addMethod('req_question', function(value, elem) {
		 if ($("#SendNotificationsUniversity").children("option:checked").length <= 0) {	
					
				return false;
			}
			 else { 
				return true;
			}					
		
	   }, 'Please select at least one univerity');
	   
	   
	var maxLength = 100;
	$('#SendNotificationsMessage').keyup(function() {
		var textlen = maxLength - $(this).val().length+ " left";
		$('#display_count').text(textlen);
	});
});
	   
</script>	
<div class="cont-right">
   <div class="cont-right-innr">
	    <div class="main-hd-in">
          <div class="row">
            <div class="col-sm-12">
              <h2 class="title-das">Send Notifications</h2>              
            </div>
          </div>
        </div>
		<div class="manage-marchant">
		<?php echo $this->Form->create('SendNotifications',array("novalidate"=>true)); ?>
			<fieldset>				
			<?php				
				echo $this->Form->input('university',array("class"=>"form-control selectUnitemp","options"=>$university,"label"=>"Universities",'multiple' => true,"width"=>"200px"));
				
				echo $this->Form->input('type',array("class"=>"form-control  selectUni","options"=>$notificationType,'label'=>'Type',"width"=>"200px"));
				
				echo $this->Form->input('message',array("type"=>"textarea",'label'=>'Message (charater limit 100)','maxlength'=>'100',"rows"=>5,"class"=>"form-control selectUnitemp"));						
				?>
				<div id="display_count"></div>
			</fieldset>
			<div style="margin-top:20px;margin-left:20px">
				  <button type="submit" class="btn btn-theme" id="save" name="save">Send</button>						  
			</div>
		</div>
	
	
   </div>		
</div>
