<style>
.td{align:left;}
</style>
<div class="cont-right">
   <div class="cont-right-innr">	
		<div class="main-hd-in">
          <div class="row">
            <div class="col-sm-12">
              <h2 class="title-das"> Manage Email Templates</h2>
              
              	<div class="show-record">                	
                   <span> Show Records </span> 
                   <?php echo $this->Form->create("emailtemplate",array("div"=>false,"type"=>"get")); 			
					 echo $this->Form->input('records',array("id"=>"records",'label'=>'',"options"=>$records,'class'=>'form-control',"selected"=>$limit));
					 echo $this->Form->end(); ?>	
                </div>
            </div>
          </div>
        </div>
		<div class="emailTemplates index border-box">
			
			<?php echo $this->Form->create("EmailTemplate",array("div"=>false,)); ?>
				<div class="srch">
					<?php echo $this->element("admins/common",array("place"=>'Search Title,Subject',"flag"=>false,"pageheader"=>'',"buttontitle"=>'no',"listflag"=>"no","action"=>'no',"selflag"=>false)); ?>
					
				</div>
				<div class="table-responsive">
				<table class="Marchant-table table table-bordered" width="100%">
				<tr>
						<th>S.No</th>
						<th><?php echo $this->Paginator->sort('header',"Title"); ?></th>
						<th><?php echo $this->Paginator->sort('subject',"Subject"); ?></th>
						
						<th><?php echo $this->Paginator->sort('email_from'); ?></th>
						<th><?php echo $this->Paginator->sort('created'); ?></th>
						<th><?php echo $this->Paginator->sort('modified'); ?></th>
						<th class="actions"><?php echo __('Actions'); ?></th>
				</tr>
				<?php foreach ($emailTemplates as $emailTemplate): ?>
				<tr>
					<td><?php echo $sno;?>&nbsp;</td>	
					<td><?php echo h($emailTemplate['EmailTemplate']['header']); ?>&nbsp;</td>
					<td><?php echo h($emailTemplate['EmailTemplate']['subject']); ?>&nbsp;</td>
					<td><?php echo h($emailTemplate['EmailTemplate']['email_from']); ?>&nbsp;</td>
					<td><?php echo h($emailTemplate['EmailTemplate']['created']); ?>&nbsp;</td>
					<td><?php echo h($emailTemplate['EmailTemplate']['modified']); ?>&nbsp;</td>				
					<td><a href="<?php echo SITE_LINK.'edit-email-template/'.$emailTemplate['EmailTemplate']['id']?>"><i class="fa fa-pencil"></i></a>	
					</td>
				</tr>
			 <?php $sno++;endforeach; ?>
				</table>
				</div>
				<div class="pagination-main">
					<div class="paging pagination">
					<?php echo $this->Paginator->prev('<i class="fa fa-caret-left"></i>', array('escape' => false), null, array('class' => 'fa prev disabled'));
					
					echo $this->Paginator->numbers(array('separator' => ''));
					echo $this->Paginator->next('<i class="fa fa-caret-right "></i>', array('escape' => false), null, array('class'	=> 'fa next disabled'));
					?>
				   </div>  
			   </div> 
				<?php echo $this->Form->end(); ?>
			</div>
		</div>
	</div>		
</div>
<script>
$(document).ready(function () {
	$("#records").change(function(e) { 
			var val=$('#records').val();
			window.location.replace(SITE_LINK+"email-templates/?records="+val);			
				  	  
	});
});	
</script>
