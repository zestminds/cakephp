<div class="biilings form">
<?php echo $this->Form->create('Biiling'); ?>
	<fieldset>
		<legend><?php echo __('Add Biiling'); ?></legend>
	<?php
		echo $this->Form->input('user_id');
		echo $this->Form->input('plan_id');
		echo $this->Form->input('bill_dates');
		echo $this->Form->input('bill_status');
		echo $this->Form->input('is_active');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Biilings'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Plans'), array('controller' => 'plans', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Plan'), array('controller' => 'plans', 'action' => 'add')); ?> </li>
	</ul>
</div>
