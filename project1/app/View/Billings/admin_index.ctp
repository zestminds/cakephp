<div class="cont-right">
   <div class="cont-right-innr">
	   <div class="main-hd-in">
          <div class="row">
            <div class="col-sm-12">
              <h2 class="title-das"> View Sales </h2>
              
              	<div class="show-record">
                	
                   <span> Show Records </span> 
                   <?php echo $this->Form->create("voucher",array("div"=>false,"type"=>"get")); 			
					 echo $this->Form->input('records',array("id"=>"records",'label'=>'',"options"=>$records,'class'=>'form-control',"selected"=>$limit));
					 echo $this->Form->end(); ?>
                </div>
            </div>
          </div>
        </div>
        <div class="manage-marchant">
        <?php echo $this->Form->create("Billing",array("div"=>false,)); ?>
			<div class="srch">
				<?php echo $this->element("admins/common",array("place"=>'Search by business Name',"flag"=>false,"pageheader"=>'',"buttontitle"=>'no',"listflag"=>"no","action"=>'no',"selflag"=>false)); ?>
			</div>
          <div class="table-responsive">
				<table class="Marchant-table table table-bordered " width="100%">
				<thead>
				<tr>							
						<th><?php echo $this->Paginator->sort('S.No'); ?></th>
						<th><?php echo $this->Paginator->sort('UserDetail.business','Business Name'); ?></th>
						<th><?php echo $this->Paginator->sort('plan_id','Amount'); ?></th>
						<th><?php echo $this->Paginator->sort('renew_date','Date'); ?></th>
						<th><?php echo $this->Paginator->sort('bill_status','Payment Status'); ?></th>		
						<?php /*<th class="actions"><?php echo __('Actions'); ?></th>*/?>
				</tr>
				</thead>
				<tbody>
				<?php foreach ($billings as $billing): ?>
				<tr>	
					<td><?php echo $sno; ?>&nbsp;</td>
					<td>
						<?php echo $billing['UserDetail']['business']; ?>
					</td>
					<td>£<?php echo $billing['Plan']['price']; ?></td>
					<td><?php $bill_date =  date_create(h($billing['Billing']['renew_date'])); echo date_format($bill_date,"jS M, Y");?>&nbsp;</td>
					<td><?php echo h($billing['Billing']['bill_status']);?>&nbsp;</td>		
				<?php /*	<td class="actions">
						<?php echo $this->Html->link(__('View'), array('action' => 'view', $billing['Billing']['id'])); ?>
						<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $billing['Billing']['id'])); ?>
						<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $billing['Billing']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $billing['Billing']['id']))); ?>
					</td>*/?>
				</tr>
			<?php $sno++; endforeach; ?>
				</tbody>
				</table>
			 </div>         
			 <div class="pagination-main">
					<div class="paging pagination">
					<?php echo $this->Paginator->prev('<i class="fa fa-caret-left"></i>', array('escape' => false), null, array('class' => 'fa prev disabled'));
					
					echo $this->Paginator->numbers(array('separator' => ''));
					echo $this->Paginator->next('<i class="fa fa-caret-right "></i>', array('escape' => false), null, array('class'	=> 'fa next disabled'));
					?>
				   </div>  
			</div>      
        </div>
	</div>
</div>
<script>
$(document).ready(function () {
	$("#records").change(function(e) { 
			var val=$('#records').val();
			window.location.replace(SITE_LINK+"view-sales/?records="+val);			
				  	  
	});
});	
</script>
View Sales
