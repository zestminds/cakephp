<div class="biilings view">
<h2><?php echo __('Biiling'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($biiling['Biiling']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('User'); ?></dt>
		<dd>
			<?php echo $this->Html->link($biiling['User']['id'], array('controller' => 'users', 'action' => 'view', $biiling['User']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Plan'); ?></dt>
		<dd>
			<?php echo $this->Html->link($biiling['Plan']['title'], array('controller' => 'plans', 'action' => 'view', $biiling['Plan']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Bill Dates'); ?></dt>
		<dd>
			<?php echo h($biiling['Biiling']['bill_dates']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Bill Status'); ?></dt>
		<dd>
			<?php echo h($biiling['Biiling']['bill_status']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Is Active'); ?></dt>
		<dd>
			<?php echo h($biiling['Biiling']['is_active']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($biiling['Biiling']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($biiling['Biiling']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Biiling'), array('action' => 'edit', $biiling['Biiling']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Biiling'), array('action' => 'delete', $biiling['Biiling']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $biiling['Biiling']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Biilings'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Biiling'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Plans'), array('controller' => 'plans', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Plan'), array('controller' => 'plans', 'action' => 'add')); ?> </li>
	</ul>
</div>
