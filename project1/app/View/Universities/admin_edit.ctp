<script>
	$(document).ready(function () {
    $('#UniversityAdminEditForm').validate({ // initialize the plugin
        rules: {			
			 
			  "data[University][title]": {
				required: true				
			  }	,
			  
			  "data[University][email_domain]": {
				required: true				
			  }			
			},
			// Specify validation error messages
			messages: {			
				"data[University][title]": {
					required: "Please enter University"			
				},
				"data[University][email_domain]": {
					required: "Please enter University domain"			
				}
			},
			// Make sure the form is submitted to the destination defined
			// in the "action" attribute of the form when valid
			submitHandler: function(form) {
			  form.submit();
			}
		  });		
});
</script>
<div class="cont-right">
   <div class="cont-right-innr">
		<div class="main-hd-in">
			  <div class="row">
				<div class="col-sm-12">
				  <h2 class="title-das"> Edit Univsersity</h2>              
				</div>
			  </div>
        </div>
		<div class="manage-marchant">
		<?php echo $this->Form->create('University'); ?>
			<fieldset>
				
			<?php
				echo $this->Form->input('id');
				echo $this->Form->input('title',array('label' => 'University Name'));
				echo $this->Form->input('email_domain',array('label' => 'University Domain'));
				echo $this->Form->input('is_active');
			?>
			</fieldset>
		   <div style="margin-top:20px;margin-left:20px">
				  <button type="submit" class="btn btn-theme" id="save" name="save">SAVE</button>
				
					<a href="<?php echo SITE_LINK ?>manage-universities" class="btn btn-theme">cancel</a>		  
		</div>
		</div>
	</div>		
</div>
