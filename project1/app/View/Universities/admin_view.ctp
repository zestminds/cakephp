<div class="cont-right">
   <div class="cont-right-innr">
		<div class="universities view">
		<h2><?php echo __('University'); ?></h2>
			<dl>
				<dt><?php echo __('Id'); ?></dt>
				<dd>
					<?php echo h($university['University']['id']); ?>
					&nbsp;
				</dd>
				<dt><?php echo __('University Name'); ?></dt>
				<dd>
					<?php echo h($university['University']['title']); ?>
					&nbsp;
				</dd>
				<dt><?php echo __('Email Domain'); ?></dt>
				<dd>
					<?php echo h($university['University']['email_domain']); ?>
					&nbsp;
				</dd>
				<dt><?php echo __('Status'); ?></dt>
				<dd>
					<?php echo h(($university['University']['is_active'])?'Active':'Inactive'); ?>
					&nbsp;
				</dd>
				<dt><?php echo __('Created'); ?></dt>
				<dd>
					<?php $joined =  date_create(($university['University']['created'])); echo date_format($joined,"jS M, Y h:i:s");?>
					&nbsp;
				</dd>
				<dt><?php echo __('Modified'); ?></dt>
				<dd>
					<?php $joined =  date_create(($university['University']['modified'])); echo date_format($joined,"jS M, Y h:i:s");?>
					&nbsp;
				</dd>
			</dl>
		 </div>
	</div>		
</div>
