<script>
	$(document).ready(function () {
    $('#DefaultValueAdminEditForm').validate({ // initialize the plugin
        rules: {			
			 
			  "data[DefaultValue][1]": {
				required: true				
			  },
			 "data[DefaultValue][2]": {
				required: true		
			  },
			 "data[DefaultValue][3]": {
				required: true		
			  },
			 "data[DefaultValue][4]": {
				required: true		
			  },
			 "data[DefaultValue][5]": {
				required: true		
			  }			
			},
			// Specify validation error messages
			messages: {			
				"data[DefaultValue][1]": {
					required: "Please enter discount value"			
				},
				"data[DefaultValue][2]": {
					required: "Please enter sub title"			
				},
				"data[DefaultValue][3]": {
					required: "Please enter description"			
				},
				"data[DefaultValue][4]": {
					required: "Please enter  terms"			
				},
				"data[DefaultValue][5]": {
					required: "Please enter terms url"			
				}
			},
			// Make sure the form is submitted to the destination defined
			// in the "action" attribute of the form when valid
			submitHandler: function(form) {
			  form.submit();
			}
		  });		
});
</script>	
<div class="tags form">
<?php echo $this->Form->create('DefaultValue'); //pr($defaultvalues); ?>
	<fieldset>
		<legend><?php echo __('Edit Default Values'); ?></legend>
	<?php	
		
		echo $this->Form->input($defaultvalues[0]['DefaultValue']['id'],array('label'=>'Discount','value'=>$defaultvalues[0]['DefaultValue']['value']));
		echo $this->Form->input($defaultvalues[1]['DefaultValue']['id'],array('label'=>'Sub Title','value'=>$defaultvalues[1]['DefaultValue']['value']));
		echo $this->Form->input($defaultvalues[2]['DefaultValue']['id'],array('label'=>'Description','value'=>$defaultvalues[2]['DefaultValue']['value']));
		echo $this->Form->input($defaultvalues[3]['DefaultValue']['id'],array('label'=>'Terms','value'=>$defaultvalues[3]['DefaultValue']['value']));
		echo $this->Form->input($defaultvalues[4]['DefaultValue']['id'],array('label'=>'Terms URL','value'=>$defaultvalues[4]['DefaultValue']['value']));
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
