<script>
 $(document).ready(function () { 
  window.setTimeout(function(){
	// Move to a new location or you can do something else
	window.location.href = SITE_LINK+"advertise-with-moola";
	}, 7000);
});
</script>
<section class="msg-banner">
    	<div class="container">
        	<div class="msg-content">
                <h2>Request Sent Successfully </h2>
                <p>A link for confirming your email was sent to the following email address</p>
                <p><?php echo $this->Session->flash(); ?></p>
                <p>Please follow the link to confirm your email.</p>
            </div>
        </div>
</section>
