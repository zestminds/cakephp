<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title><?php echo h($voucher['Voucher']['title_discount'])." Student Discount by ".h($voucher['UserDetail']['business']); ?></title>
<meta name="description" content="<?php echo h($voucher['Voucher']['description']); ?>" />
<meta property="og:title" content="<?php echo h($voucher['Voucher']['title_discount']); ?> Student Discount"/>
<meta property="og:image" content='<?php echo (($voucher['Voucher']['image']=="" || !file_exists(WWW_ROOT."img/voucher/".$voucher['Voucher']['image']))?(SITE_LINK."/img/default_voucher.jpeg"):(SITE_LINK."/img/voucher/".h($voucher['Voucher']['image']))); ?>' />
<meta property="og:site_name" content="https://www.moolarewards.co.uk/"/>
<meta property="og:description" content="<?php echo h($voucher['Voucher']['description']); ?>"/>

<meta name="twitter:card" content="summary_midium_image" />
<meta name="twitter:description" content="<?php echo h($voucher['Voucher']['description']); ?>" />
<meta name="twitter:title" content="<?php echo h($voucher['Voucher']['title_discount']); ?> Student Discount" />
<meta name="twitter:image" content="<?php echo (($voucher['Voucher']['image']=="" || !file_exists(WWW_ROOT."img/voucher/".$voucher['Voucher']['image']))?(SITE_LINK."/img/default_voucher.jpeg"):(SITE_LINK."/img/voucher/".h($voucher['Voucher']['image']))); ?>" />

	
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<!-- css -->
<?php
echo $this->Html->css(array("moola/bootstrap.min","moola/font-awesome.min","moola/owl.carousel","moola/style","moola/responsive","internal")); ?>
<script>
var SITE_LINK = "<?php echo SITE_LINK; ?>";
</script>
<!--[if IE]>
	<script src="js/html5shiv.js"></script>
<![endif]-->
</head>
<body>
<div class="share_voucher"> 
  <!-- Preloader -->
  <div class="preLoader"></div>
    <!-- End header sec -->
	<div class="vouch-details">	
		<div class="vouch-img_share">
			<?php echo ($voucher['Voucher']['image']=="" || !file_exists(WWW_ROOT."img/voucher/".$voucher['Voucher']['image']))?($this->Html->image("default_voucher.jpeg")):($this->Html->image("voucher/".h($voucher['Voucher']['image']),array('width'=>'550','height'=>'300'))); ?>
	   </div>
		<div class="vouch-desc">	  
		  <ul>
				<li>
				  
				  <p><?php echo h($voucher['UserDetail']['business']); ?></p>
				  <p><?php echo h($voucher['UserDetail']['address']); ?></p>
			  </li>
			   <li>
				  <h5><span><?php echo $this->Html->image("icon-1.png"); ?></span> About Discount</h5>
				  <h1><?php echo h($voucher['Voucher']['title_discount']); ?> Student Discount</h1>
			  </li>
			  <li>
				  <h5><span><?php echo $this->Html->image("icon-1.png"); ?></span> About this discount</h5>
				  <p><?php echo h($voucher['Voucher']['description']); ?></p>
			  </li>
			  <li>
				  <h5><span><?php echo $this->Html->image("icon-2.png"); ?></span> Terms & Conditions</h5>
				  <p><?php echo h($voucher['Voucher']['terms']); ?></p>
			  </li>
			  <li>
				  <h5><span><?php echo $this->Html->image("icon-3.png"); ?></span> Time</h5>
				  <ul>
					  <li>Start : <?php $var =  date_create(h($voucher['Voucher']['start_date'])); echo date_format($var,"jS M, Y"); ?><?php echo ($voucher['Voucher']['day_status']==0 && $voucher['Voucher']['start_hour']!="")? " at ".h($voucher['Voucher']['start_hour']):""; ?></li>
					  <li>End :  <?php $var =  date_create(h($voucher['Voucher']['end_date'])); echo date_format($var,"jS M, Y"); ?><?php echo (h($voucher['Voucher']['day_status']==0 && $voucher['Voucher']['end_hour']!=""))? " at ".h($voucher['Voucher']['end_hour']):""; ?></li>
					
				  </ul>
			  </li>		  
			  
		  </ul>
		</div>
	</div>
</body>
</html>
