<section class="banner">
<div class="container">
<div class="banner-cont">
	<div class="bnr-left">
		<div class="download-app">
			<h1>Discover Deals from your <span>FAVOURITE LOCAL STORES</span></h1>
			<p>Moola delivers the best deals from your favourite retailers right to your phone.</p>

			<div class="application">
				<ul class="list-inline">
					<li><a href="https://play.google.com/store/apps/details?id=com.androidzestminds.moola" target="_blank"> <span class="fa fa-android st-appiocn"></span> <p> available on <span> Google Store </span></p></a></li>
					<li><a href="https://itunes.apple.com/in/app/moolarewards/id1434308130?mt=8" target="_blank"> <span class="fa fa-apple st-appiocn"></span>  <p>available on <span> Apple Store </span></p></a></li>
				</ul>
			</div>
		</div>
	</div>            
	<div class="bnr-right">
		<div class="phone-rt">
			<?php echo $this->Html->Image("phone.png",array("alt"=>"Moola App")); ?>
		</div>
	</div> 
</div>
</div>
</section>
<!--/. banner sec -->
<?php /*
<section class="about-work">
<div class="container">
	<div class="main-ttl">
		 <h2>How <span>Moola</span> works</h2>
	</div>

	<div class="aboutItem-row">
		<div class="about-img half-col">
		  <figure><?php echo $this->Html->image("img-1.png",array("alt"=>"About Moola")); ?></figure>
		</div>
		<div class="about-desc half-col">
			<figure class="abt-icon"><?php echo $this->Html->image("icon1.png",array("alt"=>"About Moola")); ?></figure>
			<h3>Discover Instant offers near you</h3>
			<p>From fashion to food, you’ll have access to all the best student discounts for free.</p>
		</div>
	</div>
	<div class="aboutItem-row "> 
		<div class="about-img half-col">
		  <figure><?php echo $this->Html->image("img-2.png",array("alt"=>"About Moola")); ?></figure>
		</div>

		<div class="about-desc half-col">
			<figure class="abt-icon"><?php echo $this->Html->image("icon1.png",array("alt"=>"About Moola")); ?></figure>
			<h3>Get yourself verified on our app</h3>
			<p>Get yourself verified on our app to discover all the offers nearby your location.</p>
		</div>
	</div>
	<div class="aboutItem-row">
		<div class="about-img half-col">
		  <figure><?php echo $this->Html->image("img-3.png",array("alt"=>"About Moola")); ?></figure>
		</div>
		<div class="about-desc half-col">
			<figure class="abt-icon"><?php echo $this->Html->image("icon1.png",array("alt"=>"About Moola")); ?></figure>
			<h3>In store? Say Moola!</h3>
			<p>Simply use your Moola Student iD to prove you’re a student - no student card necessary. </p>
		</div>                
	</div>
</div>
</section>
<!--/. About sec -->
*/ ?>
<section class="vedio-tutorial"> 
<?php /*
 <div class="video-frame">
	<div class="container">
		<div class="video">
			<?php //echo $this->Html->image("video-img.jpg",array("alt"=>"Moola Video")); ?>
			<iframe width="100%" height="561" src="https://www.youtube.com/embed/3yaffVbW9AI" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
		</div>
	</div>
 </div>
*/ ?>
 <div class="container">      
	<div class="vedio-cont">
		<div class="main-inr-ttl">
			 <!--h2>Student Discount App Demo</h2-->
			 <br/>
			 <h2>Moola your local discovery app</h2>
		</div>
		<!--p>The mobile app presents students with discount vouchers near them now. Students scroll through local deals to find retailers offering the biggest and best deals to make their money stretch further. Students download the app free of charge and verify their student credentials during a simple sign-up process.</p-->
		<p>The <strong>Moola</strong> rewards app was launched in Matlock in the Derbyshire Dales to help locals and tourists <strong>discover the best places to eat, drink and shop in town.</strong> It's free to download and provides local independent retailers with the opportunity to shout about special offers and deals to a wide audience of visitors to the Peak District.</p>
		<p>The Peak district enjoys a thriving tourism economy which attracts 13.25 million visitors per year making it one of the most popular national parks in the UK.</p>
		<p>It reaches into five counties including Derbyshire, Cheshire, Staffordshire, Yorkshire and Great Manchester and is most accessible from Manchester, Sheffield, Derby and Nottingham with an estimated population of 20 million people living with in one hour's journey.</p>
		<p>The app utilises geo-targeting functionality which presents users with details of deals within easy reach of their current location such as <strong>cheap hotel deals in Matlock</strong> as well as the <strong>best restaurant deals and shopping offers.</strong></p>
		<p>There's a vast array of eating opportunities in Matlock which range from casual dinning and pub grub through to formal dinning at locations such as Stone's and Fisher's at Baslow Hall. The moola rewards app provides the opportunity to quickly review what's on offer without the need to trawl through websites and social media. The app tells you with images what's on now, which is perfect as you don't need to plan.</p>
		<p>Local retailers promote <strong>the best pub deals</strong>, club membership, gym membership, <strong>beauty salon special offers</strong> and entertainment from Gullivers Kingdom to The Heights of Abraham and anything in between.</p>
		<p>The need for the app is demonstrated by the number of hotels, bed and breakfast and holiday cottages in the region with over 100 in Matlock serving the vast tourism industry worth over £1.9 billion per year. With so many visitors looking for food, drink, shopping and entertainment the moola rewards app serves as an essential tourists tool.</p>
		<p>Download the <strong>Moola</strong> rewards app to keep up to date with all the latest hotel, restaurant, pub, shopping and entertainment deals in Matlock. You’ll be amazed by what you find and with what Matlock and The Peak District have to offer.
		</p>
	</div>
 </div>
</section>
<!--/. Video sec -->
<!--
<section class="testimonial">
 <div class="container">
	 <div class="main-ttl">
		<h2>testimonials</h2>
	 </div>
	 <div class="tasti-slider owl-carousel owl-theme">
		<div class="item">
			<div class="testi-cation">
			   <div class="testimnl-item">
				   <div class="test-inner">
						<div class="client-desc">
						   <i class="fa fa-quote-left"></i>
						   <div class="test-lead">
								 <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever</p>
							</div>
						   <i class="fa fa-quote-right"></i>
						</div>
						<div class="client-details">
							<figure class="clt-img"><?php echo $this->Html->image("client-img1.png"); ?></figure>
							<div class="clnt-name">
								<h4>John Doe</h4>
								<p>Enterpreneur</p>
							</div>
						</div> 
				   </div>
			   </div>
			</div>
		</div>
		<div class="item">
			<div class="testi-cation">
			   <div class="testimnl-item">
				   <div class="test-inner">
						<div class="client-desc">
						   <i class="fa fa-quote-left"></i>
						   <div class="test-lead">
								 <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever</p>
							</div>
						   <i class="fa fa-quote-right"></i>
						</div>
						<div class="client-details">
							<figure class="clt-img"><?php echo $this->Html->image("client-img2.png"); ?></figure>
							<div class="clnt-name">
								<h4>Mark Thomas</h4>
								<p>Enterpreneur</p>
							</div>
						</div>
				   </div>
			   </div>
			</div>
		 </div> 
		 
		 <div class="item">
		   <div class="testi-cation">
			   <div class="testimnl-item">
				   <div class="test-inner">
						<div class="client-desc">
						   <i class="fa fa-quote-left"></i>
						   <div class="test-lead">
								 <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever</p>
							</div>
						   <i class="fa fa-quote-right"></i>
						</div>
						<div class="client-details">
							<figure class="clt-img"><?php echo $this->Html->image("client-img1.png"); ?></figure>
							<div class="clnt-name">
								<h4>John Doe</h4>
								<p>Enterpreneur</p>
							</div>
						</div> 
				   </div>
			   </div>
			</div>
		</div>
		<div class="item">
			<div class="testi-cation">
			   <div class="testimnl-item">
				   <div class="test-inner">
						<div class="client-desc">
						   <i class="fa fa-quote-left"></i>
						   <div class="test-lead">
								 <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever</p>
							</div>
						   <i class="fa fa-quote-right"></i>
						</div>
						<div class="client-details">
							<figure class="clt-img"><?php echo $this->Html->image("client-img2.png"); ?></figure>
							<div class="clnt-name">
								<h4>Mark Thomas</h4>
								<p>Enterpreneur</p>
							</div>
						</div>
				   </div>
			   </div>
			</div>
		 </div> 
		 
	 </div>
 </div>
</section>
-->
