 <section class="inner-banner topheader"	>
       <div class="container">
	        <div class="banner-cont"> 
            	<h2>FAQ</h2>
                <h5>Frequently Asked questions</h5>
	        </div>
       </div>
    </section>
    <!--/. banner sec -->
    
    <section class="faqMain">
    	<div class="container">
        	<div class="row">
            	<div class="col-sm-12">
                	<div class="page-title">
                	<h2>Questions? Look Here.</h2>
                </div>
                </div>                
				<div class="col-md-8">
						<div class="faq-question">
							<div class="lcmain">
								<ul class="list-project">
									<li class="parent active"><a href="javascript:void(0)"> <?php echo strtoupper("Q. How does moolarewards work?" );?></a> <span class="arrow-slide"></span>
										<div class="sub-lising" style="display:block">
											<p>
											Moola provides you with cost effective, targeted advertising to customers. Sign up for a free account, create vouchers with our easy to use editing tools and publish. Customers see your vouchers on the moola mobile app when they’re near your venue. Create as many discount vouchers as you like for different times of the day and different days of the week. Fill those slow periods with happy customers. </p>
										</div>                                        
									</li>
										<li class="parent"><a href="javascript:void(0)"> <?php echo strtoupper("Q. Is moolarewards free to try?" );?></a> <span class="arrow-slide"></span>
										<div class="sub-lising">
											<p>
											Yes, signing up to moola is 100% free, no credit card required.
You can create drafts, invite team members to view vouchers you create and get a sense of what moola has to offer. </p>
										</div>                                        
									</li>
									<li class="parent"><a href="javascript:void(0)"> <?php echo strtoupper("Q. Is this a secure site for purchases?" );?></a> <span class="arrow-slide"></span>
										<div class="sub-lising" >
											<p>
											Absolutely! We work with PayPal which guarantees your safety and security. All site communication and data transfer are secured and encrypted. </p>
										</div>                                        
									</li>
									<li class="parent"><a href="javascript:void(0)"> <?php echo strtoupper("Q. How Can I reach your Customer Support?" );?></a> <span class="arrow-slide"></span>
										<div class="sub-lising" >
											<p>
											To reach our customer support team please email <a  href="mailto:help@moolarewards.co.uk" style="color:#606870">help@moolarewards.co.uk </a>our team will be happy to help you and bring a smile to your face :) </p>
										</div>                                        
									</li>	
									<li class="parent"><a href="javascript:void(0)"> <?php echo strtoupper("Q. Can I cancel my plan anytime? " );?></a> <span class="arrow-slide"></span>
										<div class="sub-lising" >
											<p>
											Monthly subscriptions are billed month to month. No contract. No obligation. You can cancel your monthly subscription at any time. We hope you'll fall in love with moola and never want to leave (-: </p>
										</div>                                        
									</li>	
									<li class="parent"><a href="javascript:void(0)"> <?php echo strtoupper("Q. What's the pricing model? " );?></a> <span class="arrow-slide"></span>
										<div class="sub-lising">
											<p>
											It's really simple. We offer unlimited vouchers for £9.99 + VAT per month, per location. </p>
										</div>                                        
									</li>	
										<li class="parent"><a href="javascript:void(0)"> <?php echo strtoupper("Q. Do you save credit card details? " );?></a> <span class="arrow-slide"></span>
										<div class="sub-lising" >
											<p>
											No. Absolutely not. Your credit card details are not stored on our servers. The information goes directly from your computer to our payment service provider.</p>
										</div>                                        
									</li>					 
								</ul>
							 </div>
						</div>
				 </div>
				 <div class="col-md-4">
					<div class="call-us">
						<div class="send-msg">
								<h3>Can't find the answer you are looking for?</h3>								
								<a href="mailto:help@moolarewards.co.uk" class="send-btn">Send us a message</a>
						</div>
					</div>
				 </div>                
            </div>
        </div>
    </section>
