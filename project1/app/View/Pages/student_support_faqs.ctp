 <section class="inner-banner topheader"	>
       <div class="container">
	        <div class="banner-cont"> 
            	<h2>FAQ</h2>
                <h5>Frequently Asked questions</h5>
	        </div>
       </div>
    </section>
    <!--/. banner sec -->
    
    <section class="faqMain">
    	<div class="container">
        	<div class="row">
            	<div class="col-sm-12">
                	<div class="page-title">
                	<h2>FAQ moola rewards</h2>
                </div>
                <div class="col-sm-12">                	
                	<h2>Students</h2>                
                </div>                
				<div class="col-md-8">
						<div class="faq-question">
							<div class="lcmain">
								<ul class="list-project">
									<li class="parent active"><a href="javascript:void(0)"> <?php echo strtoupper("Q. I haven’t received a verification email.");?></a> <span class="arrow-slide"></span>
										<div class="sub-lising" style="display:block">
											<p>First, make sure you've spelt your email correctly, if you find that you haven't, click 'Change email', enter your email correctly and hit submit.</p><br/>

											<p>If your email is correct, first check your spam folder to make sure it isn't hiding in there.
											</p><br/>
											<p>If you still haven't received it, you can simply send us an email from your institution email address to <a href="mailto:support@moolarewards.co.uk">support@moolarewards.co.uk</a><br/><br/></p>
										</div>                                        
									</li>
									<li class="parent"><a href="javascript:void(0)"> <?php echo strtoupper("Q. Don’t have an institution email?");?></a> <span class="arrow-slide"></span>
									<div class="sub-lising">
											<p>Many universities & colleges give students an email address on their domain (e.g. you@university.ac.uk).<br/>

											If you don't have access to an institution portal or academic email then we can accept official documentation as evidence of your student status.<br/>

											Please email copies of the following to <a href="mailto:support@moolarewards.co.uk">support@moolarewards.co.uk</a>
											<br/>
											- Institution student card<br/>
											- Acceptance letter<br/>
											- Academic transcript<br/>
											</p>
											<br/>
											Documentation will only be accepted as proof of your student status if it contains your full name, your institution's name and a date that indicates you're currently enrolled as a student at the present time.
											<br/>
											Unfortunately we can not accept student cards that aren't issued by an academic institution.
											<br/>
											If your documentation meets the above criteria your account will be verified within 3 days. You will receive a notification via email once your documents have been reviewed.<br/><br/>
											</p>
										</div>
									</li>
									<li class="parent"><a href="javascript:void(0)"> <?php echo strtoupper("Q. Institution email address invalid?");?></a> <span class="arrow-slide"></span>
									<div class="sub-lising">
											<p>You will need to enter a unique, valid email address that's specific to your educational institution.<br/>

											If an email address issued to you by your institution isn't accepted then please contact our support team through <a href="mailto:support@moolarewards.co.uk">support@moolarewards.co.uk</a><br/><br/></p>
										</div>
									</li>
									<li class="parent"><a href="javascript:void(0)"> <?php echo strtoupper("Q. Will my course or institution be accepted?");?></a> <span class="arrow-slide"></span>
									<div class="sub-lising">
											<p>Our discounts are limited to students who study full-time and attend an institution. Select online or part-time courses are accepted if they offer a recognised qualification.<br/>

											Courses we accept should offer one of the following:<br/>
											AS and A levels/Highers and Advanced Highers<br/>
											Bachelor's degrees (BA, BSc)<br/>
											Diplomas<br/>
											GCSEs/National 5s<br/>
											HNCs HNDs<br/>
											or similar qualifications<br/>

											If you are unsure, you can attempt to sign up and you will either be accepted or rejected.<br/>

											Final decisions are up to Moola Rewards and made at our discretion.<br/><br/>
											</p>
										</div>
									</li>
									<li class="parent"><a href="javascript:void(0)"> <?php echo strtoupper("Q. I haven't received an email to reset my password.");?></a> <span class="arrow-slide"></span>
									 <div class="sub-lising">
											<p>First check your spam or junk email folder. If it's not hiding in there then please add hello@moolarewasrds.co.uk to your email address book and try again. This will ensure our emails don't get blocked.<br/>

											If you're still having trouble please contact our support team <a href="mailto:support@moolarewards.co.uk"> (support@moolarewards.co.uk) </a> . <br/> <br/> </p>
											
										</div>
									 </li>
									<li class="parent"><a href="javascript:void(0)"> <?php echo strtoupper("Q. I can’t log in or reset my password.");?></a> <span class="arrow-slide"></span>
									 <div class="sub-lising">
											<p>You might have misspelt your email address when you first signed up to moola rewards. <br/>

											Please contact our support team <a href="mailto:support@moolarewards.co.uk">(support@moolarewards.co.uk)</a> who'll be happy to help.<br/><br/></p>
										</div>
									 </li>	
									 <li class="parent"><a href="javascript:void(0)"> <?php echo strtoupper("Q. I entered my institution email incorrectly.");?></a> <span class="arrow-slide"></span>
									 <div class="sub-lising">
											<p>No problem. We hardly hold any details about you at all, so delete the app and install again.<br/><br/></p>
										</div>
									 </li>
									  <li class="parent"><a href="javascript:void(0)"> <?php echo strtoupper("Q. There are no discounts near me.");?></a> <span class="arrow-slide"></span>
									 <div class="sub-lising">
											<p>We are growing day by day as more and more retailers sign-up to advertise with us. If your favourite retailer hasn’t joined in yet, please ask them to do so. Tell them to advertise with us (link ‘advertise with us’ to the merchant advertising page) and they should be very grateful. It’s only £9.99 + VAT per month to advertise unlimited vouchers. Use this phrase ‘ My generosity to you should be rewarded as I’ve put you on the student map’.
											<br/><br/></p>
										</div>
									 </li>											 
								</ul>
							 </div>
						</div>
				 </div>
				 <div class="col-md-4">
					<div class="call-us">
						<div class="send-msg">
								<h3>Can't find the answer you are looking for?</h3>
								
								<a href="mailto:help@moolarewards.co.uk" class="send-btn">Send us a message</a>
						</div>
					</div>
				 </div>                
            </div>
        </div>
    </section>
