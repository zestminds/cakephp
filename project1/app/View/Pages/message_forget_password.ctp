<script>
 $(document).ready(function () { 
  window.setTimeout(function(){
	// Move to a new location or you can do something else
	window.location.href = SITE_LINK+"advertise-with-moola";
	}, 7000);
});
</script>
<section class="msg-banner">
    	<div class="container">
        	<div class="msg-content">     
				<h2>Request Sent Successfully </h2>
                <p>A link for resetting your password was sent to the following email address</p>           
                <p><?php echo $this->Session->flash(); ?></h2> 
                <p>Please follow the link to reset your password.</p>
            </div>
        </div>
</section>
