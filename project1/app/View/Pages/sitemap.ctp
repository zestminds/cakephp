<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">
<?php $baseUrl = "https://www.moolarewards.co.uk/"; ?>
<url><loc>https://www.moolarewards.co.uk/</loc><changefreq>always</changefreq><priority>1.00</priority></url>
<url><loc>https://www.moolarewards.co.uk/advertise-with-moola</loc><changefreq>always</changefreq><priority>0.85</priority></url>
<url><loc>https://www.moolarewards.co.uk/about</loc><changefreq>always</changefreq><priority>0.85</priority></url>
<url><loc>https://www.moolarewards.co.uk/contact_us</loc><changefreq>always</changefreq><priority>0.85</priority></url>
<url><loc>https://www.moolarewards.co.uk/help</loc><changefreq>always</changefreq><priority>0.85</priority></url>
<url><loc>https://www.moolarewards.co.uk/student_support_faqs</loc><changefreq>always</changefreq><priority>0.85</priority></url>
<url><loc>https://www.moolarewards.co.uk/terms_and_conditions</loc><changefreq>always</changefreq><priority>0.85</priority></url>
<url><loc>https://www.moolarewards.co.uk/privacy_policy</loc><changefreq>always</changefreq><priority>0.85</priority></url>
<url><loc>https://www.moolarewards.co.uk/cookie_policy</loc><changefreq>always</changefreq><priority>0.85</priority></url>
<url><loc>https://www.moolarewards.co.uk/pricing</loc><changefreq>always</changefreq><priority>0.69</priority></url>
<url><loc>https://www.moolarewards.co.uk/advertiser_faqs</loc><changefreq>always</changefreq><priority>0.69</priority></url>
<?php $i= 0; foreach($urls as $key=>$val) {  ?>
<url><loc><?php echo $baseUrl.urlencode($val); ?></loc><changefreq>always</changefreq><priority>0.69</priority></url>
<?php }  ?>
</urlset>
