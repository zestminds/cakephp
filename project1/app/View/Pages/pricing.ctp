 <section class="inner-banner topheader">
       <div class="container">
	        <div class="banner-cont"> 
	        </div>
       </div>
    </section>
    <!--/. banner sec -->
    
    <section>
        <div class="container">
            <div class="pricing-card">
                <div class="pricing-plan">
                    <div class="price-crcl">
                       <p>PRICING PLAN</p>
                        <h2><span>£</span> <?php echo $plan['Plan']['price']; ?> <small>+VAT /mo</small></h2>
                    </div>
                    <div class="plan-desc">
                        <?php //pr($plan); ?>
						<?php echo $plan['Plan']['features']; ?>
                    </div>
                </div>
                <div class="price-desc">
                    <p><?php echo $plan['Plan']['description']; ?></p>
                </div>                 
            </div>  
            <div class="price-feature">
                <ul>
                    <li>
                        <div class="priceCard">
                            <div class="p-icon">
								<?php echo $this->Html->image("price-icon1.png",array("alt"=>"Unlimited Vouchers")); 	 ?>                               
                            </div>
                            <p>Unlimited Vouchers</p>
                        </div>
                    </li>
                    <li>
                        <div class="priceCard">
                            <div class="p-icon">
								<?php echo $this->Html->image("price-icon2.png",array("alt"=>"Know your audience"));  ?>
                            </div>
                            <p>Know your audience</p>
                        </div>
                    </li>
                    <li>
                        <div class="priceCard">
                            <div class="p-icon">
                                <?php echo $this->Html->image("price-icon3.png",array("alt"=>"Insights"));  ?>
                            </div>
                            <p>Insights</p>
                        </div>
                    </li>
                    <li>
                        <div class="priceCard">
                            <div class="p-icon">
                                <?php echo $this->Html->image("price-icon4.png",array("alt"=>"Facebook integration")); ?>
                            </div>
                            <p>Facebook integration</p>
                        </div>
                    </li>
                </ul>
				<?php if (!$this->Session->read("Auth.User.id")) { ?>
                <div class="try-free">
                    <a class="btn-gradint" href="#regModel" data-toggle="modal">TRY FOR FREE</a>
                </div>
				<?php } else { ?>
					<br/>
				<?php } ?>
            </div>
        </div>
    </section>
    <!--/. My Account -->
