<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8_bin">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<?php if (isset($title_for_layout)) {  ?>
	<title><?php echo $title_for_layout; ?></title>
	
<?php } else { ?>
<title><?php echo $title_for_layout; ?></title>
<?php } ?>
<?php if (isset($keywords)) {  ?>
	<meta name="keywords" content="<?php echo $keywords; ?>" />
<?php } else { ?>
	<meta name="keywords" content="moola rewards" />
<?php } ?>
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<!-- css -->
<?php 
echo $this->Html->meta('icon', $this->Html->url(SITE_LINK.'img/fav_icon.png'));
echo $this->Html->css(array("moola_cart/bootstrap.min","moola_cart/font-awesome.min","moola_cart/owl.carousel","moola_cart/style","moola_cart/responsive","internal"));
echo $this->Html->script(array('moola/jquery-2-2-4.min'));
 ?>
<script>
var SITE_LINK = "<?php echo SITE_LINK; ?>";

</script>
<!--[if IE]>
	<script src="js/html5shiv.js"></script>
<![endif]-->
<meta name="google-site-verification" content="kxv_CaLEubj-OwFp-sEvKGm4O_Sd7S00bCKbL9gRaTY" />
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window, document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '276239136433506');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=276239136433506&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->
</head>
<body class="my-voucher" style="margin:0;">
<div class="main" > 
  <!-- Preloader -->
  <div class="preLoader"></div>
	<?php echo $this->Session->flash(); ?>	
	<?php if ( !$this->Session->read("Auth.User.id") ) {
		echo $this->element("moolaheaderwithoutlogin"); 
    }
    else{
		echo $this->element("moolaheaderwithlogin"); 
	}
    ?>
    <!-- End header sec -->

    <?php echo $content_for_layout; ?>

    <?php echo $this->element("moolafooterwithoutlogin"); ?>
    
<?php echo $this->Html->script(array("moola/bootstrap.min","moola/owl.carousel","moola/custom","moola/moment","change_password","moola/popper.min")); ?>
<?php echo $this->element("jscssloader",array("js"=>$jsArray,"css"=>$cssArray)); ?>

<?php if ( $this->params['controller'] == 'vouchers' ) { ?>	
<script src="<?php echo SITE_LINK ?>js/app.js"></script>
<script src="<?php echo SITE_LINK ?>js/bootstrap-tagsinput.js"></script>
<script>
 
$(document).ready(function(){		
		
	$("div.weekdays").children("label").on("click",function(){	
		if ($("#repeat_onW").is(":checked")) { 
		$(this).toggleClass("selected");
		}
		});		
	$(document).on("click",".weekoption",function()
	{	
		if ($(this).val() == "W") {
			$(".weekdays").each(function(){
			$(this).children("input").removeAttr("disabled");
			});
		} else {
			$(".weekdays").each(function(){
			$(this).children("label").removeClass("selected");				
			$(this).children("input").attr("disabled","DISABLED");
			});
		}	
	});
	$(window).keydown(function(event){
		if(event.keyCode == 13) {
		  event.preventDefault();
		  return false;
		}
	});
	$("#VoucherStartHour").val($("#tmpstarthr").val());
	$("#VoucherEndHour").val($("#tmpendhr").val());
	
	
	
	
});

  </script>

<?php } ?>
<?php if ( $this->params['controller'] == 'pages' && $this->params['action'] == 'advertiser_faqs' || $this->params['action'] == 'student_support_faqs') { ?>
<script>
	$('.parent > a').click(function() {
		$(this).parent('.parent').siblings().removeClass('active');
		$(this).parent('.parent').toggleClass('active');
		$(this).parent('.parent').siblings().children('.sub-lising').slideUp();
        $(this).parent('.parent').children('.sub-lising').slideToggle();
    });
</script>	
<?php } 

if ( $this->params['controller'] == 'Locations' ) { ?>	
	<link href="<?php echo SITE_LINK ?>datetime/bootstrap/css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
<script type="text/javascript" src="<?php echo SITE_LINK ?>js/moola/datetime-picker-4-7-14-min.js"></script>
	<script type="text/javascript">
   $(function() {
	  $('.datetimepicker1').datetimepicker({
          //format: "DD/MM/YYYY"
          format: "YYYY-MM-DD",
         // minDate:new Date()
	  });
	  $('.datetimepicker2').datetimepicker({
           format: 'LT'
	  });
	});
</script>	
<?php } 

 if ( $this->params['controller'] == 'pages' )
 {?>
	 <script src="https://www.google.com/recaptcha/api.js?onload=CaptchaCallback&render=explicit" async defer ></script>
<?php }?> 
<script>
	
//~ var myVar;
//~ 
//~ function myFunction() {
    //~ myVar = setTimeout(showPage, 1000);
//~ }

</script>
 <?php echo $this->element('sql_dump'); ?>
</body>
</html>
