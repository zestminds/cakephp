<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Moola : Coming Soon</title>
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<!-- css -->
<?php 
echo $this->Html->meta('icon', $this->Html->url(SITE_LINK.'img/fav_icon.png'));
echo $this->Html->css(array("moola/bootstrap.min","moola/font-awesome.min","moola/owl.carousel","moola/style","moola/responsive","internal"));
echo $this->Html->script(array("moola/jquery-2-2-4.min","jquery.validate.min"));
?>

<script>
var SITE_LINK = "<?php echo SITE_LINK; ?>";
</script>
<!--[if IE]>
	<script src="js/html5shiv.js"></script>
<![endif]-->
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window, document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '276239136433506');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=276239136433506&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->
</head>
<body>
    <?php echo $content_for_layout; ?>
<?php echo $this->Html->script(array("moola/bootstrap.min","moola/owl.carousel","moola/custom","moola/moment")); ?>

</body>
</html>
