<!DOCTYPE html>
<html lang="en">
<head>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-NC8FH9Z');</script>
<!-- End Google Tag Manager -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<?php if (isset($title_for_layout)) {  ?>
	<title><?php echo $title_for_layout; ?></title>
	
<?php } else { ?>
<title><?php echo $title_for_layout; ?></title>

<?php } ?>
<meta name="google-site-verification" content="BkSs3My_0HPG5U-j4GailFxRkPGyxYsLm0uWz4Y8zNQ" />
<?php if (isset($keywords)) {  ?>
	<meta name="keywords" content="<?php echo $keywords; ?>" />
<?php } else { ?>
	<meta name="keywords" content="moola rewards" />
<?php } ?>
<?php if (isset($description)) {  ?>
	<meta name="Description" content="<?php echo $description; ?>">
<?php } else { ?>
	<meta name="Description" content="Best Deals For Students In Uk - The best mobile app moola rewards will provide discount vouchers for students nearby areas, Students can find nearby local retailers.">
<?php } ?>
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<!-- css -->
<?php 

echo $this->Html->meta('icon', $this->Html->url(SITE_LINK.'img/fav_icon.png'));
echo $this->Html->css(array("moola/bootstrap.min","moola/font-awesome.min","moola/owl.carousel","moola/style","moola/responsive","internal")); 
echo $this->Html->script(array('moola/jquery-2-2-4.min'));
?>
<script type='text/javascript' src='//platform-api.sharethis.com/js/sharethis.js#property=5bead063da105300172a30f0&product=inline-share-buttons' async defer></script>
<script>
var SITE_LINK = "<?php echo SITE_LINK; ?>";
</script>
<!--[if IE]>
	<script src="js/html5shiv.js"></script>
<![endif]-->
<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window, document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '276239136433506');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=276239136433506&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->
<meta name="p:domain_verify" content="caf90dce541ad73f72cd864ac8a6f4a8"/>
</head>
<body class="home sm-container" style="padding-right:0 !important;">
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NC8FH9Z"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<div class="main mola-mrchent"> 
  <!-- Preloader -->
  <div class="preLoader"></div>
	<?php echo $this->Session->flash(); ?>	
	<?php if ( !$this->Session->read("Auth.User.id") ) {
		echo $this->element("moolaheaderwithoutlogin"); 
    }
    else{
		echo $this->element("moolaheaderwithlogin"); 
	}
    ?>
    <!-- End header sec -->

    <?php echo $content_for_layout; ?>

    <?php echo $this->element("moolafooterwithoutlogin"); ?>
    
<?php echo $this->Html->script(array("moola/bootstrap.min","moola/owl.carousel","moola/custom")); ?>
<script src="https://www.google.com/recaptcha/api.js?onload=CaptchaCallback&render=explicit" async defer></script>
<?php echo $this->element("jscssloader",array("js"=>$jsArray,"css"=>$cssArray)); ?>

<?php if ( $this->params['controller'] == 'vouchers' ) { ?>
<script type="text/javascript" src="<?php echo SITE_LINK ?>datetime/bootstrap/js/bootstrap-datetimepicker.min.js"></script>
<script src="<?php echo SITE_LINK ?>js/bootstrap-tagsinput.js"></script>    
<script>
 
$(document).ready(function(){		
		
		$("div.weekdays").children("label").on("click",function(){			
			$(this).toggleClass("selected");
		});			
		
		$(document).on("click",".weekoption",function()
		{	
			if ($(this).val() == "W") {
				$(".weekdays").each(function(){
				$(this).children("input").removeAttr("disabled");
				});
			} else {
				$(".weekdays").each(function(){
				$(this).children("input").attr("disabled","DISABLED");
				});
			}	
		});
$(window).keydown(function(event){
    if(event.keyCode == 13) {
      event.preventDefault();
      return false;
    }
  });

});

  </script>

<?php } ?>
<?php if ( $this->params['controller'] == 'Vouchers' ) { ?>
<script>
$('.discounts-slide .owl-carousel').owlCarousel({
    loop:true,
    margin:0,
    nav:false,
	dots:true,
    autoplay:true,
    autoplayTimeout:5000,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:1
        }
    }
})
</script>
<?php } ?>
<!-- Global site tag (gtag.js) - Google Analytics ->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-131170044-1"></script>
<script>
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());

gtag('config', 'UA-131170044-1');
</script-->
 <?php echo $this->element('sql_dump'); ?>
</body>
</html>
