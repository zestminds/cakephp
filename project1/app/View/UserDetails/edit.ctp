<?php echo $this->element("topmenu"); ?>
<div class="userDetails form">
<?php echo $this->Form->create('UserDetail'); ?>
	<fieldset>
		<legend><?php echo __('Edit User Detail'); ?></legend>
	<?php
		echo $this->Form->input('id');		
		echo $this->Form->input('name');		
		echo $this->Form->input('user_type_id');
		echo $this->Form->input('longitude');
		echo $this->Form->input('latitude');
		echo $this->Form->input('is_active');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<?php echo $this->element("leftmenu_admin"); ?>
