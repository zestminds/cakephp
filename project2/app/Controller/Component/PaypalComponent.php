<?php

class PaypalComponent extends Component {
	
	var $PAYPAL_USER = PAYPAL_USER_SANDBOX;
	var $PAYPAL_PWD = PAYPAL_PWD_SANDBOX;
	var $PAYPAL_SIGNATURE = PAYPAL_SIGNATURE_SANDBOX;
	var $returnurl = "";
	var $cancelurl = "";
	var $status = false;
	var $redirectURL = "";
	
	function __construct() {
		if ( PAYMENT_MODE == 'LIVE' ) {
			$this->PAYPAL_USER = PAYPAL_USER_LIVE;
			$this->PAYPAL_PWD = PAYPAL_PWD_LIVE;
			$this->PAYPAL_SIGNATURE = PAYPAL_SIGNATURE_LIVE;
		}
	}
	
	function initializePayment($payment = NULL) {
		
		$curl = curl_init();
 
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_POST, true);
		if ( PAYMENT_MODE == 'LIVE' ) {
			curl_setopt($curl, CURLOPT_URL, 'https://api-3t.paypal.com/nvp');
		} else {
			curl_setopt($curl, CURLOPT_URL, 'https://api-3t.sandbox.paypal.com/nvp');
		}
		curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query(array(
			'USER' => $this->PAYPAL_USER,
			'PWD' => $this->PAYPAL_PWD,
			'SIGNATURE' => $this->PAYPAL_SIGNATURE,
		 
			'METHOD' => 'SetExpressCheckout',
			'VERSION' => '108',
			'LOCALECODE' => 'en_GB',
		 
			'PAYMENTREQUEST_0_AMT' => $payment,
			'PAYMENTREQUEST_0_CURRENCYCODE' => 'GBP',
			'PAYMENTREQUEST_0_PAYMENTACTION' => 'Sale',
			'PAYMENTREQUEST_0_ITEMAMT' => $payment,
		 
			'L_PAYMENTREQUEST_0_NAME0' => 'Voucher Listing Service Subscription',
			'L_PAYMENTREQUEST_0_DESC0' => 'Test Payment',
			'L_PAYMENTREQUEST_0_QTY0' => 1,
			'L_PAYMENTREQUEST_0_AMT0' => $payment,

			'L_BILLINGTYPE0' => 'RecurringPayments',
			'L_BILLINGAGREEMENTDESCRIPTION0' => 'Voucher Listing Service Subscription',
		 
			'CANCELURL' => $this->cancelurl,
			'RETURNURL' => $this->returnurl
		)));
		 
		$response =    curl_exec($curl);
		 
		curl_close($curl);
		 
		$nvp = array();
		 
		if (preg_match_all('/(?<name>[^\=]+)\=(?<value>[^&]+)&?/', $response, $matches)) {
			foreach ($matches['name'] as $offset => $name) {
				$nvp[$name] = urldecode($matches['value'][$offset]);
			}
		}
		
		if (isset($nvp['ACK']) && $nvp['ACK'] == 'Success') {
			$query = array(
				'cmd'    => '_express-checkout',
				'token'  => $nvp['TOKEN']
			);
			if (PAYMENT_MODE == "SANDBOX") { 
				$this->redirectURL = sprintf('https://www.sandbox.paypal.com/cgi-bin/webscr?%s', http_build_query($query));
			} else {
				$this->redirectURL = sprintf('https://www.paypal.com/cgi-bin/webscr?%s', http_build_query($query));
			}
			
			$this->status = true;
		} 
		
		
	}
	
	function createrecurringProfile($payment = NULL,$token = NULL,$payerid = NULL) {
		
		
		$curl = curl_init();
 
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($curl, CURLOPT_POST, true);
		if ( PAYMENT_MODE == 'LIVE' ) {
			curl_setopt($curl, CURLOPT_URL, 'https://api-3t.paypal.com/nvp');
		} else {
			curl_setopt($curl, CURLOPT_URL, 'https://api-3t.sandbox.paypal.com/nvp');
		}
		curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query(array(
			'USER' => $this->PAYPAL_USER,
			'PWD' => $this->PAYPAL_PWD,
			'SIGNATURE' => $this->PAYPAL_SIGNATURE,
		 
			'METHOD' => 'CreateRecurringPaymentsProfile',
			'VERSION' => '108',
			'LOCALECODE' => 'en_GB',
		 
			'TOKEN' => $token,
			'PayerID' => $payerid,
		 
			'PROFILESTARTDATE' => date("Y-m-d H:i:s"),
			'DESC' => 'Voucher Listing Service Subscription',
			'BILLINGPERIOD' => 'Month',
			'BILLINGFREQUENCY' => '1',
			'AMT' => $payment,
			'CURRENCYCODE' => 'GBP',
			'COUNTRYCODE' => 'GB',
			'MAXFAILEDPAYMENTS' => 3
		)));
		 
		$response =    curl_exec($curl);
		 
		curl_close($curl);
		 
		$nvp = array();
		 
		if (preg_match_all('/(?<name>[^\=]+)\=(?<value>[^&]+)&?/', $response, $matches)) {
			foreach ($matches['name'] as $offset => $name) {
				$nvp[$name] = urldecode($matches['value'][$offset]);
			}
		}
		return ($nvp);
		
		
	}
	
	
	
	
	
	/**
	 * Performs an Express Checkout NVP API operation as passed in $action.
	 *
	 * Although the PayPal Standard API provides no facility for cancelling a subscription, the PayPal
	 * Express Checkout  NVP API can be used.
	 $action can be Cancel, Suspend, Reactivate
	 
	*/
	function get_subscription_status( $profile_id ) {
	 
		$api_request = 'USER=' . urlencode( $this->PAYPAL_USER )
					.  '&PWD=' . urlencode( $this->PAYPAL_PWD )
					.  '&SIGNATURE=' . urlencode( $this->PAYPAL_SIGNATURE )
					.  '&VERSION=76.0'
					.  '&METHOD=GetRecurringPaymentsProfileDetails'
					.  '&PROFILEID=' . urlencode( $profile_id )
					.  '&NOTE=' . urlencode( 'Profile cancelled at store' );
	 
		$ch = curl_init();
		if ( PAYMENT_MODE == 'LIVE' ) {
			curl_setopt($ch, CURLOPT_URL, 'https://api-3t.paypal.com/nvp');
		} else {
			curl_setopt($ch, CURLOPT_URL, 'https://api-3t.sandbox.paypal.com/nvp');
		}
		curl_setopt( $ch, CURLOPT_VERBOSE, 1 );
	 
		// Uncomment these to turn off server and peer verification
		curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, FALSE );
		curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, FALSE );
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
		curl_setopt( $ch, CURLOPT_POST, 1 );
	 
		// Set the API parameters for this transaction
		curl_setopt( $ch, CURLOPT_POSTFIELDS, $api_request );
	 
		// Request response from PayPal
		$response = curl_exec( $ch );
		//pr($response);
		//die("here");
		// If no response was received from PayPal there is no point parsing the response
		if( ! $response )
			//die( 'Calling PayPal to change_subscription_status failed: ' . curl_error( $ch ) . '(' . curl_errno( $ch ) . ')' );
		
		
		$nvp = array();
		 
		if (preg_match_all('/(?<name>[^\=]+)\=(?<value>[^&]+)&?/', $response, $matches)) {
			foreach ($matches['name'] as $offset => $name) {
				$nvp[$name] = urldecode($matches['value'][$offset]);
			}
		}
		return $nvp;
	}
	
	function change_subscription_status( $profile_id, $action ) {
	 
		$api_request = 'USER=' . urlencode( $this->PAYPAL_USER )
					.  '&PWD=' . urlencode( $this->PAYPAL_PWD )
					.  '&SIGNATURE=' . urlencode( $this->PAYPAL_SIGNATURE )
					.  '&VERSION=76.0'
					.  '&METHOD=ManageRecurringPaymentsProfileStatus'
					.  '&PROFILEID=' . urlencode( $profile_id )
					.  '&ACTION=' . urlencode( $action )
					.  '&NOTE=' . urlencode( 'Profile cancelled at store' );
	 
		$ch = curl_init();
		if ( PAYMENT_MODE == 'LIVE' ) {
			curl_setopt($ch, CURLOPT_URL, 'https://api-3t.paypal.com/nvp');
		} else {
			curl_setopt($ch, CURLOPT_URL, 'https://api-3t.sandbox.paypal.com/nvp');
		}
		curl_setopt( $ch, CURLOPT_VERBOSE, 1 );
	 
		// Uncomment these to turn off server and peer verification
		curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, FALSE );
		curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, FALSE );
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
		curl_setopt( $ch, CURLOPT_POST, 1 );
	 
		// Set the API parameters for this transaction
		curl_setopt( $ch, CURLOPT_POSTFIELDS, $api_request );
	 
		// Request response from PayPal
		$response = curl_exec( $ch );
		//pr($response);
		//die("here");
		// If no response was received from PayPal there is no point parsing the response
		if( ! $response )
			//die( 'Calling PayPal to change_subscription_status failed: ' . curl_error( $ch ) . '(' . curl_errno( $ch ) . ')' );
		
		
		$nvp = array();
		 
		if (preg_match_all('/(?<name>[^\=]+)\=(?<value>[^&]+)&?/', $response, $matches)) {
			foreach ($matches['name'] as $offset => $name) {
				$nvp[$name] = urldecode($matches['value'][$offset]);
			}
		}
		return $nvp;
	}
}
