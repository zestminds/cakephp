<?php
App::uses('Component','Controller','AppController');

require_once '../Vendor/autoload.php';
use Firebase\JWT\JWT;

		class NotificationV1Component extends Component {
		var $requestMethod = '';
		var $variables = '';
		var $obj = '';
		var $userId = '';
		var $result = array("status"=>200);
		var $siteLink = '';
		var $User = '';
		
		public function beforeFilter(Event $event) {
			parent::beforeFilter($event); 
		}
		
		function __construct($args) {
			parent::__construct($args);
			$this->requestMethod = $_SERVER['REQUEST_METHOD'];
			$this->User = ClassRegistry::init('User');
			$this->Calendar = ClassRegistry::init('Calendar');
			$obj = $this;			
			
		}		
		
		
		
		// App user add  comment
		function fetch_notifications() {			
			if ($this->requestMethod == 'GET' ) {				
				$temp['Notification'] = $this->variables->query;				
				$user_id= $this->userId;
				if ($user=$this->User->find("first",array("conditions"=>array("User.id"=>$user_id))))	
				 { 													
					$this->conditions=array("Notification.user_id"=>$user_id,"User.is_active"=>1);
					$this->Notification = ClassRegistry::init('Notification');							
					$this->Notification->hasOne = $this->Notification->belongsTo = $this->Notification->hasMany = array();
					$this->Notification->belongsTo = array(
							"User"=>array(
								"className"=>"User",
								"foreignKey"=>false,
								"type"=>"Inner",
								"conditions"=>"User.id=Notification.sender_id"
							),
							"UserDetail"=>array(
								"className"=>"UserDetail",
								"foreignKey"=>false,
								"type"=>"Inner",
								"conditions"=>"UserDetail.user_id=Notification.sender_id"
							)
						);	
						$page = (isset($temp['Notification']['page']) && !empty($temp['Notification']['page']) )?$temp['Notification']['page']:1;
						$limit = 20;
						$notifications=$this->Notification->find("all",array("conditions"=>$this->conditions,"limit"=>$limit,"page"=>$page,"order"=>"Notification.created desc",'fields'=>array("Notification.id","Notification.sender_id","Notification.comment_id","Notification.text","Notification.type","Notification.event_id","Notification.comment_add","Notification.created","UserDetail.image","UserDetail.fbimage","UserDetail.business","User.identifier","User.user_type_id","UserDetail.see_my_profile")));	
						$i=0;	
						$notification_list=array();
						//pr($notifications);die;
						if($notifications)
						{		
							foreach($notifications as $notification)	
							{								
									if($notification["User"]["identifier"]=="")
										{
											if(empty($notification["UserDetail"]["image"]) || !file_exists(WWW_ROOT."/img/profile/".$notification['UserDetail']['image']))
											{
												 $imageprofile=SITE_LINK."img/default_new1.png";
											}
											else{
												$imageprofile=SITE_LINK."img/profile/".$notification["UserDetail"]["image"];
											}
										}
										else
										{
											if($notification["UserDetail"]["fbimage"]!=""){
												$imageprofile=$notification["UserDetail"]["fbimage"];
											}
											else if(empty($notification["UserDetail"]["image"]) || !file_exists(WWW_ROOT."/img/profile/".$notification['UserDetail']['image']))
											{
												 $imageprofile=SITE_LINK."img/default_new1.png";
											}
											else{
												$imageprofile=SITE_LINK."img/profile/".$notification["UserDetail"]["image"];
											}
										}
									$is_follow=false;
									$this->Follower = ClassRegistry::init('Follower');
									$this->Follower->hasOne = $this->Follower->belongsTo = $this->Follower->hasMany = array();
									$this->Follower->belongsTo = array(
										"User"=>array(
												"className"=>"User",
												"foreignKey"=>false,
												"type"=>"Inner",
												"conditions"=>"User.id=Follower.user_id"
											),
										"UserDetail"=>array(
											"className"=>"UserDetail",
											"foreignKey"=>false,
											"type"=>"Inner",
											"conditions"=>"UserDetail.user_id=Follower.user_id"
										)
									);	
									if ($is_following=$this->Follower->find("all",array("conditions"=>array("Follower.follower_id"=>$notification["Notification"]["sender_id"],"Follower.user_id"=>$user_id))))	
									{
										$is_follow=true;
									}	
									$flag=false;
									if($notification["User"]["user_type_id"]==3)
									{
										  $user_type="appuser";	
										  //~ if($notification['User']["id"]==$user_id)
										  //~ {
											   //~ $user_type="self";
										  //~ }	
										  if($notification["UserDetail"]["see_my_profile"]==0)//Nobody
										  {
												$flag=false;							 
										  }
										  else  if($notification["UserDetail"]["see_my_profile"]==1)//Followers
										  {		
											if($is_follow==true)
											  {				
												$flag=true;							
											  }
											  else
											  {
												  $flag=false;
											  }
										  }
										  else//everyone
										  {
											  $flag=true;
										  }
									}
									else
									{
										$flag=true;
										$user_type="webuser";
									}
									
									$created_in_text= $this->time_elapsed_string($notification["Notification"]["created"], true);
									//echo $this->time_elapsed_string('@1367367755'); # timestamp input
									//echo $this->time_elapsed_string('2013-05-01 00:22:35', true);die;
										//$notification_list[]["id"]=$notification["Notification"]["id"];
									$notification_list[$i]["created"]=$notification["Notification"]["created"];
									$notification_list[$i]["created_in_text"]=$created_in_text;
									$notification_list[$i]["comment_id"]=$notification["Notification"]["comment_id"];
									$notification_list[$i]["text"]=$notification["Notification"]["text"];
									$notification_list[$i]["event_date"]=date("Y-m-d",strtotime($notification["Notification"]["comment_add"]));
									$notification_list[$i]["type"]=$notification["Notification"]["type"];
									$notification_list[$i]["event_id"]=$notification["Notification"]["event_id"];
									$notification_list[$i]["user_id"]=$notification["Notification"]["sender_id"];
									$notification_list[$i]["image"]=$imageprofile;
									$notification_list[$i]["name"]=$notification["UserDetail"]["business"];
									$notification_list[$i]["user_type"]=$user_type;
									$notification_list[$i]["view_profile"]=$flag;
									
									
									$i++;
							}	//die;	
							$this->result = array("status"=>200,"message"=>"Notification List","data"=>$notification_list);
						}
						else
						{
							$this->result = array("status"=>200,"message"=>"No notification to display","data"=>$notification_list);
						}					
			}
				else
				{
					$this->result = array("status"=>201,"message"=>"Invalid user");
				}	
			}
			else
			{
				$this->result = array("status"=>201,"message"=>"Invalid Request");
			}
				return json_encode($this->result);
		}
		
		function time_elapsed_string($datetime, $full = false) {
			 $enterd=date("Y-m-d",strtotime($datetime));
			 $nowd=date("Y-m-d");
					
			$flag =false;
			$datetime1 = new DateTime( $nowd);
			$datetime2 = new DateTime($enterd);
			$interval = $datetime1->diff($datetime2);
			 $days= $interval->format('%R%a');
			if($days==-1)
			{
				$flag =true;
			}
			 $now = new DateTime;
			$ago = new DateTime($datetime);
			$diff = $now->diff($ago);

		    $diff->w = floor($diff->d / 7);
			
			$diff->d -= $diff->w * 7;
			$diff->ds = $diff->w * 7;
	
			$string = array(
				'y' => 'year',
				'm' => 'month',
				'w' => 'week',
				'd' => 'day',
			);
			foreach ($string as $k => &$v) {
				if ($diff->$k) {
					 $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
				} else {
					unset($string[$k]);
				}
			}

			if (!$full) $string = array_slice($string, 0, 1);
			if(count($string)>=2)
			{
			
				unset($string['d']);
			}
			
			return $string ? ($flag==true ? "Yesterday" : implode('  ', $string) . ' ago' ): 'Today';
			
		}
		
	public function push_message($user_device_key,$message,$event_id,$comment_id,$event_date,$device_type,$notificationType=null)
	{
		//($user_device_key);
		//echo API_ACCESS_KEY;die;
		#prep the bundle
			 $msg = array
				  (
				'body' 	=> $message,
				'event_id' 	=> $event_id,
				'comment_id' 	=> $comment_id,
				'event_date' 	=> $event_date,
				'type'	=> $notificationType,						
				//'icon'	=> 'myicon',/*Default Icon*/
				//'sound' => 'mySound'/*Default sound*/
				  );
				// pr($msg);
				if($device_type=="android")
				{
					$fields = array
					(
						//'event_id' =>"123",
						'registration_ids' =>$user_device_key,
						'data' =>  array(							
							"msg" => $msg
						 ),
						'content_available'=>true,
						 'priority' => 10,
						//'notificationtype'	=> $notificationType
					);			
				}
				else
				{
					$fields = array
					(
						//'event_id' =>"123",
						'registration_ids' =>$user_device_key,						
						'content_available'=>1,
						 'priority' => 10,
						'notification'	=>   $msg
						
					);	
				}
					//~ $fields = array
					//~ (
						//~ //'to' =>$registrationIds,
						//~ 'registration_ids' =>$user_device_key,
						//~ //'to'=>'c2YKnIl1gaI:APA91bHmZlT19NojIVExY_ZCeGhRsn3C3sQB4j1_ycMqWwy989XWJvXCe2vxUBJ5aJoGh8rGL9GZoxmGis4lSeNJesKGn-tvWlCFqGw_SG3nOGzfLx3WTrJvLE0-kuGq30pvTIv95MDl99ylF0GE1c5WhRL6F7k-YA',
						//'data' =>  array(							
						//	"msg" => json_encode($msg)
					//	 ) ,
						//~ 'content_available'=>true,
						//~ 'priority' => 10,
					    //~ 'notification'	=> $msg
					//~ );			
			
			$headers = array
					(
						'Authorization: key=' . API_ACCESS_KEY,
						'Content-Type: application/json'
					);

				#Send Reponse To FireBase Server	
						$ch = curl_init();
						curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
						curl_setopt( $ch,CURLOPT_POST, true );
						curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
						curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
						curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
						curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
						$cexecute = curl_exec($ch );
						curl_close( $ch );

				#Echo Result Of FireBase Server
				//echo $result;die;
				 $result=json_decode($cexecute,true);
			//pr($cexecute);die;
				return $result;
	}
	//~ public function push_message_save_cal($user_device_key,$message,$event_id,$event_date,$device_type,$notificationType=null)
	//~ {
		
		//~ //echo API_ACCESS_KEY;die;
		//~ #prep the bundle
			 //~ $msg = array
				  //~ (
				//~ 'body' 	=> $message,				
				//~ 'comment_id' 	=> $comment_id,
				//~ 'event_date' 	=> $event_date,
				//~ 'type'	=> $notificationType,						
				//~ //'icon'	=> 'myicon',/*Default Icon*/
				//~ //'sound' => 'mySound'/*Default sound*/
				  //~ );
				//~ if($device_type=="android")
				//~ {
					//~ $fields = array
					//~ (
						//~ //'event_id' =>"123",
						//~ 'registration_ids' =>$user_device_key,
						//~ 'data' =>  array(							
							//~ "msg" => $msg
						 //~ ),
						//~ 'content_available'=>true,
						 //~ 'priority' => 10,
						//~ //'notificationtype'	=> $notificationType
					//~ );			
				//~ }
				//~ else
				//~ {
					//~ $fields = array
					//~ (
						//~ //'event_id' =>"123",
						//~ 'registration_ids' =>$user_device_key,						
						//~ 'content_available'=>true,
						 //~ 'priority' => 10,
						//~ 'notification'	=>   $msg
						
					//~ );	
				//~ }	
			
			//~ $headers = array
					//~ (
						//~ 'Authorization: key=' . API_ACCESS_KEY,
						//~ 'Content-Type: application/json'
					//~ );

				//~ #Send Reponse To FireBase Server	
						//~ $ch = curl_init();
						//~ curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
						//~ curl_setopt( $ch,CURLOPT_POST, true );
						//~ curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
						//~ curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
						//~ curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
						//~ curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
						//~ $cexecute = curl_exec($ch );
						//~ curl_close( $ch );

				//~ #Echo Result Of FireBase Server
				//~ //echo $result;die;
				 //~ $result=json_decode($cexecute,true);
				//~ //pr($cexecute);die;
				//~ return $result;
	//~ }
	public function push_message_follow($user_device_key,$message,$user_id,$user_type,$device_type,$view_profile,$notificationType=null)
	{
		
		#prep the bundle
			 $msg = array
				  (
				'body' 	=> $message,
				'user_id' 	=> $user_id,
				'user_type' 	=> $user_type,	
				'view_profile' => $view_profile,		
				'type'	=> $notificationType,						
				//'icon'	=> 'myicon',/*Default Icon*/
				//'sound' => 'mySound'/*Default sound*/
				  );

			if($device_type=="android")
				{
					$fields = array
					(
						//'event_id' =>"123",
						'registration_ids' =>$user_device_key,
						'data' =>  array(							
							"msg" => $msg
						 ),
						'content_available'=>true,
						 'priority' => 10,
						//'notificationtype'	=> $notificationType
					);			
				}
				else
				{
					$fields = array
					(
						//'event_id' =>"123",
						'registration_ids' =>$user_device_key,						
						'content_available'=>1,
						 'priority' => 10,
						'notification'	=>   $msg
						
					);	
				}		
				
			$headers = array
					(
						'Authorization: key=' . API_ACCESS_KEY,
						'Content-Type: application/json'
					);

				#Send Reponse To FireBase Server	
						$ch = curl_init();
						curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
						curl_setopt( $ch,CURLOPT_POST, true );
						curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
						curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
						curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
						curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
						$cexecute = curl_exec($ch );
						curl_close( $ch );

				#Echo Result Of FireBase Server
				//echo $result;die;
				 $result=json_decode($cexecute,true);
				//pr($cexecute);die;
				return $result;
	}
	
	public function push_message1($user_device_key,$message,$notificationType=null)
	{//pr($user_device_key);echo $message;echo $notificationType;
		#API access key from Google API's Console
		//define( 'API_ACCESS_KEY', 'AAAAcrHv1cs:APA91bF-P2AvXNIvVCT6lip7Voc6xRpIeQ8dsUCBhBsf5q2pPuUBLADcPtxk_vVGqACA75dsrZeHCUDD3JAnGJm2syk9v6F9A_2s4LHlVnXYJB3ft-6fSPzS2uNGJdCWrMxU3i_Z31KZkRPsDJoWwtBXTgBWKmmXww' );
		//$registrationIds = $_GET['id']; // these are device token we will fetch from the database.

		#prep the bundle
			 $msg = array
				  (
				'body' 	=> $message,
				'type'	=> $notificationType,						
				'icon'	=> 'myicon',/*Default Icon*/
				'sound' => 'mySound'/*Default sound*/
				  );

			$fields = array
					(
						//'to' =>$registrationIds,
						'registration_ids' =>$user_device_key,
						//'to'=>'c2YKnIl1gaI:APA91bHmZlT19NojIVExY_ZCeGhRsn3C3sQB4j1_ycMqWwy989XWJvXCe2vxUBJ5aJoGh8rGL9GZoxmGis4lSeNJesKGn-tvWlCFqGw_SG3nOGzfLx3WTrJvLE0-kuGq30pvTIv95MDl99ylF0GE1c5WhRL6F7k-YA',
						'notification' =>  $msg ,
						'priority' => 10,
						//'notificationtype'	=> $notificationType
					);
			

			$headers = array
					(
						'Authorization: key=' . API_ACCESS_KEY,
						'Content-Type: application/json'
					);

				#Send Reponse To FireBase Server	
						$ch = curl_init();
						curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
						curl_setopt( $ch,CURLOPT_POST, true );
						curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
						curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
						curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
						curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
						$cexecute = curl_exec($ch );
						curl_close( $ch );

				#Echo Result Of FireBase Server
				//echo $result;die;
				 $result=json_decode($cexecute,true);
				//pr($cexecute);die;
				return $result;
	}
	
	
}
?>
