<?php
App::uses('Component','Controller','AppController');

require_once '../Vendor/autoload.php';
use Firebase\JWT\JWT;

class CalendarV1Component extends Component {
		 //var $name = 'CalendarV1';
		var $requestMethod = '';
		var $variables = '';
		var $obj = '';
		var $userId = '';
		var $result = array("status"=>200);
		var $siteLink = '';
		var $User = '';
		var $components = array('NotificationV1');
		public function beforeFilter(Event $event) {
			parent::beforeFilter($event); 			
		}
		
		function __construct($args) {
			parent::__construct($args);
			$this->requestMethod = $_SERVER['REQUEST_METHOD'];
			$this->User = ClassRegistry::init('User');
			$this->Calendar = ClassRegistry::init('Calendar');
			$obj = $this;			
		 
		}		
		
		function fetch_all_events() {			
			if ($this->requestMethod == 'GET' ) {
				$tmp = $this->variables->query;
				$total_events=0;
				$page=1;
				//die;			
				$temp['calendar']['eventdate'] = isset($this->variables->query['event_date'])?$this->variables->query['event_date']:'';	
				$user_id= $this->userId;
			
					if(empty($temp['calendar']['eventdate']))
					$month=date("Y-m-d");			
					else
					 $month=$temp['calendar']['eventdate'];
				
					
					$resultcal="";
					$ids=array();
					$list="";
					$callList=array();
					$empty=array();
					$sql="SELECT group_concat(calendars.id),group_concat(eventdate) FROM calendars , users WHERE users.is_active=1 and users.id=calendars.user_id and calendars.is_active = '1' and calendars.event_status = 1 and  calendars.repeat_event = 2 and abs(datediff('".$month."',date(calendars.eventdate))%7) = 0 and date(calendars.eventdate) <= '".$month."'" ;
					$resultcal =$this->Calendar->query($sql);
					
					if(!empty($resultcal[0][0]['group_concat(calendars.id)']))
					{	
						$id_event=explode(",",$resultcal[0][0]['group_concat(calendars.id)']);
						$eventdateList=explode(",",$resultcal[0][0]['group_concat(eventdate)']);					
						$i=0;
						foreach($id_event as $idEvent)
						{
							for($i;$i<=count($eventdateList);)
							{
								$eventdate=$eventdateList[$i];$i++;
								break;
							}
							$two_month=date("Y-m-d",strtotime($eventdate." +2 months"));
							
							if($eventdate<=$month && $month<=$two_month)
							{
								$ids[]=$idEvent;				
							}			
						}
					}				
				
					$sql="SELECT group_concat(calendars.id),group_concat(eventdate) FROM calendars as calendars , users WHERE users.is_active=1 and users.id=calendars.user_id and calendars.is_active = '1' and calendars.event_status = 1 and  'calendars.repeat_event' = 3 and abs(datediff('".$month."',date(calendars.eventdate))%14) = 0 and date(calendars.eventdate) <= '".$month."'";				
					$resultcal=$this->Calendar->query($sql);
					if(!empty($resultcal[0][0]['group_concat(calendars.id)']))
					{	
						$id_event=explode(",",$resultcal[0][0]['group_concat(calendars.id)']);
						$eventdateList=explode(",",$resultcal[0][0]['group_concat(eventdate)']);					
						$i=0;
						foreach($id_event as $idEvent)
						{
							for($i;$i<=count($eventdateList);)
							{
								$eventdate=$eventdateList[$i];$i++;
								break;
							}
							$two_month=date("Y-m-d",strtotime($eventdate." +2 months"));
							
							if($eventdate<=$month && $month<=$two_month)
							{
								$ids[]=$idEvent;				
							}			
						}
					}
					
					$sql="SELECT group_concat(calendars.id),group_concat(eventdate) FROM calendars , users WHERE users.is_active=1 and users.id=calendars.user_id and calendars.is_active = '1' and calendars.event_status = 1 and calendars.repeat_event = 4 and day('".$month."') = day(calendars.eventdate) and day(calendars.eventdate) <= day('".$month."')";
					
					$resultcal=$this->Calendar->query($sql);				
					if(!empty($resultcal[0][0]['group_concat(calendars.id)']))
					{	
						$id_event=explode(",",$resultcal[0][0]['group_concat(calendars.id)']);
						$eventdateList=explode(",",$resultcal[0][0]['group_concat(eventdate)']);					
						$i=0;
						foreach($id_event as $idEvent)
						{
							for($i;$i<=count($eventdateList);)
							{
								$eventdate=$eventdateList[$i];$i++;
								break;
							}
							$two_month=date("Y-m-d",strtotime($eventdate." +2 months"));
							
							if($eventdate<=$month && $month<=$two_month)
							{
								$ids[]=$idEvent;				
							}			
						}
					}
					
					
					$sql="SELECT group_concat(calendars.id),group_concat(eventdate) FROM calendars , users WHERE users.is_active=1 and users.id=calendars.user_id and calendars.is_active = '1' and calendars.event_status = 1 and calendars.repeat_event = 1 and date(calendars.eventdate) <= '".$month."'";
					$resultcal=$this->Calendar->query($sql);	
					if(!empty($resultcal[0][0]['group_concat(calendars.id)']))
					{	
						$id_event=explode(",",$resultcal[0][0]['group_concat(calendars.id)']);
						$eventdateList=explode(",",$resultcal[0][0]['group_concat(eventdate)']);					
						$i=0;
						foreach($id_event as $idEvent)
						{
							for($i;$i<=count($eventdateList);)
							{
								$eventdate=$eventdateList[$i];$i++;
								break;
							}
							$two_month=date("Y-m-d",strtotime($eventdate." +2 months"));
							//echo $eventdate;echo $month;echo $two_month;die;
							if($eventdate<=$month && $month<=$two_month)
							{
								$ids[]=$idEvent;				
							}			
						}
					}				
					
					$sql="SELECT group_concat(calendars.id),group_concat(eventdate) FROM calendars , users WHERE users.is_active=1 and users.id=calendars.user_id and calendars.is_active = '1' and calendars.event_status = 1 and  calendars.repeat_event = 0 and date(calendars.eventdate) = '".$month."'";	
					$resultcal=$this->Calendar->query($sql);	
					if(!empty($resultcal[0][0]['group_concat(calendars.id)']))
					{	
						$id_event=explode(",",$resultcal[0][0]['group_concat(calendars.id)']);
						$eventdateList=explode(",",$resultcal[0][0]['group_concat(eventdate)']);			
											
						$i=0;
						foreach($id_event as $idEvent)
						{
							for($i;$i<=count($eventdateList);)
							{
								$eventdate=$eventdateList[$i];$i++;
								break;
							}
							$two_month=date("Y-m-d",strtotime($eventdate." +2 months"));
							
							if($eventdate<=$month && $month<=$two_month)
							{
								$ids[]=$idEvent;				
							}			
						}
					}
					
				 if(!empty($ids))
				 {
					//Filters Unsuitable or Hidden Events
					$this->conditions1=array("UserUnsuitableEvent.user_id"=> $user_id);
					$this->UserUnsuitableEvent = ClassRegistry::init('UserUnsuitableEvent');
					if($list_unsuitable_events = $this->UserUnsuitableEvent->find('all',array("conditions"=>$this->conditions1)))
					{
						foreach($list_unsuitable_events as $list_events)
						{
							$unsuitable_events[]=$list_events["UserUnsuitableEvent"]["event_id"];	
						}
						$ids=array_diff($ids,$unsuitable_events);
					}
					$this->conditions1=array("UserHideBusiness.user_id"=> $user_id);
					$this->UserHideBusiness = ClassRegistry::init('UserHideBusiness');
					$hide_business=array();
					if($user_hide_businesses = $this->UserHideBusiness->find('all',array("conditions"=>$this->conditions1)))
					{
						foreach($user_hide_businesses as $list)
						{
								if ($user=$this->User->find("first",array("conditions"=>array("User.id"=>$list["UserHideBusiness"]["business_id"]))))	
								{
									$hide_business[]=$list["UserHideBusiness"]["business_id"];	
								}
						}						
						$this->conditions=array( 'NOT' => array('Calendar.user_id' =>$hide_business));
					}
				//	pr($this->conditions);die;
					if(!empty($ids))
					{
						if($hide_business)
						$this->conditions=array_merge($this->conditions,array("Calendar.id IN"=> $ids));
						else
						$this->conditions=array("Calendar.id IN"=> $ids);
						//Filters Location
						if (isset($tmp['lat']) && !empty($tmp['lat']) )  {
							$this->Calendar->virtualFields["distance"] = "ROUND((111.111 * DEGREES(ACOS(COS(RADIANS(".$tmp['lat'].")) * COS(RADIANS(Calendar.latitude)) * COS(RADIANS(".$tmp['long']." - Calendar.longitude))  + SIN(RADIANS(".$tmp['lat'].")) * SIN(RADIANS(Calendar.latitude))))),2)";
							$order = array("Calendar.distance asc");
						} else {
							$this->Calendar->virtualFields["distance"] = "-1";
						}
						
						if ( isset($tmp['distance']) && !empty($tmp['distance']) ) {
							$this->conditions = array_merge($this->conditions,array("Calendar.distance <= "=>$tmp['distance']));
						}
						else
						{
							//~ if(isset($tmp['search']) && !empty($tmp['search']) ) {
								
							//~ }
							//~ else
							//~ {
								$this->conditions = array_merge($this->conditions,array("Calendar.distance <= "=>40));
							//~ }
						}
						
						//Filters Tags
						$tmpCond = array();
						if (isset($tmp['tags']) && !empty($tmp['tags']) )  {
							$tags = str_replace("#",'',$tmp['tags']);
							$tmpTags = explode(",",$tags);
							$tmpc = array();
							
							//if ( count($tmpTags) > 1) {
								foreach($tmpTags as $key=>$val){
									$tmpc[] = array("Calendar.tags"=>$val);
								}
								$tmpCond['AND']['OR'] = $tmpc;
								$this->conditions = array_merge($this->conditions,$tmpCond);
								//$this->trackTags($tmpTags);
							//~ } else {
								//~ $this->conditions = array_merge($this->conditions,array("Calendar.tags"=>$tmpTags[0]));								
								//~ $tmpTags[] = $tmp['tags'];
								//~ //$this->trackTags($tmpTags);
							//~ }							
						}
						
						if ( isset($tmp['search']) && !empty($tmp['search']) ) {
							$search = strtolower($tmp['search']);
							
							$this->conditions = array_merge($this->conditions,array("OR"=>array("LOWER(Calendar.title)  LIKE "=>'%'.$search.'%',"LOWER(Calendar.short_dec)  LIKE "=>'%'.$search.'%',"LOWER(Calendar.long_dec)  LIKE "=>'%'.$search.'%',"LOWER(Calendar.location)  LIKE "=>'%'.$search.'%')));
						}					
						//~ if ( isset($tmp['location']) && !empty($tmp['location']) ) {
							//~ $location = strtolower($tmp['location']);
							
							//~ $this->conditions = array_merge($this->conditions,array("LOWER(Calendar.location)  LIKE "=>'%'.$location.'%'));
						//~ }		
						
						$this->Follower = ClassRegistry::init('Follower');
						$uList=array();
						if( isset($tmp['follow']) && !empty($tmp['follow']) ) {
							if($tmp['follow']==1)
							{
								$this->conditions1=array("Follower.user_id"=> $user_id);						
								$user_list= $this->Follower->find('all',array("conditions"=>$this->conditions1));
								
								if(!empty($user_list))
								{
									foreach($user_list as $ulist)
									{
										$uList[]=$ulist["Follower"]["follower_id"];	
									}
								}
								if(!empty($uList))
								{
									$this->conditions=array_merge($this->conditions,array("Calendar.user_id IN"=> $uList));
								}
								else
								{
									$this->result = array("status"=>200,"message"=>"No record found.");
									return;
								}

							}
						}
						
						
						$this->Calendar->hasOne = $this->Calendar->belongsTo = $this->Calendar->hasMany = array();
						$this->Calendar->belongsTo = array(	
							"User"=>array(
								"className"=>"User",
								"foreignKey"=>false,
								//"type"=>"Inner",
								"conditions"=>"Calendar.user_id=User.id"
							),					
							"UserDetail"=>array(
								"className"=>"UserDetail",
								"foreignKey"=>false,
								//"type"=>"Inner",
								"conditions"=>"Calendar.user_id=UserDetail.user_id"
							),
							"Tag" => array(
								"className" => "Tag",
								"foreignKey" => "tags",
								"type" => "Inner",
								'conditions' => false,
							)
						);
						
						
						$page = (isset($tmp['page']) && !empty($tmp['page']) )?$tmp['page']:1;
						$limit = 20;
						$total_page = $this->Calendar->find('count',array("conditions"=>array($this->conditions)));
						$total_page=ceil($total_page/$limit);	
						if(isset($tmp['map']))
						{
							$list = $this->Calendar->find('all',array("conditions"=>array($this->conditions)));
						}
						else
						{		
							$list = $this->Calendar->find('all',array("conditions"=>array($this->conditions),"limit"=>$limit,"page"=>$page));
						}
						
						if(!empty($list))
						{
							$i = 0;
							
							foreach($list as $val)
							{			
								$callList[$i]["id"]=$val["Calendar"]["id"];				
								$callList[$i]["business"]=$val["UserDetail"]["business"];
								$callList[$i]["business_id"]=$val["UserDetail"]["user_id"];
								if($val["User"]["identifier"]=="")
								{
									if(empty($val["UserDetail"]["image"]) || !file_exists(WWW_ROOT."/img/profile/".$val['UserDetail']['image']))
									{
										 $business_image=SITE_LINK."img/default_new1.png";
									}
									else{
										$business_image=SITE_LINK."img/profile/".$val["UserDetail"]["image"];
									}
								}
								else
								{
									if($val["UserDetail"]["fbimage"]!=""){
										$business_image=$val["UserDetail"]["fbimage"];
									}
									else if(empty($val["UserDetail"]["image"]) || !file_exists(WWW_ROOT."/img/profile/".$val['UserDetail']['image']))
									{
										 $business_image=SITE_LINK."img/default_new1.png";
									}
									else{
										$business_image=SITE_LINK."img/profile/".$val["UserDetail"]["image"];
									}
								}
								$callList[$i]["business_image"]=$business_image;
								$callList[$i]["ticket_free"]=$val["Calendar"]["ticket_free"];
								$callList[$i]["ticket_link"]=$val["Calendar"]["ticket_link"];
								$callList[$i]["start_price"]=$val["Calendar"]["start_price"];
								$callList[$i]["end_price"]=$val["Calendar"]["end_price"];
								$callList[$i]["short_dec"]=$val["Calendar"]["short_dec"];
								$callList[$i]["start_hour"]=date("h:i a",strtotime($val["Calendar"]["start_hour"]));
								$callList[$i]["end_hour"]=date("h:i a",strtotime($val["Calendar"]["end_hour"]));
								$callList[$i]["distance"]=$val["Calendar"]["distance"];
								$callList[$i]["tags"]='#'.$val["Tag"]["tag"];
								$callList[$i]["title"]=$val["Calendar"]["title"];
								if(empty($val["Calendar"]["image"]) || !file_exists(WWW_ROOT."/img/events/".$val['Calendar']['image']))
								{
									 $imagevent=SITE_LINK."img/default_voucher.jpeg";
								}
								else{
									$imagevent=SITE_LINK."img/events/".$val["Calendar"]["image"];
								}
								$callList[$i]["image"]=$imagevent;
								$callList[$i]["eventdate"]=$val["Calendar"]["eventdate"];
								$callList[$i]["event_status"]=$val["Calendar"]["event_status"];
								$callList[$i]["latitude"]=$val["Calendar"]["latitude"];
								$callList[$i]["longitude"]=$val["Calendar"]["longitude"];								
								$callList[$i]["location"]=$val["Calendar"]["location"];
								
								$i++;
							}
							$this->result = array("status"=>200,"message"=>"List of events","total_page"=>$total_page,"page"=>$page,"data"=>$callList);
						}
						else
						{
							$this->result = array("status"=>200,"message"=>"No record found.","data"=>$empty);
						}	
					} else {
						$this->result = array("status"=>200,"message"=>"No record found.","data"=>$empty);
					}
					
				}
				else
				{
					$this->result = array("status"=>200,"message"=>"No record found.","data"=>$empty);
				}
					
			}
			else
			{
				$this->result = array("status"=>201,"message"=>"Invalid Token Request");
			}	
			//$this->log($this->conditions);
			//$this->log($this->result);
		return json_encode($this->result);				
	  }
	  //Fetch Single Event
	function fetch_event() {
			if ($this->requestMethod == 'GET' ) {				
				$temp['calendar'] = $this->variables->query;				
				
				$user_id= $this->userId;
				if ($user=$this->User->find("first",array("conditions"=>array("User.id"=>$user_id))))	
				 { 								
					$event_id= $temp['calendar']['event_id'];
					$event_date= $temp['calendar']['event_date'];
					if(!empty($event_id))
					{
						$this->Calendar->hasOne = $this->Calendar->belongsTo = $this->Calendar->hasMany = array();
						$this->Calendar->belongsTo = array(	
						   "User"=>array(
								"className"=>"User",
								"foreignKey"=>false,
								//"type"=>"Inner",
								"conditions"=>"Calendar.user_id=User.id"
							),					
							"UserDetail"=>array(
								"className"=>"UserDetail",
								"foreignKey"=>false,
								//"type"=>"Inner",
								"conditions"=>"Calendar.user_id=UserDetail.user_id"
							),
							"Tag" => array(
								"className" => "Tag",
								"foreignKey" => "tags",
								"type" => "Inner",
								'conditions' => false,
							)
						);
						$this->conditions=array("Calendar.id"=>$event_id);			
						$callist = $this->Calendar->find('first',array("conditions"=>$this->conditions));
						if($callist)
						{
							if(empty($callist["Calendar"]["image"]) || !file_exists(WWW_ROOT."/img/events/".$callist['Calendar']['image']))
							{
								 $imagevent=SITE_LINK."img/default_voucher.jpeg";
							}
							else{
								$imagevent=SITE_LINK."img/events/".$callist["Calendar"]["image"];
							}
							
							
							
							
							if($callist["User"]["identifier"]=="")
							{
								if(empty($callist["UserDetail"]["image"]) || !file_exists(WWW_ROOT."/img/profile/".$callist['UserDetail']['image']))
								{
									 $imageprofile=SITE_LINK."img/default_new1.png";
								}
								else{
									$imageprofile=SITE_LINK."img/profile/".$callist["UserDetail"]["image"];
								}
							}
							else
							{
								if($callist["UserDetail"]["fbimage"]!=""){
									$imageprofile=$callist["UserDetail"]["fbimage"];
								}
								else if(empty($callist["UserDetail"]["image"]) || !file_exists(WWW_ROOT."/img/profile/".$callist['UserDetail']['image']))
								{
									 $imageprofile=SITE_LINK."img/default_new1.png";
								}
								else{
									$imageprofile=SITE_LINK."img/profile/".$callist["UserDetail"]["image"];
								}
							}
							
							
							
							$this->Follower = ClassRegistry::init('Follower');
							$this->Follower->hasOne = $this->Follower->belongsTo = $this->Follower->hasMany = array();
							$this->Follower->belongsTo = array(	
							   "User"=>array(
									"className"=>"User",
									"foreignKey"=>false,
									//"type"=>"Inner",
									"conditions"=>"Follower.user_id=User.id"
								)
							);
						 	$follower_id=$callist["Calendar"]["user_id"];
						 	//follower_id  is Follow ID
							$this->conditions=array("Follower.user_id"=>$user_id,"Follower.follower_id"=>$follower_id,"User.is_active"=>1);	
							if($follow_list = $this->Follower->find('first',array("conditions"=>$this->conditions)))
							{
								$follow=true;
							}
							else
							{
								$follow=false;
							}
							
							$this->conditions=array("Follower.follower_id"=>$follower_id,"User.is_active"=>1);	
							if($follower_count = $this->Follower->find('all',array("conditions"=>$this->conditions)))
							{
								$follow_count=count($follower_count);
							}
							else
							{
								$follow_count=0;
							}
							
							
							$this->SaveCalendar = ClassRegistry::init('SaveCalendar');	
							$this->conditions=array("SaveCalendar.user_id"=>$user_id,"SaveCalendar.event_id"=>$event_id,"SaveCalendar.type"=>"calendar","SaveCalendar.event_date"=>$event_date);
							//pr($this->conditions);die;	
							if($listCal = $this->SaveCalendar->find('first',array("conditions"=>$this->conditions)))
							{
								$addtocalendar=true;
							}
							else
							{
								$addtocalendar=false;
							}
							if($callist["Calendar"]["ticket_free"]==0 || !empty($callist["Calendar"]["ticket_free"]))
							{
								$ticket_free=false;
							}
							else
							{
								$ticket_free=true;
							}
							
							$start_hour=date("h:i a",strtotime($callist["Calendar"]["start_hour"]));
							$end_hour=date("h:i a",strtotime($callist["Calendar"]["end_hour"]));
							
							$list=array("id"=>$callist["Calendar"]["id"],"title"=>$callist["Calendar"]["title"],"location"=>$callist["Calendar"]["location"],"latitude"=>$callist["Calendar"]["latitude"],"longitude"=>$callist["Calendar"]["longitude"],"ticket_free"=>$ticket_free,"short_dec"=>$callist["Calendar"]["short_dec"],"long_dec"=>$callist["Calendar"]["long_dec"],"start_hour"=>$start_hour,"end_hour"=>$end_hour,"eventdate"=>$callist["Calendar"]["eventdate"],"start_price"=>$callist["Calendar"]["start_price"],"end_price"=>$callist["Calendar"]["end_price"],"image"=>$imagevent,"tags"=>'#'.$callist["Tag"]["tag"],"event_status"=>$callist["Calendar"]["event_status"],"repeat_event"=>$callist["Calendar"]["repeat_event"],"ticket_link"=>$callist["Calendar"]["ticket_link"],"business_manager_id"=>$callist["UserDetail"]["user_id"],"business"=>$callist["UserDetail"]["business"],"business_image"=>$imageprofile,"business_location"=>$callist["UserDetail"]["address"],"business_desc"=>$callist["UserDetail"]["aboutme"],"follow"=>$follow,"follower_count"=>$follow_count,"addtocalendar"=>$addtocalendar);
							
							$this->result = array("status"=>200,"message"=>"Evnet data","data"=>$list);
						}
						else
						{
							$this->result = array("status"=>201,"message"=>"Invalid eventID");
						}
					}
					else
					{
						$this->result = array("status"=>201,"message"=>"Invalid event id");
					}
				}
				else
				{
					$this->result = array("status"=>201,"message"=>"Invalid user");
				}	
			}
			else
			{
				$this->result = array("status"=>201,"message"=>"Invalid Request");
			}
				return json_encode($this->result);
		}
		
	//Fetch Tags for Filter page	
	function fetch_tags() {			
			if ($this->requestMethod == 'GET' ) {				
				$temp['calendar'] = $this->variables->query;				
				
				$user_id= $this->userId;
				if ($user=$this->User->find("first",array("conditions"=>array("User.id"=>$user_id))))	
				 { 								
					$page = (isset($temp['calendar']['page']) && !empty($temp['calendar']['page']) )?$temp['calendar']['page']:1;
					$limit = 20;					
					$this->conditions=array("is_active"=>1);		
					$this->Tag = ClassRegistry::init('Tag');	
					$callist = $this->Tag->find('all',array("conditions"=>$this->conditions,"limit"=>$limit,"page"=>$page));
					$i=0;
					if($callist)
					{
						foreach($callist as $tagList)
						{
							$tag_single[$i]['id']=$tagList["Tag"]["id"];
							$tag_single[$i]['tag']="#".strtolower($tagList["Tag"]["tag"]);
							$i++;
						}		
											
						$this->result = array("status"=>200,"message"=>"Tags list","data"=>$tag_single);
					}
					else
					{
						$this->result = array("status"=>200,"message"=>"No more tag to display","data"=>$callist);
					}
				}
				else
				{
					$this->result = array("status"=>201,"message"=>"Invalid user");
				}
			}
			else
			{
				$this->result = array("status"=>201,"message"=>"Invalid Request");
			}
				return json_encode($this->result);
		}
		// App user add  event into their calendar
		function add_event_calendar() {			
			if ($this->requestMethod == 'GET' ) {				
				$temp['calendar'] = $this->variables->query;				
				
				$user_id= $this->userId;
				if ($user=$this->User->find("first",array("conditions"=>array("User.id"=>$user_id))))	
				 { 								
					$event_id= $temp['calendar']['event_id'];
					$event_date= $temp['calendar']['event_date'];
					if(!empty($event_id))
					{
						$tmp["SaveCalendar"]["event_id"]=$event_id;
						$tmp["SaveCalendar"]["user_id"]=$user_id;	
						$tmp["SaveCalendar"]["event_date"]=$event_date;
						$tmp["SaveCalendar"]["type"]="calendar";
						$this->SaveCalendar = ClassRegistry::init('SaveCalendar');
						if($event=$this->SaveCalendar->find("first",array("conditions"=>array("event_date"=>$event_date,"user_id"=>$user_id,"event_id"=>$event_id,"type"=>"calendar"))))
						{ 						
							$this->result = array("status"=>200,"message"=>"Event already added.");
						}
						else
						{
							$callist = $this->SaveCalendar->save($tmp);
							if($callist)
							{
								$this->result = array("status"=>200,"message"=>"Event added to Calender");
							}
							else
							{
								$this->result = array("status"=>201,"message"=>"Error Occured");
							}
						}
					}
					else
					{
						$this->result = array("status"=>201,"message"=>"Invalid event id");
					}
				}
				else
				{
					$this->result = array("status"=>201,"message"=>"Invalid user");
				}	
			}
			else
			{
				$this->result = array("status"=>201,"message"=>"Invalid Request");
			}
				return json_encode($this->result);
		}
		//Fetch Calendar events added to Wishlist
		function fetch_calendar_events() {
			if ($this->requestMethod == 'GET' ) {				
				$temp['calendar'] = $this->variables->query;				
				
				$user_id= $this->userId;
				if ($user=$this->User->find("first",array("conditions"=>array("User.id"=>$user_id))))	
				 {
					$this->SaveCalendar = ClassRegistry::init('SaveCalendar');
					$this->SaveCalendar->hasOne = $this->SaveCalendar->belongsTo = $this->SaveCalendar->hasMany = array();
					$this->SaveCalendar->belongsTo = array(
						"Calendar"=>array(
							"className"=>"Calendar",
							"foreignKey"=>false,
							"type"=>"Inner",
							"conditions"=>"Calendar.id=SaveCalendar.event_id"
						)
					);
					
					$month=date("Y-m",strtotime($temp['calendar']['event_date']));				
					$startDate=$month."-01";
					$endDate=$month."-31";
					
					if ($event=$this->SaveCalendar->find("all",array("conditions"=>array("SaveCalendar.user_id"=>$user_id,"SaveCalendar.type"=>"calendar",'SaveCalendar.event_date between ? and ?' => array($startDate, $endDate)))))
					{//pr($event);die;
						foreach($event as $eventDetail)
						{
							if(empty($eventDetail["Calendar"]["image"]) || !file_exists(WWW_ROOT."/img/events/".$eventDetail['Calendar']['image']))
							{
								 $imagevent=SITE_LINK."img/default_voucher.jpeg";
							}
							else{
								$imagevent=SITE_LINK."img/events/".$eventDetail["Calendar"]["image"];
							}
							
							$events[]=array("id"=>$eventDetail["SaveCalendar"]["id"],"event_id"=>$eventDetail["SaveCalendar"]["event_id"],"event_add_date"=>$eventDetail["SaveCalendar"]["event_date"],"event_status"=>$eventDetail["Calendar"]["event_status"],"eventdate"=>$eventDetail["Calendar"]["eventdate"],"title"=>$eventDetail["Calendar"]["title"],"image"=>$imagevent);
						}
						$this->result = array("status"=>200,"message"=>"List of Calendar events","data"=>$events);					
					}
					else
					{
						$this->result = array("status"=>201,"message"=>"No event to diaplay");
					}
				}
				else
				{
					$this->result = array("status"=>201,"message"=>"Invalid user");
				}	
			}
			else
			{
				$this->result = array("status"=>201,"message"=>"Invalid Request");
			}
				return json_encode($this->result);
		}
		//Fetch Calendar events added to Wishlist
		function fetch_calendar_events_others() {
			if ($this->requestMethod == 'GET' ) {				
				$temp['calendar'] = $this->variables->query;				
				
				$user_id= $this->userId;
				if ($user=$this->User->find("first",array("conditions"=>array("User.id"=>$user_id))))	
				 {
					if (isset($temp['calendar']['event_date']) && isset($temp['calendar']['user_id']) && !empty($temp['calendar']['user_id']))	
					{
						$this->SaveCalendar = ClassRegistry::init('SaveCalendar');
						$this->SaveCalendar->hasOne = $this->SaveCalendar->belongsTo = $this->SaveCalendar->hasMany = array();
						$this->SaveCalendar->belongsTo = array(
							"Calendar"=>array(
								"className"=>"Calendar",
								"foreignKey"=>false,
								"type"=>"Inner",
								"conditions"=>"Calendar.id=SaveCalendar.event_id"
							)
						);
						
						$month=date("Y-m",strtotime($temp['calendar']['event_date']));				
						$startDate=$month."-01";
						$endDate=$month."-31";
						
						if ($event=$this->SaveCalendar->find("all",array("conditions"=>array("SaveCalendar.user_id"=>$temp['calendar']['user_id'],"SaveCalendar.type"=>"calendar",'SaveCalendar.event_date between ? and ?' => array($startDate, $endDate)))))
						{
							foreach($event as $eventDetail)
							{
								if(empty($eventDetail["Calendar"]["image"]) || !file_exists(WWW_ROOT."/img/events/".$eventDetail['Calendar']['image']))
								{
									 $imagevent=SITE_LINK."img/default_voucher.jpeg";
								}
								else{
									$imagevent=SITE_LINK."img/events/".$eventDetail["Calendar"]["image"];
								}
								
								$events[]=array("id"=>$eventDetail["SaveCalendar"]["id"],"event_id"=>$eventDetail["SaveCalendar"]["event_id"],"event_add_date"=>$eventDetail["SaveCalendar"]["event_date"],"event_status"=>$eventDetail["Calendar"]["event_status"],"eventdate"=>$eventDetail["Calendar"]["eventdate"],"title"=>$eventDetail["Calendar"]["title"],"image"=>$imagevent);
							}
							$this->result = array("status"=>200,"message"=>"List of Calendar events","data"=>$events);					
						}
						else
						{
							$this->result = array("status"=>201,"message"=>"No event to display");
						}
					}
					else
					{
						$this->result = array("status"=>201,"message"=>"User Id or Event date is missing");
					}
				}
				else
				{
					$this->result = array("status"=>201,"message"=>"Invalid user");
				}
			}
			else
			{
				$this->result = array("status"=>201,"message"=>"Invalid Request");
			}
				return json_encode($this->result);
		}
		
		function delete_calendar_events() {
			if ($this->requestMethod == 'GET' ) {				
				$temp['calendar'] = $this->variables->query;				
				//event_id,event added date
				 $user_id= $this->userId;
				if ($user=$this->User->find("first",array("conditions"=>array("User.id"=>$user_id))))	
				 {
					 if(isset($temp['calendar']["event_date"]) && isset($temp['calendar']["event_id"]))
					 {
						$this->SaveCalendar = ClassRegistry::init('SaveCalendar');
						$event_date=$temp['calendar']["event_date"];
						$event_id=$temp['calendar']["event_id"];
						if ($event=$this->SaveCalendar->find("first",array("conditions"=>array("SaveCalendar.user_id"=>$user_id,"SaveCalendar.event_id"=>$event_id,"SaveCalendar.type"=>"calendar","SaveCalendar.event_date"=>$event_date))))
						{
							$this->SaveCalendar->id=$event["SaveCalendar"]["id"];	
							$calc=$this->SaveCalendar->delete();	
							$this->result = array("status"=>200,"message"=>"Event deleted");					
						}
						else
						{
							$this->result = array("status"=>201,"message"=>"No event added with this id or date");
						}
					}						
					else
					{
						$this->result = array("status"=>201,"message"=>"Invalid event id or event date");
					}	
				}
				else
				{
					$this->result = array("status"=>201,"message"=>"Invalid user");
				}	
			}
			else
			{
				$this->result = array("status"=>201,"message"=>"Invalid Request");
			}
				return json_encode($this->result);
		}
		
		// App user add  event as unsuitable event which means Hide that event and not shown to only that paticular user
		function report_unsuitable_event() {
			if ($this->requestMethod == 'GET' ) {				
				$temp['calendar'] = $this->variables->query;				
				
				$user_id= $this->userId;
				if ($user=$this->User->find("first",array("conditions"=>array("User.id"=>$user_id))))	
				 { 	
					if ($event=$this->Calendar->find("first",array("conditions"=>array("Calendar.id"=>$temp['calendar']["event_id"]))))
					{	
						
						$tmp["Calendar"]["unsuitable_count"]=$event["Calendar"]["unsuitable_count"]+1;
						
						//Ability to set the maximum number of times a post can be reported before it is automatically deactivated. 
						$this->DefaultValue = ClassRegistry::init('DefaultValue');
						$list = $this->DefaultValue->find('first');	
						$max_reported_post=$list["DefaultValue"]["max_reported_post"];
						$max_business_post=$list["DefaultValue"]["max_business_post"];
											
						if($tmp["Calendar"]["unsuitable_count"]==$max_reported_post)
						{
							$tmp["Calendar"]["is_active"]=0;
						}
						
						
						$tmp["Calendar"]["id"]=$temp['calendar']["event_id"];
						$event_manag_id=$event['Calendar']["user_id"];
						if($this->Calendar->save($tmp))
						{
							$this->UserUnsuitableEvent = ClassRegistry::init('UserUnsuitableEvent');
							$tmps["UserUnsuitableEvent"]["user_id"]=$user_id;
							$tmps["UserUnsuitableEvent"]["event_id"]=$temp['calendar']["event_id"];
							
							if (!$event=$this->UserUnsuitableEvent->find("first",array("conditions"=>array("UserUnsuitableEvent.event_id"=>$temp['calendar']["event_id"],"UserUnsuitableEvent.user_id"=>$user_id))))
							{	
								$this->UserUnsuitableEvent->save($tmps);
								
								$this->UserDetail = ClassRegistry::init('UserDetail');
								$data["UserDetail"]["user_id"]=$event_manag_id;
								if($userdetail=$this->UserDetail->find("first",array("conditions"=>array("UserDetail.user_id"=>$event_manag_id))))
								{									
									$data["UserDetail"]["id"]=$userdetail['UserDetail']["id"];
									$data["UserDetail"]["is_reported"]="1";
									$data["UserDetail"]["is_reported_times"]=$userdetail["UserDetail"]["is_reported_times"]+1;
									$this->UserDetail->save($data);									
									if($max_business_post==$data["UserDetail"]["is_reported_times"])
									{
										$set_inactive["User"]["is_active"]=0;
										$set_inactive["User"]["id"]=$event_manag_id;
										$this->User->save($set_inactive);	
									}
									
								}
								$this->result = array("status"=>200,"message"=>"Event reported as unsuitable successfully");
							}
							else
							{
								$this->result = array("status"=>201,"message"=>"Event already reported as unsuitable");				
							}
						}
						else
						{
							$this->result = array("status"=>201,"message"=>"Error Occured");
						}

					}
					else
					{
						$this->result = array("status"=>201,"message"=>"Invalid event Id");
					}
				}
				else
				{
					$this->result = array("status"=>201,"message"=>"Invalid user");
				}	
			}
			else
			{
				$this->result = array("status"=>201,"message"=>"Invalid Request");
			}
				return json_encode($this->result);
		}
   // App user add  event as hide event  and not shown to only that particular user

		function hide_event() {			
			if ($this->requestMethod == 'GET' ) {				
				$temp['calendar'] = $this->variables->query;				
				
				$user_id= $this->userId;
				if(isset($temp['calendar']["business_id"]) && !empty($temp['calendar']["business_id"]))
				{
					if ($user=$this->User->find("first",array("conditions"=>array("User.id"=>$user_id))))	
					 { 	
						if ($user=$this->User->find("first",array("conditions"=>array("User.id"=>$temp['calendar']["business_id"]))))
						{						
							$this->UserHideBusiness = ClassRegistry::init('UserHideBusiness');
							$tmps["UserHideBusiness"]["user_id"]=$user_id;
							$tmps["UserHideBusiness"]["business_id"]=$temp['calendar']["business_id"];	
							if ($user1=$this->UserHideBusiness->find("first",array("conditions"=>array("UserHideBusiness.user_id"=>$tmps['UserHideBusiness']["user_id"],"UserHideBusiness.business_id"=>$tmps['UserHideBusiness']["business_id"]))))
							{						
								$this->result = array("status"=>201,"message"=>"User Profile already added to hide list");
							}	
							else
							{							
								if($this->UserHideBusiness->save($tmps))
								{
									$this->result = array("status"=>200,"message"=>"User Profile successfully added to hidden list");				
								}
								else
								{
									$this->result = array("status"=>201,"message"=>"Error Occured");
								}
							}						
						}
						else
						{
							$this->result = array("status"=>201,"message"=>"Invalid business Id");
						}
					}
					else
					{
						$this->result = array("status"=>201,"message"=>"Invalid user");
					}
				}	
				else
				{
					$this->result = array("status"=>201,"message"=>"Business Id is not provided");
				}	
			}
			else
			{
				$this->result = array("status"=>201,"message"=>"Invalid Request");
			}
				return json_encode($this->result);
		}
		function show_hidden_business() {			
			if ($this->requestMethod == 'GET' ) {				
				$temp['calendar'] = $this->variables->query;				
				
				$user_id= $this->userId;
				if ($user=$this->User->find("first",array("conditions"=>array("User.id"=>$user_id))))	
				 { 	
										
						$this->UserHideBusiness = ClassRegistry::init('UserHideBusiness');
						$tmps["UserHideBusiness"]["user_id"]=$user_id;						
						if ($list_profile=$this->UserHideBusiness->find("all",array("conditions"=>array("UserHideBusiness.user_id"=>$tmps['UserHideBusiness']["user_id"]))))
						{			
							foreach($list_profile as $list)
							{
								if ($user=$this->User->find("first",array("conditions"=>array("User.id"=>$list['UserHideBusiness']["business_id"],"User.is_active"=>1))))
								{
									if($user["User"]["identifier"]=="")
									{
										if(empty($user["UserDetail"]["image"]) || !file_exists(WWW_ROOT."/img/profile/".$user['UserDetail']['image']))
										{
											 $imageprofile=SITE_LINK."img/default_new1.png";
										}
										else{
											$imageprofile=SITE_LINK."img/profile/".$user["UserDetail"]["image"];
										}
									}
									else
									{
										if($user["UserDetail"]["fbimage"]!=""){
											$imageprofile=$user["UserDetail"]["fbimage"];
										}
										else if(empty($user["UserDetail"]["image"]) || !file_exists(WWW_ROOT."/img/profile/".$user['UserDetail']['image']))
										{
											 $imageprofile=SITE_LINK."img/default_new1.png";
										}
										else{
											$imageprofile=SITE_LINK."img/profile/".$user["UserDetail"]["image"];
										}
									}
									
									
									$list_users[]=array("business"=>$user["UserDetail"]["business"],"user_id"=>$user["UserDetail"]["user_id"],"image"=>$imageprofile);
								}
								
							}	
							$this->result = array("status"=>200,"message"=>"User Profile list","data"=>$list_users);
						}	
						else
						{
								$this->result = array("status"=>201,"message"=>"No Hidden profile","data"=>$list_profile);							
						}	
				}
				else
				{
					$this->result = array("status"=>201,"message"=>"Invalid user");
				}	
			}
			else
			{
				$this->result = array("status"=>201,"message"=>"Invalid Request");
			}
				return json_encode($this->result);
		}
		function unhide_business_profile() {			
			if ($this->requestMethod == 'GET' ) {				
				$temp['calendar'] = $this->variables->query;				
				
				$user_id= $this->userId;
				if ($user=$this->User->find("first",array("conditions"=>array("User.id"=>$user_id))))	
				 { 	
					if ($user=$this->User->find("first",array("conditions"=>array("User.id"=>$temp['calendar']["business_id"]))))
					{						
						$this->UserHideBusiness = ClassRegistry::init('UserHideBusiness');
						$tmps["UserHideBusiness"]["user_id"]=$user_id;
						$tmps["UserHideBusiness"]["business_id"]=$temp['calendar']["business_id"];	
						if ($hide_pr=$this->UserHideBusiness->find("first",array("conditions"=>array("UserHideBusiness.user_id"=>$tmps['UserHideBusiness']["user_id"],"UserHideBusiness.business_id"=>$tmps['UserHideBusiness']["business_id"]))))
						{		
							$this->UserHideBusiness->id=$hide_pr["UserHideBusiness"]["id"];
							if($this->UserHideBusiness->delete())
							{
								$this->result = array("status"=>200,"message"=>"User Profile successfully deleted from hidden list");				
							}
							else
							{
								$this->result = array("status"=>201,"message"=>"Error Occured");
							}				
							
						}	
						else
						{							
							$this->result = array("status"=>201,"message"=>"No User Profile added with this id in hidden list");
						}						
					}
					else
					{
						$this->result = array("status"=>201,"message"=>"Invalid profile Id");
					}
				}
				else
				{
					$this->result = array("status"=>201,"message"=>"Invalid user");
				}	
			}
			else
			{
				$this->result = array("status"=>201,"message"=>"Invalid Request");
			}
				return json_encode($this->result);
		}
		
	// Calendar -- On Click of UserProfile 	
	public function manager_profile($month = null) {
		if ($this->requestMethod == 'GET' ) {				
				$temp['calendar'] = $this->variables->query;				
				$ids=array();
				$events_List=array();
				$user_id= $this->userId;
				if ($user=$this->User->find("first",array("conditions"=>array("User.id"=>$user_id))))	
				 { 	
					$sel_date="";		
					if(empty($temp['calendar']["month"]))
					$showmonth=date("Y-m-d");			
					else
					$showmonth=$temp['calendar']["month"];
				
					if(!empty($temp['calendar']["event_manag_id"]) && isset($temp['calendar']["event_manag_id"]))
					{
						if ($user=$this->User->find("first",array("conditions"=>array("User.id"=>$temp['calendar']["event_manag_id"],"User.user_type_id IN"=>array("0"=>2,"1"=>4)))))	
						{
							$event_manag_id=$temp['calendar']["event_manag_id"];
							$sql = "select MONTH('".$showmonth."') as currentMonth,DATE_FORMAT(LAST_DAY('".$showmonth."'),'%d') lastDay,DATE_FORMAT('".$showmonth."','%Y-%m-01') firstDay, DATE_FORMAT(DATE_FORMAT('".$showmonth."','%Y-%m-01'),'%a') first_day,DATE_FORMAT(DATE_ADD('".$showmonth."', Interval 1 month),'%Y-%m-01') as nextMonth,DATE_FORMAT(DATE_SUB('".$showmonth."', Interval 1 month),'%Y-%m-01') as prevMonth, date(now()) as currDate";
											
							$date = date('M Y',strtotime($showmonth));
							$resultcal=$this->Calendar->query($sql);		
													
							$eventArr=array();							
							$this->conditions=array("Calendar.user_id"=>$event_manag_id,"Calendar.event_status"=>1,"Calendar.is_active"=>1);
						
							$this->Calendar->virtualFields["event_status_date"]="select count(*) from calendars where date(eventdate) = Calendar.eventdate";
										
							$this->Calendar->virtualFields["next_event_status"]="(select case when Calendar.repeat_event = 2 then DATE_ADD(eventdate,INTERVAL 1 WEEK) when Calendar.repeat_event = 3 then DATE_ADD(eventdate,INTERVAL 2 WEEK) when Calendar.repeat_event = 4 then DATE_ADD(eventdate,INTERVAL 1 MONTH) end from calendars where id = Calendar.id )";
							
							$this->Calendar->hasOne = $this->Calendar->belongsTo = $this->Calendar->hasMany = array();
							$this->Calendar->belongsTo = array(	
								"User"=>array(
								"className"=>"User",
								"foreignKey"=>false,
								//"type"=>"Inner",
								"conditions"=>"Calendar.user_id=User.id"
								),	
								"UserDetail"=>array(
									"className"=>"UserDetail",
									"foreignKey"=>false,
									//"type"=>"Inner",
									"conditions"=>"Calendar.user_id=UserDetail.user_id"
								)									
							);								
							
							if($callist = $this->Calendar->find('all',array("conditions"=>$this->conditions,"fields"=>array("DAY(eventdate) as day","id","eventdate","event_status","repeat_event","Calendar.event_status_date","Calendar.next_event_status","UserDetail.business","UserDetail.image","UserDetail.fbimage","User.identifier","UserDetail.aboutme","UserDetail.address","UserDetail.longitude","UserDetail.latitude"),"order"=>"event_status desc")))
							{
								foreach($callist as $list){
									//~ if(in_array($list['Calendar']['id'],$ids))
									//~ {
										$eventObj=$list['Calendar'];
										$eventObj['selected_month']=date('Y-m',strtotime($showmonth))."-".date('d');
										$eventObj['current_month']=date('Y-m-d');
										$eventObj['event_id']=$list['Calendar']["id"];
										$eventObj['business']=$list['UserDetail']["business"];
										$eventObj['image']=$list['UserDetail']["image"];
										$eventObj['fbimage']=$list['UserDetail']["fbimage"];
										$eventObj['aboutme']=$list['UserDetail']["aboutme"];
										$eventObj['selected_month_btw']=date('Y-m',strtotime($showmonth));	
										$eventArr[]=$eventObj;
										unset($eventObj);
									//~ }
									}
							
									//Making dates
									$selectedDay = array();							
									$currDate = ($resultcal[0][0]['firstDay']); 
									foreach ( $eventArr as $key=>$val ) {									
																															
										$btw_start=date("Y-m-d", strtotime($val['eventdate']));
										$btw_end=date("Y-m-d", strtotime($val['eventdate']." +2 months"));		
											
										$start = date_create($val['eventdate']);				
										$t1 = strtotime($val['eventdate']);
										$date = date("d",$t1);
										
										for ($temp = 1; $temp <= $resultcal[0][0]['lastDay']; $temp++) {

											
											$temp1 = str_pad($temp,2,0,STR_PAD_LEFT);
											$tmpCurrDate = date_create(date("Y-m-".$temp1,strtotime($currDate)));
											$t2temp = (date("Y-m-".$temp1,strtotime($currDate)));
											$t2 = strtotime($t2temp);
											$d2 = date("d",$t2);
											$diff = date_diff($start,$tmpCurrDate);
											$d = $diff->format("%R%a");
											//echo "<br/>";
											$d1 = $diff->format("%a");
											$btw_date=date("Y-m-d", strtotime($val['selected_month_btw'].'-'.$temp));
											if ( $val['repeat_event'] == 1 && $t2 >= $t1 && ($val['eventdate']<= $btw_date && $btw_date <= $btw_end)) {
												$selectedDay[$temp]['date'] = date("Y-m-",strtotime($showmonth)).$temp;
												$selectedDay[$temp]['event_id'] = $val['event_id'];
												$selectedDay[$temp]['event_status'] = $val['event_status'];
												$selectedDay[$temp]['current_month'] = $val['current_month'];
												$selectedDay[$temp]['selected_month'] = $val['selected_month'];
											
											} elseif ($val['repeat_event'] == 2 && $d >= 0 && ($d%7) == 0 && ($val['eventdate']<= $btw_date && $btw_date <= $btw_end)) {
												
												$selectedDay[$temp]['date'] = date("Y-m-",strtotime($showmonth)).$temp;
												$selectedDay[$temp]['event_id'] = $val['event_id'];
												$selectedDay[$temp]['event_status'] = $val['event_status'];
												$selectedDay[$temp]['current_month'] = $val['current_month'];
												$selectedDay[$temp]['selected_month'] = $val['selected_month'];		
															
											} elseif ($val['repeat_event'] == 3 && $d >= 0 && ($d%14) == 0 && ($val['eventdate']<= $btw_date && $btw_date <= $btw_end)) {
												$selectedDay[$temp]['date'] = date("Y-m-",strtotime($showmonth)).$temp;
												$selectedDay[$temp]['event_id'] = $val['event_id'];
												$selectedDay[$temp]['event_status'] = $val['event_status'];
												$selectedDay[$temp]['current_month'] = $val['current_month'];
												$selectedDay[$temp]['selected_month'] = $val['selected_month'];		
																
											} elseif ($val['repeat_event'] == 4 && $d >= 0 && $date == $d2 && ($val['eventdate']<= $btw_date && $btw_date <= $btw_end)) {
												
												$selectedDay[$temp]['date'] = date("Y-m-",strtotime($showmonth)).$temp;
												$selectedDay[$temp]['event_id'] = $val['event_id'];
												$selectedDay[$temp]['event_status'] = $val['event_status'];
												$selectedDay[$temp]['current_month'] = $val['current_month'];
												$selectedDay[$temp]['selected_month'] = $val['selected_month'];		
																
											} elseif ($val['repeat_event'] == 0 && $date == $d2 && ($val['eventdate']<= $btw_date && $btw_date <= $btw_end)) {										
												if(date("m",strtotime($val['eventdate']))==date("m",strtotime($val['selected_month']))){
												$selectedDay[$temp]['date'] = date("Y-m-",strtotime($showmonth)).$temp;
												$selectedDay[$temp]['event_id'] = $val['event_id'];
												$selectedDay[$temp]['event_status'] = $val['event_status'];
												$selectedDay[$temp]['current_month'] = $val['current_month'];
												$selectedDay[$temp]['selected_month'] = $val['selected_month'];
												}					
											}
											if(!empty($selectedDay[$temp]) && $val['event_status'] && !isset($selectedDay[$temp]['is_live'])){
												$selectedDay[$temp]['is_live']=true;
											}											
										}
										//die;
									}
									$i=0;
									$data=array();	
									foreach($selectedDay as $key=>$value)
									{
										$data[$i]["date"]=$value["date"];
										$data[$i]["event_id"]=$value["event_id"];
										$data[$i]["event_status"]=$value["event_status"];
										$data[$i]["current_month"]=$value["current_month"];
										$data[$i]["selected_month"]=$value["selected_month"];
										if(isset($value["is_live"]))
										{
											$data[$i]["is_live"]=$value["is_live"];
										}else{
											$data[$i]["is_live"]=false;
										}
											$i++;
									}
									if(empty($data))
									{
										$events=array("eventList"=>array(),"events"=>"No more Events to display for this month");										
									}
									else{
										$events=array("eventList"=>$data,"events"=>$data);
										//$events_List=array("eventList"=>$data,"events"=>$data);
									}
									
									
									if($callist[0]["User"]["identifier"]=="")
									{
										if(empty($callist[0]["UserDetail"]["image"]) || !file_exists(WWW_ROOT."/img/profile/".$callist[0]['UserDetail']['image']))
										{
											 $business_image=SITE_LINK."img/default_new1.png";
										}
										else{
											$business_image=SITE_LINK."img/profile/".$callist[0]["UserDetail"]["image"];
										}
									}
									else
									{
										if($callist[0]["UserDetail"]["fbimage"]!=""){
											$business_image=$callist[0]["UserDetail"]["fbimage"];
										}
										else if(empty($callist[0]["UserDetail"]["image"]) || !file_exists(WWW_ROOT."/img/profile/".$callist[0]['UserDetail']['image']))
										{
											 $business_image=SITE_LINK."img/default_new1.png";
										}
										else{
											$business_image=SITE_LINK."img/profile/".$callist[0]["UserDetail"]["image"];
										}
									}	
									
									$detail=array("business"=> $callist[0]["UserDetail"]["business"],"image"=> $business_image,"bus_desc"=> $callist[0]["UserDetail"]["aboutme"],"address"=> $callist[0]["UserDetail"]["address"],"latitude"=> $callist[0]["UserDetail"]["latitude"],"longitude"=> $callist[0]["UserDetail"]["longitude"]);
									$data=array_merge($detail,$events);
								$this->result = array("status"=>200,"message"=>"Event Manager's event list","data"=>$data);
							}
							else
							{
								$this->result = array("status"=>200,"message"=>"No Events to Display");
							}
						}
						else
						{	
							$this->result = array("status"=>201,"message"=>"Invalid Manager ID");
						}		
					}
					else
					{	
						$this->result = array("status"=>201,"message"=>"Mananer ID is empty");
					}
				}
				else
				{
					$this->result = array("status"=>201,"message"=>"Invalid user");
				}
			}
			else
			{
				$this->result = array("status"=>201,"message"=>"Invalid Request");
			}
		
	}
	// Calendar -- On Click of UserProfile 	
	public function manager_profile_new($month = null) {
		if ($this->requestMethod == 'GET' ) {				
				$temp['calendar'] = $this->variables->query;				
				$ids=array();
				$user_id= $this->userId;
				if ($user=$this->User->find("first",array("conditions"=>array("User.id"=>$user_id))))	
				 { 	
					$sel_date="";		
					if(empty($temp['calendar']["month"]))
					$showmonth=date("Y-m-d");			
					else
					$showmonth=$temp['calendar']["month"];
					
					$empty=array();
					
					if(!empty($temp['calendar']["event_manag_id"]) && isset($temp['calendar']["event_manag_id"]))
					{
						if ($user=$this->User->find("first",array("conditions"=>array("User.id"=>$temp['calendar']["event_manag_id"],"User.user_type_id IN"=>array("0"=>2,"1"=>4)))))	
						{
							$event_manag_id=$temp['calendar']["event_manag_id"];
							$sql = "select MONTH('".$showmonth."') as currentMonth,DATE_FORMAT(LAST_DAY('".$showmonth."'),'%d') lastDay,DATE_FORMAT('".$showmonth."','%Y-%m-01') firstDay, DATE_FORMAT(DATE_FORMAT('".$showmonth."','%Y-%m-01'),'%a') first_day,DATE_FORMAT(DATE_ADD('".$showmonth."', Interval 1 month),'%Y-%m-01') as nextMonth,DATE_FORMAT(DATE_SUB('".$showmonth."', Interval 1 month),'%Y-%m-01') as prevMonth, date(now()) as currDate";
											
							$date = date('M Y',strtotime($showmonth));
							$resultcal=$this->Calendar->query($sql);		
													
							$eventArr=array();							
							$this->conditions=array("Calendar.user_id"=>$event_manag_id,"Calendar.event_status"=>1,"Calendar.is_active"=>1);
						
							$this->Calendar->virtualFields["event_status_date"]="select count(*) from calendars where date(eventdate) = Calendar.eventdate";
										
							$this->Calendar->virtualFields["next_event_status"]="(select case when Calendar.repeat_event = 2 then DATE_ADD(eventdate,INTERVAL 1 WEEK) when Calendar.repeat_event = 3 then DATE_ADD(eventdate,INTERVAL 2 WEEK) when Calendar.repeat_event = 4 then DATE_ADD(eventdate,INTERVAL 1 MONTH) end from calendars where id = Calendar.id )";
							
							$this->Calendar->hasOne = $this->Calendar->belongsTo = $this->Calendar->hasMany = array();
							$this->Calendar->belongsTo = array(	
								"User"=>array(
								"className"=>"User",
								"foreignKey"=>false,
								//"type"=>"Inner",
								"conditions"=>"Calendar.user_id=User.id"
								),	
								"UserDetail"=>array(
									"className"=>"UserDetail",
									"foreignKey"=>false,
									//"type"=>"Inner",
									"conditions"=>"Calendar.user_id=UserDetail.user_id"
								)									
							);								
							
							if($callist = $this->Calendar->find('all',array("conditions"=>$this->conditions,"fields"=>array("DAY(eventdate) as day","id","eventdate","event_status","repeat_event","Calendar.event_status_date","Calendar.next_event_status","UserDetail.business","UserDetail.image","UserDetail.fbimage","User.identifier","UserDetail.aboutme","UserDetail.address","UserDetail.longitude","UserDetail.latitude"),"order"=>"event_status desc")))
							{
								foreach($callist as $list){
									//~ if(in_array($list['Calendar']['id'],$ids))
									//~ {
										$eventObj=$list['Calendar'];
										$eventObj['selected_month']=date('Y-m',strtotime($showmonth))."-".date('d');
										$eventObj['current_month']=date('Y-m-d');
										$eventObj['event_id']=$list['Calendar']["id"];
										$eventObj['business']=$list['UserDetail']["business"];
										$eventObj['image']=$list['UserDetail']["image"];
										$eventObj['fbimage']=$list['UserDetail']["fbimage"];
										$eventObj['aboutme']=$list['UserDetail']["aboutme"];
										$eventObj['selected_month_btw']=date('Y-m',strtotime($showmonth));	
										$eventArr[]=$eventObj;
										unset($eventObj);
									//~ }
									}
							
									//Making dates
									$selectedDay = array();							
									$currDate = ($resultcal[0][0]['firstDay']); 
									foreach ( $eventArr as $key=>$val ) {									
																															
										$btw_start=date("Y-m-d", strtotime($val['eventdate']));
										$btw_end=date("Y-m-d", strtotime($val['eventdate']." +2 months"));		
											
										$start = date_create($val['eventdate']);				
										$t1 = strtotime($val['eventdate']);
										$date = date("d",$t1);
										
										for ($temp = 1; $temp <= $resultcal[0][0]['lastDay']; $temp++) {

											
											$temp1 = str_pad($temp,2,0,STR_PAD_LEFT);
											$tmpCurrDate = date_create(date("Y-m-".$temp1,strtotime($currDate)));
											$t2temp = (date("Y-m-".$temp1,strtotime($currDate)));
											$t2 = strtotime($t2temp);
											$d2 = date("d",$t2);
											$diff = date_diff($start,$tmpCurrDate);
											$d = $diff->format("%R%a");
											//echo "<br/>";
											$d1 = $diff->format("%a");
											$btw_date=date("Y-m-d", strtotime($val['selected_month_btw'].'-'.$temp));
											if ( $val['repeat_event'] == 1 && $t24 >= $t1 && ($val['eventdate']<= $btw_date && $btw_date <= $btw_end)) {
												$selectedDay[$temp]['date'] = date("Y-m-",strtotime($showmonth)).$temp;
												$selectedDay[$temp]['event_id'] = $val['event_id'];
												$selectedDay[$temp]['event_status'] = $val['event_status'];
												$selectedDay[$temp]['current_month'] = $val['current_month'];
												$selectedDay[$temp]['selected_month'] = $val['selected_month'];
											
											} elseif ($val['repeat_event'] == 2 && $d >= 0 && ($d%7) == 0 && ($val['eventdate']<= $btw_date && $btw_date <= $btw_end)) {
												
												$selectedDay[$temp]['date'] = date("Y-m-",strtotime($showmonth)).$temp;
												$selectedDay[$temp]['event_id'] = $val['event_id'];
												$selectedDay[$temp]['event_status'] = $val['event_status'];
												$selectedDay[$temp]['current_month'] = $val['current_month'];
												$selectedDay[$temp]['selected_month'] = $val['selected_month'];		
															
											} elseif ($val['repeat_event'] == 3 && $d >= 0 && ($d%14) == 0 && ($val['eventdate']<= $btw_date && $btw_date <= $btw_end)) {
												$selectedDay[$temp]['date'] = date("Y-m-",strtotime($showmonth)).$temp;
												$selectedDay[$temp]['event_id'] = $val['event_id'];
												$selectedDay[$temp]['event_status'] = $val['event_status'];
												$selectedDay[$temp]['current_month'] = $val['current_month'];
												$selectedDay[$temp]['selected_month'] = $val['selected_month'];		
																
											} elseif ($val['repeat_event'] == 4 && $d >= 0 && $date == $d2 && ($val['eventdate']<= $btw_date && $btw_date <= $btw_end)) {
												
												$selectedDay[$temp]['date'] = date("Y-m-",strtotime($showmonth)).$temp;
												$selectedDay[$temp]['event_id'] = $val['event_id'];
												$selectedDay[$temp]['event_status'] = $val['event_status'];
												$selectedDay[$temp]['current_month'] = $val['current_month'];
												$selectedDay[$temp]['selected_month'] = $val['selected_month'];		
																
											} elseif ($val['repeat_event'] == 0 && $date == $d2 && ($val['eventdate']<= $btw_date && $btw_date <= $btw_end)) {										
												if(date("m",strtotime($val['eventdate']))==date("m",strtotime($val['selected_month']))){
												$selectedDay[$temp]['date'] = date("Y-m-",strtotime($showmonth)).$temp;
												$selectedDay[$temp]['event_id'] = $val['event_id'];
												$selectedDay[$temp]['event_status'] = $val['event_status'];
												$selectedDay[$temp]['current_month'] = $val['current_month'];
												$selectedDay[$temp]['selected_month'] = $val['selected_month'];
												}					
											}
											if(!empty($selectedDay[$temp]) && $val['event_status'] && !isset($selectedDay[$temp]['is_live'])){
												$selectedDay[$temp]['is_live']=true;
											}											
										}
										//die;
									}
									$i=0;
									$data=array();	
									foreach($selectedDay as $key=>$value)
									{
										$data[$i]["date"]=$value["date"];
										$data[$i]["event_id"]=$value["event_id"];
										$data[$i]["event_status"]=$value["event_status"];
										$data[$i]["current_month"]=$value["current_month"];
										$data[$i]["selected_month"]=$value["selected_month"];
										if(isset($value["is_live"]))
										{
											$data[$i]["is_live"]=$value["is_live"];
										}else{
											$data[$i]["is_live"]=false;
										}
											$i++;
									}
									if(empty($data))
									{
										$events=array("events"=>$empty);
									}
									else{
										$events=array("events"=>$data);
									}
									
									
									if($callist[0]["User"]["identifier"]=="")
									{
										if(empty($callist[0]["UserDetail"]["image"]) || !file_exists(WWW_ROOT."/img/profile/".$callist[0]['UserDetail']['image']))
										{
											 $business_image=SITE_LINK."img/default_new1.png";
										}
										else{
											$business_image=SITE_LINK."img/profile/".$callist[0]["UserDetail"]["image"];
										}
									}
									else
									{
										if($callist[0]["UserDetail"]["fbimage"]!=""){
											$imageprofile=$callist[0]["UserDetail"]["fbimage"];
										}
										else if(empty($callist[0]["UserDetail"]["image"]) || !file_exists(WWW_ROOT."/img/profile/".$callist[0]['UserDetail']['image']))
										{
											 $business_image=SITE_LINK."img/default_new1.png";
										}
										else{
											$business_image=SITE_LINK."img/profile/".$callist[0]["UserDetail"]["image"];
										}
									}
									
									
									
									
									$detail=array("business"=> $callist[0]["UserDetail"]["business"],"image"=> $business_image,"bus_desc"=> $callist[0]["UserDetail"]["aboutme"],"address"=> $callist[0]["UserDetail"]["address"],"latitude"=> $callist[0]["UserDetail"]["latitude"],"longitude"=> $callist[0]["UserDetail"]["longitude"]);
									$data=array_merge($detail,$events);
								$this->result = array("status"=>200,"message"=>"Event Manager's event list","data"=>$data);
							}
							else
							{
								$this->result = array("status"=>200,"message"=>"No Events to Display","data"=>$empty);
							}
						}
						else
						{	
							$this->result = array("status"=>201,"message"=>"Invalid Manager ID","data"=>$empty);
						}		
					}
					else
					{	
						$this->result = array("status"=>201,"message"=>"Mananer ID is empty","data"=>$empty);
					}
				}
				else
				{
					$this->result = array("status"=>201,"message"=>"Invalid user");
				}
			}
			else
			{
				$this->result = array("status"=>201,"message"=>"Invalid Request");
			}
		
	}
	// Calendar -- On Click of UserProfile -- Calendar open - on select of any date fetch all events on that day added by User	
	public function bus_cale_event_by_date($month = null) {
		if ($this->requestMethod == 'GET' ) {				
			$temp['calendar'] = $this->variables->query;				
			$user_id= $this->userId;
			
			if ($user=$this->User->find("first",array("conditions"=>array("User.id"=>$user_id))))	
			 {
				 $resultcal="";
				$ids=array();
				$list="";
				$callList=array();
				if(isset($temp['calendar']["date"]) && isset($temp['calendar']["event_manag_id"]) && !empty($temp['calendar']["date"]) && !empty($temp['calendar']["event_manag_id"]))
				{
					$month=date("Y-m-d",strtotime($temp['calendar']["date"]));
					$event_manag_id=$temp['calendar']["event_manag_id"];	
						
					$this->conditions=array("Calendar.user_id"=>$event_manag_id);
					
					$sql="SELECT group_concat(id),group_concat(eventdate) FROM `calendars` WHERE user_id=".$event_manag_id." and event_status=1 and `repeat_event` = 2 and abs(datediff('".$month."',date(`eventdate`))%7) = 0	 and date(eventdate) <= '".$month."'";
					$resultcal =$this->Calendar->query($sql);
					
					if(!empty($resultcal[0][0]['group_concat(id)']))
					{	
						$id_event=explode(",",$resultcal[0][0]['group_concat(id)']);
						$eventdateList=explode(",",$resultcal[0][0]['group_concat(eventdate)']);					
						$i=0;
						foreach($id_event as $idEvent)
						{
							for($i;$i<=count($eventdateList);)
							{
								$eventdate=$eventdateList[$i];$i++;
								break;
							}
							$two_month=date("Y-m-d",strtotime($eventdate." +2 months"));
							
							if($eventdate<=$month && $month<=$two_month)
							{
								$ids[]=$idEvent;				
							}			
						}
					}
					
					
					$sql="SELECT group_concat(id),group_concat(eventdate) FROM `calendars` WHERE user_id=".$event_manag_id." and event_status=1 and `repeat_event` = 3 and abs(datediff('".$month."',date(`eventdate`))%14) = 0 and date(eventdate) <= '".$month."'";				
					$resultcal=$this->Calendar->query($sql);
					if(!empty($resultcal[0][0]['group_concat(id)']))
					{	
						$id_event=explode(",",$resultcal[0][0]['group_concat(id)']);
						$eventdateList=explode(",",$resultcal[0][0]['group_concat(eventdate)']);					
						$i=0;
						foreach($id_event as $idEvent)
						{
							for($i;$i<=count($eventdateList);)
							{
								$eventdate=$eventdateList[$i];$i++;
								break;
							}
							$two_month=date("Y-m-d",strtotime($eventdate." +2 months"));
							
							if($eventdate<=$month && $month<=$two_month)
							{
								$ids[]=$idEvent;				
							}			
						}
					}
					
					$sql="SELECT group_concat(id),group_concat(eventdate) FROM `calendars` WHERE user_id=".$event_manag_id." and event_status=1 and  `repeat_event` = 4 and day('".$month."') = day(`eventdate`) and day(eventdate) <= day('".$month."')";
					
					$resultcal=$this->Calendar->query($sql);				
					if(!empty($resultcal[0][0]['group_concat(id)']))
					{	
						$id_event=explode(",",$resultcal[0][0]['group_concat(id)']);
						$eventdateList=explode(",",$resultcal[0][0]['group_concat(eventdate)']);					
						$i=0;
						foreach($id_event as $idEvent)
						{
							for($i;$i<=count($eventdateList);)
							{
								$eventdate=$eventdateList[$i];$i++;
								break;
							}
							$two_month=date("Y-m-d",strtotime($eventdate." +2 months"));
							
							if($eventdate<=$month && $month<=$two_month)
							{
								$ids[]=$idEvent;				
							}			
						}
					}
					//pr($ids);die;
					
					$sql="SELECT group_concat(id),group_concat(eventdate) FROM `calendars` WHERE user_id=".$event_manag_id." and event_status=1 and  `repeat_event` = 1 and date(eventdate) <= '".$month."'";
					$resultcal=$this->Calendar->query($sql);	
					if(!empty($resultcal[0][0]['group_concat(id)']))
					{	
						$id_event=explode(",",$resultcal[0][0]['group_concat(id)']);
						$eventdateList=explode(",",$resultcal[0][0]['group_concat(eventdate)']);					
						$i=0;
						foreach($id_event as $idEvent)
						{
							for($i;$i<=count($eventdateList);)
							{
								$eventdate=$eventdateList[$i];$i++;
								break;
							}
							$two_month=date("Y-m-d",strtotime($eventdate." +2 months"));
							
							if($eventdate<=$month && $month<=$two_month)
							{
								$ids[]=$idEvent;				
							}			
						}
					}
					
					$sql="SELECT group_concat(id),group_concat(eventdate) FROM `calendars` WHERE user_id=".$event_manag_id." and event_status=1 and  `repeat_event` = 0 and date(eventdate) = '".$month."'";	
					$resultcal=$this->Calendar->query($sql);	
					if(!empty($resultcal[0][0]['group_concat(id)']))
					{	
						$id_event=explode(",",$resultcal[0][0]['group_concat(id)']);
						$eventdateList=explode(",",$resultcal[0][0]['group_concat(eventdate)']);					
						$i=0;
						foreach($id_event as $idEvent)
						{
							for($i;$i<=count($eventdateList);)
							{
								$eventdate=$eventdateList[$i];$i++;
								break;
							}
							$two_month=date("Y-m-d",strtotime($eventdate." +2 months"));
							
							if($eventdate<=$month && $month<=$two_month)
							{
								$ids[]=$idEvent;				
							}			
						}
					}
					
					if(!empty($ids))
					 {
						$this->conditions=array_merge($this->conditions,array("Calendar.id IN"=> $ids));
						$this->Calendar->hasOne = $this->Calendar->belongsTo = $this->Calendar->hasMany = array();
							$this->Calendar->belongsTo = array(	
								"User"=>array(
								"className"=>"User",
								"foreignKey"=>false,
								//"type"=>"Inner",
								"conditions"=>"Calendar.user_id=User.id"
								),	
								"UserDetail"=>array(
									"className"=>"UserDetail",
									"foreignKey"=>false,
									//"type"=>"Inner",
									"conditions"=>"Calendar.user_id=UserDetail.user_id"
								)
							);
								
						$list = $this->Calendar->find('all',array("conditions"=>$this->conditions,"fields"=>"Calendar.user_id,start_hour,id,title,image,eventdate,event_status,UserDetail.latitude,UserDetail.longitude,UserDetail.address"));
						
						if(!empty($list))
						{
							$i = 0;
							foreach($list as $val)
							{								
								$callList[$i]["user_id"]=$val["Calendar"]["user_id"];
								$callList[$i]["start_hour"]=$val["Calendar"]["start_hour"];
								$callList[$i]["id"]=$val["Calendar"]["id"];
								$callList[$i]["title"]=$val["Calendar"]["title"];
								if(empty($val["Calendar"]["image"]) || !file_exists(WWW_ROOT."/img/events/".$val['Calendar']['image']))
								{
									 $imagevent=SITE_LINK."img/default_event.png";
								}
								else{
									$imagevent=SITE_LINK."img/events/".$val["Calendar"]["image"];
								}
								$callList[$i]["image"]=$imagevent;
								$callList[$i]["eventdate"]=$val["Calendar"]["eventdate"];
								$callList[$i]["event_status"]=$val["Calendar"]["event_status"];	
								$callList[$i]["latitude"]=$val["UserDetail"]["latitude"];
								$callList[$i]["longitude"]=$val["UserDetail"]["longitude"];								
								$callList[$i]["location"]=$val["UserDetail"]["address"];						
								$i++;
								
							}
							$this->result = array("status"=>200,"message"=>"Event Manager's event list","data"=>$callList);
						}
						else
						{
						   $this->result = array("status"=>200,"message"=>"Error Occured");
						}
					}
					else
					{
						$this->result = array("status"=>200,"message"=>"No data to display");
					}
				}
				else
				{
					$this->result = array("status"=>200,"message"=>"Event date OR Manager info not provided");
				}
			 }
			else
			{
				$this->result = array("status"=>201,"message"=>"Invalid user");
			}
		}
		else
		{
			$this->result = array("status"=>201,"message"=>"Invalid Request");
		}
	}
	
	
	// App user add  event into their calendar
		function add_event_viewed() {			
			if ($this->requestMethod == 'GET' ) {				
				$temp['calendar'] = $this->variables->query;				
				
				$user_id= $this->userId;
				if ($user=$this->User->find("first",array("conditions"=>array("User.id"=>$user_id))))	
				 {		
					$event_id= $temp['calendar']['event_id'];
					$event_date= $temp['calendar']['event_date'];
					if(!empty($event_id))
					{
						$tmp["SaveCalendar"]["event_id"]=$event_id;
						$tmp["SaveCalendar"]["user_id"]=$user_id;	
						$tmp["SaveCalendar"]["event_date"]=$event_date;
						$tmp["SaveCalendar"]["type"]="viewed";
						$this->SaveCalendar = ClassRegistry::init('SaveCalendar');
						if($event=$this->SaveCalendar->find("first",array("conditions"=>array("event_date"=>$event_date,"user_id"=>$user_id,"event_id"=>$event_id,"type"=>"viewed"))))
						{ 						
							$this->result = array("status"=>200,"message"=>"Event already viewed.");
						}
						else
						{
							$callist = $this->SaveCalendar->save($tmp);
							if($callist)
							{
								$this->result = array("status"=>200,"message"=>"Event viewed added");
							}
							else
							{
								$this->result = array("status"=>201,"message"=>"Error Occured");
							}	
						}					
					}
					else
					{
						$this->result = array("status"=>201,"message"=>"Invalid event id");
					}
				}
				else
				{
					$this->result = array("status"=>201,"message"=>"Invalid user");
				}	
			}
			else
			{
				$this->result = array("status"=>201,"message"=>"Invalid Request");
			}
				return json_encode($this->result);
		}
	
	// App user add  comment
		function add_comment() {			
			if ($this->requestMethod == 'POST' ) {		
					
				$temp['comment'] = $this->variables->data;	
				$this->Notification = ClassRegistry::init('Notification');
				$this->SaveCalendar = ClassRegistry::init('SaveCalendar');
				$user_id= $this->userId;
				$total_comment=0;
				$user_id_list=array();
				if($user1=$this->User->find("first",array("conditions"=>array("User.id"=>$user_id))))	
				 { 			
					if(!empty($temp['comment']['event_id']) && !empty($temp['comment']['date']))
					{	
						$this->Comment = ClassRegistry::init('Comment');
						$temp['comment']["user_id"]=$user_id;
						$temp['comment']["is_active"]=1;
						$temp['comment']["viewed"]=0;
						$temp['comment']['comment_add']=$temp['comment']["date"]." ".date("h:i:s");
						//$temp['comment']['created']=
						
						if( isset($temp['comment']["text"]))
						{			
							$temp['comment']["comment"]=$temp['comment']["text"];
						}
						else
						{
							$temp['comment']["comment"]="";
						}
						if(!empty($temp['comment']["user_ids"]))
						{
							$user_id_list=$temp['comment']["user_ids"];
						}
						elseif(!empty($temp['comment']["user_id_list"]))
						{
							$user_id_list=explode(",",$temp['comment']["user_id_list"]);							
						}
						else
						{
							$user_id_list="";
						}
						
						$callist = $this->Comment->save($temp['comment']);
						//pr($user_id_list);
					    $comment_id=$callist['Comment']["id"];
						if($callist)
						{  
							// setting for further use
							$event_id=$callist['Comment']['event_id'];
							$event_date=date('Y-m-d',strtotime($callist['Comment']['created']));
							$comment_id=$callist['Comment']['id'];
							if($user_id_list)
							//if( isset($ids) && !empty($ids))
							{
								$this->User->hasOne = array(
									"UserDevice"=>array(
										"className"=>"UserDevice",
										"foreignKey"=>false,
										"type"=>"Left",
										"conditions"=>"User.id=UserDevice.user_id"
									),
									"UserDetail" => array(
										"className" => "UserDetail",
										"foreignKey" => "user_id",
										"type" => "Inner",
										"conditions"=>"User.id=UserDevice.user_id"
									)
								);
								$mentionedUsers=$this->User->find('all',array('conditions'=>array('User.id IN'=>$user_id_list,'User.user_type_id'=>3,'UserDetail.comment_mention'=>1)));
							
								$tokenByDeviceType=array();
								$notifications=array();
								$notificationText='';
								
								
								foreach($mentionedUsers as $user){
									if(!empty($user['UserDevice']['token'])){
										$tokenByDeviceType[$user['UserDevice']['device_OS']][]=$user['UserDevice']['token'];
									}
									// adding notification to save
									$data=array();
									$data['Notification']['sender_id']=$user_id;
									$data['Notification']['user_id']=$user['User']['id'];
									if($user1["UserDetail"]["business"]!="") { $name=$user1["UserDetail"]["business"]; } else { $name="Someone" ;}
									$data['Notification']['text']= $notificationText = $user1['UserDetail']['business'].' mentioned you in a comment';
									$data['Notification']['type']='Notifications';
									$data['Notification']['comment_id']=$callist['Comment']['id'];
									$data['Notification']['event_id']=$temp['comment']['event_id'];
									$data['Notification']['comment_add']=$temp['comment']['date'];
									$notifications[]=$data;
									
								}
								
								// save notifications and if notification text sin't empty send the notifications
								if($notify=$this->Notification->saveall($notifications) && !empty($notificationText)){
									foreach($tokenByDeviceType as $deviceType => $tokens){
										$this->NotificationV1->push_message($tokens,$notificationText,$event_id,$comment_id,$event_date,$deviceType,'comment');
									}
								}
								
								
							}		
							$this->SaveCalendar->hasOne = $this->SaveCalendar->belongsTo = $this->SaveCalendar->hasMany = array();
								$this->SaveCalendar->belongsTo = array(
									"User"=>array(
										"className"=>"User",
										"foreignKey"=>false,
										"type"=>"Inner",
										"conditions"=>"User.id=SaveCalendar.user_id"
									),
									"UserDetail"=>array(
										"className"=>"UserDetail",
										"foreignKey"=>false,
										"type"=>"Inner",
										"conditions"=>"UserDetail.user_id=SaveCalendar.user_id"
									),
									"UserDevice"=>array(
										"className"=>"UserDevice",
										"foreignKey"=>false,
										"type"=>"Inner",
										"conditions"=>"UserDevice.user_id=SaveCalendar.user_id"
									)
								);								
							if($userSaveCal=$this->SaveCalendar->find("all",array("conditions"=>array("User.user_type_id"=>3,"UserDetail.comment_save_event"=>1,"event_id"=>$temp['comment']["event_id"],"SaveCalendar.event_date"=>$temp['comment']["date"],"type"=>'calendar',"User.id !="=>$user_id),'fields'=>array("SaveCalendar.user_id","UserDetail.business","UserDevice.token","UserDevice.device_OS"))))
							{				
								$tokenByDeviceType=array();
								$notifications=array();
								$notificationText='';
								foreach($userSaveCal as $calsave)
								{	
									if(!empty($calsave['UserDevice']['token'])){
										$tokenByDeviceType[$calsave['UserDevice']['device_OS']][]=$calsave['UserDevice']['token'];
									}
									$data=array();
									$data['Notification']["sender_id"]=$user_id;
									$data['Notification']["user_id"]=$calsave["SaveCalendar"]["user_id"];
									if($user1["UserDetail"]["business"]!="") { $name=$user1["UserDetail"]["business"]; } else { $name="Someone" ;}
									$data['Notification']["text"]=$notificationText = $name." commented on event you have saved";
									$data['Notification']["type"]="Notifications";	
									$data['Notification']['comment_id']=$callist['Comment']['id'];									
									$data['Notification']["event_id"]=$temp['comment']["event_id"];
									$data['Notification']["comment_add"]=$temp['comment']["date"];
									$notifications[]=$data;							 
								}
									
								// save notifications and if notification text sin't empty send the notifications
								if($notify=$this->Notification->saveall($notifications) && !empty($notificationText)){
									foreach($tokenByDeviceType as $deviceType => $tokens){
										
										$this->NotificationV1->push_message($tokens,$notificationText,$event_id,$comment_id,$event_date,$deviceType,'comment');
									}
								}					
								
							}	
							$this->result = array("status"=>200,"message"=>"Comment Added");	
								
						}
						else
						{
							$this->result = array("status"=>201,"message"=>"Error Occured");
						}
						
					}
					else
					{
						$this->result = array("status"=>201,"message"=>"Date or event ID not provided");
					}
				}
				else
				{
					$this->result = array("status"=>201,"message"=>"Invalid user");
				}	
			}
			else
			{
				$this->result = array("status"=>201,"message"=>"Invalid Request");
			}
				return json_encode($this->result);
		}
		
	 // App user add  comment
		function fetch_comment() {			
			if ($this->requestMethod == 'GET' ) {				
				$temp['comment'] = $this->variables->query;				
				$user_id= $this->userId;
				if ($user=$this->User->find("first",array("conditions"=>array("User.id"=>$user_id))))	
				 { 					
					if(!empty($temp['comment']["event_id"]) && isset($temp['comment']["event_id"]) && !empty($temp['comment']["date"]) && isset($temp['comment']["date"]))
					{											
						$this->conditions=array("Comment.event_id"=>$temp['comment']['event_id'],"Comment.is_active"=>1,"date(Comment.comment_add)"=>$temp['comment']['date']);
						$this->Comment = ClassRegistry::init('Comment');	
						
						$this->Comment->hasOne = $this->Comment->belongsTo = $this->Comment->hasMany = array();
							$this->Comment->belongsTo = array(
								"User"=>array(
									"className"=>"User",
									"foreignKey"=>false,
									"type"=>"Inner",
									"conditions"=>"User.id=Comment.user_id"
								),
								"UserDetail"=>array(
									"className"=>"UserDetail",
									"foreignKey"=>false,
									"type"=>"Inner",
									"conditions"=>"UserDetail.user_id=Comment.user_id"
								)
							);	
							$comments=$this->Comment->find("all",array("conditions"=>$this->conditions,"order"=>"Comment.created desc",'fields'=>array("Comment.id","Comment.comment","Comment.event_id","Comment.user_id","Comment.comment","Comment.created","Comment.comment_add","UserDetail.image","UserDetail.fbimage","User.identifier","UserDetail.business","UserDetail.see_my_profile","User.user_type_id","User.id","UserDetail.see_my_profile")));	
							
							$commentList=array();
							$comment_list=array();
							if($comments)
							{
								$i=0;
									foreach($comments as $comment)
									{	
										if($comment["User"]["identifier"]=="")
											{
												 if(empty($comment["UserDetail"]["image"]) || !file_exists(WWW_ROOT."/img/profile/".$comment['UserDetail']['image']))
												{
													  $imageprofile=SITE_LINK."img/default_new1.png";
												}
												else{
													$imageprofile=SITE_LINK."img/profile/".$comment["UserDetail"]["image"];
												}							
											}
											else
											{
												if($comment["UserDetail"]["fbimage"]!=""){
													$imageprofile=$comment["UserDetail"]["fbimage"];
												}
												else if(empty($comment["UserDetail"]["image"]) || !file_exists(WWW_ROOT."/img/profile/".$comment['UserDetail']['image']))
												{
													  $imageprofile=SITE_LINK."img/default_new1.png";
												}
												else{
													$imageprofile=SITE_LINK."img/profile/".$comment["UserDetail"]["image"];
												}						
											}
										
										$flag=false;
										 $is_follow=false;
										if($comment["User"]["user_type_id"]==3)
										{
											  $user_type="appuser";	
											  if($comment['User']["id"]==$user_id)
											  {
												   $user_type="self";
											  }
											  
											  $this->Follower = ClassRegistry::init('Follower');
											  if ($is_following=$this->Follower->find("first",array("conditions"=>array("Follower.follower_id"=>$comment['User']["id"],"Follower.user_id"=>$user_id))))
											  {
												  $is_follow=true;
											  }		 
											  if($comment["UserDetail"]["see_my_profile"]==0)//Nobody
											  {
													$flag=false;							 
											  }
											  else  if($comment["UserDetail"]["see_my_profile"]==1)//Followers
											  {		
												if($is_follow==true)
												  {				
													$flag=true;							
												  }
												  else
												  {
													  $flag=false;
												  }
											  }
											  else//everyone
											  {
												  $flag=true;
											  }
										}
										else
										{
											$flag=true;
											$user_type="manager";
										}
										
										$created_in_text= $this->NotificationV1->time_elapsed_string($comment["Comment"]["created"], true);
										$comment_list[$i]["id"]=$comment["Comment"]["id"];
										$comment_list[$i]["text"]=$comment["Comment"]["comment"];
										$comment_list[$i]["event_id"]=$comment["Comment"]["event_id"];
										$comment_list[$i]["user_id"]=$comment["Comment"]["user_id"];
										$comment_list[$i]["created"]=$comment["Comment"]["created"];
										$comment_list[$i]["comment_add"]=$comment["Comment"]["comment_add"];
										$comment_list[$i]["business_image"]=$imageprofile;
										$comment_list[$i]["created_in_text"]=$created_in_text;
										$comment_list[$i]["business"]=$comment["UserDetail"]["business"];
										$comment_list[$i]["user_type"]=$user_type;
										$comment_list[$i]["view_profile"]=$flag;
										$i++;
									}
								$total_comment=array("count"=>count($comments));
								$list=array("list"=>$comment_list);
								
								$commentList=array_merge($total_comment,$list);	
								$this->result = array("status"=>200,"message"=>"List Comments","data"=>$commentList);
							}
							else
							{
								$total_comment=array("count"=>0);
								$list=array("list"=>$comment_list);								
								$commentList=array_merge($total_comment,$list);	
								$this->result = array("status"=>200,"message"=>"No comment to display","data"=>$commentList);
							}						
					}
					else
					{
						$this->result = array("status"=>201,"message"=>"Date or event ID not provided");
					}
				}
				else
				{
					$this->result = array("status"=>201,"message"=>"Invalid user");
				}	
			}
			else
			{
				$this->result = array("status"=>201,"message"=>"Invalid Request");
			}
				return json_encode($this->result);
		}
		
		
		//Fetch Calendar events added to Wishlist
		function is_event_save_calendar() {
			if ($this->requestMethod == 'GET' ) {				
					$temp['calendar'] = $this->variables->query;				
					$user_id= $this->userId;
					//pr($temp['calendar']['event_id']); die;
					if(!empty($temp['calendar']['event_date']) && isset($temp['calendar']['event_date']) && !empty($temp['calendar']['event_id']) && isset($temp['calendar']['event_id'])) 
					{
						if ($user=$this->User->find("first",array("conditions"=>array("User.id"=>$user_id))))
						//pr($user); die;

						 {
							$this->SaveCalendar = ClassRegistry::init('SaveCalendar');	
							$month=date("Y-m-d",strtotime($temp['calendar']['event_date']));	
							$event=$this->SaveCalendar->find("all",array("conditions"=>array("SaveCalendar.user_id"=>$user_id,"SaveCalendar.event_id"=>$temp['calendar']['event_id'],"SaveCalendar.type"=>"calendar",'SaveCalendar.event_date' => $month)));
							//pr($user_id); die;			
							if ($event=$this->SaveCalendar->find("all",array("conditions"=>array("SaveCalendar.user_id"=>$user_id,"SaveCalendar.event_id"=>$temp['calendar']['event_id'],"SaveCalendar.type"=>"calendar",'SaveCalendar.event_date' => $month))))
							//pr($user_id); die;
							{
								$flag = true;
								$this->result = array("status"=>200,"message"=>$flag);
							
							}
							else
							{
								$flag = false;
								$this->result = array("status"=>200,"message"=>$flag);
								
							}
						}
						else
						{
							$this->result = array("status"=>201,"message"=>"Invalid user");
						}	
				   }
				else
					{
						$this->result = array("status"=>201,"message"=>"Invalid Event id or Event date");
					}	
			}
			else
			{
				$this->result = array("status"=>201,"message"=>"Invalid Request");
			}
				return json_encode($this->result);
		}
	
}
//$events[]=array("id"=>$eventDetail["UserCalendar"]["id"],"event_id"=>$eventDetail["UserCalendar"]["event_id"],"event_add_date"=>$eventDetail["UserCalendar"]["event_date"],"title"=>$eventDetail["Calendar"]["title"],"short_dec"=>$eventDetail["Calendar"]["short_dec"],"long_dec"=>$eventDetail["Calendar"]["long_dec"],"title"=>$eventDetail["Calendar"]["title"],"title"=>$eventDetail["Calendar"]["title"]);
?>
