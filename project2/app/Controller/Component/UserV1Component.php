<?php
App::uses('Component','Controller','AppController');

require_once '../Vendor/autoload.php';
use Firebase\JWT\JWT;


	class UserV1Component extends Component {
		var $name = 'UserV1';
		var $requestMethod = '';
		var $variables = '';
		var $userId = '';
		var $obj = '';
		var $result = array("status"=>200);
		var $siteLink = '';
		var $User = '';
		var $components = array('NotificationV1');
		public function beforeFilter(Event $event) {
			parent::beforeFilter($event); 
		}
		
		function __construct($args) {
			parent::__construct($args);
			$this->requestMethod = $_SERVER['REQUEST_METHOD'];
			$this->User = ClassRegistry::init('User');
			$this->UserDetail = ClassRegistry::init('UserDetail');
			$this->EmailTemplate = ClassRegistry::init('EmailTemplate');
			$obj = $this;			
			
		}
		
		function login() {
			if ($this->requestMethod == 'POST' ) {
				
				//$this->Auth->logout();
				$tmp['User'] = $this->variables->data;					
				$flag = false;
				//pr($tmp['User']);die;
				if ( isset($tmp['User']['identifier']) && !empty($tmp['User']['identifier'])  ) {					
					$api_login=date("Y-m-d h:i:s");
					$data["User"]["api_login"]=$api_login;
					
					if ( $user = $this->User->find("first",array("conditions"=>array("identifier"=>$tmp['User']['identifier']))) ) {
						if($user['User']['is_active']) {
							$flag = true;
							//$tmp = $user;
							$this->User->id=$user["User"]["id"];												
							//unset($tmp["User"]["password"],$tmp["User"]["password"]);
							$this->User->save($data["User"]);
							$user_id=$user["User"]["id"];	
							
							if(isset($tmp['User']['name']) && !empty($tmp['User']['name']))
							{
								$tmp1["UserDetail"]["business"]=$tmp['User']['name'];
							}
							if(isset($tmp['User']['image']) && !empty($tmp['User']['image']))
							{
								//$tmp1["UserDetail"]["image"]=$tmp['User']['image'];
							}
							$tmp1['UserDetail']['id']=	$user['UserDetail']['id'];
							$this->UserDetail->save($tmp1);								
													
						} else {
							$this->result = array("status"=>201,"token"=>null,"message"=>"This account has been disabled by website owner");
							return json_encode($this->result);						
						}
					} else {
						unset($this->User->validate['password']);						
						//$data['User']['password'] = $tmp['User']['identifier'];
						$data['User']['identifier'] = $tmp['User']['identifier'];	
						$data["User"]["is_active"]=1;
						$data["User"]["verified"]=1;
						$data["User"]["user_type_id"] = 3;
						//unset($tmp["User"]["password"],$tmp["User"]["password"]);
						$this->User->save($data["User"]);	
						$user_id=$this->User->getLastInsertId();						
						$tmp1['UserDetail']['user_id']=$user_id;
						if(isset($tmp['User']['name']) && !empty($tmp['User']['name']))
						{
							$tmp1["UserDetail"]["business"]=$tmp['User']['name'];
						}
						if(isset($tmp['User']['image']) && !empty($tmp['User']['image']))
						{
							$image_link = $tmp['User']['image'];//Direct link to image
							//$split_image = pathinfo($image_link);

							$ch = curl_init();
							curl_setopt($ch, CURLOPT_URL , $image_link);
							curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/525.13 (KHTML, like Gecko) Chrome/0.A.B.C Safari/525.13");
							curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
							$response= curl_exec ($ch);
							curl_close($ch);
							$file_path_to_save= "img/profile/";				
							$file_name = mt_rand().strtotime(date("y-m-d h:i:s")).".jpg";
							$file_path=$file_path_to_save."".$file_name;
							$file = fopen($file_path , 'w') or die("X_x");
							fwrite($file, $response);
							fclose($file);	
							$tmp1["UserDetail"]["image"]=$file_name;
														
						}
						
						$this->UserDetail->save($tmp1);	
						$user = $this->User->find("first",array("conditions"=>array("User.id"=>$user_id)));						
					}
					if ( $user["User"]["user_type_id"] == 3) {
					// on success generate jwt						
						if(isset($tmp['User']['device_token']))
							{
								$device_OS="";
								$device_id="";
								if(isset($tmp['User']['device_token']))
								{
									$device_OS=$tmp['User']['device_OS'];
								}			
								$device_token=$tmp['User']['device_token'];
								$this->checkDeviceToken($user["User"]["id"],$device_id,$device_token,$device_OS);
							}	
						$token=$this->genrateToken($user_id,$api_login);
						$this->result = array("status"=>200,"message"=>"Login successfull.","token"=>$token,"user_id"=>$user["User"]["id"]);								
					} else {								
						$this->result = array("status"=>201,"token"=>null,"message"=>"Invalid User.");
					}
					
				} else {
					if ( $user = $this->User->find("first",array("conditions"=>array("username"=>$tmp['User']['email'],"confirmation_token <>"=>""))) ) {
						$this->result = array("status"=>201,"token"=>null,"message"=>"This account hasn't been verified.");
						return json_encode($this->result);						
					}
					
						$password= Security::hash($tmp['User']['password'], null, true);
						
						$user = $this->User->find("first",array("conditions"=>array("username"=>$tmp['User']['email'],"password"=>$password)));
						
						if ( $user = $this->User->find("first",array("conditions"=>array("username"=>$tmp['User']['email'],"password"=>$password))) ) {
							if($user["User"]["is_active"]==1)
							{
								$this->User->id=$user["User"]["id"];
								$api_login=date("Y-m-d h:i:s");
								$this->User->api_login=$api_login;
								$data["User"]["api_login"]=$api_login;
								
								$this->User->save($data["User"]);								
								$device_id=$device_OS=$device_token=0;
								if ( $user["User"]["user_type_id"] == 3) {	
									if(isset($tmp['User']['device_OS']) && isset($tmp['User']['device_token']))
									{
										$device_id="";
										$device_OS=$tmp['User']['device_OS'];
										$device_token=$tmp['User']['device_token'];
										$this->checkDeviceToken($user["User"]["id"],$device_id,$device_token,$device_OS);
									}									
								// on success generate jwt	
									$token=$this->genrateToken($user['User']['id'],$api_login);
									$this->result = array("status"=>200,"message"=>"Login successfull.","token"=>$token,"user_id"=>$user["User"]["id"]);								
								} else {
									//$this->Auth->logout();
									$this->result = array("status"=>201,"token"=>null,"message"=>"Invalid User.");
								}
							}
							else
							{
								$this->result = array("status"=>201,"token"=>null,"message"=>"Your account is deactivated by website owner");
							}
					} else {
						$this->result = array("status"=>201,"token"=>null,"message"=>"Invalid Login credentials.");
					}
				}
			} else {
				$this->result = array("status"=>201,"token"=>null,"message"=>"Invalid request.");
			}
			//$this->log($this->result);
			return json_encode($this->result);			
		}
		//~ function getdata()
		//~ {
			//~ if ($this->requestMethod == 'POST' ) {
				//~ $tmp = $this->variables->data;	
				//~ $jwt = isset($tmp['token']) ? $tmp['token'] : null;
				//~ if (isset($jwt)) {
					//~ // validate jwt
					//~ $key = JWT_SECRET;
					//~ $decoded = JWT::decode($jwt, $key, array('HS256'));
					//~ $decoded_array = (array)$decoded;
					//~ echo $decoded_array['userid'];die;
					//~ 
					//~ 
					//~ 
					//~ $result = array(
						//~ 'code' => 200,
						//~ 'status' => 'success',
						//~ 'data' => getUserAccountData($decoded_array['userid']),
						//~ 'jwt_payload' => $decoded_array
					//~ );
				//~ }			
			//~ }
		//~ }
	   function signup() {
		if ($this->requestMethod == 'POST' ) {
			$tmp['User'] = $this->variables->data;
			
			$password="";
				
				if(isset($tmp['User']['email'])  && !empty($tmp['User']['email']) && isset($tmp['User']['password'])  && !empty($tmp['User']['password']))
				{
					if($user = $this->User->find("first",array("conditions"=>array("username"=>$tmp['User']['email']))))
					{							
						$this->result = array("status"=>201,"token"=>null,"message"=>"This email id already exists.");						
						return json_encode($this->result);
					
					}
					$password= Security::hash($tmp['User']['password'], null, true); 
					//$password=$this->encryptpass($tmp['User']['password']);
				}
				else
				{
					$this->result = array("status"=>201,"token"=>null,"message"=>"Email or Password is not provided");
					return json_encode($this->result);
				}
				
							
				$tmp['User']['user_type_id'] = 3;
				
				//$tmp['UserDetail']['name'] =$tmp['User']['name'];
				
				$tmp['User']['confirmation_token'] = $confirmToken = $this->encryptpass($tmp['User']['email']);
				
				$this->User->set($tmp);
				
				if ( isset($tmp['User']['email']) && !empty($tmp['User']['email']) ) {
					if ( $user = $this->User->find("first",array("conditions"=>array("username"=>$tmp['User']['email'],"confirmation_token <>"=>""))) ) {
						$this->result = array("status"=>201,"token"=>null,"message"=>"This account hasn't been verified.");
						
						return json_encode($this->result);
						
					}
				}
				
				
				if ( $this->User->validates() && $this->User->UserDetail->validates() ) {
					if(!empty($password)) $tmp['User']['password']=$password;
					$tmp['User']['username']=$tmp['User']['email'];
					$tmp['User']['is_active']=0;
					if ($this->User->saveAll($tmp,array("validate"=>false))) {	
						$last_id=$this->User->getLastInsertID();
						$tmp1['UserDetail']['user_id']=$last_id;						
						$this->UserDetail->save($tmp1);								
						$this->getMailData("SIGNUP_CONFIRMATION");
						$link = SITE_LINK."confirmlink/".$confirmToken;
						//$this->mailBody = str_replace("{User}",($tmp['UserDetail']['name']),$this->mailBody);
						$this->mailBody = str_replace("{User}",("Rave User"),$this->mailBody);
						$this->mailBody = str_replace("{CLICK_HERE}",$link,$this->mailBody);
						$this->mailBody = str_replace("{URL}",SITE_LINK,$this->mailBody);
						//$this->mailBody = str_replace("{LINK}",$link,$this->mailBody);
						$this->sendMail($tmp['User']['email']);
						$this->result = array("status"=>200,"token"=>null,"message"=>"Signup Successfully. Please check your email for the confirmation link.");
					}
				} else {
					$arrMsg = array();
					if (isset($this->User->validationErrors['username'][0])) {
						$arrMsg[] = $this->User->validationErrors['username'][0];
					}
					if (isset($this->User->validationErrors['password'][0])) {
						$arrMsg[] = $this->User->validationErrors['password'][0];
					}
					//~ if (isset($this->User->UserDetail->validationErrors['name'][0])) {
						//~ $arrMsg[] = $this->User->UserDetail->validationErrors['name'][0];
					//~ }
					$this->result = array("status"=>201,"token"=>null,"message"=>$arrMsg);
				}			 
		} else {
			$this->result = array("status"=>201,"token"=>null,"message"=>"Invalid request.");
		}
		return  json_encode($this->result);
	}
	
	  function encryptpass($password, $salt = '', $method = 'md5', $crop = true, $start = 4, $end = 10) {
		$salt = strtotime(date("Y-m-d h:i:s"));
		if ($crop) {
			$password = $method(substr($method($password.$salt), $start, $end));
		} else {
			$password = $method($password.$salt);
		}
		return $password;
    }
    
    
    public function forgot_password() {
			if ($this->requestMethod == 'POST' ) {
			$tmp['User'] = $this->variables->data;	
			$password="";		
			
			 $users = $this->User->find("first",array("conditions"=>array("User.username"=>$tmp['User']['email'],"User.user_type_id"=>3),"recursive"=>0));			
			 if ( !empty($users) ) {
				 if($users['User']['is_active']==1)
				 {
						$forgotToken  = $this->encryptpass($users['User']['username']);
						$users['User']['password_token'] = $forgotToken;
						if ( $this->User->save($users,array("validate"=>false)) ) {
							$this->getMailData("FORGOT_PASSWORD");
							$link = SITE_LINK."reset_password/".$forgotToken;
							$this->mailBody = str_replace("{username}",$users['UserDetail']['business'],$this->mailBody);
							$this->mailBody = str_replace("{CLICK_HERE}",$link,$this->mailBody);
							$this->mailBody = str_replace("{support_mail}","support@moolarewards.co.uk",$this->mailBody);
							$this->sendMail($users['User']['username']);
							$this->result = array("status"=>200,"token"=>null,"message"=>"Your request to reset password is submitted, please check your email.");
							
						} else {
							
							$this->result = array("status"=>201,"token"=>null,"message"=>"Invalid request.");
						} 
				} else {
							
							$this->result = array("status"=>201,"token"=>null,"message"=>"This account is un-verified or de-activated. Please contact for further information.");
						} 		
			} else {
				$this->result = array("status"=>201,"token"=>null,"message"=>"Invalid email address.");
			}
			
		}
		else
		{
			$this->result = array("status"=>201,"token"=>null,"message"=>"Invalid request");	
		}
		return json_encode($this->result);		
	}
	
	
	function changepassword() {
		
			if ($this->requestMethod == 'POST' ) {				
				$data['User'] = $this->variables->data;
				
				$jwt = isset($data['User']['token']) ? $data['User']['token'] : null;
				if (isset($jwt)) {
				// validate jwt
					$key = JWT_SECRET;
					$decoded = JWT::decode($jwt, $key, array('HS256'));
					$decoded_array = (array)$decoded;
					$user_id= $decoded_array['userid'];
					
					if ($data['User']["newpassword"]=="" || $data['User']["confirmpassword"]=="") {
						$this->result = array("status"=>201,"token"=>null,"message"=>"Current password or Confrim password is not correct.");	
						return ;
					} elseif($data['User']['newpassword']!= $data['User']['confirmpassword']) {	
						$this->result = array("status"=>201,"token"=>null,"message"=>"New and confirm password do not match.");
						return ;
					}					
					
					$user = $this->User->find("first",array("conditions"=>array("User.id"=>$user_id),'recursive'=>-1));	
					if ( $user) {											
						$oldpassword =$user['User']['password'];
						$newpassword= Security::hash($data['User']['newpassword'], null, true);			
						if($oldpassword == $newpassword){
							$this->result = array("status"=>201,"token"=>null,"message"=>"New password can not be same as current password.");
							return ;
						}
					    else {							
							$tmpData['id'] = $user_id;
							$tmpData['password'] =  $newpassword;
							$this->User->create();							
							if ( $this->User->save($tmpData,array("validate"=>false)) ) {
								$this->result = array("status"=>200,"token"=>null,"message"=>"Password has been updated successfully.");			
							} else {
								$this->result = array("status"=>201,"token"=>null,"message"=>"Password can not be updated.");			
							}
						}
					}
					else
					{
						$this->result = array("status"=>201,"token"=>null,"message"=>"Invalid User");
					}
			}
			else
			{
				$this->result = array("status"=>201,"token"=>null,"message"=>"Invalid Token Request");
			}
		}
		else
		{
			$this->result = array("status"=>201,"token"=>null,"message"=>"Invalid request");	
		}	
			return json_encode($this->result);
			
   }
	
	
	
    /*
     * @function name	: getmaildata
     * @purpose			: getting email data for various purposes
     * @arguments		: Following are the arguments to be passed:
     * id				: id of email templates from cmsemail table
     * @return			: NONE
     * @created on		: 
     * @description		: function will assign value to global variables like mailbody,from, subject which will be used while sending email
     */

    function getMailData($mail_slug = null, $to = null) {
        //$this->loadModel('EmailTemplate');
        
        $cmsemail = $this->EmailTemplate->find('first', array('conditions' => array('EmailTemplate.slug' => $mail_slug)));       
        if (!empty($cmsemail)) {
            $this->mailBody = $cmsemail['EmailTemplate']['content'];
            $this->from = $cmsemail['EmailTemplate']['email_from'];
            $this->subject = str_replace("{TO}", $to, $cmsemail['EmailTemplate']['subject']);
        }        
    }
    
    
    /* end of function */
    /*
     * @function name	: sendmail
     * @purpose			: sending email for various actions
     * @arguments		: Following are the arguments to be passed:
     * from		: contain email address from which email is sending
     * Subject	: Subject of Email
     * to		: Email address to whom the email is sending
     * body		: content of email
     * template : if defining a html template for sending email else false.
     * values	: to be given in email template like username etc.
     * @return			: true if email sending successfull else return false.
     * @created on		: 11th March 2014
     * @description		: NA
     */

    function sendMail($to, $template = 'email', $fromname = 'Rave') {
		//pr($this->mailBody);die;
        App::uses('CakeEmail', 'Network/Email');
       // if (isset($this->params->base) && !empty($this->params->base)) {
            //$email = new CakeEmail("gmail");
        //} else {
           $email = new CakeEmail("zestminds");
        //}
        //Use filter_var_array for multiple emails
        $this->from = "mandeepteja@zestminds.com";
        $is_valid = is_array($to) ? filter_var_array($to, FILTER_VALIDATE_EMAIL) : filter_var($to, FILTER_VALIDATE_EMAIL);
        if ($is_valid) {
            $email->from(array($this->from => $fromname));
            $email->to($to);
            $email->subject($this->subject);
            $headers[] = 'MIME-Version: 1.0';
            $headers[] = 'Content-type: text/html; charset=iso-8859-1';
            $email->addHeaders($headers);
            $email->emailFormat('both');
            if (empty($template)) {
				try {
					if ( !$email->send($this->mailBody)) {
						throw new Exception;
                    } else {
						return true;
                    }
                } catch (Exception $e) {
                    return false;
                }
            } else {
                if (!empty($this->mailBody)) {
                    $email->viewVars(array("mail" => $this->mailBody));
                }
                $email->template($template, '');
                try {
                    if (!$email->send()) {
						throw new Exception;
                    } else {
						return true;
                    }
                    
                } catch (Exception $e) {
					echo $e->getMessage();
					die;
                    return false;
                } 
            }
        } else {
            return false;
        }
    }

    /* end of function */
    function checkDeviceToken($user_id,$device_id = NULL, $device_token = NULL, $os = NULL) {
			$this->UserDevice = ClassRegistry::init('UserDevice');			
			if ($tmp = $this->UserDevice->find("first",array("conditions"=>array("user_id"=>$user_id))) ) {			
						
			}
			else
			{
				$tmp['UserDevice']['user_id'] = $user_id;
			}			
				$tmp['UserDevice']['device_id'] = $device_id;
				$tmp['UserDevice']['device_OS'] = $os;
				$tmp['UserDevice']['token'] = $device_token;
				if ( $this->UserDevice->save($tmp,array("validates"=>false)) ) {
					
				}
		}
		
		function updatefirebasetoken() {
			if ($this->requestMethod == 'GET' ) {
				$user_id= $this->userId;
				$data['UserDevice'] = $this->variables->query;
				$this->UserDevice = ClassRegistry::init('UserDevice');
				if ( $tmp = $this->UserDevice->find("first",array("conditions"=>array("user_id"=>$user_id))) ) {
				} else {
					$tmp['UserDevice']['user_id'] = $user_id;
				}
				$tmp['UserDevice']['device_id'] = "";
				$tmp['UserDevice']['device_token'] = $data['UserDevice']['device_token'];
				$tmp['UserDevice']['device_OS'] = $data['UserDevice']['device_OS'];
				if ( $this->UserDevice->save($tmp,array("validates"=>false)) ) {
					$this->result = array("status"=>200,"token"=>null,"message"=>"Device Token Updated");
				} else {
					$this->result = array("status"=>201,"token"=>null,"message"=>"Device Token could not be updated, please try again.");
				}
			} else
			{
				$this->result = array("status"=>201,"token"=>null,"message"=>"Invalid Request Method");	
			}
			return json_encode($this->result);
		}
		
		function logout() {		
			if ($this->requestMethod == 'GET' ) {	
				
			    $user_id= $this->userId;
				//$data['User'] = $this->variables->data;
				$data["User"]["api_login"]=date("Y-m-d h:i:s");				
				$this->User->id=$this->userId;
				if ( $this->User->save($data,array("validate"=>false)) ) {
					
					$this->result = array("status"=>200,"token"=>null,"message"=>"logout");	
				}
				else
				{
					$this->result = array("status"=>201,"token"=>null,"message"=>"Invalid request.");	
				}
			}
			else
			{
				$this->result = array("status"=>201,"token"=>null,"message"=>"Invalid Request Method");	
			}	
			return json_encode($this->result);
		}
		function myprofile() {	
			if ($this->requestMethod == 'GET' ) {					
			    $user_id= $this->userId;
				//$data['User'] = $this->variables->data;
					$following=$follower=0;		
				//$this->User->id=$this->userId;
				$comment_mention=$comment_follow_me=$comment_save_event=false;
				if ($user=$this->User->find("first",array("conditions"=>array("User.id"=>$user_id))))	
				 { 
					$data=json_decode($this->usersfollowedbyme());
					if(isset($data->data))
					{
						$following=count($data->data);
					}
					$data_follow=json_decode($this->myfollowers());
					if(isset($data_follow->data))
					{
						$follower=count($data_follow->data);
					}
					
					if($user["UserDetail"]["comment_mention"]==1)
					{
						$comment_mention=true;
					}
					if($user["UserDetail"]["comment_save_event"]==1)
					{
						$comment_save_event=true;
					}
					if($user["UserDetail"]["comment_follow_me"]==1)
					{
						$comment_follow_me=true;
					}	
					if($user["User"]["identifier"]=="")
					{				
						if(empty($user["UserDetail"]["image"]) || !file_exists(WWW_ROOT."/img/profile/".$user['UserDetail']['image']))
						{
							 $imageprofile=SITE_LINK."img/profile.png";
						}
						else{
							$imageprofile=SITE_LINK."img/profile/".$user["UserDetail"]["image"];
						}
					}
					else
					{
						if($user["UserDetail"]["fbimage"]!=""){
							$imageprofile=$user["UserDetail"]["fbimage"];
						}
						else if(empty($user["UserDetail"]["image"]) || !file_exists(WWW_ROOT."/img/profile/".$user['UserDetail']['image']))
						{
							 $imageprofile=SITE_LINK."img/profile.png";
						}
						else{
							$imageprofile=SITE_LINK."img/profile/".$user["UserDetail"]["image"];
						}
					}
					$user_detail=array("business"=>$user["UserDetail"]["business"],"aboutme"=>$user["UserDetail"]["aboutme"],"image"=>$imageprofile,"comment_mention"=>$comment_mention,"comment_save_event"=>$comment_save_event,"comment_follow_me"=>$comment_follow_me,"see_my_profile"=>$user["UserDetail"]["see_my_profile"],"following"=>$following,"follower"=>$follower);
					$this->result = array("status"=>200,"token"=>null,"message"=>"User my profile detail","data"=>$user_detail);	
				}
				else
				{
					$this->result = array("status"=>201,"token"=>null,"message"=>"Invalid user.");	
				}
			}
			else
			{
				$this->result = array("status"=>201,"token"=>null,"message"=>"Invalid Request Method");	
			}	
			return json_encode($this->result);
		}
		function updatemyprofile() {	
			if ($this->requestMethod == 'POST' ) {					
			    $user_id= $this->userId;
			    $following=$follower=0;	
				$data['UserDetail'] = $this->variables->data;	
				$this->User->id=$user_id;
				if ($user=$this->User->find("first",array("conditions"=>array("User.id"=>$user_id))))	
				 { 
					$id=$user["UserDetail"]["id"];
					 //$data['UserDetail']['user_id']=$this->userId;
					 if (isset($this->variables->form['image']['name']) && !empty($this->variables->form['image']['name'])) {
						$this->uploadImage($this->variables->form['image'],"profile");
						$data['UserDetail']['image'] = $this->imagename;
						$data['UserDetail']['fbimage'] = "";
					} elseif( isset($this->variables->form['fbimage']) && !empty($this->variables->form['fbimage']) ) {
						$data['UserDetail']['image'] = $data['UserDetail']['fbimage'];
					} else {
						//unset($tmp['UserDetail']['image']);
					}
					$this->UserDetail->id=$id;						
					if($user=$this->UserDetail->save($data))
					{
						$comment_mention=$comment_follow_me=$comment_save_event=false;
						$data=json_decode($this->usersfollowedbyme());
						if(isset($data->data))
						{
							$following=count($data->data);
						}
						$data_follow=json_decode($this->myfollowers());
						if(isset($data_follow->data))
						{
							$follower=count($data_follow->data);
						}
											
					
						$user=$this->User->find("first",array("conditions"=>array("User.id"=>$user_id)));		
						if($user["UserDetail"]["comment_mention"]==1)
						{
							$comment_mention=true;
						}
						if($user["UserDetail"]["comment_save_event"]==1)
						{
							$comment_save_event=true;
						}
						if($user["UserDetail"]["comment_follow_me"]==1)
						{
							$comment_follow_me=true;
						}
						if($user["User"]["identifier"]=="")
						{				
							if(empty($user["UserDetail"]["image"]) || !file_exists(WWW_ROOT."/img/profile/".$user['UserDetail']['image']))
							{
								 $imageprofile=SITE_LINK."img/profile.png";
							}
							else{
								$imageprofile=SITE_LINK."img/profile/".$user["UserDetail"]["image"];
							}
						}
						else
						{
							if($user["UserDetail"]["fbimage"]!=""){
								$imageprofile=$user["UserDetail"]["fbimage"];
							}
							else if(empty($user["UserDetail"]["image"]) || !file_exists(WWW_ROOT."/img/profile/".$user['UserDetail']['image']))
							{
								 $imageprofile=SITE_LINK."img/profile.png";
							}
							else{
								$imageprofile=SITE_LINK."img/profile/".$user["UserDetail"]["image"];
							}
						}				
						$user_detail=array("business"=>$user["UserDetail"]["business"],"aboutme"=>$user["UserDetail"]["aboutme"],"image"=>$imageprofile,"comment_mention"=>$comment_mention,"comment_save_event"=>$comment_save_event,"comment_follow_me"=>$comment_follow_me,"see_my_profile"=>$user["UserDetail"]["see_my_profile"]);
						$this->result = array("status"=>200,"token"=>null,"message"=>"Profile updated","data"=>$user_detail,"following"=>$following,"follower"=>$follower);
						
					}
					else
					{
						$this->result = array("status"=>200,"token"=>null,"message"=>"Error Occured");
					}
				}
				else
				{
					$this->result = array("status"=>201,"token"=>null,"message"=>"Invalid user.");	
				}
			}
			else
			{
				$this->result = array("status"=>201,"token"=>null,"message"=>"Invalid Request Method");	
			}	
			return json_encode($this->result);
		}
		//Who is following me
		function myfollowers() {
			if ($this->requestMethod == 'GET' ) {					
			    $user_id= $this->userId;
				$data['User'] = $this->variables->query;
				$this->Follower = ClassRegistry::init('Follower');
				$this->Follower->hasOne = $this->Follower->belongsTo = $this->Follower->hasMany = array();
				$this->Follower->belongsTo = array(
					"User"=>array(
							"className"=>"User",
							"foreignKey"=>false,
							"type"=>"Inner",
							"conditions"=>"User.id=Follower.user_id"
						),
					"UserDetail"=>array(
						"className"=>"UserDetail",
						"foreignKey"=>false,
						"type"=>"Inner",
						"conditions"=>"UserDetail.user_id=Follower.user_id"
					)
				);	
				$followers=array();
				if ($user=$this->User->find("first",array("conditions"=>array("User.id"=>$user_id))))	
				{ 
					$this->conditions=array("Follower.follower_id"=>$user_id,"User.is_active"=>1);
					if(isset($data['User']['search']) && !empty($data['User']['search']))
					{
						$search = strtolower($data['User']['search']);					
						$this->conditions=array_merge($this->conditions,array("OR"=>array("LOWER(UserDetail.business)  LIKE "=>'%'.$search.'%')));
					}
					if ($user=$this->Follower->find("all",array("conditions"=>$this->conditions)))	
					 {
					  foreach($user as $users_followers)
					  {
						  $is_follow=false;
						  if ($is_following=$this->Follower->find("all",array("conditions"=>array("Follower.follower_id"=>$users_followers["Follower"]["user_id"],"Follower.user_id"=>$user_id))))	
							{
								$is_follow=true;
							}						  
							
							if($users_followers["User"]["identifier"]=="")
							{
								 if(empty($users_followers["UserDetail"]["image"]) || !file_exists(WWW_ROOT."/img/profile/".$users_followers['UserDetail']['image']))
								{
									  $imageprofile=SITE_LINK."img/default_new1.png";
								}
								else{
									$imageprofile=SITE_LINK."img/profile/".$users_followers["UserDetail"]["image"];
								}							
							}
							else
							{
								if($users_followers["UserDetail"]["fbimage"]!=""){
									$imageprofile=$users_followers["UserDetail"]["fbimage"];
								}
								else if(empty($users_followers["UserDetail"]["image"]) || !file_exists(WWW_ROOT."/img/profile/".$users_followers['UserDetail']['image']))
								{
									  $imageprofile=SITE_LINK."img/default_new1.png";
								}
								else{
									$imageprofile=SITE_LINK."img/profile/".$users_followers["UserDetail"]["image"];
								}						
							}
														
							
							$flag=false;							
							if($users_followers["User"]["user_type_id"]==3)
							{
								  $user_type="appuser";	
								  if($users_followers['User']["id"]==$user_id)
								  {
									   $user_type="self";
								  }	
								  if($users_followers["UserDetail"]["see_my_profile"]==0)//Nobody
								  {
										$flag=false;							 
								  }
								  else  if($users_followers["UserDetail"]["see_my_profile"]==1)//Followers
								  {		
									if($is_follow==true)
									  {				
										$flag=true;							
									  }
									  else
									  {
										  $flag=false;
									  }
								  }
								  else//everyone
								  {
									  $flag=true;
								  }
							}
							else
							{
								$flag=true;
								$user_type="webuser";
							}
							$followers[]=array("business"=>$users_followers["UserDetail"]["business"],"user_id"=>$users_followers["UserDetail"]["user_id"],"image"=>$imageprofile,"is_follow"=>$is_follow,"user_type"=>$user_type,"view_profile"=>$flag);
						}
						$this->result = array("status"=>200,"token"=>null,"message"=>"List of followers","data"=>$followers);	
					}
					else
					{
						$this->result = array("status"=>201,"token"=>null,"message"=>"No one is following.");	
					}
				}
				else
				{
					$this->result = array("status"=>201,"token"=>null,"message"=>"Invalid user");	
				}
			}
			else
			{
				$this->result = array("status"=>201,"token"=>null,"message"=>"Invalid Request Method");	
			}	
			return json_encode($this->result);
		}
		
		//List app user on fishook (make frnds) // Not using anywhere
		
		function listappusertofollow() {
			if ($this->requestMethod == 'GET' ) {					
			    $user_id= $this->userId;
				//$data['User'] = $this->variables->data;
				$is_follow=false;
				if ($user=$this->User->find("first",array("conditions"=>array("User.id"=>$user_id))))	
				{ 
					if ($user=$this->User->find("all",array("conditions"=>array("User.user_type_id"=>3,"User.id!="=> $user_id,"User.is_active"=>1))))	
					 {
					   foreach($user as $users_followers)
					   {
						   $is_follow=false;
						   $this->Follower = ClassRegistry::init('Follower');
						  if (!$is_following=$this->Follower->find("first",array("conditions"=>array("Follower.follower_id"=>$users_followers["User"]["id"],"Follower.user_id"=>$user_id))))	
							{
								if($users_followers["User"]["identifier"]=="")
								{
								   if(empty($users_followers["UserDetail"]["image"]) || !file_exists(WWW_ROOT."/img/profile/".$users_followers['UserDetail']['image']))
									{
										  $imageprofile=SITE_LINK."img/default_new1.png";
									}
									else{
										$imageprofile=SITE_LINK."img/profile/".$users_followers["UserDetail"]["image"];
									}
								}
								else
								{
									if($users_followers["UserDetail"]["fbimage"]!=""){
										$imageprofile=$users_followers["UserDetail"]["fbimage"];
									}
									else if(empty($users_followers["UserDetail"]["image"]) || !file_exists(WWW_ROOT."/img/profile/".$users_followers['UserDetail']['image']))
									{
										  $imageprofile=SITE_LINK."img/default_new1.png";
									}
									else{
										$imageprofile=SITE_LINK."img/profile/".$users_followers["UserDetail"]["image"];
									}
								}
								$followers[]=array("business"=>$users_followers["UserDetail"]["business"],"user_id"=>$users_followers["UserDetail"]["user_id"],"image"=>$imageprofile);
							}
						}
						$this->result = array("status"=>200,"token"=>null,"message"=>"List to follow","data"=>$followers);	
					}
					else
					{
						$this->result = array("status"=>201,"token"=>null,"message"=>"No user to follow on rave");	
					}
				}
				else
				{
					$this->result = array("status"=>201,"token"=>null,"message"=>"Invalid user");	
				}
			}
			else
			{
				$this->result = array("status"=>201,"token"=>null,"message"=>"Invalid Request Method");	
			}	
			return json_encode($this->result);
		}				
		
		function followuser() {
			if ($this->requestMethod == 'GET' ) {					
			    $user_id= $this->userId;
				$data['Follower'] = $this->variables->query;
				$this->Follower = ClassRegistry::init('Follower');
				if ($user_detail=$this->User->find("first",array("conditions"=>array("User.id"=>$user_id))))	
				{
					$data['Follower']["user_id"]=$user_id;
					if(isset($data['Follower']["follower_id"]))
					{
						$follow_id=$data['Follower']["follower_id"];
											
						$this->Follower = ClassRegistry::init('Follower');
						//$follower_id=$callist["Calendar"]["user_id"];
						//follower_id  is Follow ID
						$this->conditions=array("Follower.user_id"=>$user_id,"Follower.follower_id"=>$follow_id);	
						if($follow_list = $this->Follower->find('first',array("conditions"=>$this->conditions)))
						{
							$this->result = array("status"=>200,"token"=>null,"message"=>"You already follow this person");	
						}
						else
						{
							if($follower=$this->Follower->save($data['Follower']))	
							{	
								//notification
								if($user=$this->User->find("first",array("conditions"=>array("User.id"=>$follow_id))))
								{
									if($user["User"]["user_type_id"]==3)
									{
										if($user["UserDetail"]["comment_follow_me"]==1)
										{
											
											  $view_profile=false;									  
											  if($user["UserDetail"]["see_my_profile"]==0)//Nobody
											  {
													$view_profile=false;							 
											  }
											  else  if($user["UserDetail"]["see_my_profile"]==1)//Followers
											  {			
													$view_profile=true;	
											  }
											  else//everyone
											  {
												  $view_profile=true;
											  }											
											
											
											$data['Notification']["sender_id"]=$user_id;
											$data['Notification']["user_id"]=$follow_id;
											$data['Notification']["type"]="follow";
											$data['Notification']["text"]=$user_detail["UserDetail"]["business"]." started following you";
											$user_type="app_user";
											$this->Notification = ClassRegistry::init('Notification');
										
											if($notify=$this->Notification->save($data['Notification']))	
											{
												$this->UserDevice = ClassRegistry::init('UserDevice');
												
												if($token=$this->UserDevice->find("first",array("conditions"=>array("User.id"=>$follow_id))))
												{
													$user_device_key[]=$token["UserDevice"]["token"];
													$device_type=$token["UserDevice"]["device_OS"];
																										
													$message=$data['Notification']["text"];
													$notificationType="follow";
													$data=$this->NotificationV1->push_message_follow($user_device_key,$message,$user_id,$user_type,$device_type,$view_profile,$notificationType);
												}								
									        }
										}	
									}
								}	
								$this->result = array("status"=>200,"token"=>null,"message"=>"Follower Added");	
							}
							else
							{
								$this->result = array("status"=>201,"token"=>null,"message"=>"Error Occured");	
							}
						}
					}
					else
					{
						$this->result = array("status"=>201,"token"=>null,"message"=>"Invalid data");	
					}
				}
				else
				{
					$this->result = array("status"=>201,"token"=>null,"message"=>"Invalid user");	
				}
			}
			else
			{
				$this->result = array("status"=>201,"token"=>null,"message"=>"Invalid Request Method");	
			}	
			return json_encode($this->result);
		}
		function unfollowuser() {	
			if ($this->requestMethod == 'GET' ) {					
			    $user_id= $this->userId;
				$data['Follower'] = $this->variables->query;
				$this->Follower = ClassRegistry::init('Follower');
				if ($user=$this->User->find("first",array("conditions"=>array("User.id"=>$user_id))))	
				{
					$this->conditions=array("user_id"=>$user_id,"follower_id"=>$data['Follower']["follower_id"]);				
					if ($follow=$this->Follower->find("first",array("conditions"=>$this->conditions)))					
					 { 			
						$this->Follower->id=$follow["Follower"]["id"];	
						$follower=$this->Follower->delete();	
						$this->result = array("status"=>200,"token"=>null,"message"=>"Follower Deleted");
					}
					else
					{
						$this->result = array("status"=>201,"token"=>null,"message"=>"Invalid data.");	
					}
				}
				else
				{
					$this->result = array("status"=>201,"token"=>null,"message"=>"Invalid user");	
				}
			}
			else
			{
				$this->result = array("status"=>201,"token"=>null,"message"=>"Invalid Request Method");	
			}	
			return json_encode($this->result);
		}
		// Show list of user whom I am following
		function usersfollowedbyme() {	
			if ($this->requestMethod == 'GET' ) {					
			    $user_id= $this->userId;
				$data['User'] = $this->variables->query;
				$this->Follower = ClassRegistry::init('Follower');
				if ($user=$this->User->find("first",array("conditions"=>array("User.id"=>$user_id))))	
				{
					$this->Follower->hasOne = $this->Follower->belongsTo = $this->Follower->hasMany = array();
					$this->Follower->belongsTo = array(
						"User"=>array(
							"className"=>"User",
							"foreignKey"=>false,
							"type"=>"Inner",
							"conditions"=>"User.id=Follower.follower_id"
						),
						"UserDetail"=>array(
							"className"=>"UserDetail",
							"foreignKey"=>false,
							"type"=>"Inner",
							"conditions"=>"UserDetail.user_id=Follower.follower_id"
						)
					);	
					$followers=array();
					
					$this->conditions=array("Follower.user_id"=>$user_id,"User.is_active"=>1);
					if(isset($data['User']['search']) && !empty($data['User']['search']))
					{
						$search = strtolower($data['User']['search']);					
						$this->conditions=array_merge($this->conditions,array("OR"=>array("LOWER(UserDetail.business)  LIKE "=>'%'.$search.'%')));
					}
					//pr($this->conditions);die;
					if ($user=$this->Follower->find("all",array("conditions"=>$this->conditions)))	
					 { 
					  foreach($user as $users_followers)
					  {	
								
								if($users_followers["User"]["identifier"]=="")
								{
								   if(empty($users_followers["UserDetail"]["image"]) || !file_exists(WWW_ROOT."/img/profile/".$users_followers['UserDetail']['image']))
									{
										  $imageprofile=SITE_LINK."img/default_new1.png";
									}
									else{
										$imageprofile=SITE_LINK."img/profile/".$users_followers["UserDetail"]["image"];
									}
								}
								else
								{
									if($users_followers["UserDetail"]["fbimage"]!=""){
										$imageprofile=$users_followers["UserDetail"]["fbimage"];
									}
									else if(empty($users_followers["UserDetail"]["image"]) || !file_exists(WWW_ROOT."/img/profile/".$users_followers['UserDetail']['image']))
									{
										  $imageprofile=SITE_LINK."img/default_new1.png";
									}
									else{
										$imageprofile=SITE_LINK."img/profile/".$users_followers["UserDetail"]["image"];
									}
								}
								
							$this->Follower = ClassRegistry::init('Follower');		
							$is_follow=false;
							if ($is_following=$this->Follower->find("all",array("conditions"=>array("Follower.follower_id"=>$user_id,"Follower.user_id"=>$users_followers["Follower"]["user_id"]))))	
							{
								$is_follow=true;
							}		
							$flag=false;
							
							if($users_followers["User"]["user_type_id"]==3)
							{
								  $user_type="appuser";	
								  if($users_followers['User']["id"]==$user_id)
								  {
									   $user_type="self";
								  }	
								  if($users_followers["UserDetail"]["see_my_profile"]==0)//Nobody
								  {
										$flag=false;							 
								  }
								  else  if($users_followers["UserDetail"]["see_my_profile"]==1)//Followers
								  {		
									if($is_follow==true)
									  {				
										$flag=true;							
									  }
									  else
									  {
										  $flag=false;
									  }
								  }
								  else//everyone
								  {
									  $flag=true;
								  }
							}
							else
							{
								$flag=true;
								$user_type="webuser";
							}
						$followers[]=array("business"=>$users_followers["UserDetail"]["business"],"user_id"=>$users_followers["UserDetail"]["user_id"],"image"=>$imageprofile,"user_type"=>$user_type,"view_profile"=>$flag);
						}					
						$this->result = array("status"=>200,"token"=>null,"message"=>"List of following Users","data"=>$followers);	
					}
					else
					{
						$this->result = array("status"=>201,"token"=>null,"message"=>"Not follwing anyone.");	
					}
				}
				else
				{
					$this->result = array("status"=>201,"token"=>null,"message"=>"Invalid user");	
				}	
			}
			else
			{
				$this->result = array("status"=>201,"token"=>null,"message"=>"Invalid Request Method");	
			}
			return json_encode($this->result);
		}
		// Show list of user whom I am serching to tag and also following
		function search_following_user() {
			if ($this->requestMethod == 'GET' ) {					
			    $user_id= $this->userId;
				$tmp['user'] = $this->variables->query;
				
				$this->Follower = ClassRegistry::init('Follower');
				if(isset($tmp['user']["search"]))
				{
					if ($user=$this->User->find("first",array("conditions"=>array("User.id"=>$user_id))))	
					{
						$this->Follower->hasOne = $this->Follower->belongsTo = $this->Follower->hasMany = array();
						$this->Follower->belongsTo = array(
							"User"=>array(
								"className"=>"User",
								"foreignKey"=>false,
								"type"=>"Inner",
								"conditions"=>"User.id=Follower.follower_id"
							),
							"UserDetail"=>array(
								"className"=>"UserDetail",
								"foreignKey"=>false,
								"type"=>"Inner",
								"conditions"=>"UserDetail.user_id=Follower.follower_id"
							)
						);	
						$this->conditions=array("User.is_active"=>1,"Follower.user_id"=>$user_id,"User.user_type_id"=>3);
						$followers=array();
						
						$search = str_replace("@",'',$tmp['user']["search"]);
						$search = strtolower($search);
						
						
						if(!empty($search))
						{
							$this->conditions=array_merge($this->conditions,array("OR"=>array("LOWER(UserDetail.business)  LIKE "=>'%'.$search.'%')));
						}
					
						
						if ($user=$this->Follower->find("all",array("conditions"=>$this->conditions)))
						 {				
						  foreach($user as $users_followers)
						  {	
							  if($users_followers["User"]["identifier"]=="")
									{
									   if(empty($users_followers["UserDetail"]["image"]) || !file_exists(WWW_ROOT."/img/profile/".$users_followers['UserDetail']['image']))
										{
											  $imageprofile=SITE_LINK."img/default_new1.png";
										}
										else{
											$imageprofile=SITE_LINK."img/profile/".$users_followers["UserDetail"]["image"];
										}
									}
									else
									{
										if($users_followers["UserDetail"]["fbimage"]!=""){
											$imageprofile=$users_followers["UserDetail"]["fbimage"];
										}
										else if(empty($users_followers["UserDetail"]["image"]) || !file_exists(WWW_ROOT."/img/profile/".$users_followers['UserDetail']['image']))
										{
											  $imageprofile=SITE_LINK."img/default_new1.png";
										}
										else{
											$imageprofile=SITE_LINK."img/profile/".$users_followers["UserDetail"]["image"];
										}
									}
							$followers[]=array("business"=>$users_followers["UserDetail"]["business"],"user_id"=>$users_followers["UserDetail"]["user_id"],"image"=>$imageprofile);
							}					
							$this->result = array("status"=>200,"token"=>null,"message"=>"List of following Users","data"=>$followers);	
						}
						else
						{
							$this->result = array("status"=>201,"token"=>null,"message"=>"Not follwing anyone.");	
						}
					}
					else
					{
						$this->result = array("status"=>201,"token"=>null,"message"=>"Invalid user");	
					}	
				}
				else
				{
					$this->result = array("status"=>201,"token"=>null,"message"=>"search varible is not set");	
				}	
			}
			else
			{
				$this->result = array("status"=>201,"token"=>null,"message"=>"Invalid Request Method");	
			}
			return json_encode($this->result);
		}
		
		
		//Fetch the other user profile to see and check the options for everyone,nobody,following	
		function fetch_other_user_profile() {	
			if ($this->requestMethod == 'GET' ) {					
			    $user_id= $this->userId;
				$data['User'] = $this->variables->query;
				$is_follow=false;
				$flag=false;
				$following=$follower=0;
				
				if ($user=$this->User->find("first",array("conditions"=>array("User.id"=>$user_id))))	
				{ 
					if(isset($data['User']["user_id"]))
					{
						
						if ($userDetail=$this->User->find("first",array("conditions"=>array("User.is_active"=>1,"User.id"=>$data['User']["user_id"]))))	
						 {						  
							  $this->Follower = ClassRegistry::init('Follower');
							  if ($is_following=$this->Follower->find("first",array("conditions"=>array("Follower.follower_id"=>$data['User']["user_id"],"Follower.user_id"=>$user_id))))
							  {
								  $is_follow=true;
							  }							 
							  if($userDetail["UserDetail"]["see_my_profile"]==0)//Nobody
							  {
									$flag=false;							 
							  }
							  else  if($userDetail["UserDetail"]["see_my_profile"]==1)//Followers
							  {		
								  if($is_follow==true)
								  {					
									$flag=true;							
								  }
								  else
								  {
									  $flag=false;
								  }
							  }
							  else//everyone
							  {
								  $flag=true;
							  }
							  if($flag)
							  {
								  if ($follow_list=$this->Follower->find("all",array("conditions"=>array("Follower.user_id"=>$data['User']["user_id"]))))
								  {
									  $following=count($follow_list);
								  }
								  if ($following_list=$this->Follower->find("all",array("conditions"=>array("Follower.follower_id"=>$data['User']["user_id"]))))
								  {
									  $follower=count($following_list);
								  }
								  if($userDetail["User"]["identifier"]=="")
									{
									   if(empty($userDetail["UserDetail"]["image"]) || !file_exists(WWW_ROOT."/img/profile/".$userDetail['UserDetail']['image']))
										{
											  $imageprofile=SITE_LINK."img/default_new1.png";
										}
										else{
											$imageprofile=SITE_LINK."img/profile/".$userDetail["UserDetail"]["image"];
										}
									}
									else
									{
										if($userDetail["UserDetail"]["fbimage"]!=""){
											$imageprofile=$userDetail["UserDetail"]["fbimage"];
										}
										else if(empty($userDetail["UserDetail"]["image"]) || !file_exists(WWW_ROOT."/img/profile/".$userDetail['UserDetail']['image']))
										{
											  $imageprofile=SITE_LINK."img/default_new1.png";
										}
										else{
											$imageprofile=SITE_LINK."img/profile/".$userDetail["UserDetail"]["image"];
										}
									}
																
									$profile=array("business"=>$userDetail["UserDetail"]["business"],"user_id"=>$userDetail["UserDetail"]["user_id"],"image"=>$imageprofile,"aboutme"=>$userDetail["UserDetail"]["aboutme"],"is_follow"=>$is_follow,"following"=>$following,"follower"=>$follower);
									
									$this->result = array("status"=>200,"token"=>null,"message"=>"List to follow","data"=>$profile);	
							  }
							  else
							  {
								   $this->result = array("status"=>200,"token"=>null,"message"=>"Hidden Information");
							  }
						}
						else
						{
							$this->result = array("status"=>201,"token"=>null,"message"=>"Invalid data");	
						}
					}
					else
					{
						$this->result = array("status"=>201,"token"=>null,"message"=>"Invalid data");	
					}
				}
				else
				{
					$this->result = array("status"=>201,"token"=>null,"message"=>"Invalid user");	
				}
			}
			else
			{
				$this->result = array("status"=>201,"token"=>null,"message"=>"Invalid Request Method");	
			}	
			return json_encode($this->result);
		}
		
		//Who is following me Manager
		function manager_followers() {
			if ($this->requestMethod == 'GET' ) {					
			    $user_id= $this->userId;
			    $me=false;
				$data['User'] = $this->variables->query;
				$this->Follower = ClassRegistry::init('Follower');
				$this->Follower->hasOne = $this->Follower->belongsTo = $this->Follower->hasMany = array();
				$this->Follower->belongsTo = array(
				"User"=>array(
						"className"=>"User",
						"foreignKey"=>false,
						"type"=>"Inner",
						"conditions"=>"User.id=Follower.user_id"
					),
					"UserDetail"=>array(
						"className"=>"UserDetail",
						"foreignKey"=>false,
						"type"=>"Inner",
						"conditions"=>"UserDetail.user_id=Follower.user_id"
					)
				);	
				$followers=array();
				if($user=$this->User->find("first",array("conditions"=>array("User.id"=>$user_id,"User.is_active"=>1))))	
				{ 
					if($follower=$this->Follower->find("all",array("conditions"=>array("User.is_active"=>1,"Follower.follower_id"=>$data['User']["event_manag_id"]))))//,"Follower.user_id !="=>$user_id
					 {
					  foreach($follower as $users_followers)
					  {
						  $me=false;
						  if($users_followers['Follower']["user_id"]==$user_id)
						  {
							  $me=true;
						  }
						  $is_follow=false;
						  if ($is_following=$this->Follower->find("all",array("conditions"=>array("Follower.follower_id"=>$users_followers['Follower']["user_id"],"Follower.user_id"=>$user_id))))	
							{
								$is_follow=true;
							}
							
							if($users_followers["User"]["identifier"]=="")
							{
							   if(empty($users_followers["UserDetail"]["image"]) || !file_exists(WWW_ROOT."/img/profile/".$users_followers['UserDetail']['image']))
								{
									  $imageprofile=SITE_LINK."img/default_new1.png";
								}
								else{
									$imageprofile=SITE_LINK."img/profile/".$users_followers["UserDetail"]["image"];
								}
							}
							else
							{
								if($users_followers["UserDetail"]["fbimage"]!=""){
									$imageprofile=$users_followers["UserDetail"]["fbimage"];
								}
								else if(empty($users_followers["UserDetail"]["image"]) || !file_exists(WWW_ROOT."/img/profile/".$users_followers['UserDetail']['image']))
								{
									  $imageprofile=SITE_LINK."img/default_new1.png";
								}
								else{
									$imageprofile=SITE_LINK."img/profile/".$users_followers["UserDetail"]["image"];
								}
							}					
							$flag=false;							
							if($users_followers["User"]["user_type_id"]==3)
							{
								  $user_type="appuser";	
								  if($users_followers['User']["id"]==$user_id)
								  {
									   $user_type="self";
								  }	
								  if($users_followers["UserDetail"]["see_my_profile"]==0)//Nobody
								  {
										$flag=false;							 
								  }
								  else  if($users_followers["UserDetail"]["see_my_profile"]==1)//Followers
								  {		
									if($is_follow==true)
									  {				
										$flag=true;							
									  }
									  else
									  {
										  $flag=false;
									  }
								  }
								  else//everyone
								  {
									  $flag=true;
								  }
							}
							else
							{
								$flag=true;
								$user_type="webuser";
							}
							
							$followers[]=array("view_profile"=>$flag,"user_type"=>$user_type,"business"=>$users_followers["UserDetail"]["business"],"user_id"=>$users_followers["UserDetail"]["user_id"],"image"=>$imageprofile,"is_follow"=>$is_follow,"me"=>$me);
						}
						$follower_count=array("follower_count"=>count($follower));
						$follower_list=array("follower_list"=>$followers);
						$followers=array_merge($follower_count,$follower_list);
						$this->result = array("status"=>200,"token"=>null,"message"=>"List of followers","data"=>$followers);	
					}
					else
					{
						$this->result = array("status"=>201,"token"=>null,"message"=>"No one is following.");	
					}
				}
				else
				{
					$this->result = array("status"=>201,"token"=>null,"message"=>"Invalid user");	
				}
			}
			else
			{
				$this->result = array("status"=>201,"token"=>null,"message"=>"Invalid Request Method");	
			}	
			return json_encode($this->result);
		}
		//Search on Filter Page TOP
		function list_manager() {
			if ($this->requestMethod == 'GET' ) {					
			    $user_id= $this->userId;
			    $data['User'] = $this->variables->query;	
			    $this->conidtions=array();		   
				if($user=$this->User->find("first",array("conditions"=>array("User.id"=>$user_id))))	
				{ 
					$options=array("User.is_active"=>1,"User.user_type_id IN"=>array("0"=>2,"1"=>3,"2"=>4),"User.id !="=>$user_id);
					
					if(isset($data['User']['search']) && !empty($data['User']['search']))
					{
						$search = strtolower($data['User']['search']);					
						$options=array_merge($options,array("OR"=>array("LOWER(UserDetail.business)  LIKE "=>'%'.$search.'%')));
					}
					if($managers=$this->User->find("all",array("conditions"=>$options)))
					 {
						foreach($managers as $manager)
						{
							if($manager["User"]["identifier"]=="")
								{
									if(empty($manager["UserDetail"]["image"]) || !file_exists(WWW_ROOT."/img/profile/".$manager['UserDetail']['image']))
									{
										 $imageprofile=SITE_LINK."img/default_new1.png";
									}
									else{
										$imageprofile=SITE_LINK."img/profile/".$manager["UserDetail"]["image"];
									}
								}
								else
								{
									if($manager["UserDetail"]["fbimage"]!=""){
										$imageprofile=$manager["UserDetail"]["fbimage"];
									}
									else if(empty($manager["UserDetail"]["image"]) || !file_exists(WWW_ROOT."/img/profile/".$manager['UserDetail']['image']))
									{
										 $imageprofile=SITE_LINK."img/default_new1.png";
									}
									else{
										$imageprofile=SITE_LINK."img/profile/".$manager["UserDetail"]["image"];
									}
								}
							$this->Follower = ClassRegistry::init('Follower');		
							$is_follow=false;
							if ($is_following=$this->Follower->find("all",array("conditions"=>array("Follower.follower_id"=>$manager["Follower"]["user_id"],"Follower.user_id"=>$user_id))))	
							{
								$is_follow=true;
							}	
							
							$flag=false;
							
							if($manager["User"]["user_type_id"]==3)
							{
								  $user_type="appuser";	
								  if($manager['User']["id"]==$user_id)
								  {
									   $user_type="self";
								  }	
								  if($manager["UserDetail"]["see_my_profile"]==0)//Nobody
								  {
										$flag=false;							 
								  }
								  else  if($manager["UserDetail"]["see_my_profile"]==1)//Followers
								  {		
									if($is_follow==true)
									  {				
										$flag=true;							
									  }
									  else
									  {
										  $flag=false;
									  }
								  }
								  else//everyone
								  {
									  $flag=true;
								  }
							}
							else
							{
								$flag=true;
								$user_type="webuser";
							}
							$list[]=array("user_type"=>$user_type,"business"=>$manager["UserDetail"]["business"],"user_id"=>$manager["UserDetail"]["user_id"],"image"=>$imageprofile,"view_profile"=>$flag);
						}						
						$this->result = array("status"=>200,"token"=>null,"message"=>"List of followers","data"=>$list);	
					}
					else
					{
						$this->result = array("status"=>201,"token"=>null,"message"=>"No Business profile to display","data"=>$managers);	
					}
				}
				else
				{
					$this->result = array("status"=>201,"token"=>null,"message"=>"Invalid user");	
				}
			}
			else
			{
				$this->result = array("status"=>201,"token"=>null,"message"=>"Invalid Request Method");	
			}	
			return json_encode($this->result);
		}
		
		
		function genrateToken($userid,$api_login)
		{
			
			$issuedAt = time();
			$api_login=strtotime($api_login);
			$expirationTime = $issuedAt + 63072000;  // jwt valid for 60 seconds from the issued time
			
			$payload = array(
				'api_login' => $api_login,
				'userid' => $userid,				
				'iat' => $issuedAt
				//,
				//'exp' => $expirationTime
			);
			$key = JWT_SECRET;
			$alg = 'HS256';
			$jwt = JWT::encode($payload, $key, $alg);			
			return $jwt;
		}
		
		function uploadImage($file , $destination = NULL, $old_img = false,$first = NULL,$second=NULL, $filetypes = array('jpg', 'jpeg', 'png')) {
		$flag = false;
		$file_ext = explode(".",$file['name']);
		$file_ext = strtolower(end($file_ext));
		$this->imagename = $this->uploaddir =  '';
		if ( in_array($file_ext,$filetypes) ) {
			
			$this->uploaddir = WWW_ROOT."img/".$destination."/";
			
			if ( !empty($destination) && !is_dir($this->uploaddir) ) {
				mkdir($this->uploaddir,0777,true);
			}
			//$this->imagename = $file['name'];
			$this->imagename = mt_rand().strtotime(date("y-m-d h:i:s")).".".$file_ext;
			if ( move_uploaded_file($file['tmp_name'],$this->uploaddir.$this->imagename) ) {
				 if($old_img) {
					if ( !empty($first) && file_exists($this->uploaddir.$first) ) {					
						@unlink($this->uploaddir.$first);
					}
					if ( !empty($second) && file_exists($this->uploaddir.$second) ) {				
						@unlink($this->uploaddir.$second);					
					}
				}
				$flag = true;
			} 
		} else {			
			$flag = false;
		}
		return $flag; 
	}
	//~ public function dd()
	//~ {
		//~ $data="sdfds";
		//~ return $data;
	//~ }
}
?>
