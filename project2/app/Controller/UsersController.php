<?php
App::uses('AppController', 'Controller');
/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 */
class UsersController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator',"Paypal");
	
	function beforefilter() {
		parent::beforefilter();
		$this->Auth->allow("adminlogin","login","signup","forgot_password","reset_password","confirmlink",'loginwith','logoutwith','sociallogin',"forgot_password_admin","getcategories","moola_merchant_listing");
		
	}

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->User->recursive = 0;
		$this->set('users', $this->Paginator->paginate());
	}
	
	
	function subscribelist() {
		if ( $this->request->is("ajax") ) {
			$this->loadModel("Newsletter");
			$message = "";
			if ($tmp = $this->Newsletter->find("first",array("conditions"=>array("user_id"=>$this->Auth->user("id")),"recursive"=>-1)) ) {
				$this->Newsletter->id = $tmp['Newsletter']['id'];
				$this->Newsletter->delete();
				$message = "Subscribe";
			} else {
				$tmp['Newsletter']['user_id'] = $this->Auth->user("id");
				$tmp['Newsletter']['username'] = $this->Auth->user("username");
				$tmp['Newsletter']['user_type'] = 'M';
				$this->Newsletter->save($tmp);
				$message = "Unsubscribe";
			}
			$result = array("status"=>true,"message"=>$message);
			echo json_encode($result);
			die;
		}
	}

	function deleteuseraccount() {
		if ( $this->request->is("ajax") ) {			
			$message = "";
			if ( $tmp = $this->User->find("first",array("conditions"=>array("User.id"=>$this->Auth->user("id")))) ) {				
				$paypal_profile=$tmp["User"]["payment_profile_id"];
				$this->User->id = $tmp['User']['id'];
				$this->User->delete();
				$result = $this->Paypal->change_subscription_status($paypal_profile,"Cancel");	
				//$result = $this->Paypal->get_subscription_status($paypal_profile);				
				$message = "Deleted";
			}
			else
			{
				$message = "Invalid User";
			}
			$result = array("status"=>true,"message"=>$message);
			echo json_encode($result);
			die;
		}
	}
	function pausepaypalpayment() {
		if ( $this->request->is("ajax") ) {			
			$message = "";			
			if ( $tmp = $this->User->find("first",array("conditions"=>array("User.id"=>$this->Auth->user("id")),"recursive"=>-1)) ) {
				 $paypal_profile=$tmp["User"]["payment_profile_id"];
				 $is_paid=$tmp['User']['is_paid'];
				 (($is_paid==1)?$status = "Suspend":$status = "Reactivate");
				 $result = $this->Paypal->change_subscription_status($paypal_profile,$status);					 	 	 
				 if(isset($result["ACK"]) && $result["ACK"]=="Success")
				 {
					$tmp['User']['user_id'] = $this->Auth->user("id");	
					$tmp['User']['payment_status']=$status;				
					(($is_paid==1)?$tmp['User']['is_paid'] = 0:$tmp['User']['is_paid'] = 1);					
					$this->User->save($tmp,array("validate"=>false));
					$message=$tmp['User']['is_paid'];
				 }
				 else				 
					 $message = "Fail";				
			} 
			else
			{
				$message = "Invalid User";
			}
			$result = array("status"=>true,"message"=>$message);
			echo json_encode($result);
			die;
			
		}
	}
/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}
		$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
		$this->set('user', $this->User->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->User->create();
			if ($this->User->save($this->request->data)) {
				$this->Flash->success(__('The user has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The user could not be saved. Please, try again.'));
			}
		}
		$userTypes = $this->User->UserType->find('list');
		$this->set(compact('userTypes'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->layout = "designmycalendar";				
		$this->jsArray[] = array("main","cropper.min","myaccountvalidate");		
		$this->cssArray[] = array("cropper.min","main");		
		$options = array('conditions' => array('User.id' => $this->Session->read('Auth.User.id')));
		$user = $this->User->find('first', $options);
		if ($this->request->is(array('post', 'put'))) {
			
			//$this->User->UserDetail->set($this->request->data);			
			$this->User->set($this->request->data);	
			//pr($user);die	;  
			//pr($this->request->data);die;
			if (isset($this->request->data['User']['tmpImage']) && !empty($this->request->data['User']['tmpImage'])) {				
				$this->genImage1($this->request->data['User']['tmpImage'],"profile");
				$this->request->data['UserDetail']['image'] = $this->imagename;
				$this->request->data['UserDetail']['fbimage']="";
			} else {
				unset($this->request->data['UserDetail']['image']);
			}
			$this->request->data['UserDetail']['id']=$user['UserDetail']['id']; 
			$this->request->data['User']['is_active']=1;
			//$this->request->data['UserDetail']['user_id'] =$this->Session->read('Auth.User.id');
			if ($this->User->validates()) {	
				if ($this->User->saveAll($this->request->data)) {
					
					$options = array('conditions' => array('User.id' => $this->Session->read('Auth.User.id')));					
					$user = $this->User->find('first', $options);		
					//pr($this->Session->read('Auth.User.UserDetail'));	die;
					 
					if(isset($this->request->data["UserDetail"]["image"]))   
					{   
						if($user["UserDetail"]["image"]!=$this->Session->read('Auth.User.UserDetail.image') || $user["UserDetail"]["image"]!=""  )
						$this->Session->write("Auth.User.UserDetail.image",$this->request->data["UserDetail"]["image"]);
						$this->Session->write("Auth.User.UserDetail.fbimage",'');
					}
						
					if($user["UserDetail"]["business"]!=$this->Session->read('Auth.User.UserDetail.business'))
					$this->Session->write("Auth.User.UserDetail.business",$this->request->data["UserDetail"]["business"]);
					
					$this->Session->write("Auth.User.UserDetail.address",$this->request->data["UserDetail"]["address"]);
					$this->Session->write("Auth.User.UserDetail.latitude",$this->request->data["UserDetail"]["latitude"]);
					$this->Session->write("Auth.User.UserDetail.longitude",$this->request->data["UserDetail"]["longitude"]);
					
					$this->Flash->success(__('Profile updated successfully.'));				
						
				} else {
					$this->Flash->error(__('Profile could not be saved. Please, try again.'));
				}
			}
			return $this->redirect(SITE_LINK.'my_profile');
		} 
		
		$options = array('conditions' => array('User.id' => $this->Session->read('Auth.User.id')));
		$user = $this->User->find('first', $options);
		
		$this->set('user', $user);			
	}

	public function my_account($id = null) {		
		$this->layout = "designmycalendar";						
		$this->jsArray[] = array("main","cropper.min","myaccountvalidate");		
		$this->cssArray[] = array("cropper.min","main");
		$this->User->recursive = 0;		
		$this->User->hasMany = $this->User->belongsTo = $this->User->hasOne = array();
		$this->User->hasOne = array(
			"UserDetail" => array(
				"className" => "UserDetail",
				"foreignKey" => "user_id",
				"type" => "Inner"
			)
		); 
		$options = array('conditions' => array('User.id' => $this->Session->read('Auth.User.id')));
		$user = $this->User->find('first', $options);	
		if ($this->request->is(array('post', 'put'))) {			
			$this->User->UserDetail->set($this->request->data);		
			$this->User->set($this->request->data);
		   
			if ( $this->request->data['User']['username'] == $this->Session->read("Auth.User.username") ) {		
				unset($this->User->validate['username']['isunique']);
					
			}
			$this->request->data['UserDetail']['id']=$user['UserDetail']['id']; 
			if ($this->User->validates()) {
				if ($this->User->saveAll($this->request->data)) {	
					$options = array('conditions' => array('User.id' => $this->Session->read('Auth.User.id')));
					$user = $this->User->find('first', $options);	
					if($user ["User"]["username"]!=$this->Session->read('Auth.User.username'))
					$this->Session->write("Auth.User.username",$this->request->data["User"]["username"]);
					$this->Flash->success(__('Profile updated successfully.'));					
						//return $this->redirect(SITE_LINK.'my-account');
				} else {
					$this->Flash->error(__('Profile could not be saved. Please, try again.'));
				}
				
			} else {
				$this->Flash->error(__('Profile could not be saved. Please, try again.'));
			}
		}
	
			
		$this->set('user', $user);
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->User->id = $id;
		if (!$this->User->exists()) {
			throw new NotFoundException(__('Invalid user'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->User->delete()) {
			$this->Flash->success(__('The user has been deleted.'));
		} else {
			$this->Flash->error(__('The user could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index($user_type = 2,$limit=20) {
		
		$this->layout = "mooladesignadmin";
	    $this->User->recursive = 0;
		$this->User->hasMany = $this->User->belongsTo = $this->User->hasOne = array();
		$url = ($user_type == 2)?"manage_event_manager":"manage_app_user";
		$this->User->hasOne = array(
			"UserDetail" => array(
				"className" => "UserDetail",
				"foreignKey" => "user_id",
				"foreignKey" => "user_id",
				"type" => "Inner"
			)
		); 
		$this->User->belongsTo = array(
			'UserType' => array(
				'className' => 'UserType',
				'foreignKey' => 'user_type_id',
				'conditions' => '',
				'fields' => '',
				'order' => ''
			)
		);
		
		//~ if ($user_type == 2) {
			//~ $this->User->belongsTo['UserCategory'] = array(
				//~ "className"=>"UserCategory",
				//~ "foreignKey"=>false,
				//~ "conditions"=> "User.id = UserCategory.user_id",
			//~ );			
			//~ $this->User->belongsTo['Category'] = array(
				//~ "className"=>"Category",
				//~ "foreignKey"=>false,
				//~ "conditions"=> "UserCategory.category_id = Category.id",
			//~ );			
		//~ }		
		//~ if ($user_type == 3) {
			//~ $this->User->belongsTo['UserUniversity'] = array(
				//~ "className"=>"UserUniversity",
				//~ "foreignKey"=>false,
				//~ "conditions"=> "User.id = UserUniversity.user_id",
			//~ );
			
			//~ $this->User->belongsTo['University'] = array(
				//~ "className"=>"University",
				//~ "foreignKey"=>false,
				//~ "conditions"=> "UserUniversity.university_id = University.id",
			//~ );			
		//~ }	
		
		$this->set('records', array( '20' => '20', '40' => '40','60' => '60', '80' => '80','100' => '100'));
		
		if ( $this->request->is("get") ) {		
			isset($this->request->query["records"])?$limit=$this->request->query["records"]:'';
		}	
		$this->set('limit',$limit);							
		(isset($this->params["named"]["page"]))?$sno=(($this->params["named"]["page"]*$limit)-($limit-1)):$sno=1;		
		$this->bulkactions();
		if ( $this->request->query("searchval") && !empty($this->request->query("searchval")) ) {
			
			$this->set("searchval",$this->request->query("searchval"));
			$searchval = strtolower(trim($this->request->query("searchval")));
			$this->conditions = array("OR"=>array("LOWER(User.username) like"=> "%".$searchval."%","LOWER(UserDetail.name) like"=> "%".$searchval."%"));
		}
		if ( $this->request->is("post") ) {			
			if ( !empty($this->data['User']['searchval']) ) {
				$this->redirect(SITE_LINK.$url."?searchval=".$this->data['User']['searchval']);
			} else {
				$this->redirect(SITE_LINK."".$url);
			}
		}
		$this->set('sno',$sno);
		if($user_type==2)
		{
			$user_type=array();
			$user_type[0]=2;
			$user_type[1]=4;			
			$this->conditions[] = array("User.user_type_id IN"=>$user_type);	
		}
		else
		{
			$this->conditions[] = array("User.user_type_id"=>$user_type);	
		}
			
		$this->paginate = array("order"=>"User.created desc","limit"=>$limit);
 		$this->set('users', $this->Paginator->paginate($this->conditions));
 		$this->set("user_type",$user_type);
		
	}


/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		$this->layout = "mooladesignadmin";	
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}
		$this->User->hasMany = $this->User->belongsTo = $this->User->hasOne = array();
		$this->User->hasOne = array(
			"UserDetail" => array(
				"className" => "UserDetail",
				"foreignKey" => "user_id",
				"type" => "Inner"
			)
		); 
		$this->User->belongsTo = array(
			'UserType' => array(
				'className' => 'UserType',
				'foreignKey' => 'user_type_id',
				'conditions' => '',
				'fields' => '',
				'order' => ''
			)
		);
		
		$opt=array("total_events"=>"Total Events","events_viewed"=>"Events Viewed","events_saved"=>"Events Saved To Calendar");
		$this->set('opt', $opt);
		$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
		$this->set('user', $this->User->find('first', $options));
	}
	
	
	
/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->User->create();
			if ($this->User->save($this->request->data)) {
				$this->Flash->success(__('The user has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The user could not be saved. Please, try again.'));
			}
		}
		$userTypes = $this->User->UserType->find('list');
		$this->set(compact('userTypes'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
			$this->layout = "mooladesignadmin";	
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid user'));
		}
		//$this->jsArray[] = array("myaccountvalidate","jquery.canvasCrop");			
		$this->jsArray[] = array("main","cropper.min","myaccountvalidate");			
		//$this->cssArray[] = array("imgareaselect-default","jquery.awesome-cropper");	
		//$this->cssArray[] = array("imgareaselect-default","croper/style","croper/canvasCrop");
		$this->cssArray[] = array("cropper.min","main");
		$this->User->hasMany = $this->User->belongsTo = $this->User->hasOne = array();
		$this->User->hasOne = array(
			"UserDetail" => array(
				"className" => "UserDetail",
				"foreignKey" => "user_id",
				"type" => "Inner"
			)
		); 
		$this->User->belongsTo = array(
			'UserType' => array(
				'className' => 'UserType',
				'foreignKey' => 'user_type_id',
				'conditions' => '',
				'fields' => '',
				'order' => ''
			)
		);
			
		if ($this->request->is(array('post', 'put'))) {
			echo "sdfds";
			$this->User->UserDetail->set($this->request->data);
			
			$this->User->set($this->request->data);
			if ( $this->request->data['User']['username'] == $this->request->data['User']['tmp_username'] ) {
				unset($this->User->validate['username']['isunique']);
				//die("here");
				
			}
			
			if (isset($this->request->data['User']['tmpImage']) && !empty($this->request->data['User']['tmpImage'])) {				
				$this->genImage1($this->request->data['User']['tmpImage'],"profile");
				$this->request->data['UserDetail']['image'] = $this->imagename;
			} else {
				unset($this->request->data['UserDetail']['image']);
			}
			if ($this->User->validates()) {			
				
				if ($this->User->saveAll($this->request->data,array("validate"=>false))) {
					$url = ($this->request->data['User']['user_type_id'] == 2)?"manage_event_manager":"manage_app_user";
					$this->Flash->success(__('The user has been saved.'));
				
					return $this->redirect(SITE_LINK."".$url);
						
				} else {
					
					$this->Flash->error(__('Profile could not be saved. Please, try again.'));
				}
			} else {
				$this->Flash->error(__('Profile could not be saved. Please, try again.'));
			}
			
		} else {
			$options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
			$this->request->data = $this->User->find('first', $options);
		}
		
		$options = array('conditions' => array('User.id' => $id));
		$user = $this->User->find('first', $options);	
		$this->set('user', $user);
	
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null,$user_type=NULL) {
		$this->User->id = $id;
		$this->loadModel("Calendar");
		$url = ($user_type == 2)?"manage_event_manager":"manage_app_user";
		if (!$this->User->exists()) {
			throw new NotFoundException(__('Invalid user'));
		}
		
		if ($this->User->delete()) {
			
			$this->conditions=array("Calendar.user_id"=>$id);
			$del_cal=$this->Calendar->deleteAll($this->conditions);			
			$this->Flash->success(__('The user has been deleted.'));
		} else {
			$this->Flash->error(__('The user could not be deleted. Please, try again.'));
		}
		return $this->redirect(SITE_LINK."".$url);
		
	}
	
	function getcategories(){
		$this->loadModel("Categories");
		$keyword = ($this->request->query["key"])?(($this->request->query["key"])):"";
		//echo $keyword;
		if ( !empty($keyword) ) {
			$conditions = array("is_active"=>1,"LOWER(title) like '%".strtolower($keyword)."%'");
		} else {
			$conditions = array("is_active"=>1);
		} 
		$categories = $this->Categories->find("list",array("conditions"=>$conditions,"order"=>"LOWER(REPLACE(title,' ','')) asc"));
		//~ $cat=array();
		//~ foreach($categories as $key=>$value)
		//~ {
			//~ $cat[]=$value;
		//~ }
		
		//~ $dbo = $this->Categories->getDatasource();
			//~ $logs = $dbo->getLog();
			//~ $lastLog = end($logs['log']);
			//~ pr($lastLog);
		echo json_encode($categories);
		die;
	}
	
	public function login() {
		//$this->checkRemember();
		//$this->checkLogin();
		$this->autoRender = false;
		$response = array("status"=>false);
		if (!$this->request->is("ajax") ) {
			$this->redirect(SITE_LINK);			
		}
		if ( $this->request->is("post") ) {
			if (true/*$this->verifyRecatpcha($this->request->data)*/)
			 {
				if ( $this->Auth->login() ) {
					$response = array("status"=>true);
					$user = $this->Session->read("Auth");
					if ($user['User']['is_active'] != 1 ) {
						if ( !empty($user['User']['confirmation_token']) ) {
							$response = array("status"=>false,"message"=>"You have not confirmed your account yet.");
						} else {
							$response = array("status"=>false,"message"=>"Your account is deactivated");
						}
					} else {
						//~ if ($this->Session->read("Auth.User.UserDetail.image") == "") {
							//~ //die("here");
							//~ $location = WWW_ROOT."/img/profile/";
							//~ $name = strtotime(date("Y-m-d H:i:s"));
							//~ $text = $this->Session->read("Auth.User.UserDetail.business");
							//~ $this->Image->createImage($text);
							//~ $this->Image->saveAsPng($name,$location);
							//~ $tmp['UserDetail']['id'] = $this->Session->read("Auth.User.UserDetail.id");
							//~ $this->Session->write("Auth.User.UserDetail.image",$name.".png");
							//~ $tmp['UserDetail']['image'] = $name.".png";
							//~ $this->User->UserDetail->save($tmp,array("validate"=>false));
							//~ $tmp = array();
						//~ }	
						$this->getUserDetail();
						if ( isset($this->request->data['User']['remember']) && ($this->request->data['User']['remember']) ) {
							$token = $this->encryptpass(strtotime("Y-m-d h:i:s").$this->request->data['User']['username']);
							$this->Cookie->write("rememberme",$token,false,3600000);
							$tmp['User']['remember_token'] = $token;
							//~ $this->User->create();
							//~ $this->User->id = $user['User']['id'];
							//~ $this->User->saveAll($tmp,array("validate"=>false));
						} 
							
							$this->User->create();
							$this->User->id = $user['User']['id'];
							$tmp['User']['last_login'] = date("Y-m-d h:i:s");
							$tmp['User']['is_active'] = 1;
							$this->User->save($tmp,array("validate"=>false));
						if ($user['User']['user_type_id'] == 1 ) {
							$response = array("status"=>true,"url"=>"dashboard","type"=>$user['User']['user_type_id']);
						} else {
							if (($user['User']['user_type_id'] == 3 ))
							{
								$this->Auth->logout();
								$response = array("status"=>false,"message"=>"You are not authorized to login.");
							}
							else
							{
								$response = array("status"=>true,"url"=>"my_calendar","type"=>$user['User']['user_type_id']);
							}
						}
						
					}
				} else {
					$response = array("status"=>false,"message"=>"Invalid Login Credentials");
				}
			} else {
				$response = array("status"=>false,"message"=>"Invalid captcha.");
			}
		} else {
			$response = array("status"=>false,"message"=>"Invalid Request");
		}
		echo json_encode($response);
	}
	
	public function logout() {
		($this->Session->read('Auth.User.user_type_id')==1)?($url=SITE_LINK."adminlogin"):$url=(SITE_LINK);		
		$this->Auth->logout();
		$this->Session->destroy();
		$this->Cookie->delete("rememberme");		
		$this->redirect($url);
	}
	
	
	public function signup() {
		
		$this->autoRender = false;
		$response = array("status"=>false);
		if ( $this->request->is("post") ) {
			if ( true/*$this->verifyRecatpcha($this->request->data)*/) {
				$this->User->hasMany = $this->User->belongsTo = array();
				
				//$this->loadModel("UserDetail");
				$requestData = $this->request->data;
				
				//$this->UserDetail->set($requestData);
				
				$this->User->set($requestData);
				
				if ( $this->User->validates() && $this->User->UserDetail->validates() ) {
					$requestData['User']['password'] = $this->Auth->password($requestData['User']['password']);
					$requestData['User']['user_type_id'] = 2;	
					$requestData['User']['is_active'] = 1;	
					$requestData['User']['verified'] = 1;
					$requestData['UserDetail']['business'] = $requestData['UserDetail']['name'];	
					//$requestData['User']['confirmation_token'] = $confirmToken = $this->encryptpass($requestData['User']['username']);
					
					//~ if ( $this->Session->read("Auth.UserDetail.image") == ""  ) {
						//~ $location = WWW_ROOT."/img/profile/";
						//~ $name = strtotime(date("Y-m-d H:i:s"));
						//~ 
						//~ $text = $requestData['UserDetail']['name'];
						//~ //$this->Image->createImage($text);
						//~ //$this->Image->saveAsPng($name,$location);
						//~ $requestData['UserDetail']['image'] = $name.".png";
					//~ } 
					
					if ( $this->User->saveAll($requestData,array("validate"=>false))) {
						/*$this->getMailData("SIGNUP_CONFIRMATION");
						$link = SITE_LINK."confirmlink/".$confirmToken;
						$this->mailBody = str_replace("{User}",($requestData['UserDetail']['name']),$this->mailBody);
						$this->mailBody = str_replace("{CLICK_HERE}","<a href='".$link."'>Click Here</a>",$this->mailBody);
						$this->mailBody = str_replace("{LINK}",$link,$this->mailBody);
						$this->sendMail($requestData['User']['username']);*/												
										
						if ($this->Auth->login($this->request->data['User']) ) {
							$user = $this->User->find("first",array("conditions"=>array("username"=>$requestData['User']['username']),"recursive"=>0,'is_active'=>1));					
							$this->Session->write("Auth",$user);	
							$response = array("status"=>true,"url"=>"my_calendar");							
						}
						else{
							$response = array("status"=>false,"message"=>"Invalid Data.Signup is not successfull, please try again.");
						}
					} else {
						$this->Session->destroy();
						$response = array("status"=>false,"message"=>"Signup is not successfull, please try again.");					
					}
				}
				else {
						$this->Session->destroy();
						$response = array("status"=>false,"message"=>"User with this email id already exits.");					
				}
			} else {
				$response = array("status"=>false,"message"=>"Invalid captcha.");					
			}
		} 
		else {
			$response = array("status"=>false,"message"=>"Invalid Request");
		}
		echo json_encode($response);
	}
	
	public function confirmlink($token = NULL) {
		$conditions = array("User.confirmation_token"=>$token,"OR"=>array("User.is_active"=>0,"User.is_active is NULL"));
		$users = $this->User->find("first",array("conditions"=>$conditions,"recursive"=>-1));
		
		if ( !empty($users) ) {
			$users['User']['confirmation_token'] = '';
			$users['User']['is_active'] = 1;
			$users['User']['verified'] = 1;
			$this->User->create();
			$this->User->id = $users['User']["id"];			
			if ($this->User->save($users,array("validate"=>false))){
				//$this->Flash->reset(__('Your account has been confirmed successfully.'));
				$this->redirect(SITE_LINK."account_confirmation");
			} else {
				$this->Flash->error(__('Your account has not been confirmed.'));
				//$this->Session->setFlash(__("Your account has not been confirmed successfully."));
			}

		} else{
			$this->Flash->error(__('Invalid Link'));
			//$this->Session->setFlash(__("Invalid Link"));
		}
		$this->redirect("/login");
		//$this->render(false);
	}
	
	
    
    	/*
	 * @function name	: changepassword
	 * @purpose			: display form of change password and also performs password change functionlity
	 * @arguments		: none
	 * @return			: none
	 * @description		: NA
	*/
	public function admin_changepassword() {	
		$this->layout = "mooladesignadmin";	
					
            $this->__processChangepassword();
	}
	public function changepassword() {		
		$this->layout = "mooladesignmyvoucher";		
		$this->jsArray[]=array("change_password");
        $this->__processChangepassword();
	}
	/* end of function */
	function __processChangepassword() {
		//$this->userInfo();
		if (!empty($this->data)) {
			$data = $this->data;
			$data['User']['encOldPassword'] = $this->Auth->password($data['User']['currentpassword']);
			$this->User->set($data);
			if ( $this->User->validates() ) {
				$password = $this->Auth->password($this->data['User']['currentpassword']);
				$user = $this->User->find("first",array("conditions"=>array("User.id"=>$this->Session->read('Auth.User.id'),'User.password'=>$password),'recursive'=>-1));				
				$new_pass =$this->Auth->password($this->data['User']['newpassword']);	
				if (empty($user)) {
					$this->Session->setFlash('Current password is not correct.');	
				}
				elseif(empty($this->data['User']['newpassword']) || empty($this->data['User']['confirmpassword'])) {
					$this->Session->setFlash('New and confirm password do not match.');
				} elseif($password == $new_pass){
					$this->Session->setFlash('New password can not be same as current password.');
				} elseif($this->data['User']['newpassword'] != $this->data['User']['confirmpassword']) {
					$this->Session->setFlash('New and confirm password do not match.', 'default');
				} else {				
					$data['User']['password'] =  $new_pass;
					$data['User']['id'] = $this->Session->read("Auth.User.id");
					$tmpData['id'] = $this->Session->read("Auth.User.id");
					$tmpData['password'] =  $new_pass;
					$this->User->create();
					$this->User->id = 1;	
					$this->User->saveAll($tmpData,array("validate"=>false));
					$this->Session->setFlash('Password has been updated successfully.', 'default',array("class"=>"success_message"));			
				}
			}
		}
	}
	
    public function forgot_password() {
		
		$this->autoRender = false;
		$response = array("status"=>false);
		if ( $this->request->is("post") ) {
			if (true/*$this->verifyRecatpcha($this->request->data)*/) {
					$users = $this->User->find("first",array("conditions"=>array("User.username"=>$this->request->data['User']['email'],"User.is_active"=>1),"recursive"=>0));
					if ( !empty($users) ) {
						$forgotToken  = $this->encryptpass($users['User']['username']);
						$users['User']['password_token'] = $forgotToken;
						if ( $this->User->save($users,array("validate"=>false)) ) {
							$this->getMailData("FORGOT_PASSWORD");
							$link = SITE_LINK."reset_password/".$forgotToken;
							$this->mailBody = str_replace("{username}",$users['UserDetail']['business'],$this->mailBody);
							$this->mailBody = str_replace("{CLICK_HERE}",$link,$this->mailBody);
							$this->mailBody = str_replace("{support_mail}","support@rave.com",$this->mailBody);
						 	$this->mailBody = str_replace("{URL}",SITE_LINK,$this->mailBody);
							$this->sendMail($users['User']['username']);
							if($users['User']['user_type_id']!=1)
							$this->Flash->reset(__($this->request->data['User']['email']));							
							//$this->Session->setFlash(__("Your request to reset password is submitted, please check your email."), 'default', array("class"=>"success_message"));
							$response = array("status"=>true,"message"=>"Your request to reset password is submitted, please check your email.");
							
						} else {
							$response = array("status"=>false,"message"=>"Your request could not be processed, please try again.");						
						} 
					} else {
						$response = array("status"=>false,"message"=>"Invalid email for forgot password request.");				
					}
				}
				else {
				  	$response = array("status"=>false,"message"=>"Invalid Captcha");
			}
		}
		else {
			$response = array("status"=>false,"message"=>"Invalid Request");
		}
		echo json_encode($response);
	}
	
	public function reset_password($token = null){
		$this->autoRender = false;
		$jsArray[]=array("resetPasswordvalidate");
		$users = $this->User->find("first",array("conditions"=>array("User.password_token"=>$this->request->data['User']['token'],"User.is_active"=>1),"recursive"=>-1));
		//pr($users);
		
		$response = array("status"=>false);
		if ( !empty($users) ) {
			if ( $this->request->is("post") ) {				
				$tempData["User"]["password"] = $this->request->data['User']['newpassword'];
				$tempData["User"]["confirm_password"] = $this->request->data['User']['confirm_password'];
				$this->User->set($tempData);
		
				if ( $this->User->validates() ) {
					$tmp['User']['password'] = $this->Auth->password($this->request->data['User']['newpassword']);
					$tmp['User']['password_token'] = '';
					$this->User->create();
					$this->User->id = $users['User']['id'];
					if ( $this->User->save($tmp,array("validate"=>false)) ) {
						$this->Flash->reset(__("Your password has been reset successfully."));	
						//$this->Session->setFlash(__("Your password has been reset successfully."), 'default', array("class"=>"success_message"));
						$response = array("status"=>true,"message"=>"Your password has been reset successfully.");
					} else {						
						$response = array("status"=>false,"message"=>"Your password can not be reset, please try again later.");
					}
					//$this->redirect("/login");
				} else {
					$response = array("status"=>false,"message"=>"Request not processed, please try again.");							
				}				
			}
			else {
			$response = array("status"=>false,"message"=>"Invalid Request");
			}
		} else {
			$response = array("status"=>false,"message"=>"Invalid Request");
		}
		echo json_encode($response);		
	}
	
	
	/**
 * admin_index method
 *
 * @return void
 */
	public function merchant() {
		
		$this->layout = "mooladesignmyvoucher";
		$this->User->recursive = 0;
		$this->set('users', $this->Paginator->paginate());
	}
	
	public function adminlogin() {
		/*if ( $_SERVER['SERVER_ADDR'] == '127.0.0.1' || $_SERVER['SERVER_ADDR'] == '::1'  ) {
			$user = $this->User->find("first",array("conditions"=>array("user_type_id"=>1)));
			//pr($user);
			if ( $this->Auth->login($user['User']) ) {
				//pr($this->Session->read("Auth"));
				//die("here");
				$this->redirect("/dashboard");
			}
		}*/
		if(!empty($this->Session->read('Auth.User.id')))
		{
			return $this->redirect(SITE_LINK.'dashboard');
		}
		$this->layout = "admin";
		$this->render("login");
	}
	
	public function forgot_password_admin() {
		$this->layout = "admin";		
	}
	public function admin_profile() {
		$this->layout = "mooladesignadmin";
		$this->jsArray[] = array("jquery.awesome-cropper-profile","jquery.imgareaselect");			
		$this->cssArray[] = array("imgareaselect-default","jquery.awesome-cropper");	
		$this->User->recursive = 0;
		if (!$this->User->exists($this->Session->read('Auth.User.id'))) {
			throw new NotFoundException(__('Invalid user'));
		}
		if ($this->request->is(array('post', 'put'))) {
			$this->User->hasMany = $this->User->belongsTo = $this->User->hasOne = array();
			$this->User->belongsTo = array(
				"UserDetail" => array(
					"className" => "UserDetail",
					"foreignKey" => "user_id",
					"type" => "Inner"
				) 
			);
			if ( $this->request->data['User']['username'] == $this->Session->read("Auth.User.username") ) {
				unset($this->User->validate['username']['isunique']);				
				
			}
			if (isset($this->request->data['User']['tmpImage']) && !empty($this->request->data['User']['tmpImage'])) {				
				$this->genImage1($this->request->data['User']['tmpImage'],"profile");
				$this->request->data['UserDetail']['image'] = $this->imagename;
			} else {
				unset($this->request->data['UserDetail']['image']);
			}
			
			$this->User->UserDetail->set($this->request->data);
			$this->User->set($this->request->data);
			if ($this->User->validates()) {			
				if ($this->User->saveAll($this->request->data)) {
					if (isset($this->request->data['UserDetail']['image'])) {
						$this->Session->write("Auth.User.UserDetail.image",$this->request->data['UserDetail']['image']);				   
					}
					$this->Flash->success(__('The user has been saved.'));
					return $this->redirect(array('action' => 'admin_profile'));
				} else {
					$this->Flash->error(__('The user could not be saved. Please, try again.'));
				}
			} else {
				$this->Flash->error(__('The user could not be saved. Please, try again.'));
			}
		}else {
			$options = array('conditions' => array('User.' . $this->User->primaryKey => $this->Session->read('Auth.User.id')));
			$this->request->data = $this->User->find('first', $options);
			
		}		
	}
	
	
	function admin_dashboard($days_to_fetch=30) {		
		
			
			$this->layout = "mooladesignadmin";	
			$this->jsArray = "admin_dashboard";
			$this->loadModel('Billing');
			$this->loadModel('Calendar');	
			$series1=$series2=$days_fetch_conimp= $days_fetch_voucher=$days_fetch_con=$days_fetch_imp="";			
			$startDate=date("Y")."-01-01";			
			$todayDate=date("Y-m-d");			
			$datafromtotill="Jan-".date("M")."'".date("Y");
			$days_fetch_voucher=$days_to_fetch;
			$days_fetch_imp=$days_to_fetch;
			$days_fetch_con=$days_to_fetch;
			$this->set('datafromtotill',$datafromtotill);
			$this->set('records', array('30' => '30 Days', '60' => '60 Days','90' => '90 Days'));
			//$this->set('compareImpCon', array( 'impcom' => 'Impressions Vs Conversions', 'reachimp' => 'Reach Vs Impressions'));
			
			//Reach Conversions
			$series1="Saved Events";
			$series2="Viewed Events";
			//Chart starts from that date
			$days_fetch_conimp= $days_to_fetch;
			$startDate_reachimp=date("Y,m, d",strtotime($todayDate." -".($days_to_fetch+30)." days"));	
			$final_reachimp_Arr=$this->totalSavedViewdEvents($days_fetch_conimp,"calendar");
			$final_conimp_Arr=$this->totalSavedViewdEvents($days_fetch_conimp,"viewed");					
			//Reach Conversions	
			
			//total Voucher
			$startDate_voucher= date("Y,m, d",strtotime($todayDate." -".($days_to_fetch+30)." days"));	
			
			//total Conversion
			$startDate_con= date("Y,m, d",strtotime($todayDate." -".($days_to_fetch+30)." days"));
			
			//total Impression
			$startDate_imp= date("Y,m, d",strtotime($todayDate." -".($days_to_fetch+30)." days"));	
			
			
			
			if ( $this->request->is("post") ) {
			   //isset($this->request->query["records"])?$limit=$this->request->query["records"]:'';			  
				//Reach Conversions
			   if(isset($this->data["compareConversions"]))
				{
					$days_fetch_conimp=$this->data["compareImpConVal"];
					
						$final_reachimp_Arr=$this->totalSavedViewdEvents($days_fetch_conimp,"calendar");
						$final_conimp_Arr=$this->totalSavedViewdEvents($days_fetch_conimp,"viewed");
						$series1="Saved Events";$series2="Viewed Events";
					
					//Chart starts from that date	
					$startDate_reachimp= date("Y,m, d",strtotime($todayDate." -".($days_fetch_conimp+30)." days"));					
									
					//drop Compare Dropdown Reach Conversion 	
					//$compareImpCon_dropdown_select=$this->data["compareImpCon"];
								
				}
				if(isset($this->data["totalvoucher"]))
				{
					
					if(isset($this->data["total_voucherval"]) && !empty($this->data["total_voucherval"])){
						$days_to_fetch=$this->data["total_voucherval"];
						$days_fetch_voucher=$this->data["total_voucherval"];
						$startDate_voucher= date("Y,m, d",strtotime($todayDate." -".($days_to_fetch+30)." days"));	
					}	
				}
				if(isset($this->data["totalconversions"]))
				{
					
					if(isset($this->data["total_conversionsval"]) && !empty($this->data["total_conversionsval"])){
						$days_to_fetch=$this->data["total_conversionsval"];
						$days_fetch_con=$this->data["total_conversionsval"];
						$startDate_con= date("Y,m, d",strtotime($todayDate." -".($days_to_fetch+30)." days"));	
					}
				}
				if(isset($this->data["totalimpressions"]))
				{
					
					if(isset($this->data["total_impressionsval"]) && !empty($this->data["total_impressionsval"])){
						$days_to_fetch=$this->data["total_impressionsval"];
						$days_fetch_imp=$this->data["total_impressionsval"];
						$startDate_imp= date("Y,m, d",strtotime($todayDate." -".($days_to_fetch+30)." days"));	
					}
				}
				
			}			
					
			//Reach Conversions set all values	
			//drop Compare Dropdown Reach Conversion  and days
			//$this->set('compareImpCon_dropdown_select',$compareImpCon_dropdown_select);
			$this->set('days_fetch_conimp',$days_fetch_conimp);	
			
			$this->set('series1',$series1);
			$this->set('series2',$series2);	
			$this->set('startDate_reachimp',$startDate_reachimp);				
			$this->set('final_reachimp_Arr',$final_reachimp_Arr);
			$this->set('final_conimp_Arr',$final_conimp_Arr);
		
			//Reach Conversions	
			
			//first line of dashboard
			$options= array('fields' => array('sum(Plan.price)   AS Price'),'conditions' => array('User.user_type_id'=>2,'Billing.bill_start_date between ? and ?' => array($startDate, $todayDate),'Billing.bill_status !='=>"SUSPENDED"));			
			$this->set('total_revenue', $this->Billing->find('all', $options));
			
			$options= array('conditions' => array('User.user_type_id'=>3));
			$this->set('total_students', $this->User->find('count', $options));
			
			$options= array('conditions' => array('User.user_type_id'=>2));
			$this->set('total_merchants', $this->User->find('count', $options));
			
			$events = $this->Calendar->find("count");	
			$this->set('total_vouchers', $events);
			//first line of dashboard
			
			//chart 1
			$this->totalEvents($days_fetch_voucher);	
			$this->set('final_conimp_Arr',$final_conimp_Arr);
			$this->set('days_fetch_voucher',$days_fetch_voucher);
			$this->set('startDate_voucher',$startDate_voucher);
			
			//chart 2
			$final_total_con_Arr=$this->totalSavedViewdEvents($days_fetch_con,"calendar");
			$this->set('final_total_con_Arr', $final_total_con_Arr);
			$this->set('days_fetch_con',$days_fetch_con);
			$this->set('startDate_con',$startDate_con);
			//chart 3
			$final_total_imp_Arr=$this->totalSavedViewdEvents($days_fetch_imp,"viewed");	
			$this->set('final_total_imp_Arr', $final_total_imp_Arr);	
			$this->set('days_fetch_imp',$days_fetch_imp);
			$this->set('startDate_imp',$startDate_imp);
			
			//chart date
			$startDate_chart= date("Y,m, d",strtotime($todayDate." -".($days_to_fetch*2)." days"));
			$this->set('startDate', $startDate_chart);
			
			
			//sales
			$options=array("order"=>"Billing.renew_date asc","limit"=>19);			
			$this->set('merchant_sale', $this->Billing->find('all', $options));
			
			
	}	
	public function totalEvents($days)
	{
			$conditions=array("Calendar.created >=(CURDATE() - INTERVAL ".$days." Day)");
			$this->Calendar->virtualFields = array("startDate"=>" (CURDATE())");
			
			$total_voucher_chart = $this->Calendar->find("all",array("conditions"=>$conditions,"recursive"=>-1,"fields"=>array("count(Calendar.id) as cnt","date(Calendar.created) as date",'startDate'),"group"=>"date(Calendar.created)","order"=>"Calendar.created asc"));			
			
			$startDate = (isset($total_voucher_chart[0]['Voucher']['startDate']))?$total_voucher_chart[0]['Voucher']['startDate']:'';
			$startDate= date("Y-m-d",strtotime($startDate." -".$days." days"));			
			
			foreach($total_voucher_chart as $key=>$val){	
				$dayArr[$val[0]['date']] = $val[0]['cnt'];		
			}			
			$final_total_chart_Arr = array();
			for ( $i = 0; $i<=$days; $i++ ) {
				$tmpDate = date("Y-m-d",strtotime($startDate." +".$i." days"));						
				$final_total_vou_Arr[$tmpDate] = isset($dayArr[$tmpDate])?$dayArr[$tmpDate]:0;
			}
			$this->set('final_total_vou_Arr', $final_total_vou_Arr);
			
	}
	public function totalSavedViewdEvents($days,$stat)
	{		
			$this->loadModel("SaveCalendar");	
			$total_conversion_chart="";
			$conditions=array("type"=>$stat,"SaveCalendar.created >=(CURDATE() - INTERVAL ".$days." Day)");
			$this->SaveCalendar->virtualFields = array("startDate"=>" (CURDATE())");
			
			$total_conversion_chart = $this->SaveCalendar->find("all",array("conditions"=>$conditions,"fields"=>array("count(SaveCalendar.id) as cnt","date(SaveCalendar.created) as date",'startDate'),"group"=>"date(SaveCalendar.created)","order"=>"SaveCalendar.created asc"));			
			
			$startDate = (isset($total_voucher_chart[0]['SaveCalendar']['startDate']))?$total_voucher_chart[0]['SaveCalendar']['startDate']:'';
			$startDate= date("Y-m-d",strtotime($startDate." -".$days." days"));			
			
			foreach($total_conversion_chart as $key=>$val){	
				$dayArr[$val[0]['date']] = $val[0]['cnt'];		
			}			
			$final_total_chart_Arr = array();
			for ( $i = 0; $i<=$days; $i++ ) {
				$tmpDate = date("Y-m-d",strtotime($startDate." +".$i." days"));						
				$final_total_con_Arr[$tmpDate] = isset($dayArr[$tmpDate])?$dayArr[$tmpDate]:0;
			}
			return $final_total_con_Arr;
				
	}
	//Facebook login
	public function sociallogin() {
		Configure::write('debug', 2);
		$this->autoRender = false;
		$result = array("status"=>false,"message"=>"Invalid request.");
		if ( $this->request->is("post") ) {
			$result = array("status"=>true);
			$this->loadModel("User");
			$identifier = isset($this->request->data['id'])?$this->request->data['id']:"";
			$name = isset($this->request->data['name'])?$this->request->data['name']:"";
			if ( empty($identifier) ) {
				$result = array("status"=>false,"message"=>"not logged in.");
			} elseif ( $tmpUser = $this->User->find("first",array("conditions"=>array("identifier"=>$identifier),"recursive"=>0)) ) {
				if ( $tmpUser['User']['is_active'] == 1 ) {
					$this->request->data['User']['identifier'] = $identifier;
					$this->components['Auth']['authenticate']['Form']['fields'] = array("identifier"=>"identifier");
					if ( $this->Auth->login($this->request->data['User']) ) {
						$this->Session->write("Auth",$tmpUser);
					} 
				} else {
					$result = array("status"=>false,"message"=>"Your account has been deactivate.");
				}
			} else {
				$tmpUser['User']['identifier'] = $identifier;
				$tmpUser['User']['user_type_id'] = 2;
				$tmpUser['User']['is_active'] = 1;
				$tmpUser['UserDetail']['business'] = isset($this->request->data['name'])?$this->request->data['name']:'';
				$tmpUser['User']['username'] = isset($this->request->data['email'])?$this->request->data['email']:'';
				$tmpUser['UserDetail']['fbimage'] = "http://graph.facebook.com/".$identifier."/picture?type=large";
				if ( $this->User->saveAll($tmpUser,array("validate"=>false)) ) {
					$this->request->data['User']['identifier'] = $identifier;
					$this->components['Auth']['authenticate']['Form']['fields'] = array("identifier"=>"identifier");
					$tmpUser = $this->User->find("first",array("conditions"=>array("identifier"=>$identifier),"recursive"=>0));					
					if ( $this->Auth->login($this->request->data['User']) ) {
						$this->Session->write("Auth",$tmpUser);						
					}
				} else {
					$this->Auth->logout();
					$result = array("status"=>false,"message"=>"Your account has been deactivate.");
				}
			}
		}
		echo json_encode($result);
	}
	
	public function moola_merchant_listing($limit=16)
	{		
		$this->layout = "moolahomedesign";		
		$this->set("title_for_layout","Merchants | Rave");
		$this->set("keywords","Merchants Listing, Rave");
		$this->loadModel("Category");
		$this->User->recursive = 0;		
		$this->User->hasMany = $this->User->belongsTo = $this->User->hasOne = array();
		
		$this->User->belongsTo = array(
			"UserDetail" => array(
				"className" => "UserDetail",
				'foreignKey' => false,			
				"type" => "Inner",
				"conditions"=> "User.id = UserDetail.user_id",
			),
			'UserCategory' => array(
				'className' => 'UserCategory',
				'foreignKey' => false,
				"conditions"=> "User.id = UserCategory.user_id",		
			),
			'Category' => array(
				'className' => 'Category',
				'foreignKey' => false,
				"conditions"=> "UserCategory.category_id = Category.id"		
			)
		);
		
		if(isset($this->request->query['Clear']))
		{
			$this->redirect(SITE_LINK."merchants");
		}
		if ( !empty($this->request->query['search']) || !empty($this->request->query['category']) || !empty($this->request->query['lat'])) {

			
		 	$searchval = strtolower(trim($this->request->query['search']));
			$category = strtolower(trim($this->request->query['category']));
			//$address = strtolower(trim($this->request->query['address']));
			$lat = strtolower(trim($this->request->query['lat']));
			$lng = strtolower(trim($this->request->query['lng']));
			//$this->conditions = array("OR"=>array("LOWER(UserDetail.business) like"=> "%".$searchval."%","LOWER(Category.title) like"=> "%".$category."%","LOWER(UserDetail.longitude) like"=> "%".$lng."%","LOWER(UserDetail.latitude) like"=> "%".$lat."%"));
			$this->conditions = array("OR"=>array("LOWER(UserDetail.business) like"=> "%".$searchval."%"));
			$this->conditions[] = array("OR"=>array("LOWER(Category.title) like"=> "%".$category."%"));
			$this->conditions[] = array("OR"=>array("LOWER(UserDetail.longitude ) like"=> "%".$lng."%"));
			$this->conditions[] = array("OR"=>array("LOWER(UserDetail.latitude  ) like"=> "%".$lat."%"));
			//pr($this->conditions);die;
		}

		$this->conditions[] = array("User.user_type_id"=>2);
		if( $this->request->is("ajax")) {
			$this->layout = false;
			$this->render = false;
			$this->paginate = array("order"=>"User.created desc","limit"=>"16","fields"=>array("User.id","UserDetail.image","UserDetail.business","UserDetail.address","Category.title"));
			$users = $this->Paginator->paginate($this->conditions);	
			$tmpData = array();			
			foreach( $users as $key=>$val  ) {
				$tmpData[]= array("id"=>$val['User']['id'],"image" => $val['UserDetail']['image'],"address" => $val['UserDetail']['address'],"business" => $val['UserDetail']['business'],"category" => $val['Category']['title']);			
			}
			echo json_encode($tmpData);	
			die;		
		}
		else{			
			$this->paginate = array("order"=>"User.created desc","limit"=>$limit);
			$users = $this->Paginator->paginate($this->conditions);	
			$this->set('users', $users);
			$categories = $this->Category->find("list",array("conditions"=>array("is_active"=>1)));	
			$this->set("categories",$categories);
		}
	}

}
