<?php
App::uses('AppController', 'Controller');
/**
 * CmsPages Controller
 *
 * @property CmsPage $CmsPage
 * @property PaginatorComponent $Paginator
 */
class CmsPagesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->CmsPage->recursive = 0;
		$this->set('cmsPages', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->CmsPage->exists($id)) {
			throw new NotFoundException(__('Invalid cms page'));
		}
		$options = array('conditions' => array('CmsPage.' . $this->CmsPage->primaryKey => $id));
		$this->set('cmsPage', $this->CmsPage->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->CmsPage->create();
			if ($this->CmsPage->save($this->request->data)) {
				$this->Flash->success(__('The cms page has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The cms page could not be saved. Please, try again.'));
			}
		}		
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->CmsPage->exists($id)) {
			throw new NotFoundException(__('Invalid cms page'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->CmsPage->save($this->request->data)) {
				$this->Flash->success(__('The cms page has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The cms page could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('CmsPage.' . $this->CmsPage->primaryKey => $id));
			$this->request->data = $this->CmsPage->find('first', $options);
		}
		
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->CmsPage->id = $id;
		if (!$this->CmsPage->exists()) {
			throw new NotFoundException(__('Invalid cms page'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->CmsPage->delete()) {
			$this->Flash->success(__('The cms page has been deleted.'));
		} else {
			$this->Flash->error(__('The cms page could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index($searchval = NULL,$limit=20) {
		 $this->layout = "mooladesignadmin";
		$this->bulkactions();
		
		$this->set('records', array( '20' => '20', '30' => '30','40' => '40', '60' => '60','80' => '80','100' => '100'));
		
		if ( $this->request->is("get") ) {		
			isset($this->request->query["records"])?$limit=$this->request->query["records"]:'';
			
			/*if ( !empty($this->data['User']['searchval']) ) {
				$this->redirect(SITE_LINK.$url."?searchval=".$this->data['User']['searchval']);
			} else {
				$this->redirect(SITE_LINK."".$url);
			}*/
		}	
		$this->set('limit',$limit);	
		(isset($this->params["named"]["page"]))?$sno=(($this->params["named"]["page"]*$limit)-($limit-1)):$sno=1;
		
		if ( !empty($searchval) ) {
			$this->set("searchval",$searchval);
			$this->conditions = array("OR"=>array("CmsPage.header like"=> "%".$searchval."%"));
		}
		if ( $this->request->is("post") ) {
			if ( !empty($this->data['CmsPage']['searchval']) ) {
				$this->redirect(SITE_LINK."CmsPages/".$this->data['CmsPage']['searchval']);
			} else {
				$this->redirect(SITE_LINK."CmsPages/");
			}
		}
		$this->set('sno',$sno);
		$this->paginate = array("limit"=>$limit);
		$this->CmsPage->recursive = 0;
		$this->set('cmsPages', $this->Paginator->paginate($this->conditions));
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		 $this->layout = "mooladesignadmin";
		if (!$this->CmsPage->exists($id)) {
			throw new NotFoundException(__('Invalid cms page'));
		}
		$options = array('conditions' => array('CmsPage.' . $this->CmsPage->primaryKey => $id));
		$this->set('cmsPage', $this->CmsPage->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		 $this->layout = "mooladesignadmin";
		if ($this->request->is('post')) {
			$this->CmsPage->create();
			if ($this->CmsPage->save($this->request->data)) {
				$this->Flash->success(__('The cms page has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The cms page could not be saved. Please, try again.'));
			}
		}
		$languages = $this->CmsPage->find('list');
		
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		 $this->layout = "mooladesignadmin";
		if (!$this->CmsPage->exists($id)) {
			throw new NotFoundException(__('Invalid cms page'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->CmsPage->save($this->request->data)) {
				$this->Flash->success(__('The cms page has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The cms page could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('CmsPage.' . $this->CmsPage->primaryKey => $id));
			$this->request->data = $this->CmsPage->find('first', $options);
		}
		
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->CmsPage->id = $id;
		if (!$this->CmsPage->exists()) {
			throw new NotFoundException(__('Invalid cms page'));
		}
		//$this->request->allowMethod('post', 'delete');
		if ($this->CmsPage->delete()) {
			$this->Flash->success(__('The cms page has been deleted.'));
		} else {
			$this->Flash->error(__('The cms page could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
