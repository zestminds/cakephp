<?php
App::uses('AppController', 'Controller');
/**
 * Plans Controller
 *
 * @property Plan $Plan
 * @property PaginatorComponent $Paginator
 */
class PlansController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator',"Paypal");

/**
 * index method
 *
 * @return void
 */
	public function index() {
		
		$this->layout = "mooladesignmyvoucher";		
		$plan = $this->Plan->find("all",array("conditions"=>array("is_active"=>1)));
		$this->set("plans",$plan);
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Plan->exists($id)) {
			throw new NotFoundException(__('Invalid plan'));
		}
		$options = array('conditions' => array('Plan.' . $this->Plan->primaryKey => $id));
		$this->set('plan', $this->Plan->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Plan->create();
			if ($this->Plan->save($this->request->data)) {
				$this->Flash->success(__('The plan has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The plan could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->layout = "mooladesignadmin";
		if (!$this->Plan->exists($id)) {
			throw new NotFoundException(__('Invalid plan'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Plan->save($this->request->data)) {
				$this->Flash->success(__('The plan has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The plan could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Plan.' . $this->Plan->primaryKey => $id));
			$this->request->data = $this->Plan->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Plan->id = $id;
		if (!$this->Plan->exists()) {
			throw new NotFoundException(__('Invalid plan'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Plan->delete()) {
			$this->Flash->success(__('The plan has been deleted.'));
		} else {
			$this->Flash->error(__('The plan could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->layout = "mooladesignadmin";
		$this->Plan->recursive = 0;
		$this->set('plans', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		
		if (!$this->Plan->exists($id)) {
			throw new NotFoundException(__('Invalid plan'));
		}
		$options = array('conditions' => array('Plan.' . $this->Plan->primaryKey => $id));
		$this->set('plan', $this->Plan->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->Plan->create();
			if ($this->Plan->save($this->request->data)) {
				$this->Flash->success(__('The plan has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The plan could not be saved. Please, try again.'));
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		$this->layout = "mooladesignadmin";
		if (!$this->Plan->exists($id)) {
			throw new NotFoundException(__('Invalid plan'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Plan->save($this->request->data)) {
				$this->Flash->success(__('The plan has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The plan could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Plan.' . $this->Plan->primaryKey => $id));
			$this->request->data = $this->Plan->find('first', $options);
		}
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Plan->id = $id;
		if (!$this->Plan->exists()) {
			throw new NotFoundException(__('Invalid plan'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Plan->delete()) {
			$this->Flash->success(__('The plan has been deleted.'));
		} else {
			$this->Flash->error(__('The plan could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
	
	
	function buyplan($id = NULL) {
		$this->layout=false;
		$this->render(false);
		//~ if ( !empty($id) ) {
			$plan = $this->Plan->find("first",array("conditions"=>array("is_active"=>1),"recursive"=>-1));
			$id = $plan['Plan']['id'];
			$price = $plan['Plan']['price'];
			//$vat = ceil(($price * 20)/100);
			//$price = $price+$vat;
			$this->Paypal->cancelurl = SITE_LINK."my-account";
			$this->Paypal->returnurl = SITE_LINK."create-profile/".$id."/";
			$this->Paypal->initializePayment($price);
			if ( $this->Paypal->status ) {
				$this->redirect($this->Paypal->redirectURL);
			}
		//~ }
	}
	
	function createprofile($id = NULL) {
		$plan = $this->Plan->find("first",array("conditions"=>array("id"=>$id,"is_active"=>1),"recursive"=>-1));
		$price = $plan['Plan']['price'];
		$result = $this->Paypal->createrecurringProfile($price,$this->request->query['token'],$this->request->query['PayerID']);
		/*	$result = Array
		(
			"PROFILEID"=>  "I-5JE08MXJ32W1",
			"PROFILESTATUS"=>  "ActiveProfile",
			"TIMESTAMP"=>  "2018-08-04T07:34:50Z",
			"CORRELATIONID"=>  "cebecfaf3608",
			"ACK"=>  "Success",
			"VERSION"=>  "108",
			"BUILD"=>  "46457558"
		);*/
		if ( $result['PROFILESTATUS'] == 'ActiveProfile' && $result['ACK'] == 'Success' ) {
			$tmpUser['User']['is_paid'] = 1;
			$tmpUser['User']['payment_profile_id'] = $result['PROFILEID'];
			$this->loadModel("User");
			$this->User->id = $this->Auth->user("id");
			$this->User->save($tmpUser);			
			$this->loadModel("Billing");
			$tmpBilling['Billing']['user_id'] = $this->Auth->user("id");
			$tmpBilling['Billing']['plan_id'] = $id;
			$tmpBilling['Billing']['bill_start_date'] = date("Y-m-d",strtotime($result['TIMESTAMP']));
			$tmpBilling['Billing']['bill_end_date'] = date("Y-m-d",strtotime($result['TIMESTAMP']." +1 MONTH"));
			$tmpBilling['Billing']['renew_date'] = date("Y-m-d",strtotime($result['TIMESTAMP']));
			$tmpBilling['Billing']['due_date'] = date("Y-m-d",strtotime($result['TIMESTAMP']." +1 MONTH"));
			$tmpBilling['Billing']['payment_start_on'] = ($result['TIMESTAMP']);
			$tmpBilling['Billing']['bill_status'] = ($result['PROFILESTATUS']);
			$tmpBilling['Billing']['bill_status'] = ($result['PROFILESTATUS']);
			$tmpBilling['Billing']['paypal_token'] = ($result['CORRELATIONID']);
			$tmpBilling['Billing']['paypal_profile'] = ($result['PROFILEID']);
			$this->Billing->save($tmpBilling);
			
			//send email						
			$users = $this->User->find("first",array("conditions"=>array("User.id"=>$this->Auth->user("id")),'fields' => array("UserDetail.business  AS accountname","User.username  AS accountemail")));
			$cmsemail=$this->getMailData("BILLING_RECEIPT");			
			$this->mailBody = str_replace("{username}",$users["UserDetail"]["accountname"] ,$this->mailBody);			
			$this->mailBody = str_replace("{amount}",$price ,$this->mailBody);
			$billdate=date("M d, Y",strtotime($result['TIMESTAMP']))." at <br/>".date("h:i A",strtotime($result['TIMESTAMP']));
			$nextbiilingdate=date("M d, Y",strtotime($result['TIMESTAMP']." +1 MONTH"))." at <br/>".date("h:i A",strtotime($result['TIMESTAMP']." +1 MONTH"));
			$servicedate=date("M d",strtotime($result['TIMESTAMP']))." to ".date("M, Y",strtotime($result['TIMESTAMP']." +1 MONTH"));
			$this->mailBody = str_replace("{billingdate}",$billdate,$this->mailBody);
			$this->mailBody = str_replace("{nextbiilingdate}",$nextbiilingdate,$this->mailBody);
			$this->mailBody = str_replace("{servicedate}",$servicedate,$this->mailBody);			
			$this->mailBody = str_replace("{ownername}",$users["UserDetail"]["accountname"]  ,$this->mailBody);
			$this->mailBody = str_replace("{accountemail}",$users["User"]["accountemail"] ,$this->mailBody);
			$this->mailBody = str_replace("{URL}",SITE_LINK,$this->mailBody);
			$this->sendMail($users['User']['accountemail']);
			
			$this->Session->write("Auth.User.is_paid",1);
		}
		return $this->redirect(SITE_LINK."my-account");
	}
}
