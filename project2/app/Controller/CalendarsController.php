<?php
App::uses('AppController', 'Controller');
App::uses('ExceptionRenderer', 'Error');
/**
 * Calendars Controller
 *
 * @property Calendar $Calendar
 * @property PaginatorComponent $Paginator
 */
class CalendarsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');
	
	function beforefilter(){
		parent::beforefilter();
		$this->Auth->allow("getdate");		
	}

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->layout = "designmycalendar";				
		$this->jsArray[] = array("calendar","cropper.min","main");	
		$this->cssArray[] = array("cropper.min","main");		
		$tag_list=array();
		$this->set('optionsRepeat', array( '0' => 'No','1' => 'Daily', '2' => 'Weekly','3' => 'Bi-Weekly', '4' => 'Monthly'));	
		$this->loadModel('Tag');
		$this->con_tags=array("Tag.is_active"=>1);
		
		$tags = $this->Tag->find('all',array("conditions"=>$this->con_tags));
				
		foreach($tags as $tag)
		{
			$tag_list[$tag["Tag"]["id"]]=$tag["Tag"]["tag"];
			
		}
		$this->set('hashtags',$tag_list);	
		$this->conditions=array("user_id"=>$this->Session->read('Auth.User.id'));	
		if ($this->request->is('post')) {			   
			
		}
		else{		
			$tag_list=array();
			$month=date("Y-m-d");
			$ids=$tag_show=array();
			$list=$callist="";
			//~ $this->conditions=array("user_id"=>$this->Session->read('Auth.User.id'),"Day(eventdate)"=>$today);			
			//~ $list = $this->Calendar->find('all',array("conditions"=>$this->conditions));
			
			$hashlist = $this->Calendar->find('all',array("conditions"=>$this->conditions,"fields"=>array('Tag.id','Tag.tag'),"group"=>"Tag.id"));
	
			if(!empty($hashlist))
			{
				$i=0;
				foreach($hashlist as $list)
				{					
					if(!empty($list['Tag']))
					{						
							$tag_show[$i]['id']=$list['Tag']['id'];
							$tag_show[$i]['tag']=$list['Tag']['tag'];								
					}
					$i++;
				}
			}				
			
			$this->set('total_events',count($hashlist));
			$this->set('tag_list',$tag_show);
			
			
				$sql="SELECT group_concat(id),group_concat(eventdate) FROM `calendars` WHERE user_id=".$this->Session->read('Auth.User.id')." and `repeat_event` = 2 and abs(datediff('".$month."',date(`eventdate`))%7) = 0	 and date(eventdate) <= '".$month."'";
				$resultcal =$this->Calendar->query($sql);
				
				if(!empty($resultcal[0][0]['group_concat(id)']))
				{	
					$id_event=explode(",",$resultcal[0][0]['group_concat(id)']);
					$eventdateList=explode(",",$resultcal[0][0]['group_concat(eventdate)']);					
					$i=0;
					foreach($id_event as $idEvent)
					{
						for($i;$i<=count($eventdateList);)
						{
							$eventdate=$eventdateList[$i];$i++;
							break;
						}
						$two_month=date("Y-m-d",strtotime($eventdate." +2 months"));
						
						if($eventdate<=$month && $month<=$two_month)
						{
							$ids[]=$idEvent;				
						}			
					}
				}
				
				
				$sql="SELECT group_concat(id),group_concat(eventdate) FROM `calendars` WHERE user_id=".$this->Session->read('Auth.User.id')." and `repeat_event` = 3 and abs(datediff('".$month."',date(`eventdate`))%14) = 0 and date(eventdate) <= '".$month."'";				
				$resultcal=$this->Calendar->query($sql);
				if(!empty($resultcal[0][0]['group_concat(id)']))
				{	
					$id_event=explode(",",$resultcal[0][0]['group_concat(id)']);
					$eventdateList=explode(",",$resultcal[0][0]['group_concat(eventdate)']);					
					$i=0;
					foreach($id_event as $idEvent)
					{
						for($i;$i<=count($eventdateList);)
						{
							$eventdate=$eventdateList[$i];$i++;
							break;
						}
						$two_month=date("Y-m-d",strtotime($eventdate." +2 months"));
						
						if($eventdate<=$month && $month<=$two_month)
						{
							$ids[]=$idEvent;				
						}			
					}
				}
				
				$sql="SELECT group_concat(id),group_concat(eventdate) FROM `calendars` WHERE user_id=".$this->Session->read('Auth.User.id')." and `repeat_event` = 4 and day('".$month."') = day(`eventdate`) and day(eventdate) <= day('".$month."')";
				$resultcal=$this->Calendar->query($sql);				
				if(!empty($resultcal[0][0]['group_concat(id)']))
				{	
					$id_event=explode(",",$resultcal[0][0]['group_concat(id)']);
					$eventdateList=explode(",",$resultcal[0][0]['group_concat(eventdate)']);					
					$i=0;
					foreach($id_event as $idEvent)
					{
						for($i;$i<=count($eventdateList);)
						{
							$eventdate=$eventdateList[$i];$i++;
							break;
						}
						$two_month=date("Y-m-d",strtotime($eventdate." +2 months"));
						
						if($eventdate<=$month && $month<=$two_month)
						{
							$ids[]=$idEvent;				
						}			
					}
				}
				//pr($ids);die;
				
				$sql="SELECT group_concat(id),group_concat(eventdate) FROM `calendars` WHERE user_id=".$this->Session->read('Auth.User.id')." and `repeat_event` = 1 and date(eventdate) <= '".$month."'";
				$resultcal=$this->Calendar->query($sql);	
				if(!empty($resultcal[0][0]['group_concat(id)']))
				{	
					$id_event=explode(",",$resultcal[0][0]['group_concat(id)']);
					$eventdateList=explode(",",$resultcal[0][0]['group_concat(eventdate)']);					
					$i=0;
					foreach($id_event as $idEvent)
					{
						for($i;$i<=count($eventdateList);)
						{
							$eventdate=$eventdateList[$i];$i++;
							break;
						}
						$two_month=date("Y-m-d",strtotime($eventdate." +2 months"));
						
						if($eventdate<=$month && $month<=$two_month)
						{
							$ids[]=$idEvent;				
						}			
					}
				}				
				
				$sql="SELECT group_concat(id),group_concat(eventdate) FROM `calendars` WHERE user_id=".$this->Session->read('Auth.User.id')." and `repeat_event` = 0 and date(eventdate) = '".$month."'";	
				$resultcal=$this->Calendar->query($sql);	
				if(!empty($resultcal[0][0]['group_concat(id)']))
				{	
					$id_event=explode(",",$resultcal[0][0]['group_concat(id)']);
					$eventdateList=explode(",",$resultcal[0][0]['group_concat(eventdate)']);					
					$i=0;
					foreach($id_event as $idEvent)
					{
						for($i;$i<=count($eventdateList);)
						{
							$eventdate=$eventdateList[$i];$i++;
							break;
						}
						$two_month=date("Y-m-d",strtotime($eventdate." +2 months"));
						
						if($eventdate<=$month && $month<=$two_month)
						{
							$ids[]=$idEvent;				
						}			
					}
				}
				
				 if(!empty($ids))
				 {
					
					$this->conditions=array_merge($this->conditions,array("Calendar.id IN"=> $ids));					
					$callist = $this->Calendar->find('all',array("conditions"=>$this->conditions,"fields"=>"id,user_id, start_hour, id, title, image, eventdate, event_status"));	
				}
				
				$this->set('callist',$callist);
				//pr($callist);die;
				if($callist)
				{
					$live=$save=0;
					foreach($callist as $event)
					{
						if($event["Calendar"]["event_status"]==1)
						{
							$live=$event["Calendar"]["id"];
						}
						else
						{
							$live=$event["Calendar"]["id"];
						}
						$save=0;
					}
					
					$this->set('event_live_id',$live);
				}
				
			$this->loadModel("User");
			$this->conditions=array("User.id"=> $this->Session->read('Auth.User.id'));					
			$userdetail = $this->User->find('first',array("conditions"=>$this->conditions,"fields"=>"UserDetail.address,UserDetail.longitude,UserDetail.latitude,UserDetail.image,UserDetail.fbimage,User.identifier,User.user_type_id"));	
			//pr($userdetail);die;
			//$user = $this->Session->read("Auth");	
			
			if($userdetail["User"]["identifier"]=="")
			{
				if(empty($userdetail["UserDetail"]["image"]) || !file_exists(WWW_ROOT."/img/profile/".$userdetail['UserDetail']['image']))
				{
					 $business_image=SITE_LINK."img/default_new1.png";
				}
				else{
					$business_image=SITE_LINK."img/profile/".$userdetail["UserDetail"]["image"];
				}
			}
			else
			{
				if($userdetail["UserDetail"]["fbimage"]!=""){
					$business_image=$userdetail["UserDetail"]["fbimage"];
				}
				else if(empty($userdetail["UserDetail"]["image"]) || !file_exists(WWW_ROOT."/img/profile/".$userdetail['UserDetail']['image']))
				{
					 $business_image=SITE_LINK."img/default_new1.png";
				}
				else{
					$business_image=SITE_LINK."img/profile/".$userdetail["UserDetail"]["image"];
				}
			}
			
			
				
			//~ if(isset($user['User']['UserDetail']['image']))
			//~ {
				//~ $imageprofile=SITE_LINK."img/profile/".$user['User']['UserDetail']['image'];				
			//~ }
			//~ else if(isset($user['UserDetail']['image']))
			//~ {
				//~ $imageprofile=SITE_LINK."img/profile/".$user['UserDetail']['image']; 
			//~ }
			//~ else 
			//~ {
				//~ $imageprofile=SITE_LINK."img/default_new.png"; 
			//~ }
			
			if(isset($userdetail['UserDetail']))
			{
				 $address=$userdetail['UserDetail']['address'];
				 $longitude=$userdetail['UserDetail']['longitude']; 
				 $latitude=$userdetail['UserDetail']['latitude'];				
			}
			//pr($userdetail);
			$this->set('imageprofile',$business_image);
			$this->set('address',$address);
			$this->set('longitude',$longitude);
			$this->set('latitude',$latitude);
			$this->set('user',$userdetail);	
			//pr($tag_list);die;
			
		}
		
	}
	//Event show on click of Date in Calendar
	public function show_myevent()
	{
		$list="";
		if( $this->request->is("ajax")) {
			if(!empty($this->request->query["month"]))
			{
				$this->layout = false;
				$this->render = false;
				$resultcal="";
				$ids=array();
				$list="";
				$callList=array();
				$month=$this->request->query["month"];				
				//$date=date("d",strtotime($month));
				//$this->conditions=array("user_id"=>$this->Session->read('Auth.User.id'),"Day(eventdate)"=>$date);	
				$this->conditions=array("user_id"=>$this->Session->read('Auth.User.id'));	
				
				//			// and date(eventdate) BETWEEN date(eventdate) AND 	(date(eventdate),INTERVAL 2 MONTH)
				$sql="SELECT group_concat(id), group_concat(eventdate) FROM `calendars` WHERE user_id=".$this->Session->read('Auth.User.id')." and `repeat_event` = 2 and abs(datediff('".$month."',date(`eventdate`))%7) = 0  and date(eventdate) <= '".$month."'";
				
				$resultcal =$this->Calendar->query($sql);
				
				if(!empty($resultcal[0][0]['group_concat(id)']))
				{	
					$id_event=explode(",",$resultcal[0][0]['group_concat(id)']);
					$eventdateList=explode(",",$resultcal[0][0]['group_concat(eventdate)']);					
					$i=0;
					foreach($id_event as $idEvent)
					{
						for($i;$i<=count($eventdateList);)
						{
							$eventdate=$eventdateList[$i];$i++;
							break;
						}
						$two_month=date("Y-m-d",strtotime($eventdate." +2 months"));
						
						if($eventdate<=$month && $month<=$two_month)
						{
							$ids[]=$idEvent;				
						}			
					}
				}
				
				
				$sql="SELECT group_concat(id), group_concat(eventdate) FROM `calendars` WHERE user_id=".$this->Session->read('Auth.User.id')." and `repeat_event` = 3 and abs(datediff('".$month."',date(`eventdate`))%14) = 0 and date(eventdate) <= '".$month."'";				
				$resultcal=$this->Calendar->query($sql);
				if(!empty($resultcal[0][0]['group_concat(id)']))
				{	
					$id_event=explode(",",$resultcal[0][0]['group_concat(id)']);
					$eventdateList=explode(",",$resultcal[0][0]['group_concat(eventdate)']);					
					$i=0;
					foreach($id_event as $idEvent)
					{
						for($i;$i<=count($eventdateList);)
						{
							$eventdate=$eventdateList[$i];$i++;
							break;
						}
						$two_month=date("Y-m-d",strtotime($eventdate." +2 months"));
						
						if($eventdate<=$month && $month<=$two_month)
						{
							$ids[]=$idEvent;				
						}			
					}
				}
				
				$sql="SELECT group_concat(id), group_concat(eventdate) FROM `calendars` WHERE user_id=".$this->Session->read('Auth.User.id')." and  `repeat_event` = 4 and day('".$month."') = day(`eventdate`) and day(eventdate) <= day('".$month."')";
				
				$resultcal=$this->Calendar->query($sql);				
				if(!empty($resultcal[0][0]['group_concat(id)']))
				{	
					$id_event=explode(",",$resultcal[0][0]['group_concat(id)']);
					$eventdateList=explode(",",$resultcal[0][0]['group_concat(eventdate)']);					
					$i=0;
					foreach($id_event as $idEvent)
					{
						for($i;$i<=count($eventdateList);)
						{
							$eventdate=$eventdateList[$i];$i++;
							break;
						}
						$two_month=date("Y-m-d",strtotime($eventdate." +2 months"));
						
						if($eventdate<=$month && $month<=$two_month)
						{
							$ids[]=$idEvent;				
						}			
					}
				}
				//pr($ids);die;
				//pr($month);
				$sql="SELECT group_concat(id), group_concat(eventdate) FROM `calendars` WHERE user_id=".$this->Session->read('Auth.User.id')." and  `repeat_event` = 1 and date(eventdate) <= '".$month."'";
				$resultcal=$this->Calendar->query($sql);				
				$eventdate="";
				
				if(!empty($resultcal[0][0]['group_concat(id)']))
				{	
					$id_event=explode(",",$resultcal[0][0]['group_concat(id)']);
					$eventdateList=explode(",",$resultcal[0][0]['group_concat(eventdate)']);
					
					$i=0;
					foreach($id_event as $idEvent)
					{	
						for($i;$i<=count($eventdateList);)
						{
							 $eventdate=$eventdateList[$i]; $i++;
							break;
						}
						$two_month=date("Y-m-d",strtotime($eventdate." +2 months"));
						//echo $eventdate ;echo $month;echo $two_month;
						if($eventdate<=$month && $month<=$two_month)
						{
							$ids[]=$idEvent;				
						}
					}
				}				
				//die;
				$sql="SELECT group_concat(id), group_concat(eventdate) FROM `calendars` WHERE user_id=".$this->Session->read('Auth.User.id')." and  `repeat_event` = 0 and date(eventdate) = '".$month."'";	
				$resultcal=$this->Calendar->query($sql);	
				if(!empty($resultcal[0][0]['group_concat(id)']))
				{	
					$id_event=explode(",",$resultcal[0][0]['group_concat(id)']);
					$eventdateList=explode(",",$resultcal[0][0]['group_concat(eventdate)']);					
					$i=0;
					foreach($id_event as $idEvent)
					{
						for($i;$i<=count($eventdateList);)
						{
							$eventdate=$eventdateList[$i];$i++;
							break;
						}
						$two_month=date("Y-m-d",strtotime($eventdate." +2 months"));
						
						if($eventdate<=$month && $month<=$two_month)
						{
							$ids[]=$idEvent;				
						}
					}
				}
				if(!empty($ids))
				 {
					$this->conditions=array_merge($this->conditions,array("Calendar.id IN"=> $ids));
					$list = $this->Calendar->find('all',array("conditions"=>$this->conditions,"order"=>"event_status desc","fields"=>"user_id,start_hour,id,title,image,eventdate,event_status"));
					//pr($list);die;
					if(!empty($list))
					{
						$i = 0;
						foreach($list as $val)
						{
							$callList[$i]["id"]=$val["Calendar"]["id"];
							$callList[$i]["user_id"]=$val["Calendar"]["user_id"];
							$callList[$i]["start_hour"]=$val["Calendar"]["start_hour"];
							$callList[$i]["id"]=$val["Calendar"]["id"];
							$callList[$i]["title"]=$val["Calendar"]["title"];
							if(empty($val["Calendar"]["image"]) || !file_exists(WWW_ROOT."/img/events/".$val['Calendar']['image']))
							{
								 $imagevent="img/default_event.png";
							}
							else{
								$imagevent="img/events/".$val["Calendar"]["image"];
							}
							$callList[$i]["image"]=$imagevent;
							$callList[$i]["eventdate"]=$val["Calendar"]["eventdate"];
							$callList[$i]["event_status"]=$val["Calendar"]["event_status"];							
							$i++;
						}
					}
					
				}
				
			}
		}
		echo json_encode($callList);	
		die;
	}
	public function countTotalEvents()
	{
		$count="";
		if( $this->request->is("ajax")) {
			
				$this->layout = false;
				$this->render = false;							
				$this->conditions=array("user_id"=>$this->Session->read('Auth.User.id'));			
				$hashlist = $this->Calendar->find('all',array("conditions"=>$this->conditions,"recursive"=>-1,"fields"=>'id'));
				$count=count($hashlist);							
		
		}
		echo json_encode($count);	
		die;
	}
	public function confirm_del_comment()
	{
		$list="";
		if( $this->request->is("ajax")) {
			
			if(!empty($this->request->query['id']))
			{
				$this->layout = false;
				$this->loadModel('Comment');
				$this->render = false;
				$id=$this->request->query['id'];				
				$event = $this->Comment->find("first",array("conditions"=>array("id"=>$id)));
				if (empty($event)) {
					//throw new NotFoundException(__('Invalid event'));
					$message="errortt";
				}
				else		
				{			
					$this->Comment->id = $id;					
					if ($this->Comment->delete()) {
						$message="success";
					} else {
						$message="error";
					}	
				}			
			}
		}
		echo json_encode($message);	
		die;
	}
	public function delete_event()
	{
		$list="";
		if( $this->request->is("ajax")) {
			
			if(!empty($this->request->query['id']))
			{
				$this->layout = false;
				$this->render = false;
				$id=$this->request->query['id'];				
				$event = $this->Calendar->find("first",array("conditions"=>array("id"=>$id,"user_id"=>$this->Auth->user("id")),"recursive"=>-1));
				if (empty($event)) {
					//throw new NotFoundException(__('Invalid event'));
					$message="errortt";
				}
				else		
				{			
					$this->Calendar->id = $id;	
					if(!empty($event["Calendar"]["image"]) && file_exists(WWW_ROOT."img/events/".$event['Calendar']['image']))
					{
						unlink(WWW_ROOT."img/events/".$event["Calendar"]["image"]);
					}
					if ($this->Calendar->delete()) {
						$message="success";
					} else {
						$message="error";
					}	
				}			
			}
		}
		echo json_encode($message);	
		die;
	}
	//MAke calendar in iframe
	public function calendar($month = null) {
		if($this->request->is("get") ) {	
			$this->layout = false;
			//pr($this->params);	
			$sel_date="";		
			if(empty($this->request->query))
			$showmonth=date("Y-m-d");			
			else
			$showmonth=$this->request->query['month'];
		
			if(isset($this->request->query['set_yes']))
			{
				$set_yes="no";					
			}
			else
			{
				$set_yes="yes";
			}
		
			
		
			$sql = "select MONTH('".$showmonth."') as currentMonth,DATE_FORMAT(LAST_DAY('".$showmonth."'),'%d') lastDay,DATE_FORMAT('".$showmonth."','%Y-%m-01') firstDay, DATE_FORMAT(DATE_FORMAT('".$showmonth."','%Y-%m-01'),'%a') first_day,DATE_FORMAT(DATE_ADD('".$showmonth."', Interval 1 month),'%Y-%m-01') as nextMonth,DATE_FORMAT(DATE_SUB('".$showmonth."', Interval 1 month),'%Y-%m-01') as prevMonth, date(now()) as currDate";
			//die;	
			
			$date = date('M Y',strtotime($showmonth));
			$resultcal=$this->Calendar->query($sql);		
			//pr($resultcal);
			$currMonth = $resultcal[0][0]['firstDay'];
			$tmpMonth = explode("-",$currMonth);
			$tmpMonth[2] = "{DATE}";
			$currMonth = implode("-",$tmpMonth);
			$currMonth = $resultcal[0][0]['firstDay'];
			$dayArr = array("Sun"=>1,"Mon"=>2,"Tue"=>3,"Wed"=>4,"Thu"=>5,"Fri"=>6,"Sat"=>7);			
			
			if(isset($this->request->query['sel_date']))
			{
				$sel_date=date("d",strtotime($this->request->query['sel_date']));	
					
			}
			else
			{
				if(date("d")==31)
				{
					if($resultcal[0][0]['lastDay']==date("d"))
					{
						$sel_date=date("d");
					}
					else
					{
						$sel_date=$resultcal[0][0]['lastDay'];
					}
				}
				else
				{
					$sel_date=date("d");
				}
			}
			$sel_date1=date("d");
			$eventArr=array();							
			$this->conditions=array("user_id"=>$this->Session->read('Auth.User.id'));
	
	
			// and date(eventdate) BETWEEN date(eventdate) AND 	(date(eventdate),INTERVAL 2 MONTH)
			//'eventdate BETWEEN date(eventdate) AND DATE_ADD(eventdate,INTERVAL 2 MONTH)');	//$this->conditions=array("user_id"=>$this->Session->read('Auth.User.id'),"MONTH(eventdate)=MONTH(CURDATE())","YEAR(eventdate) = YEAR(CURDATE())");
			$this->Calendar->virtualFields["event_status_date"]="select count(*) from calendars where date(eventdate) = Calendar.eventdate";
						
			//~ $this->Calendar->virtualFields["next_event_status"]="(select case when Calendar.repeat_event = 2 then DATE_ADD(eventdate,INTERVAL 1 WEEK) when Calendar.repeat_event = 3 then DATE_ADD(eventdate,INTERVAL 2 WEEK) when Calendar.repeat_event = 4 then DATE_ADD(eventdate,INTERVAL 1 MONTH) end from calendars where id = Calendar.id)";
			
			 $this->Calendar->virtualFields["next_event_status"]="(select case when Calendar.repeat_event = 2  then DATE_ADD(eventdate,INTERVAL 1 WEEK) when Calendar.repeat_event = 3 then DATE_ADD(eventdate,INTERVAL 2 WEEK) when Calendar.repeat_event = 4 then DATE_ADD(eventdate,INTERVAL 1 MONTH) end from calendars where id = Calendar.id)";
			
			
			
			$callist = $this->Calendar->find('all',array("conditions"=>$this->conditions,"fields"=>array("DAY(eventdate) as day","id","eventdate","event_status","repeat_event","Calendar.event_status_date","Calendar.next_event_status"),"order"=>"event_status desc"));
		//pr($callist);die;
			foreach($callist as $list){
				$eventObj=$list['Calendar'];
				$eventDate=date('d',strtotime($list['Calendar']['eventdate']));
				$eventObj['selected_month_equal']=date('Y-m',strtotime($showmonth))."-01";
				$eventObj['selected_month']=date('Y-m',strtotime($showmonth))."-".$sel_date1;
				$eventObj['current_month']=date('Y-m-d');
				$eventObj['selected_month_btw']=date('Y-m',strtotime($showmonth));	
				$eventArr[]=$eventObj;
				unset($eventObj);
			}	
			//$eventArr= Set::combine($eventArr,'{n}.id','{n}','{n}.eventdate');
			$this->set('eventArr',$eventArr);
			$this->set('resultcal',$resultcal[0]);
			$this->set('date',$date);
			$this->set('dayArr',$dayArr);
			$this->set('sel_date',$sel_date);
			$this->set('set_yes',$set_yes);
			//$eventObj['sel_date']=$sel_date;		
			$this->set('currMonth',date('Y-m-',strtotime($currMonth)));
			//pr($resultcal);die;		
		}
		
	}
	//Auto save event
	public function add_event($data = null) {
		if( $this->request->is("ajax")) {
			$this->layout = false;
			$this->render = false;
		// pr($this->request->data);		die;	
			$id=$this->request->data['Calendar']['id'];
			$this->request->data['Calendar']['eventdate']= date('Y-m-d',strtotime(($this->request->data['Calendar']['eventdate'])));
			$this->request->data['Calendar']['start_hour']= date('H:i',strtotime(($this->request->data['Calendar']['start_hour'])));
			//$this->request->data['Calendar']['end_hour']= date('H:i',strtotime(($this->request->data['Calendar']['end_hour'])));
				
			if(!empty($this->request->data['Calendar']['end_hour']))		
			$this->request->data['Calendar']['end_hour']=date('H:i',strtotime(($this->request->data['Calendar']['end_hour'])));
			else
			$this->request->data['Calendar']['end_hour']="";
			
			if(!empty($this->request->data['Calendar']["end_price"]))			
			$this->request->data['Calendar'][" end_price"]=$this->request->data['Calendar']['end_price'];
			else
			$this->request->data['Calendar']["end_price"]="";
					
			$this->request->data['Calendar']['user_id'] = $this->Session->read('Auth.User.id');	
			$this->Calendar->create();
			if(!empty($id))
			{					
					$this->request->data['Calendar']['id']=$id;
					if ($this->Calendar->save($this->request->data)) {			
				
					} 
			}
			else
			{				
				
				if ($this->Calendar->save($this->request->data)) {				
				  $id=$this->Calendar->getLastInsertId();
				  $this->request->data['Calendar']['id']=$id;
				} 
			}
			if($this->request->data['Calendar']['tmpImage']!=$this->request->data['Calendar']['oldImage'])
			{
				if (isset($this->request->data['Calendar']['tmpImage']) && !empty($this->request->data['Calendar']['tmpImage'])) {
						
					$this->genImage($this->request->data['Calendar']['tmpImage'],"events",$id);
					$this->request->data['Calendar']['image'] = $this->imagename;
					$this->Calendar->save($this->request->data);
				} 
			}
			echo json_encode($id);	
			die;	
		}
		
	}
	
	//fetch data for edit event
	public function fetch_event($data = null) {
		if( $this->request->is("ajax")) {
			
			$this->layout = false;
			$this->render = false;
			//$this->loadModel('Comment');
			$this->loadModel('SaveCalendar');
				//pr($this->params);			
			$id=$this->request->query['id'];
			$date=$this->request->query['date'];
			if(!empty($id))
			{
				$this->conditions=array("user_id"=>$this->Session->read('Auth.User.id'),"Calendar.id"=>$id);			
				$callist = $this->Calendar->find('all',array("conditions"=>$this->conditions));

				if(!empty($callist))
				{
					$i = 0;
					$event_view=$save_calendar="";
					$comments=$c=0;
					foreach($callist as $val)
					{						
						$save_calendar=$event_view=$unviewed=0;
						if(!empty($val["SaveCalendar"]))
						{
							foreach($val["SaveCalendar"] as $calendar)
							{
								if($calendar["type"]=="calendar" && $calendar["event_date"]==$date)
								{
									$save_calendar=$save_calendar+1;
								}
								elseif($calendar["type"]=="viewed" && $calendar["event_date"]==$date )
								{
									$event_view=$event_view+1;
								}
								else
								{
								}
							}
						}
						
						if(!empty($val["Comments"]))
						{
							
							foreach($val["Comments"] as $comment)
							{
								
								if($date==date("Y-m-d",strtotime($comment["comment_add"])))
								{
									if($comment["viewed"]=="" || $comment["viewed"]==0)
									{
										$unviewed=$unviewed+1;
									}
									$comments=$c++;
								}
								
							}
							
						}
						
						$comments=$c;
						
						
						
						$callList[$i]["id"]=$val["Calendar"]["id"];
						$callList[$i]["user_id"]=$val["Calendar"]["user_id"];
						$callList[$i]["title"]=$val["Calendar"]["title"];
						$callList[$i]["eventdate"]=$val["Calendar"]["eventdate"];
						$callList[$i]["short_dec"]=$val["Calendar"]["short_dec"];
						$callList[$i]["long_dec"]=$val["Calendar"]["long_dec"];
						
						$callList[$i]["location"]=$val["Calendar"]["location"];
						$callList[$i]["latitude"]=$val["Calendar"]["latitude"];
						$callList[$i]["longitude"]=$val["Calendar"]["longitude"];
						$callList[$i]["ticket_free"]=$val["Calendar"]["ticket_free"];
						$callList[$i]["start_price"]=$val["Calendar"]["start_price"];
						$callList[$i]["end_price"]=$val["Calendar"]["end_price"];
						$callList[$i]["start_hour"]=$val["Calendar"]["start_hour"];
						$callList[$i]["end_hour"]=$val["Calendar"]["end_hour"];						
						$callList[$i]["repeat_event"]=$val["Calendar"]["repeat_event"];	
						$callList[$i]["ticket_link"]=$val["Calendar"]["ticket_link"];	
						$callList[$i]["tags"]=$val["Calendar"]["tags"];	
						$callList[$i]["event_status"]=$val["Calendar"]["event_status"];	
						$callList[$i]["unsuitable_count"]=$val["Calendar"]["unsuitable_count"];
						$callList[$i]["is_active"]=$val["Calendar"]["is_active"];	
						
						$callList[$i]["event_view"]=$event_view;
						$callList[$i]["save_calendar"]=$save_calendar;
						$callList[$i]["comments"]=$comments;
						$callList[$i]["unviewed"]=$unviewed;
						
						if(empty($val["Calendar"]["image"]) || !file_exists(WWW_ROOT."/img/events/".$val['Calendar']['image']))
						{
							 $imagevent="img/default_event.png";
						}
						else{
							$imagevent="img/events/".$val["Calendar"]["image"];
						}
						$callList[$i]["image"]=$imagevent;
						
						$i++;
					}
					$message=$callList;
				}
				else
				{
					$message="Invalid Id";
				}
			}
			else
			{
				$message="Invalid Request";
			}
			echo json_encode($message);	
			die;	
		}
	}
	//fetch data comments
	public function fetch_comments($id = null) {
		if( $this->request->is("ajax")) {
			$this->layout = false;
			$this->render = false;
			$this->loadModel('Comment');
			
			$this->Comment->hasMany = $this->Comment->belongsTo = $this->Comment->hasOne = array();
			$this->Comment->belongsTo = array(
				"User" => array(
					"className" => "User",
					"foreignKey" => "",
					"type" => "Inner",
					"conditions"=>"User.id=Comment.user_id"
				),
				"UserDetail" => array(
					"className" => "UserDetail",
					"foreignKey" => "",
					"type" => "Inner",
					"conditions"=>"UserDetail.user_id=Comment.user_id"
				)
			); 		
			$id=$this->request->query['id'];
			$date=$this->request->query['date'];
			if(!empty($id))
			{
				$this->conditions=array("User.is_active"=>1,"Comment.event_id"=>$id,"date(Comment.comment_add)"=>$date);
			   //pr($this->conditions);		die;
			  $this->Comment->updateAll(array('viewed' => 1),array('event_id' => $id));
			   
				$callist = $this->Comment->find('all',array("conditions"=>$this->conditions,"order"=>"Comment.created asc"));			
				
				if(!empty($callist))
				{
					$message="";
					$i = 0;					
					foreach($callist as $val)
					{						
						$callList[$i]["id"]=$val["Comment"]["id"];
						$callList[$i]["created"]=$val["Comment"]["created"];
						$callList[$i]["comment"]=$val["Comment"]["comment"];
						$callList[$i]["image"]=$val["UserDetail"]["image"];
						$callList[$i]["business"]=$val["UserDetail"]["business"];
						
						if($val["User"]["identifier"]=="")
						{	
							if(empty($val["UserDetail"]["image"]) || !file_exists(WWW_ROOT."/img/profile/".$val['UserDetail']['image']))
							{
								 $imagevent=SITE_LINK."img/default_new.png";
							}
							else{
								$imagevent=SITE_LINK."img/profile/".$val["UserDetail"]["image"];
							}
						}
						else
						{
							if($val["UserDetail"]["fbimage"]!=""){
									$imagevent=$val["UserDetail"]["fbimage"];
								}
								else if(empty($val["UserDetail"]["image"]) || !file_exists(WWW_ROOT."/img/profile/".$val['UserDetail']['image']))
								{
									  $imagevent=SITE_LINK."img/default_new1.png";
								}
								else{
									$imagevent=SITE_LINK."img/profile/".$val["UserDetail"]["image"];
								}	
						}	
						$callList[$i]["image"]=$imagevent;
						
						$i++;
					}
					$message=$callList;
				}
				else
				{
					$message="no-comments";
				}
			}
			else
			{
				$message="invalid_request";
			}
			echo json_encode($message);	
			die;	
		}		
	}
	
	//add_comment
	public function add_comment($id = null) {
		if( $this->request->is("ajax")) {
			$this->layout = false;
			$this->render = false;
			$flag=false;
			$this->loadModel('Comment');
			$this->loadModel('User');
			$this->loadModel('SaveCalendar');
			//$id=$this->request->query['id'];
			//pr($this->request->query);die;
			$this->request->data["event_id"]=$this->request->query['id'];
			$this->request->data["comment"]=$this->request->query['comment'];
			
		
			
			//$this->request->data["mention_ids"]=$this->request->query['mention_ids'];
			$this->request->data["is_active"]=1;
			$this->request->data["viewed"]=1;
			$this->request->data["user_id"]=$this->Session->read('Auth.User.id');
			$this->request->data["comment_add"]=$this->request->query["date"]." ".date("h:i:s");
			//$this->request->data["created"]=$this->request->query["date"]." ".date("h:i:s");
			if(!empty($this->request->query['id']))
			{
				if ($callist=$this->Comment->save($this->request->data)) {				
					
						$message="success";
						if(isset($this->request->query['mention_ids']) && !empty($this->request->query['mention_ids']))
						{
							$users_mention=explode("!!",$this->request->query['mention_ids']);
							
							 foreach($users_mention as $uids)
							 {	
								if(is_numeric($uids) && !empty($uids))
								{									
									 if($user=$this->User->find("first",array("conditions"=>array("User.id"=>$uids))))	
									 {
										
										if($user["User"]["user_type_id"]==3)
										{
											if($user["UserDetail"]["comment_mention"]==1)
											{
												$flag=true;
											}
											if($user["UserDetail"]["comment_save_event"]==1)
											{
												if($save_calendar=$this->SaveCalendar->find("all",array("conditions"=>array("event_id"=>$this->request->data["event_id"],"user_id"=>$uids))))
												{
													$flag=true;
												}
											}										
											
											if($flag)
											{			
												$data['Notification']["sender_id"]=$this->Session->read('Auth.User.id');
												$data['Notification']["user_id"]=$uids;
												$data['Notification']["text"]=$this->Session->read('Auth.User.UserDetail.business')." mentioned you in a comment";
												$data['Notification']["type"]="Notifications";
												$data['Notification']["comment_id"]=$callist['Comment']["id"];
												$data['Notification']["event_id"]=$callist['Comment']["event_id"];
												$data['Notification']["comment_add"]=$callist['Comment']["comment_add"];
												$event_id=$callist['Comment']["event_id"];
												$comment_id=$callist['Comment']["id"];
												
												$event_date=date('Y-m-d',strtotime($callist['Comment']["created"]));												 
												$this->loadModel('Notification');
												if($notify=$this->Notification->save($data['Notification']))	
												{
													$this->loadModel('UserDevice');													
													if($token=$this->UserDevice->find("first",array("conditions"=>array("User.id"=>$uids))))
													{
														$user_device_key[]=$token["UserDevice"]["token"];
														$device_type=$token["UserDevice"]["device_OS"];
														$message=$data['Notification']["text"];
														$notificationType="comment";
														$data=$this->push_message($user_device_key,$message,$event_id,$comment_id,$event_date,$device_type,$notificationType);
														$message="success";
													}
												}
											}
										} 
									}
								}
							}						
							$message="success";
						}
				} 
				else
				{
					$message="error";
				}
			}	
			else
			{
				$message="invalid_request";
			}
			echo json_encode($message);	
			die;	
		}		
	}
	public function push_message($user_device_key,$message,$event_id,$comment_id,$event_date,$device_type,$notificationType=null)
	{
		//pr($user_device_key);die;
		//echo API_ACCESS_KEY;die;
		#prep the bundle
			 $msg = array
				  (
				'body' 	=> $message,
				'event_id' 	=> $event_id,
				'comment_id' 	=> $comment_id,
				'event_date' 	=> $event_date,
				'type'	=> $notificationType,						
				//'icon'	=> 'myicon',/*Default Icon*/
				//'sound' => 'mySound'/*Default sound*/
				  );
				if($device_type=="android")
				{
					$fields = array
					(
						//'event_id' =>"123",
						'registration_ids' =>$user_device_key,
						'data' =>  array(							
							"msg" => $msg
						 ),
						'content_available'=>true,
						 'priority' => 10,
						//'notificationtype'	=> $notificationType
					);			
				}
				else
				{
					$fields = array
					(
						//'event_id' =>"123",
						'registration_ids' =>$user_device_key,						
						'content_available'=>true,
						 'priority' => 10,
						'notification'	=>   $msg
						
					);	
				}
			$headers = array
					(
						'Authorization: key=' . API_ACCESS_KEY,
						'Content-Type: application/json'
					);

				#Send Reponse To FireBase Server	
						$ch = curl_init();
						curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
						curl_setopt( $ch,CURLOPT_POST, true );
						curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
						curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
						curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
						curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
						$cexecute = curl_exec($ch );
						curl_close( $ch );

				#Echo Result Of FireBase Server
				//echo $result;die;
				 $result=json_decode($cexecute,true);
				//pr($cexecute);die;
				return $result;
	}
	public function showtags()
	{
		$list="";
		$callList=array();
		if( $this->request->is("ajax")) {
			if(!empty($this->request->query["tag"]))
			{
				$this->layout = false;
				$this->render = false;
				$searchval=$this->request->query["tag"];
				$this->conditions=array("user_id"=>$this->Session->read('Auth.User.id'));
				
				if($searchval!="all")
				$this->conditions =  array_merge($this->conditions,array("tags"=>$searchval));
							
				$list = $this->Calendar->find('all',array("conditions"=>$this->conditions,"fields"=>"user_id,start_hour,id,title,image,eventdate,event_status"));	
				
				if(!empty($list))
					{
						$i = 0;
						foreach($list as $val)
						{
							
							$callList[$i]["user_id"]=$val["Calendar"]["user_id"];
							$callList[$i]["start_hour"]=$val["Calendar"]["start_hour"];
							$callList[$i]["id"]=$val["Calendar"]["id"];
							$callList[$i]["title"]=$val["Calendar"]["title"];
							if(empty($val["Calendar"]["image"]) || !file_exists(WWW_ROOT."/img/events/".$val['Calendar']['image']))
							{
								 $imagevent="img/default_event.png";
							}
							else{
								$imagevent="img/events/".$val["Calendar"]["image"];
							}
							$callList[$i]["image"]=$imagevent;
							$callList[$i]["eventdate"]=$val["Calendar"]["eventdate"];
							$callList[$i]["event_status"]=$val["Calendar"]["event_status"];							
							$i++;
						}
					}
							
			}
		}
		echo json_encode($callList);	
		die;
	}
	public function countUserEvents()
	{
		$message="error";
		if( $this->request->is("ajax")) {
			$date=$this->request->query['month'];
			$id=$this->request->query['id'];
			$ts = strtotime($date);
			$start = (date('w', $ts) == 0) ? $ts : strtotime('last sunday', $ts);
			$startWeek=date('Y-m-d', $start);
			$endWeek= date('Y-m-d', strtotime('next saturday', $start));
						
			$user = $this->Session->read("Auth");
			//echo $user["User"]["user_type_id"];
			if($user["User"]["user_type_id"]==2)
			{
				$this->conditions=array("user_id"=>$this->Session->read('Auth.User.id'),"id !="=>$id);
				//$this->conditions=array("user_id"=>$this->Session->read('Auth.User.id'),"eventdate"=>$date,"event_status"=>1);
				//$this->conditions =  array_merge($this->conditions,array("event_status"=>1,"AND"=>array(array('eventdate >= ' => $startWeek,'eventdate <= ' => $endWeek))));
				
				$this->conditions =  array_merge($this->conditions,array("event_status"=>1,'eventdate BETWEEN "'.$startWeek.'" AND "'.$endWeek.'"'));
					
				$list = $this->Calendar->find('all',array("conditions"=>$this->conditions,"recursive"=>-1,"fields"=>"id,eventdate,event_status"));
				$total_day_count=false;
			//pr($list);
				//~ if(!empty($list))
				//~ {
					//~ foreach($list as $val)
					//~ {
						//~ if($val["Calendar"]["eventdate"]==$date)
						//~ {
							//~ $total_day_count=true;
						//~ }
					//~ }
				//~ }
				//~ if($total_day_count)
				//~ {
					//~ $message="max_day_count";					
				//~ }
				//~ else
					//~ {
						$total_event=count($list);
									
						$this->conditions=array("id"=>$this->Session->read('Auth.User.id'));
						$this->loadModel("User");
						$user_paid = $this->User->find('first',array("conditions"=>$this->conditions,"recursive"=>-1,"fields"=>"id,is_paid"));
						
						$message="min";						
						if(empty($user_paid['User']['is_paid']) || $user_paid['User']['is_paid']==0)
						{	
							//pr($total_event);						
							if($total_event >=3 )
							{
								$message="max";
							}
						}
						else
						{
							if($total_event >=7 )
							{
								$message="max";
							}				
						}
				//~ }
			}
			else
			{
				$message="min";
			}
		}
		echo json_encode($message);	
		die;
	}
	
	function tagList()
	{
		$tag_list=array();
		if( $this->request->is("ajax")) {			
				$this->layout = false;
				$this->render = false;
				$this->conditions=array("user_id"=>$this->Session->read('Auth.User.id'));
				$this->loadModel('Tag');	
				$this->Calendar->hasMany = $this->Calendar->hasOne = $this->Calendar->belongsTo = array();
				$this->Calendar->belongsTo = array(
				"Tag"=>array(
					"className" => "Tag",
					"foriegnKey" => false,
					"conditions" => "Tag.id = Calendar.tags"
				)
				);			
				$hashlist = $this->Calendar->find('all',array("conditions"=>$this->conditions,"fields"=>array('Tag.tag','Tag.id'),'group' => '`Tag`.`id`'));
				
				$i=0;
				foreach($hashlist as $list)
				{
					if(!empty($list['Tag']['id']))
					{	
						$tag_list[$i]['id']=$list['Tag']['id'];	
						$tag_list[$i]['tag']=$list['Tag']['tag'];
						$i++;				
					}
				}
							
		}
			
		echo json_encode($tag_list);	
		die;
	}
	
	//Admin event manager Detail View 
		public function admin_filter_total_events($type = null) {  
		if($this->request->is("ajax")) {
			$this->layout = false;
			$this->render = false;
			$count=0;
			$type="total_events";
			$to=date("Y-m-d");
			$from=date("Y-m-d");
			$id=$this->request->query["id"];
			
			if(isset($this->request->query["type"]) || !empty($this->request->query["type"]))
			{
				$type=$this->request->query["type"];
			}
			if(isset($this->request->query["to"]) || !empty($this->request->query["to"]))
			{
				$to=$this->request->query["to"];
			}
			if(isset($this->request->query["from"]) || !empty($this->request->query["from"]))
			{
				$from=$this->request->query["from"];
			}
				
			$this->conditions=array("user_id"=>$id,'eventdate BETWEEN "'.$from.'" AND "'.$to.'"');			
			$list = $this->Calendar->find('all',array("conditions"=>$this->conditions,"recursive"=>-1,"fields"=>'id'));
			$count=count($list);	
			$msg="success";			
	  }
	  else
	  {
		 $msg="error";	
	  }
	  echo json_encode(array("msg"=>$msg,"count"=>$count));	
	  die;
	}
	
	//Admin event setting 
	public function admin_events_setting($type = null) {  
	
		$this->layout = "mooladesignadmin";
		$this->jsArray[] = array("increment/incrementing");	
		$this->cssArray[] = array("increment/style");
		$this->loadModel("DefaultValue");
		if ($this->request->is('post')) {	
			
			$list = $this->DefaultValue->save($this->request->data);
		}
		
		$list = $this->DefaultValue->find('first');		
		$this->set("list",$list);
					
	 
	}
	
	public function admin_index($limit=20) { 
		
		$this->set('records', array( '20' => '20', '30' => '30','40' => '40', '80' => '80','100' => '100'));
		if ( $this->request->is("get") ) {		
			isset($this->request->query["records"])?$limit=$this->request->query["records"]:20;
			
		}	
		$this->bulkactions(true);
	
		if ( $this->request->is("post") ) {
			
			if ( !empty($this->data['Calendar']['searchval']) ) {
				$url = SITE_LINK."view_events";
				if ( $this->request->query["manager"] && !empty($this->request->query["manager"]) ) {
					$url .= "?manager=".$this->request->query["manager"]."&";
				} else {
					$url .= "?";
				}
				$this->redirect($url."searchval=".$this->data['Calendar']['searchval']);
			} else {
				$this->redirect(SITE_LINK."view_events");
			}
		}
		$this->set('limit',$limit);	
		
		(isset($this->params["named"]["page"]))?$sno=(($this->params["named"]["page"]*$limit)-($limit-1)):$sno=1;
		
		$this->layout = "mooladesignadmin";
		$this->Calendar->hasMany = $this->Calendar->hasOne = $this->Calendar->belongsTo = array();
		$this->Calendar->belongsTo = array(
		"UserDetail"=>array(
			"className" => "UserDetail",
			"foriegnKey" => false,
			"conditions" => "UserDetail.user_id = Calendar.user_id"
		)
		);	
		if ( $this->request->query("searchval") && !empty($this->request->query("searchval")) ) {	
		
			$this->set("searchval",$this->request->query("searchval"));
			$searchval = strtolower(trim($this->request->query("searchval")));
			$this->conditions = array("OR"=>array("LOWER(UserDetail.address) like"=> "%".$searchval."%","LOWER(Calendar.location) like"=> "%".$searchval."%","LOWER(Calendar.title) like"=> "%".$searchval."%","LOWER(UserDetail.business) like"=> "%".$searchval."%"));
		}
		if ( $this->request->query("manager") && !empty($this->request->query("manager")) ) {
			$this->conditions = array_merge($this->conditions,array("Calendar.user_id"=>$this->request->query("manager")));
		}
		$this->set('sno',$sno);		
	   $this->paginate = array("order"=>"Calendar.created desc","limit"=>$limit);	 
	   $list=$this->Paginator->paginate($this->conditions);
	  // pr($list);  
	   $this->set('lists', $this->Paginator->paginate($this->conditions));
			
	}

	//fetch data for edit event
	public function admin_edit($id=null) {		
	     	$this->layout = "mooladesignadmin";		
			$this->jsArray[] = array("calendar_admin","cropper.min","main");	
			$this->cssArray[] = array("cropper.min","main");
			$this->set('optionsRepeat', array( '0' => 'No','1' => 'Daily', '2' => 'Weekly','3' => 'Bi-Weekly', '4' => 'Monthly'));	

			if ($this->request->is('post')) {	
			  // pr($this->request->data);die;
				$id=$this->request->data['Calendar']['id'];
				$this->request->data['Calendar']['eventdate']= date('Y-m-d',strtotime(($this->request->data['Calendar']['eventdate'])));
				$this->request->data['Calendar']['start_hour']= date('H:i',strtotime(($this->request->data['Calendar']['start_hour'])));
				$this->request->data['Calendar']['end_hour']= date('H:i',strtotime(($this->request->data['Calendar']['end_hour'])));
				
				if (isset($this->request->data['Calendar']['tmpImage']) && !empty($this->request->data['Calendar']['tmpImage'])) {
					if($this->request->data['Calendar']['tmpImage']==$this->request->data['Calendar']['oldImage'])
					unset($this->request->data['Calendar']['image']);
					else
					{
						$this->genImage($this->request->data['Calendar']['tmpImage'],"events",$id);
						$this->request->data['Calendar']['image'] = $this->imagename;
					}
				} else {
					$this->request->data['Calendar']['image'] = "";
				}
				$this->request->data['Calendar']['user_id'] = $this->Session->read('Auth.User.id');
				$this->Calendar->create();
				if ($this->Calendar->save($this->request->data)) {
					$this->Flash->success(__('The Event has been saved.'));				
					return $this->redirect(SITE_LINK."view_events");
					
				} else {
					$this->Flash->error(__('The Event could not be saved. Please, try again.'));
					return $this->redirect(SITE_LINK."view_events");
				}
			}
			else
			{
				$this->conditions=array("Calendar.id"=>$id);
				$this->Calendar->belongsTo = array(
					"UserDetail" => array(
						"className" => "UserDetail",
						"foreignKey" => "",
						"type" => "Inner",
						"conditions"=>"UserDetail.user_id=Calendar.user_id"
					)
				); 				
				$event = $this->Calendar->find('first',array("conditions"=>$this->conditions));	
				if(!empty($event['Calendar']['tags']))		{	
				$event['Calendar']['tag_list'] = explode(",",$event['Calendar']['tags']);			
				
				}
					
					$save_calendar=$event_viewed=0;
					if(!empty($val["SaveCalendar"]))
						{
							foreach($event["SaveCalendar"] as $calendar)
							{
								if($calendar["type"]=="calendar")
								{
									$save_calendar=$save_calendar+1;
								}
								else
								{
									$event_viewed=$event_viewed+1;
								}
							}
						}
					$this->set("save_calendar",$save_calendar);
						$this->set("event_viewed",$event_viewed);
				$this->set("event",$event);
				
				//Comments
				$this->loadModel('Comment');			
				$this->Comment->hasMany = $this->Comment->belongsTo = $this->Comment->hasOne = array();
				$this->Comment->belongsTo = array(
				"User" => array(
						"className" => "User",
						"foreignKey" => "",
						"type" => "Inner",
						"conditions"=>"User.id=Comment.user_id"
					),
					"UserDetail" => array(
						"className" => "UserDetail",
						"foreignKey" => "",
						"type" => "Inner",
						"conditions"=>"UserDetail.user_id=Comment.user_id"
					)
				); 		
				
					$this->conditions=array("Comment.event_id"=>$id,"User.is_active"=>1);
					$callList=array();	
					$callist = $this->Comment->find('all',array("conditions"=>$this->conditions));			
					
					if(!empty($callist))
					{
						$message="";
						$i = 0;					
						foreach($callist as $val)
						{						
							$callList[$i]["id"]=$val["Comment"]["id"];
							$callList[$i]["created"]=$val["Comment"]["created"];
							$callList[$i]["comment"]=$val["Comment"]["comment"];
							$callList[$i]["image"]=$val["UserDetail"]["image"];
							$callList[$i]["business"]=$val["UserDetail"]["business"];
							
							if(empty($val["UserDetail"]["image"]) || !file_exists(WWW_ROOT."/img/profile/".$val['UserDetail']['image']))
							{
								 $imagevent=SITE_LINK."img/default_new.png";
							}
							else{
								$imagevent=SITE_LINK."img/profile/".$val["UserDetail"]["image"];
							}
							$callList[$i]["image"]=$imagevent;
							
							$i++;
						}
						
					}
					//pr($callList);die;
					$this->set("comments_count",count($callList));
					$this->set("callList",$callList);
				}
	}
	public function admin_delete($id = null) {
		$this->Calendar->id = $id;
		if (!$this->Calendar->exists()) {
			throw new NotFoundException(__('Invalid Event'));
		}
		//$this->request->allowMethod('post', 'delete');
		if ($this->Calendar->delete()) {
			$this->Flash->success(__('The event has been deleted.'));
		} else {
			$this->Flash->error(__('The event could not be deleted. Please, try again.'));
		}
		return $this->redirect(SITE_LINK."view_events");
	}
	
	
	
	//@search for tag list
	
	public function tag_list_comment($id = null) {
		if( $this->request->is("ajax")) {
			$this->layout = false;
			$this->render = false;
			$this->loadModel('Comment');
			$userList =array();
			$data=array();
			$this->Comment->hasMany = $this->Comment->belongsTo = $this->Comment->hasOne = array();
			$this->Comment->belongsTo = array(
				"User" => array(
					"className" => "User",
					"foreignKey" => "",
					"type" => "Inner",
					"conditions"=>"User.id=Comment.user_id"
				),
				"UserDetail" => array(
					"className" => "UserDetail",
					"foreignKey" => "",
					"type" => "Inner",
					"conditions"=>"UserDetail.user_id=Comment.user_id"
				)
			); 		
			$id=$this->request->query['id'];
			$date=$this->request->query['month'];
			//$search=$this->request->query['my_string'];
			//$search = str_replace("@",'',$search);
			if(!empty($id))
			{
				
				$this->conditions=array("Comment.event_id"=>$id,"date(Comment.comment_add)"=>$date,"Comment.user_id !="=>$this->Session->read('Auth.User.id'));
			   //pr($this->conditions);		die;
			  //$this->Comment->updateAll(array('viewed' => 1),array('event_id' => $id));
			   
				$userlist = $this->Comment->find('all',array("conditions"=>$this->conditions,'fields'=>array('DISTINCT user_id','UserDetail.business','UserDetail.image','UserDetail.fbimage','User.identifier'),"order"=>"Comment.created asc"));	
				//pr($userlist);die;
				if(!empty($userlist))
				{
					$message="";
					$i = 0;					
					foreach($userlist as $val)
					{						
						$userList[$i]["name"]=$val["UserDetail"]["business"];
						$userList[$i]["id"]=$val["Comment"]["user_id"];
						if($val["User"]["identifier"]=="")
						{	
							if(empty($val["UserDetail"]["image"]) || !file_exists(WWW_ROOT."/img/profile/".$val['UserDetail']['image']))
							{
								 $imagevent=SITE_LINK."img/default_new.png";
							}
							else{
								$imagevent=SITE_LINK."img/profile/".$val["UserDetail"]["image"];
							}
						}
						else
						{
							if($val["UserDetail"]["fbimage"]!=""){
									$imagevent=$val["UserDetail"]["fbimage"];
								}
								else if(empty($val["UserDetail"]["image"]) || !file_exists(WWW_ROOT."/img/profile/".$val['UserDetail']['image']))
								{
									  $imagevent=SITE_LINK."img/default_new1.png";
								}
								else{
									$imagevent=SITE_LINK."img/profile/".$val["UserDetail"]["image"];
								}	
						}	
						$userList[$i]["image"]=$imagevent;
						
						$i++;
					}
					foreach($userList as $key => $val)
					{
						$user_data[$key]['id'] = $val['id'];
						$user_data[$key]['name'] = $val['name'];
						$user_data[$key]['avatar'] = $val['image'];
						$user_data[$key]['type'] = 'appuser';
					}
					header('Content-Type: application/json');
					$data=$user_data;
					$message="success";
				}
				else
				{
					$message="no-comments";
				}
			}
			else
			{
				$message="invalid_request";
			}
			
			//header('Content-Type: application/json');
			echo json_encode(array("data"=>$data,"message"=>$message));	
			die;	
		}		
	}
	
	
	
	
	
	
	
	//MAke calendar in iframe
	//~ public function calendarMoment($month = null) {
		//~ if($this->request->is("get") ) {	
			//~ $this->layout = false;
						//~ 
			//~ 
			//~ $eventArr=array();							
			//~ $this->conditions=array("user_id"=>$this->Session->read('Auth.User.id'));			
			//~ $this->Calendar->virtualFields["event_status_date"]="select count(*) from calendars where date(eventdate) = Calendar.eventdate";
			//~ 
		//	$this->Calendar->virtualFields["next_event_status"]="select id from calendars where date(now()) = (select case when Calendar.repeat_event = 2 then DATE_ADD(eventdate,INTERVAL 1 WEEK) when Calendar.repeat_event = 3 then DATE_ADD(eventdate,INTERVAL 2 WEEK) when Calendar.repeat_event = 4 then DATE_ADD(eventdate,INTERVAL 1 MONTH) end from calendars where id = Calendar.id and event_status = 1)";
			//~ 
			//~ $this->Calendar->virtualFields["next_event_status"]="(select case when Calendar.repeat_event = 2 then DATE_ADD(eventdate,INTERVAL 1 WEEK) when Calendar.repeat_event = 3 then DATE_ADD(eventdate,INTERVAL 2 WEEK) when Calendar.repeat_event = 4 then DATE_ADD(eventdate,INTERVAL 1 MONTH) end from calendars where id = Calendar.id)";
			//~ 
			//~ $callist = $this->Calendar->find('all',array("conditions"=>$this->conditions,"fields"=>array("DAY(eventdate) as day","id","eventdate","event_status","repeat_event","Calendar.event_status_date","Calendar.next_event_status"),"group"=>"eventdate","order"=>"event_status desc"));
			//~ //pr($callist);die;
			//~ 
			//~ foreach($callist as $list)
			//~ {
				//~ 
				//~ $eventArr[$list[0]['day']]["eventdate"] = $list["Calendar"]['eventdate'];
				//~ $eventArr[$list[0]['day']]["id"]= $list["Calendar"]['id'];
				//~ $eventArr[$list[0]['day']]["event_status"] = $list["Calendar"]['event_status'];
				//~ $eventArr[$list[0]['day']]["event_status_date"] = $list["Calendar"]['event_status_date'];
				//~ $eventArr[$list[0]['day']]["repeat_event"] = $list["Calendar"]['repeat_event'];
				//~ $eventArr[$list[0]['day']]["next_event_status"] = $list["Calendar"]['next_event_status'];				
			//~ }	
			//~ //pr($eventArr);//die;
			//~ $this->set('eventArr',$eventArr);
			//~ $this->set('startMonth',date('Y-m-d'));
			//~ //pr($resultcal);die;		
		//~ }
		//~ 
	//~ }
	
	

}	
	
