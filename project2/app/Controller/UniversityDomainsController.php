<?php
App::uses('AppController', 'Controller');
/**
 * UserUniversities Controller
 *
 * @property UserUniversity $UserUniversity
 * @property PaginatorComponent $Paginator
 */
class UniversityDomainsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index($limit=20) {
		$this->layout = "mooladesignadmin";		
		$url = "manage-domain-extensions";	
		$this->bulkactions();
		$this->set('records', array( '20' => '20', '30' => '30','40' => '40', '60' => '60','80' => '80','100' => '100'));
		
		if ( $this->request->is("get") ) {		
			isset($this->request->query["records"])?$limit=$this->request->query["records"]:'';	
		}	
		$this->set('limit',$limit);	
		(isset($this->params["named"]["page"]))?$sno=(($this->params["named"]["page"]*$limit)-($limit-1)):$sno=1;
		if ( $this->request->query("searchval") && !empty($this->request->query("searchval")) ) {
			
			$this->set("searchval",$this->request->query("searchval"));
			$searchval = strtolower(trim($this->request->query("searchval")));
			$this->conditions = array("OR"=>array("LOWER(UniversityDomain.domain_ext) like"=> "%".$searchval."%"));
		}
		if ( $this->request->is("post") ) {
			if ( !empty($this->data['UniversityDomain']['searchval']) ) {
				$this->redirect(SITE_LINK.$url."?searchval=".$this->data['UniversityDomain']['searchval']);
			} else {
				$this->redirect(SITE_LINK."".$url);
			}
		}
		//$this->paginate = array("limit"=>1);	
		$this->set('sno',$sno);
		$this->paginate = array("limit"=>$limit);
		$this->set('universitydomain', $this->Paginator->paginate($this->conditions));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		$this->layout = "mooladesignadmin";
		if ($this->request->is('post')) {
			$this->UniversityDomain->create();
			if ($this->UniversityDomain->save($this->request->data)) {
				$this->Flash->success(__('The domain extension has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The domain extension could not be saved. Please, try again.'));
			}
		}		
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		$this->layout = "mooladesignadmin";
		if (!$this->UniversityDomain->exists($id)) {
			throw new NotFoundException(__('Invalid domain extension'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->UniversityDomain->save($this->request->data)) {
				$this->Flash->success(__('The domain extension has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Flash->error(__('The domain extension could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('UniversityDomain.' . $this->UniversityDomain->primaryKey => $id));
			$this->request->data = $this->UniversityDomain->find('first', $options);
		}		
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
	
		$this->UniversityDomain->id = $id;
		if (!$this->UniversityDomain->exists()) {
			throw new NotFoundException(__('Invalid domain extension'));
		}
		
		if ($this->UniversityDomain->delete()) {
			$this->Flash->success(__('The domain extension has been deleted.'));
		} else {
			$this->Flash->error(__('The domain extension could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
