<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class PagesController extends AppController {

/**
 * This controller does not use a model
 *
 * @var array
 */
	public $uses = array();
	public $components = array('Paginator',"Paypal");
	
	function beforefilter() {
		parent::beforefilter();
		$this->Auth->allow();
	}
	
	function index($token = NULL) {
		$this->set("title_for_layout","Best Deals For Students | Discounts | Rave");
		$this->set("keywords","Discount For Students, Best Deals For Students, Cheap Discount For Students, Best Deals For Students In Uk, Rave Discounts For Students, Rave, Rave Best Deals");
		if ($this->Session->read("Auth.User.id")) 
		{
			$this->redirect(SITE_LINK."my_calendar");
			//$this->layout = "mooladesigncreatevoucher";		
		}
		else
		{
			$this->layout = "homedesign";	
		}
			
		if (!is_array($this->params['url'])) {
			$url = explode("/",$this->params['url']);
			$url = $url[0];
		} else {
			$url = "";
		}	
		if ( !empty($token) ) { 			
			$this->loadModel("User");
			$users = $this->User->find("first",array("conditions"=>array("User.password_token"=>$token,"User.is_active"=>1),"recursive"=>-1));
			if ( !empty($users) ) {				
				$this->set("token",$token);
				$this->jsArray[] = "resetPassword";
			} else {				
				$this->Flash->error(__("Your password can not be reset, please try again later."), 'default', array("class"=>"error_message"));
				$this->redirect(SITE_LINK);
			}
		} elseif ( $url == "reset_password" ) {						
		
			$this->redirect(SITE_LINK);
		} 
	}
	
	public function merchant() {
		$this->set("title_for_layout","Great Offers For Merchant | Advertise with Rave | Rave");
		$this->set("keywords","Offers For Merchant, Cheap Deals For Merchant");
		$this->layout = "homedesign";		
		if (!is_array($this->params['url'])) {
			$url = explode("/",$this->params['url']);
			$url = $url[0];
		} else {
			$url = "";
		}	
		if ( !empty($token) ) { 			
			$this->loadModel("User");
			$users = $this->User->find("first",array("conditions"=>array("User.password_token"=>$token,"User.is_active"=>1),"recursive"=>-1));
			if ( !empty($users) ) {				
				$this->set("token",$token);
				$this->jsArray[] = "resetPassword";
			} else {				
				$this->Flash->error(__("Your password can not be reset, please try again later."), 'default', array("class"=>"error_message"));
				$this->redirect(SITE_LINK);
			}
		} elseif ( $url == "reset_password" ) {						
		
			$this->redirect(SITE_LINK);
		}
	}	
	
	
	public function account_confirmation() {
		$this->set("title_for_layout","Rave : Congrats!");
		$this->set("keywords","Account Confirmation ,Rave");
		$this->layout = "previewmoolavoucher";
	}	
	public function contact_us() {
		$this->set("title_for_layout","Contact Us | Rave");
		$this->set("keywords","Contact Us , Rave");
		$this->layout = "homedesign_pages";	
		
		if ( $this->request->is("post") ) {
			$this->autoRender = false;
			$response = array("status"=>false);
			if (true/*$this->verifyRecatpcha($this->request->data)*/) {
				//pr($this->request->data);die;
				$this->getMailData("CONTACT_US");				
				$this->mailBody = str_replace("{name}",$this->request->data['Contact']['name'],$this->mailBody);
				$this->mailBody = str_replace("{company}",$this->request->data['Contact']['company'],$this->mailBody);
				$this->mailBody = str_replace("{phone}",$this->request->data['Contact']['phone'],$this->mailBody);
				$this->mailBody = str_replace("{email}",$this->request->data['Contact']['email'],$this->mailBody);
				$this->mailBody = str_replace("{message}",$this->request->data['Contact']['message'],$this->mailBody);
				//$this->mailBody = str_replace("{support_mail}","support@fishook.co.uk",$this->mailBody);				
				//~ $this->sendMail("mandeepteja@zestminds.com");
				$this->sendMail("tome@moolarewards.co.uk");			
				$response = array("status"=>true,"url"=>"contact_us");
			}
			else {
				$response = array("status"=>false,"message"=>"Invalid captcha.");					
			}
			echo json_encode($response);
			die;
		}
	}
	
	public function message_confirm() {
		$this->layout = "mooladesignmessagescreen";
	}
	public function message_confirm_link() {
		$this->layout = "mooladesignmessagescreen";
	}
	public function message_forget_password() {
		$this->layout = "mooladesignmessagescreen";
	}
	public function coming_soon() {
		
		$this->layout = "mooladesigncomingsoon";
		$this->loadModel("Newsletter");
		if ($this->request->is('post')) {	
			$this->Newsletter->create();
			$this->request->data["Newsletter"]["username"]=$this->request->data["notify"]["useremailnotify"];
			//pr($this->request->data);	die;
			if ($this->Newsletter->save($this->request->data)) {
				$this->Flash->success(__('We will notify you when our website is ready.'));				
				 $this->redirect( SITE_LINK);
			} else {
				$this->Flash->error(__('Error found.Please, try again.'));
			}
		}	
	}
	//~ public function share_url() {
		//~ $url = SITE_LINK;
		//~ 
		//~ if(isset($this->request->query["os"]))
		//~ {
			//~ $data = $this->request->query;
			//~ if ( $data['os'] == "android" ){		
				//~ $url = 'http://play.google.com/store/apps/details?id=com.truecaller&hl=en';
			//~ } elseif ( $data['os'] == "ios" ){
				//~ $url = 'http://itunes.apple.com/lb/app/truecaller-caller-id-number/id448142450?mt=8';
			//~ } else {
				//~ $url = SITE_LINK;
			//~ }
		//~ }
		//~ $this->redirect($url);
	//~ }
	//getMobileOperatingSystem
	function get_mob_system() {
			
				$iPod    = stripos($_SERVER['HTTP_USER_AGENT'],"iPod");
				$iPhone  = stripos($_SERVER['HTTP_USER_AGENT'],"iPhone");
				$iPad    = stripos($_SERVER['HTTP_USER_AGENT'],"iPad");
				$Android = stripos($_SERVER['HTTP_USER_AGENT'],"Android");

				//do something with this information
				if( $iPod || $iPhone ){
					die("1");
					//browser reported as an iPhone/iPod touch -- do something here
				}else if($iPad){
					die("2");
					//browser reported as an iPad -- do something here
				}else if($Android){
					die("3");
					//browser reported as an Android device -- do something here
				}else {
					die("5");
				}
			

		
	}
	public function share_url() {	
		$this->layout = false;	
		$this->loadModel('Calendar');
		if(isset($this->request->query["id"]))
		{	
			//~ $isfb = stripos(strtolower($_SERVER['HTTP_REFERER']),'facebook');
			//~ if ($isfb) {
				//~ $iPod    = stripos($_SERVER['HTTP_USER_AGENT'],"iPod");
				//~ $iPhone  = stripos($_SERVER['HTTP_USER_AGENT'],"iPhone");
				//~ $iPad    = stripos($_SERVER['HTTP_USER_AGENT'],"iPad");
				//~ $Android = stripos($_SERVER['HTTP_USER_AGENT'],"Android");

				//~ //do something with this information
				//~ if( $iPod || $iPhone ){
					//~ die("1");
					//~ //browser reported as an iPhone/iPod touch -- do something here
				//~ }else if($iPad){
					//~ die("2");
					//~ //browser reported as an iPad -- do something here
				//~ }else if($Android){
					//~ die("3");
					//~ //browser reported as an Android device -- do something here
				//~ }else {
					//~ $this->redirect(SITE_LINK);
				//~ }
			//~ }
				$id=$this->request->query["id"];
				$this->loadModel("Voucher");
				$this->Calendar->hasMany = $this->Calendar->belongsTo = $this->Calendar->hasOne = array();
				$this->Calendar->belongsTo = array(
					"UserDetail" => array(
						"className" => "UserDetail",
						"foreignKey" => "",
						"type" => "Inner",
						"conditions"=>"UserDetail.user_id=Calendar.user_id"
					)
				); 
				
				
			if (!$this->Calendar->exists($id)) {
				throw new NotFoundException(__('Invalid event'));
			}
			
			$options = array('conditions' => array('Calendar.id'=> $id));
			$calendar =$this->Calendar->find('first', $options);		
			//pr($calendar);die;
			if(isset($this->request->query["event_date"]) && !empty($this->request->query["event_date"]))
			{
				$event_date=date("dS, M Y",strtotime($this->request->query["event_date"]));
			}
			else
			{
				$event_date=date("dS, M Y");
			}
			$this->set("event_date",$event_date);
			$this->set(compact("calendar"));		
		} else {
			$this->redirect(SITE_LINK);
		}		
	}
	
	public function pages_database()
	{	//pr($this->params->url);die;
		$this->loadModel("CmsPage");
	
		if ($this->Session->read("Auth.User.id")) 
		{
			$this->layout = "mooladesignmyvoucher";		
		}
		else
		{
			$this->layout = "homedesign_pages";	
		}
			
		if(isset($this->params->url) || !empty($this->params->url))
		{
			$slug=$this->params->url;
			$conditions=array('conditions' =>array("seo_url"=>$slug,"is_active"=>1));	
			//pr($conditions);die;
			$tmp = $this->CmsPage->find('first', $conditions);	
			$this->set("title_for_layout",$tmp['CmsPage']['header']." | Rave ");
			$this->set("keywords",$tmp['CmsPage']['meta_keyword']);			
			$this->set('CmsPage', $tmp);
		}
		else
		{
			$this->redirect(SITE_LINK);
		}
	}
	public function google() {
		$this->autoRender = false;
		echo "google-site-verification: google4586c7ffdf2af078.html";
	} 
	//~ public function students(){
	//~ 
		//~ 
	//~ }
	//~ 
/**
 * Displays a view
 *
 * @return CakeResponse|null
 * @throws ForbiddenException When a directory traversal attempt.
 * @throws NotFoundException When the view file could not be found
 *   or MissingViewException in debug mode.
 */
	public function display() {
		$path = func_get_args();

		$count = count($path);
		if (!$count) {
			return $this->redirect('/');
		}
		if (in_array('..', $path, true) || in_array('.', $path, true)) {
			throw new ForbiddenException();
		}
		$page = $subpage = $title_for_layout = null;

		if (!empty($path[0])) {
			$page = $path[0];
		}
		if (!empty($path[1])) {
			$subpage = $path[1];
		}
		if (!empty($path[$count - 1])) {
			$title_for_layout = Inflector::humanize($path[$count - 1]);
		}
		$this->set(compact('page', 'subpage', 'title_for_layout'));

		try {
			$this->render(implode('/', $path));
		} catch (MissingViewException $e) {
			if (Configure::read('debug')) {
				throw $e;
			}
			throw new NotFoundException();
		}
	}
	
	function testpayment() {
		//$result = $this->Paypal->change_subscription_status('I-HHPAMU1JJFRS','Suspend');
		$result = $this->Paypal->get_subscription_status('I-RN518V3BTGW1');
		pr($result);
		die;
	}
}
