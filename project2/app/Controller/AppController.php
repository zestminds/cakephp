<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');
require_once '../Vendor/autoload.php';
use Firebase\JWT\JWT;
/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {
	
	
	public $components = array(
	"Session",
	"Flash","Image",
	"Cookie",
	'Auth' => array(
			'loginAction' => array(
				'controller' => 'users',
				'action' => 'login',
			),
			'authError' => 'Did you really think you are allowed to see that?',
			'authenticate' => array(
				'Form' => array(
					'fields' => array(
					  'username' => 'username', //Default is 'username' in the userModel
					  'password' => 'password'  //Default is 'password' in the userModel
					),
					'scope' => array('User.is_active' => '1'),
					//'passwordHasher' => 'Blowfish'
				)
			)
		)
	);
	
	var $conditions = array();
	var $delarr = array();
	var $updatearr = array();
	public $mailBody = 'Fishook';
    public $from = 'ranjuzestmind@gmail.com';
    public $subject = 'Fishook';
    public $imagename = '';
    public $uploaddir =  '';
    public $jsArray = array();
    public $cssArray = array();
    
    
    
    public $apiResponse = '';
    /*
     * Below written arrays and variable will be used in calling APIs and check the component to be called
     *
     */

    

    /* Actions allowed without login */
    var $allowedActions = array("signup","login","forgot_password");
   
    /* Actions allowed without login */
    //~ public function initialize()
    //~ {
        //~ parent::initialize();
 //~ 
        //~ $this->loadComponent('RequestHandler');
        //~ $this->loadComponent('Auth', [
            //~ 'storage' => 'Memory',
            //~ 'authenticate' => [
                //~ 'Form' => [
                    //~ 'scope' => ['Users.group_id' => 1]
                //~ ],
                //~ 'ADmad/JwtAuth.Jwt' => [
                    //~ 'parameter' => 'token',
                    //~ 'userModel' => 'Users',
                    //~ 'fields' => [
                        //~ 'username' => 'id'
                    //~ ],
                    //~ 'queryDatasource' => true
                //~ ]
            //~ ],
            //~ 'unauthorizedRedirect' => false,
            //~ 'checkAuthIn' => 'Controller.initialize'
        //~ ]);
    //~ }
    function beforefilter() {	
	//print_r($_SERVER); die;
                $this->Auth->scope = array('User.is_active' =>1);
                /*if ( $_SERVER['REQUEST_SCHEME'] == 'http' ) {
                        $this->redirect("https://events.fishookapp.com".$_SERVER['REQUEST_URI']);
                }*/

                if ($this->params['action'] == "api") {
                        $this->Auth->allow();
                } else {
                        if ( $_SERVER['REQUEST_SCHEME'] == 'http' ) {
                               // $this->redirect("https://events.fishookapp.com".$_SERVER['REQUEST_URI']);
                                // $this->Auth->allow( 'google' );
                        }
                }
                $this->Auth->allow( 'google' );         

		
		if ($this->params['prefix']) {
			//die("here");
			if ( $this->Session->read("Auth.User.user_type_id") != 1 ) {				
				 $this->Flash->error(__("You are not authorized to view this page."), 'default', array("class"=>"error_message"));
				 $this->redirect("/");				
			}
			$this->layout = "admin";
		} else {
			if ( $this->request->is("ajax") ) {
				
			} else {
				if ( $this->Session->read("Auth.User.user_type_id") == 1 && ($this->params['action'] != "logout") ) {				
					 //$this->Flash->error(__("You are not authorized to view this page."), 'default', array("class"=>"error_message"));
					 $this->redirect("/dashboard");				
				}
				if ( !$this->Session->read("Auth.User.id") ) {
					$this->jsArray[] = array("jquery.validate.min","loginvalidate","fblogin","jquery.form.min","signupvalidate");
				} 
				else if ($this->Session->read("Auth.User.identifier") != '' ) {
					$this->jsArray[] = array("fblogin","jquery.validate.min");			
				}
				else
				{
					$this->jsArray[] = array("fblogin","jquery.validate.min");		
				}
				
				
				
				$this->layout = "admin";
			}
		}
		
	}
	
	function beforeRender() {
		$this->set("jsArray",$this->jsArray);
		$this->set("cssArray",$this->cssArray);
	}
	
	/*
     * @function name	: api
     * @purpose			: handle api calls for customer
     * @arguments		: below are the arguments to be passed for calling apis:
     * 						:- @version : version of api
     * 						:- @action	: action to be called
     * @return			: none
     * @created by		: shivam sharma
     * @created on		: 17th Nov 2017
     * @modified by		: Ranjana
     * @modified on		: 12 Dec 2015
     * @description		: NA
     */

    function api($version = NULL,$component = NULL , $action = NULL) {
		
        $skip_auth = in_array($action, $this->allowedActions) ? true : false;
        switch ($version) {
			case 'v2':
				$version = 'V2';
				break;
			case 'v3':
				$version = 'V3';
				break;
			case 'v4':
				$version = 'V4';
				break;
			default :
				$version = 'V1';
				break;
         }
		
		$componentAction = ucfirst($component);		
		$component = $this->Components->load($componentAction.$version);
		$component->initialize($this);
		$component->siteLink = SITE_LINK;		
		$component->variables = $this->request;		
		$headers = apache_request_headers();		
		$this->log($this->request);
		if (!empty($componentAction)) {
			if(in_array($action,$this->allowedActions))
			{
					$component->{$action}();	
					$this->apiResponse = $component->result;				
			}
			else{			
				if(isset($headers["Authorization"]) && !empty($headers["Authorization"]))
				{
					$token=explode(" ",$headers["Authorization"]);
					if(!empty($token))
					$jwt=$token[1];			
				}
				else
				{
					$jwt = isset($this->request->query['token']) ? $this->request->query['token'] : null;
					
				}
			
				if (isset($jwt)) {
					$key = JWT_SECRET;
					//$decoded = JWT::decode($jwt, $key, array('HS256'));
					try{
						$decoded = JWT::decode($jwt, $key, array('HS256'));
						$decoded_array = (array)$decoded;
						$user_id=$decoded_array['userid'];	
						$api_login=$decoded_array['api_login'];	
						$authUser=$this->authenticateUser($user_id,$api_login);
					
						if($authUser)
						{
							$component->userId = $user_id;
							//pr($component->variables);
							$component->{$action}();						
							//~ $exp=$decoded_array['exp'];
							
							//~ $expires_at = date('d M Y H:i', $decoded_array['exp']);
							//~ $time = strtotime($expires_at);
							//~ $time = $time - (15 * 60);
							//~ $date = date('d M Y H:i', $time);	
							//~ if(strtotime(date("Y-m-d H:i:s", time() - date("Z"))) > $time ) {
								//~ $api_login=date('d M Y H:i', $api_login);
								//~ $jwt=$this->genrateToken($user_id,$api_login);
								//~ $component->result["token"]=$jwt;
							//~ }
							//~ 
							$this->log($component->result);
							$this->apiResponse = $component->result;
						}
						else
						{
							$this->apiResponse = array("status"=>601,"message"=>"session has been expired");
						}
					}
					catch(\Firebase\JWT\ExpiredException $e){
						 $this->apiResponse = array("status"=>601,"message"=>"session has been expired");
						// return $this->apiResponse;
						// echo 'Caught exception: ',  $e->getMessage(), "\n";
					}
				}
				else
				{
					$this->apiResponse = array("status"=>201,"message"=>"Token not found.");
				}
			}				
			
			
		} else {
			$this->apiResponse = array("status"=>404,"message"=>"Service not found.");
		}
        $this->autoRender = false;
        $this->response->type('json');
        $this->response->body(json_encode($this->apiResponse));
    }
	
	function authenticateUser($userid,$apilogin){
		$apilogin=date("Y-m-d h:i:s",$apilogin);		
		$this->loadModel("User");
		if ( $user = $this->User->find("first",array("conditions"=>array("User.id"=>$userid,"api_login"=>$apilogin))) ){
			return true;
		}else
		{
			return false;
		}	
	}
	
	function checkLogin() {
		if ( $this->Session->read("Auth.User.id") ) {
			$user = $this->Session->read("Auth");
			if ( $user['User']['user_type_id'] == 1 ) {
				$this->redirect("/dashboard");
			} else {
				$this->redirect("/merchant");
			}
		} 
	}
	
	public function getUserDetail() {
		$id = $this->Session->read("Auth.User.id");
		$this->loadModel("User");
		$temp = $this->User->find("first",array("conditions"=>array("User.id"=>$id)));
		$this->Session->write("User",$temp);
	}
	
	function bulkactions($flag = false) {
		
		$controller = is_array($this->data)?array_keys($this->data):'';
		$statuskey  = '';
		$controller = isset($controller[0])?$controller[0]:'';
		$allowedarr = array("Account","User");
		if (isset($this->data[$controller]) && !empty($this->data[$controller]['options']) && !empty($controller) && !empty($this->data['id'])) {
			
			$str = "";
			if ( !empty($this->request->query) ) {
				foreach ( $this->request->query as $key=>$val ) {
					if (empty($str)) {
						$str = $key."=".$val;
					} else {
						$str .= "&".$key."=".$val;
					}
				}
			}
			$url = $this->params->url;
			if ( !empty($str) ) {
				$url .= "?".$str;
			}
			
			
			foreach ($this->data['id'] as $key=>$val) {
				if ($val > 0) {
					$this->delarr[]	= $key;
					if ($flag) {
						$statuskey = ($this->data[$controller]['options']);
						$this->updatearr[$controller][$key] = array("id"=>$key,"voucher_status"=>($this->data[$controller]['options'] == 'Active'?1:0));
					} else {
						$statuskey = ($this->data[$controller]['options'] == 'Active'?1:0);
						$this->updatearr[$controller][$key]	= array("id"=>$key,"is_active"=>($this->data[$controller]['options'] == 'Active'?1:0));
					}
				}
			}
			
			if (isset($this->data[$controller]['options']) && $this->data[$controller]['options'] == 'Delete') {
				//~ if($flag == 1){
					//~ //if($this->unlinkDB($this->delarr)){
						//~ $this->$controller->delete($this->delarr);
						//~ $this->redirect($this->referer());
					//~ //}
				//~ }
				//~ else{
					//~ if($this->$controller->delete($this->delarr)) {
						//~ $this->Session->setFlash(__('Record has been deleted successfully.'));
					//~ }
					//~ $this->redirect($this->referer());
				//~ }
				
				if($this->$controller->delete($this->delarr)) {
						$this->Session->setFlash(__('Record has been deleted successfully.'));
					}
					$this->redirect($this->referer());
				$statuskey = -1;
			} else {
				//pr($this->updatearr[$controller]);
				//die;
				//die;
				$this->$controller->create();
				if($this->$controller->saveAll($this->updatearr[$controller],array("validate"=>false))) {
					 $this->Session->setFlash(__('Record has been updated successfully.'));
				}
			
				$this->redirect($this->referer());
			}
			if (empty($this->data['Admin']['searchval'])) {
				$this->data = array();
			}
			$this->redirect($url);
		}
		if (in_array($controller,$allowedarr) && $statuskey > -1) {
			$arr['keys'] 	= $this->delarr;
			$arr['status']  = $statuskey;
			return $arr; 
		}
		if ($flag) {
			$arr['keys'] = $this->delarr;
			$arr['status']  = $statuskey;
			return $arr; 
		}
		
		/* end of code to change status and delete by checking data from page */
	}
	
	
	/*
     * @function name	: getmaildata
     * @purpose			: getting email data for various purposes
     * @arguments		: Following are the arguments to be passed:
     * id				: id of email templates from cmsemail table
     * @return			: NONE
     * @created on		: 
     * @description		: function will assign value to global variables like mailbody,from, subject which will be used while sending email
     */

    function getMailData($mail_slug = null, $to = null) {
        $this->loadModel('EmailTemplate');
        $cmsemail = $this->EmailTemplate->find('first', array('conditions' => array('EmailTemplate.slug' => $mail_slug)));       
        if (!empty($cmsemail)) {
            $this->mailBody = $cmsemail['EmailTemplate']['content'];
            $this->from = $cmsemail['EmailTemplate']['email_from'];
            $this->subject = str_replace("{TO}", $to, $cmsemail['EmailTemplate']['subject']);
        }        
    }
    
    
    /* end of function */
    /*
     * @function name	: sendmail
     * @purpose			: sending email for various actions
     * @arguments		: Following are the arguments to be passed:
     * from		: contain email address from which email is sending
     * Subject	: Subject of Email
     * to		: Email address to whom the email is sending
     * body		: content of email
     * template : if defining a html template for sending email else false.
     * values	: to be given in email template like username etc.
     * @return			: true if email sending successfull else return false.
     * @created on		: 11th March 2014
     * @description		: NA
     */

    function sendMail($to, $template = 'email', $fromname = 'Rave') {
		//pr($this->mailBody);die;
        App::uses('CakeEmail', 'Network/Email');
       // if (isset($this->params->base) && !empty($this->params->base)) {
            //$email = new CakeEmail("gmail");
        //} else {
           $email = new CakeEmail("zestminds");
        //}
        //Use filter_var_array for multiple emails
        $this->from = "ranjuzestmind@gmail.com";
        $is_valid = is_array($to) ? filter_var_array($to, FILTER_VALIDATE_EMAIL) : filter_var($to, FILTER_VALIDATE_EMAIL);
        if ($is_valid) {
            $email->from(array($this->from => $fromname));
            $email->to($to);
            $email->subject($this->subject);
            $headers[] = 'MIME-Version: 1.0';
            $headers[] = 'Content-type: text/html; charset=iso-8859-1';
            $email->addHeaders($headers);
            $email->emailFormat('both');
            if (empty($template)) {
				try {
					if ( !$email->send($this->mailBody)) {
						throw new Exception;
                    } else {
						return true;
                    }
                } catch (Exception $e) {
                    return false;
                }
            } else {
                if (!empty($this->mailBody)) {
                    $email->viewVars(array("mail" => $this->mailBody));
                }
                $email->template($template, '');
                try {
                    if (!$email->send()) {
						throw new Exception;
                    } else {
						return true;
                    }
                    
                } catch (Exception $e) {
					echo $e->getMessage();
					die;
                    return false;
                } 
            }
        } else {
            return false;
        }
    }

    /* end of function */
    
    function encryptpass($password, $salt = '', $method = 'md5', $crop = true, $start = 4, $end = 10) {
		$salt = strtotime(date("Y-m-d h:i:s"));
		if ($crop) {
			$password = $method(substr($method($password.$salt), $start, $end));
		} else {
			$password = $method($password.$salt);
		}
		return $password;
    }
    
    function checkRemember() {
		$cookie = $this->Cookie->read("rememberme");	
		
		if ( !empty($cookie) ) {
			$this->loadModel("User");
			if ( $tmpUser = $this->User->find("first",array("conditions"=>array("remember_token"=>$cookie),"recursive"=>0)) ) {
				//pr($tmpUser); die;
				$this->request->data['User']['remember_token'] = $cookie;
				$this->components['Auth']['authenticate']['Form']['fields'] = array("remember_token"=>"remember_token");
				if ( $this->Auth->login($this->request->data['User']) ) {
					$this->Session->write("Auth",$tmpUser);
				} 
			}
		}
	}
	
	
	function uploadImage($file , $destination = NULL, $old_img = false,$first = NULL,$second=NULL, $filetypes = array('jpg', 'jpeg', 'png')) {
		$flag = false;
		$file_ext = explode(".",$file['name']);
		$file_ext = strtolower(end($file_ext));
		$this->imagename = $this->uploaddir =  '';
		if ( in_array($file_ext,$filetypes) ) {
			
			$this->uploaddir = WWW_ROOT."img/".$destination."/";
			
			if ( !empty($destination) && !is_dir($this->uploaddir) ) {
				mkdir($this->uploaddir,0777,true);
			}
			//$this->imagename = $file['name'];
			$this->imagename = mt_rand().strtotime(date("y-m-d h:i:s")).".".$file_ext;
			if ( move_uploaded_file($file['tmp_name'],$this->uploaddir.$this->imagename) ) {
				 if($old_img) {
					if ( !empty($first) && file_exists($this->uploaddir.$first) ) {					
						@unlink($this->uploaddir.$first);
					}
					if ( !empty($second) && file_exists($this->uploaddir.$second) ) {				
						@unlink($this->uploaddir.$second);					
					}
				}
				$flag = true;
			} 
		} else {			
			$flag = false;
		}
		return $flag; 
	}
	
	
	function genImage($data, $destination = NULL,$id=null) {
		if (preg_match('/^data:image\/(\w+);base64,/', $data, $type)) {
			$data = substr($data, strpos($data, ',') + 1);
			$type = strtolower($type[1]); // jpg, png, gif

			if (!in_array($type, [ 'jpg', 'jpeg', 'gif', 'png' ])) {
				throw new \Exception('invalid image type');
			}

			$data = base64_decode($data);

			if ($data === false) {
				throw new \Exception('base64_decode failed');
			}
			
			$this->imagename = $this->uploaddir =  '';
			$this->uploaddir = WWW_ROOT."img/".$destination."/";
			if ( !empty($destination) && !is_dir($this->uploaddir) ) {
				mkdir($this->uploaddir,0777,true);
			}
			$this->imagename ="fishookevent".$id.".".$type;
			//$this->imagename = mt_rand().strtotime(date("y-m-d h:i:s")).".".$type;
			$this->uploaddir.$this->imagename;
			
			if ( file_put_contents($this->uploaddir.$this->imagename, $data)) {
				//die("1");
				return true;
			} else {
				//die("10");
				return false;
			}
			
		} else {
			throw new \Exception('did not match data URI with image data');
		}
		
		
	}
	function genImage1($data, $destination = NULL) {
		if (preg_match('/^data:image\/(\w+);base64,/', $data, $type)) {
			$data = substr($data, strpos($data, ',') + 1);
			$type = strtolower($type[1]); // jpg, png, gif

			if (!in_array($type, [ 'jpg', 'jpeg', 'gif', 'png' ])) {
				throw new \Exception('invalid image type');
			}

			$data = base64_decode($data);

			if ($data === false) {
				throw new \Exception('base64_decode failed');
			}
			
			$this->imagename = $this->uploaddir =  '';
			$this->uploaddir = WWW_ROOT."img/".$destination."/";
			if ( !empty($destination) && !is_dir($this->uploaddir) ) {
				mkdir($this->uploaddir,0777,true);
			}
			$this->imagename = mt_rand().strtotime(date("y-m-d h:i:s")).".".$type;
			$this->uploaddir.$this->imagename;
			
			if ( file_put_contents($this->uploaddir.$this->imagename, $data)) {
				//die("1");
				return true;
			} else {
				//die("10");
				return false;
			}
			
		} else {
			throw new \Exception('did not match data URI with image data');
		}
		
		
	}
	
public function verifyRecatpcha($aData)
 {
    $recaptcha_secret = SECRET_KEY;
    $url = "https://www.google.com/recaptcha/api/siteverify?secret=".$recaptcha_secret."&response=".$aData['g-recaptcha-response']; 
    $response = json_decode(@file_get_contents($url));          
                
    if($response->success)
    {
        return true;
    }
    else
    {
        return false;
         
    }   
 } 
function genrateToken($userid,$api_login)
	{
		$issuedAt = time();
		$api_login=strtotime($api_login);
		$expirationTime = $issuedAt + 63072000;  // jwt valid for 60 seconds from the issued time
		
		$payload = array(
			'api_login' => $api_login,
			'userid' => $userid,				
			'iat' => $issuedAt//,
			//'exp' => $expirationTime
		);
		$key = JWT_SECRET;
		$alg = 'HS256';
		$jwt = JWT::encode($payload, $key, $alg);			
		return $jwt;
	}
}

