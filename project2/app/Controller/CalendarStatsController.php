<?php
App::uses('AppController', 'Controller');

/**
 * VoucherSales Controller
 *
 * @property VoucherSale $VoucherSale
 * @property PaginatorComponent $Paginator
 */
class CalendarStatsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $uses = array();
	public $components = array('Paginator');
	
   
/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->layout = "mooladesignmyvoucher";	
		$this->jsArray[] = array("calendar_insights");
		$query = $this->request->query;
		
		
		if( !isset($query['interval']) || empty($query['interval']) ) {
			$interval ="7 Day	";			
		} else {	
			$interval=$query['interval'];
		}		
		
		if ( !isset($query['type']) || empty($query['type']) ) {
			//$conditions = array_merge($conditions,array("VoucherStat.stat_type" => 'calendar'));
			$voucher_status='calendar';
		}	 else {
			//$conditions = array_merge($conditions,array("VoucherStat.stat_type" => $query['type']));
			$voucher_status=$query['type'];
		}	
		
		
		$this->loadModel("SaveCalendar");
		$this->SaveCalendar->hasOne = $this->SaveCalendar->belongsTo = $this->SaveCalendar->hasMany = array();
		$this->SaveCalendar->belongsTo = array(
			"Calendar"=>array(
				"className"=>"Calendar",
				"foreignKey"=>false,
				"type"=>"Inner",
				"conditions"=>"Calendar.id=SaveCalendar.event_id"
			)
		);
			
		$this->set("voucher_status",$voucher_status);
		$this->set(compact("interval"));
		$this->SaveCalendar->virtualFields = array("count"=>"select count(*) from save_calendars where save_calendars.type='".$voucher_status."' and event_id = Calendar.id  and save_calendars.created >=(CURDATE() - INTERVAL ".$interval.")");

		$this->paginate = array(
			'conditions' => array(
				"Calendar.user_id"=> $this->Session->read('Auth.User.id'),"SaveCalendar.type"=>$voucher_status,"SaveCalendar.created >=(CURDATE() - INTERVAL ".$interval.")"
			),
			"fields"=>array("Calendar.image","SaveCalendar.count"),
			"order"=>"SaveCalendar.created desc","limit"=>"2","group"=>"SaveCalendar.event_id"
		);
		
		if( $this->request->is("ajax")) {
			$this->layout = false;
			$this->render = false;
			$tmpData = array();	
			$data= $this->paginate("SaveCalendar");	
			//pr($data);	
			
			foreach( $data as $key=>$val  ) {
				($val['Calendar']['image']=="" || !file_exists(WWW_ROOT."img/events/".$val['Calendar']['image'])) ? $imgevent=SITE_LINK."img/default_voucher.jpeg" : $imgevent=SITE_LINK."img/events/".$val['Calendar']['image'];
				$tmpData[]= array("image"=>$imgevent,"count"=>$val['SaveCalendar']['count']);	
			}				
			echo json_encode($tmpData);	
			die;	
		}
		else
		{
			$this->set('calendar', $this->paginate("SaveCalendar"));
		}
	}
	
	
	//Admin event manager Detail View 
		public function admin_filter_manager($type = null) {
		if( $this->request->is("ajax")) {
			$this->layout = false;
			$this->render = false;
			$type="calendar";
			$to=date("Y-m-d");
			$from=date("Y-m-d");
			$id=$this->request->query["id"];
			if(isset($this->request->query["type"]) || !empty($this->request->query["type"]))
			{
				$type=$this->request->query["type"];
			}
			if(isset($this->request->query["to"]) || !empty($this->request->query["to"]))
			{
				$to=$this->request->query["to"];
			}
			if(isset($this->request->query["from"]) || !empty($this->request->query["from"]))
			{
				$from=$this->request->query["from"];
			}
			
			$this->loadModel("SaveCalendar");
			$this->SaveCalendar->hasOne = $this->SaveCalendar->belongsTo = $this->SaveCalendar->hasMany = array();
			$this->SaveCalendar->belongsTo = array(
				"Calendar"=>array(
					"className"=>"Calendar",
					"foreignKey"=>false,
					"type"=>"Inner",
					"conditions"=>"Calendar.id=SaveCalendar.event_id"
				)
			);			
			$this->conditions=array("Calendar.user_id"=>$id,"SaveCalendar.type"=>$type,"date(SaveCalendar.created) BETWEEN '".$from."' AND '".$to."'");
			$list = $this->SaveCalendar->find('all',array("conditions"=>$this->conditions,"fields"=>'id'));
			$count=count($list);	
			$msg="success";			
	  }
	  else
	  {
		 $msg="error";	
	  }
	  echo json_encode(array("msg"=>$msg,"count"=>$count));	
	  die;
  }
	
	
		//Admin Insights Students
	function admin_student_insights ($days=7) {	
		
			$this->layout = "mooladesignadmin";	
			$this->jsArray = "admin_insights";				
			$this->loadModel('User');								
			$startDate=date("Y")."-01-01";			
			$todayDate=date("Y-m-d");		
		
			
			$days_regver=$days_appdown=$days_reachcon="7";
			
			
			
			$this->set('days_regver',$days_regver);			
			$this->set('days_appdown',$days_appdown);			
			$this->set('days_reachcon',$days_reachcon);
			
			$this->set('show_regver',"weeks");
			$this->set('show_reachconv',"weeks");
			
			$this->set('records', array('7' => '7 Days','30' => '30 Days', '90' => '3 Months', '6 Months' => '6 Months','1 Year' => '1 Year'));
		
	}	
	// Start  Total App Downloads Students
	public function admin_total_app_downloads()
	{	
			$this->layout = false;				
			$this->loadModel('User');
									
			$startDate=date("Y")."-01-01";			
			$todayDate=date("Y-m-d");			
				
			if($this->request->is("get") ) {	
								
				if(isset($this->request->query["days"]) && !empty($this->request->query["days"])){
										
					$days_appdown=$this->request->query["days"];					
					if($days_appdown=="7" || $days_appdown=="30" || $days_appdown=="90")
					{			
						$total_app_downloads=$this->total_app_downloads_days($days_appdown);	
					}
					else
					{						
						$total_app_downloads=$this->total_app_downloads_months($days_appdown);	
					}								
				}
				
			}
		
			//total app downloads and login				
			$this->set('total_app_downloads',$total_app_downloads);			
			$this->set('days_appdown',$days_appdown);
	
	}
	public function total_app_downloads_days($days)
	{
		
		$conditions=array("User.user_type_id"=>"3","User.created >=(CURDATE() - INTERVAL ".$days." Day)");
		
		$this->User->virtualFields =array("startDate"=>" (CURDATE() - INTERVAL ".$days." Day)");
		$appdownloads = $this->User->find("all",array("conditions"=>$conditions,"fields"=>array("count(User.id) as cnt",'DAYNAME(User.created) as "dayname"',"date(User.created) as date","startDate"),"group"=>"date(User.created)","order"=>"User.created asc"));
		
		$startDate = (isset($appdownloads[0]['User']['startDate']))?$appdownloads[0]['User']['startDate']:'';
		$dayArr = array();
		foreach($appdownloads as $key=>$val){			
			$dayArr[$val[0]['date']] = $val[0]['cnt'];		
		}
		$finalArr = array();
		for ( $i = 0; $i<=$days; $i++ ) {
			
			$tmpDate = date("Y-m-d",strtotime($startDate." +".$i." days"));
			if($days==7){
				$tmpDay = date("D",strtotime($startDate." +".$i." days"));
				$finalArr[$tmpDay] = isset($dayArr[$tmpDate])?$dayArr[$tmpDate]:0;
			}
			else
			{	
				$tempshortdate=strtotime($tmpDate);
				$month=date("M j",$tempshortdate);			
				$finalArr[$month] = isset($dayArr[$tmpDate])?$dayArr[$tmpDate]:0;
			}
		}	
			return 	$finalArr;
			
	}	
	public function total_app_downloads_months($days)
	{
		if($days=="6 Months")
		{
			$startmonth = date('Y-m-01 00:00:00', strtotime("-5 month"));
			$group=array("monthname");
		}
		else{
			$startmonth = date('Y-m-01 00:00:00', strtotime("-11 month"));
			$group=array("year","monthname");			
		}
		
		$endmonth = date('Y-m-d 00:00:00');
		$endmonthDate = strtotime(date('Y-m-01 00:00:00'));
		$tmpDate = $startmonth;
		
		$conditions=array("User.user_type_id"=>3,"date(User.created) BETWEEN date('".$startmonth."') AND date('".$endmonth."')");	
						
		
		$this->User->virtualFields =array("firstDate"=>"DATE_FORMAT(User.created,'%Y-%m-01 00:00:00')");		
		$appdownloads = $this->User->find("all",array("conditions"=>$conditions,"fields"=>array("count(User.id) as cnt",'MONTHNAME(User.created) as "monthname"','Year(User.created) as year',"date(User.created) as date","firstDate"),"group"=>$group,"order"=>"User.created asc"));
		
		$tmpData = array();
		foreach($appdownloads as $key=>$val) {
			$tmpData[$val['User']['firstDate']] = $val[0];
		}
		
		$data = array();
		$i = 0;
		$finalData = array();
		while ( $endmonthDate >= $tmpDate ) {
			if ( empty($i) ) {
				$tmpDate;
			} else {
				$tmpDate = date("Y-m-d 00:00:00",$tmpDate);
			}
			$month = date("F",strtotime($tmpDate));
			if ( isset($tmpData[$tmpDate]) ) {
				$finalData[$tmpData[$tmpDate]['monthname']] = $tmpData[$tmpDate]['cnt'];
			} else {
				$finalData[$month] = 0;
			}
			$tmpDate = strtotime($tmpDate." +1 Month");
			$i++;
		}
		return $finalData;
			
	}	
	// End  Total App Downloads Students
	
	
	//Strat Facebook V/s Manual
	
	public function admin_total_registrations()
	{
		$this->layout = false;				
		$this->loadModel('User');										
		$startDate=date("Y")."-01-01";			
		$todayDate=date("Y-m-d");		
		$todayDate=date("Y-m-d");
		if($this->request->is("get") ) {
			if($this->request->query["days"])
			{				
				if(isset($this->request->query["days"]) && !empty($this->request->query["days"])){
								
					$days=$this->request->query["days"];		
					if($days=="7" || $days=="30" || $days=="90")
					{					
						$total_app_facebook=$this->total_app_facebook_days($days,"Facebook");	
						$total_app_manual=$this->total_app_facebook_days($days,"Manual");	
					}
					else
					{							
						$total_app_facebook=$this->total_app_facebook_months($days,"Facebook");
						$total_app_manual=$this->total_app_facebook_months($days,"Manual");
					}
				}
			}
		}
		
		$this->set('total_app_facebook',$total_app_facebook);
		$this->set('total_app_manual',$total_app_manual);	
		$this->set('days_regver',$days);
		
	}	
	
	public function total_app_facebook_days($days,$identifier)
	{	$this->loadModel('User');
		$conditions=array("User.user_type_id"=>3,"User.created >=(CURDATE() - INTERVAL ".$days." Day)");
		if($identifier=="Facebook"){
			
			$conditions=array_merge($conditions,array('not' => array('User.identifier' => null)));
			
		}
		else{			
				$conditions=array_merge($conditions,array('User.identifier' => null));			
		}
				
		$this->User->virtualFields =array("startDate"=>" (CURDATE() - INTERVAL ".$days." Day)");
		$appdownloads = $this->User->find("all",array("conditions"=>$conditions,"fields"=>array("count(User.id) as cnt",'DAYNAME(User.created) as "dayname"',"date(User.created) as date","startDate",'User.identifier'),"group"=>"date(User.created)","order"=>"User.created asc"));		
		//pr($appdownloads);
		$startDate = (isset($appdownloads[0]['User']['startDate']))?$appdownloads[0]['User']['startDate']:''; 
		$dayArr = array();
		foreach($appdownloads as $key=>$val){			
			$dayArr[$val[0]['date']] = $val[0]['cnt'];		
		}
		$finalArr = array();
		for ( $i = 0; $i<=$days; $i++ ) {
			$tmpDate = date("Y-m-d",strtotime($startDate." +".$i." days"));
			if($days==7){
				$tmpDay = date("D",strtotime($startDate." +".$i." days"));
			$finalArr[$tmpDay] = isset($dayArr[$tmpDate])?$dayArr[$tmpDate]:0;
			}else{	
				$tempshortdate=strtotime($tmpDate);	
				$month=date("M j",$tempshortdate);	
				$finalArr[$month] = isset($dayArr[$tmpDate])?$dayArr[$tmpDate]:0;
			}			
		}
		return $finalArr;
	}	
	
	
	public function total_app_facebook_months($days,$identifier)
	{	
		if($days=="6 Months")
		{
			$startmonth = date('Y-m-01 00:00:00', strtotime("-5 month"));
			$group=array("monthname");
		}
		else{
			$startmonth = date('Y-m-01 00:00:00', strtotime("-11 month"));
			$group=array("year","monthname");			
		}
		
		$endmonth = date('Y-m-d 00:00:00');
		$endmonthDate = strtotime(date('Y-m-01 00:00:00'));
		$tmpDate = $startmonth;
		
		$conditions=array("User.user_type_id"=>3,"date(User.created) BETWEEN date('".$startmonth."') AND date('".$endmonth."')");
		
		if($identifier=="Facebook"){			
				 $conditions=array_merge($conditions,array('not' => array('User.identifier' => null)));			
		}
		else{			
				$conditions=array_merge($conditions,array('User.identifier' => null));
		}
		
		$this->User->virtualFields =array("firstDate"=>"DATE_FORMAT(User.created,'%Y-%m-01 00:00:00')");
		$appdownloads = $this->User->find("all",array("conditions"=>$conditions,"fields"=>array("count(User.id) as cnt",'MONTHNAME(User.created) as "monthname"','Year(User.created) as year',"date(User.created) as date","firstDate"),"group"=>$group,"order"=>"User.created asc"));		
		
		$tmpData = array();
		foreach($appdownloads as $key=>$val) {
			$tmpData[$val['User']['firstDate']] = $val[0];
		}
		
		$data = array();
		$i = 0;
		$finalData = array();
		while ( $endmonthDate >= $tmpDate ) {
			if ( empty($i) ) {
				$tmpDate;
			} else {
				$tmpDate = date("Y-m-d 00:00:00",$tmpDate);
			}
			$month = date("F",strtotime($tmpDate));
			if ( isset($tmpData[$tmpDate]) ) {
				$finalData[$tmpData[$tmpDate]['monthname']] = $tmpData[$tmpDate]['cnt'];
			} else {
				$finalData[$month] = 0;
			}
			$tmpDate = strtotime($tmpDate." +1 Month");
			$i++;
		}
		return $finalData;
	}	
	
	
	//End Facebook V/s Manual
	
	//Start Total Verifications: Verified v/s Non-Verified
	public function admin_total_verifications()
	{		
		$this->layout = false;				
		$this->loadModel('User');								
		$startDate=date("Y")."-01-01";			
		$todayDate=date("Y-m-d");		
		$todayDate=date("Y-m-d");
		if($this->request->is("get") ) {
			if($this->request->query["days"])
			{				
				if(isset($this->request->query["days"]) && !empty($this->request->query["days"])){
										
					$days=$this->request->query["days"];		
					if($days=="7" || $days=="30" || $days=="90")
					{					
						$total_verified=$this->total_verified_days($days,"verified");	
						$total_nonverified=$this->total_verified_days($days,"nonverified");	
					}
					else{							
						$total_verified=$this->total_verified_months($days,"verified");	
						$total_nonverified=$this->total_verified_months($days,"nonverified");	
					}
				}
			}
		}
		
		$this->set('total_verified',$total_verified);
		$this->set('total_nonverified',$total_nonverified);		
		$this->set('days_regver',$days);
	}
	public function total_verified_days($days,$identifier)
	{	
		$conditions=array("User.user_type_id"=>3,"User.created >=(CURDATE() - INTERVAL ".$days." Day)");
				
		if($identifier=="verified"){
			
				 $conditions=array_merge($conditions,array('User.verified' =>1));			
		}
		else{			
				$conditions=array_merge($conditions,array('User.verified' =>0));			
		}
		
		$this->User->virtualFields =array("startDate"=>" (CURDATE() - INTERVAL ".$days." Day)");
		$appdownloads = $this->User->find("all",array("conditions"=>$conditions,"fields"=>array("count(User.id) as cnt",'DAYNAME(User.created) as "dayname"',"date(User.created) as date","startDate",'User.identifier'),"group"=>"date(User.created)","order"=>"User.created asc"));		
		$startDate = (isset($appdownloads[0]['User']['startDate']))?$appdownloads[0]['User']['startDate']:''; 
	
		$dayArr = array();
		foreach($appdownloads as $key=>$val){			
			$dayArr[$val[0]['date']] = $val[0]['cnt'];		
		}
		$finalArr = array();
		for ( $i = 0; $i<=$days; $i++ ) {
			$tmpDate = date("Y-m-d",strtotime($startDate." +".$i." days"));
			if($days==7){
			$tmpDay = date("D",strtotime($startDate." +".$i." days"));
			$finalArr[$tmpDay] = isset($dayArr[$tmpDate])?$dayArr[$tmpDate]:0;
			}else{
				$tempshortdate=strtotime($tmpDate);
				$month=date("M j",$tempshortdate);
				$finalArr[$month] = isset($dayArr[$tmpDate])?$dayArr[$tmpDate]:0;
			}
		}		
		return $finalArr;
	}	
	
	public function total_verified_months($days,$identifier)
	{	
		
		if($days=="6 Months")
		{
			$startmonth = date('Y-m-01 00:00:00', strtotime("-5 month"));
			$group=array("monthname");
		}
		else{
			$startmonth = date('Y-m-01 00:00:00', strtotime("-11 month"));
			$group=array("year","monthname");			
		}
		
		$endmonth = date('Y-m-d 00:00:00');
		$endmonthDate = strtotime(date('Y-m-01 00:00:00'));
		$tmpDate = $startmonth;
			
		$conditions=array("User.user_type_id"=>3,"date(User.created) BETWEEN date('".$startmonth."') AND date('".$endmonth."')");
				
		if($identifier=="verified"){
			
				 $conditions=array_merge($conditions,array('User.is_active' =>1));			
		}
		else{			
				$conditions=array_merge($conditions,array('User.is_active' =>0));			
		}
		
		$this->User->virtualFields =array("firstDate"=>"DATE_FORMAT(User.created,'%Y-%m-01 00:00:00')");
		$appdownloads = $this->User->find("all",array("conditions"=>$conditions,"fields"=>array("count(User.id) as cnt",'MONTHNAME(User.created) as "monthname"','Year(User.created) as year',"date(User.created) as date","firstDate"),"group"=>$group,"order"=>"User.created asc"));		
		
		$tmpData = array();
		foreach($appdownloads as $key=>$val) {
			$tmpData[$val['User']['firstDate']] = $val[0];
		}
		
		$data = array();
		$i = 0;
		$finalData = array();
		while ( $endmonthDate >= $tmpDate ) {
			if ( empty($i) ) {
				$tmpDate;
			} else {
				$tmpDate = date("Y-m-d 00:00:00",$tmpDate);
			}
			$month = date("F",strtotime($tmpDate));
			if ( isset($tmpData[$tmpDate]) ) {
				$finalData[$tmpData[$tmpDate]['monthname']] = $tmpData[$tmpDate]['cnt'];
			} else {
				$finalData[$month] = 0;
			}
			$tmpDate = strtotime($tmpDate." +1 Month");
			$i++;
		}
		return $finalData;
	}	
	//End Student Total Verifications: Verified v/s Non-Verified
	
	// Start Student Reach Impresions Conversions
	
	public function admin_total_reachconversions()
	{
		$this->layout = false;				
		$this->loadModel('User');
		$this->loadModel('University');
		$this->loadModel('UserUniversity');								
		$startDate=date("Y")."-01-01";			
		$todayDate=date("Y-m-d");
		$todayDate=date("Y-m-d");
		
		if($this->request->is("get") ) {
			if($this->request->query["days"])
			{				
				if(isset($this->request->query["days"]) && !empty($this->request->query["days"])){
					if($this->request->query["university"]=="")
					$university="All";
					else
					$university=$this->request->query["university"];						
					$days=$this->request->query["days"];	
						if($days=="7" || $days=="30" || $days=="90")
						{	
							$total_reach=$this->total_reach_conversions_days($days,$university,"Reach");	
							$total_impresions=$this->total_reach_conversions_days($days,$university,"Convert");
							$total_conversions=$this->total_reach_conversions_days($days,$university,"Impression");						
						}
						else
						{						
							$total_reach=$this->total_reach_conversions_months($days,$university,"Reach");	
							$total_impresions=$this->total_reach_conversions_months($days,$university,"Convert");
							$total_conversions=$this->total_reach_conversions_months($days,$university,"Impression");
						}
				}
			}
		}	
		$this->set('total_reach',$total_reach);
		$this->set('total_impresions',$total_impresions);
		$this->set('total_conversions',$total_conversions);	
	
	}

	
	public function total_reach_conversions_days($days,$university,$status)
	{
		//echo $days;echo $university;die;
		$conditions=array("VoucherStat.stat_type"=>$status,"VoucherStat.created >=(CURDATE() - INTERVAL ".$days." Day)");
		$this->VoucherStat->belongsTo = $this->VoucherStat->hasMany = $this->VoucherStat->hasOne = array();
		$this->UserUniversity->belongsTo = array(
			 "VoucherStat"=> array(
				"className" => "VoucherStat",
				"foreignKey" => false,
				"type" => "Inner",
				"conditions" => "VoucherStat.user_id = UserUniversity.user_id"
			)
		);
		if($university!="" && $university!="All")
		{
			$conditions=array_merge(array("UserUniversity.university_id"=>$university),$conditions);
		}				
		$this->UserUniversity->virtualFields =array("startDate"=>" (CURDATE() - INTERVAL ".$days." Day)");
		$appdownloads = $this->UserUniversity->find("all",array("conditions"=>$conditions,"fields"=>array("count(VoucherStat.id) as cnt",'DAYNAME(VoucherStat.created) as "dayname"',"date(VoucherStat.created) as date","startDate"),"group"=>"date(VoucherStat.created)","order"=>"VoucherStat.created asc"));	
				
		$startDate = (isset($appdownloads[0]['UserUniversity']['startDate']))?$appdownloads[0]['UserUniversity']['startDate']:''; 
		$dayArr = array();
		foreach($appdownloads as $key=>$val){			
			$dayArr[$val[0]['date']] = $val[0]['cnt'];		
		}
		$finalArr = array();
		for ( $i = 0; $i<=$days; $i++ ) {
			$tmpDate = date("Y-m-d",strtotime($startDate." +".$i." days"));
			if($days==7){
				$tmpDay = date("D",strtotime($startDate." +".$i." days"));
				$finalArr[$tmpDay] = isset($dayArr[$tmpDate])?$dayArr[$tmpDate]:0;
			}
			else{
				$tempshortdate=strtotime($tmpDate);
				$month=date("M j",$tempshortdate);
				$finalArr[$month] = isset($dayArr[$tmpDate])?$dayArr[$tmpDate]:0;
			}
		}
		
		return $finalArr;
	}
	
	public function total_reach_conversions_months($days,$university,$status)
	{
		$this->VoucherStat->belongsTo = $this->VoucherStat->hasMany = $this->VoucherStat->hasOne = array();
		$this->UserUniversity->belongsTo = array(
			 "VoucherStat"=> array(
				"className" => "VoucherStat",
				"foreignKey" => false,
				"type" => "Inner",
				"conditions" => "VoucherStat.user_id = UserUniversity.user_id"
			)
		);
		
		if($days=="6 Months")
		{
			$startmonth = date('Y-m-01 00:00:00', strtotime("-5 month"));
			$group=array("monthname");
		}
		else{
			$startmonth = date('Y-m-01 00:00:00', strtotime("-11 month"));
			$group=array("year","monthname");			
		}
		
		$endmonth = date('Y-m-d 00:00:00');
		$endmonthDate = strtotime(date('Y-m-01 00:00:00'));
		$tmpDate = $startmonth;
		
		$conditions=array("VoucherStat.stat_type"=>$status,"date(VoucherStat.created) BETWEEN date('".$startmonth."') AND date('".$endmonth."')");
		
		if($university!="" && $university!="All")
		{
			$conditions=array_merge(array("UserUniversity.university_id"=>$university),$conditions);
		}				
		$this->UserUniversity->virtualFields =array("firstDate"=>"DATE_FORMAT(VoucherStat.created,'%Y-%m-01 00:00:00')");
		
		$appdownloads = $this->UserUniversity->find("all",array("conditions"=>$conditions,"fields"=>array("count(VoucherStat.id) as cnt",'MONTHNAME(VoucherStat.created) as "monthname"','Year(VoucherStat.created) as year',"date(VoucherStat.created) as date","firstDate"),"group"=>$group,"order"=>"VoucherStat.created asc"));	

		$tmpData = array();
		foreach($appdownloads as $key=>$val) {
			$tmpData[$val['UserUniversity']['firstDate']] = $val[0];
		}
		
		$data = array();
		$i = 0;
		$finalData = array();
		while ( $endmonthDate >= $tmpDate ) {
			if ( empty($i) ) {
				$tmpDate;
			} else {
				$tmpDate = date("Y-m-d 00:00:00",$tmpDate);
			}
			$month = date("F",strtotime($tmpDate));
			if ( isset($tmpData[$tmpDate]) ) {
				$finalData[$tmpData[$tmpDate]['monthname']] = $tmpData[$tmpDate]['cnt'];
			} else {
				$finalData[$month] = 0;
			}
			$tmpDate = strtotime($tmpDate." +1 Month");
			$i++;
		}
		return $finalData;
	}
	
	// End Student Reach Impresions Conversions
	
	
	
		
}
