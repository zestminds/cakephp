<script>
	$(document).ready(function () {
    $('#UserAdminLoginForm').validate({ // initialize the plugin
        rules: {
			 
			  "data[User][username]": {
				required: true,
				email : true
			  },
			 "data[User][password]": {
				required: true			
			  },
			  "login_hiddenRecaptcha": { required: true, }			
			},
			// Specify validation error messages
			messages: {			
				"data[User][password]": {
					required: "Please enter a password"			
				},			 
			  "data[User][username]": {
				required: "Please enter an email",
				email : "Please enter valid email"			
			  },
			  "login_hiddenRecaptcha": {
				required: "Invalid captcha"					
			  }
			},
			// Make sure the form is submitted to the destination defined
			// in the "action" attribute of the form when valid
			submitHandler: function(form) {		
				$('#error_msg').hide();
				$("#error_msg_captcha").hide();
				//if ($("#login_hiddenRecaptcha").val().trim() == "") {					
					//$('#error_msg').hide();
					//$("#error_msg_captcha").show();
					//return false;
				//}
					
			   $('#UserAdminLoginForm').ajaxSubmit(function(response) { 			
					 $('#error_msg').empty();		
					response = JSON.parse(response);
					if ( response.status ) {
						if(response.type==1)
						location.href = SITE_LINK+"/"+response.url;
						else
						location.href = SITE_LINK+"/logout";
					} else {						
						  $('#error_msg').text(response.message);
						 $("#error_msg_captcha").hide();
						 $('#error_msg').show();
						  //grecaptcha.reset(widgetId1);
						 // $('#login_hiddenRecaptcha').val('');
						return false;
					}
				});
			}
		  });		
});
</script>
  <div class="login-content">
         <div id="resizeMe" class="login-page">
             <div class="container">
                 <div class="login-box">
<!--
                     <div class="log-logo">
                            <a href="#"><?php //echo $this->Html->image("admin-logo.png",array("height"=>"53px","width"=>"173px")); ?></a>
                     </div>
-->
                     <h4>Welcome</h4>
                     <p>Sign in to start your session</p>
                     <div class="alert alert-warning" id="error_msg" style="display:none;"></div>
					<div class="alert alert-danger" id="error_msg_captcha" style="display:none;"><strong>Attention!</strong> Please check captcha to proceed.</div>
                     <?php echo $this->Form->create("User",array("url"=>"/login","id"=>"UserAdminLoginForm","novalidate"=>"true")); ?>
                        <div class="login_fiels">
                          <div class="form_fiels">                            
                             <?php echo $this->Form->input("username",array("type"=>"text","maxlength"=>100,"class"=>"form-control","div"=>false,"label"=>false,"placeholder"=>"Username")); ?>                             
                          </div>  
                          <div class="form_fiels">                             
                             <?php echo $this->Form->input("password",array("type"=>"password","maxlength"=>100,"class"=>"form-control","div"=>false,"label"=>false,"placeholder"=>"Password")); ?>
                          </div>
                          <!--<div class="g-recaptcha_login" data-sitekey=<?php echo SITE_KEY; ?> id="RecaptchaField1"></div>
							<input type="hidden" class="hiddenRecaptcha" name="login_hiddenRecaptcha" id="login_hiddenRecaptcha">-->
                          <div class="form_sub_field">
                             <div class="log-fog-pwd"><a href="forgot-password" title="<?php echo __("Forgot Password?");?>">Forgot Password?</a></div>
                             <button class="btn btn-theme btn-block" type="submit">Login</button>                             
                          </div>
                        </div>
                     </form>
                 </div>
             </div>
        </div>
    </div>
<script>
var widgetId1;
var CaptchaCallback = function() {    
    widgetId1 = grecaptcha.render('RecaptchaField1', {'sitekey' : '<?php echo SITE_KEY; ?>', 'callback' : correctCaptcha_login});   
};
var correctCaptcha_login = function(response) {
    $("#login_hiddenRecaptcha").val(response);
};
</script>
