 <!--/. banner sec -->
<?php echo $this->Form->create('User',array("type"=>"file","class"=>"themeForm")); ?> 				
<?php echo $this->Form->input("id",array("value"=>$user['User']['id'])); ?>

<section class="section">
			<div class="container">
				<div class="row section-full-row">
					<div class="col-md-10 col-md-offset-1">
						<div class="row">
							<div class="col-md-6">
								<div class="form-field">
									<div class="form-heading">
										<?php echo $this->Html->image("icon7.png",array("alt"=>"Account Name")); ?>	Account Name
									</div>
									 <?php echo $this->Form->input('UserDetail.account_name',array("value"=>$user['UserDetail']['account_name'],'label'=>false,"class"=>"form-control","placeholder"=>"Account Name"));?>						
	
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-field">
									<div class="form-heading">
										<?php echo $this->Html->image("icon8.png",array("alt"=>"Email Address","title"=>"Email Address")); ?> Email Address
									</div>
									<?php echo $this->Form->hidden('UserDetail.id',array("value"=>$user['User']['id']));
										echo $this->Form->input('username',array("value"=>$user['User']['username'],'label'=>false,"class"=>"form-control","placeholder"=>"businessname@gmail.com"));?> 						
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-field">
									<div class="form-heading">
										<?php echo $this->Html->image("icon5.png",array("alt"=>"Billing Plan","title"=>"Billing Plan")); ?>
										 Billing Plan
									</div>
								</div>
							</div>
						</div>

						<ul class="plan-box">
							<li>
								<div class="plan-box-bg">
									<div class="plan-box-bg-head">
										<h3>Starter</h3>
										<h2><sup>€</sup>Free</h2>
									</div>
									<div class="plan-box-bg-content">
										<p>You are currently enjoying <br><b>3 posts/week</b> on our starter <br>pricing plan</p>
										<p><?php if($user['User']['is_paid']== 0 && $user['User']['payment_status']== ""){ echo
											 "<span class='active-now'>Active Now</span>";} ?>
										 </p>
									</div>
								</div>
							</li>
							<li class="premium-plan">
								<div class="plan-box-bg">
									<div class="plan-box-bg-head">
										<h3>Premium</h3>
										<h2><sup>€</sup>39.99<sub>/mo</sub></h2>
									</div>
									<div class="plan-box-bg-content">
										<p>Upgrade to start enjoying <br><b>7 posts/week</b> on our most <br>popular pricing plan</p>
										<p>
											<?php if($user['User']['is_paid']==0 && $user['User']['payment_status']=="") {?>
											<a href="<?php echo SITE_LINK.'buy-plan'?>"  title="Ugrade Now!" class="btn btn-update">
											Ugrade Now!</a>
										<?php }else { echo "<span class='active-now'>Active Now</span>";} ?>	
											</p>
									</div>
								</div>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="plan-bottom">
				<div class="container">
					<div class="row">
						<div class="col-md-6 plan-bottom-left">
							<a href="javascript:void(0)" title="Delete Account"   id="deleteuseraccount" class="btn btn-gray">Delete Account</a>
							<?php if($user['User']['is_paid']==1 ) {?>
									 <button type="button" class="btn btn-gray" title="Pause Payment" id="pausepaypalpayment">Pause Payment</button>
									  <?php } else if ($user['User']['is_paid']==0 && $user['User']['payment_status']=="Suspend" ){ ?>
										<button type="button" class="btn btn-gray" title="Reactivate Payment" id="pausepaypalpayment">Re-activate Payment</button>
								  <?php } else {}?>		
							
						</div>
						<div class="col-md-6 plan-bottom-right">
							<?php if(empty($user['User']['identifier'])){?><a href="<?php echo SITE_LINK?>change-password" class="btn btn-gray">Change Password</a><?php }?>		
							<button type="submit" class="btn btn-green">Save Changes</button>
							
						</div>
					</div>
				</div>
			</div>
		</section>
<!--/. My Account -->
<?php echo $this->Form->end(); ?>
