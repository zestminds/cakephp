<div class="users form">
<?php echo $this->Form->create('User'); ?>
	<fieldset>
		<legend><?php echo __('Add User'); ?></legend>
	<?php
		echo $this->Form->input('username');
		echo $this->Form->input('password');
		echo $this->Form->input('user_type_id');
		echo $this->Form->input('email_token');
		echo $this->Form->input('password_token');
		echo $this->Form->input('remember_token');
		echo $this->Form->input('is_active');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Users'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List User Types'), array('controller' => 'user_types', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User Type'), array('controller' => 'user_types', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Biilings'), array('controller' => 'biilings', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Biiling'), array('controller' => 'biilings', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Tags'), array('controller' => 'tags', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Tag'), array('controller' => 'tags', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List User Categories'), array('controller' => 'user_categories', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User Category'), array('controller' => 'user_categories', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List User Details'), array('controller' => 'user_details', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User Detail'), array('controller' => 'user_details', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List User Universities'), array('controller' => 'user_universities', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User University'), array('controller' => 'user_universities', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Voucher Sales'), array('controller' => 'voucher_sales', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Voucher Sale'), array('controller' => 'voucher_sales', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Vouchers'), array('controller' => 'vouchers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Voucher'), array('controller' => 'vouchers', 'action' => 'add')); ?> </li>
	</ul>
</div>
