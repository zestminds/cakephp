<link href="<?php echo SITE_LINK ?>datetime/bootstrap/css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
<div class="cont-right">
	<div class="cont-right-innr">		 
		<div class="col-sm-12">
			<div class="manage-marchant" style="float: left;width: 100%;">
				<div class="table-responsive">
					<h2><?php echo __('Profile: '); echo h($user['UserDetail']['business'])?> </h2>				

						<div style="margin-left:15px;">
							<div class="col-md-6 view_profile_admin"><?php echo __('Id'); ?></div>
							<div class="col-md-6 view_profile_admin"><?php echo h($user['User']['id']); ?></div>
							<div class="col-md-6 view_profile_admin"><?php echo __('Username'); ?></div>
							<div class="col-md-6 view_profile_admin"><?php echo h($user['User']['username']); ?></div>
							<div class="col-md-6 view_profile_admin"><?php echo __('Facebook'); ?></div>
							<div class="col-md-6 view_profile_admin"><?php echo h($user['User']['identifier']); ?></div>
							<div class="col-md-6 view_profile_admin"><?php echo __('Name'); ?></div>
							<div class="col-md-6 view_profile_admin"><?php echo h($user['UserDetail']['name']) ?></div>
							<div class="col-md-6 view_profile_admin"><?php echo __('Address'); ?></div>
							<div class="col-md-6 view_profile_admin"><?php echo h($user['UserDetail']['address']) ?></div>
							<div class="col-md-6 view_profile_admin"><?php echo __('Is Posts Reported'); ?></div>
							<div class="col-md-6 view_profile_admin"><?php echo h(($user['UserDetail']['is_reported'] == 1)?'Yes ('.$user['UserDetail']['is_reported_times'].' times)' :'No'); ?></div>
							<div class="col-md-6 view_profile_admin"><?php echo __('Is Active'); ?></div>
							<div class="col-md-6 view_profile_admin"><?php echo h(($user['User']['is_active'] == 1)?'Active':'Inactive'); ?></div>
							<div class="col-md-6 view_profile_admin"><?php echo __('Joined'); ?></div>
							<div class="col-md-6 view_profile_admin"><?php  $joined =  date_create(h($user['User']['created'])); echo date_format($joined,"jS M, Y h:i:s"); ?></div>
							<div class="col-md-6 view_profile_admin"><?php echo __('Last Login'); ?></div>
							<div class="col-md-6 view_profile_admin"><?php  $joined =  date_create(h($user['User']['last_login'])); echo date_format($joined,"jS M, Y h:i:s"); ?></div>
						</div>
						<?php echo $this->Form->create('User',array("type"=>"file","class"=>"themeForm"));?>

						<div class="filters_view_profile">
							<div><h2>Filters</h2></div>
							<div class="filters_ranage">
								<div class="col-md-10">
									<?php echo $this->Form->input('select_filter',array("label"=>false,"type"=>"select",'autofill'=>'false',"class"=>"form-condtrl select","options"=>$opt));?>   
								</div>
								<div class="col-md-3">
									<label class="lable-control">Date Range From:</label>										
										<div class='input_date_grp input-group date datetimepicker1'>
											<?php echo $this->Form->input('start_date',array("label"=>false,"type"=>"text",'autofill'=>'false',"class"=>"form-control"));?>    
											<span class="input-group-addon">
											<span class="fa fa-calendar"></span>
											</span>
										</div>
								</div>
								<div class="col-md-3">
									<label class="lable-control">Date Range To:</label>											
										<div class='input_date_grp input-group date datetimepicker1'>

										<?php echo $this->Form->input('end_date',array("label"=>false,"type"=>"text",'autofill'=>'false',"class"=>"form-control"));?>    
										<span class="input-group-addon">
										<span class="fa fa-calendar"></span>
										</span>
										</div>
										
								</div>
								<div class="col-md-3"><button type="submit" class="btn btn-theme" id="save" name="save">Search</button></div>
								<div class="col-md-12">
									<div class="show_count_msg alert alert-info" style="display:none"></div>
								</div>	
							</div>
						</div>
					<?php echo $this->Form->end(); ?>	
				</div>
			</div>
		</div>
	</div>	
</div>
<script>
	$(document).ready(function(){	
		$(function() {
		  $('.datetimepicker1').datetimepicker({
			  //format: "DD/MM/YYYY"
			  format: "YYYY-MM-DD",
			  //minDate:new Date()
		  });
		  $('.datetimepicker2').datetimepicker({
			   format: 'LT'
		  });
		});
		
		
		function fetch_filter()
		{
		//$("#searchTag").change(function(e){
			var val=$("#UserSelectFilter option:selected").val();			
			var from=$("#UserStartDate").val();
			var to=$("#UserEndDate").val();
			var id=<?php echo $user['User']['id']?>;
			var type="calendar";
			var href="";
			if(val=="total_events")
			{
				 href =SITE_LINK+"filter_total_events?id="+id+"&type="+val+"&from="+from+"&to="+to;	
			}
			else 
			{
				if(val=="events_viewed")
				type="viewed"
				else
				type="calendar";
				href =SITE_LINK+"filter_manager?id="+id+"&type="+type+"&from="+from+"&to="+to;
			}
			
			$.ajax({  type: "GET",   
				cache: false,   
				url: href,   			
				success: function(data){
					data = JSON.parse(data);
					
					if ( data.msg="success" ) {						
						if(val=="total_events")
						{
							$(".show_count_msg").show();
							$(".show_count_msg").text("Total numbder of events added in the specified ranage are "+data.count);
						}
						else if(val=="events_viewed")
						{
							$(".show_count_msg").show();
							$(".show_count_msg").text("Total numbder of events viewed by app users in the specified ranage are "+data.count);
						}
						else if(val=="events_saved")
						{
							$(".show_count_msg").show();
							$(".show_count_msg").text("Total numbder of events added to Calendar in the specified ranage are "+data.count);
						}

					}
				}
			});
		}
		
		$("#UserAdminViewForm").submit(function(e) {    e.preventDefault(); }).validate({ 
			
			 ignore: "",
			rules: {			
			  "data[User][start_date]": {
				required: true	
			  },
			   "data[User][end_date]": {
				required: true	
			  }
			},		
			messages: {		
			 "data[User][start_date]": {
				required: "Please enter From date"	
			  },
			   "data[User][end_date]": {
				required: "Please enter To date"	
			  }
			},
			submitHandler: function(e) {	
							
				fetch_filter();	
			}
	  });
		
	});
</script>	
