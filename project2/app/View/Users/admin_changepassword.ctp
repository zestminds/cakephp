<?php echo $this->Html->script('admin/changepassword');?>
<script>
	$(document).ready(function () {

    $('#changepassword').validate({ // initialize the plugin
        rules: {
					  
			 "data[User][currentpassword]": {
				required: true				
			  },
			   "data[User][newpassword]": {
				required: true,
				minlength: 6,
				maxlength: 20		
			  },			
			"data[User][confirmpassword]": {
				required: true,				
				equalTo: '#UserPassword'
			  }
			},
			// Specify validation error messages
			messages: {			
				 "data[User][currentpassword]": {
				required: "Please provide current password",				
			  },  
			  "data[User][newpassword]": {
				required: "Please provide new password",
				minlength: "Your password must be at least 6 characters long",
				maxlength: "Your password not be larger than 20 characters"
			  },
			  "data[User][confirmpassword]": {
				required: "Please provide confirm password",		
				equalTo:"Your Passwords and Confirm Password must match"
			  }
			},
			
			submitHandler: function(form) {
			  form.submit();
			}
		  });
		  
		  jQuery.validator.addMethod( 'passwordMatch', function(value, element) {
    
			// The two password inputs
			var password = $("data[User][newpassword]").val();
			var confirmPassword = $("data[User][confirmpassword]").val();

			// Check for equality with the password inputs
			if (password != confirmPassword ) {
				return false;
			} else {
				return true;
			}

		}, "Your Passwords Must Match");
});
</script>
<div class="cont-right">
  <div class="cont-right-innr">
		<div class="main-hd-in">
          <div class="row">
            <div class="col-sm-12">
              <h2 class="title-das"> Change Password</h2>              
            </div>
          </div>        
        <div class="manage-marchant">
		
			<?php //echo $this->Session->flash(); ?>
				<!--<h1><em class="signin-signup-icn"></em><?php echo 'CHANGE PASSWORD'; ?></h1>-->
				<fieldset>
				<?php 
					echo $this->Form->create('User',array("id"=>'changepassword',"novalidate"=>true));
				?>
				<div class="fields">
					<?php echo $this->Form->input("currentpassword",array("type"=>'password',"id"=>'CurrentPassword','placeholder'=>"Current Password",'value'=>'','div'=>false,'label'=>false)); ?>
								<?php echo $this->Form->hidden("id",array("value"=>$this->Session->read("AuthUser.User.id")));?>
				</div>	

				<div class="fields">
					<?php echo $this->Form->input("newpassword",array("type"=>'password',"id"=>'UserPassword','placeholder'=>"New Password",'value'=>'','div'=>false ,'label'=>false)); ?>
				</div>	

				<div class="fields">
					<?php echo $this->Form->input("confirmpassword",array("type"=>'password',"id"=>'UserRetypePassword','placeholder'=>"Confirm Password",'value'=>'','div'=>false,'label'=>false)); ?>
				</div>	
				
				<!-- =====Button section start===== -->
				<div style="margin-top:20px">
				
	                   <button type="submit" class="btn btn-theme" id="save" name="save">SAVE</button>
	                 
					<?php //echo $this->Form->Submit('SUBMIT',array("class"=>"btn btn-theme","title"=>"Submit",'div'=>false)); ?>
					<?php echo $this->Html->link('cancel', array('controller' => 'users', 'action' => 'dashboard'),array('class'=>"btn btn-theme","title"=>"Cancel"));  ?>
					<?php echo $this->Form->end();?>	
				</div>
				<fieldset>
			 
			</div>
	</div>		
</div>		
