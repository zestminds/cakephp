 <div class="cont-right">
      <div class="cont-right-innr">
      	  <div class="dashbord-total">
           <div class="main-hd-in">
            <div class="row">
              <div class="col-sm-12">
                <h2 class="title-das">Dashboard</h2>
              </div>
            </div>
          </div>
          <div class="bloc-total row-eq-height">
            <div class="col-lg-3 col-md-6  pd">
              <div class="Revenue-box box-1 ">
                <div class="box-lf "><?php echo $this->Html->image("doller.png",array("alt"=>""));?></div>
                <div class="bx-single">
                  <p>Total Revenue</p>
                  <h2 > <i class="fa fa-long-arrow-up"></i><?php echo  ($total_revenue[0][0]['Price']!="")?$total_revenue[0][0]['Price']:'0'?></h2>
                </div>
                <div class="relse-date">
                  <p><?php echo $datafromtotill;?></p>
                </div>
              </div>
            </div>
            <div class="col-lg-3 col-md-6  pd">
              <div class="Revenue-box box-2 ">
                <div class="box-lf "><?php echo $this->Html->image("usr.png",array("alt"=>"User"));?></div>
                <div class="bx-single">
                  <p>Total App Users</p>
                  <h2 > <i class="fa fa-long-arrow-up"></i><?php echo $total_students; ?></h2>
                </div>
                <div class="relse-date">
                  <p><?php echo $datafromtotill;?></p>
                </div>
              </div>
            </div>
            <div class="col-lg-3 col-md-6  pd">
              <div class="Revenue-box box-3 ">
                <div class="box-lf "><?php echo $this->Html->image("marchant.png",array("alt"=>"Marchant"));?></div>
                <div class="bx-single">
                  <p>Total Event Manager</p>
                  <h2 > <i class="fa fa-long-arrow-up"></i><?php echo $total_merchants; ?></h2>
                </div>
                <div class="relse-date">
                  <p><?php echo $datafromtotill; ?></p>
                </div>
              </div>
            </div>
            <div class="col-lg-3 col-md-6  pd">
              <div class="Revenue-box box-4 ">
                <div class="box-lf "><?php echo $this->Html->image("voucher.png",array("alt"=>"Voucher"));?></div>
                <div class="bx-single">
                  <p>Number of Events</p>
                  <h2 > <i class="fa fa-long-arrow-up"></i><?php echo $total_vouchers; ?></h2>
                </div>
                <div class="relse-date">
                  <p><?php echo $datafromtotill; ?></p>
                </div>
              </div>
            </div>
          </div>
        </div>
        
        	<div class="total-no">
            	<div class="col-lg-4 col-md-6 pd">
                	<div class="voucher-bx">
                    
                    	<div class="hd-vchr">
                        	<div class="vc-left">
								<p>Total Number Of Events</p>
                            </div>
                            <div class="vc-right">                           	
                               		<?php echo $this->Form->create("totalvoucher",array("id"=>"totalvoucher","name"=>"totalvoucher","type"=>"post")); 	
                               		echo $this->Form->input('form_submit',array("id"=>"form_submit","type"=>"hidden"));
                               		echo $this->Form->input('total_voucherval',array("name"=>"total_voucherval","id"=>"total_voucherval",'label'=>'',"options"=>$records,'class'=>'form-control number-of-vc',"selected"=>$days_fetch_voucher));
                               		 echo $this->Form->end(); ?>	 
									<ul class="list-inline chart-list">						  
									<li class="showtotalvouline"><?php echo $this->Html->image("chart-img-1.png",array("alt"=>"Voucher"));?></li>
                                    <li class="showtotalvoucol"><?php echo $this->Html->image("chart-img-2.png",array("alt"=>"Voucher"));?></li>
									</ul>									
                            </div>
                        </div> 
                        <div class="vc-chart-main">
							<div id="total_vou_line">	
								<div id="container_total_voucher_line" style="height: 200px"></div>
                        	</div>
                        	<div id="total_vou_col" style="display:none">	
								<div id="container_total_voucher_col" style="height: 200px" ></div>
                        	</div>
                       </div>
                    </div>
                </div>           
                <div class="col-lg-4 col-md-6 pd">
                	<div class="voucher-bx">
                    
                    	<div class="hd-vchr">
                        	<div class="vc-left">
                            	<p>Total Saved Events( Calendar)</p>
                            </div>
                            <div class="vc-right">
                            	<?php echo $this->Form->create("totalconversions",array("id"=>"totalconversions","name"=>"totalconversions","type"=>"post")); 
									echo $this->Form->input('form_submit',array("id"=>"form_submit","type"=>"hidden",'label'=>false));
                               		echo $this->Form->input('total_conversionsval',array("name"=>"total_conversionsval","id"=>"total_conversionsval",'label'=>false,"options"=>$records,'class'=>'form-control number-of-vc',"selected"=>$days_fetch_con));
                               		echo $this->Form->end(); ?>
									 <ul class="list-inline chart-list">								  	 
									<li class="showtotalconline"><?php echo $this->Html->image("chart-img-1.png",array("alt"=>"Voucher"));?></li>
                                    <li class="showtotalconcol"><?php echo $this->Html->image("chart-img-2.png",array("alt"=>"Voucher"));?></li>
									</ul>
                            </div>
                        </div>
                        <div class="vc-chart-main">
                        	<div id="total_con_line">	
								<div id="container_total_con_line" style="height: 200px"></div>
                        	</div>
                        	<div id="total_con_col" style="display:none">	
								<div id="container_total_con_col" style="height: 200px" ></div>
                        	</div>
                        </div>
                    </div>
                </div> 
                <div class="col-lg-4 col-md-6 pd">
                	<div class="voucher-bx">
                    
                    	<div class="hd-vchr">
                        	<div class="vc-left">
                            	<p>Total Viewed Events</p>
                            </div>
                            <div class="vc-right">
                            	<?php echo $this->Form->create("totalimpressions",array("id"=>"totalimpressions","name"=>"totalimpressions","type"=>"post")); ?>		
                            	  <?php echo $this->Form->input('form_submit',array("id"=>"form_submit","type"=>"hidden",'label'=>false));
                               		echo $this->Form->input('total_impressionsval',array("name"=>"total_impressionsval","id"=>"total_impressionsval",'label'=>false,"options"=>$records,'class'=>'form-control number-of-vc',"selected"=>$days_fetch_imp));							
									echo $this->Form->end(); ?>
									 <ul class="list-inline chart-list">
										 
										<li class="showtotalimpline"><?php echo $this->Html->image("chart-img-1.png",array("alt"=>"Voucher"));?></li>
										<li class="showtotalimpcol"><?php echo $this->Html->image("chart-img-2.png",array("alt"=>"Voucher"));?></li>
									</ul>
                            </div>
                        </div>
                        <div class="vc-chart-main">
                        	<div id="total_imp_line">	
								<div id="container_total_imp_line" style="height: 200px"></div>
                        	</div>
                        	<div id="total_imp_col" style="display:none">	
								<div id="container_total_imp_col" style="height: 200px" ></div>
                        	</div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="marchant-detail row-eq-height">
            	<div class="col-lg-7 pd">
                	<div class="marchant-left">
                    	<div class="table-responsive">
							<?php  $count=count($merchant_sale) ; if ($count>0){ ?>
                        	<table class="table table-bordered Marchant-tab">
                            	<tr>
                                	<th>Sr no </th>
                                    <th>ID</th>
                                    <th>Merchant Name </th>
                                    <th>Transaction Date </th>
                                    <th>Amount </th>
                                </tr>
                               <?php $i=1;foreach( $merchant_sale as $sale ){	?>
                                <tr>
                                	<td><?php echo $i;?></td>
                                    <td><?php echo $sale['UserDetail']['user_id']; ?></td>
                                    <td><?php echo $sale['UserDetail']['name']; ?></td>
                                    <td><?php $bill_date =  date_create(h($sale['Billing']['renew_date'])); echo date_format($bill_date,"j M, Y");?></td>
                                    <td><?php echo $sale['Plan']['price']; ?></td>
                                </tr>
                                <?php $i++; }?>
                            </table>
                            <?php } else{ ?><div class="box-card-nodata"><div class="alert alert-warning">No Data to display.</div></div><?php } ?>
                        </div>
                    </div>
                </div>
                <div class="col-lg-5 pd">
                	<div class="Impressions">
                    	<div class="mch-hd">
						<?php echo $this->Form->create("compareConversions",array("div"=>false,"id"=>"compareConversions","type"=>"post")); 
						echo $this->Form->input('form_submit',array("id"=>"form_submit","type"=>"hidden"));
						//echo $this->Form->input('compareImpCon',array("name"=>"compareImpCon","id"=>"compareImpCon",'label'=>false,"options"=>$compareImpCon,'class'=>'form-control selectOne insight',"selected"=>$compareImpCon_dropdown_select,"div"=>false));
						echo $this->Form->input('compareImpConVal',array("name"=>"compareImpConVal","id"=>"compareImpConVal",'label'=>false,"options"=>$records,'class'=>'form-control number-of-vc insight',"selected"=>$days_fetch_conimp,"div"=>false));
						echo $this->Form->end(); ?>	
                    </div>
                    	<div class="Impressions-md">                        	                         
                            <div class="mch-chart">
                            	<div id="container_impvscon_col"></div>
                            </div>
                        </div>
                    	
                    </div>
                </div>
            </div>
        	
      </div>
    </div>
  </div>
</div>

<link rel="stylesheet" type="text/css" href="https://rawgit.com/notryanb/highcharts_date_range_grouping/config-date-formats/dist/highcharts_date_range_grouping.css"> 
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://cdn.rawgit.com/notryanb/highcharts_date_range_grouping/config-date-formats/dist/highcharts_date_range_grouping.min.js"></script>
<script>
	Highcharts.setOptions({
 colors: ['#282828', '#e62e57','#0e7228','#0c4a87','#785a04','#717171']
});
Highcharts.setOptions({
    colors: Highcharts.map(Highcharts.getOptions().colors, function (color) {
        return {
            radialGradient: {
				r: 0.6,
                cx: 0.1,
                cy: 0.2                
            },
            stops: [
                [0, color],
                [1, Highcharts.Color(color).brighten(-0.1).get('rgb')] // darken
            ]
        };
    })
});
var categories = [
  'Demo1'
  ].map(function(date) {
    let formatOptions = { month: '2-digit', day: '2-digit', year: 'numeric' };
    return new Date(date).toLocaleDateString(undefined, formatOptions); 
  });

Highcharts.chart('container_total_voucher_line', {
  chart: {
    type: 'line'
  },
  dateRangeGrouping: false,
  title: {
    text: ''
  },
 xAxis: {
    type: 'datetime',
    padding:2
  
},
 yAxis: {
	 
        allowDecimals: false, 
       // min: 0,
		//max: 50,
		tickInterval: 2,
		lineWidth: 1,       
        title: {
            text: ''
        }
    },
    
       tooltip: {
    formatter: function() {
       // return 'Vocuher<b>' + this.x + '</b> is <b>' + this.y + '</b>, in series '+ this.series.name;
       return this.y ;
    }
},
    legend: {
        enabled: false
    },
  plotOptions: {
    series: {
      dataLabels: {
        enabled: false
      },
      enableMouseTracking: true
    }
  },
  series: [{
    name: '30 Days',
    pointStart: Date.UTC(<?php echo $startDate_voucher;?>),
      pointInterval: 24 * 3600 * 1000,
      
    data: [
      <?php $i=0; foreach( $final_total_vou_Arr as $key=>$val )		 
		 {
			 if ( $i > 0 ) {
				 echo ",".$val;
			 }
			 else{
				  echo $val;
			 }
			 $i++;
		}		 
	  ?>
    ]
  }]
});

Highcharts.chart('container_total_voucher_col', {
  chart: {
    type: 'column'
  },
  dateRangeGrouping: true,
  title: {
    text: ''
  },
 xAxis: {
    type: 'datetime',
    padding:2
  
},
 yAxis: {
	 
        allowDecimals: false, 
        //min: 0,
		//max: 50,
		tickInterval: 2,
		lineWidth: 1,       
        title: {
            text: ''
        }
    },
    
       tooltip: {
    formatter: function() {
        return this.y ;
    }
},
    legend: {
        enabled: false
    },
  plotOptions: {
    series: {
      dataLabels: {
        enabled: false
      },
      enableMouseTracking: true
    }
  },
  series: [{
    name: '30 Days',
    pointStart: Date.UTC(<?php echo $startDate_voucher;?>),
      pointInterval: 24 * 3600 * 1000,
      
    data: [
      <?php $i=0; foreach( $final_total_vou_Arr as $key=>$val )		 
		 {
			 if ( $i > 0 ) {
				 echo ",".$val;
			 }
			 else{
				  echo $val;
			 }
			 $i++;
		}		 
	  ?>
    ]
  }]
});
</script>
<script>
Highcharts.chart('container_total_con_line', {
   chart: {
    type: 'line'
  },
  dateRangeGrouping: false,
  title: {
    text: ''
  },
 xAxis: {
    type: 'datetime',
    padding:2
  
},
 yAxis: {
	 
        allowDecimals: false, 
       // min: 0,
		//max: 50,
		tickInterval: 2,
		lineWidth: 1,       
        title: {
            text: ''
        }
    },
    
       tooltip: {
    formatter: function() {
       // return 'Vocuher<b>' + this.x + '</b> is <b>' + this.y + '</b>, in series '+ this.series.name;
       return this.y ;
    }
},
    legend: {
        enabled: false
    },
  plotOptions: {
    series: {
      dataLabels: {
        enabled: false
      },
      enableMouseTracking: true
    }
  },
  series: [{
    name: '30 Days',
    pointStart: Date.UTC(<?php echo $startDate_voucher;?>),
      pointInterval: 24 * 3600 * 1000,
    data: [
      <?php $i=0; foreach( $final_total_con_Arr as $key=>$val )		 
		 {
			 if ( $i > 0 ) {
				 echo ",".$val;
			 }
			 else{
				  echo $val;
			 }
			 $i++;
		}		 
	  ?>
    ]
  }]
});
</script>
<script>
Highcharts.chart('container_total_con_col', {
  chart: {
    type: 'column'
  },
  dateRangeGrouping: true,
  title: {
    text: ''
  },
 xAxis: {
    type: 'datetime',
    padding:2
  
},
 yAxis: {
	 
        allowDecimals: false, 
        //min: 0,
		//max: 20,
		tickInterval: 2,
		lineWidth: 1,       
        title: {
            text: ''
        }
    },
    
       tooltip: {
    formatter: function() {
        return this.y ;
    }
},
    legend: {
        enabled: false
    },
  plotOptions: {
    series: {
      dataLabels: {
        enabled: false
      },
      enableMouseTracking: true
    }
  },
  series: [{
    name: '30 Days',
    pointStart: Date.UTC(<?php echo $startDate_con;?>),
      pointInterval: 24 * 3600 * 1000,
      
    data: [
      <?php $i=0; foreach( $final_total_con_Arr as $key=>$val )		 
		 {
			 if ( $i > 0 ) {
				 echo ",".$val;
			 }
			 else{
				  echo $val;
			 }
			 $i++;
		}		 
	  ?>
    ]
  }]
});
</script>
<script>
Highcharts.chart('container_total_imp_line', {
  chart: {
    type: 'line'
  },
  dateRangeGrouping: true,
  title: {
    text: ''
  },
 xAxis: {
    type: 'datetime',
    padding:2
  
},
 yAxis: {
	 
        allowDecimals: false, 
        //min: 0,
		//max: 20,
		tickInterval: 2,
		lineWidth: 1,       
        title: {
            text: ''
        }
    },
    
       tooltip: {
    formatter: function() {
        return this.y ;
    }
},
    legend: {
        enabled: false
    },
  plotOptions: {
    series: {
      dataLabels: {
        enabled: false
      },
      enableMouseTracking: true
    }
  },
  series: [{
    name: '30 Days',
    pointStart: Date.UTC(<?php echo $startDate_imp;?>),
      pointInterval: 24 * 3600 * 1000,
      
    data: [
      <?php $i=0; foreach( $final_total_imp_Arr as $key=>$val )		 
		 {
			 if ( $i > 0 ) {
				 echo ",".$val;
			 }
			 else{
				  echo $val;
			 }
			 $i++;
		}		 
	  ?>
    ]
  }]
});
</script>
<script>
Highcharts.chart('container_total_imp_col', {
  chart: {
    type: 'column'
  },
  dateRangeGrouping: true,
  title: {
    text: ''
  },
 xAxis: {
    type: 'datetime',
    padding:2
  
},
 yAxis: {
	 
        allowDecimals: false, 
        //min: 0,
		//max: 20,
		tickInterval: 2,
		lineWidth: 1,       
        title: {
            text: ''
        }
    },
    
       tooltip: {
    formatter: function() {
      return this.y ;
    }
},
    legend: {
        enabled: false
    },
  plotOptions: {
    series: {
      dataLabels: {
        enabled: false
      },
      enableMouseTracking: true
    }
  },
  series: [{
    name: '30 Days',
    pointStart: Date.UTC(<?php echo $startDate_imp;?>),
      pointInterval: 24 * 3600 * 1000,
      
    data: [
      <?php $i=0; foreach( $final_total_imp_Arr as $key=>$val )		 
		 {
			 if ( $i > 0 ) {
				 echo ",".$val;
			 }
			 else{
				  echo $val;
			 }
			 $i++;
		}		 
	  ?>
    ]
  }]
});
</script>
<script>
Highcharts.chart('container_impvscon_col', {
  chart: {
    type: 'column'
  },
  dateRangeGrouping: true,
  title: {
    text: ''
  },
 xAxis: {
    type: 'datetime',
    padding:2
  
},
 yAxis: {
	 
        allowDecimals: false, 
        //min: 0,
		//max: 50,
		tickInterval: 2,
		lineWidth: 1,       
        title: {
            text: ''
        }
    },
    
       tooltip: {
    formatter: function() {
        return this.y ;
    }
},
    legend: {
        enabled: true	
    },
  plotOptions: {
    series: {
      dataLabels: {
        enabled: false
      },
      enableMouseTracking: true
    }
  },
  series: [{
    name: '<?php echo $series1;?>',
    pointStart: Date.UTC(<?php echo $startDate_reachimp;?>),
      pointInterval: 24 * 3600 * 1000,
      
    data: [
      <?php $i=0; foreach( $final_reachimp_Arr as $key=>$val )		 
		 {
			 if ( $i > 0 ) {
				 echo ",".$val;
			 }
			 else{
				  echo $val;
			 }
			 $i++;
		}		 
	  ?>
    ]
  },{
    name: '<?php echo $series2;?>',
    pointStart: Date.UTC(<?php echo $startDate_reachimp;?>),
      pointInterval: 24 * 3600 * 1000,
      
    data: [
      <?php $i=0; foreach( $final_conimp_Arr as $key=>$val )		 
		 {
			 if ( $i > 0 ) {
				 echo ",".$val;
			 }
			 else{
				  echo $val;
			 }
			 $i++;
		}		 
	  ?>
    ]
  }]
});
</script>
