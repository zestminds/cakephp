<script>
	$(document).ready(function () {
	 $('#flashMessage').delay(5000).fadeOut();
    $('#UserAdminProfileForm').validate({ // initialize the plugin
        rules: {			
			 
			  "data[UserDetail][name]": {
				required: true,				
			  },
			  "data[User][username]": {
				required: true,		
				email: true		
			  }	,	
			},
			// Specify validation error messages
			messages: {			
				"data[UserDetail][name]": {
					required: "Please enter Name"			
				},			 
				"data[User][username]": {
					required: "Please enter email",
					email: "Please enter a valid email address",			
				}
			},
			// Make sure the form is submitted to the destination defined
			// in the "action" attribute of the form when valid
			submitHandler: function(form) {
			  form.submit();
			}
		  });	
	 $('#sample_input').awesomeCropper(
		{ width: 163, height: 163, debug: true }
     );	
	 
	
});
</script>
<div class="cont-right">
   <div class="cont-right-innr">
   <legend><?php echo __('General Information'); ?></legend>
		<div class="users form border-box">
		<?php echo $this->Form->create('User'); ?>		
			 <div class="profile-cont">	
				 <div>			 
						<div class="prof-pic-admin user_pic img-upload" style="background-image:url('<?php echo (empty($this->request->data['UserDetail']['image']) || !file_exists(WWW_ROOT."/img/profile/".$this->request->data['UserDetail']['image'])) ?  SITE_LINK.'img/default.jpeg' : SITE_LINK.'img/profile/'.$this->request->data['UserDetail']['image'];?>');"> 
						<?php echo $this->Form->input("tmpImage",array("label"=>false,"type"=>"textarea","style"=>"display:none;","id"=>"tmpImage")); ?>	
						<?php /* echo $this->Form->hidden('image_old',array('value'=>$this->request->data['UserDetail']['image']));?>
							 <div class="upload-btn-wrapper" style="display:none;"><br>
								<button class="btnfile">Edit</button>
								<?php echo $this->Form->file('UserDetail.image',array('width'=>10));?>		
							</div>	
						*/ ?>	
						</div>
						<div class="btn btn-upload image-preview-input profile-image-internal">
							<i class="fa fa-cloud-upload" aria-hidden="true"></i><span class="image-preview-input-title">Select profile image</span>
						   <?php echo $this->Form->file('image',array("type"=>"file","accept"=>"image/png, image/jpeg, image/gif","type"=>"file","id"=>"sample_input","name"=>"test[image]"));?>
								
								<!-- rename it --> 
						</div>
						
						<?php //echo $this->Form->file('image',array("type"=>"file","accept"=>"image/png, image/jpeg, image/gif","type"=>"file","id"=>"sample_input","name"=>"test[image]"));?>
						
				</div>	
				<div class="prof-details">
				
				   <h4><?php echo $this->request->data['UserDetail']['name'];?></h4>
				   <p><i class="icons"><?php echo $this->Html->image("env-icon.png",array("alt"=>"Merchant Email")); ?></i> <span><?php echo $this->request->data['User']['username'];?></span></p>
				</div>
				
			 </div>	
			<div class="account-detals">	
				<?php echo $this->Form->input('id',array('type'=>'hidden'));?>
				<?php echo $this->Form->input('UserDetail.id',array('type'=>'hidden'));?>
				<div><?php echo $this->Form->input('UserDetail.name'); ?></div>
				<div><?php echo $this->Form->input('username');?></div>	
				<div >		
			<div class="password-change"><a href="<?php echo SITE_LINK?>manage-password">Change Password</a></div>
	
				<div class="form-submit-profile">
				  
				   <button type="submit" class="btn btn-theme" id="save" name="save" onclick="voucherStatus('save');">Submit</button>
				  
				</div>
	       
		</div>
	</div>		
</div>

	
