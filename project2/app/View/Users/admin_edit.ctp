<script>
	$(document).ready(function () {
    $('#UserAdminEditForm').validate({ // initialize the plugin
        rules: {			
			 
			  "data[UserDetail][name]": {
				required: true,				
			  },
			  "data[UserDetail][business]": {
				required: true,				
			  },
			  "data[UserDetail][address]": {
				required: true				
			  }		
			},
			// Specify validation error messages
			messages: {			
				"data[UserDetail][name]": {
					required: "Please enter Name"			
				},
				"data[UserDetail][business]": {
					required: "Please enter business name"			
				},			 
			  "data[UserDetail][address]": {
					required: "Please enter Address"			
				}
			},
			// Make sure the form is submitted to the destination defined
			// in the "action" attribute of the form when valid
			submitHandler: function(form) {
			  form.submit();
			}
		  });		
});
</script>
<script type="text/javascript">
	var avatar = "<?php echo (empty($user['UserDetail']['image']) || !file_exists(WWW_ROOT."/img/profile/".$user['UserDetail']['image'])) ?  SITE_LINK.'/img/default_new.png' : SITE_LINK.'img/profile/'.$user['UserDetail']['image']; ?>";
</script>
<?php
$tmpImage = (empty($user['UserDetail']['image']) || !file_exists(WWW_ROOT."/img/profile/".$user['UserDetail']['image'])) ?  SITE_LINK.'/img/default_new.png' : SITE_LINK.'img/profile/'.$user['UserDetail']['image'];

//echo $tmpImage = file_get_contents($tmpImage); die;
 ?>
<div class="cont-right">
   <div class="cont-right-innr">
		<div class="main-hd-in">
			  <div class="row">
				<div class="col-sm-12">
				  <h2><?php echo ($this->request->data['User']['user_type_id'] == 3) ? "Edit App User" :"Edit Event Manager" ?></h2>              
				</div>
			  </div>
        </div>
		<div class="manage-marchant">
		<?php echo $this->Session->flash(); ?>	
		<?php echo $this->Form->create('User'); ?>
			
			<div class="account-profile">
			 <div class="profile-cont">	
				 <div style="float:left">			 
						<div class="prof-pic user_pic img-upload" id="img-upload" style="background-image:url('<?php echo (empty($user['UserDetail']['image']) || !file_exists(WWW_ROOT."img/profile/".$user['UserDetail']['image'])) ?  SITE_LINK.'img/default_new.png' : SITE_LINK.'img/profile/'.$user['UserDetail']['image']; ?>');"> 
						<?php echo $this->Form->input("tmpImage",array("label"=>false,"type"=>"textarea","style"=>"display:none;","id"=>"tmpImage")); ?>	
							
						</div>
						
						
				</div>	
				<div class="prof-details">
				
				   <h4><?php echo $user['UserDetail']['business'];?></h4>
				   <p><i class="icons"><?php echo $this->Html->image("env-icon.png",array("alt"=>"Merchant Email")); ?></i> <span><?php echo $user['User']['username'];?></span></p>
				   <p><i class="icons"><?php echo $this->Html->image("cal-icon.png",array("alt"=>"Joining Date")); ?></i> <span><?php $joined =  date_create(h($user['User']['created'])); echo date_format($joined,"jS M, Y");?></span></p>
				</div>
				
			 </div>

			<div class="account-detals">	
				
					<?php //echo $this->Form->hidden('image_old',array('value'=>$user['UserDetail']['image']));?>
					<div class="row">
					
					   <div class="form-group inputField col-sm-6">
						  <label class="lable-control"><i class="sm-icons"><?php echo $this->Html->image("sm-icon1.png",array("alt"=>"Full Name","placeholder"=>"Full Name")); ?></i> <?php if ($user['User']['user_type_id'] == 2) { ?> Business Name <?php } else {?> Full Name <?php } ?></label>
						  <?php echo $this->Form->input('UserDetail.business',array('label'=>false,"class"=>"form-control","placeholder"=>"Business Name"));?>						
					   </div>				
					   <div class="form-group inputField col-sm-6">
						  <label class="lable-control"><i class="sm-icons"><?php echo $this->Html->image("sm-icon2.png",array("alt"=>"Email Address","placeholder"=>"Email Address")); ?></i> Email Address</label>
						  <?php echo $this->Form->hidden('UserDetail.id');echo $this->Form->hidden('UserDetail.latitude'); echo $this->Form->hidden('UserDetail.longitude');
						  echo $this->Form->hidden('User.user_type_id');
						  echo $this->Form->hidden('UserDetail.user_id');
						  echo $this->Form->hidden('id');
						echo $this->Form->input('username',array('label'=>false,"class"=>"form-control","placeholder"=>"businessname@gmail.com")); 						
						echo $this->Form->hidden('tmp_username',array("value"=>$user['User']['username']));?> 						
					   </div>
					</div>
					<?php if ($user['User']['user_type_id'] == 2) { ?>
					<div class="row">
					   <div class="form-group inputField col-sm-6">
						 <label class="lable-control"><i class="sm-icons"><?php echo $this->Html->image("sm-icon1.png",array("alt"=>"Full Name","placeholder"=>"Full Name")); ?></i> Account Name</label>

						<?php echo $this->Form->input('UserDetail.account_name',array('label'=>false,"class"=>"form-control","placeholder"=>"businessname@gmail.com")); ?>

					   </div>
					  	<div class="form-group inputField col-sm-6">
						   
						    <label class="lable-control"><i class="sm-icons"><?php echo $this->Html->image("sm-icon4.png",array("alt"=>"Discount Venue")); ?></i> Discount Venue</label>
						    <div id="pac-container"><?php echo $this->Form->input('UserDetail.address',array("id"=>"pac-input","class"=>"form-control",'label'=>false,"placeholder"=>"123, Manchester, United Kingdom"));?></div>					   
									  
					   </div>
					</div>	
					<?php } ?>			
					<div class="row">
						 <div class="form-group inputField col-sm-12">
						  <label class="lable-control"><i class="sm-icons"><?php echo $this->Html->image("about-us.png",array("alt"=>"About Me")); ?></i> About Me</label>
						  <?php
						  echo $this->Form->input('UserDetail.aboutme',array("class"=>"form-control","type"=>"text","placeholder"=>"","rows"=>3,"label"=>false,"autocomplete"=>"off","value"=>$user['UserDetail']['aboutme']));	?>					  
					   </div>	
					</div>
					<?php if ($user['User']['user_type_id'] == 2) { ?>
					<div class="map-location">
						 <div class="map-frame">																			
							 <div id="map" style="height:490px;width:1170px;"></div>
														
							 
						 </div>
					</div>
					<?php }?>
					<div><?php echo $this->Form->input('is_active');?></div>
					 <div>
						<?php ($user['User']['user_type_id'] == 3)? $link='manage-students': $link='manage-merchants'?>
						<button type="submit" class="btn btn-theme">SAVE CHANGES</button>
						<a href="<?php echo SITE_LINK.$link ?>" class="btn btn-theme">cancel</a>				  

					</div>
				</form>
			</div>
		</div>
		

		
		</div>
	</div>
</div>	
<style>
.imageBox{width:520px !important;height:350px !important;}
</style>
<?php /*
<div class="modal fade loginModel themeModel" id="imageModal" tabindex="-1" role="dialog" aria-labelledby="imageModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
       <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title">Select Voucher Image</h4>
      </div>
      <div class="modal-body">
        <div class="selct-img-voc">
		  <div class="imageBox" >
			<!--<div id="img" ></div>-->
			<!--<img class="cropImg" id="img" style="display: none;" src="images/avatar.jpg" />-->
			<div class="mask"></div>
			<div class="thumbBox"></div>
			<div class="spinner" style="display: none">Loading...</div>
		  </div>
		  <div class="iviewer">
			 <ul class="custom-file-upload">
				 <li><label for="upload-file" class="custom-file-upload"><span ><?php echo $this->Html->image("gallery-img.png",array("alt"=>"Voucher Pic")); ?></span></label> <input id="upload-file" type="file"/></li>
				<li><a href="javascript:void(0)"><span id="rotateLeft"><?php echo $this->Html->image("rotate-left.png",array("alt"=>"Voucher Pic")); ?></span></a></li>
				 <li><a href="javascript:void(0)"><span id="rotateRight"><?php echo $this->Html->image("rotate-right.png",array("alt"=>"Voucher Pic")); ?></span></a></li>
				 <li><a href="javascript:void(0)"><span id="zoomIn"><?php echo $this->Html->image("zoom-out.png",array("alt"=>"Voucher Pic")); ?></span></a></li>
				  <li><a href="javascript:void(0)"><span id="zoomOut"><?php echo $this->Html->image("zoom-in.png",array("alt"=>"Voucher Pic")); ?></span></a></li>
			 </ul>             
		  </div>
		</div>
        
		<div class="iviewer-btn">
			<ul class="list-inline">
			   <li><button class="btn btn-custom" data-dismiss="modal">CLOSE</button></li> 
			   <li><button class="btn btn-custom btn-primary" id="saveimage">SAVE CHANGES</button></li>
			</ul>
		</div>
	 </div>  	
    </div>
  </div>
</div> */ ?>
<div class="modal fade loginModel themeModel" id="imageModal" tabindex="-1" role="dialog" aria-labelledby="imageModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
       <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title">Select Image</h4>
      </div>
      <div class="modal-body" id="actions">
        <div class="selct-img-voc">
			<div class="img-container" style="width:100%;">
				<img src="<?php echo $tmpImage; ?>" alt="Picture">
			</div>
		  <div class="iviewer" >
			 <ul class="custom-file-upload" class="docs-buttons">
				 <li><label for="upload-file" class="custom-file-upload"><span ><?php echo $this->Html->image("gallery-img.png",array("alt"=>"Voucher Pic")); ?></span></label> <input id="upload-file" type="file" name="file" accept="image/*"/></li>
				<li data-method="rotate" data-option="-90"><a href="javascript:void(0)" title="Rotate Left"><span id="rotateLeft"><?php echo $this->Html->image("rotate-left.png",array("alt"=>"Voucher Pic")); ?></span></a></li>
				 <li data-method="rotate" data-option="90"><a href="javascript:void(0)" title="Rotate Right"><span id="rotateRight"><?php echo $this->Html->image("rotate-right.png",array("alt"=>"Voucher Pic")); ?></span></a></li>
				 <li data-method="zoom" data-option="-0.1"><a href="javascript:void(0)" title="Zoom Out"><span id="zoomIn"><?php echo $this->Html->image("zoom-out.png",array("alt"=>"Voucher Pic")); ?></span></a></li>
				  <li data-method="zoom" data-option="0.1"><a href="javascript:void(0)" title="Zoom In"><span id="zoomOut"><?php echo $this->Html->image("zoom-in.png",array("alt"=>"Voucher Pic")); ?></span></a></li>
			 </ul>             
		  </div>
		  <!--div class="btn-group">
          <button type="button" class="btn btn-primary" data-method="move" data-option="-10" data-second-option="0" title="Move Left">
            <span class="docs-tooltip" data-toggle="tooltip" title="cropper.move(-10, 0)">
              <span class="fa fa-arrow-left"></span>
            </span>
          </button>
          <button type="button" class="btn btn-primary" data-method="move" data-option="10" data-second-option="0" title="Move Right">
            <span class="docs-tooltip" data-toggle="tooltip" title="cropper.move(10, 0)">
              <span class="fa fa-arrow-right"></span>
            </span>
          </button>
          <button type="button" class="btn btn-primary" data-method="move" data-option="0" data-second-option="-10" title="Move Up">
            <span class="docs-tooltip" data-toggle="tooltip" title="cropper.move(0, -10)">
              <span class="fa fa-arrow-up"></span>
            </span>
          </button>
          <button type="button" class="btn btn-primary" data-method="move" data-option="0" data-second-option="10" title="Move Down">
            <span class="docs-tooltip" data-toggle="tooltip" title="cropper.move(0, 10)">
              <span class="fa fa-arrow-down"></span>
            </span>
          </button>
        </div-->
		</div>
        
		<div class="iviewer-btn docs-buttons">
			<ul class="list-inline">
			   <li><button class="btn btn-custom" data-dismiss="modal">CLOSE</button></li> 
			   <li><button class="btn btn-custom btn-primary" data-method="getCroppedCanvas" data-option="{ &quot;maxWidth&quot;: 4096, &quot;maxHeight&quot;: 4096 }" id="saveimage">SAVE CHANGES</button></li>
			</ul>
		</div>
	 </div>  	
    </div>
  </div>
</div>
<style>
.img-container {
  /* Never limit the container height here */
  max-width: 100%;
}

.img-container img {
  /* This is important */
  width: 100%;
}
.cropper-bg {background-image:none !important;}
.cropper-modal {background-color:#EEEEEE !important;opacity:0 !important;}

</style>

 <script>
      // This example requires the Places library. Include the libraries=places
      // parameter when you first load the API. For example:
      // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

      function initMap() {
		  		
        var map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: <?php echo $user['UserDetail']['latitude']?>, lng: <?php echo $user['UserDetail']['longitude']?>},
          zoom: 13,
          
        });
        var card = document.getElementById('pac-card');
        var input = document.getElementById('pac-input');
        var types = document.getElementById('type-selector');
        var strictBounds = document.getElementById('strict-bounds-selector');
		
        map.controls[google.maps.ControlPosition.TOP_RIGHT].push(card);

        var autocomplete = new google.maps.places.Autocomplete(input);

        // Bind the map's bounds (viewport) property to the autocomplete object,
        // so that the autocomplete requests use the current map bounds for the
        // bounds option in the request.
        autocomplete.bindTo('bounds', map);
		
        var infowindow = new google.maps.InfoWindow();
        var infowindowContent = document.getElementById('infowindow-content');
        infowindow.setContent(infowindowContent);
        var marker = new google.maps.Marker({
		 position: new google.maps.LatLng( <?php echo $user['UserDetail']['latitude']?>,<?php echo $user['UserDetail']['longitude']?>), 
          map: map   
        });

          autocomplete.addListener('place_changed', function() { 
          infowindow.close();
          marker.setVisible(false);
          var place = autocomplete.getPlace();
          if (!place.geometry) {
            // User entered the name of a Place that was not suggested and
            // pressed the Enter key, or the Place Details request failed.
            //window.alert("No details available for input: '" + place.name + "'");
            return;
          }

          // If the place has a geometry, then present it on a map.
          if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
            var address =place.name;
            var geocoder = new google.maps.Geocoder();
            geocoder.geocode({ 'address': address}, function (results, status) {
                        if (status == google.maps.GeocoderStatus.OK) {
							var latitude = place.geometry.location.lat();
                            var longitude = place.geometry.location.lng();
                            $('#UserDetailLatitude').val(latitude);
                            $('#UserDetailLongitude').val(longitude);
                        } else {
                           
                        }
                    });
            
          } else {
			 
            map.setCenter(place.geometry.location);
            map.setZoom(17);  // Why 17? Because it looks good.
          }
          marker.setPosition(place.geometry.location);
          marker.setVisible(true);

          var address = '';
          if (place.address_components) {
            address = [
              (place.address_components[0] && place.address_components[0].short_name || ''),
              (place.address_components[1] && place.address_components[1].short_name || ''),
              (place.address_components[2] && place.address_components[2].short_name || '')
            ].join(' ');
          }       
          //infowindowContent.children['place-icon'].src = place.icon;
         // infowindowContent.children['place-name'].textContent = place.name;
         // infowindowContent.children['place-address'].textContent = address;
        
             infowindow = new google.maps.InfoWindow({
			content: address
			});
			infowindow.open(map, marker);
        }    
        );	
        
		map.setCenter(new google.maps.LatLng( <?php echo $user['UserDetail']['latitude']?>,<?php echo $user['UserDetail']['longitude']?>));		
        map.setZoom(17);  
        
         if(<?php echo $user['UserDetail']['latitude']?>!=""){
          marker.addListener('click', function() {
			infowindow = new google.maps.InfoWindow({
			content: "<?php echo $user['UserDetail']['address'];?>"
			});
			var addr=$("#pac-input").val(); 
			 infowindow.setContent(addr);
			infowindow.open(map, marker);
			});
		}

	}
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDcBcY5ObGmiZ6TETlFS1cEOi5ttvBRnuw&libraries=places&callback=initMap" async defer></script>
