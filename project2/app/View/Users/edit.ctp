<?php if(isset($user['User']['identifier']) && !empty($user['User']['identifier']) && !empty($user['UserDetail']['fbimage']) ){ 
	$tmpImage = $user['UserDetail']['fbimage'];

	?>
	<script type="text/javascript">
		var avatar = "<?php echo ($user['UserDetail']['fbimage']); ?>";
	</script>
	
<?php } else {
	$tmpImage = (empty($user['UserDetail']['image']) || !file_exists(WWW_ROOT."/img/profile/".$user['UserDetail']['image'])) ?  SITE_LINK.'/img/default_new.png' : SITE_LINK.'img/profile/'.$user['UserDetail']['image'];

	?>	
<script type="text/javascript">
	var avatar = "<?php echo (empty($user['UserDetail']['image']) || !file_exists(WWW_ROOT."/img/profile/".$user['UserDetail']['image'])) ?  SITE_LINK.'/img/default_new.png' : SITE_LINK.'img/profile/'.$user['UserDetail']['image']; ?>";
</script>
<?php } ?>


<?php echo $this->Form->create('User',array("type"=>"file","class"=>"themeForm")); ?> 				
<?php echo $this->Form->input("id",array("value" => $user["User"]["id"])); ?>
<?php echo $this->Form->hidden("UserDetail.id",array("value" => $user["User"]["id"])); ?>

<section class="section">
			<div class="container">
				<div class="row section-full-row">
					<div class="col-md-10 col-md-offset-1">
						<div class="row">
							<div class="col-md-3">
								<div class="your-logo">		
									<?php if(isset($user['User']['identifier']) && !empty($user['User']['identifier']) && !empty($user['UserDetail']['fbimage'] )){												
									   ?>
											<div class="prof-pic user_pic img-upload" style="background-image:url('<?php echo ($user['UserDetail']['fbimage']); ?>');"> 
											<?php echo $this->Form->input("tmpImage",array("label"=>false,"type"=>"textarea","style"=>"display:none;","id"=>"tmpImage")); ?>	
											</div>											
										
										<?php } else { ?>					 
										
											<div class="prof-pic user_pic img-upload" style="background-image:url('<?php echo (empty($user['UserDetail']['image']) || !file_exists(WWW_ROOT."/img/profile/".$user['UserDetail']['image'])) ?  SITE_LINK.'/img/default_new.png' : SITE_LINK.'img/profile/'.$user['UserDetail']['image']; ?>');"> 
											<?php echo $this->Form->input("tmpImage",array("label"=>false,"type"=>"textarea","style"=>"display:none;","id"=>"tmpImage")); ?>	
											</div>	
										<?php } ?>				
								</div>
							</div>
							<div class="col-md-9">
								<div class="form-field">
									<div class="followers-section">
										<span><?php echo count($user['Follower']);?> Followers</span>
									</div>
									<div class="form-heading">
										<?php echo $this->Html->image("icon7.png",array("alt"=>"About Me")); ?>
										 Business Name
									</div>
									 <?php echo $this->Form->input('UserDetail.business',array('label'=>false,"class"=>"form-control","placeholder"=>"Business Name","value"=>  $user["UserDetail"]["business"]));?>	

								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="form-field">
									<div class="form-heading">
										<?php echo $this->Html->image("icon9.png",array("alt"=>"About")); ?> About
									</div>
									 <?php echo $this->Form->input('UserDetail.aboutme',array("class"=>"form-control txt-no-border","type"=>"text","placeholder"=>"","rows"=>3,"maxlength"=>140,"label"=>false,"autocomplete"=>"off","value"=>$user['UserDetail']['aboutme']));	?>
								</div>
							</div>
						</div>



						<div class="row">
							<div class="col-md-12">
								<div class="form-field">
									<div class="form-heading">
										<?php echo $this->Html->image("icon10.png",array("alt"=>"Discount Venue")); ?>
										 Location
									</div>
									 <div id="pac-container"><?php echo $this->Form->input('UserDetail.address',array("value"=>$user["UserDetail"]["address"],"id"=>"pac-input","class"=>"form-control",'label'=>false,"placeholder"=>"Your address, Your Street, Your Town"));?>
									</div>		
									<?php echo $this->Form->hidden('UserDetail.latitude',array("value"=>$user["UserDetail"]["latitude"])); echo $this->Form->hidden('UserDetail.longitude',array("value"=>$user["UserDetail"]["longitude"]));?>			
						
								</div>
							</div>
						</div>
						<div class="big-map">
							 <div id="map" style="height:490px;width:800px;"></div>
							 <div id="infowindow-content">
							  <img src="" width="16" height="16" id="place-icon">
							  <span id="place-name"  class="title"></span><br>
							  <span id="place-address"></span>
							 </div>								
							 
						</div>
					</div>


				</div>
			</div>
			<div class="plan-bottom">
				<div class="container">
					<div class="row">
						<div class="col-md-12 plan-bottom-right">
							<button type="submit" class="btn btn-green" title="Save Changes" >Save Changes</button>
						</div>

					</div>
				</div>
			</div>
		</section>

<!--/. My Account -->
<?php echo $this->Form->end(); ?>
<style>
.imageBox{width:520px !important;height:350px !important;}
</style>

<div class="modal fade loginModel themeModel" id="imageModal" tabindex="-1" role="dialog" aria-labelledby="imageModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
       <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title">Select Image</h4>
      </div>
      <div class="modal-body" id="actions">
        <div class="selct-img-voc">
			<div class="img-container" style="width:100%;">
				<img src="<?php echo $tmpImage; ?>" alt="Picture">
			</div>
		  <div class="iviewer" >
			 <ul class="custom-file-upload" class="docs-buttons">
				 <li><label for="upload-file" class="custom-file-upload"><span ><?php echo $this->Html->image("gallery-img.png",array("alt"=>"Voucher Pic")); ?></span></label> <input id="upload-file" type="file" name="file" accept="image/*"/></li>
				<li data-method="rotate" data-option="-90"><a href="javascript:void(0)" title="Rotate Left"><span id="rotateLeft"><?php echo $this->Html->image("rotate-left.png",array("alt"=>"Voucher Pic")); ?></span></a></li>
				 <li data-method="rotate" data-option="90"><a href="javascript:void(0)" title="Rotate Right"><span id="rotateRight"><?php echo $this->Html->image("rotate-right.png",array("alt"=>"Voucher Pic")); ?></span></a></li>
				 <li data-method="zoom" data-option="-0.1"><a href="javascript:void(0)" title="Zoom Out"><span id="zoomIn"><?php echo $this->Html->image("zoom-out.png",array("alt"=>"Voucher Pic")); ?></span></a></li>
				  <li data-method="zoom" data-option="0.1"><a href="javascript:void(0)" title="Zoom In"><span id="zoomOut"><?php echo $this->Html->image("zoom-in.png",array("alt"=>"Voucher Pic")); ?></span></a></li>
			 </ul>             
		  </div>		
		</div>
        
		<div class="iviewer-btn docs-buttons">
			<ul class="list-inline">
			   <li><button class="btn btn-custom" data-dismiss="modal">CLOSE</button></li> 
			   <li><button class="btn btn-custom btn-primary" data-method="getCroppedCanvas" data-option="{ &quot;maxWidth&quot;: 4096, &quot;maxHeight&quot;: 4096 }" id="saveimage">SAVE CHANGES</button></li>
			</ul>
		</div>
	 </div>  	
    </div>
  </div>
</div>
<style>
.img-container {
  /* Never limit the container height here */
  max-width: 100%;
}

.img-container img {
  /* This is important */
  width: 100%;
}
.cropper-bg {background-image:none !important;}
.cropper-modal {background-color:#EEEEEE !important;opacity:0 !important;}

</style>

 <script>
      // This example requires the Places library. Include the libraries=places
      // parameter when you first load the API. For example:
      // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

      function initMap() {
		  		  
        var map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: <?php echo $user['UserDetail']['latitude']?>, lng: <?php echo $user['UserDetail']['longitude']?>},
          zoom: 17
        });
        var card = document.getElementById('pac-card');
        var input = document.getElementById('pac-input');
        var types = document.getElementById('type-selector');
        var strictBounds = document.getElementById('strict-bounds-selector');

        map.controls[google.maps.ControlPosition.TOP_RIGHT].push(card);

        var autocomplete = new google.maps.places.Autocomplete(input);

        // Bind the map's bounds (viewport) property to the autocomplete object,
        // so that the autocomplete requests use the current map bounds for the
        // bounds option in the request.
        autocomplete.bindTo('bounds', map);
		
        var infowindow = new google.maps.InfoWindow();
        var infowindowContent = document.getElementById('infowindow-content');
        infowindow.setContent(infowindowContent);
        var marker = new google.maps.Marker({
		 position: new google.maps.LatLng( <?php echo $user['UserDetail']['latitude']?>,<?php echo $user['UserDetail']['longitude']?>), 
          map: map   
        });

          autocomplete.addListener('place_changed', function() {
          infowindow.close();
          marker.setVisible(false);
          var place = autocomplete.getPlace();
          if (!place.geometry) {
            // User entered the name of a Place that was not suggested and
            // pressed the Enter key, or the Place Details request failed.
            //window.alert("No details available for input: '" + place.name + "'");
            return;
          }

          // If the place has a geometry, then present it on a map.
          if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
            var address =place.name;
            var geocoder = new google.maps.Geocoder();
            geocoder.geocode({ 'address': address}, function (results, status) {
                        if (status == google.maps.GeocoderStatus.OK) {
							var latitude = place.geometry.location.lat();
                            var longitude = place.geometry.location.lng();
                            $('#UserDetailLatitude').val(latitude);
                            $('#UserDetailLongitude').val(longitude);
                        } else {
                           
                        }
                    });
            
          } else {
			 
            map.setCenter(place.geometry.location);
            map.setZoom(17);  // Why 17? Because it looks good.
          }
          marker.setPosition(place.geometry.location);
          marker.setVisible(true);

          var address = '';
          if (place.address_components) {
            address = [
              (place.address_components[0] && place.address_components[0].short_name || ''),
              (place.address_components[1] && place.address_components[1].short_name || ''),
              (place.address_components[2] && place.address_components[2].short_name || '')
            ].join(' ');
          }       
          infowindowContent.children['place-icon'].src = place.icon;
          infowindowContent.children['place-name'].textContent = place.name;
          infowindowContent.children['place-address'].textContent = address;
         
          infowindow.setContent(address);
		  infowindow.open(map, marker);
        }    
        );	
        
		map.setCenter(new google.maps.LatLng( <?php echo $user['UserDetail']['latitude']?>,<?php echo $user['UserDetail']['longitude']?>));		
        map.setZoom(17);  
        
        
         if(<?php echo $user['UserDetail']['latitude']?>!=""){
          marker.addListener('click', function() {
			var newaddress=$("#pac-input").val(); 
			infowindow = new google.maps.InfoWindow({
			content: newaddress
			});
			var addr=$("#pac-input").val(); 
			 infowindow.setContent(addr);
			infowindow.open(map, marker);
			});
		}

	}
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDcBcY5ObGmiZ6TETlFS1cEOi5ttvBRnuw&libraries=places&callback=initMap" async defer></script>
