<script>
	$(document).ready(function () {
    $('#EmailTemplateAdminEditForm').validate({ // initialize the plugin
        rules: {			
			 
			  "data[EmailTemplate][header]": {
				required: true				
			  },
			 "data[EmailTemplate][subject]": {
				required: true		
			  },
			 "data[EmailTemplate][email_from]": {
				required: true		
			  }	,
			 "data[EmailTemplate][content]": {
				required: true		
			  }	
			},
			// Specify validation error messages
			messages: {			
				"data[EmailTemplate][header]": {
					required: "Please enter title"			
				},
				"data[EmailTemplate][subject]": {
					required: "Please enter subject"			
				},
				"data[EmailTemplate][email_from]": {
					required: "Please enter Email From"			
				},
				"data[EmailTemplate][content]": {
					required: "Please enter Email From"			
				}
			},
			// Make sure the form is submitted to the destination defined
			// in the "action" attribute of the form when valid
			submitHandler: function(form) {
			  form.submit();
			}
		  });		
});
</script>	
<div class="cont-right">
   <div class="cont-right-innr">
	    <div class="main-hd-in">
          <div class="row">
            <div class="col-sm-12">
              <h2 class="title-das">Edit Email Templates</h2>              
            </div>
          </div>
        </div>
		<div class="manage-marchant">
		<?php echo $this->Form->create('EmailTemplate',array("novalidate"=>true)); ?>
			<fieldset>
				
			<?php
				echo $this->Form->input('id');		
				echo $this->Form->input('header');
				echo $this->Form->input('subject');
				echo $this->Form->input('email_from');
				echo $this->Form->input('content', array("type"=>"textarea"));
				
				?>
			</fieldset>
		  <div style="margin-top:20px;margin-left:20px">
				  <button type="submit" class="btn btn-theme" id="save" name="save">SAVE</button>	
				<a href="<?php echo SITE_LINK ?>email-template" class="btn btn-theme">cancel</a>				  
				</div>
		</div>
	
	<?php echo $this->Html->script("ckeditor/ckeditor"); //EmailTemplateContent ?>
	<script>
	$(document).ready(function() { CKEDITOR.replace( 'EmailTemplateContent'); });
	</script>
   </div>		
</div>

