 <section class="inner-banner topheader">
       <div class="container">
	        <div class="banner-cont"> 
	          <h2>BILLING</h2>  
	        </div>
       </div>
</section>
    <!--/. banner sec -->
    
<section>
	<div class="container">
	<?php $i=1;foreach ($plans as $plan): ?>
		<div class="pricing-card">
			<div class="pricing-plan">
				<div class="price-crcl">
				   <p>PRICING PLAN</p>
					<h2><span>£</span> <?php echo  h($plan['Plan']['price']);?>  <small>+VAT /mo</small></h2>
				</div>
				<div class="plan-desc">
					<?php echo htmlspecialchars_decode(h($plan['Plan']['features']));?>                        
				</div>
			</div>
			<div class="price-desc">
				<p><?php echo htmlspecialchars_decode(h($plan['Plan']['description']));?></p>
			</div>                 
		</div>  
		<div class="price-feature">
		
			<?php if ($this->Session->read("Auth.User.is_paid")) { ?>
				<div class="alert alert-success">
				  <strong>Congratulations!</strong> You already subscribed this billing plan.
				</div>
			<?php } else { ?>
			<div class="try-free">
				<a class="btn-gradint mkpayment" href="<?php echo SITE_LINK."buy-plan/".$plan['Plan']['id'] ?>">Buy Plan</a>
			</div>
			<?php } ?>
		</div>
	   <?php $i++;endforeach; ?>	
	</div>
</section>
    <!--/. My Account -->
