<?php echo $this->element("topmenu"); ?>
<div class="userCategories index">
	<h2><?php echo __('User Categories'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('category_id'); ?></th>
			<th><?php echo $this->Paginator->sort('user_id'); ?></th>
			<th><?php echo $this->Paginator->sort('is_active'); ?></th>
			<th><?php echo $this->Paginator->sort('created'); ?></th>
			<th><?php echo $this->Paginator->sort('modified'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php foreach ($userCategories as $userCategory): ?>
	<tr>
		<td><?php echo h($userCategory['UserCategory']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($userCategory['Category']['title'], array('controller' => 'categories', 'action' => 'view', $userCategory['Category']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($userCategory['User']['id'], array('controller' => 'users', 'action' => 'view', $userCategory['User']['id'])); ?>
		</td>
		<td><?php echo h($userCategory['UserCategory']['is_active']); ?>&nbsp;</td>
		<td><?php echo h($userCategory['UserCategory']['created']); ?>&nbsp;</td>
		<td><?php echo h($userCategory['UserCategory']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $userCategory['UserCategory']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $userCategory['UserCategory']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $userCategory['UserCategory']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $userCategory['UserCategory']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<?php  echo $this->element("leftmenu_admin");?>
