 <section class="inner-banner topheader">
       <div class="container">
	        <div class="banner-cont">
	           <h2>INSIGHTS</h2>
	        </div>
       </div>
    </section>
    <!--/. banner sec -->
<?php echo $this->Form->create('vouchersale',array("type"=>"get")); ?>
    <section class="new-vouchers insights">
       <div class="container">            
             <div class="activity">
                    <div class="inside-ttl">
                         <h4>Activity </h4>
                         <p>What does this mean?</p>
                     </div>
                     <div class="tabbable tabs-header">
                      <div class="btn-group" role="group">
                      <?php echo $this->Form->hidden('voucher_status',array("id"=>'voucher_status','value'=>$voucher_status));?>
                          <ul class="nav nav-tabs">
                            <li <?php echo ($voucher_status=="calendar")?'class="active"':''?> ><a href="<?php echo SITE_LINK?>insights?type=calendar" >Added Events</a></li>
                            <li <?php echo ($voucher_status=="viewed" || $voucher_status=="")?'class="active"':''?>><a href="<?php echo SITE_LINK?>insights?type=viewed">Viewed Events</a></li>
                          </ul>
                        </div>

                         <div class="filter-activity dropdown">
                            <button type="button" class="btn dropdown-toggle" data-toggle="dropdown"><?php echo $this->Html->image("filter-icon.png",array("alt"=>"user"));?></button>
                            <ul class="dropdown-menu act-filter" role="menu">
                                <li class="vouchdays"><a data="7 Day">7 Days</a></li>
                                <li><a data="30 Day">30 Days</a></li>
                                <li><a data="3 Month">3 Months</a></li>
                                <li><a data="6 Month">6 Months</a></li>
                                <li><a data="1 Year">1 Year</a></li>
                            </ul>
                         </div>               
                    </div>
              
                   <div class="tab-content tabs-body">
                        <div class="tab-pane active" id="tab1">
                            <div class="voucher-boxCont draft">
                            <?php echo $this->Form->create('Voucher',array("id"=>"VoucherEditForm"));
								echo $this->Form->hidden('interval',array('id'=>'interval','value'=>$interval)); ?>
								<?php if(count($calendar)==0){?><div class="box-card-nodata"><div class="alert alert-warning">No Data to display.</div>
								<?php }else{ foreach ($calendar as $calendardata){ ?>
                                <div class="box-card">
                                    <div class="box-info">
                                        <a href="javascript:void(0);">
                                            <div class="card-img">
												<?php echo ($calendardata['Calendar']['image']=="" || !file_exists(WWW_ROOT."img/events/".$calendardata['Calendar']['image']))?($this->Html->image("default_voucher.jpeg")):($this->Html->image("events/".h($calendardata['Calendar']['image']))); ?>
                                            </div>
                                            <span class="pst-cont"><?php echo $calendardata['SaveCalendar']['count']?></span>
                                        </a>
                                    </div>
                                </div> <!--/. End box-card -->        
                                 <?php }} ?>                               
                                                      
                            </div> <!--/.voucher-boxCont -->
                        </div>
                        <!--/. End tab-panel -->                                             
                   </div>
             </div>  <!--/.Activity -->
       </div>      
    </section>
<?php echo $this->Form->end(); ?> 
