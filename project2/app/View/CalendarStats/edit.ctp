<div class="voucherSales form">
<?php echo $this->Form->create('VoucherSale'); ?>
	<fieldset>
		<legend><?php echo __('Edit Voucher Sale'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('user_id');
		echo $this->Form->input('voucher_id');
		echo $this->Form->input('purchase');
		echo $this->Form->input('purchase_status');
		echo $this->Form->input('is_active');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('VoucherSale.id')), array('confirm' => __('Are you sure you want to delete # %s?', $this->Form->value('VoucherSale.id')))); ?></li>
		<li><?php echo $this->Html->link(__('List Voucher Sales'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Vouchers'), array('controller' => 'vouchers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Voucher'), array('controller' => 'vouchers', 'action' => 'add')); ?> </li>
	</ul>
</div>
