<link rel="stylesheet" type="text/css" href="<?php echo SITE_LINK ?>css/admin-internal.css" />

<div id="total_app_facebook" height="200"></div>

<link rel="stylesheet" type="text/css" href="https://rawgit.com/notryanb/highcharts_date_range_grouping/config-date-formats/dist/highcharts_date_range_grouping.css"> 
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://cdn.rawgit.com/notryanb/highcharts_date_range_grouping/config-date-formats/dist/highcharts_date_range_grouping.min.js"></script>
<script>
Highcharts.setOptions({ colors: ['#e9602e', '#473490','#0e7228','#0c4a87','#785a04','#717171']});
Highcharts.setOptions({colors: Highcharts.map(Highcharts.getOptions().colors, function (color) {
	        return {
            radialGradient: {
				r: 0.6,
                cx: 0.1,
                cy: 0.2                
            },
            stops: [
                [0, color],
                [1, Highcharts.Color(color).brighten(-0.1).get('rgb')] // darken
            ]
        };
    })
});
var categories = [
  'Demo1'
  ].map(function(date) {
    let formatOptions = { month: '2-digit', day: '2-digit', year: 'numeric' };
    return new Date(date).toLocaleDateString(undefined, formatOptions); 
  });
</script>
<!--facebook Verified-->
<script>
	Highcharts.chart('total_app_facebook', {
		 chart: {     
			
		},
		title: {
			text: ''
		},
        yAxis:[{           
            allowDecimals: false            
        }],
		xAxis: {
			 categories: [<?php $i=0; foreach( $total_app_facebook as $key=>$val )		 
			 {
				 if ( $i > 0 ) {
					 echo ",'".$key."'";
				 }
				 else{
					  echo "'".$key."'";
				 }
				 $i++;
			}		 
		  ?>]
		},
		legend: {
			enabled: true
		},
		tooltip: {
			formatter: function () {
				return this.y ;
			}
		},
		labels: {
			items: [{
				html: '',
				style: {
					left: '50px',
					top: '18px',
					color: (Highcharts.theme && Highcharts.theme.textColor) || 'black'
				}
			}]
		},
		 plotOptions: {
			series: {
				dataLabels: {
					enabled: false
				}
			}
		},
		series: [{
			type: 'column',
			name: 'Facebook',
			data:  [<?php $i=0; foreach( $total_app_facebook as $key=>$val )		 
			 {
				 if ( $i > 0 ) {
					 echo ",".$val;
				 }
				 else{
					  echo $val;
				 }
				 $i++;
			}		 
		  ?>]        
		},{
			type: 'column',
			name: 'Manual',
			data:  [<?php $i=0; foreach( $total_app_manual as $key=>$val )		 
			 {
				 if ( $i > 0 ) {
					 echo ",".$val;
				 }
				 else{
					  echo $val;
				 }
				 $i++;
			}		 
		  ?>]        
		}
		
		],
			marker: {
				lineWidth: 2,
				lineColor: Highcharts.getOptions().colors[3],
				fillColor: 'white'
			}		
	});
</script>
