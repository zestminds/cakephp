<div class="cont-right">
	<div class="cont-right-innr">
		<div class="main-hd-in">
			<div class="row">
				<div class="col-md-4">
					<h2 class="title-das">Merchant Insights</h2>
				</div>
			   <div class="col-md-8">
				   <div class="tabs-header">
					   <div class="btn-group" role="tablist">
						<ul class="nav nav-tabs" >
						<li role="presentation"><a href="<?php echo SITE_LINK."view-insights" ?>" aria-controls="home" role="tab">Students</a></li>
						<li role="presentation"  class="active"><a href="<?php echo SITE_LINK."view-merchant-insights" ?>" aria-controls="profile" role="tab" >Merchants</a></li>
						</ul>
					   </div>            				
				  </div>
			   </div>	
			</div>
		</div>	
		<!-- Tab panes -->
		<div class="tab-content">
		  <div role="tabpanel" class="tab-pane active" id="home">
			  <div class="total-no">
				<div class="col-lg-12 col-md-12 pd">
					<div class="voucher-bx">
						<?php echo $this->Form->create("totalrenewals",array("id"=>"totalappdownlods","name"=>"totalappdownlods","type"=>"post"));  ?>
							<div class="voucher-bx-int">
								 <h1> Total App Downloads </h1>
								<div class="hd-vchr">
									<div class="vc-left">
										<p>Location</p>
									</div>
									<div class="vc-right">
										<?php echo $this->Form->input('loc_renewals',array("name"=>"loc_renewals","id"=>"loc_renewals",'label'=>false,"options"=>$location,"empty"=>"All",'class'=>'form-control number-of-vc',"selected"=>$loc_renewals));?>
										
									</div>
								</div>
								<div class="vc-chart-main">
									<iframe id="chart_renewals" sandbox="allow-same-origin allow-scripts allow-forms" class="charts_download" frameborder="0" src="<?php SITE_LINK ?>total_renewals/?days=7&location=All"></iframe>
								</div>
							</div>
							<div class="voucher-bx-int">
								 <h1> Total Logins </h1>
								<div class="hd-vchr">													
									 <div class="vc-left">
										<p>Select Time</p>	
									</div>
									<div class="vc-right">
									<?php echo $this->Form->input('days_renewals',array("name"=>"days_renewals","id"=>"days_renewals",'label'=>false,"options"=>$records,'class'=>'form-control Insights-select',"selected"=>$days_renewals));?>	
									</div>											
								</div>
								<div class="vc-chart-main">
									<iframe id="chart_logins" sandbox="allow-same-origin allow-scripts allow-forms" class="charts_download" frameborder="0" src="<?php SITE_LINK ?>total_merchant_logins/?days=7&location=All"></iframe>
								</div>
							<?php echo $this->Form->end();?>
						</div>
					</div>
				 </div>                
				</div>
		   
				<!-- Facebook -->
				 <div class="total-no">
					<div class="col-lg-12 col-md-12 pd">
						<div class="voucher-bx">
								<?php echo $this->Form->create("totalrenewals",array("id"=>"totalappdownlods","name"=>"totalappdownlods","type"=>"post"));
							 ?>
								<div class="voucher-bx-int">
									 <h1> Total Registration: Manual v/s FaceBook </h1>
									<div class="hd-vchr">
										<div class="vc-left">
											<p>Location</p>
										</div>
										<div class="vc-right">
											<?php echo $this->Form->input('loc_reg',array("name"=>"loc_reg","id"=>"loc_reg",'label'=>false,"options"=>$location,"empty"=>"All",'class'=>'form-control number-of-vc'));?>
											
										</div>
									</div>
									<div class="vc-chart-main">
										<iframe id="chart_reg" sandbox="allow-same-origin allow-scripts allow-forms" class="charts_download" frameborder="0" src="<?php SITE_LINK ?>total_merchant_registrations/?days=7&location=All"></iframe>
									</div>
								</div>
								<div class="voucher-bx-int">
									 <h1> Total Renewals: Paid v/s Suspended </h1>
									<div class="hd-vchr">													
										 <div class="vc-left">
											<p>Select Time</p>	
										</div>
										<div class="vc-right">
										<?php echo $this->Form->input('days_reg',array("name"=>"days_reg","id"=>"days_reg",'label'=>false,"options"=>$records,'class'=>'form-control Insights-select'));?>	
										</div>											
									</div>
									<div class="vc-chart-main">
										<iframe id="chart_paidsus" sandbox="allow-same-origin allow-scripts allow-forms" class="charts_download" frameborder="0" src="<?php //SITE_LINK ?>total_merchant_paidsus/?days=7&location=All"></iframe>
									</div>
								<?php echo $this->Form->end();?>
							</div>
						</div>
					</div>
				 </div>
				<!-- Facebook -->
				
				
				<!-- Vouchers Reach -->
				 <div class="total-no">
					<div class="col-lg-12 col-md-12 pd">
						<div class="voucher-bx">
							 <?php echo $this->Form->create("totalreachcon",array("id"=>"totalreachcon","name"=>"totalreachcon","type"=>"post")); ?>			 
							  <div class="list-select">
									<div class="col-sm-4">
										<div class="hd-vchr">
											<div class="vc-left">
												<p>Location</p>
											</div>
											<div class="vc-right">
												<?php echo $this->Form->input('loc_reachcon',array("name"=>"loc_reachcon","id"=>"loc_reachcon",'label'=>false,"options"=>$location,"empty"=>"All",'class'=>'form-control number-of-vc'));?>
											</div>
										</div>
								   </div>	
								   <div class="col-sm-4">
										<div class="hd-vchr">
											<div class="vc-left">
												<p>Business Name</p>
											</div>
											<div class="vc-right">
												<?php echo $this->Form->input('merchant_reachcon',array("name"=>"merchant_reachcon","id"=>"merchant_reachcon","empty"=>"All",'label'=>false,"options"=>$merhchantname,'class'=>'form-control Insights-select'));?>	
											</div>
										</div>
									</div>									
									<div class="col-sm-4">
										<div class="hd-vchr">
											<div class="vc-left">
												<p>Time</p>
											</div>
											<div class="vc-right">
												<?php echo $this->Form->input('days_reachcon',array("name"=>"days_reachcon","id"=>"days_reachcon",'label'=>false,"options"=>$records,'class'=>'form-control Insights-select'));?>	
											</div>
										</div>
									</div>	
							 </div>
							 <?php echo $this->Form->end();?>
							 <div class="vc-chart-main">
								<iframe id="chart_reachconversions" sandbox="allow-same-origin allow-scripts allow-forms" class="charts_draft" frameborder="0" src="<?php SITE_LINK ?>total_merchant_reachconversion/?days=7&location=All&merchant=All"></iframe>
							</div>					
						</div>
					</div>
				 </div>
				<!-- Vouchers Reach -->
				
				<!-- Vouchers Published -->
				 <div class="total-no">
					<div class="col-lg-12 col-md-12 pd">
						<div class="voucher-bx">
							 <?php echo $this->Form->create("totalreachcon",array("id"=>"totalreachcon","name"=>"totalreachcon","type"=>"post"));?>			 
							  <div class="list-select">
									<div class="col-sm-4">
										<div class="hd-vchr">
											<div class="vc-left">
												<p>Location</p>
											</div>
											<div class="vc-right">
												<?php echo $this->Form->input('loc_draft',array("name"=>"loc_draft","id"=>"loc_draft",'label'=>false,"options"=>$location,"empty"=>"All",'class'=>'form-control number-of-vc',"selected"=>$loc_draft));?>
											</div>
										</div>
								   </div>	
								   <div class="col-sm-4">
										<div class="hd-vchr">
											<div class="vc-left">
												<p>Business Name</p>
											</div>
											<div class="vc-right">
												<?php echo $this->Form->input('merchant_draft',array("name"=>"merchant_draft","id"=>"merchant_draft","empty"=>"All",'label'=>false,"options"=>$merhchantname,'class'=>'form-control Insights-select',"selected"=>$merchant_draft));?>	
											</div>
										</div>
									</div>									
									<div class="col-sm-4">
										<div class="hd-vchr">
											<div class="vc-left">
												<p>Time</p>
											</div>
											<div class="vc-right">
												<?php echo $this->Form->input('days_draft',array("name"=>"days_draft","id"=>"days_draft",'label'=>false,"options"=>$records,'class'=>'form-control Insights-select',"selected"=>$days_draft));?>	
											</div>
										</div>
									</div>	
							 </div>
							 <?php echo $this->Form->end();?>
							 <div class="vc-chart-main">
								<iframe id="chart_draft" sandbox="allow-same-origin allow-scripts allow-forms" class="charts_draft" frameborder="0" src="<?php SITE_LINK ?>total_merchant_drafted/?days=7&location=All&merchant=All"></iframe>
							</div>					
						</div>
					</div>
				 </div>
				<!-- Vouchers Published -->
				
				
				
			</div>                
		</div>
	</div>
</div>
