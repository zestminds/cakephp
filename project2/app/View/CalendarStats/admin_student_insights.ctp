 <div class="cont-right">
      <div class="cont-right-innr">
            	<div class="main-hd-in">
                	<div class="row">
                    	<div class="col-md-4">
							<h2 class="title-das">App User Insights</h2>
						</div>
         			   <div class="col-md-8">
						   <div class="tabs-header">
							   <div class="btn-group" role="tablist">
								<ul class="nav nav-tabs" >
								<li role="presentation" class="active"><a href="<?php echo SITE_LINK."student_insights" ?>" aria-controls="home" role="tab" >App User</a></li>
								<li role="presentation"><a href="<?php echo SITE_LINK."view-merchant-insights" ?>" aria-controls="profile" role="tab" >Event Manager</a></li>
								</ul>
							   </div>            				
						  </div>
           			   </div>	
                    </div>
                </div>	
               <!-- Tab panes -->
	              <div class="tab-content">
              		  <div role="tabpanel" class="tab-pane active" id="home">
                      <div class="total-no">
                      	<div class="col-lg-12 col-md-12 pd">
							<div class="voucher-bx">
								<?php echo $this->Form->create("totalappdownlods",array("id"=>"totalappdownlods","name"=>"totalappdownlods","type"=>"post")); 
										 echo $this->Form->input('form_submit',array("id"=>"form_submit","type"=>"hidden",'label'=>false));
								 ?>
									<div class="voucher-bx-int">
										 <h1> Total App Downloads </h1>
										<div class="hd-vchr" style="height:40px">										
											
										</div>
										<div class="vc-chart-main">
											<iframe id="chart_download_app" sandbox="allow-same-origin allow-scripts allow-forms" class="charts_download" frameborder="0" src="<?php SITE_LINK ?>total_app_downloads/?days=7"></iframe>
										</div>
									</div>
									<div class="voucher-bx-int">
										 <h1> Total Logins </h1>
										<div class="hd-vchr">													
											 <div class="vc-left">
												<p>Select Time	<?php echo $this->Form->input('days_appdown',array("name"=>"days_appdown","id"=>"days_appdown",'div'=>false,'label'=>false,"options"=>$records,'class'=>'form-control Insights-select',"selected"=>$days_appdown));?>	
</p>	
											</div>
																					
										</div>
										<div class="vc-chart-main">
											<iframe id="chart_logins" sandbox="allow-same-origin allow-scripts allow-forms" class="charts_download" frameborder="0" src="<?php SITE_LINK ?>total_app_downloads/?days=7"></iframe>
										</div>
									<?php echo $this->Form->end();?>
								</div>
							</div>
						</div>                
                		</div>
                        
                         <div class="total-no">
							 <div class="col-lg-12 col-md-12 pd">
								<div class="voucher-bx">
									<?php echo $this->Form->create("totalregver",array("id"=>"totalregver","name"=>"totalregver","type"=>"post")); 
											 echo $this->Form->input('form_submit',array("id"=>"form_submit_reg","type"=>"hidden",'label'=>false));
									 ?>
										<div class="voucher-bx-int">
											 <h1>  Total Registration: Manual v/s FaceBook </h1>
											<div class="hd-vchr" style="height:40px">									
											
											</div>
											<div class="vc-chart-main">												
											<iframe id="chart_registrations" sandbox="allow-same-origin allow-scripts allow-forms" class="charts_download" frameborder="0" src="<?php SITE_LINK ?>total_registrations/?days=7"></iframe>

											</div>
										</div>
										<div class="voucher-bx-int">
											 <h1>Total Verifications: Verified v/s Non-Verified</h1>
											<div class="hd-vchr">													
												 <div class="vc-left">
													<p>Select Time <?php echo $this->Form->input('days_regver',array("name"=>"days_regver","id"=>"days_regver",'label'=>false,'div'=>false,"options"=>$records,'class'=>'form-control Insights-select',"selected"=>$days_regver));?>	
</p>	
												</div>																					
											</div>
											<div class="vc-chart-main">												
												<iframe id="chart_verifications" sandbox="allow-same-origin allow-scripts allow-forms" class="charts_download" frameborder="0" src="<?php SITE_LINK ?>total_verifications/?days=7"></iframe>

											</div>
										<?php echo $this->Form->end();?>
									</div>
								</div>
							</div>                
                        </div>	
                        
                         <div class="total-no">
                         	<div class="col-lg-12 col-md-12 pd">
                	     <div class="voucher-bx">	
							 <?php echo $this->Form->create("totalreachcon",array("id"=>"totalreachcon","name"=>"totalreachcon","type"=>"post")); 
									 echo $this->Form->input('form_submit',array("id"=>"form_submit_recon","type"=>"hidden",'label'=>false));
							  ?>			 
							  <div class="list-select">
									<div class="col-sm-6">
										<div class="hd-vchr">
											<div class="vc-left">
												<p>Name of University</p>
											</div>
											<div class="vc-right">
												<?php echo $this->Form->input('uni_reachcon',array("name"=>"uni_reachcon","id"=>"uni_reachcon",'label'=>false,"options"=>$university,"empty"=>"All",'class'=>'form-control number-of-vc',"selected"=>$uni_reachcon));?>
											</div>
										</div>
								   </div>									
									<div class="col-sm-6">
										<div class="hd-vchr">
											<div class="vc-left">
												<p>Time</p>
											</div>
											<div class="vc-right">
												<?php echo $this->Form->input('days_reachcon',array("name"=>"days_reachcon","id"=>"days_reachcon",'label'=>false,"options"=>$records,'class'=>'form-control Insights-select',"selected"=>$days_reachcon));?>	
											</div>
										</div>
									</div>	
							 </div>
							 <?php echo $this->Form->end();?>
                             <div class="vc-chart-main">
								<iframe id="chart_reachconversions" sandbox="allow-same-origin allow-scripts allow-forms" class="charts_reachconv" frameborder="0" src="<?php SITE_LINK ?>total_reachconversions/?days=7&university=All"></iframe>
							</div>
							
						</div>
					
						<!-- voucherbox -->
					</div>
                         </div>
                        
                        
                      </div>                
                     </div>
                 </div>
      </div>
</div>
<link rel="stylesheet" type="text/css" href="https://rawgit.com/notryanb/highcharts_date_range_grouping/config-date-formats/dist/highcharts_date_range_grouping.css"> 
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://cdn.rawgit.com/notryanb/highcharts_date_range_grouping/config-date-formats/dist/highcharts_date_range_grouping.min.js"></script>
<script>
Highcharts.setOptions({ colors: ['#e9602e', '#473490','#0e7228','#0c4a87','#785a04','#717171']});
Highcharts.setOptions({colors: Highcharts.map(Highcharts.getOptions().colors, function (color) {
	        return {
            radialGradient: {
				r: 0.6,
                cx: 0.1,
                cy: 0.2                
            },
            stops: [
                [0, color],
                [1, Highcharts.Color(color).brighten(-0.1).get('rgb')] // darken
            ]
        };
    })
});
var categories = [
  'Demo1'
  ].map(function(date) {
    let formatOptions = { month: '2-digit', day: '2-digit', year: 'numeric' };
    return new Date(date).toLocaleDateString(undefined, formatOptions); 
  });
</script>


<!--facebook Verified-->
