 <section class="inner-banner topheader">
       <div class="container">
	        <div class="banner-cont">
	           <h2>INSIGHTS</h2>
	        </div>
       </div>
    </section>
    <!--/. banner sec -->
<?php echo $this->Form->create('vouchersale',array("type"=>"get")); ?>
    <section class="new-vouchers insights">
       <div class="container">
             <div class="audience">
                 <div class="inside-ttl">
                     <h4>Audience </h4>
                     <p>What does this mean?</p>
                 </div>
                 <div class="theme-chart row">
                     <div class="col-sm-6 chartBox">
                        <div class="chart-card" style="height: 373px;">
							<div class="chart_title">Gender</div>
                           <div id="chart_gender"></div>
                           <div class="chart_desc">								
								<?php foreach( $genderArr as $key=>$val ) {
								?>								
								<div class="img_gender"><div class="img_inner"><?php echo $this->Html->image($val['image']);?></div>
								<div class="female-data"><?php echo $val['val']; echo "%<br>".$val['gender']; ?> </div></div>
								
								<?php		
								}
								?>
                           </div>
                        </div>
                     </div>
                     <div class="col-sm-6 chartBox">
                        <div class="chart-card">
							<div class="chart_title"><span class="showWeek">Days</span>  <span class="showHour">Hours</span></div>
							<div id="chart_week">								
								<div id="audience_weekly"></div>
                            </div>
                            <div id="chart_hour" style="display:none">								
								<div id="audience_hourly"></div>
                            </div>
                        </div>
                     </div>
                 </div>
             </div> <!--/.Audience --><a href="javascript:void()" id="top"> </a>
             <div class="activity">
                    <div class="inside-ttl">
                         <h4>Activity </h4>
                         <p>What does this mean?</p>
                     </div>
                     <div class="tabbable tabs-header">
                      <div class="btn-group" role="group">
                      <?php echo $this->Form->hidden('voucher_status',array("id"=>'voucher_status','value'=>$voucher_status));?>
                          <ul class="nav nav-tabs">
                            <li <?php echo ($voucher_status=="Reach")?'class="active"':''?> ><a href="<?php echo SITE_LINK?>insights?type=Reach" >Reach</a></li>
                            <li <?php echo ($voucher_status=="Impression" || $voucher_status=="")?'class="active"':''?>><a href="<?php echo SITE_LINK?>insights?type=Impression">Impressions</a></li>
                            <li <?php echo ($voucher_status=="Convert")?'class="active"':''?>><a href="<?php echo SITE_LINK?>insights?type=Convert">Conversions</a></li>
                          </ul>
                        </div>

                         <div class="filter-activity dropdown">
                            <button type="button" class="btn dropdown-toggle" data-toggle="dropdown"><?php echo $this->Html->image("filter-icon.png",array("alt"=>"user"));?></button>
                            <ul class="dropdown-menu act-filter" role="menu">
                                <li class="vouchdays"><a data="7 Day">7 Days</a></li>
                                <li><a data="30 Day">30 Days</a></li>
                                <li><a data="3 Month">3 Months</a></li>
                                <li><a data="6 Month">6 Months</a></li>
                                <li><a data="1 Year">1 Year</a></li>
                            </ul>
                         </div>               
                    </div>
              
                   <div class="tab-content tabs-body">
                        <div class="tab-pane active" id="tab1">
                            <div class="voucher-boxCont draft">
                            <?php echo $this->Form->create('Voucher',array("id"=>"VoucherEditForm"));
								echo $this->Form->hidden('interval',array('id'=>'interval','value'=>$interval)); ?>
								<?php if(count($voucher_live_status)==0){?><div class="box-card-nodata"><div class="alert alert-warning">No Data to display.</div><?php }else{ foreach ($voucher_live_status as $voucherdata){ ?>
                                <div class="box-card">
                                    <div class="box-info">
                                        <a href="javascript:void(0);">
                                            <div class="card-img">
												<?php echo ($voucherdata['Voucher']['image']=="" || !file_exists(WWW_ROOT."img/voucher/".$voucherdata['Voucher']['image']))?($this->Html->image("default_voucher.jpeg")):($this->Html->image("voucher/".h($voucherdata['Voucher']['image']))); ?>
                                            </div>
                                            <span class="pst-cont"><?php echo $voucherdata['VoucherStat']['count']?></span>
                                        </a>
                                    </div>
                                </div> <!--/. End box-card -->        
                                 <?php }} ?>                               
                                                      
                            </div> <!--/.voucher-boxCont -->
                        </div>
                        <!--/. End tab-panel -->                                             
                   </div>
             </div>  <!--/.Activity -->
       </div>      
    </section>
<?php echo $this->Form->end(); ?> 
    <!--/. My Account -->
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/highcharts-3d.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/series-label.js"></script>



<script>
Highcharts.setOptions({
 colors: ['#5dc3f3', '#f18058','#0e7228','#0c4a87','#785a04','#717171']
});
Highcharts.setOptions({
    colors: Highcharts.map(Highcharts.getOptions().colors, function (color) {
        return {
            radialGradient: {
				r: 0.6,
                cx: 0.1,
                cy: 0.2                
            },
            stops: [
                [0, color],
                [1, Highcharts.Color(color).brighten(-0.1).get('rgb')] // darken
            ]
        };
    })
});


Highcharts.chart('chart_gender', {
    chart: {
        type: 'pie',
        height: 300,
        width: 330,
        options3d: {
            enabled: true,
            alpha: 10
        }
    },
    title: {
        text: ''
    },
    plotOptions: {
        pie: {
            innerSize: 100,
            depth: 80,
           
            dataLabels: {
                enabled: false,
            }
        }
    },
        tooltip: {
            formatter: function(){
                var point = this.point;
                console.log(point);
                return '<b>' + point.name + ': ' + point.y + '%';
            }
        },
    series: [{
        name: '%',
        data: [
			<?php
				foreach( $genderArr as $key=>$val ) {
				if ( $key > 0 ) {
			?>
				,['<?php echo $val['gender']; ?>',<?php echo $val['val']; ?>]
			<?php		
				} else  {
			?>
				['<?php echo $val['gender']; ?>',<?php echo $val['val']; ?>]
			<?php		
				}
				}
			?>     
        ]
    }]
});


Highcharts.chart('audience_weekly', {
	 chart: {     
        height: 325,
        width: 520
    },
    title: {
        text: ''
    },
    xAxis: {
		 categories: [<?php $i=0; foreach( $finalArr as $key=>$val )		 
		 {
			 if ( $i > 0 ) {
				 echo ",'".$key."'";
			 }
			 else{
				  echo "'".$key."'";
			 }
			 $i++;
		}		 
	  ?>]
    },
    legend: {
        enabled: false
    },
    tooltip: {
        formatter: function () {
            return this.y;
        }
    },
     plotOptions: {
        series: {
            dataLabels: {
                enabled: false
            }
        }
    },yAxis:[{           
            allowDecimals: false            
        }],
    series: [{
        type: 'column',
        name: '',
        data:  [<?php $i=0; foreach( $finalArr as $key=>$val )		 
		 {
			 if ( $i > 0 ) {
				 echo ",".$val;
			 }
			 else{
				  echo $val;
			 }
			 $i++;
		}		 
	  ?>]        
    } ,{
        type: 'line',
        name: 'Students',
        visible: true,        
        data: [<?php $i=0; foreach( $finalArr as $key=>$val )		 
		 {
			 if ( $i > 0 ) {
				 echo ",".$val;
			 }
			 else{
				  echo $val;
			 }
			 $i++;
		}		 
	  ?>],
        marker: {
            lineWidth: 2,
            lineColor: Highcharts.getOptions().colors[3],
            fillColor: 'white'
        }
    }]
});
Highcharts.chart('audience_hourly', {
	 chart: {     
        height: 325,
        width: 520
    },
    title: {
        text: ''
    },
    xAxis: {
		 categories: [<?php $i=0; foreach( $finalHourArr as $key=>$val )		 
		 {
			 if ( $i > 0 ) {
				 echo ",'".$key."'";
			 }
			 else{
				  echo "'".$key."'";
			 }
			 $i++;
		}		 
	  ?>]
    },
    legend: {
        enabled: false
    },
    tooltip: {
        formatter: function () {
            return this.y;
        }
    },
    labels: {
        items: [{
            html: '',
            style: {
                left: '50px',
                top: '18px',
                color: (Highcharts.theme && Highcharts.theme.textColor) || 'black'
            }
        }]
    },
     plotOptions: {
        series: {
            dataLabels: {
                enabled: false
            }
        }
    },
    yAxis:[{           
            allowDecimals: false            
        }],
    series: [{
        type: 'line',
        name: 'Students',
        visible: true,        
        data: [<?php $i=0; foreach( $finalHourArr as $key=>$val )		 
		 {
			 if ( $i > 0 ) {
				 echo ",".$val;
			 }
			 else{
				  echo $val;
			 }
			 $i++;
		}
	  ?>],
        marker: {
            lineWidth: 2,
            lineColor: Highcharts.getOptions().colors[3],
            fillColor: 'white'
        }
    }]
});

</script>
