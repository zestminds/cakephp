 <div class="cont-right">
      <div class="cont-right-innr">
            	<div class="main-hd-in">
                	<div class="row">
                    	<div class="col-md-4">
            	<h2 class="title-das">Insights</h2>
            </div>
         			   <div class="col-md-8">
                       <div class="tabs-header">
                       <div class="btn-group" role="tablist">
                       	<ul class="nav nav-tabs" >
			        <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Students</a></li>
    		    	<li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Merchants</a></li>
			   </ul>
                       </div>
            				
                      </div>
           			   </div>	
                    </div>
                </div>	
               <!-- Tab panes -->
	              <div class="tab-content">
              		  
                	 <div role="tabpanel" class="tab-pane" id="profile">
                         <div class="total-no">
                      	<div class="col-lg-6 col-md-6 pd">
                	<div class="voucher-bx">
                    	 <h1> Lorem ipsum is dummy data </h1>
                    	<div class="hd-vchr">
                        	<div class="vc-left">
                            	<p>Name of University</p>
                            </div>
                            <div class="vc-right">
                            	<select class="form-control Insights-select">
                                	<option>University 1</option>
                                </select>
                            </div>
                        </div>
                        <div class="vc-chart-main">
                        	<img src="images/total-download-app.jpg" alt="">
                        </div>
                    </div>
                </div>
                		<div class="col-lg-6 col-md-6 pd">
                	<div class="voucher-bx">
                    	 <h1> Lorem ipsum is dummy data </h1>
                    	<div class="hd-vchr">
                        	<div class="vc-left">
                            	<p>Select Time</p>
                            </div>
                            <div class="vc-right">
                            	<select class="form-control Insights-select">
                                	<option>6 Months</option>
                                </select>
                            </div>
                        </div>
                        <div class="vc-chart-main">
                        	<img src="images/total-download-app.jpg" alt="">
                        </div>
                    </div>
                </div>
                		</div>
                        
                         <div class="total-no">
                      	   <div class="col-lg-6 col-md-6 pd">
                	<div class="voucher-bx">
                    	 <h1> Total Registration: Manual v/s FaceBook
 </h1>
                    	<div class="hd-vchr">
                        	<div class="vc-left">
                            	<p>Name of University</p>
                            </div>
                            <div class="vc-right">
                            	<select class="form-control Insights-select">
                                	<option>University 2</option>
                                </select>
                            </div>
                        </div>
                        <div class="vc-chart-main">
                        	<img src="images/manual-vs-fb.jpg" alt="">
                        </div>
                    </div>
                </div>
                			<div class="col-lg-6 col-md-6 pd">
                	<div class="voucher-bx">
                    	 <h1>Total Verifications: Verified v/s Non-Verified
 </h1>
                    	<div class="hd-vchr">
                        	<div class="vc-left">
                            	<p>Select Time</p>
                            </div>
                            <div class="vc-right">
                            	<select class="form-control Insights-select">
                                	<option>30 Days</option>
                                </select>
                            </div>
                        </div>
                        <div class="vc-chart-main">
                        	<img src="images/manual-vs-fb.jpg" alt="">
                        </div>
                    </div>
                </div>
                        </div>	
                        
                         <div class="total-no">
                         	<div class="col-lg-12 col-md-12 pd">
                	     <div class="voucher-bx">
                              <div class="list-select">
                    			<div class="col-sm-6">
                                	<div class="hd-vchr">
                        	<div class="vc-left">
                            	<p>Name of University</p>
                            </div>
                            <div class="vc-right">
                            	<select class="form-control Insights-select">
                                	<option>University 2</option>
                                </select>
                            </div>
                        </div>
                                </div>
                                
                                <div class="col-sm-6">
                                	<div class="hd-vchr">
                        	<div class="vc-left">
                            	<p>Time</p>
                            </div>
                            <div class="vc-right">
                            	<select class="form-control Insights-select">
                                	<option>University 2</option>
                                </select>
                            </div>
                        </div>
                                </div>
                          </div>
                              <div class="vc-chart-main">
                        	<img src="images/nane-of-univercity.jpg" alt="">
                        </div>
                    </div>
                </div>
                         </div>
                     </div>
                 </div>
      </div>
    </div>
<link rel="stylesheet" type="text/css" href="https://rawgit.com/notryanb/highcharts_date_range_grouping/config-date-formats/dist/highcharts_date_range_grouping.css"> 
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://cdn.rawgit.com/notryanb/highcharts_date_range_grouping/config-date-formats/dist/highcharts_date_range_grouping.min.js"></script>
<script>
Highcharts.setOptions({ colors: ['#e9602e', '#473490','#0e7228','#0c4a87','#785a04','#717171']});
Highcharts.setOptions({colors: Highcharts.map(Highcharts.getOptions().colors, function (color) {
	        return {
            radialGradient: {
				r: 0.6,
                cx: 0.1,
                cy: 0.2                
            },
            stops: [
                [0, color],
                [1, Highcharts.Color(color).brighten(-0.1).get('rgb')] // darken
            ]
        };
    })
});
var categories = [
  'Demo1'
  ].map(function(date) {
    let formatOptions = { month: '2-digit', day: '2-digit', year: 'numeric' };
    return new Date(date).toLocaleDateString(undefined, formatOptions); 
  });
</script>
<?php  if($show_app_down =="weeks"){?>
<script>
	Highcharts.chart('student_total_app_downloads', {
		 chart: {     
			height: 325,
			width: 480
		},
		title: {
			text: ''
		},
		xAxis: {
			 categories: [<?php $i=0; foreach( $total_app_downloads as $key=>$val )		 
			 {
				 if ( $i > 0 ) {
					 echo ",'".$key."'";
				 }
				 else{
					  echo "'".$key."'";
				 }
				 $i++;
			}		 
		  ?>]
		},
		legend: {
			enabled: false
		},
		tooltip: {
			formatter: function () {
				return '<b>User:</b> '+this.y;
			}
		},
		labels: {
			items: [{
				html: '',
				style: {
					left: '50px',
					top: '18px',
					color: (Highcharts.theme && Highcharts.theme.textColor) || 'black'
				}
			}]
		},
		 plotOptions: {
			series: {
				dataLabels: {
					enabled: false
				}
			}
		},
		series: [{
			type: 'column',
			name: '',
			data:  [<?php $i=0; foreach( $total_app_downloads as $key=>$val )		 
			 {
				 if ( $i > 0 ) {
					 echo ",".$val;
				 }
				 else{
					  echo $val;
				 }
				 $i++;
			}		 
		  ?>]        
		}],
			marker: {
				lineWidth: 2,
				lineColor: Highcharts.getOptions().colors[3],
				fillColor: 'white'
			}		
	});
	
	Highcharts.chart('student_total_logins', {
		 chart: {     
			height: 325,
			width: 490
		},
		title: {
			text: ''
		},
		xAxis: {
			 categories: [<?php $i=0; foreach( $total_logins as $key=>$val )		 
			 {
				 if ( $i > 0 ) {
					 echo ",'".$key."'";
				 }
				 else{
					  echo "'".$key."'";
				 }
				 $i++;
			}		 
		  ?>]
		},
		legend: {
			enabled: false
		},
		tooltip: {
			formatter: function () {
				return '<b>User:</b> '+this.y;
			}
		},
		labels: {
			items: [{
				html: '',
				style: {
					left: '50px',
					top: '18px',
					color: (Highcharts.theme && Highcharts.theme.textColor) || 'black'
				}
			}]
		},
		 plotOptions: {
			series: {
				dataLabels: {
					enabled: false
				}
			}
		},
		series: [{
			type: 'column',
			name: '',
			data:  [<?php $i=0; foreach( $total_logins as $key=>$val )		 
			 {
				 if ( $i > 0 ) {
					 echo ",".$val;
				 }
				 else{
					  echo $val;
				 }
				 $i++;
			}		 
		  ?>]        
		}],
			marker: {
				lineWidth: 2,
				lineColor: Highcharts.getOptions().colors[3],
				fillColor: 'white'
			}		
	});
	
	
	
	
	
</script>
<?php }
elseif($show_app_down =="days"){?>
<script>
	Highcharts.chart('student_total_app_downloads', {
		 chart: {     
			height: 325,
			width: 450
		},
		title: {
			text: ''
		},
		xAxis: {
			 categories: [<?php $i=0; foreach( $total_logins as $key=>$val )		 
			 {
				 if ( $i > 0 ) {
					 echo ",'".$key."'";
				 }
				 else{
					  echo "'".$key."'";
				 }
				 $i++;
			}		 
		  ?>]
		},
		legend: {
			enabled: false
		},
		tooltip: {
			formatter: function () {
				return '<b>User:</b> '+this.y;
			}
		},
		labels: {
			items: [{
				html: '',
				style: {
					left: '50px',
					top: '18px',
					color: (Highcharts.theme && Highcharts.theme.textColor) || 'black'
				}
			}]
		},
		 plotOptions: {
			series: {
				dataLabels: {
					enabled: false
				}
			}
		},
		series: [{
			type: 'column',
			name: '',
			data:  [<?php $i=0; foreach( $total_app_downloads as $key=>$val )		 
			 {
				 if ( $i > 0 ) {
					 echo ",".$val;
				 }
				 else{
					  echo $val;
				 }
				 $i++;
			}		 
		  ?>]        
		}],
			marker: {
				lineWidth: 2,
				lineColor: Highcharts.getOptions().colors[3],
				fillColor: 'white'
			}		
	});
	Highcharts.chart('student_total_logins', {
		 chart: {     
			height: 325,
			width: 490
		},
		title: {
			text: ''
		},
		xAxis: {
			 categories: [<?php $i=0; foreach( $total_logins as $key=>$val )		 
			 {
				 if ( $i > 0 ) {
					 echo ",'".$key."'";
				 }
				 else{
					  echo "'".$key."'";
				 }
				 $i++;
			}		 
		  ?>]
		},
		legend: {
			enabled: false
		},
		tooltip: {
			formatter: function () {
				return '<b>User:</b> '+this.y;
			}
		},
		labels: {
			items: [{
				html: '',
				style: {
					left: '50px',
					top: '18px',
					color: (Highcharts.theme && Highcharts.theme.textColor) || 'black'
				}
			}]
		},
		 plotOptions: {
			series: {
				dataLabels: {
					enabled: false
				}
			}
		},
		series: [{
			type: 'column',
			name: '',
			data:  [<?php $i=0; foreach( $total_logins as $key=>$val )		 
			 {
				 if ( $i > 0 ) {
					 echo ",".$val;
				 }
				 else{
					  echo $val;
				 }
				 $i++;
			}		 
		  ?>]        
		}],
			marker: {
				lineWidth: 2,
				lineColor: Highcharts.getOptions().colors[3],
				fillColor: 'white'
			}		
	});

</script>	
<?php 
}
else{} ?>


<!--facebook Verified-->
<?php if($show_regver =="weeks"){?>
<script>
	Highcharts.chart('total_app_facebook', {
		 chart: {     
			height: 325,
			width: 480
		},
		title: {
			text: ''
		},
		xAxis: {
			 categories: [<?php $i=0; foreach( $total_app_facebook as $key=>$val )		 
			 {
				 if ( $i > 0 ) {
					 echo ",'".$key."'";
				 }
				 else{
					  echo "'".$key."'";
				 }
				 $i++;
			}		 
		  ?>]
		},
		legend: {
			enabled: false
		},
		tooltip: {
			formatter: function () {
				return '<b>User:</b> '+this.y;
			}
		},
		labels: {
			items: [{
				html: '',
				style: {
					left: '50px',
					top: '18px',
					color: (Highcharts.theme && Highcharts.theme.textColor) || 'black'
				}
			}]
		},
		 plotOptions: {
			series: {
				dataLabels: {
					enabled: false
				}
			}
		},
		series: [{
			type: 'column',
			name: '',
			data:  [<?php $i=0; foreach( $total_app_facebook as $key=>$val )		 
			 {
				 if ( $i > 0 ) {
					 echo ",".$val;
				 }
				 else{
					  echo $val;
				 }
				 $i++;
			}		 
		  ?>]        
		},{
			type: 'column',
			name: '',
			data:  [<?php $i=0; foreach( $total_app_manual as $key=>$val )		 
			 {
				 if ( $i > 0 ) {
					 echo ",".$val;
				 }
				 else{
					  echo $val;
				 }
				 $i++;
			}		 
		  ?>]        
		}
		
		],
			marker: {
				lineWidth: 2,
				lineColor: Highcharts.getOptions().colors[3],
				fillColor: 'white'
			}		
	});
<!--verified !>	
	Highcharts.chart('total_veri_nonveri', {
		 chart: {     
			height: 325,
			width: 480
		},
		title: {
			text: ''
		},
		xAxis: {
			 categories: [<?php $i=0; foreach( $total_verified as $key=>$val )		 
			 {
				 if ( $i > 0 ) {
					 echo ",'".$key."'";
				 }
				 else{
					  echo "'".$key."'";
				 }
				 $i++;
			}		 
		  ?>]
		},
		legend: {
			enabled: false
		},
		tooltip: {
			formatter: function () {
				return '<b>User:</b> '+this.y;
			}
		},
		labels: {
			items: [{
				html: '',
				style: {
					left: '50px',
					top: '18px',
					color: (Highcharts.theme && Highcharts.theme.textColor) || 'black'
				}
			}]
		},
		 plotOptions: {
			series: {
				dataLabels: {
					enabled: false
				}
			}
		},
		series: [{
			type: 'column',
			name: '',
			data:  [<?php $i=0; foreach( $total_verified as $key=>$val )		 
			 {
				 if ( $i > 0 ) {
					 echo ",".$val;
				 }
				 else{
					  echo $val;
				 }
				 $i++;
			}		 
		  ?>]        
		},{
			type: 'column',
			name: '',
			data:  [<?php $i=0; foreach( $total_nonverified as $key=>$val )		 
			 {
				 if ( $i > 0 ) {
					 echo ",".$val;
				 }
				 else{
					  echo $val;
				 }
				 $i++;
			}		 
		  ?>]        
		}
		
		],
			marker: {
				lineWidth: 2,
				lineColor: Highcharts.getOptions().colors[3],
				fillColor: 'white'
			}		
	});
</script>
<?php }else {?>
<script>
	Highcharts.chart('total_app_facebook', {
		 chart: {     
			height: 325,
			width: 480
		},
		title: {
			text: ''
		},
		xAxis: {
			 categories: [<?php $i=0; foreach( $total_verified as $key=>$val )		 
			 {
				 if ( $i > 0 ) {
					 echo ",'".$key."'";
				 }
				 else{
					  echo "'".$key."'";
				 }
				 $i++;
			}		 
		  ?>]
		},
		legend: {
			enabled: false
		},
		tooltip: {
			formatter: function () {
				return '<b>User:</b> '+this.y;
			}
		},
		labels: {
			items: [{
				html: '',
				style: {
					left: '50px',
					top: '18px',
					color: (Highcharts.theme && Highcharts.theme.textColor) || 'black'
				}
			}]
		},
		 plotOptions: {
			series: {
				dataLabels: {
					enabled: false
				}
			}
		},
		series: [{
			type: 'column',
			name: '',
			data:  [<?php $i=0; foreach( $total_verified as $key=>$val )		 
			 {
				 if ( $i > 0 ) {
					 echo ",".$val;
				 }
				 else{
					  echo $val;
				 }
				 $i++;
			}		 
		  ?>]        
		},{
			type: 'column',
			name: '',
			data:  [<?php $i=0; foreach( $total_nonverified as $key=>$val )		 
			 {
				 if ( $i > 0 ) {
					 echo ",".$val;
				 }
				 else{
					  echo $val;
				 }
				 $i++;
			}		 
		  ?>]        
		}
		
		],
			marker: {
				lineWidth: 2,
				lineColor: Highcharts.getOptions().colors[3],
				fillColor: 'white'
			}		
	});
	
	Highcharts.chart('total_veri_nonveri', {
		 chart: {     
			height: 325,
			width: 480
		},
		title: {
			text: ''
		},
		xAxis: {
			 categories: [<?php $i=0; foreach( $total_app_facebook as $key=>$val )		 
			 {
				 if ( $i > 0 ) {
					 echo ",'".$key."'";
				 }
				 else{
					  echo "'".$key."'";
				 }
				 $i++;
			}		 
		  ?>]
		},
		legend: {
			enabled: false
		},
		tooltip: {
			formatter: function () {
				return '<b>User:</b> '+this.y;
			}
		},
		labels: {
			items: [{
				html: '',
				style: {
					left: '50px',
					top: '18px',
					color: (Highcharts.theme && Highcharts.theme.textColor) || 'black'
				}
			}]
		},
		 plotOptions: {
			series: {
				dataLabels: {
					enabled: false
				}
			}
		},
		series: [{
			type: 'column',
			name: '',
			data:  [<?php $i=0; foreach( $total_app_facebook as $key=>$val )		 
			 {
				 if ( $i > 0 ) {
					 echo ",".$val;
				 }
				 else{
					  echo $val;
				 }
				 $i++;
			}		 
		  ?>]        
		},{
			type: 'column',
			name: '',
			data:  [<?php $i=0; foreach( $total_app_manual as $key=>$val )		 
			 {
				 if ( $i > 0 ) {
					 echo ",".$val;
				 }
				 else{
					  echo $val;
				 }
				 $i++;
			}		 
		  ?>]        
		}
		
		],
			marker: {
				lineWidth: 2,
				lineColor: Highcharts.getOptions().colors[3],
				fillColor: 'white'
			}		
	});
</script>
<?php } ?>	

<!--facebook Verified-->
<?php if($show_reachconv =="weeks"){?>
<script>
	Highcharts.chart('total_reachcon', {
		 chart: {     
			height: 325,
			width: 480
		},
		title: {
			text: ''
		},
		xAxis: {
			 categories: [<?php $i=0; foreach( $total_conversions as $key=>$val )		 
			 {
				 if ( $i > 0 ) {
					 echo ",'".$key."'";
				 }
				 else{
					  echo "'".$key."'";
				 }
				 $i++;
			}		 
		  ?>]
		},
		legend: {
			enabled: true
		},
		tooltip: {
			formatter: function () {
				return '<b>User:</b> '+this.y;
			}
		},
		labels: {
			items: [{
				html: '',
				style: {
					left: '50px',
					top: '18px',
					color: (Highcharts.theme && Highcharts.theme.textColor) || 'black'
				}
			}]
		},
		 plotOptions: {
			series: {
				dataLabels: {
					enabled: false
				}
			}
		},
		series: [{
			type: 'column',
			name: 'Conversions',
			data:  [<?php $i=0; foreach($total_conversions as $key=>$val )		 
			 {
				 if ( $i > 0 ) {
					 echo ",".$val;
				 }
				 else{
					  echo $val;
				 }
				 $i++;
			}		 
		  ?>]        
		},{
			type: 'column',
			name: 'Reach',
			data:  [<?php $i=0; foreach($total_reach as $key=>$val )		 
			 {
				 if ( $i > 0 ) {
					 echo ",".$val;
				 }
				 else{
					  echo $val;
				 }
				 $i++;
			}		 
		  ?>]        
		}
		,{
			type: 'column',
			name: 'Impressions',
			data:  [<?php $i=0; foreach( $total_impresions as $key=>$val )		 
			 {
				 if ( $i > 0 ) {
					 echo ",".$val;
				 }
				 else{
					  echo $val;
				 }
				 $i++;
			}		 
		  ?>]        
		}
		],
			marker: {
				lineWidth: 2,
				lineColor: Highcharts.getOptions().colors[3],
				fillColor: 'white'
			}		
	});
</script>
<?php }?>
	
