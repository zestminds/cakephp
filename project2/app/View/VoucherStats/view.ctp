<div class="voucherSales view">
<h2><?php echo __('Voucher Sale'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($voucherSale['VoucherSale']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('User'); ?></dt>
		<dd>
			<?php echo $this->Html->link($voucherSale['User']['id'], array('controller' => 'users', 'action' => 'view', $voucherSale['User']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Voucher'); ?></dt>
		<dd>
			<?php echo $this->Html->link($voucherSale['Voucher']['title'], array('controller' => 'vouchers', 'action' => 'view', $voucherSale['Voucher']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Purchase'); ?></dt>
		<dd>
			<?php echo h($voucherSale['VoucherSale']['purchase']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Purchase Status'); ?></dt>
		<dd>
			<?php echo h($voucherSale['VoucherSale']['purchase_status']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Is Active'); ?></dt>
		<dd>
			<?php echo h($voucherSale['VoucherSale']['is_active']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($voucherSale['VoucherSale']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($voucherSale['VoucherSale']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Voucher Sale'), array('action' => 'edit', $voucherSale['VoucherSale']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Voucher Sale'), array('action' => 'delete', $voucherSale['VoucherSale']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $voucherSale['VoucherSale']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List Voucher Sales'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Voucher Sale'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Vouchers'), array('controller' => 'vouchers', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Voucher'), array('controller' => 'vouchers', 'action' => 'add')); ?> </li>
	</ul>
</div>
