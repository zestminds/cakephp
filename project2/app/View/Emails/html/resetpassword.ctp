<?php echo $mail; ?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>:: Reset_password ::</title>
<!-- css -->
<!--[if IE]>
    <script src="js/html5shiv.js"></script>
<![endif]-->
<style type="text/css">
@import url('https://fonts.googleapis.com/css?family=Open+Sans:400,600');
body {
	font-family: 'Open Sans', sans-serif;
	margin: 0;
	padding: 0;
	background-color: #f6f6f6;
	-ms-text-size-adjust: 100%;
	-webkit-text-size-adjust: 100%;
    background: url('img/main-bg.jpg');
    background-repeat: no-repeat;
    background-size: cover;
    background-position: top center;
}
* {
	-moz-box-sizing: border-box;
	-webkit-box-sizing: border-box;
	box-sizing: border-box;
}
h2{
    color: #e2872b; 
    font-size: 25px;
    font-family: 'Roboto', sans-serif; 
    font-weight: 500;
}
img {
	max-width: 100%;
}
table {
	border-spacing: 0;
}
table td {
	border-collapse: collapse;
}
.table-main {
	width: 607px;
	margin: 0 auto;
    padding-top: 147px;
    padding-bottom: 95px;
}
</style>
</head>
<body>
<div class="table-main">
  <table cellpadding="0" cellspacing="0" border="0" width="100%" style="width:100%; font-family: 'Open Sans', sans-serif;box-shadow: 0 10px 10px rgba(0,0,0,0.1); -moz-box-shadow: 0 10px 10px rgba(0,0,0,0.1); -webkit-box-shadow: 0 10px 10px rgba(0,0,0,0.1);">
    <tr>
      <td>
          <div style="width: 100%; background: #ffffff; border-radius: 30px; padding: 35px 50px;">
                <div style="text-align: center; padding-bottom: 35px;">
                    <img src="img/moola.png" alt="Rave" title="Rave" style="border: none; outline: none;">
                </div>
                <div style="text-align: center; padding-bottom: 35px;">
                    <img src="img/lock.png" alt="Reset Password" title="Reset Password" style="border: none; outline: none;">
                </div>
                <h2 style="color: #38404b; font-size: 19px;font-family: 'Open Sans', sans-serif; font-weight: 600; margin:0; line-height: 26px; text-align: center;">Hi Smiles Davis,</h2>
                <p style="color: #38404b; font-size: 17px;font-family: 'Open Sans', sans-serif; font-weight: 400; margin:0; line-height: 26px; text-align: center;">We got a request to rest your Rave password.</p>
                <p style="color: #38404b; font-size: 17px;font-family: 'Open Sans', sans-serif; font-weight: 400; margin:0; line-height: 26px; text-align: center;">If you ignore this message, your password won’t be changed.</p>
                <div style="text-align: center; padding-top: 50px; padding-bottom: 22px;">
                    <a href="javascript:void(0)" target="_blank" title="Reset Password" style="border: none; outline: none;"><img style="border: none; outline: none;" src="img/reset.png" alt="Reset Password" title="Reset Password"></a>
                </div>
                <p style="margin: 0; font-size: 17px;font-family: 'Open Sans', sans-serif; font-weight: 400; text-align: center; color: #38404b;">If you didn’t request a password reset, <a href="javascript:void(0)" target="_blank" title="Reset Password" style="color:#138fd7;font-family: 'Open Sans', sans-serif; font-weight: 400;">let us know.</a></p>
          </div>
      </td>
    </tr>    
  </table>
</div>
</body>
</html>
