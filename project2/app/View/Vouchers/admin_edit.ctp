<link href="<?php echo SITE_LINK ?>datetime/bootstrap/css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
<link href="<?php echo SITE_LINK ?>css/app.css" rel="stylesheet" media="screen">
<link href="<?php echo SITE_LINK ?>css/bootstrap-tagsinput.css" rel="stylesheet" media="screen">
<?php //pr($this->request->data); //DIE;?>
<script>
var avatar = "<?php echo SITE_LINK."img/voucher/".trim($voucher['Voucher']['image']); ?>";
</script>
<?php $tmpImage = SITE_LINK."img/voucher/".trim($voucher['Voucher']['image']); ?>
<div class="cont-right">
  <div class="cont-right-innr">
		<div class="main-hd-in">
          <div class="row">
            <div class="col-sm-12">
              <h2 class="title-das"> Edit Voucher</h2>              
            </div>
          </div> 
		  
        </div>    
		
        <div class="manage-marchant">		
			<?php echo $this->Form->create('Voucher',array("id"=>"VoucherEditForm","type"=>"file","class"=>"themeForm")); echo $this->Form->input('id'); //echo $this->Form->hidden('User.id'); echo $this->Form->input('id');//echo $this->Form->hidden('user_id',array('value'=>$user_id));	echo $this->Form->hidden('voucher_status');
				echo $this->Form->hidden('address',array("value"=>$userdetails['UserDetail']['address']));
				echo $this->Form->hidden('business',array("value"=>$userdetails['UserDetail']['business']));
				echo $this->Form->hidden('vimage',array("value"=>$userdetails['UserDetail']['image']));
				
				?>
	               <div class="col-sm-6 vochr-left">
	               	  <div class="boxCard">
	               	  	 <div class="card-inner box-shadow">
		               	  	  <h4>Title*</h4>
		               	  	  <div class="theme-radio form-group">
		                            <div class="radio radio-info radio-inline">
										<?php 
											echo $this->Form->radio('title_status',$optionsTitle,array("legend"=>"","type"=>"radio",'id'=>'title_status','value'=>($voucher['Voucher']['title_status']== "D")?'D':'C'));	
										?>				
				                    </div>
		                       </div>
		                       <div class="form-group inputField">	<span class="percertange_sign">%</span>	                       	 
		                       	 <?php echo $this->Form->input('title_discount',array("type"=>"text",'id'=>'discountvalue','label' => ['text' => 'Discount Value','class' => 'lable-control'],'value'=>$voucher['Voucher']['title_discount'],"class"=>"form-control"));
		                       	 
		                       	  echo $this->Form->hidden("default_title",array('id'=>'default_title','value'=>$voucher['Voucher']['title_discount']));
		                       	  
		                       	 echo $this->Form->hidden("olddiscount",array('id'=>'olddiscount','value'=>$voucher['Voucher']['title_discount']));
		                       	 ?>
                                  
		                       </div>
		                    </div>
	               	      </div> <!--/. End boxCard -->

	               	      <div class="boxCard">
	               	  	 <div class="card-inner box-shadow">
		               	  	  <h4>Sub Title*</h4>
		               	  	 <!-- <div class="theme-radio form-group">
		                            <div class="radio radio-info radio-inline">
				                      <?php 
											//echo $this->Form->radio('sub_title_status',$optionsSubtitle,array("legend"=>"","type"=>"radio",'id'=>'sub_title_status','value'=>'D'));	
									   ?>
				                    </div>
		                       </div>-->
		                       <div class="form-group inputField">		                       	 
		                       	  <?php echo $this->Form->input('sub_title',array('id'=>'sub_title','label' => ['text' => 'Sub Title','class' => 'lable-control'],'value'=>$voucher['Voucher']['sub_title'], "class"=>"form-control","placeholder"=>"e.g. on all cocktails"));
		                       	   echo $this->Form->hidden("default_sub_title",array('id'=>'default_sub_title','value'=>$voucher['Voucher']['sub_title']));
		                       	  ?>
		                       </div>
		                    </div>
	               	      </div> <!--/. End boxCard -->

	               	      <div class="boxCard">
	               	  	 <div class="card-inner box-shadow">
		               	  	  <h4>Description*</h4>
		               	  	  <div class="theme-radio form-group">
		                            <div class="radio radio-info radio-inline">
				                        <?php 
											echo $this->Form->radio('description_status', $optionsDesc,array('legend'=>'','id'=>'description_status',"value"=>($voucher['Voucher']['description_status']== "D")?'D':'C'));
										?>
				                    </div>
		                       </div>
		                       <div class="form-group inputField">		                       	  
		                       	  	<?php echo $this->Form->input('description',array('type'=>"textarea",'rows'=>'6','id'=>'descriptionvalue','label' => ['text' => 'Write here','class' => 'lable-control'],'value'=>$voucher['Voucher']['description'],"class"=>"form-control"));
		                       	  	echo $this->Form->hidden("default_description",array('id'=>'default_description','value'=>$voucher['Voucher']['description']));
		                       	  	?>                                 
		                       </div>
		                    </div>
	               	      </div> <!--/. End boxCard -->

	               	      <div class="boxCard">
		               	  	 <div class="card-inner box-shadow">
			               	  	  <h4>Terms & Conditions*</h4>
			               	  	  <div class="theme-radio form-group">
			                            <div class="radio radio-info radio-inline">
					                        <?php 
												echo $this->Form->radio('terms_status', $optionsTerms,array('id'=>'terms_status','legend'=>'',"value"=>($voucher['Voucher']['terms_status']== "D")?'D':'C'));
											?>
					                    </div>
			                       </div>
			                       <div class="form-group inputField">			                       	 
	                                 	<?php echo $this->Form->input('terms',array('type'=>"textarea",'rows'=>'6','id'=>'termsvalue','label' => ['text' => 'Write here','class' => 'lable-control'],'value'=>$voucher['Voucher']['terms'],"class"=>"form-control"));
										 echo $this->Form->hidden("default_terms",array('id'=>'default_terms','value'=>$voucher['Voucher']['terms']));
										?>
			                       </div>
			                       <div class="website-field">
				                       	<h4>Terms and Conditions URL</h4>
				                       <div class="form-group inputField">
										  <?php echo $this->Form->input('terms_url',array('id'=>'terms_url','label' => false,'placeholder'=>$defaultvalue[4]['DefaultValue']['value'],'value'=>$voucher['Voucher']['terms_url'],"class"=>"form-control"));
										  ?>
				                       </div>
			                       </div>
			                    </div>
		               	    </div> <!--/. End boxCard -->
	                </div>
	                <?php echo $this->Form->hidden("tmpstartdate",array("value"=>$voucher['Voucher']['end_date'],"id"=>"tmpstartdate")); ?>
	                <?php echo $this->Form->hidden("tmpenddate",array("value"=>$voucher['Voucher']['end_date'],"id"=>"tmpenddate")); ?>
	                <div class="col-sm-6 vochr-right">
	                	<div class="boxCard">
	               	  	    <div class="card-inner box-shadow">
		               	  	  <h4>Time*</h4>	
		               	  	  <div class="datePicker_ttl">	
		               	  	      <h5>All Day </h5>               	  	 
			                      <label class="switch switch_btn">		
									  <?php									  
										echo $this->Form->checkbox('day_status', array('label'=>'','class'=>'allDay','options'=>$optionsAllday,"value"=>($voucher['Voucher']['day_status']== "1")?'1':'0'));
									
									 ?>
									  <span class="slider round"></span>
									  <!-- <span class="absolute-no">NO</span> -->
								  </label>
								  <?php echo $this->Form->hidden('VouchDayStatus',array("id"=>"VouchDayStatus","value"=>$voucher['Voucher']['day_status'])); ?>
								</div>
							    <div class="theme-Datepicker">
                                    <div class="form-group">
                                    	<div class="filed_row">
                                    		<div class="col-half">
		                                    	<label class="lable-control">Start Date</label>	
												<!-- <div class="controls input-append date form_date" data-date="" data-date-format="dd MM yyyy"data-link-field="dtp_input2" data-link-format="yyyy-mm-dd">
														<div class='input_date_grp'>
														<?php //$start_date =date_create($voucher["Voucher"]["start_date"]); $start_date=date_format($start_date,"d F Y");
														//echo $this->Form->input('start_date',array("value"=>$start_date,"label"=>false,"type"=>"text",'readonly'=>'readonly','autofill'=>'false',"class"=>"form-control"));?>                
														<span class="add-on"><i class="fa fa-calendar" aria-hidden="true"></i></span>
														</div>														
												</div>	-->											
												<div class='input_date_grp input-group date datetimepicker1'>

													<?php echo $this->Form->input('start_date',array("value"=>$voucher["Voucher"]["start_date"],"label"=>false,"type"=>"text",'autofill'=>'false',"class"=>"form-control"));?>    
													<span class="input-group-addon">
													<span class="fa fa-calendar"></span>
													</span>
												</div>	
								            </div>
								            <div class="col-half">
		                                    	<label class="lable-control">End Date</label>
												<!--<div class="controls input-append date form_date" data-date="" data-date-format="dd MM yyyy"data-link-field="dtp_input3" data-link-format="yyyy-mm-dd">
														<div class='input_date_grp'>
														<?php //$end_date =date_create($voucher["Voucher"]["end_date"]); $end_date=date_format($end_date,"d F Y");
														// echo $this->Form->input('end_date',array("value"=>$end_date,"label"=>false,"type"=>"text",'readonly'=>'readonly','autofill'=>'false',"class"=>"form-control"));?>                
														<span class="add-on"><i class="fa fa-calendar" aria-hidden="true"></i></span>
													   </div>														
												</div>-->
												<div class='input_date_grp input-group date datetimepicker1'>

													<?php echo $this->Form->input('end_date',array("value"=>$voucher["Voucher"]["end_date"],"label"=>false,"type"=>"text",'autofill'=>'false',"class"=>"form-control"));?>    
													<span class="input-group-addon">
													<span class="fa fa-calendar"></span>
													</span>
												</div>
								              </div>
								          </div>
						            </div>
						            <div class="form-group">
                                    	<div class="filed_row">
                                    		<div class="col-half">
												<div class="grp_date_show" <?php echo ($voucher['Voucher']['day_status']==1)? 'style="display:none"':'style="display:inline"'?>>
														<label class="lable-control">Start Time</label>		                                    	
														<?php echo $this->Form->hidden("tmpstarthr",array("value"=>$voucher['Voucher']['start_hour'],"id"=>"tmpstarthr")); ?>
														<!-- <div class="controls input-append date form_time datepickerdiv" data-date="" data-date-format="hh:ii p" data-link-field="dtp_input3" data-link-format="hh:ii">
															<div class='input_date_grp'>
															
															<?php echo $this->Form->input('start_hour',array("label"=>false,"type"=>"text",'readonly'=>'readonly','autofill'=>'false',"value"=>"","class"=>"form-control"));?>                
															<span class="add-on"><i class="fa fa-clock-o" aria-hidden="true"></i></span>
															</div>	
														</div> 	-->		
														<div class='input_date_grp input-group date datetimepicker2'>
														  <?php echo $this->Form->input('start_hour',array("label"=>false,"type"=>"text",'autofill'=>'false',"class"=>"form-control"));?>                
														  <span class="input-group-addon">
															<span class="fa fa-clock-o"></span>
														  </span>
														</div>											
								                </div> 
								            </div>
								            <div class="col-half">
												<div class="grp_date_show" <?php echo ($voucher['Voucher']['day_status']==1)? 'style="display:none"':'style="display:inline"'?>>
													<label class="lable-control">End Time</label>
													<?php echo $this->Form->hidden("tmpendhr",array("value"=>$voucher['Voucher']['end_hour'],"id"=>"tmpendhr")); ?>
													<!--  <div class="controls input-append date form_time" data-date="" data-date-format="hh:ii p" data-link-field="dtp_input3" data-link-format="hh:ii">
														<div class='input_date_grp'>
														<?php echo $this->Form->input('end_hour',array("label"=>false,"type"=>"text",'readonly'=>'readonly','autofill'=>'false',"class"=>"form-control","value"=>""));?>                
														<span class="add-on"><i class="fa fa-clock-o" aria-hidden="true"></i></span>
														</div>														
													</div> -->
													<div class='input_date_grp input-group date datetimepicker2'>
														  <?php echo $this->Form->input('end_hour',array("label"=>false,"type"=>"text",'autofill'=>'false',"class"=>"form-control"));?>                
														  <span class="input-group-addon">
															<span class="fa fa-clock-o"></span>
														  </span>
													</div>	
												</div>	
								            </div>
								        </div>
						            </div>
							    </div>
							    <div class="repeat-day">
							    	<h4>Repeat*</h4>
							    	<div class="theme-radio form-group">
			                            <div class="radio radio-info radio-inline">
					                       <?php 
												echo $this->Form->radio('repeat_on', $optionsRepeat,array('id'=>'repeat_on','legend'=>'',"value"=>($voucher['Voucher']['repeat_on']== "D")?'D':'W',"class"=>"weekoption"));
											?>
					                    </div>
			                       </div>
			                       <?php 
			                       if($voucher['Voucher']['repeat_on']=="D"){$repeat_dis="'disabled'=>'disabled'" ;}else{ $repeat_dis='';}
			                       echo $this->Form->input('repeat_days', array('id'=>'repeat_day','type'=>'select','multiple'=>'checkbox','options'=>$optionsWeekday,$repeat_dis,"label"=>false,"div"=>"weak-List","class"=>"weekdays","label"=>array("class"=>"selected","text"=>""))); ?>
			                       	 <div id="error_msg" style="display:none" class="error">Please check at least one box in this group.</div>	

			                      <!--ul class="weak-List">
			                       	  <li class="active"><a href="#">M</a></li>
			                       	  <li><a href="#">T</a></li>
			                       	  <li><a href="#">W</a></li>
			                       	  <li><a href="#">T</a></li>
			                       	  <li><a href="#">F</a></li>
			                       	  <li><a href="#">S</a></li>
			                       	  <li><a href="#">S</a></li>
			                       </ul-->
							    </div>
		                    </div>
	               	    </div> <!--/. End boxCard -->
	                	<div class="boxCard">
	               	  	    <div class="card-inner box-shadow">
		               	  	  <h4>Hashtags*</h4>
		                       <div class="hashtags*">
                                   <div class="inp-group" id="tags_grp">
									 <?php echo $this->Form->input('hashtags',array("value"=>"","label"=>false,"class"=>"form-srch","placeholder"=>"Type in your hashtags"));?>
									   <button type="button" class="btn srch-btn deletetag"><?php echo $this->Html->image("cros-icon.png",array("alt"=>"Delete Tags","id"=>"deletetag")); ?></button>
                                   </div><ul class="tags">
                                   <?php	
										if(empty($this->request->data['Voucher']['tags'])){}
										else{ ?>
										
										<?php
										foreach ($this->request->data['Voucher']['tags'] as $tags) {?>
										<li><a href="javascript:void(0)"><?php echo $tags ?> </a><button type='button' class='delete_button' value="<?php echo $tags ?>"><img src='<?php echo SITE_LINK?>img/cros-icon.png' alt=''/></button>    
										</li>
                                   
										<?php }?>
                                    
                                     <?php }?> </ul>
                                    <?php echo $this->Form->input('tags',array("value"=>$voucher['Voucher']['tags'],"label"=>false,"class"=>"form-srch","placeholder"=>"","type"=>"hidden"));?>
                                   </div>     
                                   
		                       </div>
		                    </div>
	               	    </div> <!--/. End boxCard -->

	               	    <div class="boxCard">
	               	  	    <div class="image-preview">
									<!-- Drop Zone -->
									<div class="upload-drop-zone" id="drop-zone"> 
									   <div class="upload-file"> 
										  <?php 
										  if(empty($voucher['Voucher']['image'])  || !file_exists(WWW_ROOT."img/voucher/".$voucher['Voucher']['image']))
										  $imageprofile=SITE_LINK."img/default_voucher.jpeg";
											else
											  $imageprofile=SITE_LINK."img/voucher/".trim($voucher['Voucher']['image']);
											echo $this->Html->image($imageprofile,array("alt"=>"Voucher Pic","id"=>"img-upload","width"=>"100%","height"=>"100%"));
											 echo $this->Form->input("tmpImage",array("label"=>false,"type"=>"textarea","style"=>"display:none;","id"=>"tmpImage")); 
											echo $this->Form->hidden('image_old',array('value'=>$voucher['Voucher']['image']));
											?>
										   <p>Upload File</p>
									  </div>
	                                   <!--div class="btn btn-upload image-preview-input">
	                                   	<i class="fa fa-cloud-upload" aria-hidden="true"></i><span class="image-preview-input-title">Select file to upload</span>
	                                   <?php //echo $this->Form->file('image',array("type"=>"file","accept"=>"image/png, image/jpeg, image/gif","type"=>"file","id"=>"sample_input","name"=>"test[image]"));?>
											
											<!-- rename it --> 
										<!--/div-->
								    </div>
								</div> 
		                    </div> <!--/. End boxCard -->
		                    <div class="col-sm-12">
								<div class="form-submit">
								   <button type="button" class="btn btn-theme" id="adminpreview" data-toggle="modal">PREVIEW</button>
								   <button type="submit" class="btn btn-theme" id="save" name="save" onclick="voucherStatus('save');">SAVE</button>
								   <button type="submit" class="btn btn-theme" id="publish" onclick="voucherStatus('publish');">PUBLISH</button>   
								</div>
							</div>
	               	    </div>
	            <?php echo $this->Form->end(); ?>	
		</div>
 </div>		
</div>
<style>
.head{font-size:20px;font-weight:bold;color:#444;margin-bottom:10px;}
.preview_voucher {padding:6px 20px;border:1px solid #444;margin-top:10px;}
.linkvoucher {color:#45ccf0 !important}
.linkvoucher:hover{color:#45ccf0 !important}
button.close {
	-webkit-appearance: none;
	/* padding: 0; */
	/* cursor: pointer; */
	background: 0 0!important;
	border: 0!important;
	color: #000!important;
}

#map {
        height: 100px;  /* The height is 400 pixels */
        width:400px;  /* The width is the width of the web page */
}
div#imageModal{top:0px !important;}

.imageBox{height:470px !important; width:520px !important;}
.thumbBox{border-radius : 0px !important; height:270px !important; width:270px !important; top:42% !important; left:42% !important;}
#drop-zone{cursor:pointer;}
</style>   
<!--Reg Modal HTML -->
<?php /*
 <!--Model Start Preview -->
        <div id="voucher-offer-model" class="modal fade voucher-offer-model themeModel">
            <div class="modal-dialog sm-model mobile-offer-add">
                <div class="modal-content">
                    <div class="modal-body">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>                    
							<div class="vouch-offer">
								<div class="auto-bar">
									<?php (h($voucher["Voucher"]["image"])!="")?$src=SITE_LINK."img/voucher/".h($voucher["Voucher"]["image"]):$src=SITE_LINK."img/default_voucher.jpeg"?>

								  <div class="mobile-main">
									<div class="ng-header">
									<div class="container mobile-container">
									  <div class="row">
										<div class="col-sm-12"> 
											<span class="back_arrow"><a href="javascript:void(0);"><?php echo $this->Html->image("back-arrow.png",array("alt"=>"Voucher"));?></a></span>Offer Details <span></span>
										  <div class="hd-right">
											<ul class="list-inline">
											  <li><a href="javascript:void(0);"><?php echo $this->Html->image("fav-iocn.png",array("alt"=>"favorite"));?></a></li>
											<li><a href="javascript:void(0);"><?php echo $this->Html->image("share-iocn.png",array("alt"=>"Share"));?></a></li>
											</ul>
										  </div>
										</div>
									  </div>
									</div>
								  </div>	  
								   <div  class="mobile_offer">
									  <img class="VoucherImg" src="<?php echo $src; ?>"/>
								 </div>  
								  <section class="jusic-banner"> 	  
									<div class="cnt_middle">
									  <div class="cnt-contant">		 
										<div class="snu-profile">
											 
										  <div class="avtar-img">
											   <?php (h($userdetails["UserDetail"]["image"])!="")?$src=SITE_LINK."img/profile/".h($userdetails["UserDetail"]["image"]):
												  $src=SITE_LINK."img/default.jpeg"?>
												  <img src='<?php echo $src ;?>'>;
										  </div>
										  <h1 class="text-center"><?php echo h($userdetails['UserDetail']['business']); ?></h1>
										  <div class="row">
											<div class="snu-student">
											  <ul class="list-inline">
												<li>
												  <div class="banr-text"> <span class="disct"><div id="Voucherdiscount"></div></span>
													<p> Student <span>Discount</span></p>
												  </div>
												</li>
												<li>
												  <div class="banr-text">
													<div id="Vouchersubtitle" class="break-content">
												  </div>
												</li>
											  </ul>
											</div>
										  </div>
										  <span class="show-status"><div id="Voucherlftmsg"></div></span> </div>
										<div class="student-conditions">
										  <div class="blog-artiale">
											<div class="map-left"><iframe  class="map-img" src="https://www.google.com/maps?q=<?php echo $userdetails['UserDetail']['address'];?>&output=embed"></iframe></div>
											<div class="articale-right">
											  <p>Less than 0.5 miles away</p>
											  <h2><?php echo h($userdetails['UserDetail']['address']); ?></h2>
											</div>
										  </div>
										  <div class="blog-artiale">
											<h2><?php echo $this->Html->image("about-help.png",array("alt"=>"About this discount"));?>About this discount</h2>
											<p><div id="Voucherdescription"><?php echo h($voucher['Voucher']['description']); ?></div>... </p>
											<a href="javascript:void(0);">Read More</a> </div>
										  <div class="blog-artiale">
											<h2><?php echo $this->Html->image("terms-condition.png",array("alt"=>"Terms & Conditions"));?>Terms & Conditions</h2>
											<p><div id="Voucherterms"></div>...</p>
											<a href="javascript:void(0)" target="_blank" id="voucherlink">Read More</a> 
										</div>
									  </div>
									</div>
								  </section>
									<div class="mobile-footr">
										<div class="container mobile-container">
										  <div class="container mobile-container">
											<h2> <?php echo $this->Html->image("get-direcation.png",array("alt"=>"Get Directions"));?>Get Directions</h2>
										  </div>
										</div>
									</div>
								</div>
								</div>
							</div>   
                    </div>                   
                </div>
            </div>
        </div>  
      <!--Voucher Offer Modal HTML -->
*/ ?>


<!--Voucher Offer Modal HTML -->
  <div id="voucher-offer-model" class="modal fade voucher-offer-model themeModel">
    <div class="modal-dialog sm-model mobile-offer">
      <div class="modal-content">
        <div class="modal-body">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <div class="vouch-offer"> 
          	<span class="top-time">9:30</span>
          <?php echo $this->Html->image("mobile-offer.png"); ?>
          
            <div class="offer-banner"><?php echo $this->Html->image("jusic-banner.jpg",array("id"=>"VoucherImg")); ?> </div>
            <div class="auto-bar">
              <div class="mobile-main">
                <div class="ng-header">
                  <div class="container mobile-container">
                    <div class="row">
                      <div class="col-sm-12"> <span class="back_arrow"><a href="javascript:void(0);"><?php echo $this->Html->image("back-arrow.png"); ?></a></span>Offer Details <span></span>
                        <div class="hd-right">
                          <ul class="list-inline">
                            <li><a href="javascript:void(0);"><?php echo $this->Html->image("fav-iocn.png",array("alt"=>"favorite"));?></a></li>
							<li><a href="javascript:void(0);"><?php echo $this->Html->image("share-iocn.png",array("alt"=>"Share"));?></a></li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <section class="jusic-banner">
                  <div class="cnt_middle">
                    <div class="cnt-contant">
                      <div class="snu-profile">
                        <div class="avtar-img" style="background:#fff;"> <?php (h($userdetails["UserDetail"]["image"])!="")?$src=SITE_LINK."img/profile/".h($userdetails["UserDetail"]["image"]):
												  $src=SITE_LINK."img/default.jpeg"?>
												  <img src='<?php echo $src ;?>'></div>
                        <h1 class="text-center"><?php echo h($userdetails['UserDetail']['business']); ?></h1>
                        <div class="row">
                          <div class="snu-student">
                            <ul class="list-inline">
                              <li>
                                <div class="banr-text"> <span class="disct"><div id="Voucherdiscount"></div></span>
                                  <p> Student <span>Discount</span></p>
                                </div>
                              </li>
                              <li>
                                <div class="banr-text">
                                  <div id="Vouchersubtitle" class="break-content">
									</div>
                                </div>
                              </li>
                            </ul>
                          </div>
                        </div>
                        <span class="show-status"><div id="Voucherlftmsg"></div></span> </div>
                      <div class="student-conditions">
                        <div class="blog-artiale">
                          <div class="map-left">  </div>
                          <div class="articale-right" style="float:right; width:68%;">
                            <p>Less than 0.5 miles away</p>
                            <h2><?php echo h($userdetails['UserDetail']['address']); ?></h2>
                          </div>
                        </div>
                        <div class="blog-artiale">
                          <h2> <?php echo $this->Html->image("about-help.png",array("alt"=>"About this discount"));?>About this discount</h2>
                          <p><div id="Voucherdescription"></div></p>
                          <a href="javascript:void(0);">Read More</a> </div>
                        <div class="blog-artiale">
                          <h2> <?php echo $this->Html->image("terms-condition.png",array("alt"=>"Terms & Conditions"));?>Terms & Conditions</h2>
                          <p><div id="Voucherterms"></div></p>
                          <a href="javascript:void(0);">Read More</a> </div>
                      </div>
                    </div>
                  </div>
                </section>
                <div class="mobile-footr">
                  <div class="container mobile-container">
                    <div class="container mobile-container">
                      <h2> <?php echo $this->Html->image("get-direcation.png",array("alt"=>"Get Directions"));?> Get Directions</h2>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!--Voucher Offer Modal HTML -->
  
  <div class="modal fade loginModel themeModel" id="imageModal" tabindex="-1" role="dialog" aria-labelledby="imageModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
       <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title">Select Voucher Image</h4>
      </div>
      <div class="modal-body" id="actions">
        <div class="selct-img-voc">
		  <div class="img-container" style="width:100%;">
				<img src="<?php echo $tmpImage; ?>" alt="Picture">
			</div>
		  <div class="iviewer" >
			 <ul class="custom-file-upload" class="docs-buttons">
				 <li><label for="upload-file" class="custom-file-upload"><span ><?php echo $this->Html->image("gallery-img.png",array("alt"=>"Voucher Pic")); ?></span></label> <input id="upload-file" type="file" name="file" accept="image/*"/></li>
				<li data-method="rotate" data-option="-90"><a href="javascript:void(0)" title="Rotate Left"><span id="rotateLeft"><?php echo $this->Html->image("rotate-left.png",array("alt"=>"Voucher Pic")); ?></span></a></li>
				 <li data-method="rotate" data-option="90"><a href="javascript:void(0)" title="Rotate Right"><span id="rotateRight"><?php echo $this->Html->image("rotate-right.png",array("alt"=>"Voucher Pic")); ?></span></a></li>
				 <li data-method="zoom" data-option="-0.1"><a href="javascript:void(0)" title="Zoom Out"><span id="zoomIn"><?php echo $this->Html->image("zoom-out.png",array("alt"=>"Voucher Pic")); ?></span></a></li>
				  <li data-method="zoom" data-option="0.1"><a href="javascript:void(0)" title="Zoom In"><span id="zoomOut"><?php echo $this->Html->image("zoom-in.png",array("alt"=>"Voucher Pic")); ?></span></a></li>
			 </ul>             
		  </div>
		</div>
        
		<div class="iviewer-btn">
			<ul class="list-inline">
			   <li><button class="btn btn-custom" data-dismiss="modal">CLOSE</button></li> 
			   <li><button class="btn btn-custom btn-primary" data-method="getCroppedCanvas" data-option="{ &quot;maxWidth&quot;: 4096, &quot;maxHeight&quot;: 4096 }" id="saveimage">SAVE CHANGES</button></li>
			</ul>
		</div>
	 </div>  	
    </div>
  </div>
</div>
<style>
.img-container {
  /* Never limit the container height here */
  max-width: 100%;
}

.img-container img {
  /* This is important */
  width: 100%;
}
.cropper-bg {background-image:none !important;}
.cropper-modal {background-color:#EEEEEE !important;opacity:0 !important;}

</style>
