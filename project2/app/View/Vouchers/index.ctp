<section class="inner-banner topheader">
       <div class="container">
	        <div class="banner-cont">
	           <h2><?php echo (($status == 0)?"Drafted Vouchers":"Published Vouchers"); ?></h2> 
               <div class="new-voucher">
                  <a class="btn-gradint" href="<?php echo SITE_LINK?>create-voucher">CREATE NEW</a> 
                </div>
	        </div>
       </div>
    </section>
    <!--/. banner sec -->
<a href="javascript:void(0)" id="top"></a>
<section class="new-vouchers">
   <div class="container">
		 <div class="tabbable tabs-header">
			  <div class="btn-group" role="group">
				  <ul class="nav nav-tabs">	
					<li <?php echo ($status==0)? '':'class="active"'?>><a href="<?php echo SITE_LINK?>drafted-vouchers" >Draft</a></li>
					<li <?php echo ($status==1)? '':'class="active"' ?>><a href="<?php echo SITE_LINK?>published-vouchers" >Published</a></li>
				  </ul>
				</div>              
		  </div>		  
		  <div class="tab-content tabs-body">
			<div class="tab-pane active" id="draft">
				<div class="voucher-boxCont draft">
					<?php $count=count($vouchers_draft);if($count==0){?><div class="box-card-nodata"><div class="alert alert-warning">No voucher in the list.</div></div><?php }else{ foreach ($vouchers_draft as $voucher_drt){ ?>
						
					<div class="box-card">
						<div class="box-info">
							<a href="javascript:void(0);">
								<div class="card-img">
									<?php echo ($voucher_drt['Voucher']['image']=="" || !file_exists(WWW_ROOT."img/voucher/".$voucher_drt['Voucher']['image']))?($this->Html->image("default_voucher.jpeg")):($this->Html->image("voucher/".h($voucher_drt['Voucher']['image']))); ?>
								</div>
							</a>
							<div class="card-overlay">
								<ul class="view-btns">
								   <!--<li><a href="<?php echo SITE_LINK?>view-voucher/<?php  echo $voucher_drt['Voucher']['id'] ?>"><?php echo $this->Html->image("eye-icon2.png",array("alt"=>"")); ?></a></li>-->
									<li><a data-value="<?php echo $voucher_drt['Voucher']['id']; ?>" href="javascript:void(0);" class="view_voucher"><?php echo $this->Html->image("eye-icon2.png",array("alt"=>"")); ?> </a></li>
								   <li><a data-value="<?php echo $voucher_drt['Voucher']['id']; ?>" href="javascript:void(0);" class="pview_voucher"><?php echo $this->Html->image("srch-icon1.png",array("alt"=>"")); ?> </a></li>
								   <?php if($status==1){?>
									<li><a href="javascript:void(0);"><?php echo $this->Html->image("link-icon1.png",array("alt"=>"")); ?></a></li>
									<?php }?>
								   
								</ul>
								<ul class="action-btns">
									 <li><a href="javascript:void();" id="<?php echo $voucher_drt['Voucher']['id']; ?>" class="copy-icon"></a></li>
									<li><a href="<?php echo SITE_LINK?>edit-voucher/<?php  echo $voucher_drt['Voucher']['id'] ?>"><?php echo $this->Html->image("edit-icon1.png",array("alt"=>"")); ?></a></li>                                      
									<!--li> <?php //echo $this->Html->link($this->Html->image('del-icon1.png', array('alt' => 'Delete','title' => 'Delete')), array('action' => 'delete',$voucher_drt['Voucher']['id']), array('escape' => false,'confirm' => 'Are you sure you want to delete?',$voucher_drt['Voucher']['title']	));?>  </li -->                                 
									<li><a href="javascript:void(0);" class="confirmdelete" action="<?php echo SITE_LINK."delete-voucher/".$voucher_drt['Voucher']['id'] ?>"><?php echo $this->Html->image('del-icon1.png', array('alt' => 'Delete','title' => 'Delete')); ?></a> <?php //echo $this->Html->link(, array('action' => 'delete',$voucher_drt['Voucher']['id']), array('escape' => false,'confirm' => 'Are you sure you want to delete?',$voucher_drt['Voucher']['title']	));?>  </li>                                 
								</ul>
							</div>
						</div>
					</div> 
					<?php } }?>
					<!--/. End box-card -->
				</div> <!--/.voucher-boxCont -->                    
			</div>
			<!--/. End tab-panel -->                
		  </div>
		</div> 
   </div>      
</section>
<!--/. My Account -->
<div class="modal in fade themeModel" tabindex="-1" role="dialog" id="cnfrmdelid">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Voucher Delete Confirmation</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Do you realy want to remove this voucher? If you delete this voucher, it will be lost forever.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary btn-theme" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-secondary btn-theme delvoucher">Ok</button>
      </div>
    </div>
  </div>
</div>
<div class="modal in fade themeModel" tabindex="-1" role="dialog" id="cnfrmcopyid">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Voucher Copy Confirmation</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>The copied Voucher will be displayed in Drafted Voucher section, and newly created voucher will be placed at the top of list.</p>
      </div>
      <div class="modal-footer">
        <!--button type="button" class="btn btn-primary btn-theme" data-dismiss="modal">Cancel</button-->
        <button type="button" class="btn btn-secondary btn-theme copyvoucher" data-dismiss="modal">Ok</button>
      </div>
    </div>
  </div>
</div>

<!--Voucher Offer Modal HTML -->
  <div id="voucher-offer-model" class="modal fade voucher-offer-model themeModel">
    <div class="modal-dialog sm-model mobile-offer">
      <div class="modal-content">
        <div class="modal-body">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <div class="vouch-offer"> 
          	<span class="top-time">9:30</span>
          <?php echo $this->Html->image("mobile-offer.png"); ?>
          
            <div class="offer-banner"><?php echo $this->Html->image("jusic-banner.jpg",array("id"=>"VoucherImg")); ?> </div>
            <div class="auto-bar">
              <div class="mobile-main">
                <div class="ng-header">
                  <div class="container mobile-container">
                    <div class="row">
                      <div class="col-sm-12"> <span class="back_arrow"><a href="javascript:void(0);"><?php echo $this->Html->image("back-arrow.png"); ?></a></span>Offer Details <span></span>
                        <div class="hd-right">
                          <ul class="list-inline">
                            <li><a href="javascript:void(0);"><?php echo $this->Html->image("fav-iocn.png",array("alt"=>"favorite"));?></a></li>
							<li><a href="javascript:void(0);"><?php echo $this->Html->image("share-iocn.png",array("alt"=>"Share"));?></a></li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <section class="jusic-banner">
                  <div class="cnt_middle">
                    <div class="cnt-contant">
                      <div class="snu-profile">
                        <div class="avtar-img" style="background:#fff;"> <img src='' id="av-img"></div>
                        <h1 class="text-center" id="bname"></h1>
                        <div class="row">
                          <div class="snu-student">
                            <ul class="list-inline">
                              <li>
                                <div class="banr-text"> <span class="disct"><div id="Voucherdiscount"></div></span>
                                  <p> Student <span>Discount</span></p>
                                </div>
                              </li>
                              <li>
                              <div class="banr-text" >
                                 <div id="bname1" class="break-content">
                                </div>                               
                              </li>
                            </ul>
                          </div>
                        </div>
                        <span class="show-status"><div id="Voucherlftmsg"></div></span> </div>
                      <div class="student-conditions">
                        <div class="blog-artiale">
                          <div class="map-left" id="map">  </div>
                          <div class="articale-right" style="float:right; width:68%;">
                            <p>Less than 0.5 miles away</p>
                            <p id="uadd"></p>
                          </div>
                        </div>
                        <div class="blog-artiale">
                          <h2> <?php echo $this->Html->image("about-help.png",array("alt"=>"About this discount"));?>About this discount</h2>
                          <p><div id="Voucherdescription"></div></p>
                          <a href="javascript:void(0);">Read More</a> </div>
                        <div class="blog-artiale">
                          <h2> <?php echo $this->Html->image("terms-condition.png",array("alt"=>"Terms & Conditions"));?>Terms & Conditions</h2>
                          <p><div id="Voucherterms"></div></p>
                          <a href="javascript:void(0);">Read More</a> </div>
                      </div>
                    </div>
                  </div>
                </section>
                <div class="mobile-footr">
                  <div class="container mobile-container">
                    <div class="container mobile-container">
                      <h2> <?php echo $this->Html->image("get-direcation.png",array("alt"=>"Get Directions"));?> Get Directions</h2>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!--Voucher Offer Modal HTML -->
