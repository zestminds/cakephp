<section class="banner">
<div class="container">
<div class="banner-cont merchant-page">
	<div class="bnr-left">
		<div class="download-app">
			<!--client data -->
			<div class="alchemist-discounts">
              <div class="alchemist-img"><img src="<?php echo SITE_LINK ?>img/profile/<?php echo $users["UserDetail"]["image"]?>" alt="<?php echo $users["UserDetail"]["business"]?>"></div>
              <div class="alchemist-cont">
                <h2><?php echo $users["UserDetail"]["business"]?></h2>
                <p>Get the app to use in store</p>
              </div>
            </div>	
            <!--client data -->	
            <!--voucher details  data -->		
			<div class="discounts-slide">
				<div class="owl-carousel owl-theme owl-loaded owl-drag">
					<!--voucher loop   data -->
					<?php if(empty($vouchers)){echo "No Voucher to dispaly";}else{  foreach ($vouchers as  $key=>$voucher){ ?>
					<div class="item">
						<div class="discount-item">
							<div class="item-left">
								<?php echo $this->Html->Image("discount-slide.jpg"); ?>		
								<div class="venu-text">
								<h2><?php echo $voucher["Voucher"]["title_discount"]?> Student Discount</h2>
								<h4><?php echo $voucher["Voucher"]["sub_title"]?></h4>
								<p>Get Complimentary </p>
								</div>
								<span class="show-status"><?php echo (!empty($voucher["Voucher"]["start_val"]))? $voucher["Voucher"]["start_val"]:$voucher["Voucher"]["end_val"] ?></span>
							</div>
							<div class="item-right"> <img src="<?php echo $voucher["Voucher"]["voucherImageUrl"]?>" alt="<?php echo $users["UserDetail"]["business"]?>" >	</div>
						</div>
					</div>
					<?php }} ?>

					<!--voucher loop   data -->	
				</div>
			</div>  
            <!--voucher details  data  -->	 
             <div class="application">
              <ul class="list-inline">
                <li><a href=""> <span class="fa fa-android st-appiocn"></span>
                  <p> available on <span> Google Store </span></p>
                  </a></li>
                <li><a href=""> <span class="fa fa-apple st-appiocn"></span>
                  <p>available on <span> Apple Store </span></p>
                  </a></li>
              </ul>
            </div>
		</div>
	</div>            
	<div class="bnr-right">
		<div class="phone-rt">
			<?php echo $this->Html->Image("phone.png",array("alt"=>"Rave App")); ?>
		</div>
	</div> 
</div>
</div>
</section>
<!--/. banner sec -->

<section class="who-they-are">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <div class="they-are-main">
            <div class="main-inr-ttl">
              <h2>Who they are</h2>
            </div>
            <p><?php echo (!empty($users["UserDetail"]["aboutme"]))?$users["UserDetail"]["aboutme"]:"No information provided."?></p>
            <div class="social-menus">
              <ul>
				  <?php if(!empty($users["UserDetail"]["facebookid"])) {?>
                <li><a href="<?php echo $users["UserDetail"]["facebookid"]?>" target="_blank"><i class="fa fa-facebook-f"></i></a></li>
                <?php } if(!empty($users["UserDetail"]["twitterid"])){?>
                <li><a href="<?php echo $users["UserDetail"]["twitterid"]?>" target="_blank"><i class="fa fa-twitter"></i></a></li>
                
                <?php } if(!empty($users["UserDetail"]["instagramid"])){?>
                <li><a href="<?php echo $users["UserDetail"]["instagramid"]?>" target="_blank"><i class="fa fa-instagram"></i></a></li>
                <?php }?>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
 </section>
<section class="map-main">
	  	  	  <iframe  src="https://www.google.com/maps?q=<?php echo $users['UserDetail']['address'];?>&output=embed" style="width:100%; height:444px;border:1px solid #f1f1f1"></iframe>
</section>
