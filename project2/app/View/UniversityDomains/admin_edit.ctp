<script>
	$(document).ready(function () {
    $('#UniversityDomainAdminEditForm').validate({ // initialize the plugin
        rules: {			
			 
			  "data[UniversityDomain][domain_ext]": {
				required: true				
			  }		
			},
			// Specify validation error messages
			messages: {			
				"data[UniversityDomain][domain_ext]": {
					required: "Please enter domain extension"			
				}
			},
			// Make sure the form is submitted to the destination defined
			// in the "action" attribute of the form when valid
			submitHandler: function(form) {
			  form.submit();
			}
		  });		
});
</script>
<div class="cont-right">
   <div class="cont-right-innr">
		<div class="main-hd-in">
			  <div class="row">
				<div class="col-sm-12">
				  <h2 class="title-das"> Edit Domain Extension</h2>              
				</div>
			  </div>
        </div>
		<div class="manage-marchant">
		<?php echo $this->Form->create('UniversityDomain'); ?>
			<fieldset>
				
			<?php
				echo $this->Form->input('id');
				echo $this->Form->input('domain_ext',array('label' => 'Doamin Extension'));				
				echo $this->Form->input('is_active');
			?>
			</fieldset>
		   <div style="margin-top:20px;margin-left:20px">
				  <button type="submit" class="btn btn-theme" id="save" name="save">SAVE</button>
				
					<a href="<?php echo SITE_LINK ?>manage-domain-extensions" class="btn btn-theme">cancel</a>		  
		</div>
		</div>
	</div>		
</div>
