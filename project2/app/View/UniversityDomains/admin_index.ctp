<div class="cont-right">
   <div class="cont-right-innr">	
	   <div class="main-hd-in">
			<div class="row">
				<div class="col-sm-12">
				  <h2 class="title-das"> Manage Domain Extensions</h2>
				  
					<div class="show-record">                	
					   <span> Show Records </span> 
					   <?php echo $this->Form->create("voucher",array("div"=>false,"type"=>"get")); 			
						 echo $this->Form->input('records',array("id"=>"records",'label'=>'',"options"=>$records,'class'=>'form-control',"selected"=>$limit));
						 echo $this->Form->end(); ?>	
					</div>
				</div>
			  </div>
			</div>
			<div class="universities index border-box">
				
				<?php echo $this->Form->create("UniversityDomain",array("div"=>false,)); ?>
				<div class="srch">
					<?php echo $this->element("admins/common",array("place"=>'Search by extenstion',"flag"=>false,"pageheader"=>'',"buttontitle"=>'no',"listflag"=>"no","action"=>'no')); ?>
					<div class="rhs_actions right">
						<a href="<?php echo SITE_LINK."add-domain-extension"; ?>">Add Domain Extension</a>			
					</div>
				</div>
				<div class="table-responsive">
				<table class="Marchant-table table table-bordered" width="100%">
				<tr>
						<th><?php echo $this->Form->input("check",array("label"=>false,"div"=>false,"id"=>'checkall',"type"=>'checkbox')); ?></th>
						<th>S.No</th>						
						<th><?php echo $this->Paginator->sort('domain_ext','Domain Extension'); ?></th>						
						<th><?php echo $this->Paginator->sort('is_active','Status'); ?></th>
						<th class="actions"><?php echo __('Actions'); ?></th>
				</tr>
				<?php foreach ($universitydomain as $extension): ?>
				<tr>
					<td><?php echo $this->Form->input("id.".$extension['UniversityDomain']['id'],array("class"=>'chk',"value"=>$extension['UniversityDomain']['id'],"type"=>'checkbox',"div"=>false,"label"=>false)); ?>
					<?php echo $this->Form->input("status.".$extension['UniversityDomain']['id'],array("type"=>'hidden',"value"=>($extension['UniversityDomain']['is_active'] == 1?0:1))); ?></td>
					<td><?php echo $sno;?>&nbsp;</td>					
					<td><?php echo h($extension['UniversityDomain']['domain_ext']); ?>&nbsp;</td>
					<td><?php echo h(($extension['UniversityDomain']['is_active'] == 1)?'Active':'Inactive');  ?>&nbsp;</td>	
					<td><a href="<?php echo SITE_LINK.'edit-domain-extension/'.$extension['UniversityDomain']['id']?>"><i class="fa fa-pencil"></i></a>	
					<?php echo $this->Html->link($this->Html->tag('i', '', array('class' => 'glyphicon glyphicon-trash')). " ",   array('action' => 'delete', $extension['UniversityDomain']['id']),array('escape'=>false),__('Are you sure you want to delete # %s?', $extension['UniversityDomain']['domain_ext']));?>
				</td>	
				</tr>
			<?php $sno++;endforeach; ?>
				</table>
				</div>
				<div class="pagination-main">
				<div class="paging pagination">
				<?php echo $this->Paginator->prev('<i class="fa fa-caret-left"></i>', array('escape' => false), null, array('class' => 'fa prev disabled'));
				
				echo $this->Paginator->numbers(array('separator' => ''));
				echo $this->Paginator->next('<i class="fa fa-caret-right "></i>', array('escape' => false), null, array('class'	=> 'fa next disabled'));
				?>
			   </div>  
			</div> 
				<?php echo $this->Form->end(); ?>
			</div>
		</div>	
	</div>		
</div>
<script>
$(document).ready(function () {
	$("#records").change(function(e) { 
			var val=$('#records').val();
			window.location.replace(SITE_LINK+"manage-domain-extensions?records="+val);			
				  	  
	});
});	
</script>
