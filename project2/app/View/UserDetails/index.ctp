<?php echo $this->element("topmenu"); ?>
<div class="userDetails index">
	<h2><?php echo __('User Details'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>	<th><?php echo $this->Paginator->sort('S.No.'); ?></th>
			<th><?php echo $this->Paginator->sort('id'); ?></th>			
			<th><?php echo $this->Paginator->sort('name'); ?></th>
			<th><?php echo $this->Paginator->sort('Joing Date'); ?></th>
			<th><?php echo $this->Paginator->sort('Verfication'); ?></th>			
			<th><?php echo $this->Paginator->sort('is_active'); ?></th>
						
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	</thead>
	<tbody>
	<?php $i = 1;foreach ($userDetails as $userDetail): ?>
	<tr>
		<td><?php echo $i;?>&nbsp;</td>	
		<td><?php echo h($userDetail['UserDetail']['id']); ?>&nbsp;</td>		
		<td><?php echo h($userDetail['UserDetail']['name']);?>?>&nbsp;</td>
		<td><?php echo h($userDetail['UserDetail']['created']); ?>&nbsp;</td>		
		<td><?php ?>&nbsp;</td>			
		<td><?php if(h($userDetail['UserDetail']['is_active'])==0) echo "INACTIVE"; else echo "ACTIVE"; ?></td>
			
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $userDetail['UserDetail']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $userDetail['UserDetail']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $userDetail['UserDetail']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $userDetail['UserDetail']['id']))); ?>
		</td>
	</tr>
<?php $i++; endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<?php echo $this->element("leftmenu_admin"); ?>
