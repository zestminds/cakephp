<script>
 $(document).ready(function () { 
	 
	 var userAgent = navigator.userAgent || navigator.vendor || window.opera;

      // Windows Phone must come first because its UA also contains "Android"
		if (/windows phone/i.test(userAgent)) {
			//return "Windows Phone";
			$(".msg_flash").html("Go to the Rave App and login with your registered email and new password.");
		}
		else if (/android/i.test(userAgent)) {
			//return "Android";
			$(".msg_flash").html("Go to the Rave App and login with your registered email and new password.");
		}
		// iOS detection from: http://stackoverflow.com/a/9039885/177710
		else if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
			//return "iOS";
			$(".msg_flash").html("Go to the Rave App and login with your registered email and new password.");
		}
		else
		{
			window.setTimeout(function(){
			// Move to a new location or you can do something else
			window.location.href = SITE_LINK;
			}, 7000);
		}
 
});
</script>
<section class="msg-banner">
    	<div class="container">
        	<div class="msg-content">                
                <h2><?php echo $this->Session->flash(); ?></h2>
                <p><div class="msg_flash"></div></p>
            </div>
        </div>
</section>
