
 <script>
 $(document).ready(function () {		
	 $('#notifyComingSoonForm').validate({ // initialize the plugin
		 ignore: "",
		 rules: {			
			 
			  "data[notify][useremailnotify]": {
				required: true,
				email: true
			  }
			},
			// Specify validation error messages
			messages: {		
						 
			   "data[notify][useremailnotify]": {
					required: "Please enter email id",
					email: "Please enter valid email id"						
				}
				
			},
			
			submitHandler: function(form) {	
					
				form.submit();
			}
	  });
});	  
</script>	
 <div id="resizeMe" class="coming-soon">
         <div class="container">
            <div class="coming-cont clearfix">
                 <div class="coming-logo col-sm-12">
                     <a href="#"><?php echo $this->Html->image("logo.png",array("alt"=>"Voucher Pic"));?></a>
                 </div>
                 <div class="coming-desc col-sm-12">
                     <h2>OUR APP WILL BE <span>READY SOON</span></h2>
                     <p>Enter your email below and we’ll let you know when it’s ready. Don’t miss out!</p>
                 </div>
                 <div class="coming-counter col-sm-12">
                     <div class="countdown-dispaly clearfix" id="countdown-display">
                        <div class="counterdown-content clearfix">
                            <span class="time-col">
                                <span class="counting counting-days">--</span>
                                <span class="subject"><span></span>days</span>
                            </span>
                            <span class="time-col">
                                <span class="counting counting-hours">--</span>
                                <span class="subject"><span></span>hours</span>
                            </span>
                            <span class="time-col">
                                <span class="counting counting-minutes">--</span>
                                <span class="subject"><span></span>minutes</span>
                            </span>
                            <span class="time-col">
                                <span class="counting counting-seconds">--</span>
                                <span class="subject"><span></span>seconds</span>
                            </span>
                        </div>
                    </div>
                 </div>
                 <div class="coming-notify col-sm-12">
                  <?php echo $this->Form->create('notify');?>
                     <div class="input_group">
                     	<?php echo $this->Form->input('useremailnotify',array('div'=>false,'id'=>'useremailnotify',"placeholder"=>"E-mail address", "class"=>"form-control","label"=>false ));?>
                         <button type="submit">NOTIFY ME</button>
                     </div>
                   </form>
                 </div>
            </div>
        </div>
</div>

