<!DOCTYPE html>
<html class="no-touch gecko firefox firefox66 firefox66_0 linux js no-hidpi datauri minw_1200 orientation_landscape" xmlns="http://www.w3.org/1999/xhtml" lang="en-US"><head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title><?php echo h($calendar['Calendar']['title']);?></title>
<meta name="description" content="<?php echo h($calendar['Calendar']['short_dec']); ?>" />
<meta property="og:title" content="<?php echo h($calendar['Calendar']['title']); ?> />
<meta property="og:image" content='<?php echo (($calendar['Calendar']['image']=="" || !file_exists(WWW_ROOT."img/voucher/".$calendar['Calendar']['image']))?(SITE_LINK."/img/default_voucher.jpeg"):(SITE_LINK."/img/events/".h($calendar['Calendar']['image']))); ?>' />
<meta property="og:site_name" content="https://events.fishookapp.com/"/>
<meta property="og:description" content="<?php echo h($calendar['Calendar']['short_dec']); ?>"/>

<meta name="twitter:card" content="summary_midium_image" />
<meta name="twitter:description" content="<?php echo h($calendar['Calendar']['short_dec']); ?>" />
<meta name="twitter:title" content="<?php echo h($calendar['Calendar']['title']); ?> Student Discount" />
<meta name="twitter:image" content="<?php echo (($calendar['Calendar']['image']=="" || !file_exists(WWW_ROOT."img/voucher/".$calendar['Calendar']['image']))?(SITE_LINK."/img/default_voucher.jpeg"):(SITE_LINK."/img/voucher/".h($calendar['Calendar']['image']))); ?>" />
<style>
.vouch-details {
    font-size: 0px;
    padding: 0px 35px;
    margin-top:30px;
}
.vouch-img_share {
    max-height: 500px;
    overflow: hidden;
    display: inline-block;   
    vertical-align: middle;
    font-size: 16px;
}
.vouch-desc {
    font-size: 16px;
    display: inline-block;    
    margin-top: -20px;
    vertical-align: top;
}
.col-sm-4 {
    width: 33.33333333%;
}
.col-sm-8 {
    width: 66.66666667%;
}
.col-sm-1, .col-sm-10, .col-sm-11, .col-sm-12, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9 {
	float: left;
}
.col-lg-1, .col-lg-10, .col-lg-11, .col-lg-12, .col-lg-2, .col-lg-3, .col-lg-4, .col-lg-5, .col-lg-6, .col-lg-7, .col-lg-8, .col-lg-9, .col-md-1, .col-md-10, .col-md-11, .col-md-12, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9, .col-sm-1, .col-sm-10, .col-sm-11, .col-sm-12, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-xs-1, .col-xs-10, .col-xs-11, .col-xs-12, .col-xs-2, .col-xs-3, .col-xs-4, .col-xs-5, .col-xs-6, .col-xs-7, .col-xs-8, .col-xs-9 {
	position: relative;
	min-height: 1px;
	padding-right: 15px;
	padding-left: 15px;
}
</style>

<script>
var SITE_LINK = "<?php echo SITE_LINK; ?>";
</script>
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<link rel="profile" href="http://gmpg.org/xfn/11">
<title>Rave – Know's what's happening.</title>
<link rel="dns-prefetch" href="https://s.w.org/">

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="shortcut icon" href="<?php echo SITE_LINK?>favicon.ico" type="image/x-icon">
<link rel="icon" href="<?php echo SITE_LINK?>favicon.ico" type="image/x-icon">
<link rel='stylesheet' id='uncode-style-css'  href='<?php echo SITE_LINK?>css/rave.css' type='text/css' media='all' />

<style type="text/css" media="print">@page { size: 602px 1024px; margin: 0; }</style></head>
<body class="home page-template-default page page-id-4 page-parent style-color-lxmt-bg smooth-scroller hormenu-position-left hmenu hmenu-position-right header-full-width main-center-align menu-mobile-animated menu-mobile-transparent menu-custom-padding textual-accent-color menu-has-cta wpb-js-composer js-comp-ver-5.6 vc_responsive hide-scrollup" data-border="0">
		<div class="body-borders" data-border="0"><div class="top-border body-border-shadow"></div><div class="right-border body-border-shadow"></div><div class="bottom-border body-border-shadow"></div><div class="left-border body-border-shadow"></div><div class="top-border style-light-bg"></div><div class="right-border style-light-bg"></div><div class="bottom-border style-light-bg"></div><div class="left-border style-light-bg"></div></div>	<div class="box-wrapper">
		<div class="box-container">
		<script type="text/javascript">UNCODE.initBox();</script>
		<div class="menu-wrapper menu-sticky menu-no-arrows no-header">
													
			<header id="masthead" class="navbar menu-primary menu-dark submenu-light style-dark-original menu-with-logo">
				<div class="menu-container style-color-jevc-bg menu-no-borders">
					<div class="row-menu limit-width">
						<div class="row-menu-inner">
							<div id="logo-container-mobile" class="col-lg-0 logo-container middle">
								<div id="main-logo" class="navbar-header style-dark">
									<a href="<?php echo SITE_LINK ?>" class="navbar-brand" data-padding-shrink="0" data-minheight="20"><div class="logo-image logo-light" data-maxheight="60" style="height: 60px;display:none;"><img src="<?php SITE_LINK?>/img/rave.png" alt="logo" class="img-responsive" width="620" height="200"></div><div class="logo-image logo-dark" data-maxheight="60" style="height: 60px;"><img src="<?php echo SITE_LINK?>/img/rave.png" alt="logo" class="img-responsive" width="620" height="200"></div></a>
								</div>
								<div class="mmb-container"><div class="mobile-menu-button mobile-menu-button-dark lines-button x2"><span class="lines"></span></div></div>
							</div>
							<div class="col-lg-12 main-menu-container middle" style="height: 0px;">
								<div class="menu-horizontal">
									<div class="menu-horizontal-inner">
										<div class="nav navbar-nav navbar-cta navbar-nav-last"></div></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</header>
	    </div>
	    <script type="text/javascript">UNCODE.fixMenuHeight();</script>
			<div class="main-wrapper">
				<div class="main-container">
					<div class="page-wrapper">
						<div class="sections-container">
						<script type="text/javascript">UNCODE.initHeader();</script>
						<article id="post-4" class="page-body style-light-bg post-4 page type-page status-publish has-post-thumbnail hentry">
							<div class="post-wrapper">
								<div class="row">
									<div class="col-sm-4">
										<div class="vouch-details">	
												<div class="vouch-img_share">
													<?php echo ($calendar['Calendar']['image']=="" || !file_exists(WWW_ROOT."img/events/".$calendar['Calendar']['image']))?($this->Html->image("default_voucher.jpeg")):($this->Html->image("events/".h($calendar['Calendar']['image']),array('width'=>'550','height'=>'300'))); ?>
											   </div>
										 </div>	   
									</div>		   
									<div class="col-sm-8">		   
											<div class="vouch-desc">	  
											  <ul>
													<li>
													 
													  <h1><?php echo h($calendar['Calendar']['title']); ?> </h1>
												  </li>
												  <li >
													  <h5><span><?php //echo $this->Html->image("icon-3.png"); ?></span> Date :  
																		 
															<?php echo $event_date;?>
													</h5>	
												  </li>		
												  <?php if ($calendar['Calendar']['ticket_free']==1) {?>
													<li >
													  <h5><span><?php //echo $this->Html->image("icon-2.png"); ?></span>		Ticket Price:  Free
														 </h5>
													</li>			  
												 <?php } else{?>			  
												  <li>
													  <h5><span><?php //echo $this->Html->image("icon-3.png"); ?></span> Ticket Price:  <?php echo $calendar['Calendar']['start_price']; if($calendar['Calendar']['end_price']!="") echo " - ".$calendar['Calendar']['end_price']; ?>
														</h5>
														
														 
													  <p><?php echo ($calendar['Calendar']['ticket_link']!="") ? "Get Ticket : ".$calendar['Calendar']['ticket_link']: '' ?></p>
												  </li>													  
												   <?php }?> 
												  <li >
													  <h5><span><?php //echo $this->Html->image("icon-3.png"); ?></span> Timing: 
																		 
															 <?php echo date("h:m:a",strtotime($calendar['Calendar']['start_hour'])); if($calendar['Calendar']['end_hour']!="") echo " - ".date("h:m:a",strtotime($calendar['Calendar']['end_hour'])); ?>
													</h5>	
												  </li>		
												  <li >
													  <h5><span><?php //echo $this->Html->image("icon-3.png"); ?></span> Location:  
																		 
															<?php echo $calendar['Calendar']['location'];?>
													</h5>	
												  </li>		
												  <li>
													  <h5><span><?php //echo $this->Html->image("icon-1.png"); ?></span> About this event</h5>
													  <p><?php echo h($calendar['Calendar']['long_dec']); ?></p>
												  </li>
												  <p class="map-main">
															<iframe  src="https://www.google.com/maps?q=<?php echo $calendar['Calendar']['location'];?>&output=embed" style="width:100%; height:444px;border:1px solid #f1f1f1"></iframe>
													</p>
									<!--
												  date_create(h($calendar['Calendar']['start_date'])); echo date_format($var,"jS M, Y"); ?><?php //echo ($calendar['Calendar']['day_status']==0 &&?>
									-->
											  </ul>
											</div>					
									</div>	
										
								 </div>	
							 </div>			
						</article>
							</div><!-- sections container -->
						</div><!-- page wrapper -->
				<footer id="colophon" class="site-footer">
					<div data-parent="true" class="vc_row style-color-jevc-bg row-container" data-section="1">
						<div class="row full-width row-parent" data-imgready="true">
							<div class="row-inner">
								<div class="pos-top pos-center align_left column_parent col-lg-4 single-internal-gutter">
									<div class="uncol style-dark">
										<div class="uncoltable">
											<div class="uncell no-block-padding">
												<div class="uncont">
													<div class="uncode-single-media  text-left">
														<div class="single-wrapper" style="max-width: 45%;">
															<div class="tmb tmb-light  tmb-media-first tmb-media-last tmb-content-overlay tmb-no-bg">
																<div class="t-inside">
																	<div class="t-entry-visual">
																		<div class="t-entry-visual-tc">
																			<div class="uncode-single-media-wrapper">
																				<img src="<?php echo SITE_LINK?>/img/rave.png" alt="" width="258" height="83">
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="icon-box icon-box-left">
														<div class="icon-box-icon fa-container">
															<a href="https://www.instagram.com/fishookapp/" target="_blank" class="text-color-xsdn-color custom-link">
																<i class="fa fa-instagram size_icon"></i></a>
														</div>
														<div class="icon-box-content">
															<p class=""><a href="https://www.instagram.com/fishookapp/">Follow Us On Instagram</a></p>
														</div>
													</div>
													<div class="uncode-single-media  text-left">
														<div class="single-wrapper" style="max-width: 34%;">
															<a class="single-media-link" href="https://www.moolarewards.co.uk/" target=" _blank">
																<div class="tmb tmb-light  tmb-media-first tmb-media-last tmb-content-overlay tmb-no-bg">
																	<div class="t-inside">
																		<div class="t-entry-visual">
																			<div class="t-entry-visual-tc">
																				<div class="uncode-single-media-wrapper">
																					<img src="<?php echo SITE_LINK?>/img/moola_rewards.png" alt="" width="140" height="20">
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</a>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="pos-top pos-center align_left column_parent col-lg-4 single-internal-gutter">
									<div class="uncol style-dark">
										<div class="uncoltable">
											<div class="uncell no-block-padding">
												<div class="uncont">
													<div class="uncode_text_column">
														<p><a href="<?php echo SITE_LINK?>privacy_policy">Privacy Policy</a></p>
														<p><a href="<?php echo SITE_LINK?>terms_and_conditions">Terms &amp; Conditions</a></p>
														<p><a href="<?php echo SITE_LINK?>cookie_policy">Cookie Policy</a></p>
														<p><a href="<?php echo SITE_LINK?>contact_us">Contact Us</a></p>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="pos-bottom pos-center align_left column_parent col-lg-4 single-internal-gutter">
									<div class="uncol style-dark">
										<div class="uncoltable">
											<div class="uncell no-block-padding">
												<div class="uncont">
													<div class="uncode-single-media  text-left">
														<div class="single-wrapper" style="max-width: 100%;">
															<div class="tmb tmb-light  tmb-media-first tmb-media-last tmb-content-overlay tmb-no-bg">
																<div class="t-inside">
																	<div class="t-entry-visual">
																		<div class="t-entry-visual-tc">
																			<div class="uncode-single-media-wrapper">
																				<img src="<?php echo SITE_LINK?>/img/enterprise-logo.png" alt="" width="516" height="86">
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="uncode_text_column"><p>© 2019 Rave. All rights reserved</p>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
						<script id="script-179785" data-row="script-179785" type="text/javascript">if ( typeof UNCODE !== "undefined" ) UNCODE.initRow(document.getElementById("script-179785"));</script>
							</div>
						</div>
					</div>
				</footer>
			</div><!-- main container -->
		</div><!-- main wrapper -->
	</div><!-- box container -->
</div><!-- box wrapper -->
<div class="style-light footer-scroll-top footer-scroll-higher" style="display: none;">
		<a href="#" class="scroll-top"><i class="fa fa-angle-up fa-stack fa-rounded btn-default btn-hover-nobg"></i></a>	
</div>

<div class="gdpr-overlay"></div>
<div class="gdpr gdpr-privacy-bar" style="display:none;">
	<div class="gdpr-wrapper">
		<div class="gdpr-content">
			<p>
				We use cookies to give you the best online experience and to 
personalize the content and advertising shown to you. By continuing to 
browse this site you are confirming you agree with our policy.			</p>
		</div>
		<div class="gdpr-right">
			<button class="gdpr-preferences" type="button">Privacy Preferences</button>
			<button class="gdpr-agreement btn-accent btn-flat" type="button">I Agree</button>
		</div>
	</div>
</div>
<!--
<div class="gdpr gdpr-privacy-preferences">
	<div class="gdpr-wrapper">
	<form method="post" class="gdpr-privacy-preferences-frm" action="https://fishookapp.com/wp-admin/admin-post.php">
		<input type="hidden" name="action" value="uncode_privacy_update_privacy_preferences">
		<input type="hidden" id="update-privacy-preferences-nonce" name="update-privacy-preferences-nonce" value="229f372ccf">
		<input type="hidden" name="_wp_http_referer" value="/">	
		
		<header>
			<div class="gdpr-box-title">
				<h3>Privacy Preference Center</h3>
				<span class="gdpr-close"></span>
			</div>
		</header>
		<div class="gdpr-content">
			<div class="gdpr-tab-content">
				<div class="gdpr-consent-management gdpr-active">
					<header>
						<h4>Privacy Preferences</h4>
					</header>
					<div class="gdpr-info">
						<p>Privacy Policy<br>
						<br>
						Our aim is to provide a platform for businesses and event organisers to 
						advertise and promote events and activities for users to discover <br>
						<br>
						We are committed to providing excellent service to all customers, 
						including respecting their concerns about privacy and taking the 
						relevant measures to safeguard their personal data in accordance with 
						applicable data protection legislation. We have provided this Privacy 
						Policy to help you, the user, understand how we collect, use and protect
						your information when you visit and use our website, mobile 
						application, social media pages, and other technological platforms in 
						whatever form we promote events (together referred to as our 
						“Platforms”), and how that information will be treated. Please read the 
						privacy policy carefully to understand our views and practices regarding
						your information and how we will treat it.</p>
					</div>
				</div>
			</div>
		</div>
		<footer>
		<input type="submit" class="btn-accent btn-flat" value="Save Preferences">
		</footer>
	</form>
	</div>
</div>
-->

<?php
echo $this->Html->script(array('moola/jquery-2-2-4.min','js-cookie.min','uncode-privacy-public.min'));
?>
<script>
	var SITE_LINK = "<?php echo SITE_LINK; ?>";
</script>
<?php
echo $this->Html->script(array('loginvalidate','signupvalidate','resetPassword','jquery.validate.min','jquery.form.min','fblogin'));
?>

</body>
</html>
