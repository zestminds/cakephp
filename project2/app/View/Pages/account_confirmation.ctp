<script>
 $(document).ready(function () { 
  //~ window.setTimeout(function(){
	//~ // Move to a new location or you can do something else
	//~ //window.location.href = SITE_LINK;
	//~ }, 7000);
});
</script>
<div class="account-confirm">
	<div class="moola-clogo"><?php echo $this->Html->image("rave.png",array("alt"=>"moola","title"=>"moola","class"=>"home_logo")); ?></div>
	<div class="moola-confirm-check"><?php echo $this->Html->image("right.png",array("alt"=>"Congrats!","title"=>"Congrats!")); ?></div>
	<h1>Congrats!</h1>
	<p>Your account has been confirmed successfully.</p>
	<p>Go to the Rave App and login with your registered email.</p>
	<h4>Follow Us on</h4>
	<ul class="ac-social">
		<li><a href="https://www.facebook.com/" title="Facebook" target="_blank"><?php echo $this->Html->image("facebook.png",array("alt"=>"Facebook","title"=>"Facebook")); ?></a></li>
		<li><a href="https://twitter.com/" title="Twitter" target="_blank"><?php echo $this->Html->image("Twitter.png",array("alt"=>"Twitter","title"=>"Twitter")); ?></a></li>
		<li><a href="https://www.youtube.com/" title="YouTube" target="_blank"><?php echo $this->Html->image("youtube.png",array("alt"=>"YouTube","title"=>"YouTube")); ?></a></li>
	</ul>
</div>
