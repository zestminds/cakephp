<script src="http://malsup.github.com/jquery.form.js"></script> <script>
	$(document).ready(function () {
    $('#ContactContactUsForm').validate({ // initialize the plugin
        rules: {
			 
			  "data[Contact][name]": {
				required: true				
			  },
			 "data[Contact][email]": {
				required: true,
				email : true		
			  },
			 "data[Contact][message]": {
				required: true			
			  },
			  "contactus_hiddenRecaptcha": { required: true }			
			},
			// Specify validation error messages
			messages: {			
				 "data[Contact][name]": {
					required: "Please enter a name"			
				},			 
			  "data[Contact][email]" :{
				required: "Please enter an email",
				email : "Please enter valid email"			
			  }
			  //~ "data[Contact][message]": {
					//~ required: "Please enter message"			
				//~ },
			  //~ "contact_hiddenRecaptcha": {
				//~ required: "Invalid captcha"					
			  //~ }
			},
			// Make sure the form is submitted to the destination defined
			// in the "action" attribute of the form when valid
			submitHandler: function(form) {		
				$('#error_msg_contact').hide();
				//$("#error_msg_captcha_contact").hide();
				//if ($("#contactus_hiddenRecaptcha").val().trim() == "") {					
				//	$('#error_msg_contact').hide();
					//$("#error_msg_captcha_contact").show();
				//	return false;
				//}
					
			   $('#ContactContactUsForm').ajaxSubmit(function(response) {
					response = JSON.parse(response);	
					 $('#error_msg_contact').empty();					
					if ( response.status ) {						
						//location.href = SITE_LINK+response.url;		
						$("#success_msg_contact").show();	
						$("#ContactContactUsForm").trigger("reset");
						$("#success_msg_contact").slideDown(function() {
							setTimeout(function() {
								$("#success_msg_contact").slideUp();
							}, 5000);
						});
							
					} else {						
						  $('#error_msg_contact').text(response.message);
						// $("#success_msg_contact").hide();
						 $('#error_msg_contact').show();
						//  grecaptcha.reset(widgetId1);
						 // $('#contactus_hiddenRecaptcha').val('');
						return false;
					}
				});
			}
		  });		
});
</script>

 <section class="inner-banner topheader"	>
       <div class="container">
	        <div class="banner-cont"> 
            	<h2>CONTACT US</h2>
                <h5></h5>
	        </div>
       </div>
    </section>
    <!--/. banner sec -->
    
 <section class="new-vouchers insights">
       <div class="container">
       		<div class="shadow-bg">
       			<div class="">
	                <div class="inside-ttl">
	                    <h4>Contact Us</h4>
	                    <p>Feel Free To Send Enquiries</p>
	                </div>
	                <div class="contact-page">						
	                 <div class="alert alert-warning" id="error_msg_contact" style="display:none;"></div>
					<div class="alert alert-success" id="success_msg_contact" style="display:none;"><strong>Congratulations!</strong> E-Mail sent successfully.</div>
	                	<?php echo $this->Form->create("Contact"); ?>
	                		<div class="contact-field">
	                			<label class="contact-field-label"><?php echo $this->Html->image("i1.png",array("alt"=>"Name","title"=>"Name")); ?>Name</label>
	                			
								<?php echo $this->Form->input("name",array("maxlength"=>200,"class"=>"field-txt","div"=>false,"label"=>false,"placeholder"=>"")); ?>

	                		</div>
	                		<div class="contact-field">
	                			<label class="contact-field-label"><?php echo $this->Html->image("i2.png",array("alt"=>"Email","title"=>"Email")); ?>Email</label>
	                			<?php echo $this->Form->input("email",array("maxlength"=>200,"class"=>"field-txt","div"=>false,"label"=>false,"placeholder"=>"")); ?>
	                		</div>
	                		<div class="contact-field">
	                			<label class="contact-field-label"><?php echo $this->Html->image("i3.png",array("alt"=>"Phone Number","title"=>"Phone Number")); ?>Phone Number</label>
	                			<?php echo $this->Form->input("phone",array("maxlength"=>200,"class"=>"field-txt","div"=>false,"label"=>false,"placeholder"=>"")); ?>
	                		</div>
	                		<div class="contact-field">
	                			<label class="contact-field-label"><?php echo $this->Html->image("i4.png",array("alt"=>"Message","title"=>"Message")); ?>Message</label>
	                			<?php echo $this->Form->input("message",array("type"=>"textarea","maxlength"=>200,"class"=>"field-txt","div"=>false,"label"=>false,"placeholder"=>"","rows"=>"3")); ?>
	                		</div>
<!--
	                		<div>
	                		<div class="g-recaptcha_contactus" data-sitekey=<?php //echo SITE_KEY; ?> id="RecaptchaField5"></div>
								<input type="hidden" class="hiddenRecaptcha" name="contactus_hiddenRecaptcha" id="contactus_hiddenRecaptcha">		
	                		</div>
-->
	                		<div class="text-right">
	                			<input type="submit" value="SUBMIT" class="field-btn">	                		
	                		</div>
	                	<?php echo $this->Form->end(); ?>
	                </div>
	            </div> <!--/.Audience -->
       		</div>
       </div>      
    </section>
   
