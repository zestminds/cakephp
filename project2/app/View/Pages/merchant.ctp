 <section class="banner">
       <div class="container">
        <div class="banner-cont">
            <div class="bnr-left">
                <div class="download-app">
                    <h1>Easily Create Beautiful <span>VOUCHERS & DEALS FOR STUDENTS</span></h1>
                    <div class="banner-btns">
                    	<a href="#regModel" class="round-btn btn-blue" data-toggle="modal" >TRY FOR FREE</a>
                    	
                    </div>
                </div>
            </div>            
            <div class="bnr-right">
                <div class="phone-rt">
                <?php echo $this->Html->image("phone.png",array("alt"=>"phone app")); ?>
                </div>
            </div> 
        </div>
    </div>
    </section>
    <!--/. banner sec -->

    <section class="our-services">
        <div class="container">
            <div class="main-ttl">
                 <h2><span>Rave</h2>
                 <p>Signup for a free account today to start advertising discounts for students now. Create unlimited vouchers with deals for all times of the day as well as specific days of the week.
Market your goods and services directly to students instantly.</p>
            </div>
        </div>
        <div class="services-list">
        	 <div class="container">
        	 	 <div class="serv-listing">
        	 	 	 <div class="serv-item">
        	 	 	 	<div class="serv-info">
	        	 	 	 	 <div class="serv-img">
								 <?php echo $this->Html->image("serv-img1.jpg",array("alt"=>"phone app")); ?>
	        	 	 	 	 </div>
	        	 	 	 	 <div class="serv-desc">
	        	 	 	 	 	<h4>Target Students Directly</h4>
	        	 	 	 	 	<p>Create discount vouchers online.</p>
	        	 	 	 	 	<a href="#regModel" data-toggle="modal">Try for free!</a>
	        	 	 	 	 </div>
	        	 	 	</div>
        	 	 	 </div> <!--/.Service-item -->
        	 	 	 <div class="serv-item">
        	 	 	 	<div class="serv-info">
	        	 	 	 	 <div class="serv-img">
								 <?php echo $this->Html->image("serv-img2.jpg",array("alt"=>"phone app")); ?>
	        	 	 	 	 </div>
	        	 	 	 	 <div class="serv-desc">
	        	 	 	 	 	<h4>Fair Pricing</h4>
	        	 	 	 	 	<p>Create unlimited vouchers for just £9.99 + VAT per month.</p>
	        	 	 	 	 	<a href="#regModel" data-toggle="modal">Try for free!</a>
	        	 	 	 	 </div>
	        	 	 	</div>
        	 	 	 </div> <!--/.Service-item -->
        	 	 	 <div class="serv-item">
        	 	 	 	<div class="serv-info">
	        	 	 	 	 <div class="serv-img">
								 <?php echo $this->Html->image("serv-img3.jpg",array("alt"=>"phone app")); ?>
	        	 	 	 	 </div>
	        	 	 	 	 <div class="serv-desc">
	        	 	 	 	 	<h4>No Contracts</h4>
	        	 	 	 	 	<p>There’s no tie in, just advertise what you want when you want.</p>
	        	 	 	 	 	<a href="#regModel" data-toggle="modal">Try for free!</a>
	        	 	 	 	 </div>
	        	 	 	</div>
        	 	 	 </div> <!--/.Service-item -->
        	 	 	 <div class="serv-item">
        	 	 	 	<div class="serv-info">
	        	 	 	 	 <div class="serv-img">
								 <?php echo $this->Html->image("serv-img4.jpg",array("alt"=>"phone app")); ?>
	        	 	 	 	 </div>
	        	 	 	 	 <div class="serv-desc">
	        	 	 	 	 	<h4>Proven Results</h4>
	        	 	 	 	 	<p>We provide you with a breakdown of the performance of each voucher.</p>
	        	 	 	 	 	<a href="#regModel" data-toggle="modal">Try for free!</a>
	        	 	 	 	 </div>
	        	 	 	</div>
        	 	 	 </div> <!--/.Service-item -->
        	 	 </div>
        	 </div>
        </div>
    </section>
     <!--/. About sec -->

    <section class="vedio-tutorial"> 
         <div class="video-frame">
            <div class="container">
                <div class="video">
					<?php // echo $this->Html->image("video-img.jpg",array("alt"=>"phone app")); ?>
					<iframe width="100%" height="561" src="https://www.youtube.com/embed/5UHk1yLGLIM?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                </div>
            </div>
         </div>
         <div class="container">      
            <div class="vedio-cont">
                <div class="main-inr-ttl">
                     <h2>Advertiser Demo Video</h2>
                </div>
                <p>Watch the video to see how easy it is to advertise directly to students by creating discount vouchers. Students see the discounts in their rave mobile app. Increase sales by offering discounts to cover slack times. Vouchers can be setup to cover different times in the day as well as different  days of the week. <a href="#regModel" data-toggle="modal">Try for free!</a></p>
            </div>
         </div>
    </section>
     <!--/. Video sec -->
