<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8_bin">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<?php if (isset($title_for_layout)) {  ?>
	<title><?php echo $title_for_layout; ?></title>
	
<?php } else { ?>
<title><?php echo $title_for_layout; ?></title>
<?php } ?>
<?php if (isset($keywords)) {  ?>
	<meta name="keywords" content="<?php echo $keywords; ?>" />
<?php } else { ?>
	<meta name="keywords" content="Rave" />
<?php } ?>
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<!-- css -->
<link rel="shortcut icon" href="<?php echo SITE_LINK?>favicon.ico" type="image/x-icon">
<link rel="icon" href="<?php echo SITE_LINK?>favicon.ico" type="image/x-icon">
<?php 

echo $this->Html->css(array("moola/font-awesome.min.css","moola/bootstrap.min","fishook/style","internal","moola/datetime-picker-4-7-14-min"));
echo $this->Html->script(array('moola/jquery-2-2-4.min'));
 ?>

<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>

<script>
var SITE_LINK = "<?php echo SITE_LINK; ?>";
var WWW_ROOT="<?php echo WWW_ROOT; ?>";
$(document).ready(function(){
	$(document).ajaxError(function(e,x,s,err){
		if(x.status === 403){
			window.location='<?php echo $this->Html->url('/')?>';
		}
	});	
	
});
</script>
<!--[if IE]>
	<script src="js/html5shiv.js"></script>
<![endif]-->
<meta name="google-site-verification" content="kxv_CaLEubj-OwFp-sEvKGm4O_Sd7S00bCKbL9gRaTY" />
</head>
<body class="my-voucher" style="margin:0;">
<div class="main" id="main"> 
  <!-- Preloader -->
  <div class="preLoader"></div>
	<?php echo $this->Session->flash(); ?>	
	<?php if ( !$this->Session->read("Auth.User.id") ) {
		echo $this->element("moolaheaderwithoutlogin"); 
    }
    else{
		echo $this->element("headerwithlogin"); 
	}
    ?>
    <!-- End header sec -->

    <?php echo $content_for_layout; ?>

    <?php echo $this->element("footer"); ?>
    
<?php echo $this->Html->script(array("moola/bootstrap.min","fishook/custom","change_password","moola/moment.js")); ?>
<?php echo $this->element("jscssloader",array("js"=>$jsArray,"css"=>$cssArray)); ?>
<script type="text/javascript" src="<?php echo SITE_LINK ?>js/moola/datetime-picker-4-7-14-min.js"></script>
<script src="<?php echo SITE_LINK ?>js/app.js"></script>
<script src="<?php echo SITE_LINK ?>js/bootstrap-tagsinput.js"></script>
<script type="text/javascript">
   $(function() {
	  $('.datetimepicker1').datetimepicker({
          //format: "DD/MM/YYYY"
          format: "YYYY-MM-DD",
         // minDate:new Date()
	  });
	  $('.datetimepicker2').datetimepicker({
           format: 'LT',
            useCurrent:false
           
	  });
	});
	//~ $(window).keydown(function(event){
		//~ if(event.keyCode == 13) {
		  //~ event.preventDefault();
		  //~ return false;
		//~ }
	//~ });
</script>
<?php if ( $this->params['controller'] == 'pages' && $this->params['action'] == 'faq' || $this->params['action'] == 'student_support_faqs') { ?>
<script>
	$('.parent > a').click(function() {
		$(this).parent('.parent').siblings().removeClass('active');
		$(this).parent('.parent').toggleClass('active');
		$(this).parent('.parent').siblings().children('.sub-lising').slideUp();
        $(this).parent('.parent').children('.sub-lising').slideToggle();
    });
</script>	
<?php } 
 if ( $this->params['controller'] == 'pages' )
 {?>
	 <script src="https://www.google.com/recaptcha/api.js?onload=CaptchaCallback&render=explicit" async defer></script>
<?php }?> 
<script>
	
//~ var myVar;
//~ 
//~ function myFunction() {
    //~ myVar = setTimeout(showPage, 1000);
//~ }

</script>
 <?php echo $this->element('sql_dump'); ?>
</body>
</html>
