<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Rave : Home</title>
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<link rel="shortcut icon" href="<?php echo SITE_LINK?>favicon.ico" type="image/x-icon">
<link rel="icon" href="<?php echo SITE_LINK?>favicon.ico" type="image/x-icon">
<!-- css -->
<?php 
echo $this->Html->css(array("moola/bootstrap.min","moola/font-awesome.min","moola/owl.carousel","moola/style","moola/responsive","internal","moola/img-upload","moola/datetime-picker-4-7-14-min","fishook/style")); ?>
<?php echo $this->Html->script(array("moola/jquery-2-2-4.min"))?>

<script>
var SITE_LINK = "<?php echo SITE_LINK; ?>";
</script>
<!--[if IE]>
	<script src="js/html5shiv.js"></script>
<![endif]-->
</head>
<body class="create-voucher">
<div class="main"> 
  <!-- Preloader -->
  <div class="preLoader"></div>
	<?php echo $this->Session->flash(); ?>	
	<?php if ( !$this->Session->read("Auth.User.id") ) {
		echo $this->element("moolaheaderwithoutlogin"); 
    }
    else{
		echo $this->element("headerwithlogin"); 
	}
    ?>
    <!-- End header sec -->

    <?php echo $content_for_layout; ?>

    <?php echo $this->element("footer"); ?>
    
<?php echo $this->Html->script(array("moola/bootstrap.min","moola/owl.carousel","moola/custom","moola/moment","moola/popper.min")); ?>
 <?php echo $this->element("jscssloader",array("js"=>$jsArray,"css"=>$cssArray)); ?>

<?php if ( $this->params['controller'] == 'vouchers' ) { ?>	
<script type="text/javascript" src="<?php echo SITE_LINK ?>js/moola/datetime-picker-4-7-14-min.js"></script>

<script src="<?php echo SITE_LINK ?>js/app.js"></script>
<script src="<?php echo SITE_LINK ?>js/bootstrap-tagsinput.js"></script>
<script type="text/javascript">
   $(function() {
	  $('.datetimepicker1').datetimepicker({
          //format: "DD/MM/YYYY"
          format: "YYYY-MM-DD",
         // minDate:new Date()
	  });
	  $('.datetimepicker2').datetimepicker({
           format: 'LT'
	  });
	});
</script>
<script>
 
$(document).ready(function(){		
		
	$("div.weekdays").children("label").on("click",function(){	
		if ($("#repeat_onW").is(":checked")) { 
		$(this).toggleClass("selected");
		}
		});		
	$(document).on("click",".weekoption",function()
	{	
		if ($(this).val() == "W") {
			$(".weekdays").each(function(){
			$(this).children("input").removeAttr("disabled");
			});
		} else {
			$(".weekdays").each(function(){
			$(this).children("label").removeClass("selected");				
			$(this).children("input").attr("disabled","DISABLED");
			});
		}	
	});
	$(window).keydown(function(event){
		if(event.keyCode == 13) {
		  event.preventDefault();
		  return false;
		}
	});
	$("#VoucherStartHour").val($("#tmpstarthr").val());
	$("#VoucherEndHour").val($("#tmpendhr").val());
	$("#VoucherStartDate").val($("#tmpstartdate").val());
	$("#VoucherEndDate").val($("#tmpenddate").val());

});

  </script>

<?php } ?>
<?php if ( $this->params['controller'] == 'pages' && $this->params['action'] == 'faq') { ?>
<script>
	$('.parent > a').click(function() {
		$(this).parent('.parent').siblings().removeClass('active');
		$(this).parent('.parent').toggleClass('active');
		$(this).parent('.parent').siblings().children('.sub-lising').slideUp();
        $(this).parent('.parent').children('.sub-lising').slideToggle();
    });
</script>	
<?php } ?>
 <?php echo $this->element('sql_dump'); ?>

</body>
</html>
