<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Rave : Home</title>
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<link rel="shortcut icon" href="<?php echo SITE_LINK?>favicon.ico" type="image/x-icon">
<link rel="icon" href="<?php echo SITE_LINK?>favicon.ico" type="image/x-icon">
<!-- css -->
<?php 
echo $this->Html->css(array("moola/bootstrap.min","moola/font-awesome.min","moola/owl.carousel","moola/style","moola/responsive","internal"));
echo $this->Html->script(array('moola/jquery-2-2-4.min')); ?>
<script>
var SITE_LINK = "<?php echo SITE_LINK; ?>";
</script>
<!--[if IE]>
	<script src="js/html5shiv.js"></script>
<![endif]-->
</head>
<body class="my-voucher">
<div class="main"> 
  <!-- Preloader -->
  <div class="preLoader"></div>
	<?php echo $this->Session->flash(); ?>	
	    <!-- End header sec -->

    <?php echo $content_for_layout; ?>

    <?php echo $this->element("footer"); ?>
    
<?php echo $this->Html->script(array("moola/bootstrap.min","moola/owl.carousel","moola/custom","moola/moment")); ?>
 <?php echo $this->element("jscssloader",array("js"=>$jsArray,"css"=>$cssArray)); ?>

<?php if ( $this->params['controller'] == 'pages' && $this->params['action'] == 'faq') { ?>
<script>
	$('.parent > a').click(function() {
		$(this).parent('.parent').siblings().removeClass('active');
		$(this).parent('.parent').toggleClass('active');
		$(this).parent('.parent').siblings().children('.sub-lising').slideUp();
        $(this).parent('.parent').children('.sub-lising').slideToggle();
    });
</script>	
<?php } ?>
</body>
</html>
