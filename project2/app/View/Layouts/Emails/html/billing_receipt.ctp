<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>:: Billing Receipt ::</title>
<!-- css -->
<!--[if IE]>
    <script src="js/html5shiv.js"></script>
<![endif]-->
<style type="text/css">
@import url('https://fonts.googleapis.com/css?family=Open+Sans:400,600,700');
@import url('https://fonts.googleapis.com/css?family=Roboto:500');
body {
	font-family: 'Open Sans', sans-serif;
	margin: 0;
	padding: 0;
	background-color: #f6f6f6;
	-ms-text-size-adjust: 100%;
	-webkit-text-size-adjust: 100%;
}
.robot{
    font-family: 'Roboto', sans-serif;
}
* {
	-moz-box-sizing: border-box;
	-webkit-box-sizing: border-box;
	box-sizing: border-box;
}
h2{
    color: #e2872b; 
    font-size: 25px;
    font-family: 'Roboto', sans-serif; 
    font-weight: 500;
}
img {
	max-width: 100%;
}
table {
	border-spacing: 0;
}
table td {
	border-collapse: collapse;
}
.table-main {
	width: 1080px;
	margin: 0 auto;
    padding-bottom: 35px;
}
</style>
</head>
<body style="background: #f7f7f8;">
<div class="table-main" style="background: #f7f7f8">
  <table cellpadding="0" cellspacing="0" border="0" width="100%" style="width:100%; background: #f7f7f8; color:#fff; font-family: 'Open Sans', sans-serif;">
    <tr>
      <td>
          <img style="border: none; display: block; width: 100%;" src="<?php echo SITE_LINK; ?>img/header-bg.png" alt="" title="">
      </td>
    </tr>
    <tr>
        <td>
          <div style="margin-top:-227px; padding: 0 25px;">
                <div style="background:rgba(255,255,255,0.97); box-shadow:0 2px 2px rgba(0,0,0,0.1); -moz-box-shadow:0 2px 2px rgba(0,0,0,0.1); -webkit-box-shadow:0 2px 2px rgba(0,0,0,0.1); padding:35px 35px 15px 35px; position: relative; border-radius: 15px; -moz-border-radius: 15px; -webkit-border-radius: 15px;">
                    <div style="padding: 0 30px;">
                        <h2 style="margin:0;color: #e2872b;font-size: 25px;font-family: 'Roboto', sans-serif; padding:0 0 15px 12px; border-bottom: 2px solid #edf0f3; font-weight: 500;">Billing Receipt</h2>
                        <div style="padding:15px;">
                            <h3 style="color: #565d65; margin: 0;font-family: 'Open Sans', sans-serif; font-weight: 600; font-size:18px;">Hi Stuart,</h3>
                            <p style="color: #565d65; font-size: 14px;font-family: 'Open Sans', sans-serif; line-height: 24px;">This email confirms that we’ve received your payment. We also wanted to take a moment to thank you personally. For our small tea, each customer’s success means that we’re making a difference. If you’re learning about how to start or run a better business, it means we’re doing something right. Your continued support keeps our product team improving LivePlan and the computer servers humming. For that, we’re extremely grateful! </p>
                            <h4 style="color: #565d65; font-size: 16px;font-family: 'Open Sans', sans-serif; font-weight: 600; margin: 0;">So thank you again, from out team to yours!</h4>
                            <h3 style="color: #38404b;font-family: 'Open Sans', sans-serif; margin:45px 0 5px 0; font-weight: 600; font-size: 18px;">Here’s a more detailed payment receipt for your records:</h3>
                        </div>
                    </div>
                    <div style="padding: 30px; background: #f2f8fc; border: 1px solid #c3d8e6;border-radius: 15px; -moz-border-radius: 15px; -webkit-border-radius: 15px;">
                        <h2 style="margin:0;color: #e2872b;font-size: 25px;font-family: 'Roboto', sans-serif; padding:0 0 15px 12px; border-bottom: 2px solid #edf0f3; font-weight: 500;">About this charge</h2>
                        <div style="padding:15px 13px;">
                            <table>
                                <tr>
                                    <td width="260" valign="top">
                                        <h4 style="color: #565d65; font-size: 16px;font-family: 'Open Sans', sans-serif; font-weight: 600; margin:0 0 5px 0;">Total amount billed:</h4>
                                        <h2 style="color: #565d65;font-family: 'Open Sans', sans-serif; font-weight: 600; margin: 0;font-size: 21px;">$19.95</h2>
                                    </td>
                                    <td  width="385" valign="top">
                                        <h4 style="color: #565d65; font-size: 16px;font-family: 'Open Sans', sans-serif; font-weight: 600; margin:0 0 5px 0;">Service Dates:</h4>
                                        <h2 style="color: #565d65;font-family: 'Open Sans', sans-serif; font-weight: 600; margin: 0;font-size: 21px;">Oct 28 to Sept, 2018</h2>
                                    </td>
                                    <td valign="top">
                                        <h4 style="color: #565d65; font-size: 16px;font-family: 'Open Sans', sans-serif; font-weight: 600; margin:0 0 5px 0;">Billing Date:</h4>
                                        <h2 style="color: #565d65;font-family: 'Open Sans', sans-serif; font-weight: 600; margin: 0; font-size: 21px;">Aug 28, 2018 at <br>4:30 PM BST</h2>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="260" valign="top">
                                        <h4 style="color: #565d65; font-size: 16px;font-family: 'Open Sans', sans-serif; font-weight: 600; margin:25px 0 5px 0;">Next billing Date:</h4>
                                        <h2 style="color: #565d65;font-family: 'Open Sans', sans-serif; font-weight: 600; margin: 0;font-size: 21px;">Sept 28, 2018 at <br>4:30 PM BST</h2>
                                    </td>
                                    <td  width="385" valign="top">
                                        <h4 style="color: #565d65; font-size: 16px;font-family: 'Open Sans', sans-serif; font-weight: 600; margin:25px 0 5px 0;">Billing to your card ending in:</h4>
                                        <h2 style="color: #565d65;font-family: 'Open Sans', sans-serif; font-weight: 600; margin: 0;font-size: 21px;">PayPal account for <br><a style="color: #5194ed;" href="mailto:info@offplanpropertyexchange.com" title="info@offplanpropertyexchange.com">info@offplanpropertyexchange.com</a></h2>
                                    </td>
                                    <td valign="top">
                                        <h4 style="color: #565d65; font-size: 16px;font-family: 'Open Sans', sans-serif; font-weight: 600; margin:25px 0 5px 0;">Billing Date:</h4>
                                        <h2 style="color: #565d65;font-family: 'Open Sans', sans-serif; font-weight: 600; margin: 0; font-size: 21px;">Aug 28, 2018 at <br>4:30 PM BST</h2>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div style="height: 16px;"></div>
                    <div style="padding: 30px; background: #f2f8fc; border: 1px solid #c3d8e6;border-radius: 15px; -moz-border-radius: 15px; -webkit-border-radius: 15px;">
                        <h2 style="margin:0;color: #e2872b;font-size: 25px;font-family: 'Roboto', sans-serif; padding:0 0 15px 12px; border-bottom: 2px solid #edf0f3; font-weight: 500;">About your account</h2>
                        <div style="padding:15px 13px;">
                            <table>
                                <tr>
                                    <td width="470" valign="top">
                                        <h4 style="color: #565d65; font-size: 16px;font-family: 'Open Sans', sans-serif; font-weight: 600; margin:0 0 5px 0;">Account Owner:</h4>
                                        <h2 style="color: #565d65;font-family: 'Open Sans', sans-serif; font-weight: 600; margin: 0;font-size: 21px;">Stuart Atkinson <br><a style="color: #5194ed;" href="mailto:moola@moolastudentbanking.com" title="moola@moolastudentbanking.com">moola@moolastudentbanking.com</a></h2>
                                    </td>
                                    <td  width="" valign="top">
                                        <h4 style="color: #565d65; font-size: 16px;font-family: 'Open Sans', sans-serif; font-weight: 600; margin:0 0 5px 0;">Account name:</h4>
                                        <h2 style="color: #565d65;font-family: 'Open Sans', sans-serif; font-weight: 600; margin: 0;font-size: 21px;">Stuart Atkinson’s Account</h2>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>

                    <div style="padding: 0 30px;">
                        <div style="padding:15px;">
                            <p style="color: #565d65; font-size: 14px;font-family: 'Open Sans', sans-serif; line-height: 24px;">Live Plan is a subscription-based online business planning and tracking service. If you don’t remember signing up, visit oue website at <a style="color: #5194ed;" href="www.liveplan.com" target="_blank" title="www.liveplan.com">www.liveplan.com</a> to check us out again. If you want to learn how to cancel your LivePlan account, <a href="#" target="_blank" style="color: #5194ed;" title="just click here">just click here.</a></p>
                            <h3 style="color: #38404b;font-family: 'Open Sans', sans-serif; margin:15px 0; font-weight: 600; font-size: 19px;">How likely is that you would recommend LivePlan to a friend or colleague?</h3>
                            <h4 style="color: #72777d; margin: 0; font-size: 17px;font-family: 'Open Sans', sans-serif; font-weight: 600;">(0 being not at all, 10 being extremely likely):</h4>
                        </div>
                        <div style="padding: 45px 0; text-align: center;">
                            <img src="<?php echo SITE_LINK; ?>img/rate.png" alt="" title="" style="border: none; outline: none;">
                        </div>
                        <div style="padding:15px;">
                            <h3 style="color: #38404b;font-family: 'Open Sans', sans-serif; margin:0 0 15px; font-weight: 600; font-size: 19px;">Have a question?</h3>
                            <p style="color: #565d65; font-size: 14px;font-family: 'Open Sans', sans-serif; line-height: 24px;">We’d love to hear from you, even if it’s just to say hi. To learn more about this charge or your account, please contact our customer service team.<br>Our customer service team is available weekdays from 8 to 5 Pacific Time. </p>
                            <h3 style="color: #38404b;font-family: 'Open Sans', sans-serif; margin:45px 0 30px 0; font-weight: 600; font-size: 19px;">To contact us:</h3>
                            <ul style="margin: 0; padding:0 0 40px 35px;">
                                <li style="background: url('<?php echo SITE_LINK; ?>img/arrow.png') no-repeat center left; list-style: none; color: #565d65; font-size: 15px; padding: 0 0 0 27px; margin: 0 0 10px 0;font-family: 'Open Sans', sans-serif;">You can reply to this message directlyc</li>
                                <li style="background: url('<?php echo SITE_LINK; ?>img/arrow.png') no-repeat center left; list-style: none; color: #565d65; font-size: 15px; padding: 0 0 0 27px;margin: 0 0 10px 0;font-family: 'Open Sans', sans-serif;"><a style="color: #5194ed;" href="#" title="Chat with us online">Chat with us online</a></li>
                                <li style="background: url('<?php echo SITE_LINK; ?>img/arrow.png') no-repeat center left; list-style: none; color: #565d65; font-size: 15px; padding: 0 0 0 27px;font-family: 'Open Sans', sans-serif;">Call us: 888-498-6138 or +1 541 683-6168 internationally.</li>
                            </ul>
                            <div style="color: #38404b; font-size: 17px;font-family: 'Open Sans', sans-serif; line-height: 24px; border-bottom: 1px solid #e6e9ea; padding-bottom:35px;">
                                Best regards, <br><span style="font-family: 'Open Sans', sans-serif; font-weight: 600;">Team Fishook Regards</span>
                            </div>
                            <div style="color: #565d65; font-size: 15px; line-height: 24px;font-family: 'Open Sans', sans-serif; padding-top: 30px; text-align: center;">
                                LivePlan is a service of Palo Alto Software, 44 W. Broadway, Suite 500, Eugene, OR 97401, USA<br>
Palo Alto Software is a non-EU provider with no VAT registration. VAT was not applied to this purchace.
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </td>
    </tr>
    
  </table>
</div>
</body>
</html>
