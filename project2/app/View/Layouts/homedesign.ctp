<!DOCTYPE html>
<html class="no-touch gecko firefox firefox66 firefox66_0 linux js no-hidpi datauri minw_1200 orientation_landscape" xmlns="http://www.w3.org/1999/xhtml" lang="en-US"><head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">

<title>Rave – Know's what's happening.</title>
<link rel="dns-prefetch" href="https://s.w.org/">

<link rel="shortcut icon" href="<?php echo SITE_LINK?>favicon.ico" type="image/x-icon">
<link rel="icon" href="<?php echo SITE_LINK?>favicon.ico" type="image/x-icon">
<link rel='stylesheet' id='uncode-style-css'  href='<?php echo SITE_LINK?>css/rave.css' type='text/css' media='all' />
<link rel='stylesheet' id='uncode-style-css'  href='<?php echo SITE_LINK?>css/vendor.css' type='text/css' media='all' />


<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


<style type="text/css" media="print">@page { size: 602px 1024px; margin: 0; }</style></head>
<body>
<header>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="logo-brand">
					<a href="http://www.theraveapp.co.uk/" class="navbar-brand" data-padding-shrink="0" data-minheight="20"><div class="logo-image logo-light" data-maxheight="60" style="height: 60px;display:none;"><img src="<?php SITE_LINK?>/img/rave.png" alt="logo" class="img-responsive" width="620" height="200"></div><div class="logo-image logo-dark" data-maxheight="60" style="height: 60px;"><img src="<?php echo SITE_LINK?>/img/rave.png" alt="logo" class="img-responsive" width="620" height="200"></div></a>
				</div>
			</div>
		</div>
	</div>
</header>
<div>
		<!-- Register-->
			<div id="regModel" class="modal1  loginModel themeModel1" <?php echo isset($token) ? "style='display:none'":"style='display:inline'" ?>>		
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<p class="modal-title">Start Uploading Events For Free</p>
							</div>
							<div class="modal-body">
								<div class="fb-login" onclick="fblogin()">
									<a href="javascript:void(0);"><i class="fa fa-facebook-f"></i> Connect With Facebook</a>
								</div>
								<span class="or-divider">OR</span>
								<div id="error_msg_signup" class="alert alert-warning" style="display:none"></div><div class="alert alert-success" style="display:none" id="success_msg_signup"></div>
								<div class="alert alert-danger" id="error_msg_captcha_signup" style="display:none;"><strong>Attention!</strong> Please check captcha to proceed.</div>
								<?php echo $this->Form->create("User",array("url"=>"/signup","class"=>"themeForm","id"=>"UserSignupForm","novalidate"=>"true")); ?>
									<div class="form-group">
									   <div class="inputIcon-grp">
										 <span class="inpIcon"><?php echo $this->Html->image("user-icon.png"); ?></span>
										 <?php echo $this->Form->input("UserDetail.name",array("type"=>"text","maxlength"=>100,"class"=>"inputText form-control","div"=>false,"label"=>false,"placeholder"=>"Business Name")); ?>							 
										</div>
									</div>
									<div class="form-group">
									   <div class="inputIcon-grp">
										 <span class="inpIcon"><?php echo $this->Html->image("env-icon.png"); ?></span>
										 <?php echo $this->Form->input("User.username",array("type"=>"text","maxlength"=>100,"class"=>"inputText form-control","div"=>false,"label"=>false,"placeholder"=>"Email")); ?>
										</div>
									</div>
								   <div class="form-group">
									   <div class="inputIcon-grp">
										 <span class="inpIcon"><?php echo $this->Html->image("pwd-icon.png"); ?></span>
										 <?php echo $this->Form->input("User.password",array("type"=>"password","maxlength"=>100,"class"=>"inputText form-control","div"=>false,"label"=>false,"placeholder"=>"Password")); ?>
										</div>
									</div> 
			<!--
									<div class="g-recaptcha_signup" data-sitekey=<?php //echo SITE_KEY; ?> id="RecaptchaField2"></div>
									<input type="hidden" class="hiddenRecaptcha" name="signup_hiddenRecaptcha" id="signup_hiddenRecaptcha">						
			-->
									<div class="form-submit text-right">
									   <span class="logSwitch">Already have an account? <a href="javascript:void(0);" id="loginPopup" >SIGN IN</a></span>
									   <button type="submit" class="btn btn-theme">SIGN UP</button>
									</div>
								<?php echo $this->Form->end(); ?>
							</div>                   
						</div>
					</div>
				</div>
			<!-- Register-->
			<!-- Login-->
				<div id="loginModel" class="modal1 loginModel themeModel1" style="display:none">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<p class="modal-title">Start Uploading Events For Free</p>
							</div>
							
							<div class="modal-body">
								<div class="fb-login" onclick="fblogin()">
									<a href="javascript:void(0);"><i class="fa fa-facebook-f"></i> Connect With Facebook</a>
								</div>
								<span class="or-divider">OR</span>
								<div class="alert alert-warning" id="error_msg" style="display:none;"></div>
								<div class="alert alert-danger" id="error_msg_captcha" style="display:none;"><strong>Attention!</strong> Please check captcha to proceed.</div>
								
								<?php echo $this->Form->create("User",array("url"=>"/login","class"=>"themeForm","id"=>"UserLoginForm","novalidate"=>"true")); ?>
								
									<div class="form-group">
										<div class="inputIcon-grp">
											<span class="inpIcon"><?php echo $this->Html->image("env-icon.png"); ?></span>
											<?php echo $this->Form->input("username",array("type"=>"text","maxlength"=>100,"class"=>" inputText form-control","div"=>false,"label"=>false,"placeholder"=>"Email","id"=>"loginusername")); ?>
										</div>
									</div>
									<div class="form-group">
									   <div class="inputIcon-grp">
											<span class="inpIcon"><?php echo $this->Html->image("pwd-icon.png"); ?></span>
											<?php echo $this->Form->input("password",array("type"=>"password","maxlength"=>100,"class"=>"inputText form-control","div"=>false,"label"=>false,"placeholder"=>"Password","id"=>"loginpassword")); ?>
										</div>
									</div>
			<!--
									<div class="g-recaptcha-login" data-sitekey="<?php //echo SITE_KEY; ?>" id="RecaptchaField1"></div>
			-->
			<!--
									<input type="hidden" class="hiddenRecaptcha" name="login_hiddenRecaptcha" value="" id="login_hiddenRecaptcha">						
			-->
									<div class="form-submit text-right">
									<div class="forg-pwd"><a href="javascript:void(0);" id="forgetpopup">Forgot Password?</a></div>   
									   <span class="logSwitch">Don’t have an account? <a href="javascript:void(0);" id="signupPopup">SIGN UP</a></span>
									   <button type="submit" class="btn btn-theme">SIGN IN</button>
									</div>
								<?php echo $this->Form->end(); ?>
							</div>                   
						</div>
					</div>
				</div>
				
				<!-- Login-->
					
				<!-- Forget Passowrd HTML -->
					<div id="forgetModel" class="modal1 loginModel themeModel1" style="display:none">
						
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<p class="modal-title">Forgot Password<br>
									<span class="text_small">Enter your email below to receive your password reset instructions</span></p>
								</div>
								<div class="modal-body">					
									<div class="alert alert-warning" style="display:none" id="error_msg_forget"></div><div style="display:none" class="alert alert-success" id="success_msg_forget"></div>
									<div class="alert alert-danger" id="error_msg_captcha_forget" style="display:none;"><strong>Attention!</strong> Please check captcha to proceed.</div>
									<?php echo $this->Form->create("User",array("url"=>"/forgot_password","class"=>"themeForm","id"=>"UserForgotPasswordForm","novalidate"=>"true")); ?>
										<div class="form-group">
										   <div class="inputIcon-grp">
											 <span class="inpIcon"><?php echo $this->Html->image("env-icon.png"); ?></span>
											<?php echo $this->Form->input("email",array("type"=>"text","maxlength"=>200,"class"=>"inputText form-control","div"=>false,"label"=>false,'placeholder'=>"Enter Email")); ?>					 
											</div>
										</div>		
				<!--
										<div class="g-recaptcha_forgetpass" data-sitekey=<?php //echo SITE_KEY; ?> id="RecaptchaField3"></div>
										<input type="hidden" class="hiddenRecaptcha" name="forgetpass_hiddenRecaptcha" id="forgetpass_hiddenRecaptcha">			
				-->
										<div class="form-submit text-right">
										   <button type="submit" class="btn btn-theme">Send Password</button>
										</div>
									<?php echo $this->Form->end(); ?>
								</div>                   
							</div>
						</div>
				</div>
				<?php if(isset($token)) { ?>
				<!--Reset Passowrd HTML -->
					<div id="resetModel" class="modal1 loginModel themeModel1" >
						
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<h4 class="modal-title">Reset Password</h4>				
								</div>
								<div class="modal-body">					
									<div class="alert alert-warning" style="display:none" id="error_msg_reset"></div><div class="alert alert-success" style="display:none" id="success_msg_reset"></div>
									<div class="alert alert-danger" id="error_msg_captcha_reset" style="display:none;"><strong>Attention!</strong> Please check captcha to proceed.</div>
									<?php echo $this->Form->create("User",array("url"=>"/process_reset_password","class"=>"themeForm","id"=>"UserResetPasswordForm","novalidate"=>"true")); ?>
										<div class="form-group">
										   <div class="inputIcon-grp">
											   <?php echo $this->Form->hidden("User.token",array("value"=>$token)); ?>	 
											 <span class="inpIcon"><?php echo $this->Html->image("pwd-icon.png"); ?></span>
											 <?php echo $this->Form->input("User.newpassword",array("type"=>"password","maxlength"=>50,"class"=>"inputText form-control","div"=>false,"label"=>false,'placeholder'=>"New Password")); ?> 
											</div>
										</div>	
										  <div class="form-group">
										   <div class="inputIcon-grp">
											 <span class="inpIcon"><?php echo $this->Html->image("pwd-icon.png"); ?></span>
											 <?php echo $this->Form->input("User.confirm_password",array("type"=>"password","maxlength"=>50,"class"=>"inputText form-control","div"=>false,"label"=>false,"placeholder"=>"Confirm Password")); ?>
											</div>
										</div> 	
														
										<div class="form-submit text-right">
										   <button type="submit" class="btn btn-theme">Set Password</button>
										</div>
									<?php echo $this->Form->end(); ?>
								</div>                   
							</div>
						</div>
				</div>
				<?php } ?>
</div>		
<footer>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<ul class="footer-nav">
					<li>
						<a href="http://www.theraveapp.co.uk/index.html">HOME</a>
					</li>
					<li>
						<a href="http://www.theraveapp.co.uk/about.html">ABOUT</a>
					</li>
				</ul>
			</div>
		</div>
		<div class="row align-items-lg-end">
			<div class="col-lg-3 order-lg-3">
				<ul class="social-list justify-content-center justify-content-lg-start">
					<li><a href="https://www.twitter.com/theraveapp_" class="ico ico-twitter"></a></li>
					<li><a href="https://www.facebook.com/theraveapp" class="ico ico-facebook"></a></li>
					<li><a href="https://www.instagram.com/theraveapp/" class="ico ico-insta"></a></li>
				</ul>
				<p class="d-none d-lg-block mt-3 mt-lg-0"><a href="https://www.moolarewards.co.uk" target="_blank">powered by <br> <img src="img/logo-moola.png" alt="moola logo"></a></p>
			</div>
			<div class="col-lg-5 text-center text-lg-left order-lg-1">
				<p><a href="" data-toggle="modal" data-target="#modal-contact">CONTACT US</a></p>
				<p><a href="mailto:tom@moolarewards.co.uk?Subject=Hello%20Rave" target="_top">tom@moolarewards.co.uk</a></p>
				<p class="mb-3"><a href="mailto:stuart@moolarewards.co.uk?Subject=Hello%20Rave" target="_top">stuart@moolarewards.co.uk</a></p>
				<p><a href="http://events.theraveapp.co.uk">Add Event</a></p>
			</div>
			<div class="col-lg-4 text-center mt-5 order-lg-2 text-lg-left">
				<p><a href="" data-toggle="modal" data-target="#modal-terms">Terms of Service</a></p>
				<p>© RAVE 2019, all rights reserved</p>
				<p class="mt-3 d-md-none"><a href="https://www.moolarewards.co.uk" target="_blank">powered by <img src="images/logo-moola.png" alt="moola logo"></a></p>
			</div>
		</div>

	</div>	
</footer>
<div class="modal fade" id="modal-terms" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="display: none;" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
         <img src="img/ico-close.svg" alt="">
      </button>
      <div class="modal-header">
        <h5 class="modal-title">Terms of Service</h5>
      </div>
      <div class="modal-body">
        <ul class="nav nav-tabs" id="myTab" role="tablist">
          <li class="nav-item">
            <a class="nav-link active" id="terms-tab" data-toggle="tab" href="#terms" role="tab" aria-controls="terms" aria-selected="true">Terms</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" id="privacy-tab" data-toggle="tab" href="#privacy" role="tab" aria-controls="privacy" aria-selected="false">Privacy</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" id="contact-tab" data-toggle="tab" href="#cookie" role="tab" aria-controls="cookie" aria-selected="false">Cookie</a>
          </li>
        </ul>
        <div class="tab-content" id="myTabContent">
          <div class="tab-pane fade show active" id="terms" role="tabpanel" aria-labelledby="terms-tab">
            <h4>Overview</h4>
            <p>Welcome to our website (the “Site”). If you continue to browse and use the Site you are agreeing to comply with and be bound by the following terms and conditions of use, which together with our Privacy and Cookie Policy and Copyright Notice govern Moola Rewards relationship with you in relation to the Site (the “Terms”).</p>
            <p>You are also responsible for ensuring that all persons who access the Site through your internet connection are aware of these Terms, along with other applicable terms and conditions, and that they comply with them. We recommend that you print a copy of these Terms for future reference.</p>

            <h4>Company information</h4>
            <p>Moola Rewards and the Site are operated by Moola SB Ltd. We registered in England and Wales under company number 10975164 and have our registered office at the Northern Assurance Building, Albert Square, 9 - 21 Princess Street, Manchester, England, M2 4DN. <br>
            “moola rewards” is a registered trademark.</p>
            <p>If you have any questions or comments about these Terms please contact us via the following email address <a href="help@moolarewards.co.uk">help@moolarewards.co.uk</a>.</p>

            <h4>Accessing the Site</h4>
            <p>The use of the Site is subject to the following terms of use:</p>

            <ol class="line-left">
              <li>You are 16 years of age or older;</li>
              <li>You do not use the Site in any way that is unlawful or fraudulent, or has any unlawful or fraudulent purpose or effect;</li>
              <li>You do not use the Site to transmit, or procure the sending of, any unsolicited or unauthorised advertising or promotional material or any other form of similar solicitation (spam);</li>
              <li>You do not use the Site to knowingly transmit any data, send or upload any material that contains viruses, Trojan horses, worms, time-bombs, keystroke loggers, spyware, adware or any other harmful programs or similar computer code designed to adversely affect the operation of any computer software or hardware;</li>
              <li>You do not engage in any defamatory, improper, indecent or offensive behaviour;</li>
              <li>You are not breaking any local, state, national or international law in your relevant jurisdiction by accessing the Site;</li>
              <li>You will treat the Site and its users with respect and will not partake in any conduct that could be considered bullying, harassment or insulting (as determined by us);</li>
              <li>You must provide us with accurate personal and contact information. You will only represent yourself and will not create false aliases or impersonate any other person (with or without their consent) while using the Site; and</li>
              <li>You do not breach any of the Terms.</li>
            </ol>
            <p>You also agree not to access without authority, interfere with, damage or disrupt:</p>
            <ol class="line-left">
              <li>Any part of the Site;</li>
              <li>Any equipment or network on which the Site is stored;</li>
              <li>Any software used in the provision of the Site; or</li>
              <li>Any equipment or network or software owned or used by any third party.</li>
            </ol>
            <p>The content of the Site is for your general information and use only and it is not intended to amount to advice on which you should rely. You must obtain professional or specialist advice before taking, or refraining from, any action on the basis of the content on the Site.</p>
            <p>Although we make reasonable efforts to update the information on the Site, we make no representations, warranties or guarantees, whether express or implied, that the content on the Site is accurate, complete or up to date.</p>
            <p>Where the Site contains information uploaded by other users of the Site this information has not been verified or approved by us. The views expressed by other users on the Site do not represent our views or values.</p>
            <p>The Site is subject to change without notice. Every time you wish to use the Site please check these Terms to ensure you understand the terms that apply at that time. These Terms were most recently updated on August 24th 2018.</p>
            <p>Access to the Site and any related application is permitted on a temporary basis, and we reserve the right to withdraw or amend the Site and the service we provide without notice. We will not be liable if for any reason the Site is unavailable at any time or for any period.</p>
            <p>We do not guarantee that the Website will be secure or free from bugs or viruses.</p>
            <p>You are responsible for configuring your information technology, computer programmes and platform to access the Site. You should use your own virus protection software.</p>
            <p>You must not misuse the Site by knowingly introducing viruses, trojans, worms, logic bombs or other material that is malicious or technologically harmful. You must not attempt to gain unauthorised access to the Site, the server on which the Site is stored or any server, computer or database connected to the Site. You must not attack the Site via a denial-of-service attack or a distributed denial-of service attack. By breaching this provision, you would commit a criminal offence under the Computer Misuse Act 1990. We will report any such breach to the relevant law enforcement authorities and we will co-operate with those authorities by disclosing your identity to them. In the event of such a breach, your right to use the Site will cease immediately.</p>
            <p>If you choose, or you are provided with, a user identification code, password or any other piece of information as part of our security procedures, you must treat such information as confidential, and you must not disclose it to any third party. We have the right to disable any user identification code or password, whether chosen by you or allocated by us, at any time, if in our opinion you have failed to comply with any of the provisions of these Terms.</p>
            <p>By obtaining a promotional code or accessing an offer or discount through Moola Rewards, you acknowledge that such offer or discount is subject to third party terms and conditions and it is your responsibility to review such terms and conditions before entering into any transaction with that third party.</p>
            <p>Moola Rewards is not intended for individuals under the age of 16 years old. If you are under 16 years of age, you are not permitted to use the Site or the services provided by Moola Rewards and your registration may be revoked. By using the service, you confirm that you are at least 16 years of age.</p>
            <p>We are the owner or the licensee of all intellectual property rights in the Site, its content and in the material published on it. Those works are protected by copyright laws and treaties around the world. All such rights are reserved.This material includes, but is not limited to, the design, layout, look, appearance and graphics of the Site. Reproduction is prohibited other than in accordance with the Copyright Notice, which forms part of these Terms.</p>
            <p>All trademarks reproduced in the Site which are not the property of, or licensed to the operator, are acknowledged on the Site.</p>
            <h4>Our liability</h4>
            <p>Moola Rewards is committed to securing great student discounts. We work directly with retailers to supply students with valid discount codes. The student discounts displayed on the Site are provided without any guarantees, conditions or warranties to their accuracy. To the extent permitted by law, we, and any third parties connected to us hereby expressly exclude:</p>
            <ol class="line-left">
              <li>All conditions, warranties and other terms which might otherwise be implied by statute, common law or the law of equity.</li>
              <li>Any liability for any direct, indirect or consequential loss or damage incurred by any user in connection with the Site or in connection with the use, inability to use, or results of the use of the Site, any websites linked to it and any materials posted on it, including, without limitation any liability for:</li>
              <li>Loss of income or revenue;</li>
              <li>Loss of business;</li>
              <li>Loss of profits or contracts;</li>
              <li>Loss of anticipated savings;</li>
              <li>Loss of data;</li>
              <li>Loss of goodwill; and</li>
              <li>Wasted management or office time; and</li>
              <li>For any other loss or damage of any kind, however arising and whether caused by tort (including negligence), breach of contract or otherwise, even if foreseeable.</li>
            </ol>
            <p>This does not affect our liability for death or personal injury arising from our negligence, nor our liability for fraudulent misrepresentation or misrepresentation as to a fundamental matter, nor any other liability which cannot be excluded or limited under applicable law.</p>
            <p>We regularly audit our discount codes. However discounts may run out, or stop working. Moola Rewards will contact the retailer to request new discount vouchers as soon as an issue emerges. Moola Rewards is not responsible for the validity of vouchers, and if there is an issue we cannot be held liable for any losses incurred.</p>
            <p>Student discounts are usually only valid on full priced items and will often exclude sale items and other limitations set by the retailer. If a voucher is not working please check the retailer’s full terms and conditions.</p>
            <p>Our discounts may link to retailer’s websites. These links are provided for your convenience to provide further information. We have no control over the contents of those websites or resources, and accept no responsibility for them or for any loss or damage that may arise from your use of them.</p>

            <h4>Moola Rewards content</h4>
            <p>Moola Rewards is committed to promoting the most up-to-date and unique content possible while maintaining standards consistent with the Internet community and the societies of the world. The Moola Rewards site is self service for merchants and is published instantly without the need for review. We actively audit content that goes live on the Site, however there may be instances where an individual deems a piece of our content to be offensive or inappropriate. In this case, please notify us immediately by email [<a href="help@moolarewards.co.uk">help@moolarewards.co.uk</a>]. Accordingly, Moola Rewards will NOT process, digitize, post, upload, store, share or otherwise display on the Site:</p>
            
            <ol class="line-left">
              <li>Content or material that we believe is pornographic, obscene, or of an adult or sexual nature;</li>
              <li>Content or material that we believe is offensive, obscene or inflammatory, including but not limited to, expressions of bigotry, prejudice, racism, hatred and profanity;</li>
              <li>Content or material that exploits children under the age of 18 or posts or discloses any personally identifying information about children under the age of 18;</li>
              <li>Content or material promoting or providing instructional information about illegal activities, promoting harm or injury to any group, individual or cruelty to animals including, but not limited to: Instructions on how to assemble or use bombs, grenades or other weapons; and Disseminating personal information about another individual for malevolent purposes, including libel, slander, defamation or violation of an individual's right to privacy;</li>
              <li>Content or material that is defamatory of any person;</li>
              <li>Content or material that promotes violence;</li>
              <li>Content or material that promotes discrimination based on race, sex, religion, nationality, disability, sexual orientation or age;</li>
              <li>Content or material that is likely to deceive any person;</li>
              <li>Content or material that we that breaches any legal duty owed to a third party, such as a contractual duty or a duty of confidence;</li>
              <li>Content or material that is in contempt of court;</li>
              <li>Content or material that we believe likely to harass, upset, embarrass, alarm or annoy any other person;</li>
              <li>Content or material that gives the impression that the content emanates from us, if this is not the case;</li>
              <li>Copyrighted content or material that is used without the express permission of the owner; photographs and other images published in books, magazines, posters, calendars, websites and other media are generally protected by copyright law;</li>
              <li>Content or material that infringes or encroaches on the rights of others, including, but not limited to, infringement of privacy and publicity rights and harm to reputation; or</li>
              <li>Any links to the above.</li>
            </ol>
            
            <p>You should be aware that in visiting the Site, you may be exposed to materials you consider to be offensive and you assume responsibility for your exposure. As stated above, if you run across something you deem to be offensive, please email [<a href="help@moolarewards.co.uk">help@moolarewards.co.uk</a>] to notify us.</p>

            <h4>Transfer of your Data to Third Parties</h4>
            <p>We use your data to improve your experience. Our service means that we keep your data inside Moola Rewards unless we are required by law to disclose your data or you ask us or give us your permission to share it, for example:</p>

            <ol class="line-left">
              <li>You ask us to or give us your permission to;</li>
              <li>To provide you with a discount or access to a partner’s platform; or</li>
              <li>If you agree to marketing emails and personalisation, to show you relevant messaging on your social media platforms, to let you know about Moola Rewards discounts we think you’ll enjoy.</li>
            </ol>
            
            <p>You accept our Privacy and Cookie Policy and agree that you will not do anything that shall compromise our compliance with the Privacy and Cookie Policy nor do anything contrary to the Privacy and Cookie Policy insofar as your use of the Site is concerned.</p>

            <p>We will only use your personal data as set out in our Privacy and Cookie Policy.</p>

            <p>We may amend the Privacy and Cookie Policy without notice and at our absolute discretion, and by continuing to use the Site you accept such changes.</p>


          </div>
          <div class="tab-pane fade" id="privacy" role="tabpanel" aria-labelledby="privacy-tab">
            <p>This privacy and cookie policy (the “Policy”) describes the personal data collected or generated (“processed”) when you use <a href="">www.moolarewards.co.uk</a> (the “Site”).</p>
            <p>This Policy describes the types of personal data collected and how your personal data is used, shared and protected. It also explains the choices you have relating to your personal data and how you can contact us.</p>
            <p>It is important that you read this Policy together with any other privacy notice or fair processing notice that we may provide at or around the time that we collect or process personal data about you so that you are fully aware of how and why we are using that data. This Policy supplements the other notices and is not intended to override or replace them.</p>
            <p>Please note that the Site is not intended for children (those aged 13 and under) and we do not knowingly collect data relating to children.</p>
            <p>For ease of reading, we have divided this Policy into several sections:</p>
            <ol class="line-left">
              <li>Who we are</li>
              <li>What information will Moola Rewards collect about me and how is it collected</li>
              <li>How will Moola Rewards use the information it collects about me?</li>
              <li>Why is Moola Rewards allowed to use my information in this way?</li>
              <li>How long will you keep my personal data?</li>
              <li>Do you share my data with any other organisations?</li>
              <li>Your rights</li>
              <li>When will Moola Rewards contact me?</li>
              <li>Web browser cookies</li>
              <li>Apps and Devices</li>
              <li>Contact Details</li>
              <li>How to complain</li>
            </ol>
            
            <p>This Policy was last updated on August 24th 2018</p>

            <h4>Who we are</h4>
            <p>Moola Rewards and the Site are operated by Moola SB Ltd. We registered in England and Wales under company number 10975164 and have our registered office at the Northern Assurance Building, Albert Square, 9 - 21 Princess Street, Manchester, England, M2 4DN. <br>
            We are a student discount platform. We offer student discounts and student deals in-store. <br>
            Moola Rewards discounts are available to students through the mobile app. <br>
            The controller responsible for your personal data is Moola SB Ltd.</p>
    
            <h4>What information will Moola Rewards collect about me and how is it collected?</h4>
            <p>When you sign up to Moola Rewards we collect personal data about you so that we can provide you with the services offered on the Site.</p>

            <p>You provide some data yourself and this may include, for instance, when registering for a Moola Rewards account:</p>
            
            <ol class="line-left">
              <li>Your email address;</li>
              <li>Your date and year of birth;</li>
            </ol>

            <p>Some personal data is collected automatically by looking at information from your browser. This is information such as:</p>
            
            <ol class="line-left">
              <li>Your IP address;</li>
              <li>Which browser you are using;</li>
              <li>Which device you’re using;</li>
              <li>Your unique identifier; and</li>
              <li>Which webpage directed you to the Site.</li>
            </ol>
            <p>If you’re signed in we’ll also know how you use Moola Rewards. This helps us give you a better, more personalised service.</p>

            <h4>How will Moola Rewards use the information it collects about me?</h4>

            <p>We use your data to make the Site and Moola Rewards better for you and everyone. Your data helps us to operate, improve and maintain our business whilst at the same time enhances your user experience. We also use your data for business, regulatory and legal purposes, like:</p>

             <ol class="line-left">
               <li>Verifying your student status;</li>
               <li>Getting in touch if we need to tell you about something, like a change to our policies or issues with a service; and</li>
               <li>Contacting you to re-verify your student status so you can keep receiving discounts.</li>
             </ol>

             <h4>Why is Moola Rewards allowed to use my information in this way?</h4>

             <p>Moola rewards will only use your personal data in the following circumstances:</p>

             <ol class="line-left">
               <li>Where you have asked us to do so, or consented to us doing so.</li>
               <li>Where we need to do so in order to perform a contract we have entered into with you.</li>
               <li>Where it is necessary for our legitimate interests (or those of a third party) and your fundamental rights do not override those interests.</li>
               <li>Where we need to comply with a legal or regulatory obligation.</li>
             </ol>

             <h4>How long will you keep my personal data?</h4>
             <p>Except if required otherwise by law, we retain your personal data for as long as necessary to fulfil the purposes for which we collect it and also where we have a legitimate interest in doing so, such as:</p>
             <p>o enable us to respond effectively to grievances that may arise after you cease to engage with us; or</p>
             <p>You can ask us to access, rectify or, in some circumstances, delete your personal data: see ‘Your Rights’ below for further information.</p>
             <p>We anonymise your data (so that it can no longer be associated with you) for research or statistical purposes in which case we may use this information indefinitely without further notice to you.</p>
             <p>If you have not used your Moola Rewards account in the last year then your account may be classed as dormant or may be deleted in line with this Policy. Please check your inbox to see if we've sent you any emails about this recently.</p>

             <h4>Do you share my data with any other organisations?</h4>
             <p>We use your data to improve your experience. Our service means that we keep your anonymised data inside Moola Rewards unless we are required by law to disclose your data or you ask us or give us your permission to share it, for example:</p>

             <ol class="line-left">
               <li>You ask us to or give us your permission to;</li>
               <li>To provide you with a discount or access to a partner’s platform; or</li>
               <li>If you agree to marketing emails and personalisation, to show you relevant messaging on your social media platforms, to let you know about Moola Rewards discounts we think you’ll enjoy.</li>
             </ol>

             <p>We may share your personal data with third party service providers who provide us with a variety of administrative, statistical, and technical services. For example to manage and service our data, distribute emails, research and analysis, manage brand and product promotions as well as administering certain services and features.</p>

             <p>We will only provide service providers with the minimum amount of personal data they need to fulfil the services we request, and we stipulate that they protect this information and do not use it for any other purpose. We take these relationships seriously and oblige all of our data processors to sign contracts with us that clearly set out their commitment to respecting individual rights, and their commitments to assisting us to help you exercise your rights as a data subject.</p>

             <p>Please note that some of our service providers may be based outside of the European Economic Area (the “EEA”). Where we transfer your data to a service provider that is outside of the EEA we seek to ensure that appropriate safeguards are in place to make sure that your personal data is held securely and that your rights as a data subject are upheld.</p>

             <p>Please contact us if you want further information in relation to the transfer of your personal data out of the EEA.</p>

             <p>When we share your personal data outside Moola Rewards we will:</p>

              <ol class="line-left">
                <li>Always share it in a secure way;</li>
                <li>Make sure it’s treated consistently with this Policy; and</li>
                <li>Not allow other companies to use it to contact you with their own marketing unless you give your permission.</li>
              </ol>

              <p>If you have registered for a Moola Rewards account this will also allow you to login to third party partner websites and apps. To provide you with a seamless experience, it may be necessary to share your personal data with third party partner websites you choose to use your Moola Rewards login to access. We will only share what we need to in order to provide the service you are using and we will never routinely share all of the data we hold about you.</p>

              <p>Your Moola Rewards account will always be covered by the policies of the Site.</p>

              <h4>Links to third party websites</h4>
              <p>Please note that where we provide links to third party websites, plug-ins and applications that are not affiliated with Moola Rewards such websites are out of our control and are not covered by this Policy. If you access third party websites using the links provided, the operators of these websites may collect information from you that could be used by them, in accordance with their own privacy policies. Please check these policies before you submit any personal data to these websites.</p>


              <h4>Your rights</h4><p>
              As a data subject you have a number of rights in relation to your personal data. Below, we have described the various rights that you have, as well as how you can exercise them.</p>


              <h4>Right of Access</h4><p>
              You may, at any time, request access to the personal data that we hold which relates to you (you may have heard of this right being described as a "subject access request").<br>
              Please note that this right entitles you to receive a copy of the personal data that we hold about you in order to enable you to check that it is correct and to ensure that we are processing that personal data lawfully. It is not a right that allows you to request personal data about other people, or a right to request specific documents from us that do not relate to your personal data.</p>

              <p>
              You can exercise this right at any time by writing to us using contact details set out here and telling us that you are making a subject access request. You do not have to fill in a specific form to make this kind of request.</p>


              <h4>Your Right to Rectification and Erasure</h4><p>
              You may, at any time, request that we correct personal data that we hold about you which you believe is incorrect or inaccurate. You may also ask us to erase personal data if you do not believe that we need to continue retaining it (you may have heard of this right described as the “right to be forgotten”).</p>

              <p>
              Please note that we may ask you to verify any new data that you provide to us and may take our own steps to check that the new data you have supplied us with is right. Further, we are not always obliged to erase personal data when asked to do so; if for any reason we believe that we have a good legal reason to continue processing personal data that you ask us to erase we will tell you what that reason is at the time we respond to your request.</p>

              <p>
              You can exercise this right at any time by writing to us using the contact details set out here and telling us that you are making a request to have your personal data rectified or erased and on what basis you are making that request. If you want us to replace inaccurate data with new data, you should tell us what that new data is. You do not have to fill in a specific form to make this kind of request.</p>


              <h4>Your Right to Restrict Processing</h4><p>
              Where we process your personal data on the basis of a legitimate interest (see the section of this Policy which explains why we use your personal data) you are entitled to ask us to stop processing it in that way if you feel that our continuing to do so impacts on your fundamental rights and freedoms or if you feel that those legitimate interests are not valid.</p>

              <p>
              You may also ask us to stop processing your personal data (a) if you dispute the accuracy of that personal data and want us verify that data's accuracy; (b) where it has been established that our use of the data is unlawful but you do not want us to erase it; (c) where we no longer need to process your personal data (and would otherwise dispose of it) but you wish for us to continue storing it in order to enable you to establish, exercise or defend legal claims.</p>

              <p>
              Please note that if for any reason we believe that we have a good legal reason to continue processing personal data that you ask us to stop processing, we will tell you what that reason is, either at the time we first respond to your request or after we have had the opportunity to consider and investigate it.</p>

              <p>
              You can exercise this right at any time by writing to us using the contact details set out here and telling us that you are making a request to have us stop processing the relevant aspect of your personal data and describing which of the above conditions you believe is relevant to that request. You do not have to fill in a specific form to make this kind of request.</p>


              <h4>Your Right to Portability</h4><p>
              Where you wish to transfer certain personal data that we hold about you, which is processed by automated means, to a third party you may write to us and ask us to provide it to you in a commonly used machine-readable format.</p>

              <p>
              Because of the kind of work that we do and the systems that we use, we do not envisage this right being particularly relevant to the majority of individuals with whom we interact. However, if you wish to transfer your data from us to a third party we are happy to consider such requests.</p>


              <h4>Your Right to stop receiving communications</h4><p>
              For details on your rights to ask us to stop sending you various kinds of communications, please contact us.</p>


              <h4>Your Right to object to automated decision making and profiling</h4><p>
              You have the right to be informed about the existence of any automated decision making and profiling of your personal data, and where appropriate, be provided with meaningful information about the logic involved, as well as the significance and the envisaged consequences of such processing that affects you.</p>


              <h4>Exercising your rights</h4><p>
              When you write to us making a request to exercise your rights we are entitled to ask you to prove that you are who you say you are. We will require you to provide copies of two of the following ID documents to help us to verify your identity:</p>


              <ol class="line-left">
                <li>Passport.</li>
                <li>Driving licence.</li>
                <li>Student photo ID card.</li>
              </ol>


              <p>It will help us to process your request if you clearly state which right you wish to exercise and, where relevant, why it is that you are exercising it. The clearer and more specific you can be the faster and more efficiently we can deal with your request. If you do not provide us with sufficient information then we may delay actioning your request until you have provided us with additional information (and where this is the case we will tell you).</p>


              <h4>When will Moola Rewards contact me?</h4>


              <ol class="line-left">
                <li>in relation to any service, activity or online content you have signed up for, in order to ensure that we can deliver the services, e.g. to verify your email when you sign up for a Moola Rewards account, or to help you reset your password or we need to verify your student status.</li>
                <li>in relation to any correspondence we receive from you or any comment or complaint you make about Moola Rewards products or services;</li>
                <li>in relation to any personalised services you are using;</li>
                <li>in relation to any contribution you have submitted to Moola Rewards, e.g. on the Moola Rewards social message boards or via text or voicemail message;</li>
                <li>to invite you to participate in surveys about Moola Rewards services (participation is always voluntary);</li>
                <li>to update you on any material changes to Moola Rewards’ policies and practices; and</li>
                <li>for marketing purposes, as set out below.</li>
              </ol>


              <p>We will never contact you to ask for your Moola Rewards account password, or other login information. Please be cautious if you receive any emails or calls from people asking for this information and claiming to be from Moola Rewards.</p>


              <h4>Will I be contacted for marketing purposes?</h4><p>
              Moola Rewards will only send you marketing emails or contact you on Moola Rewards platforms where you have agreed to this.</p>

              <p>
              We may offer regular emails, including a weekly update, to let you know about new discounts and services. From time to time we may also contact you to ask your views on issues affecting Moola Rewards.</p>

              <p>
              We may personalise the message content based upon any information you have provided to us and your use of the Site and apps.</p>

              <p>
              We may use information which we hold about you to show you relevant advertising on third party websites (e.g. Facebook, Google, Instagram, Snapchat and Twitter). This could involve showing you an advertising message where we know you have a Moola Rewards account and have used Moola Rewards discounts. If you don’t want to be shown targeted advertising messages from Moola Rewards, some third party websites allow you to request not to see messages from specific advertisers on that website in future.</p>


              <h4>Web browser cookies</h4>

              <p>1) What is a Cookie?<br>
              A cookie is a small amount of data, which often includes a unique identifier that is sent to your computer, tablet or mobile phone (all referred to here as a "device") web browser from a website's computer and is stored on your device's hard drive. Each website can send its own cookie to your web browser if your browser's preferences allow it. Many websites do this whenever a user visits their website in order to track online traffic flows. Similar technologies are also often used within emails to understand whether the email has been read or if any links have been clicked. If you continue without changing your settings, we’ll assume that you are happy to receive all cookies on the Site. However, you can change your cookie settings at any time.</p>

              <p>On the Site, cookies record information about your online preferences and allow us to tailor our websites to your interests.</p>

              <p>2) How does Moola Rewards use cookies?<br>
              Information supplied by cookies can help us to understand the profile of our visitors and help us to provide you with a better user experience. It also helps us recognise when you are signed in to your Moola Rewards account and to provide a more personalised experience. Moola Rewards uses this type of information to help improve the services it provides to its users.</p>

              <p>3) Other information collected from web browsers<br>
              Your web browser may also provide Moola Rewards with information about your device, such as an IP address and details about the browser that you are using. Where requesting local discounts, it may be possible for you to choose to provide Moola Rewards with access to your device’s location through the web-browser. We use information provided by your browser or by the link that you have clicked to understand the webpage that directed you to Moola Rewards and this may be captured by performance cookies.</p>

              <p>
              If you have any concerns about the way that we use cookies or in respect of your settings, then please contact us.</p>


              <h4>Apps and Devices</h4><p>
              When you download or use the Moola Rewards app on your mobile device, information may be accessed from or stored to your device. Most often this is used in a similar way to a web browser cookie, such as by enabling the app to ‘remember’ you or provide you with the content you have requested.</p>

              <p>Your web browser or device may also provide Moola Rewards with information about your device, such as a device identifier or IP address. Device identifiers may be collected automatically, such as the device ID, IP address, MAC address, IMEI number and app ID (a unique identifier relating to the particular copy of the app you are running).</p>

              <p>
              When you sign in to the Moola Rewards app, your sign-in details may be stored securely on the device you are using, so you can access on the same device without needing to enter your sign-in details again.</p>


              <h4>Contact Details</h4><p>
              If you any questions or comments about this Policy please contact The Data Privacy Manager by email,<a href="mailto:help@moolastudentbanking.co.uk"><span style="color:#f63"> help@moolastudentbanking.co.uk</span></a>.</p>


              <h4>How to complain</h4><p>
              We hope that we can resolve any query or concern you raise about our use of your data so please in the first instance get in touch via the contact details above.</p>

              <p>You do however have the right to make a complaint under the General Data Protection Regulation to a supervisory authority, in particular in the European Union (or European Economic Area) state where you work, normally live or where any alleged infringement of data protection laws occurred. The supervisory authority in the UK is the Information Commissioner who may be contacted at <a href="https://ico.org.uk/concerns/" target="_blank"><span style="color:#f63">https://ico.org.uk/concerns/ </span></a>or via telephone on 0303 123 1113.</p>
 
          </div>
          <div class="tab-pane fade" id="cookie" role="tabpanel" aria-labelledby="cookie-tab">
                <p>This privacy and cookie policy (the “Policy”) describes the personal data collected or generated (“processed”) when you use <a href="https://www.moolarewards.co.uk/"><span style="color:#f63">www.moolarewards.co.uk</span></a> (the “Site”).</p>


                <p>This Policy describes the types of personal data collected and how your personal data is used, shared and protected. It also explains the choices you have relating to your personal data and how you can contact us.</p>


                <p>It is important that you read this Policy together with any other privacy notice or fair processing notice that we may provide at or around the time that we collect or process personal data about you so that you are fully aware of how and why we are using that data. This Policy supplements the other notices and is not intended to override or replace them.</p>


                <p>Please note that the Site is not intended for children (those aged 13 and under) and we do not knowingly collect data relating to children.</p>


                <p>For ease of reading, we have divided this Policy into several sections:</p>


                <ol class="line-left">
                  <li>Who we are</li>
                  <li>What information will Moola Rewards collect about me and how is it collected</li>
                  <li>How will Moola Rewards use the information it collects about me?</li>
                  <li>Why is Moola Rewards allowed to use my information in this way?</li>
                  <li>How long will you keep my personal data?</li>
                  <li>Do you share my data with any other organisations?</li>
                  <li>Your rights</li>
                  <li>When will Moola Rewards contact me?</li>
                  <li>Web browser cookies</li>
                  <li>Apps and Devices</li>
                  <li>Contact Details</li>
                  <li>How to complain</li>
                </ol>


                <p>This Policy was last updated on August 24th 2018</p>


                <h4>Who we are</h4><p>
                Moola Rewards and the Site are operated by Moola SB Ltd. We registered in England and Wales under company number 10975164 and have our registered office at the Northern Assurance Building, Albert Square, 9 - 21 Princess Street, Manchester, England, M2 4DN.<br>
                We are a student discount platform. We offer student discounts and student deals in-store.<br>
                Moola Rewards discounts are available to students through the mobile app.<br>
                The controller responsible for your personal data is Moola SB Ltd.</p>


                <h4>What information will Moola Rewards collect about me and how is it collected?</h4><p>
                When you sign up to Moola Rewards we collect personal data about you so that we can provide you with the services offered on the Site.</p>

                <p>
                You provide some data yourself and this may include, for instance, when registering for a Moola Rewards account:</p>


                <ol class="line-left">
                  <li>Your email address;</li>
                  <li>Your date and year of birth;</li>
                </ol>


                <p>Some personal data is collected automatically by looking at information from your browser. This is information such as:</p>


                <ol class="line-left">
                  <li>Your IP address;</li>
                  <li>Which browser you are using;</li>
                  <li>Which device you’re using;</li>
                  <li>Your unique identifier; and</li>
                  <li>Which webpage directed you to the Site.</li>
                </ol>

                <p>If you’re signed in we’ll also know how you use Moola Rewards. This helps us give you a better, more personalised service.</p>


                <h4>How will Moola Rewards use the information it collects about me?</h4><p>
                We use your data to make the Site and Moola Rewards better for you and everyone. Your data helps us to operate, improve and maintain our business whilst at the same time enhances your user experience. We also use your data for business, regulatory and legal purposes, like:</p>


                <ol class="line-left">
                  <li>Verifying your student status;</li>
                  <li>Getting in touch if we need to tell you about something, like a change to our policies or issues with a service; and</li>
                  <li>Contacting you to re-verify your student status so you can keep receiving discounts.</li>
                </ol>


                <h4>Why is Moola Rewards allowed to use my information in this way?</h4><p>
                Moola rewards will only use your personal data in the following circumstances:</p>


                <ol class="line-left">
                  <li>Where you have asked us to do so, or consented to us doing so.</li>
                  <li>Where we need to do so in order to perform a contract we have entered into with you.</li>
                  <li>Where it is necessary for our legitimate interests (or those of a third party) and your fundamental rights do not override those interests.</li>
                  <li>Where we need to comply with a legal or regulatory obligation.</li>
                </ol>


                <h4>How long will you keep my personal data?</h4><p>
                Except if required otherwise by law, we retain your personal data for as long as necessary to fulfil the purposes for which we collect it and also where we have a legitimate interest in doing so, such as:</p>


                <ol class="line-left">
                  <li>To enable us to respond effectively to grievances that may arise after you cease to engage with us; or</li>
                </ol>


                <p>You can ask us to access, rectify or, in some circumstances, delete your personal data: see ‘Your Rights’ below for further information.</p>

                <p>
                We anonymise your data (so that it can no longer be associated with you) for research or statistical purposes in which case we may use this information indefinitely without further notice to you.</p>

                <p>
                If you have not used your Moola Rewards account in the last year then your account may be classed as dormant or may be deleted in line with this Policy. Please check your inbox to see if we've sent you any emails about this recently.</p>


                <h4>Do you share my data with any other organisations?</h4><p>
                We use your data to improve your experience. Our service means that we keep your anonymised data inside Moola Rewards unless we are required by law to disclose your data or you ask us or give us your permission to share it, for example:</p>


                <ol class="line-left">
                  <li>You ask us to or give us your permission to;</li>
                  <li>To provide you with a discount or access to a partner’s platform; or</li>
                  <li>If you agree to marketing emails and personalisation, to show you relevant messaging on your social media platforms, to let you know about Moola Rewards discounts we think you’ll enjoy.</li>
                </ol>


                <p>We may share your personal data with third party service providers who provide us with a variety of administrative, statistical, and technical services. For example to manage and service our data, distribute emails, research and analysis, manage brand and product promotions as well as administering certain services and features.</p>

                <p>
                We will only provide service providers with the minimum amount of personal data they need to fulfil the services we request, and we stipulate that they protect this information and do not use it for any other purpose. We take these relationships seriously and oblige all of our data processors to sign contracts with us that clearly set out their commitment to respecting individual rights, and their commitments to assisting us to help you exercise your rights as a data subject.</p>

                <p>
                Please note that some of our service providers may be based outside of the European Economic Area (the “EEA”). Where we transfer your data to a service provider that is outside of the EEA we seek to ensure that appropriate safeguards are in place to make sure that your personal data is held securely and that your rights as a data subject are upheld.</p>

                <p>
                Please contact us if you want further information in relation to the transfer of your personal data out of the EEA.</p>

                <p>
                When we share your personal data outside Moola Rewards we will:</p>


                <ol class="line-left">
                  <li>Always share it in a secure way;</li>
                  <li>Make sure it’s treated consistently with this Policy; and</li>
                  <li>Not allow other companies to use it to contact you with their own marketing unless you give your permission.</li>
                </ol>


                <p>If you have registered for a Moola Rewards account this will also allow you to login to third party partner websites and apps. To provide you with a seamless experience, it may be necessary to share your personal data with third party partner websites you choose to use your Moola Rewards login to access. We will only share what we need to in order to provide the service you are using and we will never routinely share all of the data we hold about you.</p>

                <p>
                Your Moola Rewards account will always be covered by the policies of the Site.</p>


                <h4>Links to third party websites</h4><p>
                Please note that where we provide links to third party websites, plug-ins and applications that are not affiliated with Moola Rewards such websites are out of our control and are not covered by this Policy. If you access third party websites using the links provided, the operators of these websites may collect information from you that could be used by them, in accordance with their own privacy policies. Please check these policies before you submit any personal data to these websites.</p>


                <h4>Your rights</h4><p>
                As a data subject you have a number of rights in relation to your personal data. Below, we have described the various rights that you have, as well as how you can exercise them.</p>


                <h4>Right of Access</h4><p>
                You may, at any time, request access to the personal data that we hold which relates to you (you may have heard of this right being described as a "subject access request").<br>
                Please note that this right entitles you to receive a copy of the personal data that we hold about you in order to enable you to check that it is correct and to ensure that we are processing that personal data lawfully. It is not a right that allows you to request personal data about other people, or a right to request specific documents from us that do not relate to your personal data.</p>

                <p>
                You can exercise this right at any time by writing to us using contact details set out here and telling us that you are making a subject access request. You do not have to fill in a specific form to make this kind of request.</p>


                <h4>Your Right to Rectification and Erasure</h4><p>
                You may, at any time, request that we correct personal data that we hold about you which you believe is incorrect or inaccurate. You may also ask us to erase personal data if you do not believe that we need to continue retaining it (you may have heard of this right described as the “right to be forgotten”).</p>

                <p>
                Please note that we may ask you to verify any new data that you provide to us and may take our own steps to check that the new data you have supplied us with is right. Further, we are not always obliged to erase personal data when asked to do so; if for any reason we believe that we have a good legal reason to continue processing personal data that you ask us to erase we will tell you what that reason is at the time we respond to your request.</p>

                <p>
                You can exercise this right at any time by writing to us using the contact details set out here and telling us that you are making a request to have your personal data rectified or erased and on what basis you are making that request. If you want us to replace inaccurate data with new data, you should tell us what that new data is. You do not have to fill in a specific form to make this kind of request.</p>


                <h4>Your Right to Restrict Processing</h4><p>
                Where we process your personal data on the basis of a legitimate interest (see the section of this Policy which explains why we use your personal data) you are entitled to ask us to stop processing it in that way if you feel that our continuing to do so impacts on your fundamental rights and freedoms or if you feel that those legitimate interests are not valid.</p>

                <p>
                You may also ask us to stop processing your personal data (a) if you dispute the accuracy of that personal data and want us verify that data's accuracy; (b) where it has been established that our use of the data is unlawful but you do not want us to erase it; (c) where we no longer need to process your personal data (and would otherwise dispose of it) but you wish for us to continue storing it in order to enable you to establish, exercise or defend legal claims.</p>

                <p>
                Please note that if for any reason we believe that we have a good legal reason to continue processing personal data that you ask us to stop processing, we will tell you what that reason is, either at the time we first respond to your request or after we have had the opportunity to consider and investigate it.</p>

                <p>
                You can exercise this right at any time by writing to us using the contact details set out here and telling us that you are making a request to have us stop processing the relevant aspect of your personal data and describing which of the above conditions you believe is relevant to that request. You do not have to fill in a specific form to make this kind of request.</p>


                <h4>Your Right to Portability</h4><p>
                Where you wish to transfer certain personal data that we hold about you, which is processed by automated means, to a third party you may write to us and ask us to provide it to you in a commonly used machine-readable format.</p>

                <p>
                Because of the kind of work that we do and the systems that we use, we do not envisage this right being particularly relevant to the majority of individuals with whom we interact. However, if you wish to transfer your data from us to a third party we are happy to consider such requests.</p>


                <h4>Your Right to stop receiving communications</h4><p>
                For details on your rights to ask us to stop sending you various kinds of communications, please contact us.</p>


                <h4>Your Right to object to automated decision making and profiling</h4><p>
                You have the right to be informed about the existence of any automated decision making and profiling of your personal data, and where appropriate, be provided with meaningful information about the logic involved, as well as the significance and the envisaged consequences of such processing that affects you.</p>


                <h4>Exercising your rights</h4><p>
                When you write to us making a request to exercise your rights we are entitled to ask you to prove that you are who you say you are. We will require you to provide copies of two of the following ID documents to help us to verify your identity:</p>


                <ol class="line-left">
                  <li>Passport.</li>
                  <li>Driving licence.</li>
                  <li>Student photo ID card.</li>
                </ol>


                <p>It will help us to process your request if you clearly state which right you wish to exercise and, where relevant, why it is that you are exercising it. The clearer and more specific you can be the faster and more efficiently we can deal with your request. If you do not provide us with sufficient information then we may delay actioning your request until you have provided us with additional information (and where this is the case we will tell you).</p>


                <h4>When will Moola Rewards contact me?<p></p>


                <ol class="line-left">
                  <li>in relation to any service, activity or online content you have signed up for, in order to ensure that we can deliver the services, e.g. to verify your email when you sign up for a Moola Rewards account, or to help you reset your password or we need to verify your student status.</li>
                  <li>in relation to any correspondence we receive from you or any comment or complaint you make about Moola Rewards products or services;</li>
                  <li>in relation to any personalised services you are using;</li>
                  <li>in relation to any contribution you have submitted to Moola Rewards, e.g. on the Moola Rewards social message boards or via text or voicemail message;</li>
                  <li>to invite you to participate in surveys about Moola Rewards services (participation is always voluntary);</li>
                  <li>to update you on any material changes to Moola Rewards’ policies and practices; and</li>
                  <li>for marketing purposes, as set out below.</li>
                </ol>


                <p>We will never contact you to ask for your Moola Rewards account password, or other login information. Please be cautious if you receive any emails or calls from people asking for this information and claiming to be from Moola Rewards.</p>


                </h4><h4>Will I be contacted for marketing purposes?</h4><p>
                Moola Rewards will only send you marketing emails or contact you on Moola Rewards platforms where you have agreed to this.</p>

                <p>
                We may offer regular emails, including a weekly update, to let you know about new discounts and services. From time to time we may also contact you to ask your views on issues affecting Moola Rewards.</p>

                <p>
                We may personalise the message content based upon any information you have provided to us and your use of the Site and apps.</p>

                <p>
                We may use information which we hold about you to show you relevant advertising on third party websites (e.g. Facebook, Google, Instagram, Snapchat and Twitter). This could involve showing you an advertising message where we know you have a Moola Rewards account and have used Moola Rewards discounts. If you don’t want to be shown targeted advertising messages from Moola Rewards, some third party websites allow you to request not to see messages from specific advertisers on that website in future.</p>


                <h4>Web browser cookies<p></p>

                <p>
                1) What is a Cookie?<br>
                A cookie is a small amount of data, which often includes a unique identifier that is sent to your computer, tablet or mobile phone (all referred to here as a "device") web browser from a website's computer and is stored on your device's hard drive. Each website can send its own cookie to your web browser if your browser's preferences allow it. Many websites do this whenever a user visits their website in order to track online traffic flows. Similar technologies are also often used within emails to understand whether the email has been read or if any links have been clicked. If you continue without changing your settings, we’ll assume that you are happy to receive all cookies on the Site. However, you can change your cookie settings at any time.</p>

                <p>
                On the Site, cookies record information about your online preferences and allow us to tailor our websites to your interests.</p>

                <p>
                2) How does Moola Rewards use cookies?<br>
                Information supplied by cookies can help us to understand the profile of our visitors and help us to provide you with a better user experience. It also helps us recognise when you are signed in to your Moola Rewards account and to provide a more personalised experience. Moola Rewards uses this type of information to help improve the services it provides to its users.</p>

                <p>
                3) Other information collected from web browsers<br>
                Your web browser may also provide Moola Rewards with information about your device, such as an IP address and details about the browser that you are using. Where requesting local discounts, it may be possible for you to choose to provide Moola Rewards with access to your device’s location through the web-browser. We use information provided by your browser or by the link that you have clicked to understand the webpage that directed you to Moola Rewards and this may be captured by performance cookies.</p>

                <p>
                If you have any concerns about the way that we use cookies or in respect of your settings, then please contact us.</p>


                </h4><h4>Apps and Devices</h4><p>
                When you download or use the Moola Rewards app on your mobile device, information may be accessed from or stored to your device. Most often this is used in a similar way to a web browser cookie, such as by enabling the app to ‘remember’ you or provide you with the content you have requested.</p>

                <p>
                Your web browser or device may also provide Moola Rewards with information about your device, such as a device identifier or IP address. Device identifiers may be collected automatically, such as the device ID, IP address, MAC address, IMEI number and app ID (a unique identifier relating to the particular copy of the app you are running).</p>

                <p>
                When you sign in to the Moola Rewards app, your sign-in details may be stored securely on the device you are using, so you can access on the same device without needing to enter your sign-in details again.</p>


                <h4>Contact Details</h4><p>
                If you any questions or comments about this Policy please contact The Data Privacy Manager by email,<a href="mailto:help@moolastudentbanking.co.uk"><span style="color:#f63"> help@moolastudentbanking.co.uk</span></a>.</p>


                <h4>How to complain</h4><p>
                We hope that we can resolve any query or concern you raise about our use of your data so please in the first instance get in touch via the contact details above.</p>

                <p>
                You do however have the right to make a complaint under the General Data Protection Regulation to a supervisory authority, in particular in the European Union (or European Economic Area) state where you work, normally live or where any alleged infringement of data protection laws occurred. The supervisory authority in the UK is the Information Commissioner who may be contacted at <a href="https://ico.org.uk/concerns/" target="_blank"><span style="color:#f63">https://ico.org.uk/concerns/ </span></a>or via telephone on 0303 123 1113.</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>	
<div class="modal fade" id="modal-contact" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style="display: none;" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
         <img src="img/ico-close-black.svg" alt="">
      </button>
      <div class="modal-header">
        <h5 class="modal-title">Contact Us!</h5>
        <p class="subtitle">Got a question? We'll give you straight answers!</p>
      </div>
      <div class="modal-body">
           <?php echo $this->Form->create("Contact",array('url'=>SITE_LINK.'contact_us')); ?>
              <div class="row">
                <div class="col-lg-12">
					  <div>
						<div class="alert alert-warning" id="error_msg_contact" style="display:none;"></div>
						<div class="alert alert-success" id="success_msg_contact" style="display:none;"><strong>Congratulations!</strong> E-Mail sent successfully.</div>
					 </div>	
				 </div>	
				 <div class="col-lg-6">
                  <div class="form-group">
                    <label for="name">NAME</label>
                   <?php echo $this->Form->input('name',array("type"=>"text",'label' => false,"class"=>"form-control"));?>

                  </div>
                  <div class="form-group">
                    <label for="company">COMPANY</label>
                   <?php echo $this->Form->input('company',array("type"=>"text",'label' => false,"class"=>"form-control"));?>
                    </div>
                  <div class="form-group">
                    <label for="telephone">TELEPHONE</label>
                     <?php echo $this->Form->input('phone',array("type"=>"text",'label' => false,"class"=>"form-control"));?>

                  </div>
                  <div class="form-group">
                    <label for="email">EMAIL</label>
                     <?php echo $this->Form->input('email',array("type"=>"email",'label' => false,"class"=>"form-control"));?>

                  </div>
                  
                </div>
                <div class="col-lg-6">
                  <div class="form-group">
                    <label for="message">MESSAGE</label>
					<?php echo $this->Form->input('message',array("type"=>"textarea",'label' => false,"class"=>"form-control"));?>

                  </div>
                </div>
              </div>
              <div class="text-right">
				  <div id="sending_msg" style="display:none">Sending mail....</div>
                <button class="link">SEND MESSAGE</button>
              </div>
            <?php echo $this->Form->end(); ?>
        
      </div>
    </div>
  </div>
</div>				
<?php
echo $this->Html->script(array('moola/jquery-2-2-4.min','js-cookie.min','uncode-privacy-public.min'));
?>
<script>
	var SITE_LINK = "<?php echo SITE_LINK; ?>";
</script>
<?php
echo $this->Html->script(array('loginvalidate','signupvalidate','resetPassword','jquery.validate.min','jquery.form.min','fblogin'));
?>
<script src="<?php echo SITE_LINK?>js/vendor.js"></script>
<script>
	$(document).ready(function () {
    $('#ContactIndexForm').validate({ // initialize the plugin
        rules: {
			 
			  "data[Contact][name]": {
				required: true				
			  },
			 "data[Contact][email]": {
				required: true,
				email : true		
			  },
			 "data[Contact][message]": {
				required: true			
			  },
			  "contactus_hiddenRecaptcha": { required: true }			
			},
			// Specify validation error messages
			messages: {			
				 "data[Contact][name]": {
					required: "Please enter a name"			
				},			 
			  "data[Contact][email]" :{
				required: "Please enter an email",
				email : "Please enter valid email"			
			  },
		   "data[Contact][message]": {
				required: "Please enter a message"			
			}	
			},	
			submitHandler: function(form) {		
				$('#error_msg_contact').hide();
				$('#sending_msg').show();						
			   $('#ContactIndexForm').ajaxSubmit(function(response) {
					response = JSON.parse(response);	
					 $('#error_msg_contact').empty();					
					if ( response.status ) {
						$("#success_msg_contact").show();	
						$("#ContactIndexForm").trigger("reset");
						$('#sending_msg').hide();
						$("#success_msg_contact").slideDown(function() {
							setTimeout(function() {
								$("#success_msg_contact").slideUp();
								$("#modal-contact").modal("hide"); 	
							}, 5000);
						});
							
					} else {						
						  $('#error_msg_contact').text(response.message);						
						 $('#error_msg_contact').show();						
						return false;
					}
				});
			}
		  });		
});
</script>

</body>
</html>
