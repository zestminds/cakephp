<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Rave : Coming Soon</title>
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<link rel="shortcut icon" href="<?php echo SITE_LINK?>favicon.ico" type="image/x-icon">
<link rel="icon" href="<?php echo SITE_LINK?>favicon.ico" type="image/x-icon">
<!-- css -->
<?php 

echo $this->Html->css(array("moola/bootstrap.min","moola/font-awesome.min","moola/owl.carousel","moola/style","moola/responsive","internal"));
echo $this->Html->script(array("moola/jquery-2-2-4.min","jquery.validate.min"));
?>

<script>
var SITE_LINK = "<?php echo SITE_LINK; ?>";
</script>
<!--[if IE]>
	<script src="js/html5shiv.js"></script>
<![endif]-->
</head>
<body>
    <?php echo $content_for_layout; ?>
<?php echo $this->Html->script(array("moola/bootstrap.min","moola/owl.carousel","moola/custom","moola/moment")); ?>

</body>
</html>
