<?php echo $this->Session->flash(); ?>	
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Rave : Login</title>
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<?php 
echo $this->Html->meta('icon', $this->Html->url(SITE_LINK.'img/fav_icon.png'));
echo $this->Html->css(array('moola/bootstrap.min','moola/font-awesome.min','moola/style','moola/responsive','admin-internal.css'));
echo $this->Html->script(array('moola/jquery-2-2-4.min'));
?>
<!--[if IE]>
        <script src="js/html5shiv.js">onload="myFunction()"</script>
    <![endif]-->
<script type="text/javascript">			
var DEFAULT_LINK = "<?php echo "http://".$_SERVER['SERVER_NAME'].$this->params->base."/"; ?>";		
var BASE_URL = "<?php echo $this->base; ?>";
var SITE_LINK = "<?php echo $this->base; ?>";
var admin = 1;       
</script>

<style type="text/css">
    html, body {
        height: 100%;
        position: relative;
    }
</style>
</head>
<body>

    <?php  echo $content_for_layout; ?>
<!-- footer -->
<?php 
	 // echo $this->element("moolafooterwithoutlogin"); 
echo $this->Html->script(array('moola/bootstrap.min','moola/owl.carousel','moola/custom','jquery.validate','admin/validate','validationmessages','jquery.validate.min'));
echo $this->element("jscssloader",array("js"=>$jsArray,"css"=>$cssArray)); ?>
<script src="https://www.google.com/recaptcha/api.js?onload=CaptchaCallback&render=explicit"></script>
</body>
</html>
    
