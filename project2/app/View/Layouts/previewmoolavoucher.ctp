<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<?php if (isset($title_for_layout)) {  ?>
<title><?php echo $title_for_layout; ?></title>
	
<?php } else { ?>
<title><?php echo $title_for_layout; ?></title>
<?php } ?>
<?php if (isset($keywords)) {  ?>
	<meta name="keywords" content="<?php echo $keywords; ?>" />
<?php } else { ?>
	<meta name="keywords" content="Rave" />
<?php } ?>

<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<link rel="shortcut icon" href="<?php echo SITE_LINK?>favicon.ico" type="image/x-icon">
<link rel="icon" href="<?php echo SITE_LINK?>favicon.ico" type="image/x-icon">
<!-- css -->
<?php 

echo $this->Html->css(array("moola/bootstrap.min","moola/font-awesome.min","moola/owl.carousel","moola/style","moola/responsive","internal")); ?>
<?php echo $this->Html->script(array("moola/jquery-2-2-4.min"));?>
<script>
var SITE_LINK = "<?php echo SITE_LINK; ?>";
</script>
<!--[if IE]>
	<script src="js/html5shiv.js"></script>
<![endif]-->
</head>
<body>
<div class="main mola-mrchent"> 
  <!-- Preloader -->
  <div class="preLoader"></div>
    <!-- End header sec -->

    <?php echo $content_for_layout; ?>

<?php echo $this->Html->script(array("moola/bootstrap.min","moola/owl.carousel","moola/custom")); ?>

<?php echo $this->element("jscssloader",array("js"=>$jsArray,"css"=>$cssArray)); ?>

</body>
</html>
