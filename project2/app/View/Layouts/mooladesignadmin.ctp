<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>::Home::</title>
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<?php 
echo $this->Html->meta('icon', $this->Html->url(SITE_LINK.'img/fav_icon.png'));
echo $this->Html->css(array('moola/bootstrap.min','moola/font-awesome.min','moola/style','moola/responsive','admin-internal',"moola/datetime-picker-4-7-14-min"));
 echo $this->Html->script(array('moola/jquery-2-2-4.min'));

?>
<!--[if IE]>
        <script src="js/html5shiv.js">onload="myFunction()"</script>
    <![endif]-->
<script type="text/javascript">			
var DEFAULT_LINK = "<?php echo "http://".$_SERVER['SERVER_NAME'].$this->params->base."/"; ?>";		
var BASE_URL = "<?php echo $this->base; ?>";
var SITE_LINK = "<?php echo SITE_LINK; ?>";
var admin = 1;       
</script>
<?php 
echo $this->Html->css(array('admin/admin'));
?>
</head>
<body>
<div class="admin-main"> 
  <!-- Preloader -->
  <div class="preLoader"></div>
	
	<?php if ( !$this->Session->read("Auth.User.id") ) {
		//echo $this->element("moolaheaderwithoutlogin"); 
    }
    else{
		echo $this->element("mooladminheaderwithlogin"); 
	}
    ?>
    <!-- End header sec -->
 
    <?php echo $this->element("mooladminleft");  echo $content_for_layout; ?>


<!-- footer -->
<?php 
	 // echo $this->element("moolafooterwithoutlogin"); 
echo $this->Html->script(array('moola/bootstrap.min','moola/owl.carousel','moola/custom','jquery.validate','admin/validate','validationmessages',"moola/moment"));//,'jquery.validate.min'
echo $this->element("jscssloader",array("js"=>$jsArray,"css"=>$cssArray)); ?>
<?php if ( $this->params['controller'] == 'Vouchers' || $this->params["action"]=="admin_edit" || $this->params["action"]=="admin_view") { ?>	
<script type="text/javascript" src="<?php echo SITE_LINK ?>js/moola/datetime-picker-4-7-14-min.js"></script>

<script src="<?php echo SITE_LINK ?>js/app.js"></script>
<script src="<?php echo SITE_LINK ?>js/bootstrap-tagsinput.js"></script>
<script>
    $(function() {
	  $('.datetimepicker1').datetimepicker({
          //format: "DD/MM/YYYY"
          format: "YYYY-MM-DD",
          //minDate:new Date()
	  });
	  $('.datetimepicker2').datetimepicker({
           format: 'LT'
	  });
	});
$(document).ready(function(){		
		
	$("div.weekdays").children("label").on("click",function(){	
		if ($("#repeat_onW").is(":checked")) { 
		$(this).toggleClass("selected");
		}
		});		
	$(document).on("click",".weekoption",function()
	{	
		if ($(this).val() == "W") {
			$(".weekdays").each(function(){
			$(this).children("input").removeAttr("disabled");
			});
		} else {
			$(".weekdays").each(function(){
			$(this).children("label").removeClass("selected");				
			$(this).children("input").attr("disabled","DISABLED");
			});
		}	
	});
	$(window).keydown(function(event){
		if(event.keyCode == 13) {
		  event.preventDefault();
		  return false;
		}
	});
	$("#VoucherStartHour").val($("#tmpstarthr").val());
	$("#VoucherEndHour").val($("#tmpendhr").val());

});

  </script>
  <?php } ?>
<script>	
	$(document).ready(function() {
		
			
			$('.btn-searh').click(function(e) {
				$('body').addClass('opensearh');
			});
			
			$("#allsearch").on("submit",function(e){
				if ($("#srch").val() == "") {
					e.preventDefault();
				}
			});
			
	});

	$(document).ready(function() {
		<?php 
			if( isset($srch) ) {
		?>
			$('body').addClass('opensearh');
		<?php
			}
		?>
			$('.btn-toggle').click(function(e) {
				$('body').toggleClass('opendash');
			});
	});

	$("#flashMessage").fadeOut(8000);

	$('.parent > a').click(function() {
			$(this).parent('.parent').siblings().removeClass('active');
			$(this).parent('.parent').toggleClass('active');
			$(this).parent('.parent').siblings().children('.subchilds').slideUp();
			$(this).parent('.parent').children('.subchilds').slideToggle();
	});

</script>
 <?php echo $this->element('sql_dump'); ?>
</body>
</html>
    
