<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<?php if (isset($title_for_layout)) {  ?>
	<title><?php echo $title_for_layout; ?></title>
	
<?php } else { ?>
<title><?php echo $title_for_layout; ?></title>
<?php } ?>
<?php if (isset($keywords)) {  ?>
	<meta name="keywords" content="<?php echo $keywords; ?>" />
<?php } else { ?>
	<meta name="keywords" content="rave" />
<?php } ?>
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<!-- css -->
<link rel="shortcut icon" href="<?php echo SITE_LINK?>favicon.ico" type="image/x-icon">
<link rel="icon" href="<?php echo SITE_LINK?>favicon.ico" type="image/x-icon">
<?php 
	echo $this->Html->css(array("moola/bootstrap.min","moola/font-awesome.min","moola/owl.carousel","moola/style","moola/responsive","internal","fishook/style")); 
	echo $this->Html->script(array('moola/jquery-2-2-4.min'));
?>

<script>
var SITE_LINK = "<?php echo SITE_LINK; ?>";
</script>
<!--[if IE]>
	<script src="js/html5shiv.js"></script>
<![endif]-->

</head>
<body class="home sm-container" style="padding-right:0 !important;">
<div class="main mola-mrchent"> 
  <!-- Preloader -->
  <div class="preLoader"></div>
	<?php echo $this->Session->flash(); ?>	
	<?php if ( !$this->Session->read("Auth.User.id") ) {
		echo $this->element("moolaheaderwithoutlogin"); 
    }
    else{
		echo $this->element("headerwithlogin"); 
	}
    ?>
    <!-- End header sec -->

    <?php echo $content_for_layout; ?>

    <?php echo $this->element("footer"); ?>
    
<?php echo $this->Html->script(array("moola/bootstrap.min","moola/owl.carousel","moola/custom")); ?>
<script src="https://www.google.com/recaptcha/api.js?onload=CaptchaCallback&render=explicit" async defer></script>
<?php echo $this->element("jscssloader",array("js"=>$jsArray,"css"=>$cssArray)); ?>
<?php if ( $this->params['controller'] == 'pages' && $this->params['action'] == 'faq' || $this->params['action'] == 'student_support_faqs') { ?>
<script>
	$('.parent > a').click(function() {
		$(this).parent('.parent').siblings().removeClass('active');
		$(this).parent('.parent').toggleClass('active');
		$(this).parent('.parent').siblings().children('.sub-lising').slideUp();
        $(this).parent('.parent').children('.sub-lising').slideToggle();
    });
</script>	
<?php } ?>
<?php if ( $this->params['controller'] == 'Calendar' ) { ?>
<script type="text/javascript" src="<?php echo SITE_LINK ?>datetime/bootstrap/js/bootstrap-datetimepicker.min.js"></script>
<script src="<?php echo SITE_LINK ?>js/bootstrap-tagsinput.js"></script>
<script type="text/javascript">
   	$('.form_date').datetimepicker({
        language:  'en',
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 2,
		minView: 2,
		forceParse: 0
    });
	$('.form_time').datetimepicker({
        language:  'en',
        weekStart: 1,
        todayBtn:  1,
		autoclose: 1,
		todayHighlight: 1,
		startView: 1,
		minView: 0,
		maxView: 1,
		forceParse: 0,
		showMeridian:true		
    });
    
    
</script>    
    
<script>
 
$(document).ready(function(){		
		
		$("div.weekdays").children("label").on("click",function(){			
			$(this).toggleClass("selected");
		});			
		
		$(document).on("click",".weekoption",function()
		{	
			if ($(this).val() == "W") {
				$(".weekdays").each(function(){
				$(this).children("input").removeAttr("disabled");
				});
			} else {
				$(".weekdays").each(function(){
				$(this).children("input").attr("disabled","DISABLED");
				});
			}	
		});
$(window).keydown(function(event){
    if(event.keyCode == 13) {
      event.preventDefault();
      return false;
    }
  });

});

  </script>

<?php } ?>
<?php if ( $this->params['controller'] == 'Vouchers' ) { ?>
<script>
$('.discounts-slide .owl-carousel').owlCarousel({
    loop:true,
    margin:0,
    nav:false,
	dots:true,
    autoplay:true,
    autoplayTimeout:5000,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:1
        },
        1000:{
            items:1
        }
    }
})
</script>
<?php } ?>
 <?php echo $this->element('sql_dump'); ?>
</body>
</html>
