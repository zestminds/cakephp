<div class="userUniversities view">
<h2><?php echo __('User University'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($userUniversity['UserUniversity']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('User'); ?></dt>
		<dd>
			<?php echo $this->Html->link($userUniversity['User']['id'], array('controller' => 'users', 'action' => 'view', $userUniversity['User']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('University'); ?></dt>
		<dd>
			<?php echo $this->Html->link($userUniversity['University']['title'], array('controller' => 'universities', 'action' => 'view', $userUniversity['University']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Valid Till'); ?></dt>
		<dd>
			<?php echo h($userUniversity['UserUniversity']['valid_till']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($userUniversity['UserUniversity']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($userUniversity['UserUniversity']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit User University'), array('action' => 'edit', $userUniversity['UserUniversity']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete User University'), array('action' => 'delete', $userUniversity['UserUniversity']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $userUniversity['UserUniversity']['id']))); ?> </li>
		<li><?php echo $this->Html->link(__('List User Universities'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User University'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Universities'), array('controller' => 'universities', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New University'), array('controller' => 'universities', 'action' => 'add')); ?> </li>
	</ul>
</div>
