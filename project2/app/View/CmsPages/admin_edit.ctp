<script>
	$(document).ready(function () {
    $('#CmsPageAdminEditForm').validate({ // initialize the plugin
        rules: {			
			 
			  "data[CmsPage][content]": {
				required: true				
			  },
			 "data[CmsPage][slug]": {
				required: true		
			  },
			 "data[CmsPage][seo_url]": {
				required: true		
			  },
			 "data[CmsPage][header]	": {
				required: true		
			  },
			 "data[CmsPage][meta_title]": {
				required: true		
			  },
			 "data[CmsPage][meta_keyword]": {
				required: true		
			  }			
			},
			// Specify validation error messages
			messages: {			
				"data[CmsPage][content]": {
					required: "Please enter page content"			
				},
				"data[CmsPage][slug]": {
					required: "Please enter slug"			
				},
				"data[CmsPage][seo_url]": {
					required: "Please enter seo url"			
				},
				"data[CmsPage][header]": {
					required: "Please header of page"			
				},
				"data[CmsPage][meta_title]": {
					required: "Please enter meta title"			
				},
				"data[CmsPage][meta_keyword]": {
					required: "Please enter meta keyword"			
				}
			},
			// Make sure the form is submitted to the destination defined
			// in the "action" attribute of the form when valid
			submitHandler: function(form) {
			  form.submit();
			}
		  });		
});
</script>	
<div class="cont-right">
   <div class="cont-right-innr">
	    <div class="main-hd-in">
          <div class="row">
            <div class="col-sm-12">
              <h2 class="title-das">Edit CMS Pages</h2>              
            </div>
          </div>
        </div>
		<div class="manage-marchant">
		<?php echo $this->Form->create('CmsPage',array("novalidate"=>true)); ?>
			<fieldset>
					
			<?php
				
				echo $this->Form->input('slug');
				echo $this->Form->input('seo_url');
				echo $this->Form->input('header');
				echo $this->Form->input('meta_title');
				echo $this->Form->input('meta_keyword');
				echo $this->Form->input('id');		
				echo $this->Form->input('content');
				echo $this->Form->input('is_active');
			?>
			</fieldset>
		  <div style="margin-top:20px;margin-left:21px">
				  <button type="submit" class="btn btn-theme" id="save" name="save">SAVE</button>
							 
					<?php //echo $this->Form->Submit('SUBMIT',array("class"=>"btn btn-theme","title"=>"save",'div'=>false)); ?>
					<a href="<?php echo SITE_LINK ?>edit-cmspage" class="btn btn-theme">cancel</a>				  
				</div>
		</div>
		<?php echo $this->Html->script("ckeditor/ckeditor"); //CmsPageContent ?>
		<script>
		$(document).ready(function() { CKEDITOR.replace( 'CmsPageContent'); });
		</script>
	</div>		
</div>
