<style>
.td{align:left;}
</style>
<div class="cont-right">
   <div class="cont-right-innr">	
      <div class="main-hd-in">
          <div class="row">
            <div class="col-sm-12">
              <h2 class="title-das"> CMS Pages</h2>
              
              	<div class="show-record">                	
                   <span> Show Records </span> 
                   <?php echo $this->Form->create("cmspage",array("div"=>false,"type"=>"get")); 			
					 echo $this->Form->input('records',array("id"=>"records",'label'=>'',"options"=>$records,'class'=>'form-control',"selected"=>$limit));
					 echo $this->Form->end(); ?>	
                </div>
            </div>
          </div>
        </div>
	   <div class="cont-right-innr border-box1">
			<div class="cmsPages index">		
				<?php echo $this->Form->create("cmspage",array("div"=>false,)); ?>
				<div class="srch">
					<?php echo $this->element("admins/common",array("place"=>'Search by Page slug',"flag"=>false,"pageheader"=>'',"buttontitle"=>'no',"listflag"=>"no","action"=>'no',"selflag"=>false)); ?>
					<div class="rhs_actions right">
						<a href="<?php echo SITE_LINK."ad-new-cmspage"; ?>">Add CmsPage</a>
						<?php // echo $this->Html->link(__('Add CmsPage'), array('action' => 'add')); ?>
					</div>
				</div>
				<div class="table-responsive">
				<table  class="Marchant-table table table-bordered" width="100%">
				<thead>
				<tr>
						<th><?php echo $this->Form->input("check",array("label"=>false,"div"=>false,"id"=>'checkall',"type"=>'checkbox')); ?></th>			
						<th>S.No</th>	
						<th><?php echo $this->Paginator->sort('slug'); ?></th>
						<th><?php echo $this->Paginator->sort('seo_url'); ?></th>
						<th><?php echo $this->Paginator->sort('header'); ?></th>
						<th><?php echo $this->Paginator->sort('is_active'); ?></th>
						<th><?php echo $this->Paginator->sort('created'); ?></th>
						<th><?php echo $this->Paginator->sort('modified'); ?></th>
						<th class="actions"><?php echo __('Actions'); ?></th>
				</tr>
				</thead>
				<tbody>
				<?php foreach ($cmsPages as $cmsPage): ?>
				<tr>
					<td><?php echo $this->Form->input("id.".$cmsPage['CmsPage']['id'],array("class"=>'chk',"value"=>$cmsPage['CmsPage'],"type"=>'checkbox',"div"=>false,"label"=>false)); ?><?php echo $this->Form->input("status.".$cmsPage['CmsPage']['id'],array("type"=>'hidden',"value"=>($cmsPage['CmsPage']['is_active'] == 1?0:1))); ?></td>
				    <td><?php echo $sno;?>&nbsp;</td>	
					<td><?php echo h($cmsPage['CmsPage']['slug']); ?>&nbsp;</td>
					<td><?php echo h($cmsPage['CmsPage']['seo_url']); ?>&nbsp;</td>
					<td><?php echo h($cmsPage['CmsPage']['header']); ?>&nbsp;</td>
					<td><?php echo h(($cmsPage['CmsPage']['is_active'] == 1)?'Active':'Inactive'); ?>&nbsp;</td>
					<td><?php echo h($cmsPage['CmsPage']['created']); ?>&nbsp;</td>
					<td><?php echo h($cmsPage['CmsPage']['modified']); ?>&nbsp;</td>					
					<td><a href="<?php echo SITE_LINK.'edit-cmspage/'.$cmsPage['CmsPage']['id']?>"><i class="fa fa-pencil"></i></a>	
					<?php //echo $this->Form->postLink($this->Html->tag('i', '', array('class' => 'glyphicon glyphicon-trash')). " ",   array('action' => 'delete', $cmsPage['CmsPage']['id']),array('escape'=>false),__('Are you sure you want to delete # %s?', $cmsPage['CmsPage']['slug']));?>
				</td>
				</tr>
				<?php $sno++; endforeach; ?>
				</tbody>
				</table>
				</div>
				<div class="pagination-main">
					<div class="paging pagination">
					<?php echo $this->Paginator->prev('<i class="fa fa-caret-left"></i>', array('escape' => false), null, array('class' => 'fa prev disabled'));
					
					echo $this->Paginator->numbers(array('separator' => ''));
					echo $this->Paginator->next('<i class="fa fa-caret-right "></i>', array('escape' => false), null, array('class'	=> 'fa next disabled'));
					?>
				   </div>  
				</div>  
			<?php echo $this->Form->end(); ?>
			</div>
		</div>		
	</div>	
</div>
<script>
$(document).ready(function () {
	$("#records").change(function(e) { 
			var val=$('#records').val();
			window.location.replace(SITE_LINK+"CmsPages/?records="+val);			
				  	  
	});
});	
</script>
