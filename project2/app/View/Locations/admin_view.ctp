<div class="cont-right">
   <div class="cont-right-innr">
		<div class="locations view">
		<h2><?php echo __('Location'); ?></h2>
			<dl>
				<dt><?php echo __('Id'); ?></dt>
				<dd>
					<?php echo h($location['Location']['id']); ?>
					&nbsp;
				</dd>
				<dt><?php echo __('Location Name'); ?></dt>
				<dd>
					<?php echo h($location['Location']['title']); ?>
					&nbsp;
				</dd>		
				<dt><?php echo __('Status'); ?></dt>
				<dd>
					<?php echo h(($location['Location']['is_active'])?'Active':'Inactive'); ?>
					&nbsp;
				</dd>
				<dt><?php echo __('Created'); ?></dt>
				<dd>
					<?php $joined =  date_create(h($location['Location']['created'])); echo date_format($joined,"jS M, Y h:i:s");?>
					&nbsp;
				</dd>
				<dt><?php echo __('Modified'); ?></dt>
				<dd>
					<?php $joined =  date_create(h($location['Location']['modified'])); echo date_format($joined,"jS M, Y h:i:s");?>
					&nbsp;
				</dd>
			</dl>
		</div>
	</div>		
</div>
