<?php
if ($this->Session->read("Auth.User.id") && $this->Session->read("Auth.User.user_type_id") == 2) {
    $typeId = $this->Session->read("AuthUser.User.usertype_id");
    $userTypes = array(1, 2, 3);
    //pr($this->params['action']);
    //die;
?>
    <div id="adminMenu" class="ddsmoothmenu actions">
        <a class="side-bar" onclick="hidepanel();" id="btn" href="javascript:void(0);" title="Click to hide panel" >Click to hide panel</a>
        <?php if ($this->Session->read("Auth.User.id") && $this->Session->read("Auth.User.user_type_id") == 2 ) { ?>  
            <ul>
                <ul class="admintoggel"></ul>
                
				<li>
					<a href="<?php echo $this->Html->url(SITE_LINK.'merchant'); ?>" <?php if ($this->params['controller'] == 'users' && $this->params['action'] == 'admin_dashboard') { ?> class="active" <?php } ?> >Dashboard </a>
				</li>	
				<li>
					<a href="<?php echo $this->Html->url(SITE_LINK.'create-voucher/'); ?>" <?php if ($this->params['controller'] == 'add') { ?> class="active" <?php } ?>>Create Vouchers</a>
				</li> 
				<li>
					<a href="<?php echo $this->Html->url(SITE_LINK.'draft-vouchers/'); ?>" <?php if ($this->params['controller'] == 'vouchers' && $this->params['action'] == 'index') { ?> class="active" <?php } ?>>Manage Vouchers</a>
				</li> 
				<li>
					<a href="<?php echo $this->Html->url(SITE_LINK.'manage-billing');	 ?>" <?php if ($this->params['controller'] == 'plans') { ?> class="active" <?php } ?> >Billing </a>
				</li>
					<?php /*			
				<li>
					<a href="<?php echo $this->Html->url(SITE_LINK.'manage_insights'); ?>" <?php if ($this->params['controller'] == '') { ?> class="active" <?php } ?> >Insights </a>
				</li>
				 <li>
					<a href="<?php echo $this->Html->url(SITE_LINK.'manage-sales'); ?>" <?php if ($this->params['controller'] == 'users'  && $this->params['action'] == 'admin_index' ) { ?> class="active" <?php } ?> >Billing </a>
				</li>**/?>
				 <li>
					<a href="<?php echo $this->Html->url(SITE_LINK.'update-profile'); ?>" <?php if ($this->params['action'] == 'profile') { ?> class="active" <?php } ?>>My Account</a>
				</li> 				
			</ul>
		<?php } ?>
    </div>
<?php } ?>

