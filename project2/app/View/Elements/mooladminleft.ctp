<div class="cnt_mid">
	<aside class="das-sidebar">
	  <div class="lcmain">
		<ul class="main-nav">
		  <li class="parent "><a href="<?php echo $this->Html->url(SITE_LINK.'dashboard'); ?>"><?php echo $this->Html->image("dasbord-img-1.png",array("alt"=>"Rave Dashboard"));?> Dashboard </a> <span class="arrow-toggle"> <i class="fa fa-angle-right"></i> </span>
		   </li>
		  <li class="parent active"><a href="javascript:void(0)"> <?php echo $this->Html->image("dasbord-img-2.png",array("alt"=>"Manage Users"));?>Manage Users</a>
			<ul class="subchilds" style="display:block;">
			  <li class="student"><a href="<?php echo $this->Html->url(SITE_LINK.'manage_app_user'); ?>"> <i class="fa fa-circle-o"></i> App User </a></li>
			  <li class="marchant"><a href="<?php echo $this->Html->url(SITE_LINK.'manage_event_manager'); ?>"> <i class="fa fa-circle-o"></i> Event Manager </a></li>			  
			</ul>
			 <span class="arrow-toggle"> <i class="fa fa-angle-right"></i> </span>
		  </li>
		  <li class="parent"><a href="<?php echo $this->Html->url(SITE_LINK.'view_events'); ?>"><?php echo $this->Html->image("calendar_icon.png",array("alt"=>"Insights"));?> Manage Events </a>
		  <span class="arrow-toggle"> <i class="fa fa-angle-right"></i> </span>
		  </li>
		  
		  <li class="parent"><a href="<?php echo $this->Html->url(SITE_LINK.'manage-profile'); ?>"><?php echo $this->Html->image("user.png",array("alt"=>"Manage Profile"));?>Manage Profile </a>
		  <span class="arrow-toggle"> <i class="fa fa-angle-right"></i> </span>
		  </li>
		 
		  <li class="parent"><a href="<?php echo $this->Html->url(SITE_LINK.'view-sales'); ?>"><?php echo $this->Html->image("dasbord-img-4.png",array("alt"=>"Sales"));?>Sales</a>
		  <span class="arrow-toggle"> <i class="fa fa-angle-right"></i> </span>
		  </li>	
		  		  
		   <li class="parent active"><a href="javascript:void(0)"> <?php echo $this->Html->image("dasbord-img-5.png",array("alt"=>"Setting"));?>Settings </a>		 
			<ul class="subchilds" style="display:block;">
			<li class="marchant"><a href="<?php echo $this->Html->url(SITE_LINK.'events_setting'); ?>"> <i class="fa fa-circle-o"></i> Events </a>

			  <li class="student"><a href="<?php echo $this->Html->url(SITE_LINK.'setting'); ?>"> <i class="fa fa-circle-o"></i> Plans </a></li>
			  <li class="marchant"><a href="<?php echo $this->Html->url(SITE_LINK.'CmsPages'); ?>"> <i class="fa fa-circle-o"></i> CmsPages </a></li>
			  <li class="student"><a href="<?php echo $this->Html->url(SITE_LINK.'email-templates'); ?>"> <i class="fa fa-circle-o"></i> Email Templates </a>
			  </li>			  
			</ul>
			 <span class="arrow-toggle"> <i class="fa fa-angle-right"></i> </span>
		  </li>
		</ul>
	  </div>
	</aside>

