<link href="<?php echo SITE_LINK ?>datetime/bootstrap/css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
<link href="<?php echo SITE_LINK ?>css/app.css" rel="stylesheet" media="screen">
<link href="<?php echo SITE_LINK ?>css/bootstrap-tagsinput.css" rel="stylesheet" media="screen">
<script type="text/javascript">
	var avatar = "<?php echo SITE_LINK."img/img-icon.png"; ?>";
</script>
<?php
$tmpImage = SITE_LINK."img/img-icon.png";

//echo $tmpImage = file_get_contents($tmpImage); die;
 ?>
 <section class="inner-banner topheader">
       <div class="container">
	        <div class="banner-cont">
	           <h2>CREATE NEW VOUCHER</h2>  
	        </div>
       </div>
    </section>
    <!--/. banner sec -->

    <section class="create-vouch-desc">
    	<div class="container">
            <div class="row">            	
				<?php echo $this->Form->create('Voucher',array("type"=>"file","class"=>"themeForm")); echo $this->Form->hidden('user_id',array('value'=>$user_id));	echo $this->Form->hidden('voucher_status');echo $this->Form->hidden('address',array("value"=>$userdetails['UserDetail']['address'])); echo $this->Form->hidden('cateid',array("value"=>$cateid)); echo $this->Form->hidden("userpaid",array("value"=>$this->Session->read("Auth.User.is_paid"))); ?>
	               <div class="col-sm-6 vochr-left">
	               	  <div class="boxCard">
	               	  	 <div class="card-inner box-shadow">
		               	  	  <h4>Title*</h4>
		               	  	  <div class="theme-radio form-group">
		                            <div class="radio radio-info radio-inline">
										<?php 
											echo $this->Form->radio('title_status',$optionsTitle,array("legend"=>"","type"=>"radio",'id'=>'title_status','value'=>'D'/*,'onClick'=>'enable_text(this.value,"discountvalue","default_title")'*/));	
										?>				
				                    </div>
		                       </div>
		                       <div class="form-group inputField">	  <span class="percertange_sign">%</span>	                       	 
		                       	 <?php echo $this->Form->input('title_discount',array("type"=>"text",'id'=>'discountvalue','label' => ['text' => 'Student Discount','class' => 'lable-control'],'value'=>$defaultvalue[0]['DefaultValue']['value']/*,'readonly'=>'readonly'*/,"class"=>"form-control"));
		                       	  echo $this->Form->hidden("default_title",array('id'=>'default_title','value'=>$defaultvalue[0]['DefaultValue']['value']));
		                       	 echo $this->Form->hidden("olddiscount",array('id'=>'olddiscount','value'=>$defaultvalue[0]['DefaultValue']['value']));

		                       	 ?>
                                  
		                       </div>
		                    </div>
	               	      </div> <!--/. End boxCard -->

	               	      <div class="boxCard">
	               	  	 <div class="card-inner box-shadow">
		               	  	  <h4>Sub Title*</h4>
		               	  	  <!--div class="theme-radio form-group">
		                            <div class="radio radio-info radio-inline">
				                      <?php 
											//echo $this->Form->radio('sub_title_status',$optionsSubtitle,array("legend"=>"","type"=>"radio",'id'=>'sub_title_status','value'=>'D'/*,'onClick'=>'enable_text(this.value,"sub_title","default_sub_title")'*/));	
									   ?>
				                    </div>
		                       </div-->
		                       <div class="form-group inputField">		                       	 
		                       	  <?php echo $this->Form->input('sub_title',array('id'=>'sub_title','label' => ['text' => 'Sub Title','class' => 'lable-control'],"placeholder"=>"e.g. on all cocktails", "class"=>"form-control"));
		                       	   echo $this->Form->hidden("default_sub_title",array('id'=>'default_sub_title','value'=>$defaultvalue[1]['DefaultValue']['value']));
		                       	  ?>
		                       </div>
		                    </div>
	               	      </div> <!--/. End boxCard -->

	               	      <div class="boxCard">
	               	  	 <div class="card-inner box-shadow">
		               	  	  <h4>Description*</h4>
		               	  	  <div class="theme-radio form-group">
		                            <div class="radio radio-info radio-inline">
				                        <?php 
											echo $this->Form->radio('description_status', $optionsDesc,array('legend'=>'','id'=>'description_status',"value"=>"D"/*,'onClick'=>'enable_text(this.value,"descriptionvalue","default_description")'*/));
										?>
				                    </div>
		                       </div>
		                       <div class="form-group inputField">		                       	  
		                       	  	<?php echo $this->Form->input('description',array('id'=>'descriptionvalue','label' => ['text' => 'Write here','class' => 'lable-control'],'value'=>$defaultdescription/*,'readonly'=>'readonly'*/,"class"=>"form-control  Description-area"));
		                       	  	echo $this->Form->hidden("default_description",array('id'=>'default_description','value'=>$defaultdescription));
		                       	  	?>                                 
		                       </div>
		                    </div>
	               	      </div> <!--/. End boxCard -->

	               	      <div class="boxCard">
		               	  	 <div class="card-inner box-shadow">
			               	  	  <h4>Terms & Conditions*</h4>
			               	  	  <div class="theme-radio form-group">
			                            <div class="radio radio-info radio-inline">
					                        <?php 
												echo $this->Form->radio('terms_status', $optionsTerms,array('id'=>'terms_status','legend'=>'',"value"=>"D"/*,'onClick'=>'enable_text(this.value,"termsvalue","default_terms")'*/));
											?>
					                    </div>
			                       </div>
			                       <div class="form-group inputField">			                       	 
	                                 	<?php echo $this->Form->input('terms',array("type"=>"textarea",'id'=>'termsvalue','value','label' => ['text' => 'Terms & Conditions copy','class' => 'lable-control'],'value'=>$defaultvalue[3]['DefaultValue']['value']/*,'readonly'=>'readonly'*/,"class"=>"form-control autoExpand  Terms-area"));
										 echo $this->Form->hidden("default_terms",array('id'=>'default_terms','value'=>$defaultvalue[3]['DefaultValue']['value']));
										?>
			                       </div>
			                       <div class="website-field">
				                       	<h4>Terms & Conditions URL</h4>
				                       <div class="form-group inputField">
										  <?php echo $this->Form->input('terms_url',array('id'=>'terms_url','label' => false,'placeholder'=>$defaultvalue[4]['DefaultValue']['value'],"class"=>"form-control"));
										  ?>
				                       </div>
			                       </div>
			                    </div>
		               	    </div> <!--/. End boxCard -->
	                </div>
	                <div class="col-sm-6 vochr-right">
	                	<div class="boxCard">
	               	  	    <div class="card-inner box-shadow">
		               	  	  <h4>Time*</h4>	
		               	  	  <div class="datePicker_ttl">	
		               	  	      <h5>All Day</h5>     
		               	  	       <?php echo $this->Form->hidden('VouchDayStatus',array("id"=>"VouchDayStatus","value"=>0)); ?>
          	  	 
			                      <label class="switch switch_btn">		
									  							  
									 <?php echo $this->Form->checkbox('day_status', array('id'=>'day_status','value'=>'0','class'=>'allDay','label'=>'','options'=>$optionsAllday)) ;
									 ?>

									  <span class="slider round"></span>
									  <!-- <span class="absolute-no">NO</span> -->
								  </label>
								</div>
							    <div class="theme-Datepicker">
                                    <div class="form-group">
                                    	<div class="filed_row">
                                    		<div class="col-half">
		                                    	<label class="lable-control">Start Date</label>	
													<!--<div class="controls input-append date form_date" data-date="" data-date-format="dd MM yyyy"data-link-field="dtp_input2" data-link-format="yyyy-mm-dd">
																<div class='input_date_grp'>
																<?php //echo $this->Form->input('start_date',array("label"=>false,"type"=>"text",'readonly'=>'readonly','autofill'=>'false',"class"=>"form-control"));?>                
																<span class="add-on alldaycal" ><i class="fa fa-calendar" aria-hidden="true"></i></span>
															</div>
															<input type="hidden" id="dtp_input2" value="" />
														</div>	-->
													<div class='input_date_grp input-group date datetimepicker1'>

													<?php echo $this->Form->input('start_date',array("label"=>false,"type"=>"text",'autofill'=>'false',"class"=>"form-control"));?>    
													<span class="input-group-addon">
													<span class="fa fa-calendar"></span>
													</span>
													</div>
													
								            </div>
								            <div class="col-half">
												<label class="lable-control">End Date</label>												
													<!--<div class="controls input-append date form_date" data-date="" data-date-format="dd MM yyyy"data-link-field="dtp_input3" data-link-format="yyyy-mm-dd">
															<div class='input_date_grp'>
															<?php //echo $this->Form->input('end_date',array("label"=>false,"type"=>"text",'readonly'=>'readonly','autofill'=>'false',"class"=>"form-control"));?>                
															<span class="add-on alldaycal" ><i class="fa fa-calendar" aria-hidden="true"></i></span>
															</div>
															<input type="hidden" id="dtp_input4" value="" />												
												   </div>	-->	 
												   
												   <div class='input_date_grp input-group date datetimepicker1'>

													<?php echo $this->Form->input('end_date',array("label"=>false,"type"=>"text",'autofill'=>'false',"class"=>"form-control"));?>    
													<span class="input-group-addon">
													<span class="fa fa-calendar"></span>
													</span>
													</div>
												                                      	
								            </div>
								        </div>
						            </div>
						            <div class="form-group">
                                    	<div class="filed_row">
                                    		<div class="col-half">
												<div class="grp_date">      
		                                    	<label class="lable-control">Start Time</label>			                                    	                         	
													<!--<div class="controls input-append date form_time datepickerdiv" data-date="" data-date-format="HH:ii" data-link-field="dtp_input3" data-link-format="hh:ii">
														<div class='input_date_grp'>
															<?php echo $this->Form->input('start_hour',array("label"=>false,"type"=>"text",'readonly'=>'readonly','autofill'=>'false',"class"=>"form-control"));?>                
															 <span class="add-on alldaycal"><i class="fa fa-clock-o" aria-hidden="true"></i></span>
															</div>
															<input type="hidden" id="dtp_input3" value="" /><br/>
													</div> -->
													<div class='input_date_grp input-group date datetimepicker2'>
													  <?php echo $this->Form->input('start_hour',array("label"=>false,"type"=>"text",'autofill'=>'false',"class"=>"form-control"));?>                
													  <span class="input-group-addon">
														<span class="fa fa-clock-o"></span>
													  </span>
													</div>	
								                </div>	     
								            </div>
								            <div class="col-half">
												<div class="grp_date">        
		                                    	<label class="lable-control">End Time</label>		                                    	
												   <!--<div class="controls input-append date form_time" data-date="" data-date-format="HH:ii" data-link-field="dtp_input3" data-link-format="hh:ii">
															<div class='input_date_grp'>
															<?php //echo $this->Form->input('end_hour',array("label"=>false,"type"=>"text",'readonly'=>'readonly','autofill'=>'false',"class"=>"form-control"));?>                
															<span class="add-on alldaycal"><i class="fa fa-clock-o" aria-hidden="true"></i></span>
														</div>
														<input type="hidden" id="dtp_input5" value="" /><br/>
													</div> -->
													
													<div class='input_date_grp input-group date datetimepicker2'>
													  <?php echo $this->Form->input('end_hour',array("label"=>false,"type"=>"text",'autofill'=>'false',"class"=>"form-control"));?>                
													  <span class="input-group-addon">
														<span class="fa fa-clock-o"></span>
													  </span>
													</div>	
												</div>	
								            </div>
								        </div>
						            </div>
							    </div>
							    <div class="repeat-day">
							    	<h4>Repeat*</h4>
							    	<div class="theme-radio form-group">
			                            <div class="radio radio-info radio-inline">
					                       <?php 
												echo $this->Form->radio('repeat_on', $optionsRepeat,array('id'=>'repeat_on','legend'=>'','value'=>'D',"class"=>"weekoption"));
											?>
					                    </div>
			                       </div>
			                       <?php echo $this->Form->input('repeat_days', array('id'=>'repeat_day','type'=>'select','multiple'=>'checkbox','options'=>$optionsWeekday,'disabled' => "disabled","label"=>false,"div"=>"weak-List","class"=>"weekdays","label"=>array("class"=>"selected","text"=>""))); ?>			                    
							    </div>
							   <div id="error_msg" style="display:none" class="error">Please check at least one box in this group.</div>	

		                    </div>
	               	    </div> <!--/. End boxCard -->
	                	<div class="boxCard">
	               	  	    <div class="card-inner box-shadow">
		               	  	  <h4>Hashtags*</h4>
		                       <div class="hashtags*">
                                   <div class="inp-group" id="tags_grp">
									  <?php echo $this->Form->input('hashtags',array("label"=>false,"class"=>"form-srch","placeholder"=>"Type in your hashtags"));?>
									   <button type="button" class="btn srch-btn deletetag"><?php echo $this->Html->image("cros-icon.png",array("alt"=>"Delete Tags","id"=>"deletetag")); ?></button>
                                   </div>     
                                   <ul class="tags"></ul>
                                    <?php echo $this->Form->input('tags',array("label"=>false,"class"=>"form-srch","placeholder"=>"","type"=>"hidden"));?>
		                       </div>
		                    </div>
	               	    </div> <!--/. End boxCard -->

	               	    <div class="boxCard">
	               	  	    <div class="image-preview">
									<!-- Drop Zone -->
									<div class="upload-drop-zone" id="drop-zone"> 
									   <div class="upload-file"> 
										   <?php echo $this->Html->image("img-icon.png",array("alt"=>"Voucher Pic","id"=>"img-upload")); ?>
										   <?php echo $this->Form->input("tmpImage",array("label"=>false,"type"=>"textarea","style"=>"display:none;","id"=>"tmpImage")); ?>
										    <p>Upload File</p>
									  </div>
	                                   <!--div class="btn btn-upload image-preview-input btn-file">	                                   
	                                   	<i class="fa fa-cloud-upload" aria-hidden="true"></i><span class="image-preview-input-title">Select file to upload</span>
	                                   <?php //echo $this->Form->file('image',array("type"=>"file","accept"=>"image/png, image/jpeg, image/gif","type"=>"file","id"=>"sample_input","name"=>"test[image]"));?>
										</div-->	
								    </div>
								</div> 
		                    </div>
	               	    </div> <!--/. End boxCard -->
	                </div>
	                <div class="col-sm-12">
	                <div class="form-submit">
	                    <button type="button" class="btn btn-theme" id="preview" data-toggle="modal">PREVIEW</button>
	                	<button type="submit" class="btn btn-theme" id="save" name="save" onclick="voucherStatus('save');">SAVE</button>
	                	<button type="submit" class="btn btn-theme" id="publish" onclick="voucherStatus('publish');">PUBLISH</button>    
	                	                	
	                </div>
	                </div>	               
	            <?php echo $this->Form->end(); ?>	
            </div>
    	</div>
 </section>

<style>
.head{font-size:20px;font-weight:bold;color:#444;margin-bottom:10px;}
.preview_voucher {padding:6px 20px;border:1px solid #444;margin-top:10px;}
.linkvoucher {color:#45ccf0 !important}
.linkvoucher:hover{color:#45ccf0 !important}
button.close {
	-webkit-appearance: none;
	/* padding: 0; */
	/* cursor: pointer; */
	background: 0 0!important;
	border: 0!important;
	color: #000!important;
}
#map {
        height: 100px;  /* The height is 400 pixels */
        width:400px;  /* The width is the width of the web page */
}
div#imageModal{top:0px !important;}

.imageBox{height:470px !important; width:470px !important;}
.thumbBox{border-radius : 0px !important; height:270px !important; width:270px !important; top:42% !important; left:42% !important;}
#drop-zone{cursor:pointer;}

</style>   
<?php /*
<div class="modal fade loginModel themeModel" id="imageModal" tabindex="-1" role="dialog" aria-labelledby="imageModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
       <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title">Select Voucher Image</h4>
      </div>
      <div class="modal-body">
        <div class="selct-img-voc">
		  <div class="imageBox" >
			<!--<div id="img" ></div>-->
			<!--<img class="cropImg" id="img" style="display: none;" src="images/avatar.jpg" />-->
			<div class="mask"></div>
			<div class="thumbBox"></div>
			<div class="spinner" style="display: none">Loading...</div>
		  </div>
		  <div class="iviewer">
			 <ul class="custom-file-upload">
				 <li><label for="upload-file" class="custom-file-upload"><span ><?php echo $this->Html->image("gallery-img.png",array("alt"=>"Voucher Pic")); ?></span></label> <input id="upload-file" type="file"/></li>
				<li><a href="javascript:void(0)"><span id="rotateLeft"><?php echo $this->Html->image("rotate-left.png",array("alt"=>"Voucher Pic")); ?></span></a></li>
				 <li><a href="javascript:void(0)"><span id="rotateRight"><?php echo $this->Html->image("rotate-right.png",array("alt"=>"Voucher Pic")); ?></span></a></li>
				 <li><a href="javascript:void(0)"><span id="zoomIn"><?php echo $this->Html->image("zoom-out.png",array("alt"=>"Voucher Pic")); ?></span></a></li>
				  <li><a href="javascript:void(0)"><span id="zoomOut"><?php echo $this->Html->image("zoom-in.png",array("alt"=>"Voucher Pic")); ?></span></a></li>
			 </ul>             
		  </div>
		</div>
        
		<div class="iviewer-btn">
			<ul class="list-inline">
			   <li><button class="btn btn-custom" data-dismiss="modal">CLOSE</button></li> 
			   <li><button class="btn btn-custom" id="saveimage">SAVE CHANGES</button></li>
			</ul>
		</div>
	 </div>  	
    </div>
  </div>
</div>
*/ ?>
<div class="modal fade loginModel themeModel" id="imageModal" tabindex="-1" role="dialog" aria-labelledby="imageModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
       <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title">Select Voucher Image</h4>
      </div>
      <div class="modal-body" id="actions">
        <div class="selct-img-voc">
		  <div class="img-container" style="width:100%;">
				<img src="<?php echo $tmpImage; ?>" alt="Picture">
			</div>
		  <div class="iviewer" >
			 <ul class="custom-file-upload" class="docs-buttons">
				 <li><label for="upload-file" class="custom-file-upload"><span ><?php echo $this->Html->image("gallery-img.png",array("alt"=>"Voucher Pic")); ?></span></label> <input id="upload-file" type="file" name="file" accept="image/*"/></li>
				<li data-method="rotate" data-option="-90"><a href="javascript:void(0)" title="Rotate Left"><span id="rotateLeft"><?php echo $this->Html->image("rotate-left.png",array("alt"=>"Voucher Pic")); ?></span></a></li>
				 <li data-method="rotate" data-option="90"><a href="javascript:void(0)" title="Rotate Right"><span id="rotateRight"><?php echo $this->Html->image("rotate-right.png",array("alt"=>"Voucher Pic")); ?></span></a></li>
				 <li data-method="zoom" data-option="-0.1"><a href="javascript:void(0)" title="Zoom Out"><span id="zoomIn"><?php echo $this->Html->image("zoom-out.png",array("alt"=>"Voucher Pic")); ?></span></a></li>
				  <li data-method="zoom" data-option="0.1"><a href="javascript:void(0)" title="Zoom In"><span id="zoomOut"><?php echo $this->Html->image("zoom-in.png",array("alt"=>"Voucher Pic")); ?></span></a></li>
			 </ul>             
		  </div>
		</div>
        
		<div class="iviewer-btn">
			<ul class="list-inline">
			   <li><button class="btn btn-custom" data-dismiss="modal">CLOSE</button></li> 
			   <li><button class="btn btn-custom btn-primary" data-method="getCroppedCanvas" data-option="{ &quot;maxWidth&quot;: 4096, &quot;maxHeight&quot;: 4096 }" id="saveimage">SAVE CHANGES</button></li>
			</ul>
		</div>
	 </div>  	
    </div>
  </div>
</div>
<style>
.img-container {
  /* Never limit the container height here */
  max-width: 100%;
}

.img-container img {
  /* This is important */
  width: 100%;
}
.cropper-bg {background-image:none !important;}
.cropper-modal {background-color:#EEEEEE !important;opacity:0 !important;}

</style>

<?php /*
<!--Reg Modal HTML -->
 <!--Model Start Preview -->
      <!--Voucher Offer Modal HTML -->
        <div id="voucher-offer-model" class="modal fade voucher-offer-model themeModel">
            <div class="modal-dialog sm-model mobile-offer-add">
                <div class="modal-content">
                    <div class="modal-body">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>                    
							<div class="vouch-offer">
								<div class="auto-bar">
								  <div class="mobile-main" >
									<div class="ng-header">
									<div class="container mobile-container">
									  <div class="row">
										<div class="col-sm-12"> 
											<span class="back_arrow"><a href="javascript:void(0);"><?php echo $this->Html->image("back-arrow.png",array("alt"=>"Voucher"));?></a></span>Offer Details <span></span>
										  <div class="hd-right">
											<ul class="list-inline">
											  <li><a href="javascript:void(0);"><?php echo $this->Html->image("fav-iocn.png",array("alt"=>"favorite"));?></a></li>
											<li><a href="javascript:void(0);"><?php echo $this->Html->image("share-iocn.png",array("alt"=>"Share"));?></a></li>
											</ul>
										  </div>
										</div>
									  </div>
									</div>
								  </div>	
								  <div  class="mobile_offer">
									  <img class="VoucherImg" src="<?php echo SITE_LINK."img/default_voucher.jpeg"?>"/>
								   </div>  
								  <section class="jusic-banner"> 	  
									<div class="cnt_middle">
									  <div class="cnt-contant">		 
										<div class="snu-profile">
											 
										  <div class="avtar-img">
											   <?php (h($userdetails["UserDetail"]["image"])!="")?$src=SITE_LINK."img/profile/".h($userdetails["UserDetail"]["image"]):
												  $src=SITE_LINK."img/default.jpeg"?>
												  <img src='<?php echo $src ;?>'>;
										  </div>
										  <h1 class="text-center"><?php echo h($userdetails['UserDetail']['business']); ?></h1>
										  <div class="row">
											<div class="snu-student">
											  <ul class="list-inline">
												<li>
												  <div class="banr-text"> <span class="disct"><div id="Voucherdiscount"></div></span>
													<p> Student <span>Discount</span></p>
												  </div>
												</li>
												<li>
												  <div class="banr-text">
													<div id="Vouchersubtitle" class="break-content">
												  </div>
												</li>
											  </ul>
											</div>
										  </div>
										  <span class="show-status"><div id="Voucherlftmsg"></div></span> </div>
										<div class="student-conditions">
										  <div class="blog-artiale">
											<div class="map-left"><iframe  class="map-img" src="https://www.google.com/maps?q=<?php echo $userdetails['UserDetail']['address'];?>&output=embed"></iframe></div>
											<div class="articale-right">
											  <p>Less than 0.5 miles away
											 <?php echo h($userdetails['UserDetail']['address']); ?></p>
											</div>
										  </div>
										  <div class="blog-artiale">
											<h2><?php echo $this->Html->image("about-help.png",array("alt"=>"About this discount"));?>About this discount</h2>
											<p><div id="Voucherdescription"></div>... </p><a href="javascript:void(0);">Read More</a> </div>
										  <div class="blog-artiale">
											<h2><?php echo $this->Html->image("terms-condition.png",array("alt"=>"Terms & Conditions"));?>Terms & Conditions</h2>
											<p><div id="Voucherterms"></div>...</p>
											<a href="javascript:void(0);" id="voucherlink">Read More</a> 
										</div>
									  </div>
									</div>
								  </section>
									<div class="mobile-footr">
										<div class="container mobile-container">
										  <div class="container mobile-container">
											<h2> <?php echo $this->Html->image("get-direcation.png",array("alt"=>"Get Directions"));?>Get Directions</h2>
										  </div>
										</div>
									</div>
								</div>
								</div>
							</div>   
                    </div>                   
                </div>
            </div>
        </div>  
		</div>
		</div>
      <!--Voucher Offer Modal HTML -->
*/ ?>


<!--Voucher Offer Modal HTML -->
  <div id="voucher-offer-model" class="modal fade voucher-offer-model themeModel">
    <div class="modal-dialog sm-model mobile-offer">
      <div class="modal-content">
        <div class="modal-body">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <div class="vouch-offer"> 
          	<span class="top-time">9:30</span>
          <?php echo $this->Html->image("mobile-offer.png"); ?>
          
            <div class="offer-banner"><?php echo $this->Html->image("jusic-banner.jpg",array("id"=>"VoucherImg")); ?> </div>
            <div class="auto-bar">
              <div class="mobile-main">
                <div class="ng-header">
                  <div class="container mobile-container">
                    <div class="row">
                      <div class="col-sm-12"> <span class="back_arrow"><a href="javascript:void(0);"><?php echo $this->Html->image("back-arrow.png"); ?></a></span>Offer Details <span></span>
                        <div class="hd-right">
                          <ul class="list-inline">
                            <li><a href="javascript:void(0);"><?php echo $this->Html->image("fav-iocn.png",array("alt"=>"favorite"));?></a></li>
							<li><a href="javascript:void(0);"><?php echo $this->Html->image("share-iocn.png",array("alt"=>"Share"));?></a></li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <section class="jusic-banner">
                  <div class="cnt_middle">
                    <div class="cnt-contant">
                      <div class="snu-profile">
                        <div class="avtar-img" style="background:#fff;"> <?php (h($userdetails["UserDetail"]["image"])!="")?$src=SITE_LINK."img/profile/".h($userdetails["UserDetail"]["image"]):
												  $src=SITE_LINK."img/default.jpeg"?>
												  <img src='<?php echo $src ;?>'></div>
                        <h1 class="text-center"><?php echo h($userdetails['UserDetail']['business']); ?></h1>
                        <div class="row">
                          <div class="snu-student">
                            <ul class="list-inline">
                              <li>
                                <div class="banr-text"> <span class="disct"><div id="Voucherdiscount"></div></span>
                                  <p> Student <span>Discount</span></p>
                                </div>
                              </li>
                              <li>
                               <div class="banr-text">
								<div id="Vouchersubtitle" class="break-content">
							   </div>
                              </li>
                            </ul>
                          </div>
                        </div>
                        <span class="show-status"><div id="Voucherlftmsg"></div></span> </div>
                      <div class="student-conditions">
                        <div class="blog-artiale">
                          <div class="map-left"> <iframe  class="map-img" src="https://www.google.com/maps?q=<?php echo $userdetails['UserDetail']['address'];?>&output=embed"></iframe> </div>
                          <div class="articale-right" style="float:right; width:68%;">
                            <p>Less than 0.5 miles away
                            <?php echo h($userdetails['UserDetail']['address']); ?></p>
                          </div>
                        </div>
                        <div class="blog-artiale">
                          <h2> <?php echo $this->Html->image("about-help.png",array("alt"=>"About this discount"));?>About this discount</h2>
                          <p><div id="Voucherdescription"></div></p>
                          <a href="javascript:void(0);">Read More</a> </div>
                        <div class="blog-artiale">
                          <h2> <?php echo $this->Html->image("terms-condition.png",array("alt"=>"Terms & Conditions"));?>Terms & Conditions</h2>
                          <p><div id="Voucherterms"></div></p>
                          <a href="javascript:void(0);">Read More</a> </div>
                      </div>
                    </div>
                  </div>
                </section>
                <div class="mobile-footr">
                  <div class="container mobile-container">
                    <div class="container mobile-container">
                      <h2> <?php echo $this->Html->image("get-direcation.png",array("alt"=>"Get Directions"));?> Get Directions</h2>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!--Voucher Offer Modal HTML -->



<div class="modal in fade themeModel" tabindex="-1" role="dialog" id="confirmaddvoucher">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Voucher Creation Confirmation</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>You have not completed your merchant profile yet, You still can publish your voucher but will not be displayed for students till you will complete your profile.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary btn-theme" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-secondary btn-theme confirmvoucheradd">Ok</button>
      </div>
    </div>
  </div>
</div>  
