<?php echo $this->Html->css(array("moola/bootstrap.min","fishook/style","internal"));?>
<div class="date-select">
   <table class="date-select-table" style="background-color:#1d2233">
      <thead>
         <tr>
            <th class="prev"><a href="javascript:void(0);" class="nextmonth" value="<?php echo $resultcal[0]['prevMonth']; ?>"><img src="img/prev.png" alt="Prev" title="Prev"></a></th>
            <th colspan="5" class="datepicker-switch"><?php echo $date ?></th>
            <th class="next"><a href="javascript:void(0);" class="nextmonth" value="<?php echo $resultcal[0]['nextMonth']; ?>" ><img src="img/next.png" alt="Next" title="Next"></a></th>
         </tr>
         <tr class="date-day">
            <th>S</th>
            <th>M</th>
            <th>T</th>
            <th>W</th>
            <th>T</th>
            <th>F</th>
            <th>S</th>
         </tr>
      </thead>
      <tbody>        
         <?php
      // pr($resultcal);
            $selectedDay = array();$m=0;
            $currDate = ($resultcal[0]['firstDay']); 
            foreach ( $eventArr as $key=>$val ) {
            	//pr($val);	
            	 $btw_start=date("Y-m-d", strtotime($val['eventdate']));
            	 $btw_end=date("Y-m-d", strtotime($val['eventdate']." +3 months"));
            		
            	$start = date_create($val['eventdate']);				
            	$t1 = strtotime($val['eventdate']);
            	$date = date("d",$t1);
            	for ($temp = 1; $temp <= $resultcal[0]['lastDay']; $temp++) {
            		$temp1 = str_pad($temp,2,0,STR_PAD_LEFT);
            		$tmpCurrDate = date_create(date("Y-m-".$temp1,strtotime($currDate)));
            		$t2temp = (date("Y-m-".$temp1,strtotime($currDate)));
            		$t2 = strtotime($t2temp);
            		$d2 = date("d",$t2);
            		$diff = date_diff($start,$tmpCurrDate);
            		$d = $diff->format("%R%a");
            		//echo "<br/>";
            		$d1 = $diff->format("%a");
            		 $btw_date=date("Y-m-d", strtotime($val['selected_month_btw'].'-'.$temp));
            		if ( $val['repeat_event'] == 1 && $t2 >= $t1 && ($val['eventdate']<= $btw_date && $btw_date <= $btw_end)) {
            			
            			$selectedDay[$temp]['date'] = $temp;
            			$selectedDay[$temp]['event_status'] = $val['event_status'];
            			$selectedDay[$temp]['current_month'] = $val['current_month'];
            			$selectedDay[$temp]['selected_month'] = $val['selected_month'];
            		
            		 } elseif ($val['repeat_event'] == 2 && $d >= 0 && ($d%7) == 0 && ($val['eventdate']<= $btw_date && $btw_date<= $btw_end) ) {
            			 $selectedDay[$temp]['date'] = $temp; 
            			$selectedDay[$temp]['event_status'] = $val['event_status'];
            			$selectedDay[$temp]['current_month'] = $val['current_month'];
            			$selectedDay[$temp]['selected_month'] = $val['selected_month'];		
            						
            		} elseif ($val['repeat_event'] == 3 && $d >= 0 && ($d%14) == 0 && ($val['eventdate']<= $btw_date && $btw_date<= $btw_end)) {
            			$selectedDay[$temp]['date'] = $temp;
            			$selectedDay[$temp]['event_status'] = $val['event_status'];
            			$selectedDay[$temp]['current_month'] = $val['current_month'];
            			$selectedDay[$temp]['selected_month'] = $val['selected_month'];		
            							
            		} elseif ($val['repeat_event'] == 4 && $d >= 0 && $date == $d2 && ($val['eventdate']<= $btw_date && $btw_date<= $btw_end)) {
            			
            			$selectedDay[$temp]['date'] = $temp;
            			$selectedDay[$temp]['event_status'] = $val['event_status'];
            			$selectedDay[$temp]['current_month'] = $val['current_month'];
            			$selectedDay[$temp]['selected_month'] = $val['selected_month'];		
            							
            		} elseif ($val['repeat_event'] == 0 && $date == $d2 && ($val['eventdate']<= $btw_date && $btw_date<= $btw_end)) {
            			//~ echo $date;
            			//~ echo "<br/>";
            			//~ echo $d2;
            			//die;
            			if(date("m",strtotime($val['eventdate']))==date("m",strtotime($val['selected_month_equal']))){
            			$selectedDay[$temp]['date'] = $temp;
            			$selectedDay[$temp]['event_status'] = $val['event_status'];
            			$selectedDay[$temp]['current_month'] = $val['current_month'];
            			$selectedDay[$temp]['selected_month'] = $val['selected_month'];
            				}					
            		}
            		if(!empty($selectedDay[$temp]) && $val['event_status'] && !isset($selectedDay[$temp]['is_live'])){
						$selectedDay[$temp]['is_live']=true;
					}
            		
            	}
            	//die;
            }
          // pr($selectedDay);
          // die;
          
            ?>
         <?php 
         $j=1; $h=0; 
         for ($i=1;$i<42;$i++) {
			if ($i == 1 || $i%7 == 1) { 
			 echo '<tr>';
			}
			$classList=['day'];
			if(isset($selectedDay[$j]["is_live"])){
				$classList[]=($selectedDay[$j]["date"] == date('d')) ? 'new' :'new';
			}
			
			if ( ($i >= $dayArr[$resultcal[0]['first_day']]) && $j <= $resultcal[0]['lastDay']) {$h=1;
				if($j==$sel_date){
						$classList[]="active";						
				}
				//~ if(isset($selectedDay[$j]["is_live"])){
					//~ if($j==date('d')){
							//~ $classList[]="today";
						//~ }
				//~ }
				
					 $check_today=date('Y')."-".str_pad($resultcal[0]["currentMonth"], 2, 0, STR_PAD_LEFT)."-".$j;
					if($check_today==date('Y-m-d') ){				
						if($j==$sel_date){			
							if(isset($selectedDay[$j]["is_live"])){
								$classList[]="active";
							}
							else
							{
								$classList[]="";
							}
						}
						else
						{
							$classList[]="today";
						}
					}
				
				if(isset($selectedDay[$j])){
				    $date=$selectedDay[$j]["date"];	//echo date("d");									
					if($date==date("d") && $selectedDay[$j]["event_status"]==0){
						if($selectedDay[$j]["selected_month"] < $selectedDay[$j]["current_month"]){
							$classList[]="old";	
						}
						else
						{
							if($date==date('d') && $sel_date==date('d'))	
							{
								if(isset($selectedDay[$j]["is_live"])){
								$classList[]="new active";
								}
								else {
								$classList[]="save active";
								}
							}
							else
							{
								$classList[]="save";
								//$classList[]="today save";
							}
						}					
					}else if($date < date("d") || $date > date("d")) {
						if($selectedDay[$j]["selected_month"] > $selectedDay[$j]["current_month"]){
							if(($selectedDay[$j]["event_status"]==0 || $selectedDay[$j]["event_status"]==1) && isset($selectedDay[$j]["is_live"])){
								$classList[]="new";
							}
							else
							{
									$classList[]="save";
							}
						}else if($selectedDay[$j]["selected_month"] < $selectedDay[$j]["current_month"]){
							$classList[]='old';
						}else if($selectedDay[$j]["selected_month"] == $selectedDay[$j]["current_month"]){							
							if($j > date("d")){
								if(($selectedDay[$j]["event_status"]==0 || $selectedDay[$j]["event_status"]==1) && isset($selectedDay[$j]["is_live"])){
								$classList[]="new";
								}
								else
								{
										$classList[]="save";
								}
							}
							else if($j < date("d")){
								if($selectedDay[$j]["event_status"]==0 || $selectedDay[$j]["event_status"]==1){
									$classList[]="old";
								}
							}
							else{
								$classList[]='old';
							}
						}else{
							$classList[]='old';
						}  
					}
				}else{ 
					
					$now_month=date("Y-m-",strtotime($currDate)).str_pad($j, 2, 0, STR_PAD_LEFT);
					
					if($now_month==date("Y-m-d")){
						$classList[]="today";
					}
					else
					{
						$classList=array();
						$classList[]="day";
						if($j==$sel_date){	
							
							if($set_yes=="no"){
								$classList[]="";								
							}
							else
							{
								$classList[]="active";
							}
						}
					}
				}
				$date=$currMonth.$j;
				$date=(date("Y-m-d",strtotime($date)));
				echo "<td  class='".implode(' ',$classList)."'><a  href='javascript:void(0)' action='".$date."' id=".$j."  class='show_myevent' value='".$date."'>".$j."</a>";				
				//show_myevent.php?date="
				echo"</td>";
				$j++;
			} else { 
				echo "<td class='day'>&nbsp;</td>";
				if($h!=1)
				 $m=$m+1;
			}
			if ($i%7 == 0) { 
			 echo '</tr>';
			} 
		} ?>	
		<?php echo $this->Form->input('extra_row',array("type"=>"hidden",'id'=>'extra_row','value'=>'')); ?>

		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
		<script>			
				$("#extra_row").val(<?php echo $m ?>);	
		</script>	
      </tbody>
      <tfoot>
         <tr>
            <th colspan="7" class="today" style="display: none;">Today</th>
         </tr>
         <tr>
            <th colspan="7" class="clear" style="display: none;">Clear</th>
         </tr>
      </tfoot>
   </table>
</div>
<script>
   //~ $('<iframe id="mycalendar"/>').load(function(){
   	//~ $(document).on('click', '.nextmonth', function(){	
   //~ var month=$(this).attr("value");		
   //~ var url_calendar="calendar?month="+month;	
   //~ $('#mycalendar').attr('src', url_calendar);
   //~ });	 
    //~ $('#mycalendar').contents().find('body').replaceWith('<script>$(function() {$(".nextmonth").on("click",function(){var month=$(this).attr("value"); var url_calendar="calendar?month="+month;$("#mycalendar").attr("src", url_calendar);}); })<\/script>');                             
   //~ 
   //~ }).appendTo("body");	
   
</script>


