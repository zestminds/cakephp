<link href="<?php echo SITE_LINK ?>datetime/bootstrap/css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
<link href="<?php echo SITE_LINK ?>css/app.css" rel="stylesheet" media="screen">
<link href="<?php echo SITE_LINK ?>css/bootstrap-tagsinput.css" rel="stylesheet" media="screen">
<?php //pr($this->request->data); //DIE;?>
<script>
var avatar = "<?php echo SITE_LINK."img/events/".trim($event['Calendar']['image']); ?>";
</script>
<?php $tmpImage = SITE_LINK."img/events/".trim($event['Calendar']['image']); ?>
<div class="cont-right">
  <div class="cont-right-innr">
		<div class="main-hd-in">
          <div class="row">
            <div class="col-sm-12">
              <h2 class="title-das"> Edit Event</h2>              
            </div>
          </div> 
		  
        </div>    
	<?php	//pr($event);die;?>
        <div class="manage-marchant">		
			<?php echo $this->Form->create('Calendar',array("type"=>"file","class"=>"themeForm","method"=>false)); ?>
					<?php echo $this->Form->input('id',array("type"=>"hidden",'id'=>'id',"value"=>$event['Calendar']['id'])); echo $this->Form->input('eventdate1',array("type"=>"hidden",'id'=>'eventdate1',"value"=>$event['Calendar']['eventdate'])); ?>
					<div class="section-two-column-right">					
						
						<div class="event-date-heading">
							<ul class="event-shadule">
								<li><a title="Prev" href="javascript:void(0)" title="Prev" id="prev" class="prenext"><?php echo $this->Html->image("prev.png",array("alt"=>"Prev","alt"=>"Prev"));?></a></li>
								<li><div id="date" class="datenext"><?php echo $event['Calendar']['eventdate']?></li>
								<?php echo $this->Form->input('eventdate',array("type"=>"hidden",'id'=>'eventdate',"value"=>$event['Calendar']['eventdate']));?>
								<li><a title="Next" href="javascript:void(0)" title="Next" id="next" class="prenext"><?php echo $this->Html->image("next.png",array("alt"=>"Next","alt"=>"Next"));?></a></li>
							</ul>
							<div class="event-show">
								<ul >
									<li class="event_show_comments"><a href="javascript:void(0)" data-toggle="modal" data-target="#myModal" class="btn showcomments" title="Comments">Comments (<?php echo $comments_count?>) <?php $this->html->image("right-arrow.png",array("alt"=>"Comments","title"=>"Comments"))?></a></li>
									<li>
										<?php if($event["Calendar"]["event_status"]==1) {?>
											<div class="btn hide-event" title="Show Event on Rave" id="showevent">Hide Event on Rave</div>
											
										<?php } else {?>
											<div class="btn" title="Show Event on Rave" id="showevent">Show Event on Rave</div>
											<?php }?>	
										
										<?php echo $this->Form->input('event_status',array("type"=>"hidden",'id'=>'event_status',"value"=>$event["Calendar"]["event_status"]));?>									
									</li>
								</ul>
							</div>
						</div>
						<div class="gray-border"></div>
						<div class="past-event red-bg" style="display:none">You must fill out the highlighted fields before showing this event on Rave</div>
						<div class="past-event gray-bg" style="display:none">
							This is a Past Event
						</div>
						<div class="live-event" style="display:none">
							This is a Live Event
						</div>
						<ul class="event-noty">
							<li><a href="javascript:void(0)" title="Comments"><?php echo $comments_count?> <small>Comments</small></a></li>
							<li><a href="javascript:void(0)" title="Views"><?php echo $event_viewed?> <small>Views</small></a></li>
							<li><a href="javascript:void(0)" title="Saved to Calendar"><?php echo $save_calendar?> <small>Saved to Calendar</small></a></li>
						</ul>
						<div class="event-form row">	
							<div class="event-form-left col-md-6">
								<div class="form-field">
									<div class="form-heading">
										<?php echo $this->Html->image("icon1.png",array("alt"=>"Upload Image","title"=>"Upload Image"));?>	Image										
									</div>
									<div class="image-preview">
										<div class="file-upload">
												<div class="upload-drop-zone" id="drop-zone"> 
														   <div class="upload-file"> 
															   <?php echo $this->Html->image("events/".$event['Calendar']['image'],array("alt"=>"Voucher Pic","id"=>"img-upload")); 
															   
															    echo $this->Form->input("tmpImage",array("label"=>false,"type"=>"textarea","style"=>"display:none;","id"=>"tmpImage","value"=>$event['Calendar']['image'])); 
															   
															   echo $this->Form->input('oldImage',array("type"=>"hidden",'id'=>'oldImage',"value"=>$event['Calendar']['image']));
															   
															   ?>
														  </div>	
												</div>			
										</div>
									</div>	
								</div>

								<div class="form-field">
									<div class="form-heading">
										<?php echo $this->Html->image("icon2.png",array("title"=>"What’s happening? (event title)","alt"=>"What’s happening? (event title)"));?>
										What’s happening? (event title)
									</div>
								 <?php echo $this->Form->input('title',array("label"=>false,"type"=>"text",'id'=>'title','placeholder'=>"Answer","class"=>"form-control","value"=>$event['Calendar']['title']));?>
								</div>

								<div class="form-field">
									<div class="form-heading">
										<?php echo $this->Html->image("icon3.png",array("title"=>"Short Description","alt"=>"Short Description"));?>
									Short Description
									</div>
									<?php echo $this->Form->input('short_dec',array("type"=>"textarea",'id'=>'short_dec',"placeholder"=>"","max-lenght"=>140,"class"=>"form-control","label"=>false,'placeholder'=>"Answer","value"=>$event['Calendar']['short_dec']));	                       	 
									 ?>
									
								</div>

								<div class="form-field">
									<div class="form-heading">
										<?php echo $this->Html->image("icon4.png",array("title"=>"Long Description","alt"=>"Long Description"));?>
										 Long Description
									</div>
								   <?php echo $this->Form->input('long_dec',array('id'=>'long_dec','label' => false,"max-lenght"=>800,"class"=>"form-control long-dis","type"=>"textarea",'placeholder'=>"Answer","value"=>$event['Calendar']['long_dec'])); ?>
									
								</div>

								<div class="form-field">
									<div class="form-heading">
										<?php echo $this->Html->image("icon2.png",array("title"=>"Hashtag","alt"=>"Hashtag"));?>
										 Hashtag
									</div>
									<?php echo $this->Form->input('hashtags',array("label"=>false,"class"=>"form-control","placeholder"=>"Type in your hashtags","value"=>''));?>								  
									<div>
									<ul class="tags">
									<?php	
										if(empty($event['Calendar']['tag_list'])){}
										else{ ?>
										
										<?php
										foreach ($event['Calendar']['tag_list'] as $tags) {?>
										<li><a href="javascript:void(0)"><?php echo $tags ?> </a><button type='button' class='delete_button' value="<?php echo $tags ?>"><img src='<?php echo SITE_LINK?>img/cros-icon.png' alt=''/></button>    
										</li>
                                   
										<?php }?>
                                    
                                     <?php }?> 
									
									</ul>
									</div>
									<?php echo $this->Form->input('tags',array("value"=>$event['Calendar']['tags'],"label"=>false,"type"=>"hidden"));?>
									
								</div>

							</div>
							<div class="event-form-right col-md-6">
								<div class="form-field">
									<div class="form-heading">
										<?php echo $this->Html->image("icon2.png",array("title"=>"Location","alt"=>"Location"));?>
										Location
									</div>
									<div id="pac-container">                   	 
										 <?php 		                   	 
										  echo $this->Form->input('location',array("id"=>"pac-input","class"=>"form-control","type"=>"text","label"=>false,"placeholder"=>"Answer","value"=>$event['Calendar']['location']));
										  ?>
								  </div>
									<div class="map-field">										
										 <div class="map-frame">																			
											 <div id="map" style="height:200px;width:433px;"></div>
											 <div id="infowindow-content">							 
											  <span id="place-name"  class="title"></span>
											  <span id="place-address"></span>
											 </div>		
										 </div>
									</div>
									<?php echo $this->Form->hidden('latitude',array("value"=>$event['Calendar']['latitude'])); echo $this->Form->hidden('longitude',array("value"=>$event['Calendar']['longitude']));?>			
										
								</div>

								<div class="form-field">
									<div class="form-heading">
										<?php echo $this->Html->image("icon5.png",array("title"=>"How much?","alt"=>"How much?"));?>										 How much? 
									</div>
									<div class="row">
										<div class="col-md-6">
											  <?php echo $this->Form->input('start_price',array("label"=>false,"type"=>"text",'autofill'=>'false',"class"=>"form-control","placeholder"=>"From","value"=>$event['Calendar']['start_price']));?>    
										</div>
										<div class="col-md-6">
											  <?php echo $this->Form->input('end_price',array("label"=>false,"type"=>"text",'autofill'=>'false',"class"=>"form-control","placeholder"=>"To","value"=>$event['Calendar']['end_price']));?>   
										</div>
									</div>
									<div class="checkbox-style" style="float:left">
										
										 <?php echo $this->Form->input('ticket_free',array("class"=>"regular-checkbox","label"=>false,"type"=>"checkbox", "autocomplete"=>"off", "id"=>"ticket_free","div"=>false ))?><label for="ticket_free"></label> 											
									</div>
									<div class="free_tick">It’s Free</div>
								</div>

								<div class="form-field">
									<div class="form-heading">
										<?php echo $this->Html->image("icon6.png",array("title"=>"What Time?","alt"=>"What Time?"));?>										What Time?
									</div>
									<div class="row">
										<div class="col-md-6">
											<div class='input_date_grp input-group date datetimepicker2'>
											  <?php echo $this->Form->input('start_hour',array("label"=>false,"type"=>"text",'autofill'=>'false',"class"=>"form-control","placeholder"=>"From","value"=>$event['Calendar']['start_hour']));?>                
											  <span class="input-group-addon">
												<li class="fa fa-clock-o"></li>
											  </span>
											</div>		
										</div>
										<div class="col-md-6">
											<div class="grp_date">   
												<div class='input_date_grp input-group date datetimepicker2'>
												  <?php echo $this->Form->input('end_hour',array("label"=>false,"type"=>"text",'autofill'=>'false',"class"=>"form-control","placeholder"=>"To","value"=>$event['Calendar']['end_hour']));?>                
												  <span class="input-group-addon">
													<span class="fa fa-clock-o"></span>
												  </span>
												</div>	
											</div>	
										</div>
									</div>
								</div>

								<div class="form-field">
									<div class="form-heading">
										<?php echo $this->Html->image("icon2.png",array("title"=>"Is this event repeated?","alt"=>"Is this event repeated?"));?>
										 Is this event repeated?
									</div>									
									<?php 		                       	 
									  echo $this->Form->input('repeat_event',array("value"=>$event['Calendar']['repeat_event'],"class"=>"form-control","type"=>"select","label"=>false,"options"=>$optionsRepeat));
									  ?>
								</div>

								<div class="form-field">
									<div class="form-heading">
										<?php echo $this->Html->image("icon2.png",array("title"=>"Ticket Link","alt"=>"Ticket Link"));?>
										Ticket Link
									</div>
									<?php 		                       	 
									  echo $this->Form->input('ticket_link',array("class"=>"form-control","type"=>"text","label"=>false,"placeholder"=>"Answer","value"=>$event['Calendar']['ticket_link']));
									  ?>
									</div>
							</div>
						
						</div>
						<div class="event-submit text-right">
							<input type="submit" class="btn btn-theme" value="Save Changes">
							<a href="<?php echo SITE_LINK ?>view_events" ><input type="button" class="btn btn-theme"  value="Cancel"></a>							
						</div>
					</div>
					</div>
					<?php echo $this->Form->end(); ?>
		</div>
 </div>		
</div>
<!-- Modal -->
							<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
							  <div class="modal-dialog" role="document">
							    <div class="modal-content">
							      	<div class="modal-header">
							        	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><img src="<?php echo SITE_LINK?>img/close-pop.png" alt="Close" title="Close"></button>
							        	<h4 class="modal-title" id="myModalLabel">"<span class="title_name"></span>" Comments (<?php echo $comments_count ?>)</h4>
							      	</div>
							      	<div class="modal-body">
								    	<ul class="pop-comment">
											<?php if(empty($callList)) {?>
												No Comment to display
											<?php } else {
													foreach($callList as $comment){
												?>						    		
								    		<li><img src="<?php echo $comment["image"]?>" alt="<?php echo $comment["business"]?>" title="<?php echo $comment["business"]?>"><h3><?php echo $comment["business"]?></h3><p><?php echo $comment["created"]?></p><p><?php echo $comment["comment"]?></p></li>
								    		<?php } }?>
								    	</ul>
								      </div>
							    </div>
							  </div>
							</div>
<style>
.selectUnitemp{background-image:none !important;}
.head{font-size:20px;font-weight:bold;color:#444;margin-bottom:10px;}
.preview_voucher {padding:6px 20px;border:1px solid #444;margin-top:10px;}
.linkvoucher {color:#45ccf0 !important}
.linkvoucher:hover{color:#45ccf0 !important}
button.close {
	-webkit-appearance: none;
	/* padding: 0; */
	/* cursor: pointer; */
	background: 0 0!important;
	border: 0!important;
	color: #000!important;
}
#map {
        height: 100px;  /* The height is 400 pixels */
        width:400px;  /* The width is the width of the web page */
}
div#imageModal{top:0px !important;}

.imageBox{height:470px !important; width:470px !important;}
.thumbBox{border-radius : 0px !important; height:270px !important; width:270px !important; top:42% !important; left:42% !important;}
#drop-zone{cursor:pointer;}

</style>   
<div class="modal fade loginModel themeModel" id="imageModal" tabindex="-1" role="dialog" aria-labelledby="imageModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
       <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title">Select OR Drag Event Image OR Paste Image(ctrl+v)</h4>
      </div>
      <div class="modal-body" id="actions">
        <div class="selct-img-voc">
		  <div class="img-container" style="width:100%;">
				<img src="<?php echo $tmpImage; ?>" alt="Picture" id="tmpImage_popup">
			</div>
		  <div class="iviewer" >
			 <ul class="custom-file-upload" class="docs-buttons">
				 <li><label for="upload-file" class="custom-file-upload"><span ><?php echo $this->Html->image("gallery-img.png",array("alt"=>"Voucher Pic")); ?></span></label> <input id="upload-file" type="file" name="file" accept="image/*"/></li>
				<li data-method="rotate" data-option="-90"><a href="javascript:void(0)" title="Rotate Left"><span id="rotateLeft"><?php echo $this->Html->image("rotate-left.png",array("alt"=>"Voucher Pic")); ?></span></a></li>
				 <li data-method="rotate" data-option="90"><a href="javascript:void(0)" title="Rotate Right"><span id="rotateRight"><?php echo $this->Html->image("rotate-right.png",array("alt"=>"Voucher Pic")); ?></span></a></li>
				 <li data-method="zoom" data-option="-0.1"><a href="javascript:void(0)" title="Zoom Out"><span id="zoomIn"><?php echo $this->Html->image("zoom-out.png",array("alt"=>"Voucher Pic")); ?></span></a></li>
				  <li data-method="zoom" data-option="0.1"><a href="javascript:void(0)" title="Zoom In"><span id="zoomOut"><?php echo $this->Html->image("zoom-in.png",array("alt"=>"Voucher Pic")); ?></span></a></li>
			 </ul>             
		  </div>
		</div>
        
		<div class="iviewer-btn">
			<ul class="list-inline">
			   <li><button class="btn btn-custom" data-dismiss="modal">CLOSE</button></li> 
			   <li><button class="btn btn-custom btn-primary" data-method="getCroppedCanvas" data-option="{ &quot;maxWidth&quot;: 4096, &quot;maxHeight&quot;: 4096 }" id="saveimage">SAVE CHANGES</button></li>
			</ul>
		</div>
	 </div>  	
    </div>
  </div>
</div>
<div class="modal in fade themeModel" tabindex="-1" role="dialog" id="cnfrmdelid">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Event Delete Confirmation</h5>       
      </div>
      <div class="modal-body">
        <p>Do you really want to remove this Event? If you delete this event, it will be lost forever.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary btn-theme" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-secondary btn-theme delvoucher">Ok</button>
      </div>
    </div>
  </div>
</div>

<style>
.img-container {
  /* Never limit the container height here */
  max-width: 100%;
}

.img-container img {
  /* This is important */
  width: 100%;
}
.cropper-bg {background-image:none !important;}
.cropper-modal {background-color:#EEEEEE !important;opacity:0 !important;}

</style>
 <script>
      // This example requires the Places library. Include the libraries=places
      // parameter when you first load the API. For example:
      // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">
	
	  				 
	  
      function initMap() {
		
		var lati = parseFloat($('#CalendarLatitude').val());
		var longi = parseFloat($('#CalendarLongitude').val());
		
		if(isNaN(lati) || lati=="")
		{
		  lati=53.4807593; longi=-2.2426305000000184;
		}		
        var map = new google.maps.Map(document.getElementById('map'), {
          center: {lat:lati, lng: longi},
          zoom: 17
        });
        var card = document.getElementById('pac-card');
        var input = document.getElementById('pac-input');
        var types = document.getElementById('type-selector');
        var strictBounds = document.getElementById('strict-bounds-selector');

        map.controls[google.maps.ControlPosition.TOP_RIGHT].push(card);

        var autocomplete = new google.maps.places.Autocomplete(input);

        // Bind the map's bounds (viewport) property to the autocomplete object,
        // so that the autocomplete requests use the current map bounds for the
        // bounds option in the request.
        autocomplete.bindTo('bounds', map);
		
        var infowindow = new google.maps.InfoWindow();
        var infowindowContent = document.getElementById('infowindow-content');
        infowindow.setContent(infowindowContent);
        var marker = new google.maps.Marker({
		 position: new google.maps.LatLng( lati,longi), 
          map: map,
          draggable: false  
        });
			

          autocomplete.addListener('place_changed', function() {
			 
          infowindow.close();
          marker.setVisible(false);
          var place = autocomplete.getPlace();
          if (!place.geometry) {
            // User entered the name of a Place that was not suggested and
            // pressed the Enter key, or the Place Details request failed.
            //window.alert("No details available for input: '" + place.name + "'");
            return;
          }

          // If the place has a geometry, then present it on a map.
          if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
            var address =place.name;
            var geocoder = new google.maps.Geocoder();
            geocoder.geocode({ 'address': address}, function (results, status) {
                        if (status == google.maps.GeocoderStatus.OK) {
							var latitude = place.geometry.location.lat();
                            var longitude = place.geometry.location.lng();
                            //alert(latitude); alert(longitude);
                            $('#CalendarLatitude').val(latitude);
                            $('#CalendarLongitude').val(longitude);
                        } else {
                           
                        }
                    });
            
          } else {
			 
            map.setCenter(place.geometry.location);
            map.setZoom(17);  // Why 17? Because it looks good.
          }
          marker.setPosition(place.geometry.location);
          marker.setVisible(true);

          var address = '';
          if (place.address_components) {
            address = [
              (place.address_components[0] && place.address_components[0].short_name || ''),
              (place.address_components[1] && place.address_components[1].short_name || ''),
              (place.address_components[2] && place.address_components[2].short_name || '')
            ].join(' ');
          }       
         // infowindowContent.children['place-icon'].src = place.icon;
          infowindowContent.children['place-name'].textContent = place.name;
          infowindowContent.children['place-address'].textContent = address;         
          	
          	infowindow.setContent(address);
		  infowindow.open(map, marker);
        }    
        );	
        
        google.maps.event.addListener(marker, 'click', (function(marker) {
                return function() {
                    var newaddress=$("#pac-input").val();
					infowindow.setContent(newaddress);
                    infowindow.open(map, marker);
                     map.setZoom(17); 
                }
            })(marker));
       
	}	  
	
	
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDcBcY5ObGmiZ6TETlFS1cEOi5ttvBRnuw&libraries=places&callback=initMap" async defer></script>



