<style>
ul.list{float:left;width:100%;list-style:none;}
ul.list li{float:left;width:25%;}
ul.list li  img{width:250px;height:200px}
</style>
<div class="vouchers index">
	<div class="rhs_actions right">
			<a href="<?php echo SITE_LINK."publish-vouchers"; ?>">Publish</a>			
		</div>
	<fieldset>
		<legend><?php echo __('List Vouchers'); ?></legend>
	<h2><?php echo __('Draft'); ?></h2>
	<ul class="list">
	<?php foreach ($vouchers as $voucher){ ?>
		<li>
			<?php echo "<img src='".SITE_LINK."img/voucher/".h($voucher['Voucher']['image'])."'>"; ?>
			<br><a href="<?php echo SITE_LINK?>edit-voucher/<?php  echo $voucher['Voucher']['id'] ?>">Edit</a>
			<a href="<?php echo SITE_LINK?>delete-voucher/<?php  echo $voucher['Voucher']['id'] ?>">Delete</a>
			<a href="copy-voucher/<?php  echo $voucher['Voucher']['id'] ?>">Copy</a><br>
			<a href="/View-voucher/<?php  echo $voucher['Voucher']['id'] ?>">View</a>
			<a href="/perview-voucher/<?php  echo $voucher['Voucher']['id'] ?>">Perview</a>
			<a href="<?php echo SITE_LINK?>publish-voucher/<?php  echo $voucher['Voucher']['id'] ?>">Publish</a>
		<?php echo $this->Form->postLink(__('Publish'), array('action' => 'publish', $voucher['Voucher']['id']), array('confirm' => __('Are you sure you want to Publish # %s?', $voucher['Voucher']['title']))); ?>
			<?php //echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $voucher['Voucher']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $voucher['Voucher']['id']))); ?>
		</li>
	<?php } ?>
	</ul>
	</fieldset>
	<?php /*
	<table cellpadding="0" cellspacing="0">	
	<tbody>
	<?php foreach ($vouchers as $voucher): ?>
	<tr>
		
		<td><?php echo h($voucher['Voucher']['image']); ?>&nbsp;</td>		
		<td><?php echo h($voucher['Voucher']['title']); ?>&nbsp;</td>
		<td><?php echo h($voucher['Voucher']['sub_title']); ?>&nbsp;</td>	
		<td><?php echo h($voucher['Voucher']['start_date']); ?>&nbsp;</td>
		<td><?php echo h($voucher['Voucher']['end_date']); ?>&nbsp;</td>
		<td><?php echo h($voucher['Voucher']['is_active']); ?>&nbsp;</td>		
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $voucher['Voucher']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $voucher['Voucher']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $voucher['Voucher']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $voucher['Voucher']['id']))); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</tbody>
	</table>
	<p>
	<?php */
	echo $this->Paginator->counter(array(
		'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
