<style>
.view
{
	border:1px solid #444;margin-top:10px;
}
.head{font-size:20px;font-weight:bold;color:#444;margin-bottom:10px;}
.view div{padding:6px 20px}
</style>
<div class="cont-right">
   <div class="cont-right-innr">
		<div class="vouchers">
		<h2><?php echo __('Voucher'); ?></h2>
			<div class="view"  >	
				<div  class="head"><b><?php echo h($voucher['Voucher']['title']); ?></b></div>		
				<div>
					<?php echo "<img width=250 height=200 src='".SITE_LINK."img/voucher/".h($voucher['Voucher']['image'])."'>" ?>
					&nbsp;
				</div>					
				<div><?php echo h($voucher['Voucher']['title_discount']); ?></div>
				<div><?php echo h($voucher['Voucher']['sub_title']); ?></div>
				<div>&nbsp;</div>
				<div style="font-weight:bold">About this discount</div>
				<div><?php echo h($voucher['Voucher']['description']); ?></div>
				<div>&nbsp;</div>
				<div style="font-weight:bold"><?php echo __('Terms and Conditions'); ?></div>
				<div><?php echo h($voucher['Voucher']['terms']); ?><a href="<?php echo h($voucher['Voucher']['terms_url']); ?>" target="_blank">Here</a></div>
				<div></div>
				<div style="font-weight:bold"><?php echo __('Time	'); ?></div>	
				<div>Start:&nbsp;&nbsp;<?php echo h($voucher['Voucher']['start_date']); ?>@<?php echo h($voucher['Voucher']['start_hour'])?></div>		
				<div>End:&nbsp;&nbsp;<?php echo h($voucher['Voucher']['end_date']); ?>@<?php echo h($voucher['Voucher']['end_hour']) ?></div>
				<div>Repeat:&nbsp;&nbsp;<?php echo ($voucher['Voucher']['repeat_on']=='W')? 'Weekly' :'Daily'; ?></div>
				<?php if($voucher['Voucher']['repeat_on']=='W') { ?>
				<div>
					<ul>
						<?php foreach($dayIntArr as $key=>$val) { ?>
							<li><?php echo $dayArr[$val]; ?></li>
						<?php } ?>
					</ul>
				</div>
				<?php } ?>
				<div style="font-weight:bold"><?php echo __('Tags'); ?></div>		
				<div><?php echo h($voucher['Voucher']['tags']); ?></div>
				<div><?php //echo str_replace(' ',' #',h($voucher['Voucher']['tags'])); ?></div>
			</div>
		</div>
	</div>	
</div>
