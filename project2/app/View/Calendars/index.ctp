<!--
 <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css">
<link rel="stylesheet" href="<?php echo SITE_LINK?>css/jquery.mentions.css">

<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<script src="<?php echo SITE_LINK?>js/jquery.mentions.js"></script>
-->
<link href='<?php echo SITE_LINK?>css/comment/style.css' rel='stylesheet' type='text/css'>
<link href='<?php echo SITE_LINK?>css/comment/jquery.mentionsInput.css' rel='stylesheet' type='text/css'>

<script src='https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min.js' type='text/javascript'></script>

<script type="text/javascript">
	var avatar = "<?php echo SITE_LINK."img/img-icon1.png"; ?>";
	
</script>
<?php
$tmpImage = SITE_LINK."img/img-icon1.png";

//echo $tmpImage = file_get_contents($tmpImage); die;
 ?>
<div id="loading">
    <p><img src="<?php SITE_LINK?>img/loading.gif" /></p>
</div> 
<section class="section">
			<div class="container">
				<div class="section-two-row">
					<div class="section-two-column-left">
						<iframe id="mycalendar" onload="loadiframe(this)" class="mycalendar"  frameborder="0" src="<?php echo SITE_LINK?>calendar" style="height:356px;width:100%;margin-bottom: 15px;"></iframe>

						<div class="sidebar-drop-down">
							<div id="drop_tags">
								<select id="searchTag" name="searchTag">
									<option value="">Serach by tag</option>
									<option value="all">All</option>
									
									<?php foreach($tag_list as $key=>$tag){?>
										
									<option value="<?php echo $tag['id'];?>"><?php echo $tag['tag'];?></option>
									<?php } ?>
								</select>
							</div>
							<span class="select-result">
								<?php echo $total_events;?>
							</span>
						</div>
						<ul class="event-list">
							<?php if(empty($callist)) {?>
								
									<li class="white">No event to display</li>
							<?php } else{
								
							foreach($callist as $list)	{
							
							$current_month=date("Y-m-d");
							$selected_month=date("Y-m",strtotime($list['Calendar']['eventdate']));
							$selected_month=$selected_month."-".date("d");	
							
							if($list['Calendar']['eventdate'] < date("Y-m-d")){
								if($selected_month==$current_month)
								{
									if($list['Calendar']['event_status'] == 1)
									{
										$class="event-new" ;
									}
									else
									{
										$class="event-today" ;
									}
								}
								else if($selected_month < $current_month)
								{
									$class="event-old" ;
								}
								 else{
								 if($list['Calendar']['event_status'] == 1)
									{
										$class="event-new" ;
									}
									else
									{
										$class="event-today" ;
									}
								}
							} 
							else if($list['Calendar']['eventdate'] == date("Y-m-d") || $list['Calendar']['eventdate'] > date("Y-m-d"))
							{ 
								if($list['Calendar']['event_status'] ==1 )
								 {
									 $class="event-new";								 
								 } else
								 { 
									 $class="event-today";
								 }
							 }  						
							else { $class="event-new";}
							?>									
							<li class="<?php echo $class; ?>">
								<div class="event-list-left">
									<?php 
									//echo $list['Calendar']['image'];
									 if(empty($list['Calendar']['image']) || !file_exists(WWW_ROOT."img/events/".$list['Calendar']['image'])){$event_image="default_event.png";}
									 else{$event_image= "events/".$list['Calendar']['image'];}

									echo $this->Html->image($event_image,array("alt"=>"image","title"=>"image"));?>
								</div>
								<div class="event-list-right">
									<a href="javascript:void(0);" tooltip="tooltip" title="Delete" class="close-event confirmdelete" action="<?php echo SITE_LINK."delete_event?id=".$list['Calendar']['id'] ?>"><?php echo $this->Html->image('close.png', array('alt' => 'Delete','title' => 'Delete')); ?></a>
									<a href="javascript:void(0);" class="editevent" value="<?php echo $list['Calendar']['id'] ?>">
									<h2><?php echo (!empty($list['Calendar']['title']))? $list['Calendar']['title'] : "Title Here" ; ?></h2>
									<p><?php  $start_hour =  date_create(h($list['Calendar']['start_hour'])); echo date_format($start_hour,"h:i a");  ?></p>
									</a>
								</div>
							</li>	
							<?php } } ?>						
						</ul>
					</div>
					<?php echo $this->Form->create('Calendar',array("type"=>"file","class"=>"themeForm","method"=>false,'novalidate' => true)); ?>
					 <?php echo $this->Form->input('id',array("type"=>"hidden",'id'=>'id'));echo $this->Form->input('mention_id',array("type"=>"hidden",'id'=>'mention_id'));  echo $this->Form->input('event_live_id',array("type"=>"hidden",'id'=>'event_live_id',"value"=>isset($event_live_id) ?$event_live_id:'')); echo $this->Form->input('oldImage',array("type"=>"hidden",'id'=>'oldImage'));?>
					<div class="section-two-column-right">
						<div class="popup-event upgrade-pop" style="display:none">
							<div class="popup-event-box">
								<div class="popup-event-box-header">
									<img src="img/rave.png" alt="Rave" title="Rave" class="home_logo">
								</div>
								<div class="popup-event-box-content">
									<h2>You have reached your events<br> limit for this week.</h2>
									<p>For 7 posts a week, upgrade to our <br>premium plan for just £39.99 /mo</p>
									<p><a href="javascript:void(0)" title="Upgrade Now" class="btn btn-upgrade upgrade_now">Upgrade Now</a></p>
									<a href="javascript:void(0)" class="close_upgrade" title="Maybe later">Maybe later</a>
								</div>
							</div>
						</div>
						<div class="popup-event upgrade-pop-day" style="display:none">
							<div class="popup-event-box">
								<div class="popup-event-box-header">
									<img src="img/rave.png" alt="Rave" title="Rave" class="home_logo">
								</div>
								<div class="popup-event-box-content">
									<h2>Sorry, you can only have one active post per day</h2>
									
								</div>
							</div>
						</div>
						<div class="popup-event unsuitable" style="display:none">
							<div class="popup-event-box">
								<div class="popup-event-box-header">
									<img src="img/rave.png" alt="Rave" title="Rave" class="home_logo">
								</div>
								<div class="popup-event-box-content">
									<h2>This event has been reported as unsuitable by Rave users and is now de-activated.</h2>
									<p>If you have any questions, <br>please email us on:</p>
									<a href="mailto:support@rave.com" title="support@rave.com">support@rave.com</a>
								</div>
							</div>
						</div>
						<div class="event-date-heading">
							<ul class="event-shadule">
								<li><a title="Prev" href="javascript:void(0);" title="Prev" id="prev" class="prenext"><?php echo $this->Html->image("prev.png",array("alt"=>"Prev","alt"=>"Prev"));?></a></li>
								<li><div id="date" class="datenext"></li>
								<?php echo $this->Form->input('tmpdate',array("type"=>"hidden",'id'=>'tmpdate')); echo $this->Form->input('eventdate',array("type"=>"hidden",'id'=>'eventdate'));?>
								<li><a title="Next" href="javascript:void(0);" title="Next" id="next" class="prenext"><?php echo $this->Html->image("next.png",array("alt"=>"Next","alt"=>"Next"));?></a></li>
							</ul>
							
							<div class="event-show">
								<ul >
									<?php //if(isset($user["User"]["user_type_id"]) && $user["User"]["user_type_id"]==4) { ?>
									<li><a href="javascript:void(0);" title="Add event" class="plus-add"><img src="<?php echo SITE_LINK;?>/img/plus.png" alt=""></a></li>
									<?php //} ?>
									<li style="display:none" class="event_show_comments"><a href="javascript:void(0);" data-toggle="modal" data-target="#myModal" class="btn showcomments" title="Comments">Comments (<span class="totalcomments"></span>) <?php $this->html->image("right-arrow.png",array("alt"=>"Comments","title"=>"Comments"))?></a>
									<div class="cart_total_items" style="display:none">0</div>
									</li>
									
									
								</ul>								
								
							</div>
						</div>
						<div class="gray-border"></div>
						<div class="past-event red-bg" style="display:none">You must fill out the highlighted fields before showing this event on Rave</div>
						<div class="past-event gray-bg" style="display:none">
							This is a Past Event
						</div>
						<div class="past-event nolive-bg" style="display:none">
							
						</div>
						<div class="past-event live-event" style="display:none">
							
						</div>
						<ul class="event-noty" style="display:none">
<!--
							<li><a href="javascript:void(0)" title="Comments"><span class="totalcomments"></span> <small>Comments</small></a></li>
-->
							<li><span id="totaleventview"></span> <small>Views</small></li>
							<li><span id="totalsavecalendar"></span> <small>Saved to Calendar</small></li>
						</ul>
						<div class="event-form row">	
							<div class="event-form-left col-md-6">
								<div class="form-field">
									<div class="form-heading">
										<?php echo $this->Html->image("image.png",array("alt"=>"Upload Image","title"=>"Upload Image"));?>	Image										
									</div>
									
									<div class="image-preview">
										<div class="file-upload">
												<div class="upload-drop-zone" id="drop-zone"> 
														   <div class="upload-file"> 
															   <?php echo $this->Html->image("img-icon1.png",array("alt"=>"Event Image","id"=>"img-upload")); ?>
															   <?php echo $this->Form->input("tmpImage",array("label"=>false,"type"=>"textarea","style"=>"display:none;","id"=>"tmpImage")); ?>
														  </div>	
												</div>			
										</div>
									</div>										
								</div>

								<div class="form-field">
									<div class="form-heading">
										<?php echo $this->Html->image("event.png",array("title"=>"What’s happening? (event title)","alt"=>"What’s happening? (event title)"));?>
										What’s happening?
									</div>
								 <?php echo $this->Form->input('title',array("type"=>"text",'id'=>'title','label' => ['text' => '','class' => 'lable-control'],'placeholder'=>"Event title","class"=>"form-control"));?>
								</div>

								<div class="form-field">
									<div class="form-heading">
										<?php echo $this->Html->image("short.png",array("title"=>"Short Description","alt"=>"Short Description"));?>
									Short Description
									</div>
									<?php echo $this->Form->input('short_dec',array("type"=>"textarea",'id'=>'short_dec',"placeholder"=>"As short as a tweet...","max-lenght"=>140,"class"=>"form-control","label"=>false));	                       	 
									 ?>
									
								</div>

								<div class="form-field">
									<div class="form-heading">
										<?php echo $this->Html->image("long.png",array("title"=>"Long Description","alt"=>"Long Description"));?>
										 Long Description
									</div>
								   <?php echo $this->Form->input('long_dec',array('id'=>'long_dec','label' => false,"max-lenght"=>800,"class"=>"form-control long-dis","type"=>"textarea",'placeholder'=>"Tell us everything...")); ?>
									
								</div>

								<div class="form-field">
									<div class="form-heading">
										<?php echo $this->Html->image("hashtag.png",array("title"=>"Hashtag","alt"=>"Hashtag"));?>
										 Hashtag
									</div>
									<?php 		                       	 
									 // echo $this->Form->input('tags',array("class"=>"form-control","type"=>"hidden","label"=>false));
									  ?>
									  <?php 		                       	 
									  echo $this->Form->input('tags',array("class"=>"form-control","type"=>"select","label"=>false,"options"=>$hashtags));
									  ?>
								</div>

							</div>
							<div class="event-form-right col-md-6">
								<div class="form-field">
									<div class="form-heading">
										<?php echo $this->Html->image("location1.png",array("title"=>"Location","alt"=>"Location"));?>
										Location
									</div>
									<div id="pac-container">                   	 
										 <?php 		                   	 
										  echo $this->Form->input('location',array("id"=>"pac-input","class"=>"form-control","type"=>"text","label"=>false,"placeholder"=>"Answer","value"=>$address));
										  ?>
								  </div>
									<div class="map-field">										
										 <div class="map-frame">																			
											 <div id="map" style="height:200px;width:306px;"></div>
											 <div id="infowindow-content">							 
											  <span id="place-name"  class="title"></span>
											  <span id="place-address"></span>
											 </div>		
										 </div>
									</div>
									<?php echo $this->Form->hidden('latitude',array("value"=>$latitude)); echo $this->Form->hidden('longitude',array("value"=>$longitude));?>			
										
								</div>

								<div class="form-field">
									<div class="form-heading">
										<?php echo $this->Html->image("doller.png",array("title"=>"How much?","alt"=>"How much?"));?>										 How much? 
									</div>
									<div class="row">
										<div class="col-md-6">
											  <?php echo $this->Form->input('start_price',array("maxlength"=>8,"type"=>"text","placeholder"=>"From","label"=>false,'autofill'=>'false',"class"=>"form-control"));?>    
										</div>
										<div class="col-md-6">
											  <?php echo $this->Form->input('end_price',array("maxlength"=>8,"type"=>"text","label"=>false,'autofill'=>'false',"class"=>"form-control","placeholder"=>"To"));?>   
										</div>
										<div class="col-md-6 minlengthstart error_price"></div>
										<div class="col-md-6 minlengthend error_price" style="float: right;" ></div>
									</div>
									<div class="checkbox-style" style="float:left">
										
										 <?php echo $this->Form->input('ticket_free',array("class"=>"regular-checkbox","label"=>false,"type"=>"checkbox", "autocomplete"=>"off", "id"=>"ticket_free","div"=>false ))?><label for="ticket_free"></label> 											
									</div>
									<div class="free_tick">It’s Free</div>
								</div>

								<div class="form-field">
									<div class="form-heading">
										<?php echo $this->Html->image("time.png",array("title"=>"What Time?","alt"=>"What Time?"));?>										What Time?
									</div>
									<div class="row">
										<div class="col-md-6">
											<div class='input_date_grp input-group date datetimepicker2'>
											  <?php echo $this->Form->input('start_hour',array("label"=>false,"type"=>"text",'autofill'=>'false',"class"=>"form-control","placeholder"=>"From"));?>                
											  <span class="input-group-addon">
												<li class="fa fa-clock-o"></li>
											  </span>
											</div>		
										</div>
										<div class="col-md-6">
											<div class="grp_date">   
												<div class='input_date_grp input-group date datetimepicker2'>
												  <?php echo $this->Form->input('end_hour',array("label"=>false,"type"=>"text",'autofill'=>'false',"class"=>"form-control","placeholder"=>"To"));?>                
												  <span class="input-group-addon">
													<span class="fa fa-clock-o"></span>
												  </span>
												</div>	
											</div>	
										</div>
									</div>
								</div>

								<div class="form-field">
									<div class="form-heading">
										<?php echo $this->Html->image("repeat.png",array("title"=>"Is this event repeated?","alt"=>"Is this event repeated?"));?>
										 Is this event repeated?
									</div>									
									<?php 		                       	 
										//echo $this->Form->input('repeat_event1',array("class"=>"form-control","type"=>"hidden","label"=>false));
									  ?>
									  <?php 		                       	 
										echo $this->Form->input('repeat_event',array("class"=>"form-control","type"=>"select","label"=>false,"options"=>$optionsRepeat));
									  ?>
								</div>

								<div class="form-field">
									<div class="form-heading">
										<?php echo $this->Html->image("tickets.png",array("title"=>"Ticket Link","alt"=>"Ticket Link"));?>
										Ticket Link
									</div>
									<?php 		                       	 
									  echo $this->Form->input('ticket_link',array("class"=>"form-control","type"=>"text","label"=>false,"placeholder"=>"https://"));
									  ?>
									</div>
							</div>
						
						</div>
						<div class="event-submit text-right">
							<div class="col-sm-4">
							<input type="button" class="btn clear" id="clear_all" value="Clear Details"></div>
								
							<?php echo $this->Form->input('event_status',array("type"=>"hidden",'id'=>'event_status',"value"=>0));?>	
							<div class="col-sm-4 save_event"><input type="button" id="btnsave" class="btn save" value="Save Changes"></div>
							<div class="col-sm-4 edit_event" style="display:none"><input type="button" class="btn edit" value="Edit Event"></div>
							<div class="col-sm-4 "><input type="submit" id="showevent" class="show_hide_event btn" value="Show Event on Rave"></div>



						</div>
					</div>
					</div>
					<?php echo $this->Form->end(); ?>
			</div>
		</section>
<!-- Modal -->
							<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
							  <div class="modal-dialog" role="document">
							    <div class="modal-content">
							      	<div class="modal-header">
							        	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><img src="img/close-pop.png" alt="Close" title="Close"></button>
							        	<h4 class="modal-title" id="myModalLabel">"<span class="title_name"></span>" Comments (<span class="totalcomments"></span>)</h4>
							      	</div>
							      	<div class="modal-body">
								    	<ul class="pop-comment">						    		
								    		
								    	</ul>
								    	<div class="pop-comment-box">
											 <?php echo $this->Html->image($imageprofile,array("alt"=>"user","class"=>"comment-logo","height"=>"34","width"=>"34"));?>
											   <textarea  style="overflow: hidden; height: 40px;" class="form-control mention" data-val-required="true" name="comment" id="comment" placeholder="write a comment"></textarea>

<!--
											  <div contenteditable="true"  class="form-control  sendNotification" 
											  id="comment" name="comment"></div>
-->
								    		<div id="list_user_comment"></div>
								    		<!--<a class="smile-icon" href="javascript:void(0)"><img src="img/smile.png" alt="Smile" title="Smile"></a>-->
								    	</div>
							     	</div>
							    </div>
							  </div>
							</div>
							
														
							
<style>
.selectUnitemp{background-image:none !important;}
.head{font-size:20px;font-weight:bold;color:#444;margin-bottom:10px;}
.preview_voucher {padding:6px 20px;border:1px solid #444;margin-top:10px;}
.linkvoucher {color:#45ccf0 !important}
.linkvoucher:hover{color:#45ccf0 !important}
button.close {
	-webkit-appearance: none;
	/* padding: 0; */
	/* cursor: pointer; */
	background: 0 0!important;
	border: 0!important;
	color: #000!important;
	opacity: 2.2;
}
#map {
        height: 100px;  /* The height is 400 pixels */
        width:400px;  /* The width is the width of the web page */
}
div#imageModal{top:0px !important;}

.imageBox{height:470px !important; width:470px !important;}
.thumbBox{border-radius : 0px !important; height:270px !important; width:270px !important; top:42% !important; left:42% !important;}
#drop-zone{cursor:pointer;}

</style>   
<div class="modal fade loginModel themeModel" id="imageModal" tabindex="-1" role="dialog" aria-labelledby="imageModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
       <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title">Select OR Drag Event Image OR Paste Image(ctrl+v)</h4>
      </div>
      <div class="modal-body" id="actions">
        <div class="selct-img-voc">
		  <div class="img-container" style="width:100%;">
				<img src="<?php echo $tmpImage; ?>" alt="Picture" id="tmpImage_popup">
			</div>
		  <div class="iviewer" >
			 <ul class="custom-file-upload" class="docs-buttons">
				 <li><label for="upload-file" class="custom-file-upload"><span ><?php echo $this->Html->image("gallery-img.png",array("alt"=>"Voucher Pic")); ?></span></label> <input id="upload-file" type="file" name="file" accept="image/*"/></li>
				<li data-method="rotate" data-option="-90"><a href="javascript:void(0);" title="Rotate Left"><span id="rotateLeft"><?php echo $this->Html->image("rotate-left.png",array("alt"=>"Voucher Pic")); ?></span></a></li>
				 <li data-method="rotate" data-option="90"><a href="javascript:void(0);" title="Rotate Right"><span id="rotateRight"><?php echo $this->Html->image("rotate-right.png",array("alt"=>"Voucher Pic")); ?></span></a></li>
				 <li data-method="zoom" data-option="-0.1"><a href="javascript:void(0);" title="Zoom Out"><span id="zoomIn"><?php echo $this->Html->image("zoom-out.png",array("alt"=>"Voucher Pic")); ?></span></a></li>
				  <li data-method="zoom" data-option="0.1"><a href="javascript:void(0);" title="Zoom In"><span id="zoomOut"><?php echo $this->Html->image("zoom-in.png",array("alt"=>"Voucher Pic")); ?></span></a></li>
			 </ul>             
		  </div>
		</div>
        
		<div class="iviewer-btn">
			<ul class="list-inline">
			   <li><button class="btn btn-custom" data-dismiss="modal">CLOSE</button></li> 
			   <li><button class="btn btn-custom btn-primary" data-method="getCroppedCanvas" data-option="{ &quot;maxWidth&quot;: 4096, &quot;maxHeight&quot;: 4096 }" id="saveimage">SAVE CHANGES</button></li>
			</ul>
		</div>
	 </div>  	
    </div>
  </div>
</div>
<div class="modal in fade themeModel" tabindex="-1" role="dialog" id="cnfrmdelid">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Event Delete Confirmation</h5>       
      </div>
      <div class="modal-body">
        <p>Are you sure you want to do this?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary btn-theme" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-secondary btn-theme delvoucher">Ok</button>
      </div>
    </div>
  </div>
</div>

<style>
.img-container {
  /* Never limit the container height here */
  max-width: 100%;
}

.img-container img {
  /* This is important */
  width: 100%;
}
.cropper-bg {background-image:none !important;}
.cropper-modal {background-color:#EEEEEE !important;opacity:0 !important;}

</style>
 <script>
      // This example requires the Places library. Include the libraries=places
      // parameter when you first load the API. For example:
      // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">
	
	  				 
	  
      function initMap() {
		
		var lati = parseFloat($('#CalendarLatitude').val());
		var longi = parseFloat($('#CalendarLongitude').val());
		
		if(isNaN(lati) || lati=="")
		{
		  lati=53.4807593; longi=-2.2426305000000184;
		}		
        var map = new google.maps.Map(document.getElementById('map'), {
          center: {lat:lati, lng: longi},
          zoom: 17
        });
        var card = document.getElementById('pac-card');
        var input = document.getElementById('pac-input');
        var types = document.getElementById('type-selector');
        var strictBounds = document.getElementById('strict-bounds-selector');

        map.controls[google.maps.ControlPosition.TOP_RIGHT].push(card);

        var autocomplete = new google.maps.places.Autocomplete(input);

        // Bind the map's bounds (viewport) property to the autocomplete object,
        // so that the autocomplete requests use the current map bounds for the
        // bounds option in the request.
        autocomplete.bindTo('bounds', map);
		
        var infowindow = new google.maps.InfoWindow();
        var infowindowContent = document.getElementById('infowindow-content');
        infowindow.setContent(infowindowContent);
        var marker = new google.maps.Marker({
		 position: new google.maps.LatLng( lati,longi), 
          map: map,
          draggable: false  
        });

		//~ google.maps.event.addListener(marker, 'dragend', function (evt) {
			//~ 
			//~ var draglat =(evt.latLng.lat().toFixed(7));			
			//~ var draglng =(evt.latLng.lng().toFixed(16));
			//~ draglat =53.4807154 ; draglng=-2.2423142999999754;
			 //~ var latlng = {lat: parseFloat(draglat), lng: parseFloat(draglng)};
			//~ var geocoder = new google.maps.Geocoder;
//~ 
			//~ geocoder.geocode({'location': latlng}, function(results, status) {
			//~ if (status === 'OK') {
			  //~ if (results[0]) {							
				//~ infowindow.setContent(results[0].formatted_address);
				//~ infowindow.open(map, marker);
				//~ map.setCenter(new google.maps.LatLng( draglat,draglng));		
				//~ map.setZoom(18); 
				//~ $("#pac-input").val(results[0].formatted_address);
			  //~ } else {
				//~ //window.alert('No results found');
			  //~ }
			//~ } else {
			  //~ //window.alert('Geocoder failed due to: ' + status);
			//~ }
		  //~ });
//~ 
			//~ }); 
			
			
	

		//~ google.maps.event.addListener(marker, 'dragstart', function (evt) {
			//~ document.getElementById('current').innerHTML = '<p>Currently dragging marker...</p>';
		//~ });

          autocomplete.addListener('place_changed', function() {
			 
          infowindow.close();
          marker.setVisible(false);
          var place = autocomplete.getPlace();
          if (!place.geometry) {
            // User entered the name of a Place that was not suggested and
            // pressed the Enter key, or the Place Details request failed.
            //window.alert("No details available for input: '" + place.name + "'");
            return;
          }

          // If the place has a geometry, then present it on a map.
          if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
            var address =place.name;
            var geocoder = new google.maps.Geocoder();
            geocoder.geocode({ 'address': address}, function (results, status) {
                        if (status == google.maps.GeocoderStatus.OK) {
							var latitude = place.geometry.location.lat();
                            var longitude = place.geometry.location.lng();
                            //alert(latitude); alert(longitude);
                            $('#CalendarLatitude').val(latitude);
                            $('#CalendarLongitude').val(longitude);
                        } else {
                           
                        }
                    });
            
          } else {
			 
            map.setCenter(place.geometry.location);
            map.setZoom(17);  // Why 17? Because it looks good.
          }
          marker.setPosition(place.geometry.location);
          marker.setVisible(true);

          var address = '';
          if (place.address_components) {
            address = [
              (place.address_components[0] && place.address_components[0].short_name || ''),
              (place.address_components[1] && place.address_components[1].short_name || ''),
              (place.address_components[2] && place.address_components[2].short_name || '')
            ].join(' ');
          }       
         // infowindowContent.children['place-icon'].src = place.icon;
          infowindowContent.children['place-name'].textContent = place.name;
          infowindowContent.children['place-address'].textContent = address;         
          	
          	infowindow.setContent(address);
		  infowindow.open(map, marker);
        }    
        );	
        
        google.maps.event.addListener(marker, 'click', (function(marker) {
                return function() {
                    var newaddress=$("#pac-input").val();
					infowindow.setContent(newaddress);
                    infowindow.open(map, marker);
                     map.setZoom(17); 
                }
            })(marker));
       
	}	  
	
	
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDcBcY5ObGmiZ6TETlFS1cEOi5ttvBRnuw&libraries=places&callback=initMap" async defer></script>
<script src='<?php echo SITE_LINK?>js/comment/jquery.events.input.js' type='text/javascript'></script>
<script src='<?php echo SITE_LINK?>js/comment/jquery.elastic.js' type='text/javascript'></script>
<script src='<?php echo SITE_LINK?>js/comment/jquery.mentionsInput.js' type='text/javascript'></script>
<script src='<?php echo SITE_LINK?>css/comment/examples.js' type='text/javascript'></script>
<script>
function loadiframe()
	{	
		console.log("show_event2");
		var data_event1='<li class="{CLASS}"><div class="event-list-left"><img src="{SITE_LINK}{IMAGE}" alt="{TITLE}" title="{TITLE}"/></div><div class="event-list-right"><a href="javascript:void(0);" tooltip="tooltip" title="Delete" class="close-event confirmdelete" action="{SITE_LINK}delete_event?id={ID}"><img src="{SITE_LINK}img/close.png" alt="Close" title="Close"/></a><a href="javascript:void(0);" class="editevent_search" action="{EVENTDATE}" value="{ID}"><h2>{TITLE}</h2><p>{STARTHOUR}</p></a></div></li>';

		var data_event='<li class="{CLASS}"><div class="event-list-left"><img src="{SITE_LINK}{IMAGE}" alt="{TITLE}" title="{TITLE}"/></div><div class="event-list-right"><a href="javascript:void(0);" tooltip="tooltip" title="Delete" class="close-event confirmdelete" action="{SITE_LINK}delete_event?id={ID}"><img src="{SITE_LINK}img/close.png" alt="Close" title="Close"/></a><a href="javascript:void(0);" class="editevent" action="{EVENTDATE}" value="{ID}"><h2>{TITLE}</h2><p>{STARTHOUR}</p></a></div></li>';
		
		
		function formatAMPM(time)
		{  
			// var d = new Date(date);
			// Check correct time format and split into components
			  time = time.toString ().match (/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];

			  if (time.length > 1) { // If time format correct
				time = time.slice (1);  // Remove full string match value
				time[5] = +time[0] < 12 ? ' AM' : ' PM'; // Set AM/PM
				time[0] = +time[0] % 12 || 12; // Adjust hours
			  }
			  return time.join (''); // return adjusted time or original string
		}
	
		var flagvalue=true;var i=0;var x="";
		function edit_event(id,sel_date)
		{
			//alert("yes");alert(id);alert(sel_date);
			$(".gray-border").show();
			$(".nolive-bg").hide();		
			$(".cart_total_items").hide();
			$(".cart_total_items").html();
			
			
			 var href =SITE_LINK+"fetch_event?id="+id+"&date="+sel_date;
			 $.ajax({   
				type: "GET",   
				cache: false,   
				url: href,   			
				success: function(data){   
					data = JSON.parse(data);
									
					$(".tags").empty();	
					$('input,textarea,select').removeClass("error");	
					$('.red-bg').hide();
					$('.gray-border').show();
					$("#drop-zone").removeClass("error_full");			
					$("#CalendarHashtags").removeClass("error");	
					$(".event-noty").hide();$(".event_show_comments").hide();$(".gray-bg").hide();
					$(".popup-event").hide();	
					
					$("#id").val(data[0]["id"]);
					$("#title").val(data[0]["title"]);
					$("#short_dec").val(data[0]["short_dec"]);
					$("#long_dec").val(data[0]["long_dec"]);
					$("#pac-input").val(data[0]["location"]);
					$("#CalendarLatitude").val(data[0]["latitude"]);
					$("#CalendarLongitude").val(data[0]["longitude"]);
					initMap();
					if(data[0]["ticket_free"]=="1")
					{			
						
						$('#CalendarEndPrice').val();		
						$('#ticket_free').prop('checked',true);	
						$('#CalendarStartPrice').prop('disabled',true);
						$('#CalendarEndPrice').prop('disabled',true);
						$('#CalendarStartPrice').val('');
						$('#CalendarEndPrice').val('');
							
					}
					else
					{
						$('#ticket_free').prop('checked',false);	
						$("#CalendarStartPrice").val(data[0]["start_price"]);
						$("#CalendarEndPrice").val(data[0]["end_price"]);
					}
					var start_hour=formatAMPM(data[0]["start_hour"]);
					var end_hour=formatAMPM(data[0]["end_hour"]);
					$("#CalendarStartHour").val(start_hour);
					$("#CalendarEndHour").val(end_hour);
					
					$("#CalendarRepeatEvent").val(data[0]["repeat_event"]);
					//$("#CalendarRepeatEvent1").val(data[0]["repeat_event"]);
					$("#CalendarTicketLink").val(data[0]["ticket_link"]);				
					$('.upload-file img').attr("src",SITE_LINK+data[0]["image"]);	
					$('.img-container img').attr("src",SITE_LINK+data[0]["image"]);			
				
					$("#tmpImage").val(data[0]["image"]);				
					$("#oldImage").val(data[0]["image"]);				
					$("#CalendarTags").val(data[0]["tags"]);
					//$("#CalendarTags1").val(data[0]["tags"]);
					
				
					$("#event_status").val(data[0]["event_status"]);
					var event_status=data[0]["event_status"];
					$(".gray-border").hide();
					if(event_status==1)
					{
						$('#event_status').val(1);		
						$('#showevent').val("Hide Event on Rave"); 
						$("#showevent").addClass("hide-event");					
					}
					else
					{
						$(".live-event").hide();
						$(".gray-border").show();
						$('#event_status').val(0);
						$('#showevent').val("Show Event on Rave")
						$("#showevent").removeClass("hide-event");
					}		
					var today = new Date();						
					var currentdate= today.getFullYear() + '-' +('0'+(today.getMonth()+1)).slice(-2) + '-' + ('0' +today.getDate()).slice(-2);
					
					$("#eventdate").val(data[0]["eventdate"]);
					//~ theDate= new Date(data[0]["eventdate"]).getTime();	
					var d = new Date(sel_date);
					$("#tmpdate").val(sel_date);
					var date=months[d.getMonth()] + ' ' + d.getDate()+nth(d.getDate());				
					$("#date").html(date);
					//$("#tmpdate").val(data[0]["eventdate"]);
					theDate= new Date(sel_date).getTime();	
					
					$(".event-noty").show();
					$(".event_show_comments").show();
					
					$("#totalsavecalendar").html(data[0]["save_calendar"]);
					$("#totaleventview").html((data[0]["event_view"]));
					$(".totalcomments").html((data[0]["comments"]));
					
					if(data[0]["unviewed"]>0)
					{			
						$(".cart_total_items").show();
						$(".cart_total_items").html((data[0]["unviewed"]));
					}
					if(data[0]["is_active"]==0)
					{
						$(".unsuitable").show();
					}
					//Commented bcouz in new changes refreshing the clanede		
						//var url_calendar="calendar?month="+data[0]["eventdate"]+"&sel_date="+sel_date;	
						//$('#mycalendar').attr('src', url_calendar);	
					//Commented bcouz in new changes refreshing the claneder
					
					
										
					//var url_calendar="calendar?month="+data[0]["eventdate"];	
						
					
					$('#CalendarIndexForm input,textarea').prop('readonly', true);
					$('#CalendarIndexForm select').prop('disabled', true);				
					document.getElementById('drop-zone').style.pointerEvents = 'none';
					document.getElementById('img-upload').style.pointerEvents = 'none';
					document.getElementById('pac-input').style.pointerEvents = 'none';	
					document.getElementById('next').style.pointerEvents = 'none';
					document.getElementById('prev').style.pointerEvents = 'none';			
					$('#ticket_free').prop('disabled', true);	
					$(".save_event").hide();	
					$(".edit_event").show();
					$('#comment').prop('readonly', false);
					
					if(event_status)
					{
						//  $('.edit').attr('disabled',true);
						// Show Event Button//
								// document.getElementById('showevent').style.pointerEvents = 'none';
						  // Show Event Button//
					}	
					else
					{
						 $('.edit').attr('disabled',false);
						 // Show Event Button//
							//document.getElementById('showevent').style.pointerEvents = 'none';
						 // Show Event Button//
					}
					$('#clear_all').attr('disabled',true);
					//$('#showevent').attr('readonly',true);
					
					
					//var end = new Date(data[0]["end_hour"]);
					//var start = new Date(data[0]["start_hour"])
					
					var now_full_date = new Date();
					var now_full_date=now_full_date.getFullYear()+'-' +('0'+(now_full_date.getMonth()+1)).slice(-2) + '-' + ('0' +now_full_date.getDate()).slice(-2);
					
					
					var currentdate=  ('0'+(d.getMonth()+1)).slice(-2) + '/' + ('0' +d.getDate()).slice(-2)+ '/' +d.getFullYear();
					
					var click_date=$("#tmpdate").val();
					var showdate=$("#eventdate").val();
					showdate=showdate +" "+ data[0]["start_hour"] + ":00";
									
					var start_hour=currentdate +" "+ data[0]["start_hour"] + ":00";
					var end_hour=currentdate +" "+ data[0]["end_hour"]+ ":00";
					//currentdate=currentdate +" "+ start_hour + ":00";
					 //alert(start_hour);
					 start_hour= new Date(start_hour).getTime();
					 end_hour= new Date(end_hour).getTime();
							$(".plus-add").show();						
					var countDownDate = new Date(start_hour).getTime();
					var now = new Date().getTime();
					$(".live-event").hide(); 
					
					if(data[0]["event_status"]==true)
					{	
						
						if(now_full_date==click_date || now_full_date < click_date)
						{
							$(".live-event").show();	
							$(".live-event").html("This event is live on The Rave Mobile App");
						}	
						else 
						{
								$(".gray-bg").show();
																		
						}				
					}				
					else if(data[0]["event_status"]==false)
					{
						flagvalue=true; 		
						
						if(now_full_date==click_date || now_full_date < click_date)
						{	
							$(".nolive-bg").hide();	
							$(".live-event").hide();	
							$(".gray-border").show();
							$(".gray-bg").hide();
						}
						else
						{	
							$(".gray-border").hide();
							$(".gray-bg").show();
							
						}
					}				
				}  
			});
		}

		function formatTime(date) {
		  var hours = date.getHours();
		  var minutes = date.getMinutes();
		  var ampm = hours >= 12 ? 'pm' : 'am';
		  hours = hours % 12;
		  hours = hours ? hours : 12; // the hour '0' should be '12'
		  minutes = minutes < 10 ? '0'+minutes : minutes;
		  var strTime = hours + ':' + minutes + ' ' + ampm;
		  return strTime;
		}


		
		function formatDate(date)
		{  
			//console.log(time);
			
			var mydate = new Date(date);
			var time = formatTime(mydate);				
			var month = ["Jan", "Feb", "March", "April", "May", "June",
			"July", "Aug", "Sept", "Oct", "Nov", "Dec"][mydate.getMonth()];
			var str = month + ', ' + mydate.getDate()+ ' ' + mydate.getFullYear()+ ' ' + time;		
			//var currentDate = fullDate.getFullYear() + "-" +(fullDate.getMonth() + 1) + "-" + fullDate.getDate() ;		
			return str;
		}
		
		function currentDate()
		{
			var fullDate = new Date()				
			//var twoDigitMonth = ((fullDate.getMonth().length+1) === 1)? (fullDate.getMonth()+1) : '0' + (fullDate.getMonth()+1);
			 
			var currentdate = fullDate.getFullYear() + "-" +(fullDate.getMonth() + 1) + "-" + fullDate.getDate() ;
			return currentdate;
		}
		
		function leftbareventdata(href,sel=null)
		{
			$.ajax({  type: "GET",   
						cache: false,   
						url: href,   			
						success: function(data){   
							data = JSON.parse(data);														
							if ( data.length > 0 ) {
								var valsel=$("#searchTag option:selected").val();
															
								$(".event-list").empty();
								id_save=0;
								$.each(data,function(key,val){
									
								if(sel=="search" && id_save==0 && valsel!="")
								{	
										
									$("#eventdate").val(val['eventdate']);		
									$("#event_live_id").val(val['id']);	
									id_save=1;														
								}	
								
											
										
									if(val['title']=="")
									title="Title Here"
									else
									title=val['title'];
									
									var className = "";
									//var currentdate=currentDate();
									var fullDate=new Date();
									var currentdate =fullDate.getDate();
									var current_month =fullDate.getFullYear()+"-"+(fullDate.getMonth()+1)+"-"+fullDate.getDate();
									
									
									var fullDate1=new Date(val['eventdate']);
									var eventdate =fullDate1.getDate();
									var month=$(this).attr("value");											
									var start_hour=formatAMPM(val["start_hour"]);		
									if(sel=="search" && valsel!="")
									{
										tmpNew = data_event1.replace(/{IMAGE}/g,val["image"]);
									}
									else
									{
										tmpNew = data_event.replace(/{IMAGE}/g,val["image"]);
									}
									
									tmpNew = tmpNew.replace(/{TITLE}/g,title);							
									tmpNew = tmpNew.replace(/{STARTHOUR}/g,start_hour);
									tmpNew = tmpNew.replace(/{ID}/g,val['id']);
									tmpNew = tmpNew.replace(/{EVENTDATE}/g,val['eventdate']);
									tmpNew = tmpNew.replace(/{SITE_LINK}/g,SITE_LINK);
									tmpNew = tmpNew.replace(/{CLASS}/g,className);
								
									$(".event-list").append(tmpNew);
									
									loadFlag1 = true;
								});
							} else {
								flag = false;
								loadFlag1 = false;
								$(".event-list").html("<li class='white'>No event to display<li>");
								reset_from_after_edit();
							}
						}  
					});
		}
		
		var months = ["Jan", "Feb", "Mar","April", "May", "June", "July", "Aug","Sept", "Oct", "Nov", "Dec"];
		
		function getTheDate(getDate) 
		{
			  var days = ["Sunday", "Monday", "Tuesday","Wednesday", "Thursday", "Friday", "Saturday"];
		 
			  var theCDate = new Date(getDate);	 
			  var date=months[theCDate.getMonth()] + ' ' + theCDate.getDate()+nth(theCDate.getDate());
			  var date1=theCDate.getFullYear() + '-' +('0'+(theCDate.getMonth()+1)).slice(-2) + '-' + ('0' +theCDate.getDate()).slice(-2);
			  $("#eventdate").val(date1);
			  $("#tmpdate").val(date1);
			  return  date;
		}
		
		function nth(d) {
		  if (d > 3 && d < 21) return 'th'; 
		  switch (d % 10) {
			case 1:  return "st";
			case 2:  return "nd";
			case 3:  return "rd";
			default: return "th";
		  }
		}
		
		function resetFrom()
		{
			$(".plus-add").hide();	
			$(".tags").empty();		
			$('input,textarea,select').removeClass("error");	
			$('.red-bg').hide();
			$('.gray-border').show();
			$('.live-event').hide();
			$("#drop-zone").removeClass("error_full");			
			$("#CalendarHashtags").removeClass("error");	
			$('.upload-file img').attr("src",SITE_LINK+"img/img-icon1.png");
			$('.img-container img').attr("src",SITE_LINK+"img/img-icon.png");
			$('#event_status').val(0);
			$('#id').val('');		
			$('#CalendarTags').val('');		
			$('#showevent').val("Show Event on Rave")
			$("#showevent").removeClass("hide-event");	
			$(".event-noty").hide();$(".event_show_comments").hide();$(".gray-bg").hide();		
			$('#CalendarIndexForm')[0].reset();
			$(".popup-event").hide();
			$( "#CalendarStartPrice" ).prop( "disabled", false );
			$( "#CalendarEndPrice" ).prop( "disabled", false );
			var date=$('#tmpdate').val();	
			$('#eventdate').val(date);			
		}
	
	
		function reset_from_after_edit()
		{
			$(".edit_event").hide();
			$(".save_event").show();		
			$(".nolive-bg").hide();
			$('.save').attr('disabled',false);
			$(".live-event-bg").hide();		
			$('#CalendarIndexForm input,textarea').prop('readonly', false);
			$('#CalendarIndexForm select').prop('disabled', false);	
			$('#ticket_free').prop('disabled', false);	
			$('.cart_total_items').hide();				
			$('.edit').attr('disabled',false);
			$('#clear_all').attr('disabled',false);
			$('.nolive-bg').hide();
			//$('#showevent').attr('readonly',false);	
			// Show Event Button//
			//document.getElementById('showevent').style.pointerEvents = 'auto';
			// Show Event Button//
			document.getElementById('drop-zone').style.pointerEvents = 'auto';
			document.getElementById('img-upload').style.pointerEvents = 'auto';
			document.getElementById('pac-input').style.pointerEvents = 'auto'; 
			document.getElementById('ticket_free').style.pointerEvents = 'auto';
			document.getElementById('next').style.pointerEvents = 'auto';
			document.getElementById('prev').style.pointerEvents = 'auto';	
		}	
		
        $("#mycalendar").contents().on("click",".nextmonth", function(){
			   var month=$(this).attr("value");	
			  
			   var fullDate=new Date(month);
			   var year=fullDate.getFullYear();
			   var monthcurent =('0' + (fullDate.getMonth()+1)).slice(-2)
			   
			   
			   var today_date=new Date();
			   //( '0' + (myDate.getMonth()+1) ).slice( -2 );
			   today_date=( '0' + (today_date.getDate()) ).slice( -2 );
			 
				if(monthcurent=="02" || monthcurent=="04" || monthcurent=="06" || monthcurent=="09" || monthcurent=="11" ){
					today_date1=new Date(year, monthcurent, 0).getDate();
					if(today_date1!=today_date)
					{
						today_date=today_date1;
					}				   
				}
			 
			 
			   var full_date=year+"-"+monthcurent+"-"+today_date;
			 
			   theDate= new Date(full_date).getTime(); 
			   
			   var monthdate=getTheDate(full_date);             
			   $("#date").html(monthdate);
				
				$("#eventdate").html(full_date);	
            
			  $(".event-list").empty();
			  $("current_month_selected").val(month);		
				var url_calendar="calendar?month="+month+"&set_yes=no";	
				$('#mycalendar').attr('src', url_calendar);
				
				var fullDate=new Date();			
				var currentdate =year+"-"+monthcurent+"-"+fullDate.getDate();
				
				href =SITE_LINK+"show_myevent?month="+currentdate;
				
				nextprevmonthEdit(href,currentdate);
				leftbareventdata(href);
				resetFrom();
        });
        
      
		function nextprevmonthEdit(href,currentdate)
		{
			 $.ajax({   
				type: "GET",   
				cache: false,   
				url: href, 
				success: function(data){   
					data = JSON.parse(data);
					//console.log(data);			
					if ( data.length > 0 ) {
						var tmpNew = data_event;
						
						//$(".event-list").text="sfds";
						//$(".event-list").empty();
						//console.log(WWW_ROOT);
						var id_live=id_save=0;
						var event_status;
						var tmplink="";
						$.each(data,function(key,val){
							
							if(val['event_status'] == 1)
							{
								id_live=val['id'];
								event_status=1;							
								tmplink=val['title'];
							}
							else if(id_save == 0)
							{
								id_live=val['id'];
								event_status=0;
								tmplink=val['title'];
							}
							id_save=1;
							var className = "";
							//var currentdate=currentDate();
							var fullDate=new Date();
							var currentdate =fullDate.getDate();
							var current_month =fullDate.getFullYear()+"-"+(fullDate.getMonth()+1)+"-"+fullDate.getDate();
							
							
							var fullDate1=new Date(val['eventdate']);
							var eventMonth =fullDate1.getMonth()+1;	
							
						    loadFlag = true;
						});
						
						 edit_event(id_live,currentdate);
						  $("#tmpdate").val(currentdate);	
					} else {
						reset_from_after_edit();
					}
				}  
			});	
		}
		
		var iframeBody = $('body', $('#mycalendar')[0].contentWindow.document);
        $(iframeBody).on("click",".show_myevent", function(){
			
		reset_from_after_edit();
		$('[id=searchTag]').val( '' );
			
			//$('#searchTag option:eq("all")').prop('selected', true)
		   tmplink = $(this).attr("action");			
           var month=$(this).attr("value");	              
           var fullDate=new Date(month);
           var year=fullDate.getFullYear();         
           var monthselected =('0' + (fullDate.getMonth()+1)).slice(-2);           
           var today_date=( '0' + (fullDate.getDate())).slice( -2 );
           var full_date=year+"-"+monthselected+"-"+today_date;        
            
            
             var monthdate=getTheDate(full_date);             
             $("#date").html(monthdate);	
             //console.log($("#date").val());
             $("#eventdate").val(full_date);	
             theDate= new Date(full_date).getTime();
	
	
			var fullDatecurrent=new Date();
			var current_month =fullDatecurrent.getFullYear()+"-"+('0' + (fullDatecurrent.getMonth()+1)).slice( -2 )+"-"+( '0' + (fullDatecurrent.getDate())).slice( -2 );
             var imagevent="";
			 var href =SITE_LINK+"show_myevent?month="+month;		
			 resetFrom();
			 $.ajax({   
				type: "GET",   
				cache: false,   
				url: href, 
				success: function(data){
					console.log(data);
					//~ alert(data);
					//~ if(data=='"error_auth"'){		window.location.href=SITE_LINK ;}
					//~ else
					//~ {
					data = JSON.parse(data);
					//console.log(data);			
					if ( data.length > 0 ) {
						var tmpNew = data_event;
						var ff="";
						//$(".event-list").text="sfds";
						$(".event-list").empty();
						//console.log(WWW_ROOT);
						var id_live=id_save=0;
						var event_status;
						$.each(data,function(key,val){
							
							if(val['event_status'] == 1)
							{
								id_live=val['id'];
								event_status=1;							
								var tmplink=val['title'];
							}
							else if(id_save == 0)
							{
								id_live=val['id'];
								event_status=0;
								var tmplink=val['title'];
							}
							id_save=1;						
							if(val['title']=="" || val['title'] === null)
							title="Title Here"
							else
							title=val['title'];
							
							var className = "";
							//var currentdate=currentDate();
							
							var currentdate =fullDatecurrent.getDate();
														
							
							var fullDate1=new Date(val['eventdate']);
							var eventMonth =fullDate1.getMonth()+1;
							
							var cMonth=fullDate.getMonth()+1;
							
							var click_date=new Date(month);
							day=click_date.getDate();	
										
							var start_hour=formatAMPM(val["start_hour"]);		
							tmpNew = data_event.replace(/{IMAGE}/g,val["image"]);
							tmpNew = tmpNew.replace(/{TITLE}/g,title);							
							tmpNew = tmpNew.replace(/{STARTHOUR}/g,start_hour);
							tmpNew = tmpNew.replace(/{ID}/g,val['id']);
							tmpNew = tmpNew.replace(/{EVENTDATE}/g,val['eventdate']);
							tmpNew = tmpNew.replace(/{SITE_LINK}/g,SITE_LINK);
							tmpNew = tmpNew.replace(/{CLASS}/g,className);
							$(".event-list").append(tmpNew);
						    loadFlag = true;
						});
						
						 edit_event(id_live,tmplink);
						  $("#tmpdate").val(tmplink);	
					} else {
						flag = false;
						loadFlag = false;
						$(".event-list").html("<li class='white'>No event to display<li>");
					}
					//~ }
					
				}  
			});			
				
				var extra_row=$("#mycalendar").contents().find('#extra_row').val();				
				var k=1-parseInt(extra_row );				
				var today_date=new Date();
				today_date =today_date.getDate();	
				$(this).parent("td").parent("tr").parent("tbody").children("tr").each(function(){
					$(this).children("td").each(function(){
						//cut
						current_month=('0' + (fullDatecurrent.getMonth()+1)).slice( -2 );
						if(k==today_date)
						{ $(this).removeClass("active");
							if(monthselected==current_month)
							{
								
								$(this).addClass("today");		
								//$(this).closest( "td" ).toggleClass('classname');
								var cl =$(this).closest( "td" ).attr('class');
								if(cl.indexOf('savetoday') != -1){
									$(this).removeClass("savetoday");								
									$(this).addClass("saveToday");
								}
							}
							
						}
						else{
							$(this).removeClass("active");
						}
						var monthdate =('0' + (fullDate.getDate())).slice(-2);
				
						if(monthdate==today_date)
						{
							var cl =$(this).closest( "td" ).attr('class');	
							$(this).removeClass("today");			
							if(cl.indexOf('saveToday') != -1){
								
								
								$(this).removeClass("saveToday");$(this).addClass("save");
							}
							
						}
						k=k+1;
								
					});
				});
							
			     
				$(this).parent().addClass("active");
        });
		
		//~ $(this).contents().on('click', '.date-select-table', function () {alert("Sdf");		
			//~ if ( $(this).hasClass('active') ) {
				//~ $(this).removeClass('active');
			//~ }
		//~ });
		
		function imageExists(url, callback) {
		var img = new Image();
		img.onload = function() { callback(true); };
		img.onerror = function() { callback(false); };
		img.src = url;
	  }
	  
  }


</script>

