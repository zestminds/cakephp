<?php // pr($lists); die; ?>
<div class="cont-right">
  <div class="cont-right-innr">
	  <div class="main-hd-in">
          <div class="row">
            <div class="col-sm-12">
              <h2 class="title-das"> Manage Events</h2>
              
              	<div class="show-record">                	
                   <span> Show Records </span> 
                   <?php echo $this->Form->create("voucher1",array("div"=>false,"type"=>"get")); 			
					 echo $this->Form->input('records',array("id"=>"records",'label'=>'',"options"=>$records,'class'=>'form-control',"selected"=>$limit));
					 echo $this->Form->end(); ?>	
                </div>
            </div>
          </div>
        </div>
		<?php echo $this->Form->create("Calendar",array("div"=>false,)); ?>
			<div class="srch" style="margin-left:30px;">
				<?php echo $this->element("admins/common",array("place"=>'Search Event',"flag"=>false,"pageheader"=>'',"buttontitle"=>'no',"listflag"=>"no","action"=>'no')); ?>
				
			</div>
        <div class="manage-marchant">
			  <div class="table-responsive">
				<table class="Marchant-table table table-bordered merchant_headfont" id="voc_index" width="100%">
				  <tr>
					<th><?php echo $this->Form->input("check",array("label"=>false,"div"=>false,"id"=>'checkall',"type"=>'checkbox')); ?></th>
					<th>Image</th>
					<th><?php echo $this->Paginator->sort('UserDetail.business',"Name"); ?></th>
					<th><?php echo $this->Paginator->sort('title','Title'); ?></th>
					<th><?php echo $this->Paginator->sort('UserDetail.address','Location'); ?></th>	
					<th><?php echo $this->Paginator->sort('unsuitable_count','Reported'); ?></th>				
					<th><?php echo $this->Paginator->sort('created'); ?></th>	
					<th><?php echo $this->Paginator->sort('event_status','Status'); ?></th>
					<th><?php echo $this->Paginator->sort('is_active','Active'); ?></th>
					<th> action </th>
				  </tr>
				  <?php $i=$sno; foreach ($lists as $key=>$list): 
				 // pr($list);
				  ?>
				  <tr>
					<td><?php echo $this->Form->input("id.".$list['Calendar']['id'],array("class"=>'chk',"value"=>$list['Calendar']['id'],"type"=>'checkbox',"div"=>false,"label"=>false)); ?>
				<?php echo $this->Form->input("event_status.".$list['Calendar']['id'],array("type"=>'hidden',"value"=>($list['Calendar']['event_status'] == 1?0:1))); ?></td>
					<td><?php echo ($list['Calendar']['image'] == "" || !file_exists(WWW_ROOT."img/events/".$list['Calendar']['image'])) ? '<img src="'.SITE_LINK.'img/default_voucher.jpeg" style="width:75px;height:75px">':' <img src="'.SITE_LINK.'img/events/'.h($list['Calendar']['image']).'" style="width:75px;height:75px">';?></td>
					<td><a href="<?php echo SITE_LINK."edit_event_manager/".$list['UserDetail']['user_id']; ?>"><?php echo h($list['UserDetail']['business']); ?></a></td>
					<td><?php echo h($list['Calendar']['title']); ?></td>
					<td><?php echo h($list['Calendar']['location']); ?></td>
					<td><?php echo h(($list['Calendar']['unsuitable_count'] > 0)?'Yes('.$list['Calendar']['unsuitable_count'].'times)':'No');?></td>
					<td><?php $startdate =  date_create(h($list['Calendar']['created'])); echo date_format($startdate,"j M, Y"); ?></td>
					<td><?php echo h(($list['Calendar']['event_status'] == 1)?'Live':'Drafted');?></td>
					<td><?php echo h(($list['Calendar']['is_active'] == 1)?'Active':'Inactive');?></td>
					<td><a href="<?php echo SITE_LINK.'edit/'.$list['Calendar']['id']?>"><i class="fa fa-pencil"></i></a>	
						<?php echo $this->html->link($this->Html->tag('i', '', array('class' => 'glyphicon glyphicon-trash')). " ",   array('action' => 'delete', $list['Calendar']['id']),array('escape'=>false),__('Are you sure you want to delete event?', ''));?>
					</td>
				  </tr>
				  <?php $i++; endforeach; ?>
				</table>
			</div>
			  
			<div class="pagination-main">
				<div class="paging pagination">
				<?php echo $this->Paginator->prev('<i class="fa fa-caret-left"></i>', array('escape' => false), null, array('class' => 'fa prev disabled'));
				
				echo $this->Paginator->numbers(array('separator' => ''));
				echo $this->Paginator->next('<i class="fa fa-caret-right "></i>', array('escape' => false), null, array('class'	=> 'fa next disabled'));
				?>
			   </div>  
			</div>         
        </div>
		<?php echo $this->Form->end(); ?>
	</div>
</div>
<script>
$(document).ready(function () {
	$("#records").change(function(e) { 
	var val=$('#records').val();
	window.location.replace(SITE_LINK+"/view_events/?records="+val);	
			//$("#voucherAdminIndexForm").submit();
				  	  
	});
});	
</script>
