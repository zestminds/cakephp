<style>
input, textarea {
	width: 50px;

}

</style>	
<div class="cont-right">
   <div class="cont-right-innr">
	    <div class="main-hd-in">
          <div class="row">
            <div class="col-sm-12">
              <h2 class="title-das">Event Post Setting</h2>              
            </div>
          </div>
        </div>
		<div class="manage-marchant">
		<?php echo $this->Form->create('DefaultValue',array("novalidate"=>true)); ?>
			<fieldset>					
			<input name="id" id="id" value="<?php echo $list["DefaultValue"]["id"]?>" type="hidden">
				<div class="numbers-row">
					<label for="name">Maximum number of times a post can be reported before it is automatically deactivated </label>
					<div class="col-md-1"><input name="max_reported_post" id="max_reported_post" value="<?php echo $list["DefaultValue"]["max_reported_post"]?>" type="text"><br>
					</div>
					
				 </div>
			<div class="numbers-row">
					<label for="name">Maximum number of reported posts that will auto disable a business account.</label>
					<div class="col-md-1"><input name="max_business_post" id="max_business_post" value="<?php echo $list["DefaultValue"]["max_business_post"]?>" type="text"><br>
					</div>
				 </div>
		
			</fieldset>
		  <div style="margin-top:20px;margin-left:21px">
				  <button type="submit" class="btn btn-theme" id="save" name="save">SAVE</button>						  
				</div>
		</div>
	</div>		
</div>
