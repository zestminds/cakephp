<script>
	$(document).ready(function () {
	
	
    $('#CategoryAdminAddForm').validate({ // initialize the plugin
        rules: {			
			 
			  "data[Category][title]": {
				required: true				
			  }			
			},
			// Specify validation error messages
			messages: {			
				"data[Category][title]": {
					required: "Please enter a category"			
				}
			},
			// Make sure the form is submitted to the destination defined
			// in the "action" attribute of the form when valid
			submitHandler: function(form) {
				$('#error_msg').hide();	
				var imgVal = $('#tmpImage').val();
				if(imgVal=='') 	{	
					$('#error_msg').show();				
					return false;
				}
			  form.submit();
			}
		  });
		  
		  
		  //Image upload
	 var flag = true;
	 var avatar = "<?php echo SITE_LINK."img/img-icon.png"; ?>";
	$("#img-upload,#drop-zone").on("click",function(){
		$("#imageModal").modal("show");
	});
	/*
	$(function(){
        var rot = 0,ratio = 1;
        var CanvasCrop = $.CanvasCrop({
            cropBox : ".imageBox",
            imgSrc : avatar,
            limitOver : 2
        });
        
        
        $('#upload-file').on('change', function(){
            var reader = new FileReader();
            reader.onload = function(e) {
                CanvasCrop = $.CanvasCrop({
                    cropBox : ".imageBox",
                    imgSrc : e.target.result,
                    limitOver : 2
                });
                rot =0 ;
                ratio = 1;
            }
            reader.readAsDataURL(this.files[0]);
            flag = false;
            //this.files = [];
        });
        
        $("#rotateLeft").on("click",function(){
            rot -= 90;
            rot = rot<0?270:rot;
            CanvasCrop.rotate(rot);
        });
        $("#rotateRight").on("click",function(){
            rot += 90;
            rot = rot>360?90:rot;
            CanvasCrop.rotate(rot);
        });
        $("#zoomIn").on("click",function(){
            ratio =ratio*0.9;
            CanvasCrop.scale(ratio);
        });
        $("#zoomOut").on("click",function(){
            ratio =ratio*1.1;
            CanvasCrop.scale(ratio);
        });
        $("#alertInfo").on("click",function(){
            var canvas = document.getElementById("visbleCanvas");
            var context = canvas.getContext("2d");
            context.clearRect(0,0,canvas.width,canvas.height);
        });
        
        $("#saveimage").on("click",function(){
            //var src = CanvasCrop.getDataURL("jpeg");           
			var src = CanvasCrop.getDataURL("png");
			
            //$("body").append("<div style='word-break: break-all;'>"+src+"</div>");  
            //$(".container").append("<img src='"+src+"' />");
			//console.log(src);
			$("#img-upload").attr("src",src);
			$("#tmpImage").val(src);
			$("#imageModal").modal("hide");
			avatar = src;	
		});
        
       
    });		*/
});


</script>
<?php
$tmpImage = SITE_LINK."img/img-icon.png";

//echo $tmpImage = file_get_contents($tmpImage); die;
 ?>
<div class="cont-right">
   <div class="cont-right-innr">
	    <div class="main-hd-in">
          <div class="row">
            <div class="col-sm-12">
              <h2 class="title-das"> Add Category</h2>              
            </div>
          </div>
        </div>
		<div class="manage-marchant">
		<?php echo $this->Form->create('Category'); ?>
			<fieldset>
					<div><?php echo $this->Form->input('title',array('label' => 'Category Name'));?> </div>
					<div class="upload-drop-zone" id="drop-zone"> 
						   <div class="upload-file"> 
							     <?php echo $this->Html->image("img-icon.png",array("alt"=>"Voucher Pic","id"=>"img-upload")); ?>
								 <?php echo $this->Form->input("tmpImage",array("label"=>false,"type"=>"textarea","style"=>"display:none;","id"=>"tmpImage")); ?>
								 <p>Upload File</p>
						   </div>
						  <!-- <div class="btn btn-upload image-preview-input btn-file">	                                   
							<i class="fa fa-cloud-upload" aria-hidden="true"></i><span class="image-preview-input-title">Select file to upload</span>
						   <?php //echo $this->Form->file('image',array("type"=>"file","accept"=>"image/png, image/jpeg, image/gif","type"=>"file","id"=>"sample_input","name"=>"test[image]"));?>
							</div>	-->	
					</div>
					<div id="error_msg" style="display:none" class="error">Please select Image</div>		         	        
				  <div><?php echo $this->Form->input('is_active');?> </div>			
				  <div style="margin-top:20px">
				  <button type="submit" class="btn btn-theme" id="save" name="save">SAVE</button>
							 
					<?php //echo $this->Form->Submit('SUBMIT',array("class"=>"btn btn-theme","title"=>"Submit",'div'=>false)); ?>
					<a href="<?php echo SITE_LINK ?>manage-categories" class="btn btn-theme">cancel</a>		  
				</div>
		</fieldset>
		<?php echo $this->Form->end();?>	
		</div>
		</div>
	</div>		
</div>
<div class="modal fade loginModel themeModel" id="imageModal" tabindex="-1" role="dialog" aria-labelledby="imageModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
       <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title">Select Category Image</h4>
      </div>
      <div class="modal-body" id="actions">
        <div class="selct-img-voc">
		  <div class="img-container" style="width:100%;">
				<img src="<?php echo $tmpImage; ?>" alt="Picture">
			</div>
		  <div class="iviewer" >
			 <ul class="custom-file-upload" class="docs-buttons">
				 <li><label for="upload-file" class="custom-file-upload"><span ><?php echo $this->Html->image("gallery-img.png",array("alt"=>"Voucher Pic")); ?></span></label> <input id="upload-file" type="file" name="file" accept="image/*"/></li>
				<li data-method="rotate" data-option="-90"><a href="javascript:void(0)" title="Rotate Left"><span id="rotateLeft"><?php echo $this->Html->image("rotate-left.png",array("alt"=>"Voucher Pic")); ?></span></a></li>
				 <li data-method="rotate" data-option="90"><a href="javascript:void(0)" title="Rotate Right"><span id="rotateRight"><?php echo $this->Html->image("rotate-right.png",array("alt"=>"Voucher Pic")); ?></span></a></li>
				 <li data-method="zoom" data-option="-0.1"><a href="javascript:void(0)" title="Zoom Out"><span id="zoomIn"><?php echo $this->Html->image("zoom-out.png",array("alt"=>"Voucher Pic")); ?></span></a></li>
				  <li data-method="zoom" data-option="0.1"><a href="javascript:void(0)" title="Zoom In"><span id="zoomOut"><?php echo $this->Html->image("zoom-in.png",array("alt"=>"Voucher Pic")); ?></span></a></li>
			 </ul>             
		  </div>
		</div>
        
		<div class="iviewer-btn">
			<ul class="list-inline">
			   <li><button class="btn btn-custom" data-dismiss="modal">CLOSE</button></li> 
			   <li><button class="btn btn-custom btn-primary" data-method="getCroppedCanvas" data-option="{ &quot;maxWidth&quot;: 4096, &quot;maxHeight&quot;: 4096 }" id="saveimage">SAVE CHANGES</button></li>
			</ul>
		</div>
	 </div>  	
    </div>
  </div>
</div>
<style>
.img-container {
  /* Never limit the container height here */
  max-width: 100%;
}

.img-container img {
  /* This is important */
  width: 100%;
}
.cropper-bg {background-image:none !important;}
.cropper-modal {background-color:#EEEEEE !important;opacity:0 !important;}

</style>