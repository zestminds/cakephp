<div class="cont-right">
   <div class="cont-right-innr">
		<div class="categories view">
		<h2><?php echo __('Category'); ?></h2>
			<dl>
				
				<dt></dt>
				<dd>
					<?php echo $this->Html->image(SITE_LINK.'img/category/'.h($category['Category']['image']),array("alt"=>"Category Pic")); ?>
					&nbsp;
				</dd>
				<dt><?php echo __('Category Name'); ?></dt>
				<dd>
					<?php echo h($category['Category']['title']); ?>
					&nbsp;
				</dd>
				<dt><?php echo __('Created'); ?></dt>
				<dd>
					<?php $joined =  date_create(h($category['Category']['created'])); echo date_format($joined,"jS M, Y h:i:s"); ?>
					&nbsp;
				</dd>
				<dt><?php echo __('Modified'); ?></dt>
				<dd>
					<?php $joined =  date_create(h($category['Category']['modified'])); echo date_format($joined,"jS M, Y h:i:s");?>
					&nbsp;
				</dd>
				<dt><?php echo __('Status'); ?></dt>
				<dd>
					<?php if (h($category['Category']['is_active'])==0) echo "INACTIVE"; else echo "ACTIVE" ; ?>
					&nbsp;
				</dd>		
				
			</dl>
		</div>
	</div>		
</div>
