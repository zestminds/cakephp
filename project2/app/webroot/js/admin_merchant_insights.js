$(document).ready(function () {	
	
	$("#loc_renewals").change(function(e) {
		var days=$("#days_renewals option:selected").val();
		var location=$("#loc_renewals option:selected").val();		
		var url_download="total_renewals/?days="+days+"&location="+location;		
		var url_logins="total_merchant_logins/?days="+days+"&location="+location;
		
		$('#chart_renewals').attr('src', url_download);
		$('#chart_logins').attr('src', url_logins);
	});
	
	$("#days_renewals").change(function(e) {
		var days=$("#days_renewals option:selected").val();
		var location=$("#loc_renewals option:selected").val();
		
		var url_download="total_renewals/?days="+days+"&location="+location;
		var url_logins="total_merchant_logins/?days="+days+"&location="+location;
		$('#chart_renewals').attr('src', url_download);
		$('#chart_logins').attr('src', url_logins);
	});
	
	$("#loc_reg").change(function(e) {
		var days=$("#days_reg option:selected").val();
		var location=$("#loc_reg option:selected").val();			
		var url_reg="total_merchant_registrations/?days="+days+"&location="+location;		
		var url_paidus="total_merchant_paidsus/?days="+days+"&location="+location;
		
		$('#chart_reg').attr('src', url_reg);
		$('#chart_paidsus').attr('src', url_paidus);
	});
	
	$("#days_reg").change(function(e) {
		var days=$("#days_reg option:selected").val();
		var location=$("#loc_reg option:selected").val();
		
		var url_reg="total_merchant_registrations/?days="+days+"&location="+location;
		var url_paidus="total_merchant_paidsus/?days="+days+"&location="+location;
		$('#chart_reg').attr('src', url_reg);
		$('#chart_paidsus').attr('src', url_paidus);
	});
	
	$("#loc_reachcon").change(function(e) {
		var days=$("#days_reachcon option:selected").val();
		var location=$("#loc_reachcon option:selected").val();
		var merchant=$("#merchant_reachcon option:selected").val();				
		var url_reg="total_merchant_reachconversion/?days="+days+"&location="+location+"&merchant="+merchant;	
		$('#chart_reachconversions').attr('src', url_reg);
	});
	
	$("#days_reachcon").change(function(e) {
		var days=$("#days_reachcon option:selected").val();
		var location=$("#loc_reachcon option:selected").val();		
		var merchant=$("#merchant_reachcon option:selected").val();			
		var url_reg="total_merchant_reachconversion/?days="+days+"&location="+location+"&merchant="+merchant;	
		$('#chart_reachconversions').attr('src', url_reg);
	});
	
	$("#merchant_reachcon").change(function(e) {
		var days=$("#days_reachcon option:selected").val();
		var location=$("#loc_reachcon option:selected").val();	
		var merchant=$("#merchant_reachcon option:selected").val();		
		var url_reg="total_merchant_reachconversion/?days="+days+"&location="+location+"&merchant="+merchant;
		$('#chart_reachconversions').attr('src', url_reg);
	});
	
	$("#loc_draft").change(function(e) {
		var days=$("#days_draft option:selected").val();
		var location=$("#loc_draft option:selected").val();
		var merchant=$("#merchant_draft option:selected").val();				
		var url_reg="total_merchant_drafted/?days="+days+"&location="+location+"&merchant="+merchant;	
		$('#chart_draft').attr('src', url_reg);
	});
	$("#days_draft").change(function(e) {
		var days=$("#days_draft option:selected").val();
		var location=$("#loc_draft option:selected").val();
		var merchant=$("#merchant_draft option:selected").val();				
		var url_reg="total_merchant_drafted/?days="+days+"&location="+location+"&merchant="+merchant;	
		$('#chart_draft').attr('src', url_reg);
	});
	$("#merchant_draft").change(function(e) {
		var days=$("#days_draft option:selected").val();
		var location=$("#loc_draft option:selected").val();
		var merchant=$("#merchant_draft option:selected").val();				
		var url_reg="total_merchant_drafted/?days="+days+"&location="+location+"&merchant="+merchant;	
		$('#chart_draft').attr('src', url_reg);
	});
	
});

