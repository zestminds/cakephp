$(document).ready(function () {
	
    var validator1 = $('#UserLoginForm').validate({ // initialize the plugin
        rules: {			
			 
			  "data[User][username]": {
				required: true,
				email : true
			  },
			 "data[User][password]": {
				required: true			
			  },
			  "login_hiddenRecaptcha": { required: true, }
			},
			// Specify validation error messages
			messages: {			
				"data[User][password]": {
					required: "Please enter a password"			
				},			 
			  "data[User][username]": {
				required: "Please enter a email",
				email : "Please enter valid email"			
			  },
			  "login_hiddenRecaptcha": {
				required: "Invalid captcha"					
			  }
			},
			// Make sure the form is submitted to the destination defined
			// in the "action" attribute of the form when valid
			submitHandler: function(e) {				
				$('#error_msg').hide();
				$("#error_msg_captcha").hide();
				//~ if ($("#login_hiddenRecaptcha").val().trim() == "") {					
					//~ $('#error_msg').hide();
					//~ $("#error_msg_captcha").show();
					//~ //return false;
				//~ }
				
				$('#UserLoginForm').ajaxSubmit(function(response) { 					
					 $('#error_msg').empty();		
					response = JSON.parse(response);
					if ( response.status ) {
						location.href = SITE_LINK+response.url;
					} else {						
						 $('#error_msg').text(response.message);
						 $("#error_msg_captcha").hide();
						 $('#error_msg').show();
						 // grecaptcha.reset(widgetId1);
						 // $('#login_hiddenRecaptcha').val('');
						
						return false;
					}
				});
			}
	});
	
	var validator1 = $('#UserForgotPasswordForm').validate({ // initialize the plugin
        rules: {			
			 
			  "data[User][email]": {
				required: true,
				email : true
			  },
			  "forgetpass_hiddenRecaptcha": {
				required: true
			  }					 		
			},
			// Specify validation error messages
			messages: {			
						 
			  "data[User][email]": {
				required: "Please enter a email",
				email : "Please enter valid email"			
			  },
			  "forgetpass_hiddenRecaptcha": {
				required: "Invalid captcha"					
			  }
			},
			// Make sure the form is submitted to the destination defined
			// in the "action" attribute of the form when valid
			submitHandler: function(form) {				
					$('#error_msg_forget').hide();
					$("#error_msg_captcha_forget").hide();
					
					//~ if ($("#forgetpass_hiddenRecaptcha").val().trim() == "") {
						//~ $('#error_msg_forget').hide();
						//~ $('#success_msg_forget').hide();
						//~ $("#error_msg_captcha_forget").show();
						//~ //return false;
					//~ }
			 $('#UserForgotPasswordForm').ajaxSubmit(function(response) { 		
					response = JSON.parse(response);
					if (response.status) {						
						 location.href = SITE_LINK+"message_forget_password";						 				
					} else {							
						 $("#error_msg_forget").show();				
						 $('#error_msg_forget').text(response.message);	
						 //grecaptcha.reset(widgetId3);
						 //$('#forgetpass_hiddenRecaptcha').val('');		
						 		 						
						return false;
					}
				});
			}
		  });		
	
		
	$("#signupPopup").on("click",function(){
		validator1.resetForm();
		$("#regModel").show();
		$("#loginModel").hide();
		$("#error_msg").hide();
	});
	
	$("#forgetpopup").on("click",function(){
		//validator1.resetForm();
		$("#regModel").hide();
		$("#loginModel").hide();
		$("#forgetModel").show();
		$("#success_msg_forget").hide();
		$("#error_msg_forget").hide();
	});
});
