	
$(document).ready(function () {	
	
	loadFlag1 = false;	
	var months = ["Jan", "Feb", "Mar",
		"April", "May", "June", "July", "Aug",
		"Sept", "Oct", "Nov", "Dec"
	  ];
var confirmSubmit = false;
 
$("#CalendarAdminEditForm").validate({// initialize the plugin
	 ignore: "",
	 rules: {			
		  "data[Calendar][tmpImage]": {
			required: true	
		  },
		   "data[Calendar][title]": {
			required: true	
		  },
		   "data[Calendar][short_dec]": {
			required: true	
		  },
		   "data[Calendar][tags]": {
			required: true	
		  },
		   "data[Calendar][location]": {
			required: true	
		  },
		  "data[Calendar][start_price]": {
			required: true	
		  }
		  ,
		  "data[Calendar][end_price]": {
			required: true	
		  }
		  ,
		  "data[Calendar][start_hour]": {
			required: true	
		  }
		  ,
		  "data[Calendar][end_hour]": {
			required: true	
		  }
		},		
		messages: {		
		
		},	
		 errorPlacement: function(){
			if($('#tmpImage').val()=="")
			{
				$("#drop-zone").addClass("error_full");
			}
			if($('#CalendarTags').val()=="")
			{
				$("#CalendarHashtags").addClass("error");
			}
			$(".red-bg").show();
			$(".gray-border").hide();
            return false;  // suppresses error message text
        },	
		submitHandler: function(e) {	
			$(".red-bg").hide();
			$(".gray-border").show();				
			form.submit();
		}
	  });
		

	
	$(document).on("click",".delete_button",function(){
		
		var itemtoRemove=($(this).attr("value"));		
		var result = $("#CalendarTags").val().split(',');
		for(var i = result.length-1; i >= 0; i--){  
			if(result[i] == itemtoRemove){         
				result.splice(i,1);           
			}
		}	
		var result=result.join(",");
		 $("#CalendarTags").val(result);
			$(this).parent().remove();	
	});
	
	$( ".deletetag").click(function( event ) {
		 $("#CalendarHashtags").val('');
	});
	
	
	$("#img-upload,#drop-zone").on("click",function(){
		$("#imageModal").modal("show");
	});
	
	$("#ticket_free").on("click",function(){
		if($("#ticket_free").prop('checked') == true){
			$("#CalendarStartPrice").val('');
			$("#CalendarEndPrice").val('');
			$( "#CalendarStartPrice" ).prop( "disabled", true );
			$( "#CalendarEndPrice" ).prop( "disabled", true );
		}
		else
		{			
			$("#CalendarStartPrice").prop( "disabled", false );
			$("#CalendarEndPrice").prop( "disabled", false );
		}
	});	
     $("#clear_all").on("click",function(){
		 $('#CalendarAdminEditForm')[0].reset();		 
	});	 
	
	$(".close_upgrade").on("click",function(){
		$(".upgrade-pop").hide();
	});
	$(".upgrade_now").on("click",function(){
		window.location.href=SITE_LINK+"buy-plan";
	});
	$("#showevent").on("click",function(){
		var event_status=$('#event_status').val();
		var month=$('#eventdate').val();
		if(event_status==0)
		{			
			 var href =SITE_LINK+"count_user_events?month="+month;		
			 $.ajax({   
				type: "GET",   
				cache: false,   
				url: href,   			
				success: function(data){   
					data = JSON.parse(data);
					if(data=="max")
					{
						$(".upgrade-pop").show();
					}
					else{
					$('#event_status').val(1);		
						$('#showevent').text("Hide Event on Rave"); 
						$("#showevent").addClass("hide-event");	
					}
				}
			});		
			
		}
		else
		{
			$('#event_status').val(0);
			$('#showevent').text("Show Event on Rave")
			$("#showevent").removeClass("hide-event");
		}
	});
	//var pre=1;
	if (!Date.now) {
	  Date.now = function() {
		return new Date().getTime();
	  }
	}
	
	var theDate = Date.parse($("#eventdate1").val());
	document.getElementById('date').innerText = getTheDate($("#eventdate1").val());
		
	document.getElementById('prev').addEventListener("click", function() {
	  let prevDate=theDate-86400000;
	  let prevDay=getTheDate(prevDate);
	  theDate = prevDate;
	  document.getElementById('date').innerText = prevDay;
	  
	})
	document.getElementById('next').addEventListener("click", function() {
	  let nextDate=theDate+86400000;	  
	  let nextDay=getTheDate(nextDate);
	  theDate = nextDate;
	  document.getElementById('date').innerText = nextDay;
	
	})
	
	
	function getTheDate(getDate) {
	  var days = ["Sunday", "Monday", "Tuesday",
		"Wednesday", "Thursday", "Friday", "Saturday"
	  ];
	  
	  var theCDate = new Date(getDate);	 
	  var date=months[theCDate.getMonth()] + ' ' + theCDate.getDate()+nth(theCDate.getDate());
	  var date1=theCDate.getFullYear() + '-' +('0'+(theCDate.getMonth()+1)).slice(-2) + '-' + theCDate.getDate();
	  $("#eventdate").val(date1);	 
	  return  date;
	}
	function nth(d) {
	  if (d > 3 && d < 21) return 'th'; 
	  switch (d % 10) {
		case 1:  return "st";
		case 2:  return "nd";
		case 3:  return "rd";
		default: return "th";
	  }
	}
	
	
	$(document).on("click",".confirmdelete",function(){
		tmplink = $(this).attr("action");
		$("#cnfrmdelid").modal("show");
	});

	
	$(document).on("click","#clear_all",function(){	
	resetFrom();
	});
	function resetFrom()
	{
		$(".tags").empty();			
		$('input,textarea,select').removeClass("error");	
		$('.red-bg').hide();
		$('.gray-border').show();
		$("#drop-zone").removeClass("error_full");			
		$("#CalendarHashtags").removeClass("error");	
		$('.upload-file img').attr("src",SITE_LINK+"img/img-icon.png");
		$('.img-container img').attr("src",SITE_LINK+"img/img-icon.png");
		$('#event_status').val(0);
		$('#id').val('');		
		$('#CalendarTags').val('');		
		$('#showevent').text("Show Event on Rave")
		$("#showevent").removeClass("hide-event");	
		$(".event-noty").hide();$(".event_show_comments").hide();$(".gray-bg").hide();		
		$('#CalendarAdminEditForm')[0].reset();
		$(".popup-event").hide();
	}
	 $(document).on("click",".editevent",function(){
		 var id=$(this).attr("value");
		 
		 
		 var href =SITE_LINK+"fetch_event?id="+id;
		 $.ajax({   
			type: "GET",   
			cache: false,   
			url: href,   			
			success: function(data){   
				data = JSON.parse(data);
								
				$(".tags").empty();	
				$('input,textarea,select').removeClass("error");	
				$('.red-bg').hide();
				$('.gray-border').show();
				$("#drop-zone").removeClass("error_full");			
				$("#CalendarHashtags").removeClass("error");	
				$(".event-noty").hide();$(".event_show_comments").hide();$(".gray-bg").hide();
				$(".popup-event").hide();	
				
				$("#id").val(data[0]["id"]);
				$("#title").val(data[0]["title"]);
				$("#short_dec").val(data[0]["short_dec"]);
				$("#long_dec").val(data[0]["long_dec"]);
				$("#pac-input").val(data[0]["location"]);
				$("#CalendarLatitude").val(data[0]["latitude"]);
				$("#CalendarLongitude").val(data[0]["longitude"]);
				initMap();
				if(data[0]["ticket_free"]=="1")
				{					
					$('#ticket_free').prop('checked',true);	
					$('#CalendarStartPrice').prop('disabled',true);
					$('#CalendarEndPrice').prop('disabled',true);	
				}
				else
				{
					$('#ticket_free').prop('checked',false);	
					$("#CalendarStartPrice").val(data[0]["start_price"]);
					$("#CalendarEndPrice").val(data[0]["end_price"]);
				}
				var start_hour=formatAMPM(data[0]["start_hour"]);
				var end_hour=formatAMPM(data[0]["end_hour"]);
				$("#CalendarStartHour").val(start_hour);
				$("#CalendarEndHour").val(end_hour);
				
				$("#CalendarRepeatEvent").val(data[0]["repeat_event"]);
				$("#CalendarTicketLink").val(data[0]["ticket_link"]);				
				$('.upload-file img').attr("src",SITE_LINK+data[0]["image"]);	
				$('.img-container img').attr("src",SITE_LINK+data[0]["image"]);			
			
				$("#tmpImage").val(data[0]["image"]);				
				$("#oldImage").val(data[0]["image"]);				
				$("#CalendarTags").val(data[0]["tags"]);
				if(data[0]["tags"]!="")
				{
					$(".tags").empty();	
					var tags=data[0]["tags"];
					var result =tags.split(',');
					for(var i=0; i < result.length; i++){ 						
					 var vals="<li><a href='javascript:void(0)'>"+result[i]+"</a><button type='button' class='delete_button' value="+result[i]+"><img src='"+SITE_LINK+"img/cros-icon.png' alt='' onclick='delete_tag_data(this.value)'/></button> </li>";	
					  $(".tags").append(vals);	
					}
				}
				$("#event_status").val(data[0]["event_status"]);
				var event_status=data[0]["event_status"];
				if(event_status==1)
				{
					$('#event_status').val(1);		
					$('#showevent').text("Hide Event on Rave"); 
					$("#showevent").addClass("hide-event");
				}
				else
				{
					$('#event_status').val(0);
					$('#showevent').text("Show Event on Rave")
					$("#showevent").removeClass("hide-event");
				}
				
				var d = new Date(data[0]["eventdate"]);
				var date=months[d.getMonth()] + ' ' + d.getDate()+nth(d.getDate());
			
				$("#date").html(date);
				$("#eventdate").val(data[0]["eventdate"]);
				theDate= new Date(data[0]["eventdate"]).getTime();	
				$(".event-noty").show();
				$(".event_show_comments").show();
				
				$("#totalsavecalendar").html(data[0]["save_calendar"]);
				$("#totaleventview").html((data[0]["event_view"]));
				$(".totalcomments").html((data[0]["comments"]));				
				
				if(data[0]["is_active"]==0)
				{
					$(".unsuitable").show();
				}
				
				
						
			}  
		});
	});	 
	
	function formatAMPM(time)
	{  
		// var d = new Date(date);
		// Check correct time format and split into components
		  time = time.toString ().match (/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];

		  if (time.length > 1) { // If time format correct
			time = time.slice (1);  // Remove full string match value
			time[5] = +time[0] < 12 ? ' AM' : ' PM'; // Set AM/PM
			time[0] = +time[0] % 12 || 12; // Adjust hours
		  }
		  return time.join (''); // return adjusted time or original string
	}
	
	

	function formatTime(date) {
	  var hours = date.getHours();
	  var minutes = date.getMinutes();
	  var ampm = hours >= 12 ? 'pm' : 'am';
	  hours = hours % 12;
	  hours = hours ? hours : 12; // the hour '0' should be '12'
	  minutes = minutes < 10 ? '0'+minutes : minutes;
	  var strTime = hours + ':' + minutes + ' ' + ampm;
	  return strTime;
	}


	
	function formatDate(date)
	{  
		//console.log(time);
		
		var mydate = new Date(date);
		var time = formatTime(mydate);				
		var month = ["Jan", "Feb", "March", "April", "May", "June",
		"July", "Aug", "Sept", "Oct", "Nov", "Dec"][mydate.getMonth()];
		var str = month + ', ' + mydate.getDate()+ ' ' + mydate.getFullYear()+ ' ' + time;		
		//var currentDate = fullDate.getFullYear() + "-" +(fullDate.getMonth() + 1) + "-" + fullDate.getDate() ;		
		return str;
	}
	function currentDate()
	{
		var fullDate = new Date()				
		//var twoDigitMonth = ((fullDate.getMonth().length+1) === 1)? (fullDate.getMonth()+1) : '0' + (fullDate.getMonth()+1);
		 
		var currentdate = fullDate.getFullYear() + "-" +(fullDate.getMonth() + 1) + "-" + fullDate.getDate() ;
		return currentdate;
	}
	 $(document).on("click",".showcomments",function(){	
		 
		  var id=$("#id").val();
		$(".pop-comment").empty();
		 var href =SITE_LINK+"fetch_comments?id="+id;		
			comments_show(href);
		});
		
		function comments_show(href)
		{
			 $.ajax({   
				type: "GET",   
				cache: false,   
				url: href,   			
				success: function(data){   
					data = JSON.parse(data);
					var title_name=$("#title").val();
					$(".title_name").html(title_name);
					if(data=="no-comments" || data=="invalid_request")
					{
						$("#pop-comment").html("No Comment to display");
					}
					else
					{
						if ( data.length > 0 ) {
							var tmpNew1 = data_comment;
							var ff="";
							//$(".event-list").text="sfds";
							$(".pop-comment").empty();
							$.each(data,function(key,val){														
								
								var start_hour=formatDate(val["created"]);		
								tmpNew1 = data_comment.replace(/{IMAGE}/g,val['image']);
								tmpNew1 = tmpNew1.replace(/{NAME}/g,val['business']);							
								tmpNew1 = tmpNew1.replace(/{DATETIME}/g,start_hour);
								//tmpNew = tmpNew.replace(/{ID}/g,val['Calendar']['id']);
								tmpNew1 = tmpNew1.replace(/{COMMENT}/g,val['comment']);
								tmpNew1 = tmpNew1.replace(/{SITE_LINK}/g,SITE_LINK);
								$(".pop-comment").append(tmpNew1);
								
								
								//$(".pop-comment").val($("#totalcomments").val());
							});
						} 				
					}
					
				}	
			});
			}
		
		var data_comment='<li><img src="{SITE_LINK}{IMAGE}" alt="{NAME}" title="{NAME}"><h3>{NAME}</h3><p>{DATETIME}</p><p>{COMMENT}</p></li>';
		
		$("#comment").keyup(function(e){
		if((e.keyCode || e.which) == 13) { //Enter keycode
		 
			 var id=$("#id").val();
			 var comment=$("#comment").val();			  
			 var href =SITE_LINK+"add_comment?id="+id+"&comment="+comment;	
			 	
				 $.ajax({   
					type: "GET",   
					cache: false,   
					url: href,   			
					success: function(data){   
						data = JSON.parse(data);
						if(data=="success")
						{							
							href =SITE_LINK+"fetch_comments?id="+id;
							comments_show(href);
							$("#comment").val('');
						}
					}
				});
			};
		});
		
		$(document).on('change','#searchTag',function(){
		//$("#searchTag").change(function(e){
			var val=$("#searchTag option:selected").val();
			resetFrom();
			if(val!="")	{ 
				val = val.replace("#",'');
				 var href =SITE_LINK+"showtags?tag="+val;	
				 leftbareventdata(href);
			}
			else
			{
				month=$("#eventdate").val();
				var href =SITE_LINK+"show_myevent?month="+month;
				//$(".event-list").html("<li class='white'>No event to display<li>");
			}
		});
		
		function leftbareventdata(href)
		{
			$.ajax({  type: "GET",   
						cache: false,   
						url: href,   			
						success: function(data){   
							data = JSON.parse(data);																
							if ( data.length > 0 ) {
								var tmpNew = data_event;							
								$(".event-list").empty();
								$.each(data,function(key,val){
										
									if(val['title']=="")
									title="Title Here"
									else
									title=val['title'];
									
									var className = "";
							//var currentdate=currentDate();
							var fullDate=new Date();
							var currentdate =fullDate.getDate();
							var current_month =fullDate.getFullYear()+"-"+(fullDate.getMonth()+1)+"-"+fullDate.getDate();
							
							
							var fullDate1=new Date(val['eventdate']);
							var eventdate =fullDate1.getDate();
							var month=$(this).attr("value");	
							//var selected_month =fullDate1.getFullYear()+"-"+(fullDate1.getMonth()+1);
							//selected_month=selected_month+"-"+fullDate.getDate();
							//selected_month=month;
							//console.log(selected_month);
							//console.log(current_month);
							if(eventdate < currentdate)
							{ 
								if(month==current_month)
								{
									
									if(val['event_status'] == 1)
									{
										className="event-new" ;
									}
									else
									{
										className="event-today" ;
									}
								}
								else if(month < current_month)
								{
									
									className="event-old" ;
								}
								 else{
									if(val['event_status'] == 1)
									{
										className="event-new" ;
									}
									else
									{
										className="event-today" ;
									}
								}
							
							} 
							else if(eventdate == currentdate || eventdate > currentdate){ if(val['event_status'] ==1 ) {className="event-new" ; } else {className="event-today" ; } } 						 
							else { className="event-new" ;}				
									var start_hour=formatAMPM(val["start_hour"]);		
									tmpNew = data_event.replace(/{IMAGE}/g,val["image"]);
									tmpNew = tmpNew.replace(/{TITLE}/g,title);							
									tmpNew = tmpNew.replace(/{STARTHOUR}/g,start_hour);
									tmpNew = tmpNew.replace(/{ID}/g,val['id']);
									tmpNew = tmpNew.replace(/{SITE_LINK}/g,SITE_LINK);
									tmpNew = tmpNew.replace(/{CLASS}/g,className);
									$(".event-list").append(tmpNew);
									loadFlag1 = true;
								});
							} else {
								flag = false;
								loadFlag1 = false;
								$(".event-list").html("<li class='white'>No event to display<li>");
							}
						}  
					});
		}
		
		$("#CalendarHashtags").focusout(function(event){
			event.preventDefault();		
			 var vals="<li><a href='javascript:void(0)'>"+$(this).val()+" </a><button type='button' class='delete_button' value="+$(this).val()+"><img src='"+SITE_LINK+"img/cros-icon.png' alt='' onclick='delete_tag_data(this.value)'/></button></li>";		
			 if($("#CalendarHashtags").val()!=""){
				 if($("#CalendarTags").val()=="")
				 var valtag=$(this).val()+ $("#CalendarTags").val();
				 else
				 var valtag=$(this).val()+","+ $("#CalendarTags").val();
				  $(".tags").append(vals);
				  $("#CalendarHashtags").val('');
				  $("#CalendarTags").val(valtag);
				  $("#CalendarTags").removeClass("error");
				  $("#CalendarHashtags").removeClass("error");
				  
			  }
			});
});

//~ if(eventdate < currentdate)
//~ { 
	//~ if(month==current_month)
	//~ {
		//~ 
		//~ if(val['event_status'] == 1)
		//~ {
			//~ className="event-new" ;
		//~ }
		//~ else
		//~ {
			//~ className="event-today" ;
		//~ }
	//~ }
	//~ else if(month < current_month)
	//~ {
		//~ 
		//~ className="event-old" ;
	//~ }
	 //~ else{
		//~ if(val['event_status'] == 1)
		//~ {
			//~ className="event-new" ;
		//~ }
		//~ else
		//~ {
			//~ className="event-today" ;
		//~ }
	//~ }
//~ 
//~ } 
//~ else if(eventdate == currentdate || eventdate > currentdate){ if(val['event_status'] ==1 ) {className="event-new" ; } else {className="event-today" ; } } 						 
//~ else { className="event-new" ;}
