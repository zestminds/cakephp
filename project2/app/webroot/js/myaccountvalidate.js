	$(document).ready(function () {
	 $('#flashMessage').delay(5000).fadeOut();
    $('#UserEditForm').validate({ // initialize the plugin
        rules: {			
			 
			  "data[UserDetail][name]": {
				required: true,				
			  },
			  "data[UserDetail][address]": {
				required: true				
			  }	,
			  "data[UserCategory][category_id]": {
				required: true				
			  },
			  "data[UserDetail][business]": {
				required: true				
			  }		
			},
			// Specify validation error messages
			messages: {			
				"data[UserDetail][name]": {
					required: "Please enter Name"			
				},			 
				"data[UserDetail][address]": {
					required: "Please enter Address"			
				},			 
				"data[UserDetail][business]": {
					required: "Please add business name"			
				}
			},
			// Make sure the form is submitted to the destination defined
			// in the "action" attribute of the form when valid
			submitHandler: function(form) {
			  form.submit();
			}
		  });	
	 /*$('#sample_input').awesomeCropper(
		{ width: 163, height: 163, debug: true }
     );	*/
    var flag = true;
	$(".img-upload").on("click",function(){
		//var reader = new FileReader();
		//reader.onload = function(e) {
			/*var CanvasCrop = $.CanvasCrop({
				cropBox : ".imageBox",
				imgSrc : avatar,
				limitOver : 2
			});
			rot =0 ;
			ratio = 1;*/
		//}
		//reader.readAsDataURL(avatar);
		$("#imageModal").modal("show");
		
	});
	/*
	$(function(){
        var rot = 0,ratio = 1;
        var CanvasCrop = $.CanvasCrop({
            cropBox : ".imageBox",
            imgSrc : avatar,
            limitOver : 2
        });
        
        
        $('#upload-file').on('change', function(){
            var reader = new FileReader();
            reader.onload = function(e) {
                CanvasCrop = $.CanvasCrop({
                    cropBox : ".imageBox",
                    imgSrc : e.target.result,
                    limitOver : 2
                });
                rot =0 ;
                ratio = 1;
            }
            reader.readAsDataURL(this.files[0]);
            flag = false;
            //this.files = [];
        });
        
        $("#rotateLeft").on("click",function(){
            rot -= 90;
            rot = rot<0?270:rot;
            CanvasCrop.rotate(rot);
        });
        $("#rotateRight").on("click",function(){
            rot += 90;
            rot = rot>360?90:rot;
            CanvasCrop.rotate(rot);
        });
        $("#zoomIn").on("click",function(){
            ratio =ratio*0.9;
            CanvasCrop.scale(ratio);
        });
        $("#zoomOut").on("click",function(){
            ratio =ratio*1.1;
            CanvasCrop.scale(ratio);
        });
        $("#alertInfo").on("click",function(){
            var canvas = document.getElementById("visbleCanvas");
            var context = canvas.getContext("2d");
            context.clearRect(0,0,canvas.width,canvas.height);
        });
        
        $(".btn-primary").on("click",function(){
            //console.log($("#visbleCanvas").attr("style"));
            //var src = CanvasCrop.getDataURL("jpeg");
            if ( flag ) {
				var src = CanvasCrop.getDataURL1("png");
			} else {
				var src = CanvasCrop.getDataURL("png");
			}
            //$("body").append("<div style='word-break: break-all;'>"+src+"</div>");  
            //$(".container").append("<img src='"+src+"' />");
			//console.log(src);
			$(".img-upload").attr("style","background-image:url('"+src+"')");
			$("#tmpImage").val(src);
			$("#imageModal").modal("hide");
			avatar = src;			
			
		});
        
       
    }); */
	
	$("#UserCategoryCategoryId").on("focus",function(){
		//if ( $(this).val().trim() != "" ) {
			getresults($(this).val().trim());
		//}
	});
	
	$("#UserCategoryCategoryId").on("keyup",function(){
		//if ( $(this).val().trim() != "" && ($(this).val().trim().length >= 3) ) {
			getresults($(this).val().trim());
		//}
	});
	$("#UserCategoryCategoryId").on("blur",function(){
		setTimeout(function(){ $("#inresult").html(""); $(".result").hide(); },1000);
		//$("#inresult").html("");
		//$(".result").hide();
	});
	
	var listCategories = "<li id='{ID}' class='selcat'>{VALUE}</li>";
	var getresults = function(keyword){
		$("#inresult").html("");
		$(".result").hide()
		$.get(SITE_LINK+"users/getcategories?key="+keyword, function(data, status){
			var obj = JSON.parse(data);
			//console.log(obj);
			tmpNew = listCategories;
			$.each(obj,function(i,val){
				tmpNew = listCategories.replace(/{ID}/g,i);
				tmpNew = tmpNew.replace(/{VALUE}/g,val);
				$("#inresult").append(tmpNew);
			});
		});
		$(".result").show();
	}
	$(document).on("click",".selcat",function(){
		$("#UserCategoryCategoryIdTmp").val($(this).attr("id"));
		$("#UserCategoryCategoryId").val($(this).html());
	});
	
	
	 $(document).on("click","#subscribelist",function(e){
		 var href =SITE_LINK+"subscribelist";		
		 $.ajax({   
			type: "GET",   
			cache: false,   
			url: href,   
			success: function(data){
				data = JSON.parse(data);
				$("#subscribelist").html((data.message));
			}  
		});		
	});
	
	$(document).on("click","#deleteuseraccount",function(e){
		 var href =SITE_LINK+"deleteuseraccount";		
		 $.ajax({   
			type: "GET",   
			cache: false,   
			url: href,   
			success: function(data){
				data = JSON.parse(data);
				if(data.message=="Deleted")
				window.location.replace(SITE_LINK+"logout");

			}  
		});		
	});
	$(document).on("click","#pausepaypalpayment",function(e){
		 var href =SITE_LINK+"pausepaypalpayment";		
		 $.ajax({   
			type: "GET",   
			cache: false,   
			url: href,   
			success: function(data){
				data = JSON.parse(data);				
				if(data.message=="0"){
					$("#pausepaypalpayment").html(("Re-activate Payment"));
				}
				else if(data.message=="1")
				{
					$("#pausepaypalpayment").html(("Pause Payment"));
				}
				else{}
					
			}  
		});		
	});
	
});
