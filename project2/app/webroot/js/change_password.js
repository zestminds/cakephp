	$(document).ready(function () {

    
    $('#UserChangepasswordForm').validate({ // initialize the plugin
        rules: {
					  
			 "data[User][currentpassword]": {
				required: true				
			  },
			   "data[User][newpassword]": {
				required: true,
				minlength: 6,
				maxlength: 20		
			  },
			
			"data[User][confirmpassword]": {
				required: true,				
				equalTo: '#UserPassword'
			  }
			},
			// Specify validation error messages
			messages: {			
				 "data[User][currentpassword]": {
				required: "Please provide current password",				
			  },  
			  "data[User][newpassword]": {
				required: "Please provide new password",
				minlength: "Your password must be at least 6 characters long",
				maxlength: "Your password not be larger than 20 characters"
			  },
			  "data[User][confirmpassword]": {
				required: "Please provide confirm password"			
				
			  }
			},
			
			submitHandler: function(form) {
			  form.submit();
			}
		  });
		  
		  jQuery.validator.addMethod( 'passwordMatch', function(value, element) {
    
			// The two password inputs
			var password = $("data[User][newpassword]").val();
			var confirmPassword = $("data[User][confirmpassword]").val();

			// Check for equality with the password inputs
			if (password != confirmPassword ) {
				return false;
			} else {
				return true;
			}

		}, "Your Passwords Must Match");
});

