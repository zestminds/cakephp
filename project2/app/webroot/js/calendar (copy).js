	//var SITE_LINK="http://localhost/projects/fishook/v1/";
$(document).ready(function () {	
	var iFrame = $('#mycalendar');
	$(function() {

            /**
             * Will wait for an iframe to be ready
             * for DOM manipulation. Just listening for
             * the load event will only work if the iframe
             * is not already loaded. If so, it is necessary
             * to observe the readyState. The issue here is
             * that Chrome will initialize iframes with
             * "about:blank" and set its readyState to complete.
             * So it is furthermore necessary to check if it's
             * the readyState of the target document property.
             * Errors that may occur when trying to access the iframe
             * (Same-Origin-Policy) will be catched and the error
             * function will be called.
             * @param {jquery} $i - The jQuery iframe element
             * @param {function} successFn - The callback on success. Will 
             * receive the jQuery contents of the iframe as a parameter
             * @param {function} errorFn - The callback on error
             */
            var onIframeReady = function($i, successFn, errorFn) {
                try {
                    const iCon = $i.first()[0].contentWindow,
                        bl = "about:blank",
                        compl = "complete";
                    const callCallback = () => {
                        try {
                            const $con = $i.contents();
                            if($con.length === 0) { // https://git.io/vV8yU
                                throw new Error("iframe inaccessible");
                            }
                            successFn($con);
                        } catch(e) { // accessing contents failed
                            errorFn();
                        }
                    };
                    const observeOnload = () => {
                        $i.on("load.jqueryMark", () => {
                            try {
                                const src = $i.attr("src").trim(),
                                    href = iCon.location.href;
                                if(href !== bl || src === bl || src === "") {
                                    $i.off("load.jqueryMark");
                                    callCallback();
                                }
                            } catch(e) {
                                errorFn();
                            }
                        });
                    };
                    if(iCon.document.readyState === compl) {
                        const src = $i.attr("src").trim(),
                            href = iCon.location.href;
                        if(href === bl && src !== bl && src !== "") {
                            observeOnload();
                        } else {
                            callCallback();
                        }
                    } else {
                        observeOnload();
                    }
                } catch(e) { // accessing contentWindow failed
                    errorFn();
                }
            };

           // var $iframe = iFrame;
            onIframeReady(iFrame, function($contents) {
                console.log("Ready to got");
                console.log($contents.find("*"));
            }, function() {
                console.log("Can not access iframe");
            });
        });
	
		
	
	//Calender
	iFrame.bind('load', function() {
	//$("iframe").load(function(){
console.log("show_event3");
        $(this).contents().on("click",".nextmonth", function(){
           var month=$(this).attr("value");	
          
           var fullDate=new Date(month);
           var year=fullDate.getFullYear();
           var monthcurent =('0' + (fullDate.getMonth()+1)).slice(-2)
           
           
           var today_date=new Date();
           //( '0' + (myDate.getMonth()+1) ).slice( -2 );
           today_date=( '0' + (today_date.getDate()) ).slice( -2 );
         
			if(monthcurent=="02" || monthcurent=="04" || monthcurent=="06" || monthcurent=="09" || monthcurent=="11" ){
				today_date1=new Date(year, monthcurent, 0).getDate();
				if(today_date1!=today_date)
				{
					today_date=today_date1;
				}				   
			}
         
         
           var full_date=year+"-"+monthcurent+"-"+today_date;
         
           theDate= new Date(full_date).getTime(); 
           
           var monthdate=getTheDate(full_date);             
           $("#date").html(monthdate);
           	
            $("#eventdate").html(full_date);	
            
          $(".event-list").empty();
          $("current_month_selected").val(month);		
			var url_calendar="calendar?month="+month+"&set_yes=no";	
			$('#mycalendar').attr('src', url_calendar);
			
			var fullDate=new Date();			
			var currentdate =year+"-"+monthcurent+"-"+fullDate.getDate();
			
			href =SITE_LINK+"show_myevent?month="+currentdate;
			
			nextprevmonthEdit(href,currentdate);
			leftbareventdata(href);
			resetFrom();
        });
        console.log("show_event2");
		function nextprevmonthEdit(href,currentdate)
		{
			 $.ajax({   
				type: "GET",   
				cache: false,   
				url: href, 
				success: function(data){   
					data = JSON.parse(data);
					//console.log(data);			
					if ( data.length > 0 ) {
						var tmpNew = data_event;
						
						//$(".event-list").text="sfds";
						//$(".event-list").empty();
						//console.log(WWW_ROOT);
						var id_live=id_save=0;
						var event_status;
						var tmplink="";
						$.each(data,function(key,val){
							
							if(val['event_status'] == 1)
							{
								id_live=val['id'];
								event_status=1;							
								tmplink=val['title'];
							}
							else if(id_save == 0)
							{
								id_live=val['id'];
								event_status=0;
								tmplink=val['title'];
							}
							id_save=1;
							var className = "";
							//var currentdate=currentDate();
							var fullDate=new Date();
							var currentdate =fullDate.getDate();
							var current_month =fullDate.getFullYear()+"-"+(fullDate.getMonth()+1)+"-"+fullDate.getDate();
							
							
							var fullDate1=new Date(val['eventdate']);
							var eventMonth =fullDate1.getMonth()+1;	
							
						    loadFlag = true;
						});
						
						 edit_event(id_live,currentdate);
						  $("#tmpdate").val(currentdate);	
					} else {
						reset_from_after_edit();
					}
				}  
			});	
		}
		console.log("show_event");
        $(this).contents().on("click",".show_myevent", function(){
			
		reset_from_after_edit();
		$('[id=searchTag]').val( '' );
			
			//$('#searchTag option:eq("all")').prop('selected', true)
		   tmplink = $(this).attr("action");			
           var month=$(this).attr("value");	              
           var fullDate=new Date(month);
           var year=fullDate.getFullYear();         
           var monthselected =('0' + (fullDate.getMonth()+1)).slice(-2);           
           var today_date=( '0' + (fullDate.getDate())).slice( -2 );
           var full_date=year+"-"+monthselected+"-"+today_date;        
            
            
             var monthdate=getTheDate(full_date);             
             $("#date").html(monthdate);	
             //console.log($("#date").val());
             $("#eventdate").val(full_date);	
             theDate= new Date(full_date).getTime();
	
	
			var fullDatecurrent=new Date();
			var current_month =fullDatecurrent.getFullYear()+"-"+('0' + (fullDatecurrent.getMonth()+1)).slice( -2 )+"-"+( '0' + (fullDatecurrent.getDate())).slice( -2 );
             var imagevent="";
			 var href =SITE_LINK+"show_myevent?month="+month;		
			 resetFrom();
			 $.ajax({   
				type: "GET",   
				cache: false,   
				url: href, 
				success: function(data){
					console.log(data);
					//~ alert(data);
					//~ if(data=='"error_auth"'){		window.location.href=SITE_LINK ;}
					//~ else
					//~ {
					data = JSON.parse(data);
					//console.log(data);			
					if ( data.length > 0 ) {
						var tmpNew = data_event;
						var ff="";
						//$(".event-list").text="sfds";
						$(".event-list").empty();
						//console.log(WWW_ROOT);
						var id_live=id_save=0;
						var event_status;
						$.each(data,function(key,val){
							
							if(val['event_status'] == 1)
							{
								id_live=val['id'];
								event_status=1;							
								var tmplink=val['title'];
							}
							else if(id_save == 0)
							{
								id_live=val['id'];
								event_status=0;
								var tmplink=val['title'];
							}
							id_save=1;						
							if(val['title']=="" || val['title'] === null)
							title="Title Here"
							else
							title=val['title'];
							
							var className = "";
							//var currentdate=currentDate();
							
							var currentdate =fullDatecurrent.getDate();
														
							
							var fullDate1=new Date(val['eventdate']);
							var eventMonth =fullDate1.getMonth()+1;
							
							var cMonth=fullDate.getMonth()+1;
							
							var click_date=new Date(month);
							day=click_date.getDate();	
										
							var start_hour=formatAMPM(val["start_hour"]);		
							tmpNew = data_event.replace(/{IMAGE}/g,val["image"]);
							tmpNew = tmpNew.replace(/{TITLE}/g,title);							
							tmpNew = tmpNew.replace(/{STARTHOUR}/g,start_hour);
							tmpNew = tmpNew.replace(/{ID}/g,val['id']);
							tmpNew = tmpNew.replace(/{EVENTDATE}/g,val['eventdate']);
							tmpNew = tmpNew.replace(/{SITE_LINK}/g,SITE_LINK);
							tmpNew = tmpNew.replace(/{CLASS}/g,className);
							$(".event-list").append(tmpNew);
						    loadFlag = true;
						});
						
						 edit_event(id_live,tmplink);
						  $("#tmpdate").val(tmplink);	
					} else {
						flag = false;
						loadFlag = false;
						$(".event-list").html("<li class='white'>No event to display<li>");
					}
					//~ }
					
				}  
			});			
				
				var extra_row=$("#mycalendar").contents().find('#extra_row').val();				
				var k=1-parseInt(extra_row );				
				var today_date=new Date();
				today_date =today_date.getDate();	
				$(this).parent("td").parent("tr").parent("tbody").children("tr").each(function(){
					$(this).children("td").each(function(){
						//cut
						current_month=('0' + (fullDatecurrent.getMonth()+1)).slice( -2 );
						if(k==today_date)
						{ $(this).removeClass("active");
							if(monthselected==current_month)
							{
								
								$(this).addClass("today");		
								//$(this).closest( "td" ).toggleClass('classname');
								var cl =$(this).closest( "td" ).attr('class');
								if(cl.indexOf('savetoday') != -1){
									$(this).removeClass("savetoday");								
									$(this).addClass("saveToday");
								}
							}
							
						}
						else{
							$(this).removeClass("active");
						}
						var monthdate =('0' + (fullDate.getDate())).slice(-2);
				
						if(monthdate==today_date)
						{
							var cl =$(this).closest( "td" ).attr('class');	
							$(this).removeClass("today");			
							if(cl.indexOf('saveToday') != -1){
								
								
								$(this).removeClass("saveToday");$(this).addClass("save");
							}
							
						}
						k=k+1;
								
					});
				});
							
			     
				$(this).parent().addClass("active");
        });
		
		//~ $(this).contents().on('click', '.date-select-table', function () {alert("Sdf");		
			//~ if ( $(this).hasClass('active') ) {
				//~ $(this).removeClass('active');
			//~ }
		//~ });
		
		function imageExists(url, callback) {
		var img = new Image();
		img.onload = function() { callback(true); };
		img.onerror = function() { callback(false); };
		img.src = url;
	  }
	  
	  
	 
	});	
	
	
	var event_live_id=$("#event_live_id").val();	
	if(event_live_id!="")
	{
		var today = new Date();						
		var currentdate= today.getFullYear() + '-' +('0'+(today.getMonth()+1)).slice(-2) + '-' + ('0' +today.getDate()).slice(-2);
		edit_event(event_live_id,currentdate);	
		$("#event_live_id").val('');
		//var end = new Date(currentdate);
		//timer = setInterval(showRemaining, 1000);
	}
	
  
	
	loadFlag1 = false;	
	var months = ["Jan", "Feb", "Mar",
		"April", "May", "June", "July", "Aug",
		"Sept", "Oct", "Nov", "Dec"
	  ];
var flagsubmit1 = false;
var flagsubmit = false;
var flagsave=false;
var showhide=true;
var yes_showhide="0";
$.validator.addMethod("currency", function (value, element) {
  return this.optional(element) || /^\$(\d{1,3}(\,\d{3})*|(\d+))(\.\d{2})?$/.test(value);
}, "Please specify a valid amount");
$("#CalendarIndexForm").submit(function(e) {    e.preventDefault(); }).validate({ // initialize the plugin
	 ignore: "",
	 rules: {			
		  "data[Calendar][tmpImage]": {
			required: true	
		  },
		   "data[Calendar][title]": {
			required: true	
		  },
		   "data[Calendar][short_dec]": {
			required: true	
		  },
		   "data[Calendar][tags]": {
			required: true	
		  },
		   "data[Calendar][location]": {
			required: true	
		  },
		  "data[Calendar][start_price]": {
			required: true,			
		  },
		  "data[Calendar][start_hour]": {
			required: true	
		  }
		},		
		messages: {		
		
		},	
		 errorPlacement: function(){
			if($('#tmpImage').val()=="")
			{
				$("#drop-zone").addClass("error_full");
			}
			if($('#CalendarTags').val()=="")
			{
				$("#CalendarHashtags").addClass("error");
			}
			$(".red-bg").show();
			$(".gray-border").hide();
            return false;  // suppresses error message text
        },	
		submitHandler: function(e) {	
			$(".red-bg").hide();
			//if(flagsubmit1 && flagsubmit){	
				
				if(yes_showhide=="1")	{
					showhideEvent();	
					setTimeout(function() {   //calls click event after a certain time
					
						if(showhide)
						{
							//if(showhide)
								//{	
									$('.save').attr('disabled',true);	
									showLoading();						
									save_data_ajax();							
									setTimeout(function() {   //calls click event after a certain time
									   aftersave();
									}, 5000);
								//}
						}
						else
						{
							return false;
						}
					}, 1000);	
											
				}
		//	}
			//form.submit();
		}
	  });
	
	function aftersave()
	{
		if(flagsave){	
			
			count_total_events();	
			tagList();
			month=$("#eventdate").val();
			if($("#tmpdate").val()=="")
			{
				month=$("#eventdate").val();
			}
			else
			{
				var month=$("#tmpdate").val();
				$(".gray-border").hide();
			}
			
			edit_event(id,$("#tmpdate").val());
			var href =SITE_LINK+"show_myevent?month="+month;			
			leftbareventdata(href);	
			//var url_calendar="calendar?month="+month;
			var sel_date=$("#eventdate").val();
			var old_month=$("#tmpdate").val();
			if(sel_date!=old_month)
			var sel_date=$("#tmpdate").val();
			else
			var sel_date=$("#eventdate").val();
				
			var url_calendar="calendar?month="+month+"&sel_date="+sel_date;		
			$('#mycalendar').attr('src', url_calendar);
			hideLoading();
		}
	}	
	
	function showLoading() {
    $("#loading").show();
	}

	function hideLoading() {
		$("#loading").hide();
	}		
	function tagList()
	{
		$("#drop_tags").empty();
		 var href =SITE_LINK+"taglist";		
			 $.ajax({   
				type: "GET",   
				cache: false,   
				url: href,
				dataType: 'json',   			
				success: function(data){
					var searchTag ='<select id="searchTag" name="searchTag"><option value="">Search By tag</option><option value="all">All</option>';
					
					if ( data.length > 0 ) {
						 $.each(data, function (index, val) {
						  searchTag  += '<option value="'+val['id']+'">'+val["tag"]+'</option>';							
						})		
					}	
							
					searchTag +="</select>";	
					
					$("#drop_tags").html(searchTag);				
				}
			});		
	}
	var n="";
	var searchTag="";
	//$( ".sendNotification").keyup(function( event ) {
	function list_user_mention()
	{
		//var my_string =$("#comment").html();
		
		var id =$("#id").val();
		var month =$("#tmpdate").val();
		//n = my_string.split(" ");
		
		//n=n[n.length - 1];
		
		//var last_character = my_string[my_string.length-1];console.log(last_character);

			event.preventDefault();		
			var href =SITE_LINK+"tag_list_comment?month="+month+"&id="+id;		
			 $.ajax({   
				type: "GET",   
				cache: false,   
				url: href,   			
				success: function(data){   
					data = JSON.parse(data);
					$("#list_user_comment").show();
					//var searchTag ='<ul>';
				
					if ( data.length > 0 ) {
						 $.each(data, function (index, val) {
						  searchTag  += "{value:'"+val['business']+"',uid:'user:"+val['business']+"},";							
						})		
					}	
					//searchTag +="</ul>";
					//$("#list_user_comment").html(searchTag);
					console.log(searchTag);
					return searchTag;
				}
			});		
		 	
	}
	$('textarea.mentions').mentionsInput({
		onDataRequest:function (mode, query, onDataRequestCompleteCallback) {
		var id =$("#id").val();		
		var month =$("#tmpdate").val();
		var my_string =$("#comment").html();
		var href =SITE_LINK+"tag_list_comment?month="+month+"&id="+id;	
		$.ajax({
                url: href,
                method: "GET",
               // data: {keyword: keyword},
                dataType: "json",
                success: function (response) {
                   var data = response;
                   // console.log(data);
                    data = jQuery.grep(data, function( item ) {
                        return item.name.toLowerCase().indexOf(keyword.toLowerCase()) > -1;
                    });
                    onDataRequestCompleteCallback.call(this, data);
                    // setTimeout(data, 1000); 
                }
           });
		}
	});
	
	
	$("#btnsave").on("click",function(){
		yes_showhide=0;
		showhideEvent1();
		setTimeout(function() {   //calls click event after a certain time					
			if(showhide==true)
			{
				$('.save').attr('disabled',true);	
				showLoading();	
				save_data_ajax();							
					setTimeout(function() {   //calls click event after a certain time
					   aftersave();
					}, 8000);
			}
		}, 1000);
	});
	$("#showevent").on("click",function(){
		yes_showhide=1;
	});
	function showhideEvent1(){	
		var event_status=$('#event_status').val();
		var month=$('#eventdate').val();
		var id=$('#id').val();
		//reset_from_after_edit();
		yes_showhide=1;		
			console.log(event_status);
		if(event_status==1)
		{
			 var href =SITE_LINK+"count_user_events?month="+month+"&id="+id;		
			 $.ajax({   
				type: "GET",   
				cache: false,   
				url: href,   			
				success: function(data){   
					data = JSON.parse(data);
					$(".upgrade-pop-day").hide();
					$(".upgrade-pop").hide();
					if(data=="max")
					{
						$(".upgrade-pop").show();
						showhide=false;						
					}
					else if(data=="max_day_count")
					{
						$(".upgrade-pop-day").show();
						showhide=false;
					}
					else{
						showhide=true;
					}
				}
			});		
			
		}
		else
		{
			showhide=true;
		}
	}	
	
	
	
	
	function showhideEvent(){	
		var event_status=$('#event_status').val();
		var month=$('#eventdate').val();
		var id=$('#id').val();
		//reset_from_after_edit();
		yes_showhide=1;		
		console.log(event_status);
		if(event_status==0)
		{console.log(event_status);
			 var href =SITE_LINK+"count_user_events?month="+month+"&id="+id;		
			 $.ajax({   
				type: "GET",   
				cache: false,   
				url: href,   			
				success: function(data){   
					data = JSON.parse(data);
					$(".upgrade-pop-day").hide();
					$(".upgrade-pop").hide();
					if(data=="max")
					{
						$(".upgrade-pop").show();
						showhide=false;						
					}
					else if(data=="max_day_count")
					{
						$(".upgrade-pop-day").show();
						showhide=false;
					}
					else {
						$('#event_status').val(1);		
						$('#showevent').val("Hide Event on Rave"); 
						$("#showevent").addClass("hide-event");	
						showhide=true;	
										
						//$(".gray-border").show();
						//$(".live-event").show();
						
						//~ var today = new Date();						
						//~ var currentdate= today.getFullYear() + '-' +('0'+(today.getMonth()+1)).slice(-2) + '-' + ('0' +today.getDate()).slice(-2);
						
						 //~ if (month < currentdate ) { 
							 //~ $(".live-event").html("This Event was live on "+month);
						 //~ }
						 //~ else
						 //~ {
							  //~ $(".live-event").html("This Event will be live on "+month);
						 //~ }
						
					}
				}
			});		
			
		}
		else
		{
			$('#event_status').val(0);
			$('#showevent').val("Show Event on Rave")
			$("#showevent").removeClass("hide-event");
			$(".live-event").hide();				
			$(".live-event").html("");
			showhide=true;	
		}
	}	
	
	$(document).on("click",".delete_button",function(){
		
		var itemtoRemove=($(this).attr("value"));		
		var result = $("#CalendarTags").val().split(',');
		for(var i = result.length-1; i >= 0; i--){  
			if(result[i] == itemtoRemove){         
				result.splice(i,1);           
			}
		}	
		var result=result.join(",");
		 $("#CalendarTags").val(result);
			$(this).parent().remove();	
	});
	
	$( ".deletetag").click(function( event ) {
		 $("#CalendarHashtags").val('');
	});
	
	
	$("#img-upload,#drop-zone").on("click",function(){
		$("#imageModal").modal("show");
	});
	
	$("#ticket_free").on("click",function(){
		if($("#ticket_free").prop('checked') == true){
			$("#CalendarStartPrice").val('');
			$("#CalendarEndPrice").val('');
			$( "#CalendarStartPrice" ).prop( "disabled", true );
			$( "#CalendarEndPrice" ).prop( "disabled", true );
		}
		else
		{			
			$("#CalendarStartPrice").prop( "disabled", false );
			$("#CalendarEndPrice").prop( "disabled", false );
		}
	});	
     $("#clear_all").on("click",function(){
		 $('#CalendarIndexForm')[0].reset();		 
	});	 
	
	$(".close_upgrade").on("click",function(){
		$(".upgrade-pop").hide();
		$(".upgrade-pop-day").hide();
		showhide=false;
	});
	$(".upgrade_now").on("click",function(){
		window.location.href=SITE_LINK+"my-account";
	});
	$(document).on("click",".plus-add",function(){	
		reset_from_after_edit();
		resetFrom();
	});
	function resetFrom()
	{
		$(".plus-add").hide();	
		$(".tags").empty();		
		$('input,textarea,select').removeClass("error");	
		$('.red-bg').hide();
		$('.gray-border').show();
		$('.live-event').hide();
		$("#drop-zone").removeClass("error_full");			
		$("#CalendarHashtags").removeClass("error");	
		$('.upload-file img').attr("src",SITE_LINK+"img/img-icon1.png");
		$('.img-container img').attr("src",SITE_LINK+"img/img-icon.png");
		$('#event_status').val(0);
		$('#id').val('');		
		$('#CalendarTags').val('');		
		$('#showevent').val("Show Event on Rave")
		$("#showevent").removeClass("hide-event");	
		$(".event-noty").hide();$(".event_show_comments").hide();$(".gray-bg").hide();		
		$('#CalendarIndexForm')[0].reset();
		$(".popup-event").hide();
		$( "#CalendarStartPrice" ).prop( "disabled", false );
		$( "#CalendarEndPrice" ).prop( "disabled", false );
		var date=$('#tmpdate').val();	
		$('#eventdate').val(date);			
	}
	
	function reset_from_after_edit()
	{
		$(".edit_event").hide();
		$(".save_event").show();		
		$(".nolive-bg").hide();
		$('.save').attr('disabled',false);
		$(".live-event-bg").hide();		
		$('#CalendarIndexForm input,textarea').prop('readonly', false);
		$('#CalendarIndexForm select').prop('disabled', false);	
		$('#ticket_free').prop('disabled', false);	
		$('.cart_total_items').hide();				
		$('.edit').attr('disabled',false);
		$('#clear_all').attr('disabled',false);
		$('.nolive-bg').hide();
		//$('#showevent').attr('readonly',false);	
		// Show Event Button//
		//document.getElementById('showevent').style.pointerEvents = 'auto';
		// Show Event Button//
		document.getElementById('drop-zone').style.pointerEvents = 'auto';
		document.getElementById('img-upload').style.pointerEvents = 'auto';
		document.getElementById('pac-input').style.pointerEvents = 'auto'; 
		document.getElementById('ticket_free').style.pointerEvents = 'auto';
		document.getElementById('next').style.pointerEvents = 'auto';
		document.getElementById('prev').style.pointerEvents = 'auto';	
	}	
	
	//var pre=1;
	if (!Date.now) {
	  Date.now = function() {
		return new Date().getTime();
	  }
	}
	
	
	
	var theDate = Date.now();

	document.getElementById('date').innerText = getTheDate(theDate)
	var a = 1;
	var b = '';
	var cmonth = new Date().getMonth()+1;
	document.getElementById('prev').addEventListener("click", function() {
	  var prevDate=theDate-86400000;
	  var prevDay=getTheDate(prevDate);
	  theDate = prevDate;
	  document.getElementById('date').innerText = prevDay;
	  if($("#mycalendar").contents().find('a[value="'+$("#eventdate").val()+'"]').length){
		
		$("#mycalendar").contents().find('.date-select-table td.day.active').removeClass('active');
		$("#mycalendar").contents().find('a[value="'+$("#eventdate").val()+'"]').parent().addClass('active');
	  }
	   var  curmonth= new Date($("#eventdate").val());
	   
		var pday = a =curmonth.getDate();
		
		  if(pday != cmonth)
		  { 
			  cmonth = pday;
			var prvmonth= curmonth.getFullYear() + '-' +('0'+(curmonth.getMonth()+1)).slice(-2) + '-' + ('0' +curmonth.getDate()).slice(-2);	
			var url_calendar="calendar?month="+prvmonth;		
			var url_calendar="calendar?month="+prvmonth+"&sel_date="+prvmonth;				
			//var url_calendar="calendar?month="+data[0]["eventdate"];	
			$('#mycalendar').attr('src', url_calendar);	
	  }
	  //~ if($('#event_status').val()==1)
	  //~ {
		//~ var today = new Date();						
		//~ var currentdate= today.getFullYear() + '-' +('0'+(today.getMonth()+1)).slice(-2) + '-' + ('0' +today.getDate()).slice(-2);
		//~ var month=$("#eventdate").val();
		
		 //~ if (month < currentdate ) { 
			 //~ $(".live-event").html("This Event was live on "+month);
		 //~ }
		 //~ else
		 //~ {
			  //~ $(".live-event").html("This Event will be live on "+month);
		 //~ }
		//~ }
	})
	document.getElementById('next').addEventListener("click", function() {
	  var nextDate=theDate+86400000;	  
	  var nextDay=getTheDate(nextDate);
	  theDate = nextDate;
	  document.getElementById('date').innerText = nextDay;
	  
	  //console.log($("#mycalendar").contents().find('a[value="'+$("#eventdate").val()+'"]'));
	  if($("#mycalendar").contents().find('a[value="'+$("#eventdate").val()+'"]').length){
	
		$("#mycalendar").contents().find('.date-select-table td.day.active').removeClass('active');
		$("#mycalendar").contents().find('a[value="'+$("#eventdate").val()+'"]').parent().addClass('active');
	  }
	  
		var  curmonth= new Date($("#eventdate").val());
		var day = b =curmonth.getDate();
		
		  if(day != cmonth)
		  { 
			  cmonth = day;
			var nextmonth= curmonth.getFullYear() + '-' +('0'+(curmonth.getMonth()+1)).slice(-2) + '-' + ('0' + curmonth.getDate()).slice(-2);			
			//var url_calendar="calendar?month="+nextmonth;	
			
			var url_calendar="calendar?month="+nextmonth+"&sel_date="+nextmonth;	
			//('0' + date.getDate()).slice(-2)			
			//var url_calendar="calendar?month="+data[0]["eventdate"];	
			$('#mycalendar').attr('src', url_calendar);	
		  }
		 //~ if($('#event_status').val()==1)
		//~ {
			//~ var today = new Date();						
			//~ var currentdate= today.getFullYear() + '-' +('0'+(today.getMonth()+1)).slice(-2) + '-' + ('0' +today.getDate()).slice(-2);
			//~ var month=$("#eventdate").val();		
			 //~ if (month < currentdate ) { 
				 //~ $(".live-event").html("This Event was live on "+month);
			 //~ }
			 //~ else
			 //~ {
				  //~ $(".live-event").html("This Event will be live on "+month);
			 //~ }
		 //~ }
	})
	
	
	function getTheDate(getDate) {
	  var days = ["Sunday", "Monday", "Tuesday",
		"Wednesday", "Thursday", "Friday", "Saturday"
	  ];
	 
	  var theCDate = new Date(getDate);	 
	  var date=months[theCDate.getMonth()] + ' ' + theCDate.getDate()+nth(theCDate.getDate());
	  var date1=theCDate.getFullYear() + '-' +('0'+(theCDate.getMonth()+1)).slice(-2) + '-' + ('0' +theCDate.getDate()).slice(-2);
	  $("#eventdate").val(date1);
	  $("#tmpdate").val(date1);
	  return  date;
	}
	function nth(d) {
	  if (d > 3 && d < 21) return 'th'; 
	  switch (d % 10) {
		case 1:  return "st";
		case 2:  return "nd";
		case 3:  return "rd";
		default: return "th";
	  }
	}
	//~ $(document).on("click",".prenext",function(){
		  //~ $selected_date=$("#eventdate").val();	
		  //~ var fullDate=new Date(selected_date);
		  //~ var date=fullDategetDate();
	//~ });
	 
	//end Saving data on Blur auto save
	
	function formatDateLiveEvent(date)
	{		
		var mydate = new Date(date);
		var time = formatTime(mydate);				
		var month = ["Jan", "Feb", "March", "April", "May", "June",
		"July", "Aug", "Sept", "Oct", "Nov", "Dec"][mydate.getMonth()];
		var day = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday",
		"Saturday"][mydate.getDay()];
		
		var date=mydate.getDate()+''+nth(mydate.getDate);
		
		//var str = month + ', ' + mydate.getDate()+ ' ' + mydate.getFullYear()+ ' ' + time;	
		var str = day+' '+date+ ' of '+ month + ' at ' +  time;			
		//var currentDate = fullDate.getFullYear() + "-" +(fullDate.getMonth() + 1) + "-" + fullDate.getDate() ;	
		//  Thursday  18 th 	  of	  April	  at	  11.15am	
		
		return str;	
	}
	function setLiveStatus(flagvalue,countDownDate)
	{
		
		//var startDateTime = new Date(2014,8,26,2,1,0,0); // YYYY (M-1) D H m s (start time and date from DB)
		//var startStamp = countDownDate;

		var newDate = new Date();
		var newStamp = newDate.getTime();

		var timer;

		function updateClock() {
			newDate = new Date();
			newStamp = newDate.getTime();
			
			if(flagvalue==false)
			{										
				stopFunction(x);
				flagvalue=true;	
				setTimeout(setInterval, 500);
			}
			
			var diff = Math.round((newStamp-countDownDate)/1000);
			
			var d = Math.floor(diff/(24*60*60));
			diff = diff-(d*24*60*60);
			var h = Math.floor(diff/(60*60));
			diff = diff-(h*60*60);
			var m = Math.floor(diff/(60));
			diff = diff-(m*60);
			var s = diff;
			
			//$(".live-event").html("This Event has been live on Rave for "+ h+" hrs "+m+" mins, "+s+" secs");
		}

		x=setInterval(updateClock, 1000);
	}
	function stopFunction(){
		clearInterval(x); // stop the timer	
	}
	function changeTimeInterval(flagvalue,countDownDate)
	{
		
		 x = setInterval(function() {						
			
		  // Get todays date and time
		   now = new Date().getTime();
		 
			if(flagvalue==false)
			{										
				//clearInterval(x);
				flagvalue=true;	
				//setTimeout(setInterval, 1000);
			}
		  // Find the distance between now and the count down date
		  var distance = countDownDate - now;
			
		  // Time calculations for days, hours, minutes and seconds
		  var days = Math.floor(distance / (1000 * 60 * 60 * 24));
		  var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
		  var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
		  var seconds = Math.floor((distance % (1000 * 60)) / 1000);
		  
		  //$(".live-event").html("This	Event has been	live on Rave for "+month);
		  // Output the result in an element with id="demo"
		  //$(".live-event").html("This Event has been live on Rave for 0days " + hours + "hrs "		  + minutes + "mins " + seconds + "secs ");
			
		  // If the count down is over, write some text 
		  if (distance < 0) {
			clearInterval(x);
			// $(".live-event").html("EXPIRED");
		  }
		}, 1000);
	}
	function save_data_ajax()
	{
		 var startprice=$("CalendarStartPrice").val();
		 var endprice=$("CalendarEndPrice").val();
		 if(startprice != "")
		 startprice=  $("#CalendarStartPrice").val().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"); 
		 if(endprice != "")
		 endprice= $("#CalendarEndPrice").val().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		 $(".live-event").hide();
		$("#CalendarStartPrice").val(startprice);
		$("#CalendarEndPrice").val(endprice);
		//$("#tmpdate").val($("#eventdate").val());
		 var href =SITE_LINK+"add_event/";		
		 $.ajax({   
			type: "POST",   
			cache: false,   
			url: href,   
			data:$("#CalendarIndexForm").serialize(),
			success: function(data){
				data = JSON.parse(data);
				flagsave=true;
				//id=$("#id").val();
				id=data;
				if(id=="")
				{
					resetFrom();
					reset_from_after_edit();
					$("#CalendarStartPrice").val();
					$("#CalendarEndPrice").val();				
				}
				else
				{
					var d  = new Date($("#tmpdate").val());
					//~ var currentdate=  ('0'+(d.getMonth()+1)).slice(-2) + '/' + ('0' +d.getDate()).slice(-2)+ '/' +d.getFullYear();
				
					//~ var end_hour=$("#CalendarEndHour").val();					
					//~ currentdate=currentdate +" "+ end_hour + ":00";					
					//~ var countDownDate = new Date(currentdate).getTime();					
					//~ var now = new Date().getTime();	
					//~ if((now==countDownDate || countDownDate > now) && $("#event_status").val()==1)
					//~ {	
						//~ stopFunction();				
						//~ flagvalue=false;
						//~ //$(".live-event").show();	
						//~ //setInterval(function(){changeTimeInterval(flagvalue,countDownDate)},1000);					
									
					//~ }
					//~ else
					//~ {						
						//~ flagvalue=false; 
						//~ $(".live-event").hide();
						//~ $(".gray-bg").show();
					//~ }					
					
				}
				
			}  
			
		});	
		
	}	
	
	 $(document).on("click",".edit_event",function(){	
		$(".edit_event").hide();
		$(".save_event").show();		
		$('.save').attr('disabled',false);
		$('#CalendarIndexForm input,textarea').prop('readonly', false);
		$('#CalendarIndexForm select').prop('disabled', false);	
		$('#ticket_free').prop('disabled', false);				
		$('#clear_all').attr('disabled',false);
		//$('#showevent').attr('readonly',false);
		// Show Event Button//
		//document.getElementById('showevent').style.pointerEvents = 'auto';
		// Show Event Button//
		document.getElementById('drop-zone').style.pointerEvents = 'auto';
		document.getElementById('img-upload').style.pointerEvents = 'auto';
		document.getElementById('pac-input').style.pointerEvents = 'auto'; 
		document.getElementById('ticket_free').style.pointerEvents = 'auto';
		document.getElementById('next').style.pointerEvents = 'auto';
		document.getElementById('prev').style.pointerEvents = 'auto';	
		
	});
	$(document).on("click",".confirmdelete",function(){
		tmplink = $(this).attr("action");
		$("#cnfrmdelid").modal("show");
	});
	
	 $(document).on("click",".delvoucher",function(){
		//console.log(tmplink);
		var date=$("#eventdate").val();		
		 var href =tmplink+"&date="+date;		
		 $.ajax({   
			type: "GET",   
			cache: false,   
			url: href, 
			success: function(data){   
				data = JSON.parse(data);				
				if(data=="success"){
					$("#cnfrmdelid").modal("hide");
					month= $("#eventdate").val();	
					//var href =SITE_LINK+"show_myevent?month="+month;
					var val=$("#searchTag option:selected").val();
						
					resetFrom();
					if(val!="")	{ 
						val = val.replace("#",'');
						 var href =SITE_LINK+"showtags?tag="+val;							
					}
					else
					{									
						var href =SITE_LINK+"show_myevent?month="+month;
						//nextmonth$(".event-list").html("<li class='white'>No event to display<li>");						
					}					
					leftbareventdata(href);	
					
					var url_calendar="calendar?month="+month+"&sel_date="+date;	
					$('#mycalendar').attr('src', url_calendar);					
					
					
					count_total_events();
					resetFrom();
				}
			}  
		});
	});
	
	function count_total_events()	{
			
		 var href =SITE_LINK+"count_total_events";	
		 $.ajax({   
			type: "GET",   
			cache: false,   
			url: href, 
			success: function(data){   
				data = JSON.parse(data);
				$('.select-result').html(data);
			}  
		});
	}
	$(document).on("click","#clear_all",function(){	
	resetFrom();
	});
	
	function formatAMPM(time)
	{  
		// var d = new Date(date);
		// Check correct time format and split into components
		  time = time.toString ().match (/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];

		  if (time.length > 1) { // If time format correct
			time = time.slice (1);  // Remove full string match value
			time[5] = +time[0] < 12 ? ' AM' : ' PM'; // Set AM/PM
			time[0] = +time[0] % 12 || 12; // Adjust hours
		  }
		  return time.join (''); // return adjusted time or original string
	}
	
	

	function formatTime(date) {
	  var hours = date.getHours();
	  var minutes = date.getMinutes();
	  var ampm = hours >= 12 ? 'pm' : 'am';
	  hours = hours % 12;
	  hours = hours ? hours : 12; // the hour '0' should be '12'
	  minutes = minutes < 10 ? '0'+minutes : minutes;
	  var strTime = hours + ':' + minutes + ' ' + ampm;
	  return strTime;
	}


	
	function formatDate(date)
	{  
		//console.log(time);
		
		var mydate = new Date(date);
		var time = formatTime(mydate);				
		var month = ["Jan", "Feb", "March", "April", "May", "June",
		"July", "Aug", "Sept", "Oct", "Nov", "Dec"][mydate.getMonth()];
		var str = month + ', ' + mydate.getDate()+ ' ' + mydate.getFullYear()+ ' ' + time;		
		//var currentDate = fullDate.getFullYear() + "-" +(fullDate.getMonth() + 1) + "-" + fullDate.getDate() ;		
		return str;
	}
	function currentDate()
	{
		var fullDate = new Date()				
		//var twoDigitMonth = ((fullDate.getMonth().length+1) === 1)? (fullDate.getMonth()+1) : '0' + (fullDate.getMonth()+1);
		 
		var currentdate = fullDate.getFullYear() + "-" +(fullDate.getMonth() + 1) + "-" + fullDate.getDate() ;
		return currentdate;
	}
	 $(document).on("click",".showcomments",function(){
		 
		  var id=$("#id").val();
		   var date=$("#tmpdate").val();
		   $("#comment").val();
		$(".pop-comment").empty();
		 var href =SITE_LINK+"fetch_comments?id="+id+"&date="+date;		
			comments_show(href);
		});
		
		function comments_show(href)
		{
			 $.ajax({   
				type: "GET",   
				cache: false,   
				url: href,   			
				success: function(data){   
					data = JSON.parse(data);
					var title_name=$("#title").val();
					$(".title_name").html(title_name);
					if(data=="no-comments" || data=="invalid_request")
					{
						$("#pop-comment").html("No Comment to display");
					}
					else
					{
						if ( data.length > 0 ) {
							var tmpNew1 = data_comment;
							var name="";
							//$(".event-list").text="sfds";
							$(".pop-comment").empty();
							$.each(data,function(key,val){														
								if(val['business']=="" || val['business']==null ){name="";}else { name=val['business']}
								var start_hour=formatDate(val["created"]);		
								tmpNew1 = data_comment.replace(/{IMAGE}/g,val['image']);
								tmpNew1 = tmpNew1.replace(/{ID}/g,val['id']);
								tmpNew1 = tmpNew1.replace(/{NAME}/g,name);							
								tmpNew1 = tmpNew1.replace(/{DATETIME}/g,start_hour);
								//tmpNew = tmpNew.replace(/{ID}/g,val['Calendar']['id']);
								tmpNew1 = tmpNew1.replace(/{COMMENT}/g,val['comment']);
								tmpNew1 = tmpNew1.replace(/{SITE_LINK}/g,SITE_LINK);
								$(".pop-comment").append(tmpNew1);								
								
								//$(".pop-comment").val($("#totalcomments").val());
							});
						}
						$(".totalcomments").html(data.length); 
						
						$(".cart_total_items").html(0);		
						$(".cart_total_items").hide();
					}
					
				}	
			});
			}
		
		var data_comment='<li class="row"><div class="col-xs-1"><img src="{IMAGE}" alt="{NAME}" title="{NAME}"></div> <div class="col-xs-9 responsive-add"><h3>{NAME}</h3><p>{DATETIME}</p><p class="comment_txt">{COMMENT}</p></div><div class="col-xs-2 comment_img"><a href="javascript:void(0);" tooltip="tooltip" title="Delete" class="confirm_del_comment comment_dete" action="{ID}"><img src="{SITE_LINK}img/delete_comment.png" alt="Close" title="Close"></a></div></li>';
	
		 $(document).on("click",".confirm_del_comment",function(){
		//console.log(tmplink);
			var id= $(this).attr("action");
		
			href=SITE_LINK+"confirm_del_comment?id="+id;
			 $.ajax({   
				type: "GET",   
				cache: false,   
				url: href, 
				success: function(data){   
					data = JSON.parse(data);				
					if(data=="success"){	
						var id=$("#id").val();
					    var date=$("#tmpdate").val();
					    $("#comment").val();
					    $(".pop-comment").empty();
					    var href =SITE_LINK+"fetch_comments?id="+id+"&date="+date;		
						comments_show(href);
					}
				}  
			});
		});
		
		$("#comment").keydown(function(e){
		  if((e.keyCode || e.which) == 13) { 
			var comment=$("#comment").val();
			 if(comment!="") {
			  //Enter keycode
				var post_data = "";
				 $('textarea.mention').mentionsInput('val', function(text) {						
					 if(text != '')
						{
							post_data = text; 
							
						}
				 })
				
				 var id=$("#id").val();
				  var date=$("#tmpdate").val();
					
				  var mention_ids=post_data;		  
				 var href =SITE_LINK+"add_comment?id="+id+"&comment="+comment+"&date="+date+"&mention_ids="+mention_ids;	
					
					 $.ajax({   
						type: "GET",   
						cache: false,   
						url: href,   			
						success: function(data){   
							data = JSON.parse(data);
							if(data=="success")
							{							
								href =SITE_LINK+"fetch_comments?id="+id+"&date="+date;
								comments_show(href);
								$("#comment").val('');
								$("#comment").attr("placeholder", "write a comment");
								$("#mention_id").val('');
								$("textarea.mention").mentionsInput('reset');
								$("textarea.mention").removeClass('error_full');
							}
						}
					});
				}
				else
				{
						$("textarea.mention").addClass('error_full');
				}
			};
		});
		
		
		var data_event1='<li class="{CLASS}"><div class="event-list-left"><img src="{SITE_LINK}{IMAGE}" alt="{TITLE}" title="{TITLE}"/></div><div class="event-list-right"><a href="javascript:void(0);" tooltip="tooltip" title="Delete" class="close-event confirmdelete" action="{SITE_LINK}delete_event?id={ID}"><img src="{SITE_LINK}img/close.png" alt="Close" title="Close"/></a><a href="javascript:void(0);" class="editevent_search" action="{EVENTDATE}" value="{ID}"><h2>{TITLE}</h2><p>{STARTHOUR}</p></a></div></li>';

	var data_event='<li class="{CLASS}"><div class="event-list-left"><img src="{SITE_LINK}{IMAGE}" alt="{TITLE}" title="{TITLE}"/></div><div class="event-list-right"><a href="javascript:void(0);" tooltip="tooltip" title="Delete" class="close-event confirmdelete" action="{SITE_LINK}delete_event?id={ID}"><img src="{SITE_LINK}img/close.png" alt="Close" title="Close"/></a><a href="javascript:void(0);" class="editevent" action="{EVENTDATE}" value="{ID}"><h2>{TITLE}</h2><p>{STARTHOUR}</p></a></div></li>';
	//Calender
	 $(document).on("click",".editevent",function(){
		 var id=$(this).attr("value");
		 var tmplink=$("#tmpdate").val();
		//var eventdate=$(this).attr("action");
		 edit_event(id,tmplink);
		
	});	 
	
	 $(document).on("click",".editevent_search",function(){
		 var id=$(this).attr("value");
		// var tmplink=$("#tmpdate").val();
		 var tmplink=$(this).attr("action");
		 edit_event(id,tmplink);
		
	});	
	
	var flagvalue=true;var i=0;var x="";
	function edit_event(id,sel_date)
	{
		//alert("yes");alert(id);alert(sel_date);
		$(".gray-border").show();
		$(".nolive-bg").hide();		
		$(".cart_total_items").hide();
		$(".cart_total_items").html();
		
		
		 var href =SITE_LINK+"fetch_event?id="+id+"&date="+sel_date;
		 $.ajax({   
			type: "GET",   
			cache: false,   
			url: href,   			
			success: function(data){   
				data = JSON.parse(data);
								
				$(".tags").empty();	
				$('input,textarea,select').removeClass("error");	
				$('.red-bg').hide();
				$('.gray-border').show();
				$("#drop-zone").removeClass("error_full");			
				$("#CalendarHashtags").removeClass("error");	
				$(".event-noty").hide();$(".event_show_comments").hide();$(".gray-bg").hide();
				$(".popup-event").hide();	
				
				$("#id").val(data[0]["id"]);
				$("#title").val(data[0]["title"]);
				$("#short_dec").val(data[0]["short_dec"]);
				$("#long_dec").val(data[0]["long_dec"]);
				$("#pac-input").val(data[0]["location"]);
				$("#CalendarLatitude").val(data[0]["latitude"]);
				$("#CalendarLongitude").val(data[0]["longitude"]);
				initMap();
				if(data[0]["ticket_free"]=="1")
				{			
					
					$('#CalendarEndPrice').val();		
					$('#ticket_free').prop('checked',true);	
					$('#CalendarStartPrice').prop('disabled',true);
					$('#CalendarEndPrice').prop('disabled',true);
					$('#CalendarStartPrice').val('');
					$('#CalendarEndPrice').val('');
						
				}
				else
				{
					$('#ticket_free').prop('checked',false);	
					$("#CalendarStartPrice").val(data[0]["start_price"]);
					$("#CalendarEndPrice").val(data[0]["end_price"]);
				}
				var start_hour=formatAMPM(data[0]["start_hour"]);
				var end_hour=formatAMPM(data[0]["end_hour"]);
				$("#CalendarStartHour").val(start_hour);
				$("#CalendarEndHour").val(end_hour);
				
				$("#CalendarRepeatEvent").val(data[0]["repeat_event"]);
				//$("#CalendarRepeatEvent1").val(data[0]["repeat_event"]);
				$("#CalendarTicketLink").val(data[0]["ticket_link"]);				
				$('.upload-file img').attr("src",SITE_LINK+data[0]["image"]);	
				$('.img-container img').attr("src",SITE_LINK+data[0]["image"]);			
			
				$("#tmpImage").val(data[0]["image"]);				
				$("#oldImage").val(data[0]["image"]);				
				$("#CalendarTags").val(data[0]["tags"]);
				//$("#CalendarTags1").val(data[0]["tags"]);
				
			
				$("#event_status").val(data[0]["event_status"]);
				var event_status=data[0]["event_status"];
				$(".gray-border").hide();
				if(event_status==1)
				{
					$('#event_status').val(1);		
					$('#showevent').val("Hide Event on Rave"); 
					$("#showevent").addClass("hide-event");					
				}
				else
				{
					$(".live-event").hide();
					$(".gray-border").show();
					$('#event_status').val(0);
					$('#showevent').val("Show Event on Rave")
					$("#showevent").removeClass("hide-event");
				}		
				var today = new Date();						
				var currentdate= today.getFullYear() + '-' +('0'+(today.getMonth()+1)).slice(-2) + '-' + ('0' +today.getDate()).slice(-2);
				
				$("#eventdate").val(data[0]["eventdate"]);
				//~ theDate= new Date(data[0]["eventdate"]).getTime();	
				var d = new Date(sel_date);
				$("#tmpdate").val(sel_date);
				var date=months[d.getMonth()] + ' ' + d.getDate()+nth(d.getDate());				
				$("#date").html(date);
				//$("#tmpdate").val(data[0]["eventdate"]);
				theDate= new Date(sel_date).getTime();	
				
				$(".event-noty").show();
				$(".event_show_comments").show();
				
				$("#totalsavecalendar").html(data[0]["save_calendar"]);
				$("#totaleventview").html((data[0]["event_view"]));
				$(".totalcomments").html((data[0]["comments"]));
				
				if(data[0]["unviewed"]>0)
				{			
					$(".cart_total_items").show();
					$(".cart_total_items").html((data[0]["unviewed"]));
				}
				if(data[0]["is_active"]==0)
				{
					$(".unsuitable").show();
				}
				//Commented bcouz in new changes refreshing the clanede		
					//var url_calendar="calendar?month="+data[0]["eventdate"]+"&sel_date="+sel_date;	
					//$('#mycalendar').attr('src', url_calendar);	
				//Commented bcouz in new changes refreshing the claneder
				
				
									
				//var url_calendar="calendar?month="+data[0]["eventdate"];	
					
				
				$('#CalendarIndexForm input,textarea').prop('readonly', true);
				$('#CalendarIndexForm select').prop('disabled', true);				
				document.getElementById('drop-zone').style.pointerEvents = 'none';
				document.getElementById('img-upload').style.pointerEvents = 'none';
				document.getElementById('pac-input').style.pointerEvents = 'none';	
				document.getElementById('next').style.pointerEvents = 'none';
				document.getElementById('prev').style.pointerEvents = 'none';			
				$('#ticket_free').prop('disabled', true);	
				$(".save_event").hide();	
				$(".edit_event").show();
				$('#comment').prop('readonly', false);
				
				if(event_status)
				{
					//  $('.edit').attr('disabled',true);
					// Show Event Button//
							// document.getElementById('showevent').style.pointerEvents = 'none';
					  // Show Event Button//
				}	
				else
				{
					 $('.edit').attr('disabled',false);
					 // Show Event Button//
					    //document.getElementById('showevent').style.pointerEvents = 'none';
					 // Show Event Button//
				}
				$('#clear_all').attr('disabled',true);
				//$('#showevent').attr('readonly',true);
				
				
				//var end = new Date(data[0]["end_hour"]);
				//var start = new Date(data[0]["start_hour"])
				
				var now_full_date = new Date();
				var now_full_date=now_full_date.getFullYear()+'-' +('0'+(now_full_date.getMonth()+1)).slice(-2) + '-' + ('0' +now_full_date.getDate()).slice(-2);
				
				
				var currentdate=  ('0'+(d.getMonth()+1)).slice(-2) + '/' + ('0' +d.getDate()).slice(-2)+ '/' +d.getFullYear();
				
				var click_date=$("#tmpdate").val();
				var showdate=$("#eventdate").val();
				showdate=showdate +" "+ data[0]["start_hour"] + ":00";
								
				var start_hour=currentdate +" "+ data[0]["start_hour"] + ":00";
				var end_hour=currentdate +" "+ data[0]["end_hour"]+ ":00";
				//currentdate=currentdate +" "+ start_hour + ":00";
				 //alert(start_hour);
				 start_hour= new Date(start_hour).getTime();
				 end_hour= new Date(end_hour).getTime();
						$(".plus-add").show();						
				var countDownDate = new Date(start_hour).getTime();
				var now = new Date().getTime();
				$(".live-event").hide(); 
				
				if(data[0]["event_status"]==true)
				{	
					
					if(now_full_date==click_date || now_full_date < click_date)
					{
						$(".live-event").show();	
						$(".live-event").html("This event is live on The Rave Mobile App");
					}	
					else 
					{
							$(".gray-bg").show();
																	
					}				
				}				
				else if(data[0]["event_status"]==false)
				{
					flagvalue=true; 		
					
				  	if(now_full_date==click_date || now_full_date < click_date)
					{	
						$(".nolive-bg").hide();	
						$(".live-event").hide();	
						$(".gray-border").show();
						$(".gray-bg").hide();
					}
					else
					{	
						$(".gray-border").hide();
						$(".gray-bg").show();
						
					}
				}				
			}  
		});
	} 
		
		var id_gen=false;	
		
		$(document).on('change','#searchTag',function(){
		//$("#searchTag").change(function(e){	
		$(".nolive-bg").hide();
		$('.save').attr('disabled',false);
		$(".live-event-bg").hide();	
		
			var val=$("#searchTag option:selected").val();
			resetFrom();
			if(val!="")	{
				val = val.replace("#",'');
				 var href =SITE_LINK+"showtags?tag="+val;	
				 leftbareventdata(href,'search');
					
				 setTimeout(function() { 
				 var event_live_id=$("#event_live_id").val();
				 var event_date=$("#eventdate").val();
				 edit_event(event_live_id,event_date);
				}, 1000);
			}
			else
			{
				month=$("#eventdate").val();				
				var href =SITE_LINK+"show_myevent?month="+month;
				//nextmonth$(".event-list").html("<li class='white'>No event to display<li>");
				leftbareventdata(href,'search');	
			}
		});
		
		function leftbareventdata(href,sel=null)
		{
			$.ajax({  type: "GET",   
						cache: false,   
						url: href,   			
						success: function(data){   
							data = JSON.parse(data);														
							if ( data.length > 0 ) {
								var valsel=$("#searchTag option:selected").val();
															
								$(".event-list").empty();
								id_save=0;
								$.each(data,function(key,val){
									
								if(sel=="search" && id_save==0 && valsel!="")
								{	
										
									$("#eventdate").val(val['eventdate']);		
									$("#event_live_id").val(val['id']);	
									id_save=1;														
								}	
								
											
										
									if(val['title']=="")
									title="Title Here"
									else
									title=val['title'];
									
									var className = "";
									//var currentdate=currentDate();
									var fullDate=new Date();
									var currentdate =fullDate.getDate();
									var current_month =fullDate.getFullYear()+"-"+(fullDate.getMonth()+1)+"-"+fullDate.getDate();
									
									
									var fullDate1=new Date(val['eventdate']);
									var eventdate =fullDate1.getDate();
									var month=$(this).attr("value");											
									var start_hour=formatAMPM(val["start_hour"]);		
									if(sel=="search" && valsel!="")
									{
										tmpNew = data_event1.replace(/{IMAGE}/g,val["image"]);
									}
									else
									{
										tmpNew = data_event.replace(/{IMAGE}/g,val["image"]);
									}
									
									tmpNew = tmpNew.replace(/{TITLE}/g,title);							
									tmpNew = tmpNew.replace(/{STARTHOUR}/g,start_hour);
									tmpNew = tmpNew.replace(/{ID}/g,val['id']);
									tmpNew = tmpNew.replace(/{EVENTDATE}/g,val['eventdate']);
									tmpNew = tmpNew.replace(/{SITE_LINK}/g,SITE_LINK);
									tmpNew = tmpNew.replace(/{CLASS}/g,className);
								
									$(".event-list").append(tmpNew);
									
									loadFlag1 = true;
								});
							} else {
								flag = false;
								loadFlag1 = false;
								$(".event-list").html("<li class='white'>No event to display<li>");
								reset_from_after_edit();
							}
						}  
					});
		}
$(".upgrade-pop").on('blur',function(){
    $(this).fadeOut(300);
});	
		
$("#CalendarStartPrice").on({
    keyup: function() {
		$(".minlengthstart").hide();
      formatCurrency($(this));
    },
    blur: function() { 
		$(".minlengthstart").hide();
      formatCurrency($(this), "blur");
    },
    click: function() { 
      formatCurrencybackstart($(this));      
    }
});



function formatCurrencybackstart(n) {
	var input_val = n.val();
	var number = input_val.split(".");
	if(number[0])
	{
		number=number[0];
	}
	//var number =number.replace(/,/g, '');
	var len=number.length;
	
	if(len>3)
	{
		flagsubmit=false;
		$(".minlengthstart").show();
		$(".minlengthstart").html("Only 4 digits are allowed before '.00'");
	}
	else
	{
		flagsubmit=true;
		$(".minlengthstart").hide();
		$(".minlengthstart").html("");
	}
	$("#CalendarStartPrice").val(number);
}

$("#CalendarEndPrice").on({
    keyup: function() {
		$(".minlengthend").hide();
      formatCurrency($(this));
    },
    focusout: function() { 
		$(".minlengthend").hide();
		
      formatCurrency($(this), "blur");
    },
    click: function() { 
      formatCurrencybackend($(this));
      
    }
});
function formatCurrencybackend(n) {
	var input_val = n.val();
	var number = input_val.split(".");
	if(number[0])
	{
		number=number[0];
	}
	//var number =number.replace(/,/g, '');
	var len=number.length;
	
	if(len>3)
	{
		flagsubmit1=false;
		$(".minlengthend").show();
		$(".minlengthend").html("Only 4 digits are allowed before '.00'");
	}
	else
	{
		flagsubmit1=true;
		$(".minlengthend").hide();
		$(".minlengthend").html("");
	}
	$("#CalendarEndPrice").val(number);
}

function formatNumber(n) {
  // format number 1000000 to 1,234,567
  return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",")
}


function formatCurrency(input, blur) {
  // appends $ to value, validates decimal side
  // and puts cursor back in right position.
  
  // get input value
  var input_val = input.val();
  
  // don't validate empty input
  if (input_val === "") { return; }
  
  // original length
  var original_len = input_val.length;


  // initial caret position 
  var caret_pos = input.prop("selectionStart");
    
  // check for decimal
  if (input_val.indexOf(".") >= 0) {

    // get position of first decimal
    // this prevents multiple decimals from
    // being entered
    var decimal_pos = input_val.indexOf(".");

    // split number by decimal point
    var left_side = input_val.substring(0, decimal_pos);
    var right_side = input_val.substring(decimal_pos);

    // add commas to left side of number
    left_side = formatNumber(left_side);

    // validate right side
    right_side = formatNumber(right_side);
    
    // On blur make sure 2 numbers after decimal
    if (blur === "blur") {
      right_side += "00";
    }
    
    // Limit decimal to only 2 digits
    right_side = right_side.substring(0, 2);

    // join number by .
    input_val = "" + left_side + "." + right_side;

  } else {
    // no decimal entered
    // add commas to number
    // remove all non-digits
    input_val = formatNumber(input_val);
    input_val = "" + input_val;
    
    // final formatting
    if (blur === "blur") {
      input_val += ".00";
    }
  }
  
  // send updated string to input
  input.val(input_val);
  
  if(blur=='blur') return;
  
  // put caret back in the right position
  var updated_len = input_val.length;
  caret_pos = updated_len - original_len + caret_pos;
  input[0].setSelectionRange(caret_pos, caret_pos);
}


 //~ $(window).keydown(function(event){
	 
      //~ if(event.target.tagName == 'TEXTAREA') {
		
        //~ if(event.keyCode =="13") {			
            //~ event.preventDefault();
			//~ var s = $("#long_dec").val();
			//~ $("#long_dec").val(s+"\n");          
        //~ }
      //~ }
  //~ });
});
