$(document).ready(function () {
    $("#resetModel").show();
    //~ $('#resetModel').on('hidden.bs.modal', function () {
		//~ location.href = SITE_LINK;
	//~ });	
	
	$('#UserResetPasswordForm').validate({ // initialize the plugin
        rules: {
					  
			 "data[User][newpassword]": {
				required: true,
				minlength: 6,
				maxlength: 20
			  },			
			"data[User][confirm_password]": {
				required: true,				
				equalTo: '#UserNewpassword'
			  }
			},
			// Specify validation error messages
			messages: {			 
			  "data[User][newpassword]": {
				required: "Please provide a password",
				minlength: "Your password must be at least 6 characters long"
			  },
			  "data[User][confirm_password]": {
				required: "Please provide confirm password"				
			  }			  		 
			},
			// Make sure the form is submitted to the destination defined
			// in the "action" attribute of the form when valid
			submitHandler: function(form) {
				
				$('#error_msg_reset').hide();				
				
				$('#UserResetPasswordForm').ajaxSubmit(function(response) { 	
					 $('#error_msg_reset').empty();
					 $('#success_msg_reset').empty();
					response = JSON.parse(response);
					if (response.status) {
						location.href = SITE_LINK+"message_confirm";
					} else {						
						 $("#error_msg_reset").show();
						 $('#error_msg_reset').text(response.message);										
						 return false;
					}
				});
			}
		  });
});
