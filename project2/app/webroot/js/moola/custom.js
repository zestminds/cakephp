// JavaScript Document

$(document).ready(function() {

	$(window).scroll(function() {
	      var sticky = $('.header'),
	        scroll = $(window).scrollTop();
	       
	      if (scroll >= 100) { 
	        sticky.addClass('fixed'); }
	      else { 
	       sticky.removeClass('fixed');

	    }
     });

	
	$('.menuIcn').click(function(e) {
		$('body').addClass('openmenu');
	});
	
	$('.hdrrLeft ul li a > i, .topSrch').click(function(e) {
		$('body').toggleClass('open-search');
	});
		
	$('.cossIcnInr, .overlay,.closeIcn > span').click(function(e) {
		$('body').removeClass('openmenu');
	});
	
	
	$('.parent > .arrowbtn').click(function() {
		$(this).parent('.parent').siblings().removeClass('active');
		$(this).parent('.parent').toggleClass('active');
		$(this).parent('.parent').siblings().children('.submenu').slideUp();
		$(this).parent('.parent').children('.submenu').slideToggle();
	});

	$('.har_shop_slider  .owl-carousel').owlCarousel({
	    loop:false,
	    margin:10,
	    nav:true,
	    items:1,
	    autoplay:true,
    	autoplayTimeout:5000,
	})
     
     $('.tasti-slider').owlCarousel({
        loop:true,
        margin:30,
        responsiveClass:true,
        responsive:{
            0:{
                items:1,
                nav:true,
                loop:true
            },
            600:{
                items:1,
                nav:true,
                loop:true
            },
            800:{
                items:2,
                nav:true,
                loop:true
            }
        }
      })
});

//           File Upload Js
//====================================
$(document).ready( function() {
	$(document).on('change', '.btn-file :file', function() {
	var input = $(this),
		label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
	input.trigger('fileselect', [label]);
	});

	$('.btn-file :file').on('fileselect', function(event, label) {
	    
	    var input = $(this).parents('.input-group').find(':text'),
	        log = label;
	    
	    if( input.length ) {
	        input.val(log);
	    } else {
	       // if( log ) alert(log);
	    }
    
	});
	function readURL(input) {
	    if (input.files && input.files[0]) {
	        var reader = new FileReader();
	        
	        reader.onload = function (e) {
	            $('#img-upload').attr('src', e.target.result);
	        }
	        
	        reader.readAsDataURL(input.files[0]);
	    }
	}

	$("#imgInp").change(function(){
	    readURL(this);
	}); 	
});


//           Counter pa Js
//====================================
(function () {
   "use strict";
  
    var expiryDate = "January 1 2019 01:57:59 GMT+0700";

    function getTimeLeft(endTime) {

        var dateDiff = Date.parse(endTime) - Date.parse(new Date());
        var seconds = Math.floor((dateDiff / 1000) % 60);
        var minutes = Math.floor((dateDiff / 1000 / 60) % 60);
        var hours = Math.floor((dateDiff / (1000 * 60 * 60)) % 24);
        var days = Math.floor(dateDiff / (1000 * 60 * 60 * 24));

        if (dateDiff <= 0) {
            days = "0";
            hours = "0";
            minutes = "0";
            seconds = "0";                    
        }

        return {
            "total": dateDiff,
            "days": days,
            "hours": hours,
            "minutes": minutes,
            "seconds": seconds
        };

    }

    function counterStart(id, endTime) {

        var counterPlay = document.getElementById(id);
        var daysEle = counterPlay.querySelector(".counting-days");
        var hoursEle = counterPlay.querySelector(".counting-hours");
        var minutesEle = counterPlay.querySelector(".counting-minutes");
        var secondsEle = counterPlay.querySelector(".counting-seconds");

        function timerUpdate() {
            var time = getTimeLeft(endTime);

            daysEle.textContent = time.days;
            hoursEle.textContent = ("0" + time.hours).slice(-2);
            minutesEle.textContent = ("0" + time.minutes).slice(-2);
            secondsEle.textContent = ("0" + time.seconds).slice(-2);

            if (time.total <= 0) {
                clearInterval(timeinterval);
            }
        }

        timerUpdate();
        var timeinterval = setInterval(timerUpdate, 1000);

    }

    counterStart("countdown-display", expiryDate);

})();

