$(document).ready(function () {	
	
	
	$("#days_appdown").change(function(e) {
		var days=$("#days_appdown option:selected").val();
		
		var url_download="total_app_downloads/?days="+days;		
		$('#chart_download_app').attr('src', url_download);
		$('#chart_logins').attr('src', url_download);
	});		
	
	$("#days_regver").change(function(e) {
		
		var days=$("#days_regver option:selected").val();		
		var url_reg="total_registrations/?days="+days;
		var url_ver="total_verifications/?days="+days;		
		$('#chart_registrations').attr('src', url_reg);
		$('#chart_verifications').attr('src', url_ver);
	});		
	
	$("#days_reachcon").change(function(e) {
		var days=$("#days_reachcon option:selected").val();
		var university=$("#uni_reachcon option:selected").val();
		var url_reg="total_reachconversions/?days="+days+"&university="+university;
		$('#chart_reachconversions').attr('src', url_reg);
	});	
	
});

