<?php
App::uses('AppModel', 'Model');
/**
 * UserCategory Model
 *
 * @property Category $Category
 * @property User $User
 */
class UserCategory extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'user_id';


	// The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Category' => array(
			'className' => 'Category',
			'foreignKey' => 'category_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
