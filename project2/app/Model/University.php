<?php
App::uses('AppModel', 'Model');
/**
 * University Model
 *
 */
class University extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'title';

}
