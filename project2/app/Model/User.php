<?php
App::uses('AppModel', 'Model');
/**
 * User Model
 *
 * @property UserType $UserType
 * @property Biiling $Biiling
 * @property Tag $Tag
 * @property UserCategory $UserCategory
 * @property UserDetail $UserDetail
 * @property UserUniversity $UserUniversity
 * @property VoucherSale $VoucherSale
 * @property Voucher $Voucher
 */
class User extends AppModel {


	// The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'UserType' => array(
			'className' => 'UserType',
			'foreignKey' => 'user_type_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
	);
	
	
	public $hasOne = array(
		"UserDetail" => array(
			"className" => "UserDetail",
			"foreignKey" => "user_id",
			"type" => "Inner",
			'dependent' => true,
		)
	);
public $hasMany= array(
		"Follower" => array(
			"className" => "Follower",
			"foreignKey" => "follower_id",
			"type" => "Inner",
			'dependent' => true,
		)
	);
//~ Public $hasMany = array(
//~ 'UserCategory' => array(
			//~ 'className' => 'UserCategory',
			//~ 'foreignKey' => 'user_id',
			//~ 'dependent' => false,
			//~ 'conditions' => '',
			//~ 'fields' => '',
			//~ 'order' => '',
			//~ 'limit' => '',
			//~ 'offset' => '',
			//~ 'exclusive' => '',
			//~ 'finderQuery' => '',
			//~ 'counterQuery' => ''
		//~ )
//~ );
/**
 * hasMany associations
 *
 * @var array
 */
 /*
	public $hasMany = array(
		'Biiling' => array(
			'className' => 'Biiling',
			'foreignKey' => 'user_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Tag' => array(
			'className' => 'Tag',
			'foreignKey' => 'user_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'UserCategory' => array(
			'className' => 'UserCategory',
			'foreignKey' => 'user_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'VoucherSale' => array(
			'className' => 'VoucherSale',
			'foreignKey' => 'user_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		),
		'Voucher' => array(
			'className' => 'Voucher',
			'foreignKey' => 'user_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);
	
	*/
	public $validate = array (
		"username" => array (
			"notempty" => array (
				"rule" => "notBlank",
				"message" => "Please enter email."
			),
			
			"email" => array (
				"rule" => "email",
				"message" => "Please enter valid email."
			),
			
			"isunique" => array (
				"rule" => "isunique",
				"message" => "email is already in use.",
				//"on"=>"create"
			),
			
		),
		"password" => array (
		    "notempty" => array (
				"rule" => "notBlank",
				"message" => "Please enter Password."
				
			),
				
			"length" => array (
				"rule" => array ('between', 6, 20),
				"message" => "Your password must be between 6 and 20 characters."
			)
		),
		"confirm_password" => array(
            "notempty" => array (
				"rule" => "notBlank",
				"message" => "Please enter confirm Password."
				
			),
              "match"=>array(
              "rule" => "validate_password",
              "message" => "Passwords do not match"
      )
    )
		
		
	);
	
	function validate_password() {
		return (isset($this->data['User']['confirm_password']) && ($this->data['User']['password'] == $this->data['User']['confirm_password']))?true:false;
	}
	

}
