<?php
App::uses('AppModel', 'Model');
/**
 * Voucher Model
 *
 * @property User $User
 * @property VoucherSale $VoucherSale
 */
class Calendar extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'title';


	// The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		"Tag" => array(
				"className" => "Tag",
				"foreignKey" => "tags",
				"type" => "Inner",
				'conditions' => false,
			)
		//~ ,
		//~ "UserDetail" => array(
				//~ "className" => "UserDetail",
				//~ "foreignKey" => false,
				//~ "type" => "Inner",
				//~ 'conditions' => 'Calendar.user_id=UserDetail.user_id',
		//~ )
	);
	//~ public $hasOne = array(			
			//~ "Tags" => array(
				//~ "className" => "Tags",
				//~ "foreignKey" => false,
				//~ "type" => "Inner",
				//~ 'conditions' => 'Calendar.tags=Tags.id',
			//~ )
		//~ );
	public $hasMany= array(
			"EventView" => array(
				"className" => "EventView",
				"foreignKey" => "event_id",
				"type" => "Inner",
				'dependent' => true,
			),
			"SaveCalendar" => array(
				"className" => "SaveCalendar",
				"foreignKey" => "event_id",
				"type" => "Inner",
				'dependent' => true,
			),
			"Comments" => array(
				"className" => "Comments",
				"foreignKey" => "event_id",
				"type" => "Inner",
				'dependent' => true,
			)
		);
/**
 * hasMany associations
 *
 * @var array
	public $hasMany = array(
		'VoucherSale' => array(
			'className' => 'VoucherSale',
			'foreignKey' => 'voucher_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	); */
	

}
